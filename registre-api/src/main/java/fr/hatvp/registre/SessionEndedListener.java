/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.PublicationService;

/**
 * Listener de fin de session utilisé pour gérer certaines ressources quand une session HTTP se termine.
 * Utilisé pour nettoyer les documents de publications temporaires stockés en base de données.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Component
public class SessionEndedListener implements ApplicationListener<SessionDestroyedEvent> {

    /** Service des publications. */
    @Autowired
    private PublicationService publicationService;
    
    /**
     * Méthode appelée lors de la destruction de la session.
     * On y lance le nettoyage de la publication temporaire pour la session en cours.
     */
    @Override
    public void onApplicationEvent(SessionDestroyedEvent event)
    {
        this.publicationService.viderSessionPublication(event.getId());
    }

}
