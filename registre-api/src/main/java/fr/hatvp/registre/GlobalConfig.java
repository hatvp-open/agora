/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *
 * Cette classe permet la configuration globale de l'application.
 *
 * @version $Revision$ $Date${0xD}
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "fr.hatvp.registre")
@PropertySource(value = "classpath:application.properties", encoding = "UTF-8")
@ImportResource("classpath:applicationContext-jpa.xml")
public class GlobalConfig extends WebMvcConfigurerAdapter {

	/** Initialisation du bean des placeHolder. */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {

		return new PropertySourcesPlaceholderConfigurer();
	}

	/** Initialisation du bean multipart pour le téléchargement des fichiers. */
	@Bean
	public CommonsMultipartResolver multipartResolver() {

		final CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setDefaultEncoding("UTF-8");
		return multipartResolver;
	}

}