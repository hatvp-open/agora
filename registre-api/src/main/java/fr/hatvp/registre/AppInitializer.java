/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 *
 * Cette classe permet d'initialiser spring mvc.
 *
 * @version $Revision$ $Date${0xD}
 */
public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected Class<?>[] getRootConfigClasses() {

		return new Class[] { GlobalConfig.class };
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isAsyncSupported() {
		return true;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void onStartup(final ServletContext servletContext) throws ServletException {
		super.onStartup(servletContext);
		servletContext.addListener(new SessionConfig());
		// Listener pour les événements relatifs à la session HTTP.
		servletContext.addListener(HttpSessionEventPublisher.class);
	}

}
