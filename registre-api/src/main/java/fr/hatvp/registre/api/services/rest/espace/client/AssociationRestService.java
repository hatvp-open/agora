/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace.client;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.business.ClientAndAssociationsService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.OrganisationService;
import fr.hatvp.registre.commons.dto.AssoClientBatchDto;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;

/**
 * Controlleur des associations.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/information/association")
public class AssociationRestService {

	/**
	 * Sevice de gestion des associations et clients.
	 */
	@Autowired
	private ClientAndAssociationsService associationsService;

	/**
	 * Sevice de gestion des clients.
	 */
	@Autowired
	private OrganisationService organisationService;
	
    /** Service Espace  */
    @Autowired
    private EspaceOrganisationService espaceOrganisationService;

	/**
	 * Renvoie une liste paginée des associations
	 *
	 * @param pageNumber
	 * @param resultPerPage
	 * @param currentEspaceOrganisationId
	 * @return clients for a given corporate space id paginated for a given page with a defined number of occurrences
	 */
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRATEUR', 'ROLE_PUBLICATEUR', 'ROLE_CONTRIBUTEUR')")
	@GetMapping("/page/{page_no}/{result_per_page}")
	public ResponseEntity<PaginatedDto<OrganisationDto>> getPaginatedEspaceCorporateAssociations(@PathVariable("page_no") final Integer pageNumber,
			@PathVariable("result_per_page") final Integer resultPerPage, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {

		final PaginatedDto<OrganisationDto> paginatedAssociations = this.associationsService.getPaginatedAssociations(pageNumber, resultPerPage, currentEspaceOrganisationId);

		return new ResponseEntity<>(paginatedAssociations, HttpStatus.OK);
	}

	/**
	 * Ajout d'une association d'apartenance à l'espace collaboratif.
	 *
	 * @param organisationDto
	 *            : l'organisation à ajouter.
	 * @param context
	 *            : le contexte d'ajout (client ou association).
	 * @param currentEspaceOrganisationId
	 *            : l'identifiant de l'espace (du contexte) auquel on ajoute l'organisation.
	 * @return le client ou association enregistré.
	 */
	@PostMapping
	public ResponseEntity<AssoClientDto> addAssociation(@RequestBody @Valid final OrganisationDto organisationDto,
			@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		final AssoClientDto savedClient = this.associationsService.addAssociation(currentEspaceOrganisationId, organisationDto);
        espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		return new ResponseEntity<>(savedClient, HttpStatus.CREATED);
	}

	/**
	 * Supprimer une association.
	 *
	 * @param id
	 *            id de l'rganisation pro.
	 * @param authentication
	 *            objet d'authentification.
	 * @return 200 si le client a bié été supprimé.
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteAssociation(@PathVariable final Long id, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		this.associationsService.deleteAssociation(id, currentEspaceOrganisationId);
        espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Vérification préalable, en vur d'obtention d'un rapport complet, pour... l'ajout d'associations d'apartenance à l'espace collaboratif, par lot.
	 *
	 * @param organisationIdList
	 *            : les organisations à ajouter.
	 * @param currentEspaceOrganisationId
	 *            : l'identifiant de l'espace (du contexte) auquel on ajoute l'organisation.
	 * @return l'association enregistré.
	 */
	@PostMapping(path = "/verify")
	public ResponseEntity<AssoClientBatchDto> verifyAssociationBatch(@RequestBody final List<String> organisationIdList,
			@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		AssoClientBatchDto batchReport = this.organisationService.lookUpByNationalNumberBatch(organisationIdList);
		batchReport = this.associationsService.verifyAssociationBatch(batchReport, currentEspaceOrganisationId);
		return new ResponseEntity<>(batchReport, HttpStatus.FOUND);
	}

	/**
	 * Ajout effective de clients ou d'associations d'apartenance à l'espace collaboratif, par lot.
	 *
	 * @param organisationIdList
	 *            : les organisations à ajouter.
	 * @param currentEspaceOrganisationId
	 *            : l'identifiant de l'espace (du contexte) auquel on ajoute l'organisation.
	 * @return void
	 */
	@PostMapping(path = "/batch")
	public ResponseEntity<Void> addAssociationBatch(@RequestBody final List<String> organisationIdList, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		AssoClientBatchDto batchReport = this.organisationService.lookUpByNationalNumberBatch(organisationIdList);
		this.associationsService.addAssociationBatch(currentEspaceOrganisationId, batchReport.getOrganisationList());
        espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		return ResponseEntity.accepted().build();
	}
}
