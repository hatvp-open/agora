/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

/**
 *
 * Cette classe configure le succès de déconnexion.
 * Permet d'éviter la redirection par défaut.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CustomLogoutSuccess
    implements LogoutSuccessHandler
{
    /**
     * {@inheritDoc}
     */
    @Override
    public void onLogoutSuccess(final HttpServletRequest httpServletRequest,
                                final HttpServletResponse httpServletResponse, final Authentication authentication)
        throws IOException,
        ServletException
    {
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
    }
}
