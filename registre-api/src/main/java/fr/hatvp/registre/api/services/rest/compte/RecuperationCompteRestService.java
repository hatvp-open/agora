/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.compte;

import static fr.hatvp.registre.commons.utils.ValidateUtils.validatePassword;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fr.hatvp.registre.business.DeclarantService;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.validators.ValidationMessagesConstantes;

import java.util.Map;

/**
 * Controlleur de récupération de compte.
 * 
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/account/recover")
public class RecuperationCompteRestService
{
    /**
     * LOggeur des informations.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(RecuperationCompteRestService.class);

    /**
     * Service de déclarant utilisé.
     */
    private DeclarantService declarantService;

    /**
     * Initialisation du service.
     *
     * @param declarantService service à initialisé.
     */
    @Autowired
    public void setDeclarantService(final DeclarantService declarantService)
    {
        this.declarantService = declarantService;
    }

    /**
     * Demande de récupération de compte.
     *
     * @param email contient l'email de récupération de compte.
     * @return http status 202, si un mail de récupération a été envoyé au déclarant.
     */
    @PutMapping("/email")
    public ResponseEntity<Void> recoverAccountEmail(@RequestBody Map<String, String> emailMap)
    {
        String email = emailMap.get("email");

        if ((email == null) || email.isEmpty()) {
            LOGGER.error("{}\n {}", ValidationMessagesConstantes.ADRRESSE_EMAIL_INTROUVABLE, email);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }

        this.declarantService.recuperationDeCompte(email);
        
        LOGGER.info( "Demande changement de mot de passe compte {}", emailMap);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);

    }

    /***
     * Validation de la récupération de compte.
     *
     * @param declarantDto contient le nouveau et l'ancien mot de passe.
     * @param key clé de récupération de compte.
     * @return http status 202 si le compte a bien été récupérer.
     */
    @PutMapping("/password/{key}")
    public ResponseEntity<Void> recoverAccountPassword(@RequestBody final DeclarantDto declarantDto,
            @PathVariable final String key)
    {

        final String password = declarantDto.getPassword();

        if (declarantDto.getPreviousPassword() != null) {

            LOGGER.error("{}\n {}", ValidationMessagesConstantes.LE_CHAMP_PREV_PASSWORD_N_EST_PAS_AUTORISE, declarantDto);
            throw new BusinessGlobalException(
                    ValidationMessagesConstantes.LE_CHAMP_PREV_PASSWORD_N_EST_PAS_AUTORISE, 400);
        }

        if (!validatePassword(password)) {
            LOGGER.error(ValidationMessagesConstantes.MOT_DE_PASSE_INCORRECT);
            throw new BusinessGlobalException(ValidationMessagesConstantes.MOT_DE_PASSE_INCORRECT,
                    400);
        }

        /*
         * Ensure that only password will be passed to update method
         */
        final DeclarantDto declarantDtoTemp = new DeclarantDto();
        declarantDtoTemp.setRecuperationPasswordKeyCode(key);
        declarantDtoTemp.setPassword(declarantDto.getPassword());

        this.declarantService.updateMotDePasseRecuperationDeCompte(declarantDtoTemp);
        
        LOGGER.info( "Changement de mot de passe nUserId= {}", declarantDto.getId());

        return new ResponseEntity<>(HttpStatus.ACCEPTED);

    }

}
