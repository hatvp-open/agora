/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.declarant;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.DeclarantService;
import fr.hatvp.registre.commons.dto.DeclarantContactDto;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.utils.ValidateUtils;
import fr.hatvp.registre.commons.validators.ValidationMessagesConstantes;

/**
 * Controlleur qui permet de mettre à jour des déclarants.
 *
 * @version $Revision$ $Date${0xD}
 */

@RestController
@RequestMapping("/declarant")
public class UpdateDeclarantRestService {

	/** Le logger d'information. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateDeclarantRestService.class);

	/** Le service de déclarant à utiliser. */
	private DeclarantService declarantService;

	/**
	 * Initialisation du service.
	 *
	 * @param declarantService
	 *            le service à injecter.
	 */
	@Autowired
	public void setDeclarantService(final DeclarantService declarantService) {
		this.declarantService = declarantService;
	}

	/**
	 * Cette méthode permet de mettre à jour l'email principal.
	 *
	 * @param declarantDto
	 *            objet déclarant en entrée.
	 * @param authentication
	 *            objet d'authentification injecté par spring security.
	 * @return les informations du déclarant mises à jour.
	 */
	@PutMapping("/email")
	public ResponseEntity<DeclarantDto> updateDeclarantEmail(@RequestBody Map<String, String> emailMap, @CurrentUserId Long userId) {
		String email = emailMap.get("email");

		// Vérification du format de l'email
		if (StringUtils.isEmpty(email)) {
			LOGGER.error("Appel avec paramètre email incorrect.");
			throw new BusinessGlobalException(ValidationMessagesConstantes.LE_FORMAT_DU_MAIL_N_EST_PAS_VALIDE, 400);
		}
		// EMAIL pattern: RFC 5322
		if (!ValidateUtils.validateEmail(email)) {
			LOGGER.error("{}\n{}", ValidationMessagesConstantes.LE_FORMAT_DU_MAIL_N_EST_PAS_VALIDE, email);
			throw new BusinessGlobalException(ValidationMessagesConstantes.LE_FORMAT_DU_MAIL_N_EST_PAS_VALIDE, 400);
		}

		// Préparation d'un DTO pour persistance de l'email uniquement
		DeclarantDto declarantDto = new DeclarantDto();
		declarantDto.setEmail(email);
		declarantDto.setId(userId);

		final DeclarantDto newDeclarantDto = this.declarantService.updateDeclarant(declarantDto);
		return new ResponseEntity<>(newDeclarantDto, HttpStatus.ACCEPTED);

	}

	/**
	 * Cette méthode permet de mettre à jour le téléphone.
	 *
	 * @param telephone
	 *            à persister.
	 * @param userid
	 *            injecté par spring security.
	 * @return les informations du déclarant mises à jour.
	 */
	@PutMapping("/telephone")
	public ResponseEntity<DeclarantDto> updateDeclarantTelephone(@RequestBody Map<String, String> telephoneMap, @CurrentUserId Long userId) {
		String telephone = telephoneMap.get("telephone");

		if (StringUtils.isEmpty(telephone)) {
			throw new BusinessGlobalException(ErrorMessageEnum.TELEPHONE_INTROUVABLE.getMessage(), 404);
		}

		DeclarantDto declarantDto = new DeclarantDto();
		declarantDto.setId(userId);
		declarantDto.setTelephone(telephone);
		final DeclarantDto newDeclarantDto = this.declarantService.updateDeclarant(declarantDto);

		return new ResponseEntity<>(newDeclarantDto, HttpStatus.ACCEPTED);
	}

	/**
	 * Cette méthode permet de mettre à jour le mot de passe.
	 *
	 * @param password
	 *            password à modifier.
	 * @param prev_password
	 *            ancien password pour valider le changement.
	 * @param userid
	 *            injecté par spring security.
	 * @return les informations du déclarant mmisent à jour.
	 */
	@PutMapping("/password")
	public ResponseEntity<DeclarantDto> updateDeclarantPassword(@RequestBody final Map<String, String> passwordMap, @CurrentUserId Long userId) {
		String password = passwordMap.get("password");
		String prevPassword = passwordMap.get("prev_password");

		if (!StringUtils.isEmpty(password)) {

			if (StringUtils.isEmpty(prevPassword)) {
				LOGGER.error( "{} \nUserId= {}", ValidationMessagesConstantes.ANCIEN_MOT_DE_PASSE_INCORRECT, userId);
				throw new BusinessGlobalException(ValidationMessagesConstantes.ANCIEN_MOT_DE_PASSE_INCORRECT, 400);
			}

			if (!ValidateUtils.validatePassword(password)) {
				LOGGER.error( "{} \nUserId= {}", ValidationMessagesConstantes.LE_PATTERN_DU_MOT_DE_PASSE_INVALIDE, userId);
				throw new BusinessGlobalException(ValidationMessagesConstantes.LE_PATTERN_DU_MOT_DE_PASSE_INVALIDE, 400);
			}
		}

		DeclarantDto declarantDto = new DeclarantDto();
		declarantDto.setId(userId);
		declarantDto.setPassword(password);
		declarantDto.setPreviousPassword(prevPassword);
		final DeclarantDto newDeclarantDto = this.declarantService.updateDeclarant(declarantDto);

		return new ResponseEntity<>(newDeclarantDto, HttpStatus.ACCEPTED);
	}

	/**
	 * Cette méthode permet de metttre à jour les données de contact.
	 *
	 * @param emails_complement
	 *            : la liste des emails complémentaires saisis.
	 * @param telephones
	 *            : la liste des téléphones complémentaires saisis.
	 * @param userid
	 *            injecté par spring security.
	 * @return les informations du déclarant mmises à jour.
	 */
	@PutMapping("/contact")
	public ResponseEntity<DeclarantDto> updateDeclarantContact(@RequestBody final Map<String, List<DeclarantContactDto>> contactMap, @CurrentUserId Long userId) {
		List<DeclarantContactDto> emailsComplementaires = contactMap.get("emails_complement");
		List<DeclarantContactDto> telephonesComplementaires = contactMap.get("telephones");

		// Validation des catégories saisies pour les emails
		if (emailsComplementaires != null && !emailsComplementaires.isEmpty()
				&&ValidateUtils.isContactListCategoryValid(emailsComplementaires)) {
			throw new BusinessGlobalException(ValidationMessagesConstantes.CATEGORY_INCORRECT, 400);
		}

		// Validation des catégories saisies pour les téléphones
		if (telephonesComplementaires != null && !telephonesComplementaires.isEmpty()
				&& ValidateUtils.isContactListCategoryValid(telephonesComplementaires)) {
			throw new BusinessGlobalException(ValidationMessagesConstantes.CATEGORY_INCORRECT, 400);
		}

		DeclarantDto declarantDto = new DeclarantDto();
		declarantDto.setId(userId);
		declarantDto.setEmailComplement(emailsComplementaires);
		declarantDto.setPhone(telephonesComplementaires);

		final DeclarantDto newDeclarantDto = this.declarantService.updateDeclarant(declarantDto);

		return new ResponseEntity<>(newDeclarantDto, HttpStatus.ACCEPTED);
	}

	@PutMapping("/guidelines")
    public void hasReadGuidelines(@CurrentUserId Long userId){
        declarantService.updateFirstConn(userId);
    }
}
