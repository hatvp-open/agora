/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.declarant;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.hatvp.registre.business.DeclarantService;
import fr.hatvp.registre.business.GestionPiecesEspaceDeclarantService;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.EspaceDeclarantPieceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.commons.utils.PiecesUtils;

/**
 * Controlleur qui permet d'enregistrer des déclarants.
 *
 * @version $Revision$ $Date${0xD}
 */

@RestController
@RequestMapping("/declarant/add")
public class AddDeclarantRestService
{

	/** Le logger d'information. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AddDeclarantRestService.class);

	/**
	 * Le service de déclarant à utiliser.
	 */
	private DeclarantService declarantService;

	/** Service pour pour la gestion des pièces du déclarant. */
	@Autowired
	private GestionPiecesEspaceDeclarantService gestionPiecesEspaceDeclarantService;

	/**
	 * Initialisation du service.
	 *
	 * @param declarantService
	 *         le service à injecter.
	 */
	@Autowired
	public void setDeclarantService(final DeclarantService declarantService)
	{
		this.declarantService = declarantService;
	}

	/**
	 * Inscription d'un déclarant.
	 *
	 * @param file1 fichier de pièce d'identité à charger.
	 * @param declarant les informations du declarant
	 * @return http status 200 si bien chargé et données enregistrées, 404: si des éléments sont
	 * introuvable dans la requête
	 */
	@PostMapping
	public ResponseEntity<DeclarantDto> addDeclarant(
			@RequestParam final MultipartFile file1,
			@RequestParam final String declarant)
	{

		// Validation présence pièce d'identité
		if (file1.isEmpty()) {
			throw new BusinessGlobalException(ErrorMessageEnum.VEUILLEZ_SELECTIONNER_UN_FICHIER_A_CHARGER.getMessage());
		}

		// Validation de la conformité des pièces chargées
		PiecesUtils.checkConformitePiece(file1);

		// Transformation des pièces récupérées
		EspaceDeclarantPieceDto piece1 = PiecesUtils.mapToEspaceDeclarantPieceDto(file1);

		// Validation declarant
		if (declarant == null) {
			throw new BusinessGlobalException(ErrorMessageEnum.INVALID_DECLARANT_DATA.getMessage());
		}

		try {
			//Transformer le string objet organisation envoyer en paramètre, en un DTO organisation
			final ObjectMapper mapper = new ObjectMapper();
			DeclarantDto declarantDto;
			try {
				declarantDto = mapper.readValue(declarant, DeclarantDto.class);
			} catch (JsonMappingException | JsonParseException e) {
				LOGGER.error(ErrorMessageEnum.INVALID_DECLARANT_DATA.getMessage() + " : {}", e);
				throw new BusinessGlobalException(ErrorMessageEnum.INVALID_DECLARANT_DATA.getMessage());
			}

			//Sauvegarder le declarant
			final DeclarantDto declarantDto1 = this.declarantService.saveDeclarant(declarantDto);
			declarantDto1.setPassword(null);// clean password

			this.gestionPiecesEspaceDeclarantService.verserPiece(
					declarantDto1.getId(),
					piece1,
					OrigineVersementEnum.INSCRIPTION_DECLARANT);

			LOGGER.info("Le déclarant a été enregistré! \n {}", declarantDto1);

			return new ResponseEntity<>(declarantDto1, HttpStatus.CREATED);
		} catch (final IOException e) {
			LOGGER.error(ErrorMessageEnum.INVALID_DECLARANT_DATA.getMessage() + " : {}", e);
			throw new BusinessGlobalException(ErrorMessageEnum.INVALID_DECLARANT_DATA.getMessage());
		}
	}

}
