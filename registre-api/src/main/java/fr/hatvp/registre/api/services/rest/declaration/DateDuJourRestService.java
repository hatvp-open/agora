/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.declaration;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.commons.utilities.LocalDateNowBuilder;

@Profile({ "dev", "qualif", "preprod" })
@RestController
@RequestMapping("/datedujour")
public class DateDuJourRestService {

	@Autowired
	private LocalDateNowBuilder localDateNowBuilder;

	@GetMapping("/{date}")
	public ResponseEntity<Void> setDate(@PathVariable final String date) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		localDateNowBuilder.setClock(Clock.fixed(LocalDate.parse(date, dtf).atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault()));
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
