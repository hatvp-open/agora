/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.declaration;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.DeclarationActiviteService;
import fr.hatvp.registre.commons.dto.activite.ActiviteRepresentationInteretDto;

/**
 * Controlleur Rest qui permet de gérer les déclarations d'activitée.
 *
 */
@RestController
@RequestMapping("/declaration/fiche")
public class DeclarationActiviteRestService {

	@Autowired
	private DeclarationActiviteService declarationActiviteService;

	/**
	 * récupére une déclaration d'activitée
	 *
	 * @param ficheId
	 * @param currentEspaceOrganisationId
	 * @return une déclaration d'activitée
	 */
	@GetMapping("/{ficheId}")
	public ResponseEntity<ActiviteRepresentationInteretDto> getFiche(@PathVariable final Long ficheId, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		ActiviteRepresentationInteretDto activiteRepresentationInteret = this.declarationActiviteService.getDeclarationActivite(ficheId);
		return ResponseEntity.ok(activiteRepresentationInteret);
	}

	/**
	 * enregistre une déclaration d'activitée
	 *
	 * @param activiteRepresentationInteret
	 * @param currentEspaceOrganisationId
	 * @param currentUserId
	 * @return une déclaration d'activitée
	 */
	@PostMapping()
	public ResponseEntity<ActiviteRepresentationInteretDto> addFiche(@RequestBody @Valid final ActiviteRepresentationInteretDto activiteRepresentationInteret,
			@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId, @CurrentUserId final Long currentUserId) {

		return ResponseEntity.ok(this.declarationActiviteService.createDeclarationActivite(activiteRepresentationInteret, currentUserId, currentEspaceOrganisationId));
	}

	/**
	 * met à jour une déclaration d'activitée
	 *
	 * @param ficheId
	 * @param activiteRepresentationInteret
	 * @param currentEspaceOrganisationId
	 * @return une déclaration d'activitée
	 */
	@PutMapping("/{ficheId}")
	public ResponseEntity<ActiviteRepresentationInteretDto> updateFiche(@PathVariable final Long ficheId,
			@RequestBody @Valid final ActiviteRepresentationInteretDto activiteRepresentationInteret, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		return ResponseEntity.accepted().body(this.declarationActiviteService.updateDeclarationActivite(activiteRepresentationInteret, currentEspaceOrganisationId));
	}

	/**
	 * supprime une déclaration d'activitée
	 *
	 * @param ficheId
	 * @param currentEspaceOrganisationId
	 * @return rien
	 */
	@DeleteMapping("/{ficheId}")
	public ResponseEntity<Void> deleteFiche(@PathVariable final Long ficheId, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		this.declarationActiviteService.deleteDeclarationActivite(ficheId);
		return ResponseEntity.accepted().build();
	}

}