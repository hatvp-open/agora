/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.security;

import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * Cette interface permet de rajouter des données supplémentaires à l'authentification.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface MyUserDetails
    extends UserDetails
{
    /** id de l'utilisateur connecté. */
    Long getId();

    /** id de l'espace organisation courrrant. */
    Long getCurrentEspaceOrganisationId();

    void setCurrentEspaceOrganisationId(Long currentEspaceOrganisationId);

    Boolean getCurrentEspaceEnabled();

    void setCurrentEspaceEnabled(Boolean currentEspaceEnabled);
}
