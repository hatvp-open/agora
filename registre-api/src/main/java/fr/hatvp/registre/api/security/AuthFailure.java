/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.hatvp.registre.commons.dto.erreur.ErrorResourceDto;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Cette classe configure l'erreur d'authentification.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class AuthFailure
    extends SimpleUrlAuthenticationFailureHandler {

    /** LOGGER de classe. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthFailure.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAuthenticationFailure(final HttpServletRequest request,
                                        final HttpServletResponse response, final AuthenticationException exception)
        throws IOException, ServletException {
        super.onAuthenticationFailure(request, response, exception);

        LOGGER.error("Erreur d'authentification: {}", exception.getMessage());

        final ErrorResourceDto error = new ErrorResourceDto();
        error.setCode("401");

        if (exception instanceof BadCredentialsException) {
            error.setMessage(ErrorMessageEnum.ERREUR_AUTHENTIFICATION.getMessage());
        } else if (exception instanceof DisabledException) {
            error.setMessage(ErrorMessageEnum.COMPTE_INACTIF.getMessage());
        } else {
            error.setMessage(exception.getMessage());
        }

        final ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(error));

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
}
