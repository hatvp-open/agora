/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.hatvp.registre.business.ExerciceComptableService;
import fr.hatvp.registre.business.InscriptionEspaceService;
import fr.hatvp.registre.commons.dto.*;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.DesinscriptionService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.OrganisationService;
import fr.hatvp.registre.business.security.EspaceOrganisationSecurityService;
import fr.hatvp.registre.commons.dto.client.ClientSimpleDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.exceptions.InvalidRequestException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.IdentifiantCardInfosGeneralesEnum;
import fr.hatvp.registre.commons.utils.PiecesUtils;
import fr.hatvp.registre.commons.validators.ValidationMessagesConstantes;

/**
 * Controlleur qui permet de gérer l'espace organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate")
public class EspaceOrganisationRestService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EspaceOrganisationRestService.class);

	/** Service utilisé avec ce controlleur. */
	private OrganisationService organisationService;

	/** Service pour Espace Organisation */
	@Autowired
	private EspaceOrganisationService espaceOrganisationService;

	/** Service de sécurité pour les espaces corporates. */
	@Autowired
	private EspaceOrganisationSecurityService espaceOrganisationSecurityService;

    @Autowired
    private InscriptionEspaceService inscriptionEspaceService;

    @Autowired
    private ExerciceComptableService exerciceComptableService;

    @Autowired
    private DesinscriptionService desinscriptionService;

	/**
	 * Initialisation du service.
	 *
	 * @param organisationService
	 *            service à initialisé.
	 */
	@Autowired
	public void setEspaceOrganisationService(final OrganisationService organisationService) {
		this.organisationService = organisationService;
	}

	/**
	 * Vérifie si l'organisation a un espace organisation, et renvoie les données de l'organisation.
	 *
	 * @param organisationId
	 *            id de l'organisation à rechercher.
	 * @return l'organisation recherchée.
	 */
	@GetMapping("/{organization_id}")
	public ResponseEntity<OrganisationDto> getEspaceCorporate(@PathVariable("organization_id") final Long organisationId) {

		return new ResponseEntity<>(this.organisationService.getOrganizationDataById(organisationId), HttpStatus.OK);
	}

	/**
	 * Récupérer organisation courant pour infos générales
	 *
	 * @param currentEspaceOrganisationId
	 *            identifiant espace organisation injecté depuis contexte Spring Security.
	 * @param currentUserId
	 *            identifiant utilisateur courant injecté depuis contexte Spring Security.
	 * @return informations générales organisation
	 */
	@GetMapping("/find/espace")
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRATEUR', 'ROLE_PUBLICATEUR', 'ROLE_CONTRIBUTEUR')")
	public ResponseEntity<EspaceOrganisationAffichageFODto> getById(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId, @CurrentUserId final Long currentUserId) {

		this.espaceOrganisationSecurityService.checkUserAuthorization(currentEspaceOrganisationId, currentUserId);
		final EspaceOrganisationAffichageFODto organisation = this.espaceOrganisationService.getEspaceOrganisationInformations(currentEspaceOrganisationId);

		return ResponseEntity.ok(organisation);
	}

	/**
	 * Upload du logo de l'espace organisation.
	 *
	 * @return l'organisation recherchée.
	 */
	@PostMapping("/logo/upload")
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	public ResponseEntity<Void> uploadLogo(@RequestParam("file") final MultipartFile file, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		if (file.getSize() > FileUtils.ONE_MB) {
			throw new BusinessGlobalException(ValidationMessagesConstantes.MSG_TAILLE_LIMITE_FICHIER_LOGO, 422);
		}

		final String[] allowedFileExtensions = { "image/jpg", "image/jpeg", "image/png" };

		if (!PiecesUtils.checkFileExtensionAndType(file, allowedFileExtensions)) {
			throw new BusinessGlobalException(ValidationMessagesConstantes.MSG_EXTENSIONS_ACCEPTEES, 422);
		}

		try {
			this.espaceOrganisationService.uploadLogo(currentEspaceOrganisationId, file.getBytes(), file.getContentType());
		} catch (final IOException e) {
			throw new BusinessGlobalException(e.getLocalizedMessage(), 400);
		}

		return ResponseEntity.accepted().build();
	}

	/**
	 * Suppression du logo de l'espace organisation.
	 *
	 * @return l'organisation recherchée.
	 */
	@DeleteMapping("/logo/upload")
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	public ResponseEntity<Void> deleteLogo(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		this.espaceOrganisationService.deleteLogo(currentEspaceOrganisationId);

		return ResponseEntity.accepted().build();
	}

	/**
	 * Mettre à jour les informations générales pour une organisation.
	 *
	 * @param cardId
	 *            identifiant de la card côté front
	 * @param espaceOrganisation
	 *            DTO de l'espace organisation
	 * @param currentEspaceOrganisationId
	 *            l'identifiant de l'espace courant en session à modifier
	 * @param bindingResult
	 *            objet bindings jackson
	 * @return 202 accepted
	 */
	@PutMapping("/update/{cardId}")
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	public ResponseEntity<EspaceOrganisationDto> updateData(@PathVariable final IdentifiantCardInfosGeneralesEnum cardId, @RequestBody @Valid final EspaceOrganisationDto espaceOrganisation,
			@CurrentEspaceOrganisationId Long currentEspaceOrganisationId, final BindingResult bindingResult) {

		EspaceOrganisationDto espaceOrganisationDto = null;

		// Injection de l'id de l'espace courant pour éviter l'altération des données d'un autre espace
		espaceOrganisation.setId(currentEspaceOrganisationId);

		// Validations de surface selon carte validée et MAJ données de la carte
		switch (cardId) {
		case ASSO_APPARTENANCE:

			break;
		case DIRIGEANTS:


			espaceOrganisationDto = this.espaceOrganisationService.updateDirigeants(espaceOrganisation);
			break;
		case DONNEES:


			espaceOrganisationDto = this.espaceOrganisationService.updateDonnees(espaceOrganisation);
			break;
		case INFOS_CONTACT:


			espaceOrganisationDto = this.espaceOrganisationService.updateInfosContact(espaceOrganisation);
			break;
		case LOCALISATION:


			espaceOrganisationDto = this.espaceOrganisationService.updateLocalisation(espaceOrganisation);
			break;
		case PROFILE_ORGA:

			if (espaceOrganisation.getCategorieOrganisation() == null) {
				throw new InvalidRequestException(ErrorMessageEnum.DONNEES_INCORRECTES.getMessage(), bindingResult);
			}

			espaceOrganisationDto = this.espaceOrganisationService.updateProfileOrga(espaceOrganisation, false);
			break;
		case WEB:


			espaceOrganisationDto = this.espaceOrganisationService.updateWeb(espaceOrganisation);
			break;
		case PRESENTATION_INTERET:

			if (espaceOrganisation.getListSecteursActivites().isEmpty() || espaceOrganisation.getListNiveauIntervention().isEmpty()
					|| espaceOrganisation.getListNiveauIntervention().size() > 5) {
				throw new InvalidRequestException(ErrorMessageEnum.DONNEES_INCORRECTES.getMessage(), bindingResult);
			}
			espaceOrganisationDto = this.espaceOrganisationService.updateActivites(espaceOrganisation);
			break;
		case EXERCICE_COMPTABLE:

			espaceOrganisationDto = this.espaceOrganisationService.updateExerciceComptable(espaceOrganisation);
			break;
		}

		return ResponseEntity.accepted().body(espaceOrganisationDto);
	}

	/**
	 * Ajout d'un lien externe avec la liste des membres
	 *
	 * @param currentEspaceOrganisationId
	 *            id de l'espace organisation.
	 * @param espaceOrganisation
	 *            données de l'espace organisation.
	 * @return données mise à jour de l'espace organisation.
	 */
	@PutMapping("/update/lienMembres")
	public ResponseEntity<EspaceOrganisationDto> addLienMembres(@CurrentEspaceOrganisationId Long currentEspaceOrganisationId,
			@RequestBody @Valid final EspaceOrganisationDto espaceOrganisation) {
		EspaceOrganisationDto espaceOrganisationDto = null;
		espaceOrganisationDto = this.espaceOrganisationService.updateLienMembres(espaceOrganisation, currentEspaceOrganisationId);
		return ResponseEntity.accepted().body(espaceOrganisationDto);
	}

	/**
	 * Attestation de OUI/NON déclaration pour des clients.	 * 
	 *
	 * @param currentEspaceOrganisationId
	 *            id de l'espace organisation.
	 * @param espaceOrganisationDto
	 *            données de l'espace organisation.
	 * @return données mise à jour de l'espace organisation.
	 */
	@PutMapping("/atteste/client")
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	public ResponseEntity<EspaceOrganisationDto> setAttestationDePublicationClient(@CurrentEspaceOrganisationId Long currentEspaceOrganisationId,
			@RequestBody final EspaceOrganisationDto espaceOrganisationDto) {
		final EspaceOrganisationDto dto = this.espaceOrganisationService.setAttestationClient(currentEspaceOrganisationId, espaceOrganisationDto);
		return ResponseEntity.accepted().body(dto);
	}

	/**
	 * Attestation de OUI/NON déclaration des associations.
	 *
	 * @param currentEspaceOrganisationId
	 *            id de l'espace organisation.
	 * @param espaceOrganisationDto
	 *            données de l'espace organisation.
	 * @return données mise à jour de l'espace organisation.
	 */
	@PutMapping("/atteste/association")
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	public ResponseEntity<EspaceOrganisationDto> setAttestationDePublicationAssociation(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId,
			@RequestBody final EspaceOrganisationDto espaceOrganisationDto) {
		final EspaceOrganisationDto dto = this.espaceOrganisationService.setAttestationAssociation(currentEspaceOrganisationId, espaceOrganisationDto);
		return ResponseEntity.accepted().body(dto);
	}

	/**
	 * Retourne la liste des activités disponibles au format JSON.
	 *
	 * @return list des activités.
	 */
	@GetMapping("/activite/list")
	public ResponseEntity<ListActiviteDto> getListeDesActiviteDeRepresentationDinteret() {
		return ResponseEntity.ok(this.espaceOrganisationService.getListeDesActiviteDeRepresentationDinteret());
	}

	/**
	 * Récupérer une liste simplifié des client de l'espace organisation.
	 *
	 * @return liste des clients.
	 */
	@GetMapping("/client/list/simple")
	public ResponseEntity<List<ClientSimpleDto>> getClientEspaceOrganization(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		final List<ClientSimpleDto> clients = this.espaceOrganisationService.getClientEspaceOrganisation(currentEspaceOrganisationId);
		return ResponseEntity.ok().body(clients);
	}

    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    @PostMapping("/unsubscribe")
    public ResponseEntity<DesinscriptionDto> unsubscribe(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId,@CurrentUserId final Long currentUserId,@RequestParam(required = false) final ArrayList<MultipartFile> files, @RequestParam final String demandeDesinscription) {
        // Transformer le string en un DTO
        ArrayList<DesinscriptionPieceDto> pieces = new ArrayList<>();
        final ObjectMapper mapper = new ObjectMapper();
        DesinscriptionDto desinscriptionDto;

        try {
            desinscriptionDto = mapper.readValue(demandeDesinscription, DesinscriptionDto.class);
            // Si au moins une activité ou un moyen ont été publié après la date de cessation on empêche la désinscription
            boolean publicationApresCessation = this.exerciceComptableService.hasPublishedAfterCessationDate(currentEspaceOrganisationId, desinscriptionDto.getCessationDate());
            if(publicationApresCessation){
                throw new BusinessGlobalException(ErrorMessageEnum.DESINSCRIPTION_INTERDITE_FO.getMessage());
            }
            for(MultipartFile file : files){
                PiecesUtils.checkConformitePiece(file);
                pieces.add(PiecesUtils.mapToDesinscriptionDto(file));
            }
            this.inscriptionEspaceService.demanderDesinscription(currentEspaceOrganisationId,currentUserId,desinscriptionDto,pieces);
            return ResponseEntity.accepted().build();
        } catch (IOException e) {
            LOGGER.error(ErrorMessageEnum.ECHEC_ENREGISTREMENT_DESINSCRIPTION.getMessage(), e);
            throw new BusinessGlobalException(ErrorMessageEnum.ECHEC_ENREGISTREMENT_DESINSCRIPTION.getMessage());
        } catch (BusinessGlobalException e) {
            throw new BusinessGlobalException(e.getMessage());
        }
    }
    @GetMapping("/unsubscribe/desinscription_no_activite")
    public ResponseEntity<Void> desinscriptionNoActivite(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
        this.desinscriptionService.sendMailDesinscriptionNoActivite(currentEspaceOrganisationId);
        return ResponseEntity.ok().build();
    }
}
