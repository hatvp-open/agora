/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;

/**
 *
 * Cette classe configure le handler de logout.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CustomLogoutHandler
    implements LogoutHandler
{

    /**
     * {@inheritDoc}
     */
    @Override
    public void logout(final HttpServletRequest request, final HttpServletResponse response,
                       final Authentication authentication)
    {

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if ((auth != null) && auth.isAuthenticated()) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
            response.setStatus(HttpServletResponse.SC_OK);
        }

        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }
}