/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.declarant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.DeclarantService;
import fr.hatvp.registre.commons.dto.DeclarantDto;

/**
 * Controlleur qui permet de récupérer les déclarants.
 * 
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/declarant")
public class GetDeclarantRestService
{
    /** Le logger d'information. */
    private static Logger LOGGER = LoggerFactory.getLogger(GetDeclarantRestService.class);

    /**
     * Le service de déclarant à utiliser.
     */
    private DeclarantService declarantService;

    /**
     * Initialisation du service.
     *
     * @param declarantService le service à injecter.
     */
    @Autowired
    public void setDeclarantService(final DeclarantService declarantService)
    {
        this.declarantService = declarantService;
    }

    /**
     *
     * @param userId user id.
     * @return les informations du déclarant connecté.
     */
    @GetMapping
    public ResponseEntity<DeclarantDto> getConnectedDeclarantData(@CurrentUserId Long userId)
    {
        final DeclarantDto declarantDto = this.declarantService.loadDeclarantById(userId);

        if (declarantDto == null) {
            LOGGER.error("No data found for the connected declarant (BIG PROBLEM)");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        LOGGER.info("Declarant data has been returned successfully");
        return new ResponseEntity<>(declarantDto, HttpStatus.OK);
    }
}
