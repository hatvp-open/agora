/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.DesinscriptionService;
import fr.hatvp.registre.business.GestionPiecesEspaceOrganisationService;
import fr.hatvp.registre.commons.dto.DesinscriptionPieceDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.utils.PiecesUtils;

/**
 * Controlleur qui permet de gérer les pièces de l'espace organisation.
 * 
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate")
public class EspaceOrganisationPiecesRestService {

    /** Service pour pour la gestion des pièces de l'espace. */
    @Autowired
    private GestionPiecesEspaceOrganisationService gestionPiecesEspaceOrganisationService;

    @Autowired
    private DesinscriptionService desinscriptionService;


    
    /**
     * Enregistre les pièces complémentaires soumises par le déclarant.
     * La première pièce est exigées les autre sont facultatives.
     */
    @PostMapping("/complement")
    @PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
    public ResponseEntity<Void> uploadPicesComplementaires(@RequestParam final MultipartFile file1,
                                                           @RequestParam(required=false) final MultipartFile file2,
                                                           @RequestParam(required=false) final MultipartFile file3,
                                                           @RequestParam(required=false) final MultipartFile file4,
                                                           @CurrentUserId final Long currentUserId,
                                                           @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
        // Validation des pièces.
    	PiecesUtils.checkConformitePiece(file1);
        PiecesUtils.checkConformitePiece(file2);
        PiecesUtils.checkConformitePiece(file3);
        PiecesUtils.checkConformitePiece(file4);
        
        // Transformation des fichiers en DTO
        List<EspaceOrganisationPieceDto> pieces = new ArrayList<>();
        EspaceOrganisationPieceDto piece1 = PiecesUtils.mapToDto(file1); 
        EspaceOrganisationPieceDto piece2 = PiecesUtils.mapToDto(file2);
        EspaceOrganisationPieceDto piece3 = PiecesUtils.mapToDto(file3);
        EspaceOrganisationPieceDto piece4 = PiecesUtils.mapToDto(file4);
        
        // Création de la liste des pièces
        // Si pas de pièce versée : erreur.
        if(piece1 == null) {
            throw new BusinessGlobalException(ErrorMessageEnum.VEUILLEZ_SELECTIONNER_UN_FICHIER_A_CHARGER.getMessage());
        }
        
        pieces.add(piece1);

        
        if(piece2 != null) {
            pieces.add(piece2);
        }
        
        if(piece3 != null) {
            pieces.add(piece3);
        }
        
        if(piece4 != null) {
            pieces.add(piece4);
        }

        // Appel du service de versement des pièces complémentaires pour l'espace.
        this.gestionPiecesEspaceOrganisationService.verserPiecesComplementaires(
                currentEspaceOrganisationId, 
                currentUserId, 
                pieces);
        
        return ResponseEntity.accepted().build();
    }


    @PostMapping("/download/attest")
    public ResponseEntity<byte[]> telechargerAttestDesinscription(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId){

        Pair<byte[], String> pdfByteArray = this.gestionPiecesEspaceOrganisationService.buildPdf(currentEspaceOrganisationId);

        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "pdf"));
        header.set("Content-Disposition", "attachment; filename=" + pdfByteArray.getRight() + ".pdf");
        header.setContentLength(pdfByteArray.getLeft().length);
        header.add("x-filename", pdfByteArray.getRight());

        return new ResponseEntity<>(pdfByteArray.getLeft(), header, HttpStatus.OK);
    }
    /**
     * Enregistre les pièces complémentaires soumises par le déclarant.
     * La première pièce est exigées les autre sont facultatives.
     */
    @PostMapping("/unsubscribe/uploadPiecesDesinscription")
    @PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
    public ResponseEntity<Void> uploadPiecesDesinscription(@RequestParam final MultipartFile file1,
                                                           @RequestParam(required=false) final MultipartFile file2,
                                                           @RequestParam(required=false) final MultipartFile file3,
                                                           @RequestParam(required=false) final MultipartFile file4,
                                                           @CurrentUserId final Long currentUserId,
                                                           @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
        // Validation des pièces.
        PiecesUtils.checkConformitePiece(file1);
        PiecesUtils.checkConformitePiece(file2);
        PiecesUtils.checkConformitePiece(file3);
        PiecesUtils.checkConformitePiece(file4);

        // Transformation des fichiers en DTO
        List<DesinscriptionPieceDto> pieces = new ArrayList<>();
        DesinscriptionPieceDto piece1 = PiecesUtils.mapToDesinscriptionDto(file1);
        DesinscriptionPieceDto piece2 = PiecesUtils.mapToDesinscriptionDto(file2);
        DesinscriptionPieceDto piece3 = PiecesUtils.mapToDesinscriptionDto(file3);
        DesinscriptionPieceDto piece4 = PiecesUtils.mapToDesinscriptionDto(file4);

        // Création de la liste des pièces
        // Si pas de pièce versée : erreur.
        if(piece1 == null) throw new BusinessGlobalException(ErrorMessageEnum.VEUILLEZ_SELECTIONNER_UN_FICHIER_A_CHARGER.getMessage());
        pieces.add(piece1);
        if(piece2 != null) pieces.add(piece2);
        if(piece3 != null) pieces.add(piece3);
        if(piece4 != null) pieces.add(piece4);

        // Appel du service de versement des pièces complémentaires pour l'espace.
        this.desinscriptionService.verserPiecesComplementairesDesinscription(
            currentEspaceOrganisationId,
            currentUserId,
            pieces);

        return ResponseEntity.accepted().build();
    }
}
