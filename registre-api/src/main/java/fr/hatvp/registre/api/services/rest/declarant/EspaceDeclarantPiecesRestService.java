/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.declarant;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.GestionPiecesEspaceDeclarantService;
import fr.hatvp.registre.commons.dto.EspaceDeclarantPieceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.commons.utils.PiecesUtils;

/**
 * Controlleur qui permet de gérer les pièces du déclarant
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/declarant/pieces")
public class EspaceDeclarantPiecesRestService
{

	/** Service pour pour la gestion des pièces du déclarant. */
	@Autowired
	private GestionPiecesEspaceDeclarantService gestionPiecesEspaceDeclarantService;

	/**
	 * Enregistre les pièces soumises par le déclarant.
	 *
	 * @param file1
	 * @param declarantId
	 * @return
	 */
	@PostMapping("/add")
	public ResponseEntity<Void> uploadPieceDeclarant(@RequestParam final MultipartFile file1, @CurrentUserId final Long declarantId) {
		// Validation des pièces.
		PiecesUtils.checkConformitePiece(file1);

		// Transformation des fichiers en DTO
		EspaceDeclarantPieceDto piece = PiecesUtils.mapToEspaceDeclarantPieceDto(file1);

		// Si pas de pièce versée : erreur.
		if(piece == null) {
			throw new BusinessGlobalException(ErrorMessageEnum.VEUILLEZ_SELECTIONNER_UN_FICHIER_A_CHARGER.getMessage());
		}

		// Appel du service de versement des pièces complémentaires pour l'espace.
		this.gestionPiecesEspaceDeclarantService.verserPiece(
				declarantId,
				piece,
				OrigineVersementEnum.FO_DECLARANT);

		return ResponseEntity.accepted().build();
	}

	/**
	 * Récupération de la liste des pièces versées pour la création d'un espace.
	 * @param declarantId
	 * @return la liste des pièces.
	 */
	@GetMapping("/all")
	public ResponseEntity<List<EspaceDeclarantPieceDto>> getListePieces(@CurrentUserId final Long declarantId) {
		return ResponseEntity.ok(this.gestionPiecesEspaceDeclarantService.getListePiecesAllEspaceDeclarant(declarantId));
	}

	/**
	 * Téléchargement d'une pièce selon son identifiant.
	 * @param pieceId
	 * @param currentDeclarantId
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(path = "/download/{pieceId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Void> telechargerPiece(
			@PathVariable final Long pieceId, @CurrentUserId final Long currentDeclarantId,
			final HttpServletResponse response) throws IOException
	{
		EspaceDeclarantPieceDto piece = this.gestionPiecesEspaceDeclarantService.findOne(pieceId);

		if(currentDeclarantId.equals(piece.getDeclarantId())){
			response.setHeader("Content-Disposition", "attachment;filename=\""
					+ piece.getNomFichier() + "\"");
			response.getOutputStream()
			.write(piece.getContenuFichier());
			response.flushBuffer();

			return ResponseEntity.ok().build();
		}

		return ResponseEntity.status(HttpStatus.FORBIDDEN).build();

	}

}
