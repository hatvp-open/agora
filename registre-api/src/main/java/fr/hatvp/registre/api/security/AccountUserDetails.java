/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.security;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.commons.dto.DeclarantDto;

/**
 *
 * Cette classe permet d'initialiser les données de connexion.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class AccountUserDetails
    implements MyUserDetails
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -7307537566163438986L;

    /** Logger de la classe. */
    private static Logger LOGGER = LoggerFactory.getLogger(AccountUserDetails.class);

    /** Information du déclarant connecté. */
    private final DeclarantDto account;

    /** Espace organisation courrant. */
    private Long currentEspaceOrganisationId;

    /** Etat de l'espace organisation actuel. */
    private Boolean isCurrentEspaceEnabled;

    /** Constructeur de la classe. */
    public AccountUserDetails(final DeclarantDto account)
    {
        this.account = account;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        final ArrayList<GrantedAuthority> authorities = new ArrayList<>();

        if (this.account.getInscriptionEspaceFavRoles() != null) {
            this.account.getInscriptionEspaceFavRoles()
                    .forEach(i -> authorities.add(new SimpleGrantedAuthority("ROLE_" + i.name())));
        }
        authorities.forEach(grantedAuthority -> LOGGER.info("Declarant connecté avec les roles : {}",  grantedAuthority.getAuthority()));

        return authorities;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPassword()
    {
        return this.account.getPassword();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUsername()
    {
        return this.account.getEmail();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEnabled()
    {
        return this.account.isActivated();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getId()
    {
        return this.account.getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getCurrentEspaceOrganisationId()
    {
        return this.currentEspaceOrganisationId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCurrentEspaceOrganisationId(final Long currentEspaceOrganisationId)
    {
        this.currentEspaceOrganisationId = currentEspaceOrganisationId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean getCurrentEspaceEnabled()
    {
        return this.isCurrentEspaceEnabled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCurrentEspaceEnabled(final Boolean currentEspaceEnabled)
    {
        this.isCurrentEspaceEnabled = currentEspaceEnabled;
    }
}
