/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.publication;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.PublicationActiviteService;
import fr.hatvp.registre.business.PublicationExerciceService;
import fr.hatvp.registre.business.PublicationService;
import fr.hatvp.registre.commons.dto.publication.PublicationFrontDto;

/**
 * Classe de controlleur d'accès pour les publications.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/publication")
public class PublicationRestService {

	/** Service des publications. */
	@Autowired
	private PublicationService publicationService;

	@Autowired
	private PublicationExerciceService publicationExerciceService;

	@Autowired
	private PublicationActiviteService publicationActiviteService;

	/**
	 * Enregistrer une publication.
	 */
	@PostMapping
	@PreAuthorize("hasRole('ROLE_PUBLICATEUR')")
	public ResponseEntity<Void> savePublication(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId, @CurrentUserId final Long currentUserId) {
		final String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
		this.publicationService.savePublication(currentUserId, currentEspaceOrganisationId, sessionId);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	/**
	 * Récupérer la dernière publication d'un espace organisation.
	 */
	@GetMapping("/last")
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRATEUR', 'ROLE_PUBLICATEUR', 'ROLE_CONTRIBUTEUR')")
	public ResponseEntity<PublicationFrontDto> getLastPublication(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		final PublicationFrontDto publicationDto = this.publicationService.getDernierePublication(currentEspaceOrganisationId);
		return new ResponseEntity<>(publicationDto, HttpStatus.OK);
	}

	/**
	 * Récupérer la nouvelle publication d'un espace organisation. Le contenu de la publication est stocké temporairement pour être enregistré définitivement lors du save de
	 * PublicationRestService#savePublication(Long, Long)
	 * 
	 * @see PublicationRestService#savePublication(Long, Long)
	 */
	@GetMapping("/new")
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRATEUR', 'ROLE_PUBLICATEUR', 'ROLE_CONTRIBUTEUR')")
	public ResponseEntity<PublicationFrontDto> getNewPublication(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		final String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
		final PublicationFrontDto publicationDto = this.publicationService.getNouvellePublication(currentEspaceOrganisationId, sessionId);
		return new ResponseEntity<>(publicationDto, HttpStatus.OK);
	}

	/**
	 * Enregistrer une publication exercice.
	 */
	@PostMapping("/exercice/{exerciceId}")
	@PreAuthorize("hasRole('ROLE_PUBLICATEUR')")
	public ResponseEntity<Void> savePublicationExercice(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId, @CurrentUserId final Long currentUserId,
			@PathVariable final Long exerciceId) {
		this.publicationExerciceService.savePublicationExercice(currentUserId, currentEspaceOrganisationId, exerciceId);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	/**
	 * Enregistrer une publication activite.
	 */
	@PostMapping("/activite/{activiteId}")
	@PreAuthorize("hasRole('ROLE_PUBLICATEUR')")
	public ResponseEntity<Void> savePublicationActivite(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId, @CurrentUserId final Long currentUserId,
			@PathVariable final Long activiteId) {
		this.publicationActiviteService.savePublicationActivite(currentUserId, currentEspaceOrganisationId, activiteId);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}


    /**
     * Retourne 0 si aucune publi, 1 si publi identité, 2 si publi activite, 3 si désinscription effectué
     */
    @GetMapping("/hasPublished/{inscriptionEspaceId}")
    public ResponseEntity<Integer> hasPublished (@PathVariable long inscriptionEspaceId){
        Integer published = this.publicationService.hasPublished(inscriptionEspaceId);
        return ResponseEntity.ok(published);
    }

    /**
     * Retourne une pair contenant respectivement le nbre de publications d'activités et de moyens
     */
    @GetMapping("/count/{inscriptionEspaceId}")
    public ResponseEntity<Map<String, String>> getCountPublications (@PathVariable long inscriptionEspaceId){
        Map<String, String> publicationsCount = this.publicationService.getCountPublications(inscriptionEspaceId);
        return ResponseEntity.ok(publicationsCount);
    }
}
