/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;
import org.springframework.security.web.csrf.MissingCsrfTokenException;
import org.springframework.stereotype.Component;

/**
 *
 * Cette classe configure les accès interdits.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CustomAccessDenied
    implements AccessDeniedHandler
{
    /**
     * {@inheritDoc}
     */
    @Override
    public void handle(final HttpServletRequest httpServletRequest,
                       final HttpServletResponse httpServletResponse, final AccessDeniedException e)
        throws IOException,
        ServletException
    {

        if (e instanceof MissingCsrfTokenException) {
            /*
             * Handle as a session timeout (redirect, etc).
             * Even better if you inject the InvalidSessionStrategy
             * used by your SessionManagementFilter, like this:
             */
            httpServletResponse.sendError(419, "Your session has been expired");

        }
        else if (e instanceof InvalidCsrfTokenException) {
            httpServletResponse.sendError(HttpStatus.I_AM_A_TEAPOT.value(),
                "CSRF token is invalid");
        }
        else {
            /* Redirect to an error page, send HTTP 401, etc. */
            httpServletResponse.sendError(HttpStatus.FORBIDDEN.value(), "Access denied");
        }

    }
}
