/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.business.CollaborateurService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.commons.dto.CollaborateurDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;

/**
 * Controleur REST pour les collaborateurs.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/information/collaborateur")
public class CollaborateurRestService {

	/** Service Collaborateurs */
	@Autowired
	private CollaborateurService collaborateurService;

	/** Service Espace */
	@Autowired
	private EspaceOrganisationService espaceOrganisationService;

	/**
	 * Récupérer tous les collaborateurs d'un espace organisation.
	 *
	 * @param currentEspaceOrganisationId
	 *            l'identifiant de l'espace organisation courant
	 * @return liste de tous les collaborateurs pour l'espace organisation actuel
	 */
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRATEUR', 'ROLE_PUBLICATEUR', 'ROLE_CONTRIBUTEUR')")
	@GetMapping
	public ResponseEntity<List<CollaborateurDto>> getAllCollaborateurs(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		return ResponseEntity.ok(this.collaborateurService.getAllCollaborateurs(currentEspaceOrganisationId));
	}

	/**
	 * Ajouter un collaborateur additionnel.
	 *
	 * @param collaborateurDto
	 *            informations du collaborateur additionnel à rajouter
	 * @param currentEspaceOrganisationId
	 *            l'identifiant de l'espace organisation courant
	 * @return objet collaborateur créé
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@PostMapping
	public ResponseEntity<CollaborateurDto> addCollaborateur(@RequestBody @Valid final CollaborateurDto collaborateurDto,
			@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		collaborateurDto.setEspaceOrganisationId(currentEspaceOrganisationId);
		this.espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		return ResponseEntity.ok(this.collaborateurService.addCollaborateurAdditionel(collaborateurDto, currentEspaceOrganisationId));
	}

	/**
	 * Supprimer un collaborateur additionnel. Rendre un collaborateur inscrit inactif.
	 *
	 * @param id
	 *            identifiant collaborateur à supprimer
	 * @param currentEspaceOrganisationId
	 *            l'identifiant de l'espace organisation courant
	 * @return 202 accepté si collaborateur supprimé
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteCollaborateur(@PathVariable final Long id, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		this.collaborateurService.deleteCollaborateur(id, currentEspaceOrganisationId);
		this.espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		return ResponseEntity.accepted().build();
	}

	/**
	 * Modifier les données d'un collaborateur.
	 *
	 * @param collaborateurDto
	 *            données à mettre à jour.
	 * @param currentEspaceOrganisationId
	 *            l'identifiant de l'espace organisation courant
	 * @return 202 status si la mise à jour a bien été faite, sinon, des exceptions sont levées.
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@PutMapping
	public ResponseEntity<CollaborateurDto> modifierCollaborateur(@RequestBody final CollaborateurDto collaborateurDto, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		if (collaborateurDto.getId() == null || collaborateurDto.getVersion() == null) {
			throw new BusinessGlobalException(ErrorMessageEnum.MODIFICATION_COLLABORATEUR_IMPOSSIBLE.getMessage());
		}
		this.espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		return new ResponseEntity<>(this.collaborateurService.editFonctionCollaborateur(collaborateurDto, currentEspaceOrganisationId), HttpStatus.ACCEPTED);
	}

	/**
	 * Supprimer un collaborateur additionnel. Rendre un collaborateur inscrit inactif.
	 *
	 * @param id
	 *            identifiant collaborateur
	 * @param actif
	 *            si activer ou désactiver collaborateur
	 * @param currentEspaceOrganisationId
	 *            l'identifiant de l'espace organisation courant
	 * @return 202 accepté si collaborateur modifié
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@PutMapping("/activate/{id}")
	public ResponseEntity<Void> activateCollaborateur(@PathVariable final Long id, @RequestBody final boolean actif, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		this.espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		this.collaborateurService.setCollaborateurActif(id, currentEspaceOrganisationId, actif);
		return ResponseEntity.accepted().build();
	}

	/**
	 * Remonter en hierarchie un collaborateur.
	 *
	 * @param id
	 *            identifiant collaborateur
	 * @param authentication
	 *            objet authentification
	 * @return 202 accepté si collaborateur modifié
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@PutMapping("/{id}/up")
	public ResponseEntity<Void> remonterCollaborateur(@PathVariable final Long id, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		this.collaborateurService.remonterCollaborateur(id, currentEspaceOrganisationId);
		return ResponseEntity.accepted().build();
	}

	/**
	 * Faire descendre en hierarchie un collaborateur.
	 *
	 * @param id
	 *            identifiant collaborateur
	 * @param authentication
	 *            objet authentification
	 * @return 202 accepté si collaborateur modifié
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@PutMapping("/{id}/down")
	public ResponseEntity<Void> descendreCollaborateur(@PathVariable final Long id, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		this.collaborateurService.descendreCollaborateur(id, currentEspaceOrganisationId);
		return ResponseEntity.accepted().build();
	}

}
