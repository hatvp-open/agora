/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace.client;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.business.ClientAndAssociationsService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.OrganisationService;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.client.ClientSimpleDto;

/**
 * Controlleur des clients.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/information/client")
public class ClientRestService {

	/** Sevice de gestion des clients. */
	@Autowired
	private ClientAndAssociationsService clientService;

	/**
	 * Sevice de gestion des clients.
	 */
	@Autowired
	private OrganisationService organisationService;

	/** Service Espace */
	@Autowired
	private EspaceOrganisationService espaceOrganisationService;

	/**
	 * Renvoie une liste paginée des clients
	 *
	 * @param pageNumber
	 * @param resultPerPage
	 * @param currentEspaceOrganisationId
	 * @return clients for a given corporate space id paginated for a given page with a defined number of occurrences
	 */
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRATEUR', 'ROLE_PUBLICATEUR', 'ROLE_CONTRIBUTEUR')")
	@GetMapping("/type/{type}/page/{page_no}/{result_per_page}")
	public ResponseEntity<PaginatedDto<ClientSimpleDto>> getPaginatedEspaceCorporateClients(@PathVariable("type") final String type,
			@PathVariable("page_no") final Integer pageNumber,
			@PathVariable("result_per_page") final Integer resultPerPage, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {

		final PaginatedDto<ClientSimpleDto> paginatedClients = this.clientService.getPaginatedClientsByEspaceOrganisation(type, pageNumber, resultPerPage, currentEspaceOrganisationId);

		return new ResponseEntity<>(paginatedClients, HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRATEUR', 'ROLE_PUBLICATEUR', 'ROLE_CONTRIBUTEUR')")
	@GetMapping("/ancien/{clientId}")
	public ResponseEntity<Void> changeIsAncienClient(@PathVariable final Long clientId, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {

		this.clientService.changeIsAncienClient(clientId, currentEspaceOrganisationId);
		this.espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRATEUR', 'ROLE_PUBLICATEUR', 'ROLE_CONTRIBUTEUR')")
	@GetMapping("/ancien/all/{isAncienClient}")
	public ResponseEntity<Void> changeIsAncienClientAllClients(@PathVariable final Boolean isAncienClient, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {

		this.clientService.changeIsAncienClientAllClients(isAncienClient, currentEspaceOrganisationId);
		this.espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Ajout d'un client d'apartenance à l'espace collaboratif.
	 *
	 * @param organisationDto
	 *            : l'organisation à ajouter.
	 * @param currentEspaceOrganisationId
	 *            : l'identifiant de l'espace (du contexte) auquel on ajoute l'organisation.
	 * @return le client ou association enregistré.
	 */
	@PostMapping
	public ResponseEntity<AssoClientDto> addClient(@RequestBody @Valid final OrganisationDto organisationDto, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		final AssoClientDto savedClient = this.clientService.addClient(currentEspaceOrganisationId, organisationDto);
		this.espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);

		return new ResponseEntity<>(savedClient, HttpStatus.CREATED);
	}

	/**
	 * Supprimer un client.
	 *
	 * @param clientId
	 *            id du client.
	 * @param currentEspaceOrganisationId
	 * @return 200 si le client a bié été supprimé.
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@DeleteMapping("/{client_id}")
	public ResponseEntity<Void> deleteClient(@PathVariable("client_id") final Long clientId, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		this.clientService.deleteClient(clientId, currentEspaceOrganisationId);
		this.espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	
	/**
	 * Activation/désactivation d'un client
	 * @param clientSimpleDto
	 * @return
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@PostMapping(path = "/update")
	public ResponseEntity<Void> updateClient(@RequestBody @Valid final ClientSimpleDto clientSimpleDto) {
		if(Boolean.TRUE.equals(clientSimpleDto.isAncienClient())) {
			this.clientService.updatePeriodeActiviteClientRepository(clientSimpleDto);
		}else {
			this.clientService.addPeriodeActiviteClientRepository(clientSimpleDto);
		}
		return ResponseEntity.accepted().build();
	}
}
