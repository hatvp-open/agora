/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.declaration;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.business.ExerciceComptableService;
import fr.hatvp.registre.commons.dto.ListStatutPubDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableEtDeclarationDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableSimpleDto;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;

/**
 * Service Rest de gestion des exercices comptables
 *
 */
@RestController
@RequestMapping("/declaration/exercice")
public class ExerciceComptableRestService {

	@Autowired
	private ExerciceComptableService exerciceComptableService;

	/**
	 * recupère une liste des exercices comptables simplifiés.
	 *
	 * @param currentEspaceOrganisationId
	 * @return une liste des exercices comptables simplifiés
	 */
	@GetMapping("/all")
	public ResponseEntity<List<ExerciceComptableSimpleDto>> getExerciceComptableSimple(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		List<ExerciceComptableSimpleDto> exercicesComptable = this.exerciceComptableService.findAllBasicByEspaceOrganisationId(currentEspaceOrganisationId);
		return ResponseEntity.ok(exercicesComptable);
	}

	/**
	 * recupère une liste des exercices comptables avec une liste simplifiée des activités liées.
	 *
	 * @param currentEspaceOrganisationId
	 * @return une liste des exercices comptables avec une liste simplifiée des activités liées
	 */
	@GetMapping("/fiche/all")
	public ResponseEntity<List<ExerciceComptableEtDeclarationDto>> getExerciceComptableWithFiche(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		List<ExerciceComptableEtDeclarationDto> exercicesComptable = this.exerciceComptableService.findAllWithDeclarationByEspaceOrganisationId(currentEspaceOrganisationId);
		return ResponseEntity.ok(exercicesComptable);
	}

	/**
	 * met à jour les données d'un exercice comptable
	 *
	 * @param exerciceId
	 * @param exerciceComptable
	 * @param currentEspaceOrganisationId
	 * @return un exercice comptable
	 */
	@PutMapping("/{exerciceId}")
	public ResponseEntity<ExerciceComptableSimpleDto> updateExerciceComptable(@PathVariable final Long exerciceId, @RequestBody @Valid final ExerciceComptableSimpleDto exerciceComptable,
			@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		ExerciceComptableSimpleDto exerciceComptableDto = this.exerciceComptableService.updateExerciceComptable(exerciceId, exerciceComptable, currentEspaceOrganisationId);
		return ResponseEntity.accepted().body(exerciceComptableDto);
	}

	/**
	 * précise qu'aucune activité n'a été déclarée pour cette période
	 *
	 * @param exerciceId
	 * @param exerciceComptable
	 * @param currentEspaceOrganisationId
	 * @return un exercice comptable
	 */
	@PutMapping("/nulle/{exerciceId}")
	public ResponseEntity<ExerciceComptableSimpleDto> updateExerciceComptableNoActivite(@PathVariable final Long exerciceId, @RequestBody final ExerciceComptableSimpleDto exerciceComptable,
			@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		ExerciceComptableSimpleDto exerciceComptableDto = this.exerciceComptableService.updateExerciceComptableNoActivite(exerciceId, exerciceComptable, currentEspaceOrganisationId);
		return ResponseEntity.accepted().body(exerciceComptableDto);
	}

	/**
	 * Fournit un pdf récapitulatif des déclarations d'un exercice comptable
	 *
	 * @param exerciceId
	 * @param currentEspaceOrganisationId
	 * @return
	 * @throws IOException
	 */
	@PostMapping(value = "/pdf/{exerciceId}/{withMoyens}")
	public ResponseEntity<byte[]> getRapportActivitePdf(@PathVariable final Long exerciceId, @PathVariable Boolean withMoyens, @RequestBody List<StatutPublicationEnum> statuts,
			@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {

		Pair<byte[], String> pdfByteArray = this.exerciceComptableService.buildPdf(exerciceId, withMoyens, statuts, currentEspaceOrganisationId);

		HttpHeaders header = new HttpHeaders();
		header.setContentType(new MediaType("application", "pdf"));
		header.set("Content-Disposition", "attachment; filename=" + pdfByteArray.getRight() + ".pdf");
		header.setContentLength(pdfByteArray.getLeft().length);
		header.add("x-filename", pdfByteArray.getRight());

		return new ResponseEntity<>(pdfByteArray.getLeft(), header, HttpStatus.OK);
	}

	/**
	 * Retourne la liste des activités disponibles au format JSON.
	 *
	 * @return list des activités.
	 */
	@GetMapping("/statut/list")
	public ResponseEntity<ListStatutPubDto> getListeDesActiviteDeRepresentationDinteret() {
		return ResponseEntity.ok(this.exerciceComptableService.getListStatutPubDto());
	}

}
