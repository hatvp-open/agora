/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.business.DirigeantService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;

/**
 * Controleur REST pour les dirigeants.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/information/dirigeant")
public class DirigeantRestService {

	/** Service Dirigeants */
	@Autowired
	private DirigeantService dirigeantService;

	/** Service Espace */
	@Autowired
	private EspaceOrganisationService espaceOrganisationService;

	/**
	 * Récupérer tous les dirigeants d'un espace organisation.
	 *
	 * @param authentication
	 *            objet authentification
	 * @return liste de tous les dirigeants pour l'espace organisation actuel
	 */
	@PreAuthorize("hasAnyRole({'ROLE_ADMINISTRATEUR', 'ROLE_PUBLICATEUR', 'ROLE_CONTRIBUTEUR'})")
	@GetMapping
	public ResponseEntity<List<DirigeantDto>> getAllDirigeants(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		return ResponseEntity.ok(this.dirigeantService.getAllDirigeants(currentEspaceOrganisationId));
	}

	/**
	 * Ajouter un dirigeant.
	 *
	 * @param dirigeantDto
	 *            informations du dirigeant à rajouter
	 * @param authentication
	 *            objet authentification
	 * @return objet dirigeant créé
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@PostMapping
	public ResponseEntity<DirigeantDto> addDirigeant(@RequestBody @Valid final DirigeantDto dirigeantDto, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {

		dirigeantDto.setEspaceOrganisationId(currentEspaceOrganisationId);
		this.espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		return ResponseEntity.ok(this.dirigeantService.addDirigeant(dirigeantDto, currentEspaceOrganisationId));
	}

	/**
	 * Supprimer un dirigeant.
	 *
	 * @param id
	 *            identifiant dirigeant à supprimer
	 * @return 202 accepté si dirigeant supprimé
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@DeleteMapping("/{dirigeantId}")
	public ResponseEntity<Void> deleteDirigeant(@PathVariable final Long dirigeantId, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		this.dirigeantService.deleteDirigeant(dirigeantId, currentEspaceOrganisationId);
		this.espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		return ResponseEntity.accepted().build();
	}

	/**
	 * Modifier les données d'un dirigeant.
	 *
	 * @param dto
	 *            données à mettre à jour.
	 * @return 202 status si la mise à jour a bien été faite, sinon, des exceptions sont levées.
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@PutMapping
	public ResponseEntity<DirigeantDto> modifierCollaborateur(@RequestBody final DirigeantDto dto, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		if (dto.getId() == null || dto.getVersion() == null) {
			throw new BusinessGlobalException(ErrorMessageEnum.MODIFICATION_DIRIGEANT_IMPOSSIBLE.getMessage());
		}

		dto.setEspaceOrganisationId(currentEspaceOrganisationId);
		this.espaceOrganisationService.updateFlagElementsAPublier(currentEspaceOrganisationId);
		return new ResponseEntity<>(this.dirigeantService.editDirigeant(dto, currentEspaceOrganisationId), HttpStatus.ACCEPTED);
	}
	
	/**
	 * Remonter en hierarchie un dirigeant.
	 *
	 * @param id
	 *            identifiant dirigeant
	 * @param authentication
	 *            objet authentification
	 * @return 202 accepté si dirigeant modifié
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@PutMapping("/{id}/up")
	public ResponseEntity<Void> remonterDirigeant(@PathVariable final Long id, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		this.dirigeantService.remonterDirigeant(id, currentEspaceOrganisationId);
		return ResponseEntity.accepted().build();
	}

	/**
	 * Faire descendre en hierarchie un collaborateur.
	 *
	 * @param id
	 *            identifiant collaborateur
	 * @param authentication
	 *            objet authentification
	 * @return 202 accepté si collaborateur modifié
	 */
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	@PutMapping("/{id}/down")
	public ResponseEntity<Void> descendreDirigeant(@PathVariable final Long id, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		this.dirigeantService.descendreDirigeant(id, currentEspaceOrganisationId);
		return ResponseEntity.accepted().build();
	}
}
