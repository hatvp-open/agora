/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.security;

import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Cette classe permet de configurer le point d'entrée de l'application.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class EntryPointUnAuthorisedHandler
    implements AuthenticationEntryPoint {
    /**
     * {@inheritDoc}
     */
    @Override
    public void commence(final HttpServletRequest request, final HttpServletResponse response,
                         final AuthenticationException authException)
        throws IOException, ServletException {

        response.sendError(419, ErrorMessageEnum.SESSION_INTROUVABLE.getMessage());
    }
}
