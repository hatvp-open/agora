/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.OrganisationService;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.TypeOrganisationDto;

/**
 * Controleur REST pour les organisations.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate")
public class OrganisationRestService {

	/** Service Organisation. */
	@Autowired
	private OrganisationService organisationService;

	/**
	 * récupérer liste d'organisations par identifiant national
	 *
	 * @param nationalId
	 *            identifiant national
	 * @return liste des organisations
	 */
	@GetMapping("/find/nid/{nationalId}")
	public ResponseEntity<List<OrganisationDto>> getOrganisationByNationalId(@PathVariable("nationalId") final String nationalId) {
		final List<OrganisationDto> loDto = this.organisationService.lookUpByNationalNumber(nationalId);
		return new ResponseEntity<>(loDto, HttpStatus.OK);
	}

	/**
	 * retourne la liste des types d'organisation
	 *
	 * @return
	 */
	@GetMapping("/type/organisation")
	public ResponseEntity<List<TypeOrganisationDto>> getTypesOrganisations() {
		final List<TypeOrganisationDto> loDto = this.organisationService.getListeTypesOrganisations();
		return new ResponseEntity<>(loDto, HttpStatus.OK);
	}

	/**
	 * enregiste une organisation recherché pour une déclaration d'intêret
	 *
	 * @return
	 */
	@PostMapping("/organisation")
	public ResponseEntity<OrganisationDto> saveSearchResult(@RequestBody @Valid final OrganisationDto OrganisationDto) {
		final OrganisationDto dto = this.organisationService.saveSearchResult(OrganisationDto);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
}