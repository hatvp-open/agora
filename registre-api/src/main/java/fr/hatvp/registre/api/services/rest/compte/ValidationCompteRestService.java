/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.compte;

import static fr.hatvp.registre.commons.utils.ValidateUtils.validatePassword;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fr.hatvp.registre.business.DeclarantService;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.validators.ValidationMessagesConstantes;


@RestController
@RequestMapping("/account/validate")
public class ValidationCompteRestService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationCompteRestService.class);

    /**
     * Service de déclarant utilisé.
     */
    private DeclarantService declarantService;

    /**
     * Initialisation du service.
     *
     * @param declarantService service à initialisé.
     */
    @Autowired
    public void setDeclarantService(final DeclarantService declarantService)
    {
        this.declarantService = declarantService;
    }

    /**
     * Activation de la nouvelle adresse email.
     *
     * @param key clé de" validation.
     * @param declarantDto contient le mot de passe du déclarant.
     * @return le déclarant avec les informations modifiées
     */
    @PutMapping("/email/{key}")
    public ResponseEntity<DeclarantDto> validateNewAddressEmail(@PathVariable final String key,
            @RequestBody final String password)
    {
        this.controleDeSurfaceAvantValidation(key, password);

        final DeclarantDto declarantDto1 = this.declarantService
                .activationDeLaNouvelleAdresseEmail(key, password);

        return new ResponseEntity<>(declarantDto1, HttpStatus.OK);
    }

    /**
     * Valider la création du compte déclarant.
     *
     * @param key clé de validation.
     * @param declarantDto contient le mot de passe du déclarant.
     * @return le déclarant avec les informations modifiées
     */
    @PutMapping("/{key}")
    public ResponseEntity<DeclarantDto> validateAccount(@PathVariable final String key,
            @RequestBody final String password)
    {
        this.controleDeSurfaceAvantValidation(key, password);

        final DeclarantDto declarantDto1 = this.declarantService.activationDeCompte(key, password);

        return new ResponseEntity<>(declarantDto1, HttpStatus.OK);
    }

    /**
     * Controle de surface avant la validation de compte et l'email principal.
     *
     * @param key clé de" validation.
     * @param declarantDto contient le mot de passe du déclarant.
     */
    private void controleDeSurfaceAvantValidation(@PathVariable final String key,
            @RequestBody final String password)
    {
        if ((key == null) || key.isEmpty()) {
            LOGGER.error(ValidationMessagesConstantes.TOKEN_DE_VALIDATION_INTROUVABLE);
            throw new BusinessGlobalException(
                    ValidationMessagesConstantes.TOKEN_DE_VALIDATION_INTROUVABLE, 404);
        }

        if (!validatePassword(password)) {
            LOGGER.error(ValidationMessagesConstantes.MOT_DE_PASSE_INCORRECT);
            throw new BusinessGlobalException(ValidationMessagesConstantes.MOT_DE_PASSE_INCORRECT,
                    400);
        }
    }

}