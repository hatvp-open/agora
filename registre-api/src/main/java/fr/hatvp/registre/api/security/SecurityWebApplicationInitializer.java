/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.stereotype.Component;

/***
 *
 * Cette classe permet d'initialiser le filter de spring security.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class SecurityWebApplicationInitializer
    extends AbstractSecurityWebApplicationInitializer
{
}
