/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.DemandeOrganisationService;
import fr.hatvp.registre.commons.dto.DemandeOrganisationDto;

/**
 * Contrôleur REST pour les Demandes d'ajout d'une Organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/contact")
public class DemandeOrganisationRestService {

    /** Service DemandeOrganisation. */
    @Autowired
    private DemandeOrganisationService demandeOrganisationService;

    /**
     * Ajouter une demande d'ajout d'organisation.
     *
     * @param demande        dto demande d'ajout d'organisation
     * @param authentication objet authentification
     */
    @PostMapping
    public ResponseEntity<Void> saveDemande(
            @RequestBody final DemandeOrganisationDto demande,
            @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId,
            @CurrentUserId final Long currentUserId)
    {
        demande.setDeclarantId(currentUserId);
        demande.setEspaceOrganisationId(currentEspaceOrganisationId);
        
        this.demandeOrganisationService.saveDemandeOrganisation(demande);

        return ResponseEntity.accepted().build();
    }

}
