/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace.inscription;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.InscriptionEspaceService;
import fr.hatvp.registre.business.OrganisationService;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.utils.PiecesUtils;

/**
 * Controlleur de création d'un espace organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/registration")
public class CreateEspaceOrganisationRestService {
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateEspaceOrganisationRestService.class);

	/** Service des espaces organisations. */
	@Autowired
	private InscriptionEspaceService inscriptionEspaceService;

	/** Service Organisation. */
	@Autowired
	private OrganisationService organisationService;

	/**
	 * Chargement du ficher de mandat de déclaration pour un tiers, limité à 10MO
	 *
	 * @param file1
	 *            fichier de pièce d'identité à charger.
	 * @param file2
	 *            fichier de mandat à charger.
	 * @param file3
	 *            justificatif de mandat à charger.
	 * @return http status 200 si bien chargé et données enregistrées, 404: si des éléments sont introuvable dans la requête
	 */
	@PostMapping("/create")
	public ResponseEntity<InscriptionEspaceDto> handleFileUpload(@RequestParam final MultipartFile file1, @RequestParam(required = false) final MultipartFile file2,
			@RequestParam final MultipartFile file3, @RequestParam(required = false) final MultipartFile file4, @RequestParam final boolean representantLegal,
			@RequestParam final boolean association, @RequestParam final String organisation, @CurrentUserId final Long currentUserId) {

		// Validation présence pièce d'identité
		if (file1.isEmpty() || file3.isEmpty()) {
			throw new BusinessGlobalException(ErrorMessageEnum.VEUILLEZ_SELECTIONNER_UN_FICHIER_A_CHARGER.getMessage());
		}

		// Validation présence mandat
		if (!representantLegal && file2.isEmpty()) {
			throw new BusinessGlobalException(ErrorMessageEnum.VEUILLEZ_SELECTIONNER_UN_FICHIER_A_CHARGER.getMessage());
		}

		// Validation présence statut
		if (association && file4.isEmpty()) {
			throw new BusinessGlobalException(ErrorMessageEnum.VEUILLEZ_SELECTIONNER_UN_FICHIER_A_CHARGER.getMessage());
		}

		// Validation de la conformité des pièces chargées
		PiecesUtils.checkConformitePiece(file1);
		PiecesUtils.checkConformitePiece(file2);
		PiecesUtils.checkConformitePiece(file3);
		PiecesUtils.checkConformitePiece(file4);

		// Transformation des pièces récupérées
		List<EspaceOrganisationPieceDto> pieces = new ArrayList<>();
		EspaceOrganisationPieceDto piece1 = PiecesUtils.mapToDto(file1);
		EspaceOrganisationPieceDto piece2 = PiecesUtils.mapToDto(file2);
		EspaceOrganisationPieceDto piece3 = PiecesUtils.mapToDto(file3);
		EspaceOrganisationPieceDto piece4 = PiecesUtils.mapToDto(file4);
		if (piece1 != null) {
			pieces.add(piece1);
		}
		if (piece2 != null) {
			pieces.add(piece2);
		}
		if (piece3 != null) {
			pieces.add(piece3);
		}
		if (piece4 != null) {
			pieces.add(piece4);
		}

		// Validation organisation
		if (organisation == null) {
			throw new BusinessGlobalException(ErrorMessageEnum.ORGANISATION_INTROUVABLE.getMessage());
		}

		try {
			// Transformer le string objet organisation envoyer en paramètre, en un DTO organisation
			final ObjectMapper mapper = new ObjectMapper();
			OrganisationDto organisationDto;
			try {
				organisationDto = mapper.readValue(organisation, OrganisationDto.class);
			} catch (JsonMappingException | JsonParseException e) {
				LOGGER.error(ErrorMessageEnum.ORGANISATION_PARAM_INCORRECT.getMessage() + " : {}", e);
				throw new BusinessGlobalException(ErrorMessageEnum.ORGANISATION_PARAM_INCORRECT.getMessage());
			}
			organisationDto.setCreationDate(new Date());

			// Enregistrer l'organisation
			if (Boolean.FALSE.equals(organisationDto.getOrganisationExist())) {
				// On ne veut pas garder l'id et la version prise du référentiel sous peine d'avoir des collisions dans notre base organisation (organisation != référentiel)
				organisationDto.setId(null);
				organisationDto.setVersion(null);
				organisationDto = this.organisationService.saveSearchResult(organisationDto);
			}

			final InscriptionEspaceDto inscriptionEspaceDto = new InscriptionEspaceDto();
			inscriptionEspaceDto.setOrganizationId(organisationDto.getId());

			inscriptionEspaceDto.setRepresentantLegal(representantLegal);

			return ResponseEntity.ok(this.inscriptionEspaceService.inscriptionNouvelEspace(currentUserId, inscriptionEspaceDto, pieces));

		} catch (final IOException e) {
			LOGGER.error(ErrorMessageEnum.IMPOSSIBLE_DE_RECONNAITRE_LORGANISATION.getMessage() + " : {}", e);
			throw new BusinessGlobalException(ErrorMessageEnum.IMPOSSIBLE_DE_RECONNAITRE_LORGANISATION.getMessage());
		}
	}
}
