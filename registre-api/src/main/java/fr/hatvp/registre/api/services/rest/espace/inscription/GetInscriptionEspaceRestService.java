/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace.inscription;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.InscriptionEspaceService;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;

/**
 * Controleur qui gère les récupérations d'inscriptions aux espace organisations.
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/registration")
public class GetInscriptionEspaceRestService
{

    /**
     * Service de gestion des inscriptions.
     */
    @Autowired
    private InscriptionEspaceService inscriptionEspaceService;

    /**
     * Récupérer liste de toutes les inscriptions valides (non refusées) du déclarant connecté
     *
     * @param authentication
     *         objet d'authentification Spring security
     * @return liste des inscriptions non supprimées du déclarant
     */
    @GetMapping
    public ResponseEntity<List<InscriptionEspaceDto>> getAllRegistrations(@CurrentUserId final Long currentUserId)
    {
    	//On recupere la liste des InscriptionEspaceDto
    	List<InscriptionEspaceDto> listeInscriptionEspaceDto = this.inscriptionEspaceService.getAllByDeclarantId(currentUserId);
    	//On parcours la liste afin de retirer les informations sensibles avant envoi de la reponse
    	for (InscriptionEspaceDto dto :listeInscriptionEspaceDto) {
    		dto.setAdministrateurs(null);
    	}
    	 final List<InscriptionEspaceDto> res = listeInscriptionEspaceDto.stream().sorted(Comparator.comparing(InscriptionEspaceDto::getDateDemande))
    								.collect(Collectors.toList());
       
        return ResponseEntity.ok(res);
    }

    /**
     * GESTION DES DEMANDES EN ATTENTES.
     * L'ensemble des demandes en attente pour un espace organisation.
     *
     * @param currentEspaceOrganisationId
     *         identifiant espace organisation en session
     * @return liste des demandes d'inscription en attente.
     */
    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    @GetMapping("/pending")
    public ResponseEntity<List<InscriptionEspaceDto>> getPendingRequests(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId)
    {
        final List<InscriptionEspaceDto> res = this.inscriptionEspaceService.getAllPendingRequestsByEspaceOrganisationId(currentEspaceOrganisationId);
        return ResponseEntity.ok(res);
    }


    /**
     * GESTION DES DECLARANTS.
     * L'ensemble des inscriptions valides pour un espace organisation.
     *
     * @param currentEspaceOrganisationId  identifiant espace organisation en session.
     * @return
     */
    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    @GetMapping("/inscriptions")
    public ResponseEntity<List<InscriptionEspaceDto>> getValidRequests(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId)
    {
        final List<InscriptionEspaceDto> res = this.inscriptionEspaceService.getAllResgitrationsByEspaceOrganisationId(currentEspaceOrganisationId);
        return ResponseEntity.ok(res);
    }

    /**
     * GESTION DES DECLARANTS.
     * L'ensemble des inscriptions valides ayant un rôle Administrateur (Contact Opértationnel) pour un espace organisation.
     *
     * @param currentEspaceOrganisationId  identifiant espace organisation en session.
     * @return
     */
    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    @GetMapping("/admin")
    public ResponseEntity<List<InscriptionEspaceDto>> getAdminRegistrations(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId)
    {
        final List<InscriptionEspaceDto> res = this.inscriptionEspaceService.getAllAdminRegistrationsByEspaceOrganisationId(currentEspaceOrganisationId);
        return ResponseEntity.ok(res);
    }

    /**
     * GESTION DES DECLARANTS.
     * L'ensemble des inscriptions valides ayant le rôle Administrateur (Contact Opértationnel) pour un espace organisation.
     *
     * @param currentEspaceOrganisationId  identifiant espace organisation en session.
     * @return
     */
    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    @GetMapping("/nonadmin")
    public ResponseEntity<List<InscriptionEspaceDto>> getNonAdminRegistrations(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId)
    {
        final List<InscriptionEspaceDto> res = this.inscriptionEspaceService.getAllNonAdminRegistrationsByEspaceOrganisationId(currentEspaceOrganisationId);
        return ResponseEntity.ok(res);
    }

}
