/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.DeclarantService;
import fr.hatvp.registre.commons.dto.DeclarantDto;

/**
 *
 * Cette classe permet de configurer la logique d'authentification.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class UserDetailServiceImpl
    implements UserDetailsService
{

    /** LOGGER de la classe. */
    private static Logger LOGGER = LoggerFactory.getLogger(UserDetailServiceImpl.class);

    /** Service des déclarants. */
    private DeclarantService declarantService;

    /** Initialisation du service. */
    @Autowired
    public void setDeclarantService(final DeclarantService declarantService)
    {
        this.declarantService = declarantService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserDetails loadUserByUsername(final String username)
    {

        final DeclarantDto account = this.declarantService.loadDeclarantByEmail(username, true);

        if (account == null) {
            LOGGER.info("L'email saisi ( {} ) est introuvable:)", username);
            throw new UsernameNotFoundException("Utilisateur introuvable: " + username);
        }

        LOGGER.info("Tentative de connexion avec l'email: {}" , username);
        return new AccountUserDetails(account);
    }

}
