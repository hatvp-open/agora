/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace.inscription;

import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.AccountUserDetails;
import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.InscriptionEspaceService;
import fr.hatvp.registre.commons.dto.DeclarantRolesDto;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;

/**
 * Controlleur d'inscription à l'espace organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/registration")
public class UpdateInscriptionEspaceRestService {
    private static final Logger LOGGER = LoggerFactory
        .getLogger(UpdateInscriptionEspaceRestService.class);

    /**
     * Service des inscription espaces.
     */
    @Autowired
    private InscriptionEspaceService inscriptionEspaceService;

    /**
     * Récupérer l'inscription à un espace organisation par son id.
     * Charger les nouveaux roles de cet espace dans le contexte.
     *
     * @param accountUserDetails objet d'authentification Spring security.
     * @param registrationId     id de l'inscription.
     * @return 200 OK
     */
    @PutMapping("/current/{registrationId}")
    public ResponseEntity<Void> setCurrentEspaceOrganisation(@PathVariable final Long registrationId, @AuthenticationPrincipal AccountUserDetails accountUserDetails) {

        final InscriptionEspaceDto inscriptionEspaceDto = this.inscriptionEspaceService.getRegistrationByIdAndDeclarantId(registrationId, accountUserDetails.getId());

        // Enregistrer l'etat de l'espace organisation courant ds spring security.
        accountUserDetails.setCurrentEspaceEnabled(inscriptionEspaceDto.getVerrouille());
        accountUserDetails.setCurrentEspaceOrganisationId(inscriptionEspaceDto.getEspaceOrganisationId());

        this.inscriptionEspaceService.changeRolesInContext(inscriptionEspaceDto.getRoles(), inscriptionEspaceDto.getVerrouille());
        return ResponseEntity.ok().build();
    }

    /**
     * Modifier l'espace organisation favori du déclarant connecté.
     *
     * @param id            identifiant inscription espace.
     * @param currentUserId de l'utilisateur injecté par Spring Security.
     * @return 201 accepté.
     */
    @PutMapping("/favorite/{id}")
    public ResponseEntity<Void> setFavorite(@PathVariable final Long id, @CurrentUserId Long currentUserId) {

        this.inscriptionEspaceService.setFavoriteByDeclarantId(id, currentUserId);
        LOGGER.info("New favorite {} for Declarant {}", id, currentUserId);

        return ResponseEntity.accepted().build();
    }

    /**
     * Valider une demande en attente.
     *
     * @param inscriptionId               identifiant inscription espace.
     * @param currentEspaceOrganisationId identifiant de l'espace courant.
     */
    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    @PutMapping("/validate/{inscriptionId}")
    public ResponseEntity<Void> setValidRequest(
        @PathVariable final Long inscriptionId,
        @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
        this.inscriptionEspaceService.setPendingRequestValidity(inscriptionId, currentEspaceOrganisationId, true, true);
        return ResponseEntity.accepted().build();
    }

    /**
     * Rejeter une demande en attente.
     *
     * @param inscriptionId               identifiant inscription espace.
     * @param currentEspaceOrganisationId identifiant de l'espace courant.
     */
    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    @PutMapping("/reject/{inscriptionId}")
    public ResponseEntity<Void> setRejectRequest(
        @PathVariable final Long inscriptionId,
        @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
        this.inscriptionEspaceService.setPendingRequestValidity(inscriptionId, currentEspaceOrganisationId, false, true);
        return ResponseEntity.accepted().build();
    }

    /**
     * Mettre à jour les rôles et verrou d'une inscription.
     *
     * @param inscriptionId               identifiant inscription espace.
     * @param declarantRolesDto
     * @param currentEspaceOrganisationId
     * @return 201 accepté.
     */
    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    @PutMapping("/access/{inscriptionId}")
    public ResponseEntity<Void> setAccessRoles(
        @PathVariable final Long inscriptionId,
        @RequestBody final DeclarantRolesDto declarantRolesDto,
        @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
        final Set<RoleEnumFrontOffice> roles = new TreeSet<>();
        if (Boolean.TRUE.equals(declarantRolesDto.isAdministrator())) {
            roles.add(RoleEnumFrontOffice.ADMINISTRATEUR);
        }
        if (Boolean.TRUE.equals(declarantRolesDto.isPublisher())) {
            roles.add(RoleEnumFrontOffice.PUBLICATEUR);
        }
        if (Boolean.TRUE.equals(declarantRolesDto.isContributor())) {
            roles.add(RoleEnumFrontOffice.CONTRIBUTEUR);
        }

        this.inscriptionEspaceService.setDeclarantRoles(inscriptionId, currentEspaceOrganisationId, roles, declarantRolesDto.isLocked());

        return ResponseEntity.accepted().build();
    }
}
