/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.commons;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.utils.SessionsUtil;

/**
 * Cette classe permet de connaître le numéro de version de l'application.
 * On l'utilise pour permettre à l'aplication front d'initialiser les token (session et xsrf).
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/sessions")
public class SessionsRestService
{

	@Autowired
	SessionsUtil sessionsUtil;

	@GetMapping
	public ResponseEntity<String> checkSessionsNumber()
	{
		return ResponseEntity.ok(sessionsUtil.getCountSessions().toString() );
	}
}