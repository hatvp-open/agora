/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.exceptions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fr.hatvp.registre.commons.dto.erreur.ErrorResourceDto;
import fr.hatvp.registre.commons.dto.erreur.FieldErrorResourceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.exceptions.GlobalServerException;
import fr.hatvp.registre.commons.exceptions.InvalidRequestException;



@ControllerAdvice
public class RegistreExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(RegistreExceptionHandler.class);

	/**
	 * Handler des {@link InvalidRequestException}
	 */
	@ExceptionHandler({ InvalidRequestException.class })
	protected ResponseEntity<Object> handleInvalidRequest(final RuntimeException e, final WebRequest request) {
		final InvalidRequestException ire = (InvalidRequestException) e;
		final List<FieldErrorResourceDto> fieldErrorResources = new ArrayList<>();

		final List<FieldError> fieldErrors = ire.getErrors().getFieldErrors();
		for (final FieldError fieldError : fieldErrors) {
			final FieldErrorResourceDto fieldErrorResource = new FieldErrorResourceDto();
			fieldErrorResource.setResource(fieldError.getObjectName());
			fieldErrorResource.setField(fieldError.getField());
			fieldErrorResource.setCode(fieldError.getCode());
			fieldErrorResource.setMessage(fieldError.getDefaultMessage());
			fieldErrorResources.add(fieldErrorResource);
		}

		final ErrorResourceDto error = new ErrorResourceDto("Invalid Request", ire.getMessage());
		error.setFieldErrors(fieldErrorResources);

		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		LOGGER.error(e.getMessage(), e);
		return this.handleExceptionInternal(e, error, headers, HttpStatus.UNPROCESSABLE_ENTITY, request);
	}

	/**
	 * Handler des {@link BusinessGlobalException}
	 */
	@ExceptionHandler({ BusinessGlobalException.class })
	protected ResponseEntity<Object> handleglobalException(final RuntimeException e, final WebRequest request) {

		final BusinessGlobalException ire = (BusinessGlobalException) e;

		final ErrorResourceDto error = new ErrorResourceDto("Invalid Request", e.getMessage());

		final List<FieldErrorResourceDto> fieldErrorResources = new ArrayList<>();
		fieldErrorResources.add(ire.getFieldErrorResourceList());

		error.setFieldErrors(fieldErrorResources);

		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		LOGGER.warn(e.getMessage(), e);
		return this.handleExceptionInternal(e, error, headers, HttpStatus.BAD_REQUEST, request);
	}

	/**
	 * Handler des {@link GlobalServerException}
	 */
	@ExceptionHandler(GlobalServerException.class)
	public ResponseEntity<Object> handleServersException(final RuntimeException e, final WebRequest request) {

		final GlobalServerException ire = (GlobalServerException) e;

		final ErrorResourceDto error = new ErrorResourceDto("Internal Server", ire.getMessage());

		final List<FieldErrorResourceDto> fieldErrorResources = new ArrayList<>();
		fieldErrorResources.add(ire.getFieldErrorResourceList());

		error.setFieldErrors(fieldErrorResources);

		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		final HttpStatus httpStatus;
		
		if(ire.getCodeError() == 503) {
			httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
		}else {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		
		LOGGER.error(e.getMessage(), e);
		return this.handleExceptionInternal(e, error, headers, httpStatus, request);
	}

	/**
	 * Excpetion handler for Optimistic Lock
	 *
	 * @param e
	 *            Exception lancée
	 * @param request
	 *            Requête d'origine
	 * @return l'erreur avec texte expliquatif
	 */
	@ExceptionHandler(ObjectOptimisticLockingFailureException.class)
	public ResponseEntity<Object> handleOptimisticLockException(final ObjectOptimisticLockingFailureException e, final WebRequest request) {
		LOGGER.error("Optimistic lock: " + e.getRootCause().getMessage(), e);

		final ErrorResourceDto error = new ErrorResourceDto("Optimistic lock", ExceptionUtils.getRootCauseMessage(e));

		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return this.handleExceptionInternal(e, error, headers, HttpStatus.PRECONDITION_FAILED, request);
	}

	/**
	 * Cette méthode permet de gérer les exceptions d'unicité {@link EntityExistsException}.
	 *
	 * @return http status 409.
	 */
	@ExceptionHandler(EntityExistsException.class)
	public ResponseEntity<ErrorResourceDto> notUniqueException(final EntityExistsException e) {
		LOGGER.error(e.getMessage(), e);
		final ErrorResourceDto error = new ErrorResourceDto("Ressource existe déjà", e.getMessage());
		return new ResponseEntity<>(error, HttpStatus.CONFLICT);
	}

	/**
	 * Cette méthode permet de gérer les exceptions de non existence{@link EntityNotFoundException}.
	 *
	 * @return http status 409.
	 */
	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<ErrorResourceDto> notFoundException(final EntityNotFoundException e) {
		LOGGER.error(e.getMessage(), e);
		final ErrorResourceDto error = new ErrorResourceDto("Ressource introuvable", e.getMessage());
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	/**
	 * Méthode Générique pour tous les {@link RuntimeException} non traitées.
	 *
	 * @return http status 400.
	 */
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ErrorResourceDto> allRuntimeExceptions(final RuntimeException e) {
		LOGGER.error(e.getMessage(), e);
		final ErrorResourceDto error = new ErrorResourceDto("Erreur serveur", e.getMessage());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Méthode Générique pour tous les {@link AccessDeniedException} non traitées.
	 *
	 * @return http status 403.
	 */
	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<ErrorResourceDto> accessDenied(final RuntimeException e) {
		LOGGER.error(e.getMessage(), e);
		final ErrorResourceDto error = new ErrorResourceDto("Accès refusé", e.getMessage());
		return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
	}

	/**
	 * Méthode de traitement des erreurs de validations.
	 *
	 * @return http status 400.
	 */
	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		LOGGER.error(ex.getMessage(), ex);
		List<FieldErrorResourceDto> fieldErrors = ex.getBindingResult().getFieldErrors().stream().map(error -> {
			return new FieldErrorResourceDto(error.getObjectName(), error.getField(), error.getCode(), error.getDefaultMessage());
		}).collect(Collectors.toList());
		// TODO modifier le message de ErrorResourceDto: deuxième paramètre
		return ResponseEntity.badRequest().body(new ErrorResourceDto("Erreur de validation", "Erreur de validation", fieldErrors));
	}

	/**
	 * Méthode de traitement des erreurs de json invalide lors d'une requête.
	 *
	 * @return http status 400.
	 */
	@Override
	public ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		LOGGER.error(ex.getMessage(), ex);
		return ResponseEntity.badRequest().body(new ErrorResourceDto("Erreur: requête HTTP mal formulé", ex.getMessage()));
	}

}
