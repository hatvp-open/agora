/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.declaration;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.QualificationObjetService;
import fr.hatvp.registre.commons.dto.activite.QualificationObjetDto;

/**
 * Controlleur Rest qui permet de gé
 * rer les déclarations d'activitée.
 *
 */
@RestController
@RequestMapping("/declaration/qualif/")
public class QualificationObjetRestService
{

	@Autowired
	private QualificationObjetService qualificationObjetService;


	
	/**
	 * Enregistrer une publication activite.
	 */
	@PostMapping("/getQualif")
	public ResponseEntity<QualificationObjetDto> qualifObjetActivite(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId, @CurrentUserId final Long currentUserId,
			@RequestBody @Valid final QualificationObjetDto qualificationObjetDto) {
		return new ResponseEntity<>(this.qualificationObjetService.qualifObjetActivite(currentEspaceOrganisationId, qualificationObjetDto),HttpStatus.OK);
	}

	/**
	 * Enregistrer une publication activite.
	 */
	@PostMapping("/updateQualif/{idFiche}")
	public ResponseEntity<QualificationObjetDto> qualifObjetActivite(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId, @CurrentUserId final Long currentUserId,
			@RequestBody @Valid final Long qualificationObjetId, @PathVariable final String idFiche) {
		return new ResponseEntity<>(this.qualificationObjetService.updateQualificationObjet(qualificationObjetId, idFiche, currentEspaceOrganisationId),HttpStatus.OK);
	}
}