/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace.inscription;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.hatvp.registre.api.security.CurrentEspaceOrganisationId;
import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.DemandeChangementMandantService;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.DemandeChangementMandantDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ChangementContactEnum;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.utils.PiecesUtils;

/**
 * Controlleur d'inscription à l'espace organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/mandant")
public class GestionMandantsRestService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GestionMandantsRestService.class);

	/**
	 * Service des inscriptions à l'espace organisation.
	 */
	@Autowired
	private DemandeChangementMandantService demandeChangementMandantService;

	/**
	 * Historiques de mes demandes côté Front (besoin non spécifié, mais peu servir si une logique de traitement émerge => utile pour les tests sinon). L'ensemble des demandes de
	 * changement de mandant en cours
	 *
	 * @param currentEspaceOrganisationId
	 *            identifiant espace organisation en session.
	 * @return la liste de demandes
	 */
	@PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
	@GetMapping("/demandes")
	public ResponseEntity<List<DemandeChangementMandantDto>> getAllRequests(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		final List<DemandeChangementMandantDto> res = this.demandeChangementMandantService.getAllDemandesChangementByEspaceOrganisation(currentEspaceOrganisationId);

		return ResponseEntity.ok(res);
	}

	/**
	 * Détail d'une demande
	 *
	 * @param demandeId
	 *            identifiant de la demande à charger.
	 * @return la liste de demandes
	 */
	@PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
	@PostMapping("/demandes/{demandeId}")
	public ResponseEntity<DemandeChangementMandantDto> getRequest(@PathVariable final Long demandeId) {
		final DemandeChangementMandantDto res = this.demandeChangementMandantService.getDemandeChangementMandant(demandeId);
		return ResponseEntity.ok(res);
	}

	/**
	 * Création d'une nouvelle demande
	 *
	 * @param currentEspaceOrganisationId
	 *            identifiant espace organisation en session.
	 * @param identite
	 *            fichier de pièce d'identité à charger.
	 * @param mandat
	 *            fichier de mandat à charger.
	 * @param demandeChangementMandant
	 *            la demande de changement, au format JSON minifié dans une chaine String
	 * @param currentUserId
	 *            le declarant auteur de la demande
	 * @return http status 200 si bien chargé et données enregistrées, 404: si des éléments sont introuvable dans la requête
	 */
	@PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
	@PostMapping("/demande")
	public ResponseEntity<Void> addRequest(@CurrentEspaceOrganisationId final Long currentEspaceOrganisationId, @RequestParam(required = false) final MultipartFile identite,
			@RequestParam final MultipartFile mandat, @RequestParam final String demandeChangementMandant, @CurrentUserId final Long currentUserId) {

		// Transformer le string objet demandeChangementMandant envoyé en paramètre, en un DTO demandeChangementMandant
		final ObjectMapper mapper = new ObjectMapper();
		DemandeChangementMandantDto demandeChangementMandantDto;
		try {
			demandeChangementMandantDto = mapper.readValue(demandeChangementMandant, DemandeChangementMandantDto.class);
		} catch (IOException e) {
			LOGGER.error(ErrorMessageEnum.DEMANDE_CHANGEMENT_MANDANT_PARAM_INCORRECT.getMessage() + " : {}", e);
			throw new BusinessGlobalException(ErrorMessageEnum.DEMANDE_CHANGEMENT_MANDANT_PARAM_INCORRECT.getMessage());
		}
		demandeChangementMandantDto.setDateDemande(new Date());
		DeclarantDto declarant = new DeclarantDto();
		declarant.setId(currentUserId);
		demandeChangementMandantDto.setDeclarantDemande(declarant);

		// Validation présence pièce d'identité
		if (demandeChangementMandantDto.getTypeDemande().equals(ChangementContactEnum.CHANGEMENT_REPRESENTANT) && identite.isEmpty()) {
			throw new BusinessGlobalException(ErrorMessageEnum.VEUILLEZ_SELECTIONNER_UN_FICHIER_A_CHARGER.getMessage());
		}

		// Validation présence mandat
		if (mandat.isEmpty()) {
			throw new BusinessGlobalException(ErrorMessageEnum.VEUILLEZ_SELECTIONNER_UN_FICHIER_A_CHARGER.getMessage());
		}

		// Validation de la conformité des pièces chargées
		PiecesUtils.checkConformitePiece(identite);
		PiecesUtils.checkConformitePiece(mandat);

		// Transformation des pièces récupérées
		EspaceOrganisationPieceDto piece1 = PiecesUtils.mapToDto(identite);
		EspaceOrganisationPieceDto piece2 = PiecesUtils.mapToDto(mandat);

		demandeChangementMandantDto.setEspaceOrganisationId(currentEspaceOrganisationId);
		this.demandeChangementMandantService.saveDemandeChangementMandant(demandeChangementMandantDto, piece1, piece2);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	/**
	 * Enregistre les pièces complémentaires soumises par le déclarant. La première pièce est exigées les autre sont facultatives.
	 */
	@PostMapping("/complement")
	@PreAuthorize("hasRole('ROLE_CONTRIBUTEUR')")
	public ResponseEntity<Void> uploadPicesComplementaires(@RequestParam final MultipartFile file1, @RequestParam(required = false) final MultipartFile file2,
			@RequestParam(required = false) final MultipartFile file3, @RequestParam(required = false) final MultipartFile file4,
			@RequestParam(required = true) final Long demandeId, @CurrentUserId final Long currentUserId, @CurrentEspaceOrganisationId final Long currentEspaceOrganisationId) {
		// Validation des pièces.
		PiecesUtils.checkConformitePiece(file1);
		PiecesUtils.checkConformitePiece(file2);
		PiecesUtils.checkConformitePiece(file3);
		PiecesUtils.checkConformitePiece(file4);

		// Transformation des fichiers en DTO
		List<EspaceOrganisationPieceDto> pieces = new ArrayList<>();
		EspaceOrganisationPieceDto piece1 = PiecesUtils.mapToDto(file1);
		EspaceOrganisationPieceDto piece2 = PiecesUtils.mapToDto(file2);
		EspaceOrganisationPieceDto piece3 = PiecesUtils.mapToDto(file3);
		EspaceOrganisationPieceDto piece4 = PiecesUtils.mapToDto(file4);

		// Création de la liste des pièces
		// Si pas de pièce versée : erreur.
		if (piece1 == null) {
			throw new BusinessGlobalException(ErrorMessageEnum.VEUILLEZ_SELECTIONNER_UN_FICHIER_A_CHARGER.getMessage());
		}

		pieces.add(piece1);

		if (piece2 != null) {
			pieces.add(piece2);
		}

		if (piece3 != null) {
			pieces.add(piece3);
		}

		if (piece4 != null) {
			pieces.add(piece4);
		}

		// Appel du service de versement des pièces complémentaires pour l'espace.
		this.demandeChangementMandantService.verserPiecesComplementaires(currentEspaceOrganisationId, currentUserId, pieces, demandeId);

		return ResponseEntity.accepted().build();
	}
}
