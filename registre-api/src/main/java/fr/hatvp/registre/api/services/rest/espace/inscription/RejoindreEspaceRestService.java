/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.espace.inscription;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.api.security.CurrentUserId;
import fr.hatvp.registre.business.InscriptionEspaceService;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;

/**
 * Controlleur d'inscription à l'espace organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/registration")
public class RejoindreEspaceRestService {

    /** Service des inscriptions à l'espace organisation. */
    @Autowired
    private InscriptionEspaceService inscriptionEspaceService;

    /**
     * Rejoindre un espace organisation s'il existe.
     *
     * @param organisationDto inscription espace DTO.
     * @param authentication  objet authentification.
     * @return http status 201 si l'espace est créé.
     */
    @PostMapping
    public ResponseEntity<Void> joinSpace(
        @RequestBody final OrganisationDto organisationDto,
        @CurrentUserId final Long currentUserId)
    {
        if (organisationDto.getId() == null) {
            throw new BusinessGlobalException(ErrorMessageEnum.ORGANISATION_INTROUVABLE.getMessage());
        }

        final InscriptionEspaceDto inscriptionEspaceDto = new InscriptionEspaceDto();
        inscriptionEspaceDto.setOrganizationId(organisationDto.getId());
        this.inscriptionEspaceService.inscriptionEspaceExistant(currentUserId, inscriptionEspaceDto);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}
