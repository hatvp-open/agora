/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.hatvp.registre.business.DeclarantService;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Cette classe configure le succès d'authentification.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class AuthSuccess
    implements AuthenticationSuccessHandler {

    /** Service des déclarants. */
    private DeclarantService declarantService;

    /** Initialisation du service. */
    @Autowired
    public void setDeclarantService(final DeclarantService declarantService) {
        this.declarantService = declarantService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request,
                                        final HttpServletResponse response, final Authentication authentication)
        throws IOException, ServletException {

        final AccountUserDetails connectedDeclarant = (AccountUserDetails) authentication
            .getPrincipal();
        final Long connected_declarant_id = connectedDeclarant.getId();

        final DeclarantDto declarantDto = this.declarantService.findOne(connected_declarant_id);

        this.declarantService.updateLastConnexionDate(declarantDto.getId());

        final ObjectMapper mapper = new ObjectMapper();
        declarantDto.setPassword(null);
        response.getWriter().write(mapper.writeValueAsString(declarantDto));

        response.setStatus(HttpServletResponse.SC_OK);
    }
}
