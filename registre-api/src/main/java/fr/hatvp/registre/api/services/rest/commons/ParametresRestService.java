/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.commons;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.backoffice.ParametresService;
import fr.hatvp.registre.commons.dto.backoffice.ParametresDto;

/**
 * Cette classe permet de modifier les parametres du mode
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/limitation")
public class ParametresRestService
{
	@Autowired ParametresService parametresService;

	/**
	 * etat actuel du mode
	 *
	 * @return ParametresDto
	 */
	@GetMapping("/activation/state")
	@PreAuthorize("permitAll()")
	public ResponseEntity<ParametresDto> getState() {
		ParametresDto params = parametresService.getState();
		return ResponseEntity.ok(params);
	}
}