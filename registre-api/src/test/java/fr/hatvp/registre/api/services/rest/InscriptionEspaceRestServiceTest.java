/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest;

import fr.hatvp.registre.api.TestApiConfig;
import fr.hatvp.registre.api.services.rest.espace.inscription.UpdateInscriptionEspaceRestService;
import fr.hatvp.registre.commons.dto.DeclarantRolesDto;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.Assert.fail;

/**
 * Test du service Rest {@link UpdateInscriptionEspaceRestService}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestApiConfig.class}, loader = AnnotationConfigContextLoader.class)
public class InscriptionEspaceRestServiceTest
{

    @InjectMocks
    private UpdateInscriptionEspaceRestService inscriptionEspaceRestService;

    /**
     * Initialisation des tests.
     *
     * @throws Exception
     *         erreur
     */
    @Before
    public void setUp() throws Exception
    {
        // Process mock annotations
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test method for
     * {@link fr.hatvp.registre.api.services.rest.espace.inscription.GetInscriptionEspaceRestService#getAllRegistrations(org.springframework.security.core.Authentication)}.
     */
    @Test
    @Ignore
    public final void testGetAllRegistrations()
    {
        fail("Not yet implemented"); // TODO
    }

    /**
     * Test method for {@link UpdateInscriptionEspaceRestService#setFavorite(java.lang.Long,
     * org.springframework.security.core.Authentication)}.
     */
    @Test
    @Ignore
    public final void testSetFavorite()
    {
        fail("Not yet implemented"); // TODO
    }

    /**
     * Test method for {@link fr.hatvp.registre.api.services.rest.espace.inscription
     * .GetInscriptionEspaceRestService#getPendingRequests(Long,
     * Authentication)}.
     */
    @Test
    @Ignore
    public final void testGetPendingRequests()
    {
        fail("Not yet implemented"); // TODO
    }

    /**
     * Test method for
     * {@link UpdateInscriptionEspaceRestService#setValidRequest(java.lang.Long)}.
     */
    @Test
    @Ignore
    public final void testSetValidRequest()
    {
        fail("Not yet implemented"); // TODO
    }

    /**
     * Test method for
     * {@link UpdateInscriptionEspaceRestService#setRejectRequest(java.lang.Long)}.
     */
    @Test
    @Ignore
    public final void testSetRejectRequest()
    {
        fail("Not yet implemented"); // TODO
    }

    /**
     * Test method for
     * {@link UpdateInscriptionEspaceRestService#setAccessRoles(Long, DeclarantRolesDto)}.
     */
    @Test
    @Ignore
    public final void testSetAccessRoles()
    {
        fail("Not yet implemented"); // TODO
    }

}
