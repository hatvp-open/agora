/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.api.services.rest.commons;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.api.TestApiConfig;

/**
 * Test du service REST [{@link VersionRestService}.
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestApiConfig.class}, loader = AnnotationConfigContextLoader.class)
public class VersionRestServiceTest
{

    @InjectMocks
    private VersionRestService versionRestService;

    /**
     * Initialisation des tests.
     * @throws Exception erreur
     */
    @Before
    public void setUp() throws Exception
    {
        // Process mock annotations
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test de la méthode loadDeclarantById
     */
    @Test
    public void testInitSession()
    {
        final ResponseEntity<String> responseEntity = this.versionRestService.initSession();
        Assert.assertNotNull(responseEntity);
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
}
