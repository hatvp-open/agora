/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.hatvp.registre.api.services.rest.AccountRestService;
import fr.hatvp.registre.business.DeclarantService;
import fr.hatvp.registre.commons.dto.request.DeclarantDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApiTestConfig.class}, loader = AnnotationConfigContextLoader.class)
public class TestAccountRestService {

    @Mock
    private DeclarantService declarantService;

    @InjectMocks
    private AccountRestService accountRestService;

    private MockMvc mockMvc;

    @Before
    public void setup() {

        // Process mock annotations
        MockitoAnnotations.initMocks(this);

        // Setup Spring test in standalone mode
        this.mockMvc = MockMvcBuilders.standaloneSetup(accountRestService).build();

    }

    @Test
    public void testValidateEmailService() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String key = UUID.randomUUID().toString();
        String password = "blabla123456a";
        String email = "Email exist";

        DeclarantDto declarantDto = new DeclarantDto();
        declarantDto.setEmail(email);
        declarantDto.setPassword(password);

        when(declarantService.updateNewEmailAddress(key, password)).thenReturn(declarantDto);

        mockMvc.perform(put("/account/validate/email/" + key, declarantDto)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(declarantDto)))
                .andExpect(status().isOk());

        verify(declarantService, times(1)).updateNewEmailAddress(key, password);
        verifyNoMoreInteractions(declarantService);
    }

    @Test
    public void recoverAccountPassword() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String key = UUID.randomUUID().toString();

        DeclarantDto declarantDto = new DeclarantDto();
        declarantDto.setPassword("blabla123456a");
        declarantDto.setKeyCode(key);

        when(declarantService.updateDeclarant(declarantDto, null)).thenReturn(declarantDto);

        mockMvc.perform(put("/account/recover/password/" + key, declarantDto)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(declarantDto)))
                .andExpect(status().isOk());

    }

}
