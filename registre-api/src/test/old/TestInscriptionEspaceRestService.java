

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.test;

import fr.hatvp.registre.api.services.rest.DeclarantRestService;
import fr.hatvp.registre.api.services.rest.InscriptionEspaceRestService;
import fr.hatvp.registre.business.InscriptionEspaceService;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/** 
* InscriptionEspaceRestService Tester. 
* 
*/
@RunWith(SpringJUnit4ClassRunner.class)
public class TestInscriptionEspaceRestService {

    @Mock
    private InscriptionEspaceService inscriptionEspaceService;

    @InjectMocks
    private InscriptionEspaceRestService inscriptionEspaceRestService;

    private MockMvc mockMvc;

    @Before
    public void before() throws Exception {
        // Process mock annotations
        MockitoAnnotations.initMocks(this);

        // Setup Spring test in standalone mode
        this.mockMvc = MockMvcBuilders.standaloneSetup(inscriptionEspaceRestService).build();

        mockMvc.perform(post("/login"));
    }

    /**
    *
    * Method: getAllRegistrations(Authentication authentication)
    *
    */
    @Test
    public void testGetAllRegistrations() throws Exception {
        mockMvc.perform(get("/corporate/registration"))
                .andExpect(status().isOk());
    }

    /**
    *
    * Method: setFavorite(@PathVariable Long id, Authentication authentication)
    *
    */
    @Test
    public void testSetFavoriteSuccess() throws Exception {
        mockMvc.perform(put("/corporate/registration/favorite/1"))
                .andExpect(status().isAccepted());
    }


    /**
     *
     * Method: setFavorite(@PathVariable Long id, Authentication authentication)
     *
     */
    @Test
    public void testSetFavorite404() throws Exception {
        mockMvc.perform(put("/corporate/registration/favorite/10000"))
                .andExpect(status().isNotFound());
    }

    /**
     *
     * Method: setFavorite(@PathVariable Long id, Authentication authentication)
     *
     */
    @Test
    public void testSetFavorite403() throws Exception {
        mockMvc.perform(put("/corporate/registration/favorite/3"))
                .andExpect(status().isForbidden());
    }


} 
