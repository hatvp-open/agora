/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.test;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import fr.hatvp.registre.api.services.rest.DeclarantRestService;
import fr.hatvp.registre.business.DeclarantService;
import fr.hatvp.registre.commons.dto.request.DeclarantContactDto;
import fr.hatvp.registre.commons.dto.request.DeclarantDto;
import fr.hatvp.registre.commons.lists.CategoryTelephoneMail;
import fr.hatvp.registre.commons.lists.CiviliteEnum;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApiTestConfig.class}, loader = AnnotationConfigContextLoader.class)
public class TestDeclarantRestService {

    @Mock
    private DeclarantService declarantService;

    @InjectMocks
    private DeclarantRestService declarantRestService;

    private MockMvc mockMvc;

    @Before
    public void setup() {

        // Process mock annotations
        MockitoAnnotations.initMocks(this);

        // Setup Spring test in standalone mode
        this.mockMvc = MockMvcBuilders.standaloneSetup(declarantRestService).build();
    }

    @Test
    public void testGetDeclarantDataById() throws Exception {

        Long declarant_id = 1L;

        when(declarantService.loadDeclarantById(declarant_id)).thenReturn(getUser());

        mockMvc.perform(get("/declarant/" + declarant_id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void testGetDeclarantDataByExistingId() throws Exception {

        Long declarant_id = 1L;

        DeclarantDto declarantDto = getUser();

        when(declarantService.loadDeclarantById(declarant_id)).thenReturn(declarantDto);

        mockMvc.perform(get("/declarant/" + declarant_id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void testGetDeclarantDataByNonExistingId() throws Exception {

        Long declarant_id = 1L;

        when(declarantService.loadDeclarantById(declarant_id)).thenReturn(null);

        mockMvc.perform(get("/declarant/" + declarant_id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    private DeclarantDto getUser() {
        DeclarantDto declarantDto = new DeclarantDto();
        declarantDto.setFirstName("toto");
        declarantDto.setLastName("tata");
        declarantDto.setEmail("test@test.fr");
        declarantDto.setPassword("testtest1223");
        declarantDto.setBirthDate(new Date());
        declarantDto.setCivility(CiviliteEnum.MME.getValue());


        List<DeclarantContactDto> emails = new ArrayList<>();
        emails.add(new DeclarantContactDto("t.t@t.t", null, CategoryTelephoneMail.PERSONNEL.getType()));
        List<DeclarantContactDto> phones = new ArrayList<>();
        phones.add(new DeclarantContactDto(null, "075113xxxx", CategoryTelephoneMail.PERSONNEL.getType()));

        declarantDto.setEmailComplement(emails);
        declarantDto.setPhone(phones);
        return declarantDto;
    }
}
