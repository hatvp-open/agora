-- Liste des espaces collaboratifs
COPY(
select e.id , o.denomination,e.nom_usage,o.type_identifiant_national, (case when o.type_identifiant_national = 'SIREN' THEN o.siren when o.type_identifiant_national = 'RNA' then o.rna  else o.hatvp_number end) as identifiant ,e.categorie_organisation,e.adresse,e.date_creation,e.statut_creation_bo,publi.date_publi as date_premiere_pub,
e.publication_enabled,(case when e.espace_actif = true THEN 'Actif' ELSE 'Inactif' END) as statut,cli.nombreClients,cli.liste as clients,collab.nombreCollab,collab.liste as collabs,affiliations.nombreAffiliations,affiliations.liste as affilitations, contactsOP.liste as contactsOP, publi.listeSec, publi.listeNiv, chargees.liste as agents_en_charge, lastCharge.liste as dernier_agent_en_charge
from espace_organisation e
natural join (select id as organisation_id,denomination,type_identifiant_national,siren,rna,hatvp_number from organisation) o
left outer join (select c.espace_organisation_id,count(c.organisation_id) nombreClients,string_agg(o.denomination||' ('||COALESCE(o.nom_usage_siren, ' ')||') '||o.type_identifiant_national||'['||(case when o.type_identifiant_national = 'SIREN' THEN o.siren when o.type_identifiant_national = 'RNA' then o.rna  else o.hatvp_number end)||']', ',') liste from clients c,organisation o where o.id=c.organisation_id group by espace_organisation_id) cli on cli.espace_organisation_id=e.id
left outer join (select c.espace_organisation_id,count(c.organisation_id) nombreAffiliations,string_agg(o.denomination||' ('||COALESCE(o.nom_usage_siren, ' ')||') '||o.type_identifiant_national||'['||(case when o.type_identifiant_national = 'SIREN' THEN o.siren when o.type_identifiant_national = 'RNA' then o.rna  else o.hatvp_number end)||']', ',') liste from association_pro c,organisation o where o.id=c.organisation_id group by espace_organisation_id) affiliations on affiliations.espace_organisation_id=e.id
left outer join (select i.espace_organisation_id , string_agg(civilite||' '||prenom||' '||nom||'('||email||')', ', ') liste from declarant d, inscription_espace i, organisation o, espace_organisation e where e.organisation_id = o.id and e.id = i.espace_organisation_id and d.id = i.declarant_id and i.id in (select distinct inscription_espace_id from roles where role = 'ADMINISTRATEUR') group by espace_organisation_id ) contactsOP on contactsOP.espace_organisation_id = e.id
left outer join (select espace_organisation_id,count(id) nombreCollab,string_agg(prenom||' '||nom||'('||email||')', ', ') liste from collaborateur group by espace_organisation_id) collab on collab.espace_organisation_id=e.id
left outer join (select espace_organisation_id, string_agg(distinct secteur_activite, ', ') listeSec , string_agg(distinct niveau_intervention , ', ') listeNiv, MIN(date_creation) date_publi from publication group by espace_organisation_id) publi on publi.espace_organisation_id = e.id
left outer join (select espace_organisation_id , string_agg(distinct prenom||' '||nom, ', ') liste from utilisateur_bo u, espace_organisation_lock_bo el where u.id = el.utilisateur_bo_id group by espace_organisation_id) chargees on chargees.espace_organisation_id = e.id
left outer join (select espace_organisation_id , (array_agg(prenom||' '||nom||' ('|| el.locked_time ||')' order by el.locked_time asc))[array_upper(array_agg(prenom||' '||nom||' ('|| el.locked_time ||')'), 1)] liste from utilisateur_bo u, espace_organisation_lock_bo el where u.id = el.utilisateur_bo_id group by espace_organisation_id) lastCharge on lastCharge.espace_organisation_id = e.id
order by e.date_creation desc
) TO STDOUT
WITH
CSV
DELIMITER ';'
HEADER
ENCODING 'UTF8';