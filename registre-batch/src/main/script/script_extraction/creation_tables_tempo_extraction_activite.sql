-- création tables tempo
select 
pub.activite_id,
(array_agg(d.civilite||' '||prenom||' '||nom||'' order by pub.date_creation desc))[array_upper(array_agg(civilite||' '||prenom||' '||nom||''), 1)] premier_publicateur, 
MIN(pub.date_creation) as date_premiere_publication, 
MAX(pub.date_creation) as date_derniere_maj, 
(array_agg(d.civilite||' '||prenom||' '||nom||'' order by pub.date_creation asc))[array_upper(array_agg(civilite||' '||prenom||' '||nom||''), 1)] publicateur_derniere_maj 
into temp_info_publi_activite
from activite_representation_interet a, publication_activite pub, declarant d 
where d.id= pub.publicateur_id and a.date_creation > '2019-01-01' and a.date_creation < '2020-01-01' group by pub.activite_id;


select 
pub.activite_id,
(array_agg(email  order by pub.date_creation desc))[array_upper(array_agg(email), 1)] premier_publicateur_mail, 
(array_agg(email order by pub.date_creation asc))[array_upper(array_agg(email), 1)] publicateur_derniere_maj_mail 
into temp_info_publi_activite_mail
from activite_representation_interet a, publication_activite pub, declarant d 
where d.id= pub.publicateur_id and a.date_creation > '2019-01-01' and a.date_creation < '2019-02-01' group by pub.activite_id;


select 
a.id as acid, 
a.createur_declaration, 
string_agg(civilite||' '||prenom||' '||nom||'('||email||')', ', ') liste 
into temp_info_createur
from declarant d, activite_representation_interet a 
where d.id = a.createur_declaration  group by a.id ;

select 
activiterepresentationinteretentity_id,
string_agg(distinct n.categorie, '; ') liste 
into temp_secteur
from activite_representation_domaine_intervention, activite_representation_interet a, nomenclature_domaines_intervention n 
where a.id = activiterepresentationinteretentity_id and domainesintervention_id = n.id
group by activiterepresentationinteretentity_id;

select 
activiterepresentationinteretentity_id, 
string_agg(n.libelle, '; ') liste 
into temp_domaine
from activite_representation_domaine_intervention, activite_representation_interet a, nomenclature_domaines_intervention n 
where a.id = activiterepresentationinteretentity_id and domainesintervention_id = n.id 
group by activiterepresentationinteretentity_id;

select 
ex.id, 
a.exercice_comptable_id, 
a.id as acid, 
a.createur_declaration, 
string_agg(distinct r.role, ', ') liste  
into temp_role
from exercice_comptable ex, activite_representation_interet a, roles r, inscription_espace i, declarant d 
where a.exercice_comptable_id = ex.id and ex.espace_organisation_id = i.espace_organisation_id and d.id = i.declarant_id
and a.date_creation > '2019-01-01' and a.date_creation < '2020-01-01'
group by acid, ex.id;