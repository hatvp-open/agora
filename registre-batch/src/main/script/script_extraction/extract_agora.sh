#!/bin/bash

# dossier extraction
EH=/home/it-hatvp/extractions
AGORA=/var/lib/tomcat/webapps/registre-bo-api/WEB-INF/classes

# date du jour
NOW=`date +"%Y%m%d-%H%M%S"`

# backup
mkdir $EH/archives/archives_$NOW
mv $EH/extracted/* $EH/archives/archives_$NOW

# creation tables temporaire pour alleger generation extraction activite (attention dates a mettre a jour)
psql -U ithatvp -f $EH/scripts/creation_tables_tempo_extraction_activite.sql hatvp_registre

# generation des extractions au format csv dans le dossier extracted
psql -U ithatvp -f $EH/scripts/1_Liste_des_espaces_collaboratifs.sql hatvp_registre > $EH/extracted/1_Liste_des_espaces_collaboratifs.csv
psql -U ithatvp -f $EH/scripts/2_Liste_des_fiches_activites.sql hatvp_registre > $EH/extracted/2_Liste_des_fiches_activites.csv
psql -U ithatvp -f $EH/scripts/3_Liste_des_comptes_personnels_crees.sql hatvp_registre > $EH/extracted/3_Liste_des_comptes_personnels_crees.csv
psql -U ithatvp -f $EH/scripts/4_Liste_des_demandes_ajout_referentiel.sql hatvp_registre > $EH/extracted/4_Liste_des_demandes_ajout_referentiel.csv
psql -U ithatvp -f $EH/scripts/5_nombre_fiches_activites.sql hatvp_registre > $EH/extracted/5_nombre_fiches_activites.csv
psql -U ithatvp -f $EH/scripts/6_antoinette.sql hatvp_registre > $EH/extracted/6_antoinette.csv

rm $AGORA/1_Liste_des_espaces_collaboratifs.csv
rm $AGORA/2_Liste_des_fiches_activites.csv
rm $AGORA/3_Liste_des_comptes_personnels_crees.csv
rm $AGORA/4_Liste_des_demandes_ajout_referentiel.csv
rm $AGORA/5_nombre_fiches_activites.csv
rm $AGORA/6_antoinette.csv

cp $EH/extracted/1_Liste_des_espaces_collaboratifs.csv $AGORA/
cp $EH/extracted/2_Liste_des_fiches_activites.csv $AGORA/
cp $EH/extracted/3_Liste_des_comptes_personnels_crees.csv $AGORA/
cp $EH/extracted/4_Liste_des_demandes_ajout_referentiel.csv $AGORA/
cp $EH/extracted/5_nombre_fiches_activites.csv $AGORA/
cp $EH/extracted/6_antoinette.csv $AGORA/

chmod 755 $AGORA/1_Liste_des_espaces_collaboratifs.csv
chmod 755 $AGORA/2_Liste_des_fiches_activites.csv
chmod 755 $AGORA/3_Liste_des_comptes_personnels_crees.csv
chmod 755 $AGORA/4_Liste_des_demandes_ajout_referentiel.csv
chmod 755 $AGORA/5_nombre_fiches_activites.csv
chmod 755 $AGORA/6_antoinette.csv


# suppression tables temporaire pour alleger generation extraction activite 
psql -U ithatvp -f $EH/scripts/suppression_tables_tempo_extraction_activite.sql hatvp_registre