-- Liste des comptes personnels crees
COPY(
select id, prenom, nom, (case when d.compte_actif = true THEN 'Actif' ELSE 'Inactif' END) as statut, (case when EXISTS(select * from espace_declarant_piece where declarant_id = d.id) then 'Oui' else 'Non' end) as piece_identite, ec.liste as liste_espaces_collabs
from declarant d
left outer join (select i.declarant_id did, string_agg(o.denomination||' ('||COALESCE(o.nom_usage_siren, ' ')||') '||o.type_identifiant_national||'['||(case when o.type_identifiant_national = 'SIREN' THEN o.siren when o.type_identifiant_national = 'RNA' then o.rna  else o.hatvp_number end)||']', ', ') liste from declarant d, inscription_espace i, espace_organisation e, organisation o where i.declarant_id = d.id and i.espace_organisation_id = e.id and e.organisation_id = o.id and (i.date_validation is not NULL and i.date_suppression is null)  group by did ) ec on ec.did = d.id
order by d.id desc
) TO STDOUT
WITH
CSV
DELIMITER ';'
HEADER
ENCODING 'UTF8';