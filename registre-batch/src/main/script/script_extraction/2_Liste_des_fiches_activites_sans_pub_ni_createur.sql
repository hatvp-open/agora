-- Liste des fiches d'activités
COPY(
select a.id,esp.espace_organisation_id,
a.objet, secteurs.liste as secteurs_activites, 
domaines.liste as domaines_intervention, exercice.date_debut, 
exercice.date_fin, a.date_creation,
--createur.liste, roles.liste as createur_roles,
null as liste, null as createur_roles,
a.statut, 
--pub.date_premiere_publication, pub.premier_publicateur, pub.date_derniere_maj, pub.publicateur_derniere_maj
null as date_premiere_publication, null as premier_publicateur, null as date_derniere_maj, null as publicateur_derniere_maj
from activite_representation_interet a
left outer join (select ex.id,ex.espace_organisation_id from exercice_comptable ex) esp on a.exercice_comptable_id = esp.id
left outer join (select activiterepresentationinteretentity_id, string_agg(distinct n.categorie, '; ') liste from activite_representation_domaine_intervention, activite_representation_interet a, nomenclature_domaines_intervention n where a.id = activiterepresentationinteretentity_id and domainesintervention_id = n.id group by activiterepresentationinteretentity_id) secteurs on secteurs.activiterepresentationinteretentity_id=a.id
left outer join (select activiterepresentationinteretentity_id, string_agg(n.libelle, '; ') liste from activite_representation_domaine_intervention, activite_representation_interet a, nomenclature_domaines_intervention n where a.id = activiterepresentationinteretentity_id and domainesintervention_id = n.id group by activiterepresentationinteretentity_id) domaines on domaines.activiterepresentationinteretentity_id=a.id
left outer join (select ex.id, a.exercice_comptable_id, a.id as acid, ex.date_debut, ex.date_fin from exercice_comptable ex, activite_representation_interet a where a.exercice_comptable_id = ex.id group by acid, ex.id) exercice on exercice.acid=a.id
--left outer join (select a.id as acid, a.createur_declaration, string_agg(civilite||' '||prenom||' '||nom||'('||email||')', ', ') liste from declarant d, activite_representation_interet a where d.id = a.createur_declaration  group by a.id ) createur on createur.acid = a.id
--left outer join (select ex.id, a.exercice_comptable_id, a.id as acid, a.createur_declaration, string_agg(distinct r.role, ', ') liste  from exercice_comptable ex, activite_representation_interet a, roles r, inscription_espace i, declarant d where a.exercice_comptable_id = ex.id and ex.espace_organisation_id = i.espace_organisation_id and d.id = i.declarant_id group by acid, ex.id) roles on roles.acid=a.id
--left outer join (select pub.activite_id,(array_agg(d.civilite||' '||prenom||' '||nom||'('||email||')' order by pub.date_creation desc))[array_upper(array_agg(civilite||' '||prenom||' '||nom||'('||email||')'), 1)] premier_publicateur, MIN(pub.date_creation) as date_premiere_publication, MAX(pub.date_creation) as date_derniere_maj, (array_agg(d.civilite||' '||prenom||' '||nom||'('||email||')' order by pub.date_creation asc))[array_upper(array_agg(civilite||' '||prenom||' '||nom||'('||email||')'), 1)] publicateur_derniere_maj from activite_representation_interet a, publication_activite pub, declarant d where d.id= pub.publicateur_id group by pub.activite_id ) pub on pub.activite_id = a.id
where a.date_creation > '2020-01-01' and a.date_creation < '2021-01-01'
order by a.date_creation desc
) TO STDOUT
WITH
CSV
DELIMITER ';'
HEADER
ENCODING 'UTF8';
