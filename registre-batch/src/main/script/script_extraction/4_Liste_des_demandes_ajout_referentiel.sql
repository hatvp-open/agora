-- Liste des demandes d'ajout dans le référentiel
COPY(
SELECT id, denomination, demandeur.liste as demandeur, date_creation as date_demande, statut, chargees.liste as agents_en_charge, lastCharge.liste as dernier_agent_en_charge
FROM demande_organisation deo
left outer join (select deo.id as doid, string_agg(civilite||' '||prenom||' '||nom||'('||email||')', ', ') liste from declarant d, demande_organisation deo where d.id = deo.declarant_id group by deo.id ) demandeur on doid = deo.id
left outer join (select demande_organisation_id , string_agg(distinct prenom||' '||nom, ', ') liste from utilisateur_bo u, demande_organisation_lock_bo el where u.id = el.utilisateur_bo_id group by demande_organisation_id) chargees on chargees.demande_organisation_id = deo.id
left outer join (select demande_organisation_id , (array_agg(prenom||' '||nom||' ('|| el.locked_time ||')' order by el.locked_time asc))[array_upper(array_agg(prenom||' '||nom||' ('|| el.locked_time ||')'), 1)] liste from utilisateur_bo u, demande_organisation_lock_bo el where u.id = el.utilisateur_bo_id group by demande_organisation_id) lastCharge on lastCharge.demande_organisation_id = deo.id
order by date_demande desc
) TO STDOUT
WITH
CSV
DELIMITER ';'
HEADER
ENCODING 'UTF8';