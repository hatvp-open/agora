-- Liste des fiches d'activités
COPY(
select org.siren siren ,org.denomination denomination,esp.fin_exercice_fiscal,ex.date_debut,ex.date_fin,ex.statut,count(a.id) nombre_fiches_publiees from exercice_comptable ex,activite_representation_interet a,espace_organisation esp,organisation org where ex.espace_organisation_id = esp.id and esp.organisation_id = org.id  and a.statut in ('PUBLIEE','MAJ_PUBLIEE') and a.exercice_comptable_id = ex.id and esp.espace_actif = true group by siren,denomination,fin_exercice_fiscal,date_debut,date_fin,ex.statut order by siren,denomination,ex.date_debut,nombre_fiches_publiees 
) TO STDOUT
WITH
CSV
DELIMITER ';'
HEADER
ENCODING 'UTF8';
