-- Liste des fiches d'activités
COPY(
select a.id,esp.espace_organisation_id, a.objet, secteurs.liste as secteurs_activites, domaines.liste as domaines_intervention, exercice.date_debut, exercice.date_fin, a.date_creation, createur.liste, roles.liste as createur_roles, a.statut, pub.date_premiere_publication, pub.premier_publicateur, pub_mail.premier_publicateur_mail, pub.date_derniere_maj, pub.publicateur_derniere_maj , pub_mail.publicateur_derniere_maj_mail
from activite_representation_interet a
left outer join (select ex.id,ex.espace_organisation_id from exercice_comptable ex) esp on a.exercice_comptable_id = esp.id
left outer join temp_secteur secteurs on secteurs.activiterepresentationinteretentity_id=a.id
left outer join temp_domaine domaines on domaines.activiterepresentationinteretentity_id=a.id
left outer join (select ex.id, a.exercice_comptable_id, a.id as acid, ex.date_debut, ex.date_fin from exercice_comptable ex, activite_representation_interet a where a.exercice_comptable_id = ex.id group by acid, ex.id) exercice on exercice.acid=a.id
left outer join temp_info_createur createur on createur.acid = a.id
left outer join temp_role roles on roles.acid=a.id
left outer join temp_info_publi_activite pub on pub.activite_id = a.id
left outer join temp_info_publi_activite_mail pub_mail on pub_mail.activite_id = a.id
where a.date_creation > '2019-01-01' and a.date_creation < '2020-01-01'
order by a.date_creation desc
) TO STDOUT
WITH
CSV
DELIMITER ';'
HEADER
ENCODING 'UTF8';