-- Nombre de fiches d'activites
COPY(
select e.id , o.denomination,e.nom_usage,o.type_identifiant_national, (case when o.type_identifiant_national = 'SIREN' THEN o.siren when o.type_identifiant_national = 'RNA' then o.rna  else o.hatvp_number end) as identifiant ,e.categorie_organisation,e.date_creation,e.statut_creation_bo,
(case when e.espace_actif = true THEN 'Actif' ELSE 'Inactif' END) as statut, fiches.nb as nombre_de_fiches_activites
from espace_organisation e
natural join (select id as organisation_id,denomination,type_identifiant_national,siren,rna,hatvp_number from organisation) o
left outer join (select count(a.id) nb, e.espace_organisation_id eid from activite_representation_interet a, exercice_comptable e, espace_organisation es where a.exercice_comptable_id = e.id and e.espace_organisation_id = es.id group by e.espace_organisation_id ) fiches on fiches.eid = e.id
order by e.date_creation desc
) TO STDOUT
WITH
CSV
DELIMITER ';'
HEADER
ENCODING 'UTF8';
