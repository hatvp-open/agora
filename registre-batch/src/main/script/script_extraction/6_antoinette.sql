-- Extractions complete antoinette
COPY(
select id, version, coefficient_confiance, date_enregistrement, id_fiche, identifiant_national, note,regexp_replace(objet, E'[\\n\\r]+', ' ', 'g') as objet, denomination, nomusage from qualification_objet order by id desc
) TO STDOUT
WITH
CSV
DELIMITER ';'
HEADER
ENCODING 'UTF8';
