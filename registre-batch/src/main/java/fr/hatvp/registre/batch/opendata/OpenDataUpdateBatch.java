/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.opendata;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import fr.hatvp.registre.batch.common.smtp.service.EmailSenderService;
import fr.hatvp.registre.batch.common.ssh.service.SftpTransferService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.PublicationService;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.commons.exceptions.TechnicalException;

/**
 * Cette classe offre les fonctionnalités nécessaires à la production du fichier open data
 * des déclarations faites dans le registre vers le site internet institutionnel de la HATVP.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Component
public class OpenDataUpdateBatch
{
    /** Service de publication. */
    @Autowired
    private PublicationService publicationService;

    /** Service des espaces. */
    @Autowired
    private EspaceOrganisationService espaceOrganisationService;

    /** Service d'envoi des emails. */
    @Autowired
    private EmailSenderService emailSenderService;

    /** Service SFTP. */
    @Autowired
    private SftpTransferService sftpTransferService;

    /** Destinataire des email d'alerte en cas d'exception. */
    @Value("${batch.alert.recipient}")
    private String destinataireAlerte;

    /** Adresse du serveur pour la connexion SFTP. */
    @Value("${batch.publication.ssh.host}")
    private String sshHost;

    /** Utilisateur pour la connexion SFTP. */
    @Value("${batch.publication.ssh.user}")
    private String sshUser;

    /** Mot de passe pour la connexion SFTP. */
    @Value("${batch.publication.ssh.password}")
    private String sshPassword;

    /** Port pour la connexion SFTP. */
    @Value("${batch.publication.ssh.port}")
    private int sshPort;

    /** Dossier distant où envoyer les fichiers de la publication. */
    @Value("${batch.opendata.dist.folder}")
    private String opendataDistFolder;

    /** Dossier local où stocker temporairement les fichiers de la publication. */
    @Value("${batch.opendata.local.folder}")
    private String opendataLocalFolder;

    /** Nom du fichier opendata à produire. */
    @Value("${batch.opendata.filename}")
    private String opendataFileName;
    
    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenDataUpdateBatch.class);

    /**
     * Procédure qui lance le processus et traite les exceptions.
     */
    public void launchProcess()
    {
        try {
            this.runPublicationProcess();
        }
        catch (final RuntimeException exception) {
            LOGGER.error("Erreur imprévue survenue pendant l'exécution du programme.");
            LOGGER.error(exception.getMessage(), exception);
            this.disconnect();
            try {
                this.emailSenderService.sendMailAsAutomateHatvp(
                        "AGORA : erreur critique pendant la publication open data des déclarations",
                        "Veuillez trouvez la trace ci-dessous : \r\n\r\n"
                                + ExceptionUtils.getMessage(exception)
                                + "\r\n\r\n"
                                +  ExceptionUtils.getStackTrace(exception),
                        this.destinataireAlerte);
            }
            catch (final MessagingException e) {
                LOGGER.error("Erreur lors de l'envoi de l'email.");
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Cette procédure implémente le processus de génération du fichier open data lancée à intervalles réguliers.
     * @throws JSchException
     * @throws SftpException
     * @throws IOException
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    protected void runPublicationProcess()
    {
        // Étape 1 : initialisation - on liste les identifiants de tous les espaces publiés.
        final List<Long> espacesPublies = this.espaceOrganisationService
                .listeIdEspacesPublies();

        // On nettoie le dossier local de stockage temporaire des publications.
        final Iterator<File> filesToCleanUp = FileUtils
                .iterateFiles(new File(this.opendataLocalFolder), TrueFileFilter.TRUE, null);
        while (filesToCleanUp.hasNext()) {
            final File file = filesToCleanUp.next();
            FileUtils.deleteQuietly(file);
        }


        // Étape 2 : on produit localement le document json.
        try {
            // Jackson object mapper pour sérialiser en JSON.
            final ObjectMapper mapper = new ObjectMapper();
            
            // On ouvre le tableau où sont stockés les publications dans le document JSON
            try (FileWriter fw = this.getOpenDataFileWriter()) {
                fw.write("{\"publications\": [");
            }
            
            // Compteur des itérations sur les espaces.
            int i = 1;
            
            // On enrichit le document JSON des données de chaque espace publié. 
            for (final Long espaceId : espacesPublies) {
                
                // Writer du fichier opendata : on doit le réinstancier à 
                // chaque itération car il est fermé par l'object mapper.
                try (FileWriter fw = this.getOpenDataFileWriter()) {
                    // Séparateur avant d'écrire l'élément suivant (on produit un tableau d'objets JSON)
                    // On ne produit pas de virgule pour la première itération.
                    if( i != 1) {
                        fw.write(",");
                    }
                    
                    // Récupération de la publication pour l'espace courant.
                    final PublicationDto nouvellePublication = this.publicationService.getDernierContenuPourPublication(espaceId);
                    
                    // Écriture dans le fichier à produire.
                    mapper.writeValue(fw, nouvellePublication);
                    
                    i++;
                    LOGGER.info("Écriture information espace id : {} ok", espaceId);
                }
            }
            
            // On écrit les caractères pour fermer le tableau et le document JSON
            try (FileWriter fw = this.getOpenDataFileWriter()) {
                fw.write("]}");
            }
            
            LOGGER.info("Production du fichier opendata terminée.");
        }
        catch (final IOException exception) {
            throw new TechnicalException(exception.getMessage(), exception);
        }

        // Transfert du fichier produit vers le site de la HATVP.
        try {
            // Étape 3 :  On ouvre la connexion SFTP.
            LOGGER.info("Transfert vers le site HATVP.");
            this.sftpTransferService.connect(this.sshHost, this.sshUser, this.sshPassword,
                    this.sshPort);
            LOGGER.info("Connexion au serveur du site HATVP établie.");
            
            // Étape 4 : on recopie les fichiers produits vers le serveur. Puis on le supprime
            // localement après transfert.
            LOGGER.info("Envoi des fichiers.");
            final Iterator<File> filesToUpload = FileUtils
                    .iterateFiles(new File(this.opendataLocalFolder), TrueFileFilter.TRUE, null);
            while (filesToUpload.hasNext()) {
                final File file = filesToUpload.next();
                final String fileName = new String(file.getName());
                this.sftpTransferService.sendFileToPath(file.getPath(),
                        this.opendataDistFolder + "/" + fileName);
                FileUtils.deleteQuietly(file);
                LOGGER.info("Fichier {} transféré et supprimé localement.", fileName);
            }

            // Étape 5 : on ferme la connexion SFTP.
            this.disconnect();
        }
        catch (final JSchException | SftpException exception) {
            throw new TechnicalException(exception.getMessage(), exception);
        }

    }

    /**
     * Déconnexion au serveur de publication.
     */
    private void disconnect()
    {
        this.sftpTransferService.disconnect();
        LOGGER.info("Déconnexion au serveur de publication établie.");
    }

    /**
     * Writer en mode "append" utilisé pour écrire les documents JSON dans le fichier.
     * @return le filewriter pour le fichier open data.
     * @throws IOException 
     */
    private FileWriter getOpenDataFileWriter() throws IOException {
    	return new FileWriter(this.opendataLocalFolder + "/" + this.opendataFileName, true);
    }
}
