/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.watchdog;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.mail.MessagingException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import fr.hatvp.registre.batch.common.smtp.service.EmailSenderService;
import fr.hatvp.registre.business.DesinscriptionService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.ExerciceComptableService;
import fr.hatvp.registre.business.utils.PublicHolidayUtil;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.activite.batch.ContactOpMailingDto;
import fr.hatvp.registre.commons.dto.activite.batch.DeclarationReminderDto;
import fr.hatvp.registre.commons.dto.activite.batch.PublicationRelanceDto;
import fr.hatvp.registre.commons.lists.PublicationRelanceNiveauEnum;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;

/**
 * Cette classe offre les fonctionnalités nécessaires au watchdog Vérifie les espaces organisations et leurs publications de moyens/activités associées et envoie des mails de
 * rappel selon 4 cas 1. Pas de date de références ou de dates de cloture 2. Pas de pub. de moyens ni d'activités CAS 00 3. Moyens publiés mais pas d'activités pub. CAS 10 4. Pas
 * de moyens publiés mais des activités pub. CAS 01
 *
 * Pour explicité le système de numérotation 0 = non publié et 1 = publié selon la matrice ci-dessous
 * ----------------------- | MOYENS | ACTIVITES |
 * ---------------------- | 0 | 0
 * | CAS 00 [nok] | 0 | 1 | CAS 01 [nok] | 1 | 0 | CAS 10 [nok] | 1 | 1 | CAS 11 [ok]
 * -----------------------
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class PublicationWatchDogBatch {

	private static final Logger LOGGER = LoggerFactory.getLogger(PublicationWatchDogBatch.class);

	@Autowired
	private ExerciceComptableService exerciceComptableService;

	@Autowired
    DesinscriptionService desinscriptionService;
	
	@Autowired
    EspaceOrganisationService espaceOrganisationService;

	@Autowired
	private EmailSenderService emailSenderService;


	/** Destinataire des email d'alerte en cas d'exception. */
	@Value("${batch.alert.recipient}")
	private String destinataireAlerte;

	@Value("${batch.relanceInfo.recipient}")
	private String mailRelancePRP;

	@Value("${batch.watchdog.mail.subject.prealable}")
	private String subjectPrealable;

	@Value("${batch.watchdog.mail.subject.autres}")
	private String subjectAutres;

	@Value("${batch.watchdog.date.month.after.cloture}")
	private long monthAfterCloture;
	@Value("${batch.watchdog.date.day.before.prealable1}")
	private long dayBeforeCloture1;
	@Value("${batch.watchdog.date.day.before.prealable2}")
	private long dayBeforeCloture2;
	@Value("${batch.watchdog.date.day.after.relance1}")
	private long dayAfterRelance1;
	@Value("${batch.watchdog.date.day.after.relance2}")
	private long dayAfterRelance2;
	@Value("${batch.watchdog.date.day.after.blacklist}")
	private long dayAfterBlacklist;

	private static final String template = "email/rappel_publication";

	private final List<PublicationRelanceDto> relanceList = new ArrayList<>();

	private final List<Pair<String, Exception>> errorList = new ArrayList<>();

	private LocalDate today = LocalDate.now();

	private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

	/**
	 * Procédure qui lance le processus et traite les exceptions.
	 *
	 * @param date
	 */
	public void launchProcess(String date) {
		if (date != null) {
			try {
				this.today = LocalDate.parse(date, this.dateFormatter);
			} catch (Exception e) {
				LOGGER.error("Erreur lors du parsing de la date passée en argument : {}", e);
			}
		}
		this.runWatchDogProcess();
		if (!this.errorList.isEmpty()) {
			LOGGER.error("Des erreurs imprévues sont survenue lors de l'exécution du programme.");
			try {
				StringBuilder sb = new StringBuilder("Veuillez trouvez la listes des erreurs ci-dessous (" + errorList.size() + ") : <br/>" );
				this.errorList.forEach(error -> {
					sb.append("<h3>"+error.getFirst() + "</h3><strong>" + ExceptionUtils.getMessage(error.getSecond()) + "</strong><br/>" + ExceptionUtils.getStackTrace(error.getSecond()) + "<br/><br/>");
				});
				this.emailSenderService.sendMailAsAutomateHatvp("AGORA : Liste des erreurs du batch de relance pendant son exécution", sb.toString(), this.destinataireAlerte);
			} catch (final MessagingException e) {
				LOGGER.error("[CRITIQUE] Erreur lors de l'envoi de l'email recapilatif des erreurs.");
				LOGGER.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * Procédure qui relance les contacts opérationnels des espaces collaboratifs au sujet de l'absence de publications
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private void runWatchDogProcess() {
		if (this.today.getDayOfWeek().equals(DayOfWeek.SATURDAY) || this.today.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
			LOGGER.info("[WATCHDOG] -- WEEK END [{}]", LocalDateTime.of(this.today, LocalTime.now()));
			return;
		}
		PublicHolidayUtil publicHolidayUtil = new PublicHolidayUtil(this.today.getYear());
		if (publicHolidayUtil.getPublicHolidays().contains(this.today)) {
			LOGGER.info("[WATCHDOG] -- JOUR FERIE [{}]",  LocalDateTime.of(this.today, LocalTime.now()));
			return;
		}

		LOGGER.info("[WATCHDOG] -- DEBUT [{}]",LocalDateTime.of(this.today, LocalTime.now()));
		
		try {
			//Changement statut DESINSCRIPTION_VALIDEE_HATVP par DESINSCRIT pour les espaces dont date de cessation = date du jour
			this.desinscriptionService.saveDesinscritStatut();
		} catch (Exception e) {
			LOGGER.error("Erreur, les espaces organisations validés HATVP n'ont pas pu être passé en statut désisncrit {}", e);
			this.errorList.add(Pair.of("Erreur, tous les espaces organisations validés HATVP n'ont pas pu être passé en statut désisncrit {}", e));
		}

		try {
			this.exerciceComptableService.createExerciceComptableForAllEspace();
		} catch (Exception e) {
			LOGGER.error("Erreur, tous les exercices comptables n'ont pas pu être créés en date du {} : {}", this.today.format(this.dateFormatter), e);
			this.errorList.add(Pair.of("Erreur, tous les exercices comptables n'ont pas pu être créés en date du " + this.today.format(this.dateFormatter), e));
		}		

		try {
			this.launchFailedReminderMailing();
		} catch (Exception e) {
			LOGGER.error("Erreur, il se peut que la totalité du rejeu du mailing de relance n'ai pas été effectué en date du " + this.today.format(this.dateFormatter) + " : {}", e);
			this.errorList.add(Pair.of("Erreur, il se peut que la totalité du rejeu du mailing de relance n'ai pas été effectué en date du " + this.today.format(this.dateFormatter), e));
		}

		try {
			this.launchReminderMailing();
		} catch (Exception e) {
			LOGGER.error("Erreur critique , le batch de relance ne s'est pas exécuté correctement en date du " + this.today.format(this.dateFormatter)
					+ "  et il est possible que tout ou parti du traitement soit à refaire : {}", e);
			this.errorList.add(Pair.of("Erreur critique , le batch de relance ne s'est pas exécuté correctement en date du " + this.today.format(this.dateFormatter)
					+ "  et il est possible que tout ou partie du traitement soit à refaire", e));
		}

		try {
			this.exerciceComptableService.savePublicationRelance(this.relanceList, this.today);
		} catch (Exception e) {
			LOGGER.error("Erreur critique, l'historique des relances n'a pas été sauvegardé en date du " + this.today.format(this.dateFormatter) + " : {}", e);
			this.errorList.add(Pair.of("Erreur critique, l'historique des relances n'a pas été sauvegardé en date du " + this.today.format(this.dateFormatter), e));
		}

		LOGGER.info("[WATCHDOG] -- FIN");
	}
	/**
	 * Une fois que l' espace organisation est au statut DESINSCRIT , l'oganisation n'est pas relancée 
	 * => relance si <> (DESINSCRIT)
	 */
	private void launchFailedReminderMailing() {
		LOGGER.info("Lancement du rejeu des mailing de relance");
		Map<Long, DeclarationReminderDto> failedMailingData = this.exerciceComptableService.getFailedReminderMail();
		List<Long> succesfulRelanceIds = new ArrayList<>();
		failedMailingData.forEach((relanceId, reminderDto) -> {
            if(this.doitEtreRelance(reminderDto.getEspaceOrganisationId()) && this.prepareAndSendFailedReminderMail(reminderDto)) {
            	succesfulRelanceIds.add(relanceId);
            }
		});
		if (!succesfulRelanceIds.isEmpty()) {
			this.exerciceComptableService.updatePublicationRelance(succesfulRelanceIds, this.today);
		}
		LOGGER.info("Rejeu des mailing de relance effectué");
	}
	/**
	 * Une fois que l' espace organisation est au statut DESINSCRIT, l'oganisation n'est pas relancée 
	 * => relance si <> (DESINSCRIT)
	 */
	private void launchReminderMailing() {
		LOGGER.info("Lancement du mailing de relance");
		List<DeclarationReminderDto> listDeclarationReminderDto = this.exerciceComptableService.getExerciceWithoutDeclarationByDate(this.today);
		for (DeclarationReminderDto declarationReminderDto : listDeclarationReminderDto) {
			try {	
				if(this.doitEtreRelance(declarationReminderDto.getEspaceOrganisationId())) {
	            	prepareAndSendReminderMail(declarationReminderDto);
	            }
			} catch (Exception e) {
				LOGGER.error("Erreur lors d'envoi d'email a tous les contact d'un espace : {}", e);
				this.errorList.add(Pair.of("Erreur lors d'envoi d'email a tous les contact d'un espace", e));
			}
		}
		LOGGER.info("Mailing de relance effectué");
	}

	private void prepareAndSendReminderMail(DeclarationReminderDto ex) {
		String listContactOp = "relance envoyée à : ";
		HashMap<String, String> templateData = prepareGenericTemplateData(ex);
		String subject = prepareMailSubject(ex, templateData);
		for (ContactOpMailingDto contactOp : ex.getContactOp()) {
			try {
				templateData.put("civilite", contactOp.getCivilite().getLibelleLong());
				LOGGER.info("Tentative d'envoi d'email à : {}...", contactOp.getEmail());
				this.emailSenderService.sendMailAsAutomateHatvpWithLogoForBatch(subject, templateData, template, contactOp.getEmail());
				LOGGER.info("Email envoyé !");
				listContactOp += "(" + contactOp.getEmail() + ") ";
				this.relanceList.add(new PublicationRelanceDto(ex.getExerciceCompatbleId(), contactOp.getId(), ex.getNiveauRelance(), ex.getTypeRelance(), true));
				LOGGER.info("Envoi historicisé");
			} catch (Exception e) {
				LOGGER.error("Erreur lors de l'envoi de l'email : {}", e);
				this.errorList.add(Pair.of("Erreur lors de l'envoi de l'email", e));
				this.relanceList.add(new PublicationRelanceDto(ex.getExerciceCompatbleId(), contactOp.getId(), ex.getNiveauRelance(), ex.getTypeRelance(), false));
				LOGGER.info("Echec historisé");
			}
		}
		templateData.put("civilite", listContactOp);
		sendMailToPRP(subject, templateData);
	}

	private void sendMailToPRP(String subject, HashMap<String, String> templateData) {
		try {
			LOGGER.info("Tentative d'envoi d'email à : {}...", this.mailRelancePRP);
			this.emailSenderService.sendMailAsAutomateHatvpWithLogoForBatch(subject, templateData, template, this.mailRelancePRP);
			LOGGER.info("Email envoyé !");
		} catch (Exception e) {
			LOGGER.error("Erreur lors de l'envoi de l'email d'information au PRP: {}", e);
		}
	}

	private boolean prepareAndSendFailedReminderMail(DeclarationReminderDto ex) {
		HashMap<String, String> templateData = prepareGenericTemplateData(ex);
		String subject = prepareMailSubject(ex, templateData);
		ContactOpMailingDto contactOp = ex.getContactOp().get(0);
		try {
			templateData.put("civilite", contactOp.getCivilite().getLibelleLong());
			LOGGER.info("Tentative d'envoi d'email à : {}...", contactOp.getEmail());
			this.emailSenderService.sendMailAsAutomateHatvpWithLogoForBatch(subject, templateData, template, contactOp.getEmail());
			LOGGER.info("Email envoyé !");
			templateData.put("civilite", "relance envoyée à : (" + contactOp.getEmail() + ")");
			sendMailToPRP(subject, templateData);
			return true;
		} catch (Exception e) {
			LOGGER.error("Erreur lors de l'envoi de l'email : {}", e);
			this.errorList.add(Pair.of("Erreur lors de l'envoi de l'email", e));
			return false;
		}
	}

	private HashMap<String, String> prepareGenericTemplateData(DeclarationReminderDto ex) {
		HashMap<String, String> templateData = new HashMap<>();
		templateData.put("denomination", ex.getDenomination());
		templateData.put("dateDebut", ex.getDateDebut().format(this.dateFormatter));
		templateData.put("dateFin", ex.getDateFin().format(this.dateFormatter));
		templateData.put("niveauRappel", ex.getNiveauRelance().toString());
		templateData.put("typeRappel", ex.getTypeRelance().toString());
		templateData.put("nombreActivite", String.valueOf(ex.getNombreFiche()));
		templateData.put("dateLimite", ex.getDateFin().plusMonths(this.monthAfterCloture).plusDays(this.dayAfterRelance1).format(this.dateFormatter));
		Optional.ofNullable(ex.getDateRelance()).ifPresent(dateRelance -> templateData.put("dateRelance", dateRelance.format(this.dateFormatter)));
		templateData.put("dateBlackList", this.today.plusDays(this.dayAfterBlacklist).minusDays(this.dayAfterRelance2).format(this.dateFormatter));
		templateData.put("reference", "[" + ex.getNiveauRelance().toString() + ex.getTypeRelance().toString() + "]");
		return templateData;
	}

	private String prepareMailSubject(DeclarationReminderDto ex, HashMap<String, String> templateData) {
		String subject;
		if (ex.getNiveauRelance().equals(PublicationRelanceNiveauEnum.P)) {
			subject = this.subjectPrealable;
		} else {
			subject = this.subjectAutres;
		}
		subject = StringUtils.replace(subject, "[DATE_DEBUT]", templateData.get("dateDebut"));
		subject = StringUtils.replace(subject, "[DATE_FIN]", templateData.get("dateFin"));
		subject = StringUtils.replace(subject, "[REFERENCE]", templateData.get("reference"));
		return subject;
	}
	/**
	 * Une fois que l' espace organisation est au statut DESINSCRIT, l'oganisation n'est pas relancée 
	 * => relance si <> (DESINSCRIT)
	 */
	private boolean doitEtreRelance (Long espaceOrganisationId) {
		EspaceOrganisationDto espaceOrganisationDto = this.espaceOrganisationService.getEspaceOrganisationDtoById(espaceOrganisationId);
        if(!espaceOrganisationDto.getStatut().equals(StatutEspaceEnum.DESINSCRIT)){
        	return true;
        }else {
        	return false;
        }				
	}

}
