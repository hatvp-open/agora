/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.exceptions;

/**
 * 
 * Exception lancée lorsque le programme détecte que le traitement n'a pas fait de mise à jour
 * depuis un temps trop long (paramétrable).
 * 
 * @version $Revision$ $Date${0xD}
 */
public class SireneUpdateOverdueException
    extends Exception
{

    /**
     * Constructeur avec message.
     * @param string
     */
    public SireneUpdateOverdueException(final String string)
    {
        super(string);
    }

    /** serialVersionUID. */
    private static final long serialVersionUID = -5696717648713325873L;

}
