/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.publication;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import fr.hatvp.registre.batch.common.smtp.service.EmailSenderService;
import fr.hatvp.registre.batch.common.ssh.service.SftpTransferService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.PublicationService;
import fr.hatvp.registre.commons.dto.batch.EspaceOrganisationPublicationBatchDto;
import fr.hatvp.registre.commons.dto.publication.PublicationBucketDto;
import fr.hatvp.registre.commons.exceptions.TechnicalException;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

/**
 * Cette classe offre les fonctionnalités nécessaires à la publication
 * des déclarations faites dans le registre vers le site internet institutionnel de la HATVP.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Component
public class PublicationUpdateBatch
{
    /** Service de publication. */
    @Autowired
    private PublicationService publicationService;

    /** Service des espaces. */
    @Autowired
    private EspaceOrganisationService espaceOrganisationService;

    /** Repository pour la mise à jour des espaces traités */
    @Autowired
    private EspaceOrganisationRepository espaceOrganisationRepository;

    /** Service d'envoi des emails. */
    @Autowired
    private EmailSenderService emailSenderService;

    /** Service SFTP. */
    @Autowired
    private SftpTransferService sftpTransferService;

    /** Destinataire des email d'alerte en cas d'exception. */
    @Value("${batch.alert.recipient}")
    private String destinataireAlerte;

    /** Adresse du serveur pour la connexion SFTP. */
    @Value("${batch.publication.ssh.host}")
    private String sshHost;

    /** Utilisateur pour la connexion SFTP. */
    @Value("${batch.publication.ssh.user}")
    private String sshUser;

    /** Mot de passe pour la connexion SFTP. */
    @Value("${batch.publication.ssh.password}")
    private String sshPassword;

    /** Port pour la connexion SFTP. */
    @Value("${batch.publication.ssh.port}")
    private int sshPort;

    /** Dossier distant où envoyer les fichiers de la publication. */
    @Value("${batch.publication.dist.folder}")
    private String publicationDistFolder;

    /** Dossier local où stocker temporairement les fichiers de la publication. */
    @Value("${batch.publication.local.folder}")
    private String publicationLocalFolder;

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PublicationUpdateBatch.class);

    /**
     * Procédure qui lance le processus et traite les exceptions.
     */
    public void launchProcess()
    {
        try {
            this.runPublicationProcess();
        }
        catch (final RuntimeException exception) {
            LOGGER.error("Erreur imprévue survenue pendant l'exécution du programme.");
            LOGGER.error(exception.getMessage(), exception);
            this.disconnect();
            try {
                this.emailSenderService.sendMailAsAutomateHatvp(
                        "AGORA : erreur critique pendant la publication des déclarations",
                        "Veuillez trouvez la trace ci-dessous : \r\n\r\n"
                                + ExceptionUtils.getMessage(exception)
                                + "\r\n\r\n"
                                + ExceptionUtils.getStackTrace(exception),
                        this.destinataireAlerte);
            }
            catch (final MessagingException e) {
                LOGGER.error("Erreur lors de l'envoi de l'email.");
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Cette procédure implémente le processus de publication lancé à intervalles réguliers.
     * @throws JSchException
     * @throws SftpException
     * @throws IOException
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    protected void runPublicationProcess()
    {

        // Jackson object mapper pour sérialiser en JSON.
        final ObjectMapper mapper = new ObjectMapper();

        // Étape 1 : initialisation - on récupère les listes des documents à produire :
        // Listes des espaces à publier.
        final List<EspaceOrganisationPublicationBatchDto> espacesAPublier = this.espaceOrganisationService
                .listeEspacesAPublier();
        // Listes des espaces à dépublier.
        final List<EspaceOrganisationPublicationBatchDto> espacesADepublier = this.espaceOrganisationService
                .listeEspacesADepublier();

        // On détermine s'il y a des espaces à traiter.
        if (espacesAPublier.isEmpty() && espacesADepublier.isEmpty()) {
            LOGGER.info("Pas de nouvelle publication a traiter. Aucun fichier ne sera produit.");
            return;
        }

        // On nettoie le dossier local de stockage temporaire des publications.
        final Iterator<File> filesToCleanUp = FileUtils
                .iterateFiles(new File(this.publicationLocalFolder), TrueFileFilter.TRUE, null);
        while (filesToCleanUp.hasNext()) {
            final File file = filesToCleanUp.next();
            FileUtils.deleteQuietly(file);
        }

        /*
         * PRODUCTION DES FICHIERS
         */
        try {
            // Étape 2 : on produit localement le fichier qui récapitule l'ensemble des dernières
            // publications (utilisé pour la recherche sur le site hatvp.fr)
            mapper.writeValue(FileUtils.getFile(this.publicationLocalFolder, "stock.json"),
                    this.publicationService.getPublicationsGlobalesRechercheLivrable());

            LOGGER.info("Production du fichier stock.json réussie.");

            // Étape 2 : on produit localement la liste des types d'organisations.
            FileUtils.write(
                    FileUtils.getFile(this.publicationLocalFolder, "typesOrganisations.json"),
                    TypeOrganisationEnum.typesOrganisationsAsJson());
            LOGGER.info("Production du fichier typesOrganisations.json réussie.");

            // Étape 3 : pour chaque espace à publier on produit localement le document json à
            // charger et on met à jour le statut de publication.
            for (final EspaceOrganisationPublicationBatchDto espace : espacesAPublier) {
                final PublicationBucketDto nouvellePublication = this.publicationService
                        .getPublicationLivrable(espace.getId());
                if (nouvellePublication != null) {

                    mapper.writeValue(FileUtils.getFile(this.publicationLocalFolder,
                            espace.getNationalId() + ".json"), nouvellePublication);
                    LOGGER.warn("Production du fichier {}.json réussie",  espace.getNationalId() );
                }
                else {
                    LOGGER.warn("L'espace {} n'a aucune donnée à publier. Aucun document JSON ne sera généré.",espace.getNationalId());
                }

                // Mise à jour du flag pour l'espace
                final EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository
                        .findOne(espace.getId());
                final boolean flagMiseAJour = this.espaceMisAJour(espaceOrganisationEntity, espace);
                if (flagMiseAJour) {
                    espaceOrganisationEntity.setNewPublication(Boolean.FALSE);
                    this.espaceOrganisationRepository.save(espaceOrganisationEntity);
                }
                else {
                    LOGGER.info("L'espace " + espace.getNationalId()
                            + " à été mis a jour pendant l'exécution du batch. Il sera republié à la prochaine exécution.");
                }
            }
            LOGGER.info("Production des fichiers à publier terminée.");
        }
        catch (final IOException exception) {
            throw new TechnicalException(exception.getMessage(), exception);
        }

        /*
         * TRANSFERT VERS LE SITE DE PUBLICATION
         */

        try {
            // Étape 4 : on ouvre la connexion SFTP.
            LOGGER.info("Transfert vers le serveur de publication.");
            this.sftpTransferService.connect(this.sshHost, this.sshUser, this.sshPassword,
                    this.sshPort);
            LOGGER.info("Connexion au serveur de publication établie.");
            // Étape 5 : on recopie les fichiers produits vers le serveur. Puis on le supprime
            // localement après transfert.
            LOGGER.info("Envoi des fichiers.");
            final Iterator<File> filesToUpload = FileUtils
                    .iterateFiles(new File(this.publicationLocalFolder), TrueFileFilter.TRUE, null);
            while (filesToUpload.hasNext()) {
                final File file = filesToUpload.next();
                final String fileName = new String(file.getName());
                this.sftpTransferService.sendFileToPath(file.getPath(),
                        this.publicationDistFolder + "/" + fileName);
                FileUtils.deleteQuietly(file);
                LOGGER.info("Fichier {} transféré et supprimé localement.",fileName);
            }

            // Étape 6 : on supprime du server distant les fichiers des espaces dépubliés et on met
            // à jour le statut de publication.
            for (final EspaceOrganisationPublicationBatchDto espace : espacesADepublier) {
                this.sftpTransferService.removeDistantFile(
                        this.publicationDistFolder + "/" + espace.getNationalId() + ".json");
                LOGGER.info("Suppression du fichier {}.json réussie",espace.getNationalId());

                // Mise à jour du flag pour l'espace
                final EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository
                        .findOne(espace.getId());
                final boolean flagMiseAJour = this.espaceMisAJour(espaceOrganisationEntity, espace);
                if (flagMiseAJour) {
                    espaceOrganisationEntity.setNewPublication(Boolean.FALSE);
                    this.espaceOrganisationRepository.save(espaceOrganisationEntity);
                }
                else {
                    LOGGER.info("L'espace " + espace.getNationalId()
                            + " à été mis a jour pendant l'exécution du batch. Il sera republié à la prochaine exécution.");
                }
            }

            // Étape 7 : on ferme la connexion SFTP.
            this.disconnect();
        }
        catch (final JSchException | SftpException exception) {
            throw new TechnicalException(exception.getMessage(), exception);
        }

    }

    /**
     * Déconnexion au serveur de publication.
     */
    private void disconnect()
    {
        this.sftpTransferService.disconnect();
        LOGGER.info("Déconnexion au serveur de publication établie.");
    }

    /**
     * On peut marquer que la publication de l'espace a été faite si le numéro de version de
     * l'espace n'a pas changé.
     * Si le numéro a changé on considère qu'il faudra republier l'espace à la prochaine exécution
     * du batch
     * (hypothèse la plus sécurisante).
     * @param espaceOrganisationEntity
     * @param espaceOrganisationDto
     * @return vrai si le flag a été mis à jour, faux sinon.
     */
    protected boolean espaceMisAJour(final EspaceOrganisationEntity espaceOrganisationEntity,
            final EspaceOrganisationPublicationBatchDto espaceOrganisationDto)
    {
        return (espaceOrganisationDto.getVersion().intValue() == espaceOrganisationEntity
                .getVersion().intValue());
    }
}
