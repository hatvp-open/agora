/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.common.smtp.impl;

import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import fr.hatvp.registre.batch.common.smtp.service.EmailSenderService;
import fr.hatvp.registre.batch.publication.logo.Logo;
import fr.hatvp.registre.business.email.ApplicationMailer;

/**
 * {@inheritDoc}
 */
@Component
public class EmailSender extends ApplicationMailer implements EmailSenderService {

	@Autowired
	protected EmailSender(JavaMailSender sender, TemplateEngine templateEngine) {
		super(sender, templateEngine);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @throws MessagingException
	 */
	@Override
	public void sendMailAsAutomateHatvp(final String subject, final String body, final String recipient) {
		super.prepareAndSendEmail(recipient, subject, body);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @throws MessagingException
	 */
	@Override
	public void sendMailAsAutomateHatvpWithLogo(final String subject, final String body, final String recipient) {
		super.prepareAndSendEmailWhithLogo(recipient, subject, body);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @throws MessagingException
	 */
	@Override
	public void sendMailAsAutomateHatvpWithLogoForBatch(final String subject, final Map<String, String> templateData, final String template, final String recipient)
			throws MessagingException {
		Context context = new Context();
		templateData.forEach((key, value) -> {
			context.setVariable(key, value);
		});
		context.setVariable("imageResourceName", this.imageResourceName);
		String mailContent = this.templateEngine.process(template, context);
		super.prepareAndSendEmailWhithLogoBatch(recipient, subject, mailContent, Logo.getLogo());
	}
}
