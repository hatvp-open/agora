/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.referentiel.strategy;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.batch.exceptions.SireneAndRnaFileFormatException;
import fr.hatvp.registre.batch.referentiel.pojos.ReferentielSireneLine;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielSireneEntity;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
import fr.hatvp.registre.persistence.repository.referenciel.ReferencielSirenRepository;

/**
 * Implémentation du traitement des lignes du ficheir CSV de mise à jour quotidienne de la base
 * SIRENE.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Component
@Qualifier("referentielSireneFileProcessor")
public class ReferentielSireneFileProcessor
    extends ReferentielFileProcessor
{
    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ReferentielSireneFileProcessor.class);

    /**
     * Repo pour la mise à jour du référentiel sirene en base de données.
     */
    @Autowired
    private ReferencielSirenRepository referencielSirenRepository;

    /**
     * Repo pour la mise à jour du référentiel organisation en base de données.
     */
    @Autowired
    private OrganisationRepository organisationRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public void flushRepository()
    {
        this.referencielSirenRepository.flush();
        this.organisationRepository.flush();
    }

    /**
     * {@inheritDoc}
     * @throws SireneAndRnaFileFormatException
     */
    @Override
    public boolean processLine(final String[] lineContent) throws SireneAndRnaFileFormatException
    {
        boolean ligneTraitee = false;

        // 1. Vérification du nombre de champs.
        if (lineContent.length == 118) {

            // Mapping des champs dans un pojo
            final ReferentielSireneLine referentielSireneLine = this
                    .mapArrayToReferentielSireneLine(lineContent);

            // 2. Vérification de la modification à appliquer d'après le champ vMaj. On ne traite
            // que les sieges d'où :
            /*
             * D'après la documentation :
             *
             * C Création d'établissement
             * E Suppression d'établissement
             * I Modification, état initial
             * F Modification, état final
             * D Entrée dans la diffusion
             * O Sortie de la diffusion
             */
            final String vMaj = referentielSireneLine.getvMaj();
            // Création ou entrée dans la diffusion ou mise à jour
            if ("C".equals(vMaj) || "D".equals(vMaj) || "F".equals(vMaj)) {
                // On ne traite que les sièges (niveau SIREN et pas SIRET donc).
                if ("1".equals(referentielSireneLine.getSiege())) {

                    // Mise à jour dans le référentiel sirene de l'application.
                    final ReferentielSireneEntity entity = this
                            .updateOrCreateEntryIntoSireneReferentiel(referentielSireneLine);

                    // Mise à jour dans le référentiel organisation de l'application (si
                    // applicable).
                    this.updateEntryFromOrganisation(entity);

                    ligneTraitee = true;
                }
            }
            // Sortie de la diffusion ou suppression : on traite tout pour gérer le cas du
            // changement de siège (l'établissement n'est plus le siège mais était dans notre
            // référentiel en tant que siège)
            else if ("E".equals(vMaj) || "O".equals(vMaj)) {
                this.deleteEntryFromSireneReferentiel(referentielSireneLine);
                ligneTraitee = true;
            }
            else {
                LOGGER.info("Ligne ignorée. vMaj=" + vMaj + "; siren="
                        + referentielSireneLine.getSiren() + "; nic="
                        + referentielSireneLine.getNic());
            }
        }
        else {
            throw new SireneAndRnaFileFormatException(
                    "Format du fichier de mise à jour SIRENE incorrect. Il devrait contenir 118 champs.");
        }

        return ligneTraitee;
    }

    /**
     * Écrit les champs d'une ligne du CSV de mise à jour de la base SIRENE vers un POJO
     * {@link ReferentielSireneLine}.
     * @param lineContent : contenu de la ligne CSV
     * @return le contenu de la ligne dans une instance de {@link ReferentielSireneLine}
     */
    protected ReferentielSireneLine mapArrayToReferentielSireneLine(final String[] lineContent)
    {
        final ReferentielSireneLine referentielSireneLine = new ReferentielSireneLine();

        referentielSireneLine.setAmIntrEn(lineContent[84]);
        referentielSireneLine.setDateMaj(lineContent[99]);
        referentielSireneLine.setdCrEn(lineContent[83]);
        referentielSireneLine.setL1Normalisee(lineContent[2]);
        referentielSireneLine.setL2Normalisee(lineContent[3]);
        referentielSireneLine.setL3Normalisee(lineContent[4]);
        referentielSireneLine.setL4Normalisee(lineContent[5]);
        referentielSireneLine.setL5Normalisee(lineContent[6]);
        referentielSireneLine.setL6Normalisee(lineContent[7]);
        referentielSireneLine.setL7Normalisee(lineContent[8]);
        referentielSireneLine.setmAdresse(lineContent[105]);
        referentielSireneLine.setmNicSiege(lineContent[112]);
        referentielSireneLine.setmNomEn(lineContent[110]);
        referentielSireneLine.setNic(lineContent[1]);
        referentielSireneLine.setNomEnLong(lineContent[60]);
        referentielSireneLine.setRna(lineContent[65]);
        referentielSireneLine.setSiege(lineContent[35]);
        referentielSireneLine.setSiren(lineContent[0]);
        referentielSireneLine.setvMaj(lineContent[95]);

        return referentielSireneLine;
    }

    /**
     * Mise à jour ou création d'une nouvelle entrée dans le référentiel d'après une ligne du
     * fichier CSV.
     * @param ligne du fichier CSV à traiter
     * @return l'entité persistée dans le référentiel SIRENE
     */
    protected ReferentielSireneEntity updateOrCreateEntryIntoSireneReferentiel(
            final ReferentielSireneLine line)
    {
        ReferentielSireneEntity entity = null;

        // On essaie de récupérer l'entrée de la base référentiel.
        entity = this.referencielSirenRepository.findFirstBySiren(line.getSiren());

        if (entity == null) {
            entity = new ReferentielSireneEntity();
            LOGGER.info("Un nouveau siren va être ajouté au référentiel sirene. siren="
                    + line.getSiren());
        }
        else {
            LOGGER.info("Un siren va être mis à jour dans le référentiel sirene. siren="
                    + line.getSiren());
        }

        // Enregistrement dans le référentiel de l'application.
        this.mapSireneLineToEntity(line, entity);
        this.referencielSirenRepository.save(entity);

        return entity;
    }

    /**
     * Recopie les données de l'entity sirene passée en paramètre vers la table organisation.
     * La copie a lieu seulement si l'organisation n'est pas inscrite au registre (pas d'espace
     * organisation).
     * @param entity données du SIREN à persister
     */
    protected void updateEntryFromOrganisation(final ReferentielSireneEntity entity)
    {
        // Récupération de l'organisation si :
        // - elle existe
        // - provient du référentiel SIREN (TypeIdentifiantNationalEnum.SIREN)
        // - n'a pas d'espace organisation non refusé
        final Optional<OrganisationEntity> organisation = this.organisationRepository
                .findByEspaceOrganisationIsNullAndSirenAndOriginNationalIdSiren(entity.getSiren());

        if (organisation.isPresent()) {
            final OrganisationEntity organisationEntity = organisation.get();

            organisationEntity.setAdresse(entity.getAdresse());
            organisationEntity.setCodePostal(entity.getCodePostal());
            organisationEntity.setDenomination(entity.getDenomination());
            organisationEntity.setPays(entity.getPays());
            organisationEntity.setRna(entity.getRna());
            organisationEntity.setVille(entity.getVille());

            this.organisationRepository.save(organisationEntity);

            LOGGER.info("Organisation mise à jour dans la table organisation. siren="
                    + organisationEntity.getSiren());
        }
    }

    /**
     * Fait la suppression d'un établissement (SIREN + NIC) du référentiel SIRENE s'il y est
     * présent.
     * @param line : la ligne du fichier CSV contenant les informations de l'établissement à
     * supprimer.
     */
    protected void deleteEntryFromSireneReferentiel(final ReferentielSireneLine line)
    {
        // Suppression de l'établissement (SIREN et NIC) s'il existe.
        final Long res = this.referencielSirenRepository.deleteBySirenAndNic(line.getSiren(),
                line.getNic());
        if (res.longValue() > 0) {
            LOGGER.info("Établissement supprimé du référentiel sirene. siren=" + line.getSiren()
                    + "; nic=" + line.getNic());
        }
        else {
            LOGGER.warn("Établissement introuvable pour suppression. siren=" + line.getSiren()
                    + "; nic=" + line.getNic());
        }
    }

    /**
     * Renseigne les champs de l'entité à persiter avec les champs de la ligne du CSV.
     * @param line
     * @param entity
     */
    protected void mapSireneLineToEntity(final ReferentielSireneLine line,
            final ReferentielSireneEntity entity)
    {
        final String dateCreationEntreprise = StringUtils.trim(line.getdCrEn());
        final Date dateDernierTraitement = Calendar.getInstance().getTime();
        final String dateEntreeEntreprise = StringUtils.trim(line.getAmIntrEn());
        final String dateMajEntreprise = StringUtils.trim(line.getDateMaj());
        final String denomination = this.getDenominationFromLine(line);
        final String nic = StringUtils.trim(line.getNic());
        final String pays = StringUtils.upperCase(StringUtils.trim(line.getL7Normalisee()));
        final String rna = StringUtils.trim(line.getRna());
        final String siren = StringUtils.trim(line.getSiren());

        entity.setDateCreationEntreprise(dateCreationEntreprise);
        entity.setDateDernierTraitement(dateDernierTraitement);
        entity.setDateEntreeEntreprise(dateEntreeEntreprise);
        entity.setDateMajEntreprise(dateMajEntreprise);
        entity.setDenomination(denomination);
        entity.setNic(nic);
        entity.setPays(pays);
        entity.setRna(StringUtils.isEmpty(rna) ? null : rna);
        entity.setSiren(siren);

        this.setAdresseVilleCpFromLine(line, entity);
    }

    /**
     * Retraite le champ NomEnLong pour déterminer la dénomination de l'entreprise.
     * @param line : ligne du CSV de mise à jour de la base SIRENE
     * @return la dénomination retraitée
     */
    protected String getDenominationFromLine(final ReferentielSireneLine line)
    {
        String denomination = line.getNomEnLong();
        denomination = StringUtils.replace(denomination, "/", " ");
        denomination = StringUtils.replace(denomination, "*", " ");
        denomination = StringUtils.trim(denomination);
        denomination = StringUtils.upperCase(denomination);
        return denomination;
    }

    /**
     * Calcul et positionne les champs ville, code postal et adresse selon que le champ
     * L6_NORMALISEE est bien normalisé ou non
     * @param line : ligne du CSV de mise à jour de la base SIRENE
     * @param entity : entité du referentiel sirene à mettre à jour
     */
    protected void setAdresseVilleCpFromLine(final ReferentielSireneLine line,
            final ReferentielSireneEntity entity)
    {
        final String l1Normalisee = line.getL1Normalisee();
        final String l2Normalisee = line.getL2Normalisee();
        final String l3Normalisee = line.getL3Normalisee();
        final String l4Normalisee = line.getL4Normalisee();
        final String l5Normalisee = line.getL5Normalisee();
        final String l6Normalisee = line.getL6Normalisee();

        String adresse = "";
        String codePostal = "";
        String ville = "";

        // 1. On détermine si le champ L6_NORMALISEE respecte le format code postal (5 chiffres)
        // suivi d'un espace et de la ville
        final Pattern pattern = Pattern.compile("[0-9]{5}[ ].*");
        if (pattern.matcher(l6Normalisee).matches()) {
            adresse = StringUtils.join(l1Normalisee, "\r\n", l2Normalisee, "\r\n", l3Normalisee,
                    "\r\n", l4Normalisee, "\r\n", l5Normalisee);
            codePostal = StringUtils.substring(l6Normalisee, 0, 5);
            ville = StringUtils.substring(l6Normalisee, 6);
        }
        else {
            adresse = StringUtils.join(l1Normalisee, "\r\n", l2Normalisee, "\r\n", l3Normalisee,
                    "\r\n", l4Normalisee, "\r\n", l5Normalisee, "\r\n", l6Normalisee);
        }

        // 2. Nettoyage du champ adresse :
        // On met en majuscules
        adresse = StringUtils.upperCase(adresse);
        // On traite les retours à la ligne immédiatement suivis d'espaces
        adresse = StringUtils.replacePattern(adresse, "[\r\n]+[ ]+", "\r\n");
        // On retire les retours à la ligne multiples dans le champ
        adresse = StringUtils.replacePattern(adresse, "[\r\n]+", "\r\n");
        // Supression des retours à la ligne en début et en fin d'adresse
        adresse = StringUtils.strip(adresse, "\r\n");
        // On retire les espaces multiples dans le champ adresse
        adresse = StringUtils.replacePattern(adresse, "[ ]+", " ");
        // On retire les espaces en début et fin
        adresse = StringUtils.trim(adresse);

        // 3. Nettoyage du champ ville :
        ville = StringUtils.upperCase(ville);
        ville = StringUtils.trim(ville);

        // 4. Enregistrement des champs calculés dans l'entité à persister
        entity.setAdresse(adresse);
        entity.setCodePostal(codePostal);
        entity.setVille(ville);
    }
}
