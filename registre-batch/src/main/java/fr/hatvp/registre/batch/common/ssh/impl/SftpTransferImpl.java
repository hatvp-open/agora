/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.common.ssh.impl;

import org.springframework.stereotype.Component;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import fr.hatvp.registre.batch.common.ssh.service.SftpTransferService;

/**
 * 
 * Implémentation du service de transfert SFTP.
 * Encapsule l'implémentation JSch.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Component
public class SftpTransferImpl implements SftpTransferService
{

    /**
     * Le Jsch encapsulé pour la connexion ssh.
     */
    final private JSch jsch = new JSch();
    
    /**
     * L'objet de gestion de la session ssh.
     */
    private Session session = null;
    
    /**
     * L'objet de gestion des transferts SFTP.
     */
    private ChannelSftp sftpChannel = null;
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void connect(final String sshHost, final String sshUser, final String sshPassword, final int sshPort) throws JSchException {
        this.disconnect();
        this.session = this.jsch.getSession(sshUser, sshHost, sshPort);
        this.session.setPassword(sshPassword);
        this.session.setConfig("StrictHostKeyChecking", "no");
        this.session.connect();
        this.sftpChannel = (ChannelSftp) this.session.openChannel("sftp");
        this.sftpChannel.connect();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void disconnect(){
        if(sftpChannel != null && sftpChannel.isConnected()) {
            sftpChannel.exit();
        }
        if(session != null && session.isConnected()) {
            session.disconnect();
        }
        this.sftpChannel = null;
        this.session = null;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void sendFileToPath(final String localFilePath, final String distFilePath) throws SftpException {
        this.sftpChannel.put(localFilePath, distFilePath, ChannelSftp.OVERWRITE);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void removeDistantFile(final String distFilePath) throws SftpException {
        try {
            this.sftpChannel.rm(distFilePath);
        }
        catch(SftpException e) {
            // Si le fichier n'existe pas on ne lance pas d'exception
            if(e.id != ChannelSftp.SSH_FX_NO_SUCH_FILE) {
                throw e;
            }
        }
    }
}
