/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.qualif;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.QualificationObjetService;

/**
 * Cette classe offre les fonctionnalités nécessaires à la production du fichier open data
 * des déclarations faites dans le registre vers le site internet institutionnel de la HATVP.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Component
public class RepriseQualifBatch
{
    /** Service de publication. */
    @Autowired
    private QualificationObjetService qualificationObjetService;

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RepriseQualifBatch.class);

    /**
     * Procédure qui lance le processus et traite les exceptions.
     */
    public void launchProcess()
    {
        try {
            this.runPublicationProcess();
        }
        catch (final RuntimeException exception) {
            LOGGER.error("Erreur imprévue survenue pendant l'exécution du programme.");
            LOGGER.error(exception.getMessage(), exception);
        }
    }

    /**
     *
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    protected void runPublicationProcess()
    {
        qualificationObjetService.repriseQualif();
    }
}
