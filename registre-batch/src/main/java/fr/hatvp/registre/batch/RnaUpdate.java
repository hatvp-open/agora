/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch;

import fr.hatvp.registre.batch.common.smtp.service.EmailSenderService;
import fr.hatvp.registre.batch.exceptions.SireneAndRnaFileFormatException;
import fr.hatvp.registre.batch.exceptions.SireneUpdateOverdueException;
import fr.hatvp.registre.batch.referentiel.ReferentielRnaBatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.lang.UsesJava8;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;

/**
 * Batch du referentiel du registre.
 *
 * Le programme télécharge également mensuellement le fichier RNA et l'intègre à la table
 * referentiel_rna.
 */
@UsesJava8
public class RnaUpdate {

    /** Spring conf files location. */
    private static final String[] springContext = {"classpath:applicationContext.xml"};

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RnaUpdate.class);

    /** Lancement du batch. */
    public static void main(final String[] args) {
        // Loads Spring context
        final AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(springContext);
        ctx.registerShutdownHook();

        // Lancement du batch de traitement de mise à jour du référentiel RNA
        final ReferentielRnaBatch referentielRnaBatch = ctx
            .getBean(ReferentielRnaBatch.class);
        final EmailSenderService emailSenderService = ctx.getBean(EmailSenderService.class);
        try {
            referentielRnaBatch.runReferentielRnaBatch();
            // FIXME utiliser adresse email paramétrée dans batch.sirene.alert.recipient
            // FIXME utiliser le mailer de l'application registre
        } catch (final IOException | SireneAndRnaFileFormatException | ParseException e) {
            LOGGER.error("Erreur imprévue survenue pendant l'exécution du programme : {}", e);
            try {
                emailSenderService.sendMailAsAutomateHatvp(
                    "REPERTOIRE: erreur de mise à jour du référentiel SIRENE",
                    "Veuillez trouvez la trace ci-dessous : \r\n"
                        + Arrays.toString(e.getStackTrace()),
                    "admin.si@hatvp.fr");
            } catch (final MessagingException e1) {
                LOGGER.error("Erreur lors de l'envoi de l'email : {}", e1);
            }
            
        } catch (final SireneUpdateOverdueException e) {
            LOGGER.error("Il semble que le référentiel RNA ne soit plus à jour : {}", e);
            try {
                emailSenderService.sendMailAsAutomateHatvp(
                    "REPERTOIRE: le référentiel SIRENE n'est plus à jour", e.getMessage(),
                    "admin.si@hatvp.fr");
            } catch (final MessagingException e1) {
                LOGGER.error("Erreur lors de l'envoi de l'email : {}", e1);
            }
        }

        // Close Spring context
        ctx.close();
    }

}
