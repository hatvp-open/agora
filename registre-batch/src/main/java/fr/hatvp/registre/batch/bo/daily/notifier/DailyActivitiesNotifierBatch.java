/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.bo.daily.notifier;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.batch.common.smtp.service.EmailSenderService;
import fr.hatvp.registre.business.DemandeChangementMandantService;
import fr.hatvp.registre.business.DemandeOrganisationService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.PublicationService;
import fr.hatvp.registre.business.backoffice.UtilisateurBoService;
import fr.hatvp.registre.commons.lists.StatutDemandeEnum;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;

@Component
public class DailyActivitiesNotifierBatch {

	private static final Logger LOGGER = LoggerFactory.getLogger(DailyActivitiesNotifierBatch.class);
	private static final String DD_MM_YYYY = "dd-MM-yyyy";
	@Autowired
	private EmailSenderService emailSenderService;

	@Autowired
	private UtilisateurBoService utilisateurBoService;

	@Autowired
	private DemandeOrganisationService demandeOrganisationService;

	@Autowired
	private EspaceOrganisationService espaceOrganisationService;

	@Autowired
	private DemandeChangementMandantService demandeChangementMandantService;

	@Autowired
	private PublicationService publicationService;

	@Value("${batch.alert.recipient}")
	private String destinataireAlerte;

	public void launchProcess() {
		try {
			this.runNotificationProcess();
		} catch (final RuntimeException exception) {
			LOGGER.error("Erreur imprévue survenue pendant l'exécution du programme.");
			LOGGER.error(exception.getMessage(), exception);
			try {
				this.emailSenderService.sendMailAsAutomateHatvp("AGORA : erreur critique pendant la publication des déclarations",
						"Veuillez trouvez la trace ci-dessous : \r\n\r\n" + ExceptionUtils.getMessage(exception) + "\r\n\r\n" + ExceptionUtils.getStackTrace(exception),
						this.destinataireAlerte);
			} catch (final MessagingException e) {
				LOGGER.error("Erreur lors de l'envoi de l'email.");
				LOGGER.error(e.getMessage(), e);
			}
		}

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private void runNotificationProcess() {

		if (LocalDate.now().getDayOfWeek().equals(DayOfWeek.SATURDAY) || LocalDate.now().getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
			LOGGER.info("Pas de notification quotidienne, c'est WEEK END : {}" , LocalDateTime.now());
			return;
		}

		List<String> mails = this.utilisateurBoService.getMailOfUsersToNotify();

		Map<String, String> data = new HashMap<>();
		data.put("creationEC", this.espaceOrganisationService.getNombreDemandesParStatut(StatutEspaceEnum.NOUVEAU).toString());
		data.put("complementEC", this.espaceOrganisationService.getNombreDemandesParStatut(StatutEspaceEnum.COMPLEMENT_RECU).toString());
		data.put("demandeMandat", this.demandeChangementMandantService.getNombreDemandesParStatut(StatutDemandeEnum.NOUVELLE).toString());
		data.put("complementMandat", this.demandeChangementMandantService.getNombreDemandesParStatut(StatutDemandeEnum.COMPLEMENT_RECU).toString());
		data.put("demandeOrga", this.demandeOrganisationService.getNombreDemandesStatutNouvelle().toString());

		Map<String, String> count = this.publicationService.countEspaceInscritWithDifferenceThroughTime();
		data.putAll(count);

		data.put("date", LocalDate.now().format(DateTimeFormatter.ofPattern(DD_MM_YYYY)));
		data.put("heure", LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));

		data.put("minusOneWeek", LocalDate.now().minusDays(7).format(DateTimeFormatter.ofPattern(DD_MM_YYYY)));
		data.put("minusOneMonth", LocalDate.now().minusMonths(1).format(DateTimeFormatter.ofPattern(DD_MM_YYYY)));

		data.put("espace", String.valueOf(this.espaceOrganisationService.countNombreEspace()));

		mails.forEach(mail -> {
			try {
				this.emailSenderService.sendMailAsAutomateHatvpWithLogoForBatch("État des activités", data, "email/etat_activites", mail);
			} catch (MessagingException e) {
				LOGGER.error("Erreur lors de l'envoi de l'email : {}", e);
			}
		});

	}

}
