/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.referentiel;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.batch.common.web.service.FileDownloaderClientProviderService;
import fr.hatvp.registre.batch.exceptions.SireneAndRnaFileFormatException;
import fr.hatvp.registre.batch.exceptions.SireneUpdateOverdueException;
import fr.hatvp.registre.batch.referentiel.reader.ZipCsvReader;
import fr.hatvp.registre.batch.referentiel.strategy.ReferentielSireneFileProcessor;
import fr.hatvp.registre.persistence.entity.batch.BatchSireneEntity;
import fr.hatvp.registre.persistence.repository.batch.BatchSireneRepository;

/**
 * 
 * Cette classe prend en charge l'exécution du processus de mise à jour quotidien du référentiel
 * SIRENE.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Component
public class ReferentielSireneBatch
{
    /**
     * Repository qui permet de conserver en base de données l'historique des fichiers traités.
     */
    @Autowired
    private BatchSireneRepository batchSireneRepository;

    /**
     * Client HTTP qui permet de télécharger les fichiers.
     */
    @Autowired
    private FileDownloaderClientProviderService fileDownloaderClientProviderService;

    /**
     * Objet qui permet la lecture et le traitement des fichiers téléchargés.
     */
    @Autowired
    private ZipCsvReader<ReferentielSireneFileProcessor> sireneZipCsvReader;

    /**
     * Délai en jours au delà duquel on considère être en retard des mises à jours.
     */
    @Value("${batch.sirene.overdue.delay.days}")
    private int delaiRetardMiseAJour;

    /**
     * Destinataire des emails d'alerte.
     */
    @Value("${batch.alert.recipient}")
    private String sendEmailTo;

    /**
     * Motif pour déterminer les url des ficheirs à télécharger.
     */
    @Value("${batch.sirene.download.url.pattern}")
    private String urlPattern;

    /**
     * Emplacement du téléchargement des fichiers.
     */
    @Value("${batch.sirene.download.path}")
    private String downloadPath;

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReferentielSireneBatch.class);

    /**
     * Procédure qui exécute le traitement des mises à jour.
     * @throws IOException
     * @throws SireneUpdateOverdueException
     * @throws SireneAndRnaFileFormatException
     */
    public void runReferentielSireneBatch()
        throws IOException,
        SireneUpdateOverdueException,
        SireneAndRnaFileFormatException,
        ParseException
    {
        boolean updated = false;
        int nbJoursATraiter = 0;

        // 1. Récupération des informations du dernier fichier traité : utile pour déterminer la
        // liste des futurs fichiers à traiter
        final BatchSireneEntity batchSireneEntity = this.batchSireneRepository
                .findTopByOrderByDateTraitementDesc();

        // 2. On produit et parcours la liste des URL pour le téléchargement des mises à jour
        // quotidiennes. 1 URL par jour. On ignore a priori si le fichier sera présent ou non sur
        // le serveur (il existe des jours sans mise à jour).
        final SortedMap<String, String> urlATraiter = this.determinerUrlATraiter(batchSireneEntity);

        // Le nombre d'éléments de la map correspond au nombre de jours à traiter
        nbJoursATraiter = urlATraiter.size();

        // Téléchargement et traitement des fichiers
        for (final SortedMap.Entry<String, String> entry : urlATraiter.entrySet()) {
            // Téléchargement du fichier
            final String fileName = this.fileDownloaderClientProviderService
                    .downloadToLocalFile(entry.getValue(), this.downloadPath);

            // Si fichier téléchargé
            if (fileName != null) {
                final String fullPathFileName = this.downloadPath + "/" + fileName;

                LOGGER.info("Début du traitement de : {}" , fullPathFileName);

                // On traite le fichier téléchargé
                final int updatedRowsCount = this.sireneZipCsvReader
                        .processCsvFilesFromArchive(fullPathFileName);

                // On enregistre en BDD les informations de l'archive qui vient d'être traitée
                final BatchSireneEntity newBatchSireneEntity = new BatchSireneEntity();
                newBatchSireneEntity.setSequenceFichier(entry.getKey());
                newBatchSireneEntity.setUrlFichier(entry.getValue());
                newBatchSireneEntity.setUpdatedRowsCount(updatedRowsCount);
                this.batchSireneRepository.save(newBatchSireneEntity);

                updated = true;
            }
            else {
                LOGGER.warn("Le fichier n'a pas pu être téléchargé : {}" , entry.getValue());
            }
        }

        // Si aucun traitement n'a eu lieu depuis plus de 5 jours, on envoie un email d'alerte.
        if (!updated && (nbJoursATraiter > this.delaiRetardMiseAJour)) {
            throw new SireneUpdateOverdueException(
                    "Aucune mise à jour traitée depuis " + nbJoursATraiter + " jours.");
        }
    }

    /**
     * Procédure qui détermine les fichiers à télécharger pour le traitement d'après le numéro du
     * dernier fichier traité.
     * @return la liste des url à traiter. 1 par jour. Clé : numéro de séquence du fichier au format
     * YYYYDDD ; valeur : l'url http du fichier.
     */
    private SortedMap<String, String> determinerUrlATraiter(
            final BatchSireneEntity batchSireneEntity)
    {
        final SortedMap<String, String> urlATraiter = new TreeMap<>();

        // 1. On détermine l'année et le jour du dernier fichier traité d'après sa séquence au
        // format YYYYDDD
        final String sequenceFichier = batchSireneEntity.getSequenceFichier();
        final String anneeSequence = StringUtils.substring(sequenceFichier, 0, 4);
        final String jourSequence = StringUtils.substring(sequenceFichier, 4);

        // 2. On détermine les bornes : début au lendemain du dernier fichier traité. Fin à la date
        // du jour.
        final LocalDate start = LocalDate
                .ofYearDay(Integer.valueOf(anneeSequence), Integer.valueOf(jourSequence))
                .plusDays(1);
        final LocalDate end = LocalDate.now();

        // 2. Parcours de tous les jours depuis le dernier fichier traité + 1, jusqu'au jour courant
        for (LocalDate date = start; date.isBefore(end.plusDays(1)); date = date.plusDays(1)) {
            // 3. On génère le nom du fichier à traiter et on l'ajoute à la liste
            // Séquence au format YYYYDDD
            final String numeroSequence = String.valueOf(date.getYear())
                    + StringUtils.leftPad(String.valueOf(date.getDayOfYear()), 3, "0");
            urlATraiter.put(numeroSequence, this.urlPattern.replace("[SEQ]", numeroSequence));
        }

        return urlATraiter;
    }

}
