/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.lang.UsesJava8;

import fr.hatvp.registre.batch.bo.daily.notifier.DailyActivitiesNotifierBatch;

/**
 * Classe principale contenant le main du batch de notification journalière de l'état des activités
 *
 * Envoi un mail de l'état des activités aux utilisateurs BO avec le rôle 'ETAT_ACTIVITES'
 *
 */
@UsesJava8
public class DailyActivitiesNotifier {
	/** Spring conf files location. */
	private static final String[] springContext = { "classpath:applicationContext.xml" };

	/** Logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DailyActivitiesNotifier.class);

	/** Lancement du batch. */
	public static void main(final String[] args) {

		LOGGER.info("Chargement du contexte Spring");
		try (final AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(springContext)) {
			final DailyActivitiesNotifierBatch dailyActivitiesNotifierBatch = ctx.getBean(DailyActivitiesNotifierBatch.class);
			LOGGER.info("Lancement de la notification quotidienne");
			dailyActivitiesNotifierBatch.launchProcess();
			LOGGER.info("Fin de la notification quotidienne");
		}
	}

}
