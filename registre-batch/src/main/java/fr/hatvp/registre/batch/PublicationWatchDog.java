/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.lang.UsesJava8;

import fr.hatvp.registre.batch.watchdog.PublicationWatchDogBatch;

/**
 * Classe principale contenant le main du batch de gestion de l'absence de publication de fiche REG-518
 *
 * Avertir les CO des organisations dont aucune publication d'activités de RI n'a été constatée 3 mois après la date de cloture de l'EC.
 *
 */
@UsesJava8
public class PublicationWatchDog {
	/** Spring conf files location. */
	private static final String[] springContext = { "classpath:applicationContext.xml" };

	/** Logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PublicationWatchDog.class);

	/** Lancement du batch. */
	public static void main(final String[] args) {
		String date = null;
		if (args.length > 0) {
			date = args[0];
		}
		LOGGER.info("Chargement du contexte Spring");
		try (final AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(springContext)) {
			final PublicationWatchDogBatch publicationWatchDogBatch = ctx.getBean(PublicationWatchDogBatch.class);
			LOGGER.info("Démarrage du watchdog");
			publicationWatchDogBatch.launchProcess(date);
			LOGGER.info("Fin du watchdog");
		}
	}

}
