/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.common.ssh.service;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

/**
 * 
 * Description de la classe.
 * 
 * @version $Revision$ $Date${0xD}
 */
public interface SftpTransferService
{

    /**
     * Ouvre une connexion SFTP au serveur.
     * 
     * @param sshHost adresse du serveur.
     * @param sshUser utilisateur ssh.
     * @param sshPassword mot de passe ssh.
     * @param sshPort port ssh.
     * @throws JSchException
     */
    void connect(String sshHost, String sshUser, String sshPassword, int sshPort) throws JSchException;
    
    /**
     * Ferme la connexion SFTP au serveur.
     */
    void disconnect();
    
    /**
     * Écrit un fichier vers le serveur distant.
     * @param file le fichier à écrire.
     * @param fullDistPath le dossier où écrire le fichier
     * @throws SftpException
     */
    void sendFileToPath(String localFilePath, String distFilePath) throws SftpException;
    
    /**
     * Supprime un fichier sur le serveur distant.
     * @param distFilePath le chemin du fichier à supprimer.
     * @throws SftpException
     */
    void removeDistantFile(String distFilePath) throws SftpException;
    
}
