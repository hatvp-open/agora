/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.referentiel.strategy;

import fr.hatvp.registre.batch.exceptions.SireneAndRnaFileFormatException;

import java.text.ParseException;

/**
 * Classe générique de traitement des lignes d'un fichier CSV.
 * 
 * @version $Revision$ $Date${0xD}
 */
public abstract class ReferentielFileProcessor
{
    /**
     * Fonction qui traite une ligne d'un fichier CSV représentée sous forme de String.
     * @param lineContent : le contenu d'une ligne du fichier CSV.
     * @return true si la ligne a été persistée en base de données.
     * @throws SireneAndRnaFileFormatException
     */
    public abstract boolean processLine(String[] lineContent) throws SireneAndRnaFileFormatException, ParseException;

    /**
     * Permet de faire le flush des informations persistées à intervalles réguliers.
     * Doit être une simple ré-encapsulation du flush() de JpaRepository.
     */
    public abstract void flushRepository();

}
