/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.common.smtp.service;

import java.util.Map;

import javax.mail.MessagingException;

/**
 * Stuff we use to send e-mails to people (Wow)!
 *
 *
 */
public interface EmailSenderService {

	/**
	 * Sends an email using the "automate HATVP" e-mail account
	 *
	 * @param subject
	 * @param body
	 * @param recipient
	 * @throws MessagingException
	 */
	public void sendMailAsAutomateHatvp(String subject, String body, String recipient) throws MessagingException;

	/**
	 * Sends an email using the "automate HATVP" e-mail account with Logi
	 *
	 * @param subject
	 * @param body
	 * @param recipient
	 * @throws MessagingException
	 */
	public void sendMailAsAutomateHatvpWithLogo(String subject, String body, String recipient) throws MessagingException;

	/**
	 * Sends a publication reminder email using the "automate HATVP" e-mail account with Logo
	 *
	 * @param subject
	 * @param body
	 * @param recipient
	 * @throws MessagingException
	 */
	void sendMailAsAutomateHatvpWithLogoForBatch(String subject, Map<String, String> templateData, String template, String recipient) throws MessagingException;
}
