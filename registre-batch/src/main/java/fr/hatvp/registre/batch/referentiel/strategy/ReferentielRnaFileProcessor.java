/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.referentiel.strategy;

import fr.hatvp.registre.batch.exceptions.SireneAndRnaFileFormatException;
import fr.hatvp.registre.batch.referentiel.pojos.ReferentielRnaLine;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielRnaEntity;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
import fr.hatvp.registre.persistence.repository.referenciel.ReferencielRnaRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * Implémentation du traitement des lignes du fichr CSV de mise à jour quotidienne de la base
 * RNA.
 *
 */

@Component
@Qualifier("referentielRnaFileProcessor")
public class ReferentielRnaFileProcessor extends ReferentielFileProcessor{

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ReferentielRnaFileProcessor.class);

    /**
     * Repo pour la mise à jour du référentiel rna en base de données.
     */
    @Autowired
    private ReferencielRnaRepository referencielRnaRepository;

    /**
     * Repo pour la mise à jour du référentiel organisation en base de données.
     */
    @Autowired
    private OrganisationRepository organisationRepository;
    /**
     * {@inheritDoc}
     */
    @Override
    public void flushRepository()
    {
        this.referencielRnaRepository.flush();
        this.organisationRepository.flush();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean processLine(String[] lineContent)
    throws SireneAndRnaFileFormatException,
        ParseException
    {
        boolean ligneTraitee = false;

        // 1. Vérification du nombre de champs.
        if (lineContent.length == 41) {

            if(!lineContent[0].isEmpty()) {
                // Mapping des champs dans un pojo
                final ReferentielRnaLine ReferentielRnaLine = this
                    .mapArrayToReferentielRnaLine(lineContent);

                // 2. Vérification de la modification à appliquer d'après le champ position. On ne traite
                // que les sieges d'où :
            /*
             * D'après la documentation :
             *
             * S supprimé
             * D dissoute
             * A active
             */
                final String position = ReferentielRnaLine.getPosition();
                // Création ou entrée dans la diffusion ou mise à jour
                if ("A".equals(position)) {

                    // Mise à jour dans le référentiel rna de l'application.
                    final ReferentielRnaEntity entity = this
                        .updateOrCreateEntryIntoRnaReferentiel(ReferentielRnaLine);

                    // Mise à jour dans le référentiel organisation de l'application (si
                    // applicable).
                    this.updateEntryFromOrganisation(entity);

                    ligneTraitee = true;
                }
                // Sortie de la diffusion ou suppression : on traite tout pour gérer le cas du
                // changement de siège (l'établissement n'est plus le siège mais était dans notre
                // référentiel en tant que siège)
                else if ("S".equals(position) || "D".equals(position) || "".equals(position)) {
                    this.deleteEntryFromRnaReferentiel(ReferentielRnaLine);
                    ligneTraitee = true;
                } else {
                    LOGGER.info("Ligne ignorée. position={}; rna= {}" , position, ReferentielRnaLine.getRna());
                }
            }
        }
        else {
            throw new SireneAndRnaFileFormatException(
                "Format du fichier de mise à jour Rna incorrect.");
        }

        return ligneTraitee;
    }

    /**
     * Écrit les champs d'une ligne du CSV de mise à jour de la base RNA vers un POJO
     * {@link ReferentielRnaLine}.
     * @param lineContent : contenu de la ligne CSV
     * @return le contenu de la ligne dans une instance de {@link ReferentielRnaLine}
     */

    protected ReferentielRnaLine mapArrayToReferentielRnaLine(final String[] lineContent)
    {
        final ReferentielRnaLine referentielRnaLine = new ReferentielRnaLine();

        referentielRnaLine.setRna(lineContent[0]);
        referentielRnaLine.setSiren(lineContent[2]);
        referentielRnaLine.setDateCreation(lineContent[5]);
        referentielRnaLine.setDateDeclaration(lineContent[6]);
        referentielRnaLine.setDatePublication(lineContent[7]);
        referentielRnaLine.setTitre(lineContent[11]);
        referentielRnaLine.setObjet(lineContent[13]);
        referentielRnaLine.setNumVoie(lineContent[17]);
        referentielRnaLine.setTypeVoie(lineContent[19]);
        referentielRnaLine.setLibVoie(lineContent[20]);
        referentielRnaLine.setCP(lineContent[23]);
        referentielRnaLine.setVille(lineContent[24]);
        referentielRnaLine.setPays(lineContent[32]);
        referentielRnaLine.setSiteWeb(lineContent[35]);
        referentielRnaLine.setPosition(lineContent[39]);
        referentielRnaLine.setMajTime(lineContent[40]);

        return referentielRnaLine;
    }

    /**
     * Mise à jour ou création d'une nouvelle entrée dans le référentiel d'après une ligne du
     * fichier CSV.
     * @param line du fichier CSV à traiter
     * @return l'entité persistée dans le référentiel RNA
     */
    protected ReferentielRnaEntity updateOrCreateEntryIntoRnaReferentiel(final ReferentielRnaLine line)
    throws ParseException
    {
        ReferentielRnaEntity entity = null;

        // On essaie de récupérer l'entrée de la base référentiel.
        entity = this.referencielRnaRepository.findFirstByRna(line.getRna());

        if (entity == null) {
            entity = new ReferentielRnaEntity();
            LOGGER.info("Un nouveau rna va être ajouté au référentiel rna. rna="
                + line.getRna());
        }
        else {
            LOGGER.info("Un rna va être mis à jour dans le référentiel rna. rna="
                + line.getRna());
        }

        // Enregistrement dans le référentiel de l'application.
        this.mapRnaLineToEntity(line, entity);
        this.referencielRnaRepository.save(entity);

        return entity;
    }

    /**
     * Renseigne les champs de l'entité à persiter avec les champs de la ligne du CSV.
     * @param line
     * @param entity
     */
    protected void mapRnaLineToEntity(final ReferentielRnaLine line, final ReferentielRnaEntity entity)
    throws ParseException
    {
        final String rna = StringUtils.trim(line.getRna());
        final String siren = StringUtils.trim(line.getSiren());
        final String nic = StringUtils.trim(line.getNic());
        final Date dateCreation = this.stringToDate(line.getDateCreation());
        final Date dateDeclaration = this.stringToDate(line.getDateDeclaration());
        final Date datePublication = this.stringToDate(line.getDatePublication());
        final String objet = StringUtils.trim(line.getObjet());
        final String siteWeb = StringUtils.trim(line.getSiteWeb());
        final String majTime = StringUtils.trim(line.getMajTime());
        final String titre = StringUtils.trim(line.getTitre());
        final String pays = StringUtils.trim(line.getPays());
        final String ville = StringUtils.trim(line.getVille());


        entity.setRna(rna);
        entity.setSiren(StringUtils.isEmpty(siren) ? null : siren);
        entity.setNic(nic);
        entity.setDateCreation(dateCreation);
        entity.setDateDeclaration(dateDeclaration);
    	entity.setDatePublication(datePublication);
        entity.setObjet(objet);
        entity.setSiteWeb(siteWeb);
        entity.setMajTime(majTime);

        entity.setVille(ville);
        entity.setDenomination(titre);
        entity.setPays(pays);
        this.setAdresseVilleCpFromLine(line, entity);
    }

    /**
     * Calcul et positionne les champs code postal et adresse selon que les champs
     * adrs_numvoie, adrs_typevoie, adrs_libvoie et adrs_codepostale soit bien référencé
     *
     * @param line : ligne du CSV de mise à jour de la base SIRENE
     * @param entity : entité du referentiel sirene à mettre à jour
     */
    protected void setAdresseVilleCpFromLine(final ReferentielRnaLine line,
                                             final ReferentielRnaEntity entity)
    {
        final String numVoie = line.getNumVoie();
        final String libVoie = line.getLibVoie();
        final String typeVoie = line.getTypeVoie();
        final String codePostalBrut = line.getCP();


        String adresse;
        String codePostal;

        // 1. On concatène les champs numvoie, typevoie et libvoie pour former une adresse
        adresse = numVoie+" "+typeVoie+" "+libVoie;

        // 2. On vérifie que le champs codepostal soit correctement renseigné et on rajoute
        // un 0 sur les codes postaux à 4 chiffres
        if(codePostalBrut.length()==5){
            codePostal = codePostalBrut;
        }else if(codePostalBrut.length() == 4){
            codePostal = "0"+codePostalBrut;
        }else{
            codePostal="";
        }


        // 3. Nettoyage du champ adresse :
        // On met en majuscules
        adresse = StringUtils.upperCase(adresse);
        // On traite les retours à la ligne immédiatement suivis d'espaces
        adresse = StringUtils.replacePattern(adresse, "[\r\n]+[ ]+", "\r\n");
        // On retire les retours à la ligne multiples dans le champ
        adresse = StringUtils.replacePattern(adresse, "[\r\n]+", "\r\n");
        // Supression des retours à la ligne en début et en fin d'adresse
        adresse = StringUtils.strip(adresse, "\r\n");
        // On retire les espaces multiples dans le champ adresse
        adresse = StringUtils.replacePattern(adresse, "[ ]+", " ");
        // On retire les espaces en début et fin
        adresse = StringUtils.trim(adresse);


        // 4. Enregistrement des champs calculés dans l'entité à persister
        entity.setAdresse(adresse);
        entity.setCodePostal(codePostal);
    }
    /**
     * Recopie les données de l'entity rna passée en paramètre vers la table organisation.
     * La copie a lieu seulement si l'organisation n'est pas inscrite au registre (pas d'espace
     * organisation).
     * @param entity données du RNA à persister
     */
    protected void updateEntryFromOrganisation(final ReferentielRnaEntity entity)
    {
        // Récupération de l'organisation si :
        // - elle existe
        // - provient du référentiel RNA (TypeIdentifiantNationalEnum.RNA)
        // - n'a pas d'espace organisation non refusé
        final Optional<OrganisationEntity> organisation = this.organisationRepository
            .findByEspaceOrganisationIsNullAndRnaAndOriginNationalIdRna(entity.getRna());

        if (organisation.isPresent()) {
            final OrganisationEntity organisationEntity = organisation.get();

            organisationEntity.setAdresse(entity.getAdresse());
            organisationEntity.setCodePostal(entity.getCodePostal());
            organisationEntity.setDenomination(entity.getDenomination());
            organisationEntity.setPays(entity.getPays());
            organisationEntity.setRna(entity.getRna());
            organisationEntity.setVille(entity.getVille());

            this.organisationRepository.save(organisationEntity);

            LOGGER.info("Organisation mise à jour dans la table organisation. rna="
                + organisationEntity.getRna());
        }
    }

    /**
     * Fait la suppression d'un établissement (RNA) du référentiel SIRENE s'il y est
     * présent.
     * @param line : la ligne du fichier CSV contenant les informations de l'établissement à
     * supprimer.
     */
    protected void deleteEntryFromRnaReferentiel(final ReferentielRnaLine line)
    {
        // Suppression de l'établissement (RNA) s'il existe.
    	if ("...".equals(line.getRna())) {
    		LOGGER.info("Etablissement mal renseigné");
    	}else {
    		final Long res = this.referencielRnaRepository.deleteByRna(line.getRna());
    		if (res.longValue() > 0) {
                LOGGER.info("Établissement supprimé du référentiel sirene. rna= {}; nic= {}", line.getRna(), line.getNic());
            }
            else {
                LOGGER.warn("Établissement introuvable pour suppression. rna= {} " , line.getRna());
            }
    	}
        
    }

    protected Date stringToDate (final String date) throws ParseException{

    	return new SimpleDateFormat("yyyy-mm-dd").parse(date);
    }
}
