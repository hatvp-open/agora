/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.exceptions;

/**
 * 
 * Exception lancée lorsque le programme détecte que le format du fichier SIRENE traité est
 * incorrect.
 * 
 * @version $Revision$ $Date${0xD}
 */
public class SireneFileFormatException
    extends Exception
{
    /** Clé de sérialisation. */
    private static final long serialVersionUID = 357026571748040917L;

    /**
     * Constructeur avec message d'erreur.
     * @param string
     */
    public SireneFileFormatException(final String string)
    {
        super(string);
    }

}
