/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.referentiel.pojos;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Cette classe représente les données utiles d'une ligne du fichier CSV de mise à jour de la base
 * SIRENE.
 * 
 * @version $Revision$ $Date${0xD}
 */
public class ReferentielSireneLine
{
    /**
     * Identifiant de l'entreprise.
     */
    private String siren = "";

    /**
     * Numéro interne de classement de l'établissement.
     */
    private String nic = "";

    /**
     * Numéro d’identification au répertoire national des associations.
     */
    private String rna = "";

    /**
     * Qualité de siège ou non de l'établissement.
     */
    private String siege = "";

    /**
     * Nom ou raison sociale de l'entreprise.
     */
    private String nomEnLong = "";

    /**
     * Première ligne de l’adressage de l’établissement.
     */
    private String l1Normalisee = "";

    /**
     * Deuxème ligne de l’adressage de l’établissement.
     */
    private String l2Normalisee = "";

    /**
     * Troisième ligne de l’adressage de l’établissement.
     */
    private String l3Normalisee = "";

    /**
     * Quatrième ligne de l’adressage de l’établissement.
     */
    private String l4Normalisee = "";

    /**
     * Cinquième ligne de l’adressage de l’établissement.
     */
    private String l5Normalisee = "";

    /**
     * Sixième ligne de l’adressage de l’établissement.
     */
    private String l6Normalisee = "";

    /**
     * Septième ligne de l’adressage de l’établissement.
     */
    private String l7Normalisee = "";

    /**
     * Année et mois de création de l'entreprise.
     */
    private String dCrEn = "";

    /**
     * Année et mois d'introduction de l'entreprise dans la base de diffusion.
     */
    private String amIntrEn = "";

    /**
     * Date de traitement de la mise à jour.
     */
    private String dateMaj = "";

    /**
     * Nature de la mise à jour (création, suppression, modification).
     */
    private String vMaj = "";

    /**
     * Indicateur de mise à jour de l'adresse de localisation de l'établissement.
     */
    private String mAdresse = "";

    /**
     * Indicateur de la mise à jour du nom ou de la raison sociale.
     */
    private String mNomEn = "";

    /**
     * Indicateur de mise à jour du Nic du siège ou de l'établissement principal.
     */
    private String mNicSiege = "";

    public String getSiren()
    {
        return this.siren;
    }

    public String getNic()
    {
        return this.nic;
    }

    public String getRna()
    {
        return this.rna;
    }

    public String getSiege()
    {
        return this.siege;
    }

    public String getNomEnLong()
    {
        return this.nomEnLong;
    }

    public String getL1Normalisee()
    {
        return this.l1Normalisee;
    }

    public String getL2Normalisee()
    {
        return this.l2Normalisee;
    }

    public String getL3Normalisee()
    {
        return this.l3Normalisee;
    }

    public String getL4Normalisee()
    {
        return this.l4Normalisee;
    }

    public String getL5Normalisee()
    {
        return this.l5Normalisee;
    }

    public String getL6Normalisee()
    {
        return this.l6Normalisee;
    }

    public String getL7Normalisee()
    {
        return this.l7Normalisee;
    }

    public String getdCrEn()
    {
        return this.dCrEn;
    }

    public String getAmIntrEn()
    {
        return this.amIntrEn;
    }

    public String getDateMaj()
    {
        return this.dateMaj;
    }

    public String getvMaj()
    {
        return this.vMaj;
    }

    public String getmAdresse()
    {
        return this.mAdresse;
    }

    public String getmNomEn()
    {
        return this.mNomEn;
    }

    public String getmNicSiege()
    {
        return this.mNicSiege;
    }

    public void setSiren(final String siren)
    {
        this.siren = siren;
    }

    public void setNic(final String nic)
    {
        this.nic = nic;
    }

    public void setRna(final String rna)
    {
        this.rna = rna;
    }

    public void setSiege(final String siege)
    {
        this.siege = siege;
    }

    public void setNomEnLong(final String nomEnLong)
    {
        this.nomEnLong = nomEnLong;
    }

    public void setL1Normalisee(final String l1Normalisee)
    {
        this.l1Normalisee = l1Normalisee;
    }

    public void setL2Normalisee(final String l2Normalisee)
    {
        this.l2Normalisee = l2Normalisee;
    }

    public void setL3Normalisee(final String l3Normalisee)
    {
        this.l3Normalisee = l3Normalisee;
    }

    public void setL4Normalisee(final String l4Normalisee)
    {
        this.l4Normalisee = l4Normalisee;
    }

    public void setL5Normalisee(final String l5Normalisee)
    {
        this.l5Normalisee = l5Normalisee;
    }

    public void setL6Normalisee(final String l6Normalisee)
    {
        this.l6Normalisee = l6Normalisee;
    }

    public void setL7Normalisee(final String l7Normalisee)
    {
        this.l7Normalisee = l7Normalisee;
    }

    public void setdCrEn(final String dCrEn)
    {
        this.dCrEn = dCrEn;
    }

    public void setAmIntrEn(final String amIntrEn)
    {
        this.amIntrEn = amIntrEn;
    }

    public void setDateMaj(final String dateMaj)
    {
        this.dateMaj = dateMaj;
    }

    public void setvMaj(final String vMaj)
    {
        this.vMaj = vMaj;
    }

    public void setmAdresse(final String mAdresse)
    {
        this.mAdresse = mAdresse;
    }

    public void setmNomEn(final String mNomEn)
    {
        this.mNomEn = mNomEn;
    }

    public void setmNicSiege(final String mNicSiege)
    {
        this.mNicSiege = mNicSiege;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
