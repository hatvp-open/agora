/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch;

import fr.hatvp.registre.batch.qualif.RepriseQualifBatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.lang.UsesJava8;

/**
 * Classe principale contenant le main du batch de reprise de qualification pour
 * requalifié tous les objets en erreur
 *
 * @version $Revision$ $Date${0xD}
 */
@UsesJava8
public class RepriseQualification
{
	/** Spring conf files location. */
	private static final String[] springContext = { "classpath:applicationContext.xml" };

	/** Logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(RepriseQualification.class);

	/** Lancement du batch. */
	public static void main(final String[] args) {

		// Loads Spring context
		final AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(springContext);
		ctx.registerShutdownHook();

		// Lancement du batch de traitement de production du fichier open data
		final RepriseQualifBatch repriseQualifBatch = ctx.getBean(RepriseQualifBatch.class);

		LOGGER.info("Démarrage du traitement de reprises de qualifications des objets dont la qualification avait échouée");
        repriseQualifBatch.launchProcess();
		LOGGER.info("Fin du traitement");

		// Close Spring context
		ctx.close();
	}

}
