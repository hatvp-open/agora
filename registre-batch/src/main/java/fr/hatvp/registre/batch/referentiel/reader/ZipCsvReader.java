/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.referentiel.reader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import au.com.bytecode.opencsv.CSVReader;
import fr.hatvp.registre.batch.exceptions.SireneAndRnaFileFormatException;
import fr.hatvp.registre.batch.referentiel.strategy.ReferentielFileProcessor;

/**
 * Classe qui permet la lecture d'un fichier CSV à l'intérieur d'une archive zip.
 * Elle est paramètrée avec une classe qui implémente la stratégie de traitement de chaque ligne du
 * fichier CSV.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Component
public class ZipCsvReader<P extends ReferentielFileProcessor>
{
    /**
     * L'implémentation du traitement des lignes du fichier CSV.
     */
    @Autowired
    @Qualifier("referentielSireneFileProcessor")
    P referentielFileProcessor;

    /**
     * Traite tous les fichiers CSV trouvés dans l'archive zip passée en paramètre.
     * Utile pour le traitement des archives zip contenant un unique fichier CSV.
     * 
     * Fait un flush toutes les 1000 lignes traitées.
     * 
     * @param zipFileLocation : le chemin complet du fichier zip à traiter
     * @return le nom du fichier csv dézippé
     * @throws IOException
     * @throws SireneAndRnaFileFormatException
     */
    @Transactional
    public int processCsvFilesFromArchive(final String zipFileLocation)
        throws IOException,
        SireneAndRnaFileFormatException,
        ParseException
    {
        int updatedRows = 0;
        int flushCounter = 0;

        try (ZipFile zipFile = new ZipFile(zipFileLocation)) {

            ZipEntry fileEntry = null;
            final Enumeration<? extends ZipEntry> entries = zipFile.entries();
            // Parcours des éléments du zip jusqu'à trouver un fichier CSV
            while (entries.hasMoreElements()) {

                fileEntry = entries.nextElement();

                // Si on trouve un fichier CSV
                if ((fileEntry != null) && !fileEntry.isDirectory()
                        && fileEntry.getName().endsWith(".csv")) {

                    // On stream le contenu du CSV
                    final InputStream inputStream = zipFile.getInputStream(fileEntry);
                    final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    final CSVReader csvReader = new CSVReader(inputStreamReader, ';', '"', 1);
                    String[] lineContent = null;

                    // Traitement des lignes du fichier délégué au file processor
                    while ((lineContent = csvReader.readNext()) != null) {

                        // Traitement de la ligne courante
                        final boolean updated = this.referentielFileProcessor
                                .processLine(lineContent);

                        // Incrément du compteur des lignes mises à jour.
                        if (updated) {
                            flushCounter++;
                            updatedRows++;
                        }

                        // On flush les données traitées toutes les 1000 occurences
                        if (flushCounter == 1000) {
                            this.referentielFileProcessor.flushRepository();
                            flushCounter = 0;
                        }
                    }
                    inputStream.close();
                    inputStreamReader.close();
                    csvReader.close();
                }
            }
        }

        return updatedRows;
    }
}
