/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.referentiel.pojos;

/**
 * Cette classe représente les données utiles d'une ligne du fichier CSV de mise à jour de la base
 * RNA.
 */
public class ReferentielRnaLine {

    /**
     * Numéro d’identification au répertoire national des associations.
     */
    private String rna = "";

    /**
     * Numéro de siren si l'association est inscrite au répertoire siren.
     * extrait de siret.
     */
    private String siren = "";

    /**
     * Numéro interne de classement de l'établissement.
     * extrait de siret.
     */
    private String nic = "";

    /**
     * Date de déclaration de création de l'association
     */
    private String dateCreation = "";

    /**
     * Date de la dernière déclaration
     */
    private String dateDeclaration = "";

    /**
     * Date de publication de l'avis de création
     */
    private String datePublication = "";

    /**
     * Dénomination de l'association.
     */
    private String titre = "";

    /**
     * objet des activitées de l'association.
     */
    private String objet = "";

    /**
     * Numéro de voie de l'adresse.
     */
    private String numVoie = "";

    /**
     * Type de voie de l'adresse.
     */
    private String typeVoie = "";

    /**
     * Libellé de l'adresse.
     */
    private String libVoie = "";

    /**
     * Code postal du siège
     */
    private String CP = "";

    /**
     * Ville du siège
     */
    private String ville = "";

    /**
     * Url du site web si précisé sur le fichier
     */
    private String siteWeb = "";

    /**
     * Position d'activité de l'association (Active, Dissoute ou Supprimé).
     */
    private String position = "";

    /**
     * Date de mise à jour de l'article.
     */
    private String majTime = "";

    /**
     * Pays du siège de l'association
     */
    private String pays = "";

    public String getRna() {
        return rna;
    }

    public void setRna(String rna) {
        this.rna = rna;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getDateDeclaration() {
        return dateDeclaration;
    }

    public void setDateDeclaration(String dateDeclaration) {
        this.dateDeclaration = dateDeclaration;
    }

    public String getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(String datePublication) {
        this.datePublication = datePublication;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getNumVoie() {
        return numVoie;
    }

    public void setNumVoie(String numVoie) {
        this.numVoie = numVoie;
    }

    public String getTypeVoie() {
        return typeVoie;
    }

    public void setTypeVoie(String typeVoie) {
        this.typeVoie = typeVoie;
    }

    public String getLibVoie() {
        return libVoie;
    }

    public void setLibVoie(String libVoie) {
        this.libVoie = libVoie;
    }

    public String getCP() {
        return CP;
    }

    public void setCP(String CP) {
        this.CP = CP;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public String getMajTime() {
        return majTime;
    }

    public void setMajTime(String majTime) {
        this.majTime = majTime;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSiren() {
        return siren;
    }

    public void setSiren(String siren) {
        this.siren = siren;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }
}
