/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.lang.UsesJava8;

import fr.hatvp.registre.batch.publication.PublicationUpdateBatch;

/**
 * Classe principale contenant le main du batch de publication des déclarations publiées dans Agora. Le programme a pour vocation de s'exécuter à intervalles réguliers au fil de la
 * journée (probablement chaque quart d'heure).
 *
 * S'il détermine qu'il y a des publications à faire, il va produire 3 types de documents : - un document stock.json qui résume l'ensemble des dernières publications ; - un
 * document typesOrganisations.json liste les types d'organisations proposées par le service ; - un ou plusieurs documents [identifiant national].json listent pour chaque
 * organisation l'historique de ses publications.
 *
 * @version $Revision$ $Date${0xD}
 */
@UsesJava8
public class PublicationUpdate {
	/** Spring conf files location. */
	private static final String[] springContext = { "classpath:applicationContext.xml" };

	/** Logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PublicationUpdate.class);

	/** Lancement du batch. */
	public static void main(final String[] args) {

		// Loads Spring context
		final AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(springContext);
		ctx.registerShutdownHook();

		// Lancement du batch de traitement de mise à jour des publications
		final PublicationUpdateBatch publicationUpdateBatch = ctx.getBean(PublicationUpdateBatch.class);

		LOGGER.info("Démarrage du traitement de mise à jour des déclarations.");
		publicationUpdateBatch.launchProcess();
		LOGGER.info("Fin du traitement de mise à jour des déclarations.");

		// Close Spring context
		ctx.close();
	}

}
