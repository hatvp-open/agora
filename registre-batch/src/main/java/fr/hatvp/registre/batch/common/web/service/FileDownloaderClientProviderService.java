/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.common.web.service;

import java.net.MalformedURLException;

/**
 * Service de téléchargement des fichiers de mise à jour depuis internet.
 */
public interface FileDownloaderClientProviderService
{

    /**
     * Télécharge un contenu web en http vers un fichier local.
     * @param url : l'adresse de la ressource à télécharger.
     * @param fileDirLocation : le dossier où on écrit le contenu téléchargé.
     * @return le nom du fichier téléchargé. null si erreur 404.
     * @throws MalformedURLException
     */
    String downloadToLocalFile(final String url, final String fileDirLocation)
        throws MalformedURLException;

}
