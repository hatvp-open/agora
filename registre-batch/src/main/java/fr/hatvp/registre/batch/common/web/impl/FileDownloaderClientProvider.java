/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.common.web.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

import fr.hatvp.registre.batch.common.web.service.FileDownloaderClientProviderService;

@Component
public class FileDownloaderClientProvider
    implements FileDownloaderClientProviderService
{

    @Autowired
    private RestTemplate restTemplate;

    /**
     * {@inheritDoc}
     * @throws MalformedURLException
     */
    @Override
    public String downloadToLocalFile(final String address, final String fileDirLocation)
        throws MalformedURLException
    {
        // On détermine la validité de l'URL
        final URL url = new URL(address);

        // On détermine le nom du fichier téléchargé
        final String fileName = FilenameUtils.getName(url.getPath());
        String retour = null;

        try {
            this.downloadToFile(address, fileDirLocation + "/" + fileName);
            retour = fileName;
        }
        catch (final HttpClientErrorException e) {
            // Erreur 404 : seul cas où une exception signifie qu'on peut continuer l'exécution
            // du batch. La fonction retournera alors null.
            if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
                throw e;
            }
        }

        return retour;
    }

    /**
     * Télécharge le fichier en HTTP vers le fichier local.
     * @param address l'adresse http
     * @param filePath le chemin du fichier local
     */
    private void downloadToFile(final String address, final String filePath)
    {
        // Accept header
        final RequestCallback requestCallback = request -> request.getHeaders()
                .setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL));

        // Stream la réponse vers un fichier plutôt que de tout charger en mémoire (évitons les
        // heap
        // space exception)
        final ResponseExtractor<Void> responseExtractor = response -> {
            final Path path = Paths.get(filePath);
            Files.copy(response.getBody(), path, StandardCopyOption.REPLACE_EXISTING);
            return null;
        };

        // Téléchargement
        this.restTemplate.execute(address, HttpMethod.GET, requestCallback, responseExtractor);
    }

}
