/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.lang.UsesJava8;

import fr.hatvp.registre.batch.opendata.OpenDataUpdateBatch;

/**
 * Classe principale contenant le main du batch de publication du fichier Opendata d'Agora. Le programme a pour vocation de s'exécuter à intervalles réguliers au fil de la journée
 * (probablement une fois chaque nuit).
 *
 * - un document .json qui contient l'ensemble des dernières publications faites dans Agora.
 *
 * @version $Revision$ $Date${0xD}
 */
@UsesJava8
public class OpenDataUpdate {
	/** Spring conf files location. */
	private static final String[] springContext = { "classpath:applicationContext.xml" };

	/** Logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(OpenDataUpdate.class);

	/** Lancement du batch. */
	public static void main(final String[] args) {

		// Loads Spring context
		final AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(springContext);
		ctx.registerShutdownHook();

		// Lancement du batch de traitement de production du fichier open data
		final OpenDataUpdateBatch openDataUpdateBatch = ctx.getBean(OpenDataUpdateBatch.class);

		LOGGER.info("Démarrage du traitement de production du fichier open data.");
		openDataUpdateBatch.launchProcess();
		LOGGER.info("Fin du traitement de production du fichier open data.");

		// Close Spring context
		ctx.close();
	}

}
