/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.batch.referentiel.strategy;

import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.hatvp.registre.batch.exceptions.SireneAndRnaFileFormatException;
import fr.hatvp.registre.batch.referentiel.pojos.ReferentielSireneLine;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielSireneEntity;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
import fr.hatvp.registre.persistence.repository.referenciel.ReferencielSirenRepository;

/**
 * Test unitaire traitement des lignes SIRENE.
 */
public class ReferentielSireneFileProcessorTest
{
    @Mock
    private ReferencielSirenRepository referencielSirenRepository;

    @Mock
    private OrganisationRepository organisationRepository;

    @InjectMocks
    ReferentielSireneFileProcessor referentielSireneFileProcessor;

    /**
     * Initialisation des tests..
     */
    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetDenominationFromLine()
        throws SecurityException,
        NoSuchMethodException,
        IllegalAccessException,
        IllegalArgumentException,
        InvocationTargetException
    {
        // Initialisation
        final ReferentielSireneLine line = new ReferentielSireneLine();
        line.setNomEnLong("     *Dénomination*de/test*slash/étoile*     ");

        final String denomination = this.referentielSireneFileProcessor
                .getDenominationFromLine(line);

        Assert.assertEquals("DÉNOMINATION DE TEST SLASH ÉTOILE", denomination);
    }

    @Test(expected = SireneAndRnaFileFormatException.class)
    public void testProcessLineWrongFormat() throws SireneAndRnaFileFormatException
    {
        final String[] line = new String[117];

        this.referentielSireneFileProcessor.processLine(line);
    }

    @Test
    public void testProcessLineFormatOk() throws SireneAndRnaFileFormatException
    {
        final String[] line = new String[118];

        this.referentielSireneFileProcessor.processLine(line);
    }

    @Test
    public void testMapArrayToReferentielSireneLineOk()
    {
        final String[] lineContent = new String[118];

        lineContent[0] = "0";
        lineContent[1] = "1";
        lineContent[2] = "2";
        lineContent[3] = "3";
        lineContent[4] = "4";
        lineContent[5] = "5";
        lineContent[6] = "6";
        lineContent[7] = "7";
        lineContent[8] = "8";
        lineContent[35] = "35";
        lineContent[60] = "60";
        lineContent[65] = "65";
        lineContent[83] = "83";
        lineContent[84] = "84";
        lineContent[95] = "95";
        lineContent[99] = "99";
        lineContent[105] = "105";
        lineContent[110] = "110";
        lineContent[112] = "112";

        final ReferentielSireneLine line = this.referentielSireneFileProcessor
                .mapArrayToReferentielSireneLine(lineContent);

        Assert.assertEquals(lineContent[0], line.getSiren());
        Assert.assertEquals(lineContent[1], line.getNic());
        Assert.assertEquals(lineContent[2], line.getL1Normalisee());
        Assert.assertEquals(lineContent[3], line.getL2Normalisee());
        Assert.assertEquals(lineContent[4], line.getL3Normalisee());
        Assert.assertEquals(lineContent[5], line.getL4Normalisee());
        Assert.assertEquals(lineContent[6], line.getL5Normalisee());
        Assert.assertEquals(lineContent[7], line.getL6Normalisee());
        Assert.assertEquals(lineContent[8], line.getL7Normalisee());
        Assert.assertEquals(lineContent[35], line.getSiege());
        Assert.assertEquals(lineContent[60], line.getNomEnLong());
        Assert.assertEquals(lineContent[65], line.getRna());
        Assert.assertEquals(lineContent[83], line.getdCrEn());
        Assert.assertEquals(lineContent[84], line.getAmIntrEn());
        Assert.assertEquals(lineContent[95], line.getvMaj());
        Assert.assertEquals(lineContent[99], line.getDateMaj());
        Assert.assertEquals(lineContent[105], line.getmAdresse());
        Assert.assertEquals(lineContent[110], line.getmNomEn());
        Assert.assertEquals(lineContent[112], line.getmNicSiege());
    }

    @Test
    public void testMapSireneLineToEntityFranceOk()
    {
        final Date start = Calendar.getInstance().getTime();

        final ReferentielSireneLine line = new ReferentielSireneLine();
        final ReferentielSireneEntity entity = new ReferentielSireneEntity();

        entity.setVersion(Integer.valueOf(5));

        line.setDateMaj("   DAETMAJ   ");
        line.setAmIntrEn("   AAAAMM   ");
        line.setdCrEn("   AAAAMM  ");
        line.setNomEnLong(" *  fgsrs grgrevgg*fgrfg /frfwfw  /");
        line.setNic("   12345   ");
        line.setSiren("    123456789   ");
        line.setRna("    W123456789      ");
        line.setL1Normalisee("");
        line.setL2Normalisee("rue des   tulipes");
        line.setL3Normalisee("");
        line.setL4Normalisee("lieu dit les lilas   ");
        line.setL5Normalisee("");
        line.setL6Normalisee("75002   paris    ");
        line.setL7Normalisee("  pays pays   ");

        this.referentielSireneFileProcessor.mapSireneLineToEntity(line, entity);

        Assert.assertEquals("RUE DES TULIPES\r\nLIEU DIT LES LILAS",entity.getAdresse());
        Assert.assertEquals("75002", entity.getCodePostal());
        Assert.assertEquals("AAAAMM", entity.getDateCreationEntreprise());
        Assert.assertEquals("AAAAMM",entity.getDateEntreeEntreprise());
        Assert.assertEquals("DAETMAJ", entity.getDateMajEntreprise());
        Assert.assertEquals("FGSRS GRGREVGG FGRFG  FRFWFW", entity.getDenomination());
        Assert.assertEquals("12345", entity.getNic());
        Assert.assertEquals("PAYS PAYS", entity.getPays());
        Assert.assertEquals("W123456789", entity.getRna());
        Assert.assertEquals("123456789", entity.getSiren());
        Assert.assertEquals("PARIS", entity.getVille());
        Assert.assertTrue(start.before(entity.getDateDernierTraitement())
                || start.equals(entity.getDateDernierTraitement()));
        Assert.assertEquals(null, entity.getId());
        Assert.assertEquals(Integer.valueOf(5), entity.getVersion());
    }

    @Test
    public void testMapSireneLineToEntityEtrangerOk()
    {
        final Date start = Calendar.getInstance().getTime();

        final ReferentielSireneLine line = new ReferentielSireneLine();
        final ReferentielSireneEntity entity = new ReferentielSireneEntity();

        entity.setVersion(Integer.valueOf(5));

        line.setDateMaj("   DAETMAJ   ");
        line.setAmIntrEn("   AAAAMM   ");
        line.setdCrEn("   AAAAMM  ");
        line.setNomEnLong(" *  fgsrs grgrevgg*fgrfg /frfwfw  /");
        line.setNic("   12345   ");
        line.setSiren("    123456789   ");
        line.setRna("          ");
        line.setL1Normalisee(" address 1   ");
        line.setL2Normalisee(" address 2 ");
        line.setL3Normalisee(" address 3  ");
        line.setL4Normalisee(" street   ");
        line.setL5Normalisee(" address   5  ");
        line.setL6Normalisee("   london    ");
        line.setL7Normalisee("  country");

        this.referentielSireneFileProcessor.mapSireneLineToEntity(line, entity);

        Assert.assertEquals("ADDRESS 1 \r\nADDRESS 2 \r\nADDRESS 3 \r\nSTREET \r\nADDRESS 5 \r\nLONDON", entity.getAdresse());
        Assert.assertEquals("", entity.getCodePostal());
        Assert.assertEquals("AAAAMM", entity.getDateCreationEntreprise());
        Assert.assertEquals("AAAAMM", entity.getDateEntreeEntreprise());
        Assert.assertEquals("DAETMAJ", entity.getDateMajEntreprise());
        Assert.assertEquals("FGSRS GRGREVGG FGRFG  FRFWFW", entity.getDenomination());
        Assert.assertEquals("12345", entity.getNic());
        Assert.assertEquals("COUNTRY", entity.getPays());
        Assert.assertEquals(null, entity.getRna());
        Assert.assertEquals("123456789", entity.getSiren());
        Assert.assertEquals("", entity.getVille());
        Assert.assertTrue(start.before(entity.getDateDernierTraitement())
                || start.equals(entity.getDateDernierTraitement()));
        Assert.assertEquals(null, entity.getId());
        Assert.assertEquals(Integer.valueOf(5), entity.getVersion());
    }

}
