# 'AGORA' : répertoire numérique des représentants d'intérêts


## Contexte

Depuis le 1er juillet 2017, les acteurs socio-économiques qui exercent des actions d’influence sur les responsables publics sont tenus de s’inscrire sur le répertoire numérique des représentants d’intérêts géré par la Haute Autorité pour la transparence de la vie publique (HATVP). Ce répertoire vise à fournir une information aux citoyens sur les représentants d’intérêts et sur leurs relations avec les responsables publics lorsque sont prises des décisions publiques. L’objectif est de mieux connaître et de mesurer l’impact des représentants d'intérêts sur le processus normatif, autrement dit de « savoir qui influence la loi ». Il est ainsi possible de consulter en open data une série d’informations permettant de connaître l’identité des représentants d’intérêts inscrits au répertoire, leurs actions de représentation d’intérêts (ou "lobbying") ainsi que les moyens consacrés à ces actions. Ces informations doivent être déclarées chaque année par les représentants d’intérêts.
Pour accéder au répertoire des représentants d'intérêts : https://www.hatvp.fr/le-repertoire/

L'application web AGORA, accessible sur le lien https://repertoire.hatvp.fr, est conçue comme un téléservice destiné aux représentants d'intérêts, qui leur permet de s'inscrire au répertoire et de déclarer leurs activités en temps réel.



## Ouverture du code

Dans le cadre de sa participation à l’Open Government Partnership (OGP – Partenariat pour le Gouvernement ouvert), la Haute Autorité a pris l’engagement d’assurer une plus grande transparence des activités des représentants d’intérêts. Elle publie ainsi le code source d'AGORA afin d’en faciliter l’exploitation.
Cet outil est encore en cours d'évolution et développé activement par les services de la HATVP. De ce fait, les contributions à ce projet ne sont pas attendues. 

Pour toute question vous pouvez contacter : webmestre@hatvp.fr


## Installation 

### Dépendances techniques 


- Tomcat >= 8.0.X
- Maven >= 3.2.5
- Node >= 10.17.0
- npm >= 6.11
- git >= 2.24
- bower >= 1.8
- gulp >= 3.9

### Build

```bash
npm install -g gulp
npm install -g bower
cd registre-web
cd ..
npm install
cd registre-bo-web
npm install
cd ..
mvn clean install
```

## License

Le dépôt est publié sous licence MIT.

Les auteurs sont la Haute Autorité pour la transparence de la vie publique.

