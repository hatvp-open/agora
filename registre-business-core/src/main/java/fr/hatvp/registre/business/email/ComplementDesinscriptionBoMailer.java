/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.email;

import fr.hatvp.registre.commons.dto.DeclarantDto;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Locale;

@Component
public class ComplementDesinscriptionBoMailer extends ApplicationMailer {
    /**
     * Initialisation du {@link JavaMailSender}
     *
     * @param sender
     * @param templateEngine
     */
    protected ComplementDesinscriptionBoMailer(JavaMailSender sender, TemplateEngine templateEngine) {
        super(sender, templateEngine);
    }

    /** Template de demande de complément pour un espace. */
    private static final String templateComplementDesinscription = "email/complement_desinscription";

    /**
     * Envoi de mail de demande de complément pour la création de l'espace organisation
     *
     * @param to email destinataire
     */
    public void sendDemandeComplementDesinscriptionEmail(final DeclarantDto to, final String message,
                                                 final String denomination)
    {
        this.prepareAndSendDesinscriptionEmail(to, "Demande de complément de désinscription de l'espace "+denomination,denomination, templateComplementDesinscription, null, message);
    }
    /**
     * Préparer et envoyer le mail.
     */
    private void prepareAndSendDesinscriptionEmail(final DeclarantDto to, final String object, final  String denomination,
                                           final String template, final String link, final String message)
    {

        final Context ctx = new Context(Locale.FRANCE);
        ctx.setVariable("civilite", to.getCivility().getLibelleLong());
        ctx.setVariable("denomination", denomination);
        ctx.setVariable("message", message);
        ctx.setVariable("link", link);
        ctx.setVariable("imageResourceName", this.imageResourceName);

        final String mailContent = this.templateEngine.process(template, ctx);
        super.prepareAndSendEmailWhithLogo(to.getEmail(), object, mailContent);
    }

}
