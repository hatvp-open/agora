/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.DesinscriptionService;
import fr.hatvp.registre.business.GestionPiecesEspaceOrganisationService;
import fr.hatvp.registre.business.email.ComplementDesinscriptionBoMailer;
import fr.hatvp.registre.business.email.DesinscriptionMailer;
import fr.hatvp.registre.business.email.EspaceOrganisationMailer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.DesinscriptionPieceTransformer;
import fr.hatvp.registre.business.transformer.proxy.DesinscriptionTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationTransformer;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.DesinscriptionDto;
import fr.hatvp.registre.commons.dto.DesinscriptionPieceDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.OrigineSaisieEnum;
import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.commentaire.CommentEspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.PeriodeActiviteClientEntity;
import fr.hatvp.registre.persistence.entity.piece.DesinscriptionPieceEntity;
import fr.hatvp.registre.persistence.entity.publication.PubClientEntity;
import fr.hatvp.registre.persistence.entity.publication.batch.relance.PublicationRelanceEntity;
import fr.hatvp.registre.persistence.repository.activite.ExerciceComptableRepository;
import fr.hatvp.registre.persistence.repository.backoffice.UtilisateurBoRepository;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentEspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionPieceRepository;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.PeriodeActiviteClientRepository;
import fr.hatvp.registre.persistence.repository.espace.SurveillanceRepository;
import fr.hatvp.registre.persistence.repository.publication.PubClientRepository;
import fr.hatvp.registre.persistence.repository.publication.batch.relance.PublicationRelanceRepository;

@Transactional
@Service
public class DesinscriptionServiceImpl extends AbstractCommonService<DesinscriptionDto, DesinscriptionEntity> implements DesinscriptionService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DesinscriptionServiceImpl.class);

    // Repository
    @Autowired
    EspaceOrganisationRepository espaceOrganisationRepository;

    @Autowired
    DesinscriptionRepository desinscriptionRepository;

    @Autowired
    DesinscriptionPieceRepository desinscriptionPieceRepository;

    @Autowired
    SurveillanceRepository surveillanceRepository;    

    @Autowired
    CommentEspaceOrganisationRepository commentEspaceOrganisationRepository;
    
    @Autowired
    ExerciceComptableRepository exerciceComptableRepository;

    // Transformer
    @Autowired
    DesinscriptionTransformer desinscriptionTransformer;

    @Autowired
    EspaceOrganisationTransformer espaceOrganisationTransformer;

    @Autowired
    DesinscriptionPieceTransformer desinscriptionPieceTransformer;

    @Autowired
    PublicationTransformer publicationTransformer;

    @Autowired
    DeclarantTransformer declarantTransformer;

    @Autowired
    DeclarantRepository declarantRepository;
    
    @Autowired
    PublicationRelanceRepository publicationRelanceRepository;

    // Mailing
    @Autowired
    private DesinscriptionMailer desinscriptionMailer;

    @Autowired
    private ComplementDesinscriptionBoMailer complementDesinscriptionBoMailer;

    @Autowired
    private EspaceOrganisationMailer espaceOrganisationMailer;
    
    @Autowired
    private GestionPiecesEspaceOrganisationService gestionPiecesEspaceOrganisationService;
    
    @Autowired
    UtilisateurBoRepository utilisateurBoRepository;
    
    @Autowired
    PeriodeActiviteClientRepository periodeActiviteClientRepository;
    
    @Autowired
    PubClientRepository pubClientRepository;
    
    @Autowired
    OrganisationRepository organisationRepository;

    @Override
    public JpaRepository<DesinscriptionEntity, Long> getRepository() {
        return null;
    }

    @Override
    public CommonTransformer<DesinscriptionDto, DesinscriptionEntity> getTransformer() {
        return null;
    }
	/**
	 * Enregistrement et/ou validation désincription via le backoffice
	 */
    @Transactional
    @Override
    public DesinscriptionDto saveDesinscription (DesinscriptionDto desinscriptionDto, List<DesinscriptionPieceDto> pieces, Long userId, Long espaceOrganisationId){
        desinscriptionDto.setEspaceOrganisation(this.espaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.findById(espaceOrganisationId)));
        DesinscriptionEntity entity = this.desinscriptionTransformer.dtoToModel(desinscriptionDto);
        EspaceOrganisationEntity espaceOrganisationEntity = espaceOrganisationRepository.findById(entity.getEspaceOrganisation().getId());
        
        // on set les informations complémentaires
        espaceOrganisationEntity.setNewPublication(true);
        //si la date de cessation est antérieure ou égale à la date du jour on passe directement à DESINSCRIT
        if(desinscriptionDto.getCessationDate().isBefore(LocalDate.now()) || desinscriptionDto.getCessationDate().compareTo(LocalDate.now()) == 0) {
        	//désactiviation des éventuels clients      	
        	if(!espaceOrganisationEntity.getClients().isEmpty())desactivationClient(espaceOrganisationEntity); 
        	espaceOrganisationEntity.setStatut(StatutEspaceEnum.DESINSCRIT);
        }else {
        	espaceOrganisationEntity.setStatut(StatutEspaceEnum.DESINSCRIPTION_VALIDEE_HATVP);
        }
        
        
        //cas d'une saisie par agent sinon on ne change pas la donnée
       if(entity.getId() == null) {
    	   entity.setOrigineSaisie(OrigineSaisieEnum.AGENT);
       }        

        // On arrete les exercices comptables : 
        if(!espaceOrganisationEntity.getExercicesComptable().isEmpty()){
        	List<ExerciceComptableEntity> exerciceComptableEntities = miseAjourExerciceComptable(espaceOrganisationEntity.getExercicesComptable(), entity.getCessationDate());
            
            espaceOrganisationEntity.setExercicesComptable(exerciceComptableEntities);
        }
        //pièce supplémentaire ajouté par l'agent lors validation demande de désincription ou
        if (pieces !=null) {
        	List<DesinscriptionPieceEntity> listPieces = ajoutPiecesJointesAgent(pieces, entity);
            entity.setPieces(listPieces);
        }
        this.desinscriptionRepository.save(entity);
        this.espaceOrganisationRepository.save(espaceOrganisationEntity);
        
        LOGGER.info("Saved desinscription pour espcage {} ", espaceOrganisationId);
        
      //envoi notif de la demande de la validation d'une désinscription ou d'enregistrement d'une désinscription par un agent
        this.desinscriptionMailer.sendDesinscriptionValidationMailDRI(espaceOrganisationEntity, entity);       

        // on envoie le mail de désinscription
        if(espaceOrganisationEntity.getCreateurEspaceOrganisation() != null){
            this.desinscriptionMailer.sendDesinscriptionMail(espaceOrganisationEntity, entity,espaceOrganisationEntity.getCreateurEspaceOrganisation());
        }
        
        LOGGER.info("Mail sent desinscription pour espcage {} ", espaceOrganisationId);
        return desinscriptionDto;
    }



    @Override
    public DesinscriptionDto getDesinscription(long espaceId){
    	return this.desinscriptionTransformer.modelToDto(this.desinscriptionRepository.findByEspaceOrganisation(this.espaceOrganisationRepository.findById(espaceId)));
    }

    @Override
    public List<DesinscriptionPieceDto> getPiecesDesinscription(long espaceId) {
        // On transforme la liste d'entity en dto en vidant le contenu du fichier qui n'est pas utile dans ce contexte.
        Stream<DesinscriptionPieceEntity> desinscriptionPieceEntities= this.desinscriptionPieceRepository.findByDesinscription_EspaceOrganisation_IdOrderByDateVersementPieceDesc(espaceId);
        List<DesinscriptionPieceDto> desinscriptionPieceDtos = desinscriptionPieceEntities.map(e -> {
            DesinscriptionPieceDto dto = desinscriptionPieceTransformer.modelToDto(e);
            // Inutile de renvoyer le contenu du fichier dans cette liste. Économisons la mémoire !
            dto.setContenuFichier(null);
            return dto;
        }).collect(Collectors.toList());
        return desinscriptionPieceDtos;
    }

    @Override
    public Integer getNombreDemandeDesinscription() {
        return this.espaceOrganisationRepository.findByStatut(StatutEspaceEnum.DESINSCRIPTION_DEMANDEE).size();
    }

    @Override
    public Integer getNombreComplementsDemandeDesinscriptionReçu() {
        return this.espaceOrganisationRepository.findByStatut(StatutEspaceEnum.COMPLEMENTS_DEMANDE_DESINSCRIPTION_RECU).size();
    }

    @Override
    public void rejectDemande(long desinscriptionId) {
        DesinscriptionEntity desinscriptionEntity = this.desinscriptionRepository.findById(desinscriptionId);
        EspaceOrganisationEntity espaceOrganisationEntity = desinscriptionEntity.getEspaceOrganisation();
        List<DesinscriptionPieceEntity> desinscriptionPieceEntity = this.desinscriptionPieceRepository.findByDesinscription(desinscriptionEntity);
        this.desinscriptionPieceRepository.delete(desinscriptionPieceEntity);
        this.desinscriptionRepository.delete(desinscriptionEntity);
        espaceOrganisationEntity.setStatut(StatutEspaceEnum.ACCEPTEE);
        this.espaceOrganisationRepository.save(espaceOrganisationEntity);
    }

    @Override
    public void askForComplement(long desinscriptionId, String msg, long currentUserId) {
        DesinscriptionEntity desinscriptionEntity = this.desinscriptionRepository.findById(desinscriptionId);
        final EspaceOrganisationEntity espaceOrganisationEntity = desinscriptionEntity.getEspaceOrganisation();
        
        //template commun pour la gestion de demande de complément, mention d'ordinaire pré-enregistré en fonction
        //du template, ici obligé de concaténer la mention au message reçu
        msg += "\n\nMerci de verser les pièces manquantes depuis votre espace AGORA via l’onglet « Verser les pièces manquantes, depuis l’onglet « identité »";

        // statut complément demandé
        espaceOrganisationEntity.setStatut(StatutEspaceEnum.COMPLEMENTS_DEMANDE_DESINSCRIPTION);

        // envoi email demande complément
        this.complementDesinscriptionBoMailer.sendDemandeComplementDesinscriptionEmail(this.declarantTransformer.modelToDto(espaceOrganisationEntity.getCreateurEspaceOrganisation()), msg,
        		espaceOrganisationEntity.getOrganisation().getDenomination());
        
        //enregistrement commentaire
        saveCommentEspaceOrganisation(espaceOrganisationEntity, msg, currentUserId);

        // sauvegarde du changement de statut
        this.espaceOrganisationRepository.save(espaceOrganisationEntity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void verserPiecesComplementairesDesinscription(Long idEspaceOrganisation, Long idDeclarant, List<DesinscriptionPieceDto> pieces) {
        EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository.findOne(idEspaceOrganisation);
        DesinscriptionEntity desinscriptionEntity = this.desinscriptionRepository.findByEspaceOrganisation(espaceOrganisationEntity);
        ArrayList<DesinscriptionPieceEntity> piecesEntity = new ArrayList<>();

        // On vérifie le statut
        if (!StatutEspaceEnum.COMPLEMENTS_DEMANDE_DESINSCRIPTION.equals(espaceOrganisationEntity.getStatut())) {
            throw new BusinessGlobalException(ErrorMessageEnum.AJOUT_COMPLEMENT_INTERDIT.getMessage());
        }

        if (pieces !=null) {
            for (DesinscriptionPieceDto piece : pieces) {
                DesinscriptionPieceEntity e = desinscriptionPieceTransformer.dtoToModel(piece);
                e.setDateVersementPiece(new Date());
                e.setOrigineVersement(OrigineVersementEnum.DEMANDE_DESINSCRIPTION_ESPACE);
                e.setDesinscription(desinscriptionEntity);
                piecesEntity.add(e);
            }
            desinscriptionEntity.setPieces(piecesEntity);
        }
        this.desinscriptionRepository.save(desinscriptionEntity);
        // Mise à jour du statut de l'espace pour le BO.
        espaceOrganisationEntity.setStatut(StatutEspaceEnum.COMPLEMENTS_DEMANDE_DESINSCRIPTION_RECU);
    }
    
    /**
     * Génération de la pièce jointe puis envoi de l'email dans le cas d'une demande de désinscription
     * d'une organisation n'ayant pas publié d'activité
     */
    @Override
    public void sendMailDesinscriptionNoActivite(long espaceId) {
        EspaceOrganisationEntity espaceOrganisationEntity = espaceOrganisationRepository.findById(espaceId);
        EspaceOrganisationDto espaceOrganisationDto = this.espaceOrganisationTransformer.modelToDto(espaceOrganisationEntity);
        DeclarantDto contactOperationnel = this.declarantTransformer.modelToDto(espaceOrganisationEntity.getCreateurEspaceOrganisation());
        
        byte[] pdfByteArray = this.gestionPiecesEspaceOrganisationService.buildPdfPieceJointeDesinscriptionNoActivite(espaceOrganisationDto);
        final InputStreamSource myInputStream = new ByteArrayResource(pdfByteArray);
        
        this.espaceOrganisationMailer.sendDesinscriptionNoActiviteMail(contactOperationnel, espaceOrganisationEntity.getOrganisation().getDenomination(),myInputStream);
    }
    
    /**
     * enregistre un commentaire automatiquement quand une demande de complément est faite lors d'une demande
     * de désinscription
     * @param espaceOrganisationEntity
     * @param msg
     * @param currentUserId
     */
    public void saveCommentEspaceOrganisation(EspaceOrganisationEntity espaceOrganisationEntity, String msg, long currentUserId) {
    	final CommentEspaceOrganisationEntity commentEspaceOrganisationEntity = new CommentEspaceOrganisationEntity();
        commentEspaceOrganisationEntity.setDateCreation(new Date());
        UtilisateurBoEntity utilisateurBo = this.utilisateurBoRepository.findById(currentUserId);
        commentEspaceOrganisationEntity.setEditeur(utilisateurBo);
        commentEspaceOrganisationEntity.setEspaceOrganisationEntity(espaceOrganisationEntity);
        commentEspaceOrganisationEntity.setMessage(msg);
        
        this.commentEspaceOrganisationRepository.save(commentEspaceOrganisationEntity);
    }
    /**
     * 1- Si date début > date cessation on supprime l'exercice
     * 2- Date début et date fin encadrent date cessation on modifie la date de fin
     * @param exerciceComptableEntities
     * @return
     */
    public List<ExerciceComptableEntity> miseAjourExerciceComptable(List<ExerciceComptableEntity> exerciceComptableEntities, LocalDate dateCessation) {
   	
        exerciceComptableEntities.sort(Comparator.comparing(ExerciceComptableEntity::getDateFin));
        
        for(int i = exerciceComptableEntities.size()-1; i>=0; i--) {
        	ExerciceComptableEntity exerciceComptableEntity = exerciceComptableEntities.get(i);
        	if(exerciceComptableEntity.getDateDebut().isAfter(dateCessation)) {
//        		on vérifie s'il y a une relance en place et on supprime
        		if(exerciceComptableEntity.getPublicationRelance() != null) {
        			for(PublicationRelanceEntity relance : exerciceComptableEntity.getPublicationRelance()) {
            			publicationRelanceRepository.delete(relance);
            		}        			
        		}
        		        		
        		exerciceComptableRepository.delete(exerciceComptableEntity);
        		exerciceComptableEntities.remove(i);        		
        	}
        	if((exerciceComptableEntity.getDateDebut().isBefore(dateCessation) && exerciceComptableEntity.getDateFin().isAfter(dateCessation))
        			|| exerciceComptableEntity.getDateDebut().compareTo(dateCessation) == 0) {
        		exerciceComptableEntities.get(i).setDateFin(dateCessation);
        	}            	
        }
        return exerciceComptableEntities;
    }
    
    public List<DesinscriptionPieceEntity> ajoutPiecesJointesAgent(List<DesinscriptionPieceDto> pieces, DesinscriptionEntity desinscriptionEntity) {
    	List<DesinscriptionPieceEntity> piecesEntity = new ArrayList<>();
    	for (DesinscriptionPieceDto piece : pieces) {
            DesinscriptionPieceEntity e = desinscriptionPieceTransformer.dtoToModel(piece);
            e.setDateVersementPiece(new Date()); 
            e.setOrigineVersement(OrigineVersementEnum.DESINSCRIPTION_ESPACE);
            e.setDesinscription(desinscriptionEntity);
            piecesEntity.add(e);
        }    	
    	return piecesEntity;    	
    }
    
    /**
     * Fait passer les espaces organisation dont le statut = 'DESINSCRIPTION_VALIDEE_HATVP' en 'DESINSCRIT'
     */
    public void saveDesinscritStatut() {
    	List<EspaceOrganisationEntity> listEspaceOrganisationEntities = this.espaceOrganisationRepository.findEspaceOrganisationADesinscrire();
    	
    	if(!listEspaceOrganisationEntities.isEmpty()) {
    		for(EspaceOrganisationEntity espaceOrganisationEntity: listEspaceOrganisationEntities) {
    			//désactiviation des éventuels clients
    			 desactivationClient(espaceOrganisationEntity);    			
    			
    			espaceOrganisationEntity.setStatut(StatutEspaceEnum.DESINSCRIT);
    			espaceOrganisationEntity.setNewPublication(Boolean.TRUE);
    			this.espaceOrganisationRepository.save(espaceOrganisationEntity);
    			this.espaceOrganisationRepository.flush();
    		}    		
    	}    	
    }
    
    /**
     * Désactivation d'éventuels clients à la désinscription
     * Maj dernière pubClient pour que la désactivation soit publiée
     * @param espaceOrganisationId
     */
    public void desactivationClient(EspaceOrganisationEntity espaceOrganisation) {
    	
    	List<PeriodeActiviteClientEntity> list = this.periodeActiviteClientRepository.findPeriodeActiveByEspaceId(espaceOrganisation.getId());
    	
    	if(list != null) {
    		for(PeriodeActiviteClientEntity periodeActiviteClientEntity : list) {    			   			
        		periodeActiviteClientEntity.setDateDesactivation(LocalDateTime.now());
        		periodeActiviteClientEntity.setCommentaire("DESINSCRIPTION");
        		this.periodeActiviteClientRepository.saveAndFlush(periodeActiviteClientEntity);
        		
        		Optional<OrganisationEntity> organisation = organisationRepository.findById(periodeActiviteClientEntity.getClient().getOrganisation().getId());
        		
        		PubClientEntity lastPublication = pubClientRepository.getLastPubClientByEspaceOrganisationAndorganisation(espaceOrganisation.getId(), organisation.get().getId());
        		if(lastPublication != null) {
        			lastPublication.setDateDesactivation(periodeActiviteClientEntity.getDateDesactivation());
        			lastPublication.setIsAncienClient(true);
        			this.pubClientRepository.saveAndFlush(lastPublication);  
        		}     		      		
        	}
    	}
    	    	
    }
}
