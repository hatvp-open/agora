/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.referentiel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.OrganisationTransformer;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

/**
 * Classe de transformation entre {@link OrganisationDto} et {@link OrganisationEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class OrganisationTransformerImpl
        extends AbstractCommonTansformer<OrganisationDto, OrganisationEntity>
        implements OrganisationTransformer
{

	/** On a besoin de ce repository pour déterminer un espace existe pour l'organisation. */
	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository; 
	
    /**
     * {@inheritDoc}
     */
    @Override
    public OrganisationEntity dtoToModel(final OrganisationDto dto)
    {
        final OrganisationEntity model = new OrganisationEntity();
        RegistreBeanUtils.copyProperties(model, dto);

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrganisationDto modelToDto(final OrganisationEntity model)
    {
        //Copie des champs identiques entre dto et entité.
        final OrganisationDto dto = new OrganisationDto();
        RegistreBeanUtils.copyProperties(dto, model);

        //définir l'identifiant national suivant l'origine de recherche d el'organisation.
        dto.setNationalId(super.determineIdentifiantNational(model));

        // FIXME =============> Ce block doit dégager d'ici, m'enfin....
        // Préciser si l'organisation est rattachée à un espace organisation.
        // On exclut les espaces dont la création a été refusée pour qu'ils puissent être recréés.
        EspaceOrganisationEntity espaceOrganisationEntity = espaceOrganisationRepository
        		.findEspaceOrganisationByOrganisationIdAndStatutNotRefusee(model.getId());
        dto.setOrganizationSpaceExist(espaceOrganisationEntity != null);
        // FIXME ============================================= fin FIXME

        // On considère que l'organisation existe déjà dans la table organisation
        // car on transforme depuis une entité de la table organisation.
        dto.setOrganisationExist(true);

        return dto;

    }
}