/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.nomenclature;

import fr.hatvp.registre.commons.dto.nomenclature.ActionMeneeDto;
import fr.hatvp.registre.persistence.entity.nomenclature.ActionMeneeEntity;

public interface ActionMeneeTransformer extends CommonNomenclatureTransformer<ActionMeneeDto, ActionMeneeEntity> {

}
