/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.email;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;

/**
 *
 * <p>
 * Cette classe envoie les emails suivants:
 * 1- Confirmation de demande d'inscription à un espace organisation
 * 2- Confirmation de demande de création d'espace organisation
 **/
@Component
public class OrganisationEspaceMailer
    extends ApplicationMailer
{

    @Autowired
    public OrganisationEspaceMailer(final JavaMailSender sender,
            final TemplateEngine templateEngine)
    {
        super(sender, templateEngine);
    }

    /** Objet du mail de modification de rôles. */
    @Value("${mail.espace.inscription.roles.subject}")
    private String espaceInscriptionRolesSubject;

    /** Objet du mail d'inscription à un EO. */
    @Value("${mail.espace.inscription.subject}")
    private String espaceInscriptionObject;
    /** Objet du mail de création d'un EO. */
    @Value("${mail.espace.creation.subject}")
    private String espaceCreationObject;

    /** Objet du mail de validation d'un EO. */
    @Value("${mail.espace.validation.subject}")
    private String espaceValidationObject;
    /** Objet du mail de rejet d'un EO. */
    @Value("${mail.espace.rejet.subject}")
    private String espaceRejetSubject;

    /** Template modification des rôles. */
    private static final String editRolesTemplate = "email/espace/edit_roles";

    /** Template demande d'ajout d'un nouvel espace. */
    private static final String addEspaceTemplate = "email/espace/create_espace";
    /** Template demande d'inscription à un espace (déclarant). */
    private static final String joinEspaceTemplate = "email/espace/join_espace";
    /** Template demande d'inscription à un espace (cotact opérationnel). */
    private static final String joinEspaceRequestTemplate = "email/espace/join_espace_request";

    /** Template rejet demande d'inscription à un espace. */
    private static final String rejectEspaceTemplate = "email/espace/reject_access_by_admin";
    /** Template validation demande d'inscription à un espace. */
    private static final String validateEspaceTemplate = "email/espace/validate_access_by_admin";

    /**
     * Envoi de mail de validation d'inscription à un espace organisation
     *
     * @param to destinataire.
     */
    public void sendEditRolesEmail(final DeclarantDto to,
            final InscriptionEspaceDto inscriptionEspaceDto, final String espaceNom)
    {
        this.sendEmail(to, inscriptionEspaceDto, this.espaceInscriptionRolesSubject, espaceNom,
                editRolesTemplate);
    }

    /**
     * Envoi de mail de validation d'inscription à un espace organisation
     *
     * @param to destinataire.
     */
    public void sendJoinEspaceEmailValidated(final DeclarantDto to,
            final InscriptionEspaceDto inscriptionEspaceDto, final String espaceNom)
    {
        this.sendEmail(to, inscriptionEspaceDto, this.espaceValidationObject, espaceNom,
                validateEspaceTemplate);
    }

    /**
     * envoi de mail de rejet d'inscription à un espace organisation
     *
     * @param to destinataire.
     */
    public void sendJoinEspaceEmailRejected(final DeclarantDto to,
            final InscriptionEspaceDto inscriptionEspaceDto, final String espaceNom)
    {
        this.sendEmail(to, inscriptionEspaceDto, this.espaceRejetSubject, espaceNom,
                rejectEspaceTemplate);
    }

    /**
     * envoi de mail de confirmation d'inscription à un espace organisation
     *
     * @param to destinataire.
     */
    public void sendJoinEspaceEmail(final DeclarantDto to,
            final InscriptionEspaceDto inscriptionEspaceDto, final String espaceNom)
    {
        this.sendEmail(to, inscriptionEspaceDto, this.espaceInscriptionObject, espaceNom,
                joinEspaceTemplate);
    }

    /**
     * envoi de mail de demande d'inscription à un espace organisation
     *
     * @param to destinataire.
     */
    public void sendJoinEspaceAdminEmail(final DeclarantDto to,
            final InscriptionEspaceDto inscriptionEspaceDto, final String espaceNom)
    {
        this.sendEmail(to, inscriptionEspaceDto, this.espaceInscriptionObject, espaceNom,
                joinEspaceRequestTemplate);
    }

    /**
     * Envoi de mail de confirmation de création de l'espace organisation
     *
     * @param to destinataire.
     */
    public void sendCreateEspaceEmail(final DeclarantDto to,
            final InscriptionEspaceDto inscriptionEspaceDto, final String espaceNom)
    {
        this.sendEmail(to, inscriptionEspaceDto, this.espaceCreationObject, espaceNom,
                addEspaceTemplate);
    }

    /**
     * Méthode commune d'envoi des emails de l'espace corporate.
     */
    private void sendEmail(final DeclarantDto declarantDto,
            final InscriptionEspaceDto inscriptionEspaceDto, final String title,
            final String espaceNom, final String template)
    {

        final Context ctx = new Context(Locale.FRANCE);
        ctx.setVariable("civilite", declarantDto.getCivility().getLibelleLong());
        ctx.setVariable("denomination", espaceNom);
        ctx.setVariable("url", this.frontRootPath);
        ctx.setVariable("imageResourceName", this.imageResourceName);
        ctx.setVariable("declarantName", inscriptionEspaceDto.getDeclarantNomComplet());
        ctx.setVariable("dateDemande", new SimpleDateFormat("dd/MM/yyyy à HH:mm")
                .format(inscriptionEspaceDto.getDateDemande()));
        ctx.setVariable("date", new SimpleDateFormat("dd/MM/yyyy à HH:mm").format(new Date()));

        if (inscriptionEspaceDto.getRoles() != null) {
            final List<String> roles = inscriptionEspaceDto.getRoles().stream()
                    .map(String::toLowerCase).collect(Collectors.toList());
            if (roles.indexOf("administrateur") > -1)
                roles.set(roles.indexOf("administrateur"),
                        RoleEnumFrontOffice.ADMINISTRATEUR.getLabel().toLowerCase());
            ctx.setVariable("roles", roles);
        }

        final String mailContent = this.templateEngine.process(template, ctx);
        super.prepareAndSendEmailWhithLogo(declarantDto.getEmail(), title, mailContent);
    }

}