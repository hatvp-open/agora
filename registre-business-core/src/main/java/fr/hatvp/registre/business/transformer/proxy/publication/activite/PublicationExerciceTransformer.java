/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication.activite;

import java.util.List;

import fr.hatvp.registre.commons.dto.publication.activite.PublicationExerciceDto;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity;

/**
 * Proxy du transformeur entre {@link fr.hatvp.registre.persistence.entity.publication.activite.common.PublicationEntity} et {@link fr.hatvp.registre.commons.dto.publication.activite.PublicationExerciceDto}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationExerciceTransformer
{
    /**
     * Copier les données de l'exercice dans la publication de l'exercice.
     *
     * @param exercice exercice à transformer.
     * @return une instance de {@link PublicationExerciceDto} contenant les données de l'exercice.
     */
    PublicationExerciceDto exerciceToPublication(final PublicationExerciceEntity exercice);

    /**
     * Copier les données de l'exercice dans la publication de l'exercice.
     *
     * @param exercice exercice à transformer.
     * @return une instance de {@link PublicationExerciceDto} contenant les données de l'exercice.
     */
    PublicationExerciceDto exerciceToPublication(final ExerciceComptableEntity exercice);

    /**
     * Map un dto de publication d'exercice vers une entité de publication d'exercice
     *
     * @param dto publication exercice à transformer.
     * @return une instance de {@link PublicationExerciceEntity} contenant les données de l'exercice.
     */
    PublicationExerciceEntity dtoToModel(PublicationExerciceDto dto);

    /**
     * Map une entité de publication d'exercice vers un dto de publication d'exercice
     *
     * @param entity entité exercice à transformer.
     * @return une instance de {@link PublicationExerciceDto} contenant les données de l'exercice.
     */
    PublicationExerciceDto modelToDto(PublicationExerciceEntity entity);



	List<PublicationExerciceDto> exercicesToPublication(List<PublicationExerciceEntity> models);
}
