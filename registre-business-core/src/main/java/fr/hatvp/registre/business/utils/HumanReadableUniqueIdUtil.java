/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.utils;

import java.util.stream.Collectors;

public class HumanReadableUniqueIdUtil {

	private static final String DEFAULT_DICTIONARY = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	private static final String DEFAULT_KEYWORD = "";

	private static final int DEFAULT_MIN_HASH_LENGTH = 0;
	private static final int MIN_DICTIONARY_LENGTH = 16;
	private static final int GUARD_DIV = 12;

	private final String keyword;
	private final int minLength;
	private final String dictionary;
	private final String guards;

	public HumanReadableUniqueIdUtil(String keyword, int minLength, String dictionary) {
		this.keyword = keyword != null ? keyword : DEFAULT_KEYWORD;
		this.minLength = minLength > 0 ? minLength : DEFAULT_MIN_HASH_LENGTH;
		if (dictionary == null) {
			dictionary = DEFAULT_DICTIONARY;
		}

		dictionary = dictionary.chars().distinct().mapToObj(c -> (char) c).map(c -> Character.toString(c)).collect(Collectors.joining()).replaceAll("[ ]", "");

		if (dictionary.length() < MIN_DICTIONARY_LENGTH) {
			throw new IllegalArgumentException("dictionary is to short. minimum " + MIN_DICTIONARY_LENGTH + " unique characters");
		}

		dictionary = this.consistentShuffle(dictionary, this.keyword);
		// use double to round up
		final int guardCount = (int) Math.ceil((double) dictionary.length() / GUARD_DIV);

		String guards = dictionary.substring(0, guardCount);
		dictionary = dictionary.substring(guardCount);

		this.guards = guards;
		this.dictionary = dictionary;
	}

	public String encode(long number) {

		if (number < 0 && number > Long.MAX_VALUE) {
			throw new IllegalArgumentException("invalid long argument. should be between 0 and max long value");
		}

		final long numberHashInt = number % 100;

		String dictionary = this.dictionary;
		final int dictionaryLength = dictionary.length();

		String returnedHash = String.valueOf(dictionary.charAt((int) (numberHashInt % dictionaryLength)));

		final String randomizer = returnedHash + this.keyword + dictionary;
		dictionary = this.consistentShuffle(dictionary, randomizer.substring(0, dictionaryLength));

		String hash = "";
		do {
			final int index = (int) (number % dictionaryLength);
			if (index >= 0 && index < dictionaryLength) {
				hash = dictionary.charAt(index) + hash;
			}
			number /= dictionaryLength;
		} while (number > 0);

		returnedHash += hash;

		if (returnedHash.length() < this.minLength) {
			long guardIndex = (numberHashInt + (returnedHash.charAt(0))) % this.guards.length();
			char guard = this.guards.charAt((int) guardIndex);

			returnedHash = guard + returnedHash;

			if (returnedHash.length() < this.minLength) {
				guardIndex = (numberHashInt + (returnedHash.charAt(2))) % this.guards.length();
				guard = this.guards.charAt((int) guardIndex);

				returnedHash += guard;
			}
		}

		final int halfLength = dictionaryLength / 2;
		while (returnedHash.length() < this.minLength) {
			dictionary = this.consistentShuffle(dictionary, dictionary);
			returnedHash = dictionary.substring(halfLength) + returnedHash + dictionary.substring(0, halfLength);
			final int excess = returnedHash.length() - this.minLength;
			if (excess > 0) {
				final int startPos = excess / 2;
				returnedHash = returnedHash.substring(startPos, startPos + this.minLength);
			}
		}

		return returnedHash;
	}

	public long decode(String hash) {
		if (hash == null || hash.isEmpty()) {
			throw new IllegalArgumentException("invalid String argument. hash can't be null or empty");
		}

		final String validChars = this.dictionary + this.guards;
		if (!hash.chars().allMatch(c -> validChars.contains(String.valueOf((char) c)))) {
			throw new IllegalArgumentException("invalid character in hash");
		}

		String dictionary = this.dictionary;
		long returnedNumber = 0;

		String hashBreakdown = hash.replaceAll("[" + this.guards + "]", " ");
		final String[] hashArray = hashBreakdown.split(" ");

		int k = 0;
		if (hashArray.length == 3 || hashArray.length == 2) {
			k = 1;
		}

		if (hashArray.length > 0) {
			hashBreakdown = hashArray[k];
			if (!hashBreakdown.isEmpty()) {
				final char lottery = hashBreakdown.charAt(0);

				hashBreakdown = hashBreakdown.substring(1);
				final String randomizer = lottery + this.keyword + dictionary;
				dictionary = this.consistentShuffle(dictionary, randomizer.substring(0, dictionary.length()));

				for (int i = 0; i < hashBreakdown.length(); i++) {
					long pos = dictionary.indexOf(hashBreakdown.charAt(i));
					returnedNumber = returnedNumber * dictionary.length() + pos;
				}
			} else {
				throw new IllegalArgumentException("invalid hash submitted");
			}
		} else {
			throw new IllegalArgumentException("invalid hash submitted");
		}

		if (!this.encode(returnedNumber).equals(hash)) {
			throw new IllegalArgumentException("invalid hash submitted");
		}

		return returnedNumber;
	}

	private String consistentShuffle(String dictionary, String randomizer) {
		if (randomizer.length() <= 0) {
			return dictionary;
		}

		final char[] shuffleArr = dictionary.toCharArray();
		for (int i = shuffleArr.length - 1, v = 0, p = 0; i > 0; i--, v++) {
			v %= randomizer.length();
			final int ascVal = randomizer.charAt(v);
			p += ascVal;
			final int j = (ascVal + v + p) % i;
			final char tmp = shuffleArr[j];
			shuffleArr[j] = shuffleArr[i];
			shuffleArr[i] = tmp;
		}

		return new String(shuffleArr);
	}

}