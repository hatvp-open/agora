/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice.comment;

import fr.hatvp.registre.business.backoffice.comment.CommonCommentService;
import fr.hatvp.registre.business.impl.AbstractCommonService;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

/**
 * classe d'implémentation commune des services de commentaire.
 *
 * @version $Revision$ $Date${0xD}
 */
public abstract class CommonCommentServiceImpl<T extends AbstractCommonDto, E extends AbstractCommonEntity>
    extends AbstractCommonService<T, E>
    implements CommonCommentService<T>
{

}
