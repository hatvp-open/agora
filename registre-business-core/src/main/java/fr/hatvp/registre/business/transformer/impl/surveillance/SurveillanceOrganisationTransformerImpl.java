/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.surveillance;

import java.util.ArrayList;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.GroupBoTransformer;
import fr.hatvp.registre.business.transformer.backoffice.UtilisateurBoTransformer;
import fr.hatvp.registre.business.transformer.proxy.surveillance.SurveillanceOrganisationTransformer;
import fr.hatvp.registre.commons.dto.backoffice.SurveillanceDetailDto;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;

@Component
public class SurveillanceOrganisationTransformerImpl implements SurveillanceOrganisationTransformer {

	@Autowired
	private UtilisateurBoTransformer utilisateurBoTransformer;
	
	@Autowired
	private GroupBoTransformer groupBoTransformer;

	@Override
	public SurveillanceDetailDto modelToDto(SurveillanceEntity entity) {
		SurveillanceDetailDto dto = new SurveillanceDetailDto();
		dto.setOrganisationId(entity.getOrganisationEntity().getId());
		dto.setDenomination(entity.getOrganisationEntity().getDenomination());
		dto.setNomUsage(entity.getOrganisationEntity().getNomUsageSiren() != null ? entity.getOrganisationEntity().getNomUsageSiren() : null);

		dto.setTypeSurveillance(entity.getTypeSurveillance());
		dto.setSurveillants(this.utilisateurBoTransformer.modelToDto(new ArrayList<>(entity.getSurveillants())));
		dto.setSurveillantsGroupes(this.groupBoTransformer.modelToDto(new ArrayList<>(entity.getSurveillantsGroupes())));
		dto.setDateSurveillance(entity.getDateSurveillance().toLocalDate());
		dto.setMotif(entity.getMotif());
		dto.setId(entity.getId());
		return dto;
	}

    @Override
    public SurveillanceEntity dtoToModel(SurveillanceDetailDto dto) {

        SurveillanceEntity model = new SurveillanceEntity();

        model.setMotif(dto.getMotif());
        model.setTypeSurveillance(dto.getTypeSurveillance());
        model.setSurveillants(new HashSet<>(this.utilisateurBoTransformer.dtoToModel(dto.getSurveillants())));
        model.setSurveillantsGroupes(new HashSet<>(this.groupBoTransformer.dtoToModel(dto.getSurveillantsGroupes())));


        return model;
    }


}
