/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.common;

import fr.hatvp.registre.commons.dto.CollaborateurDto;
import fr.hatvp.registre.persistence.entity.espace.common.CommonCollaborateurEntity;

/**
 * Interface de transformation commune des collaborateurs.
 * @version $Revision$ $Date${0xD}
 */
public interface CommonCollaborateurTransformer <E extends CommonCollaborateurEntity>
        extends CommonTransformer<CollaborateurDto, E>
{

}
