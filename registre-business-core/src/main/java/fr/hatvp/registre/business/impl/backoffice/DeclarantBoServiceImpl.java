/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice;

import static fr.hatvp.registre.commons.utils.RegistreUtils.generateNewDate;
import static fr.hatvp.registre.commons.utils.RegistreUtils.randomString;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.backoffice.DeclarantBoService;
import fr.hatvp.registre.business.email.AccountMailer;
import fr.hatvp.registre.business.impl.AbstractCommonService;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.InscriptionEspaceTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantContactRepository;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;

/**
 * Classe de service de gestion des declarants {@link DeclarantEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional(readOnly = true)
public class DeclarantBoServiceImpl extends AbstractCommonService<DeclarantDto, DeclarantEntity> implements DeclarantBoService {

	/**
	 * Repository du service declarant.
	 */
	@Autowired
	private DeclarantRepository declarantRepository;

	/**
	 * Repository du service declarant contact.
	 */
	@Autowired
	private DeclarantContactRepository declarantContactRepository;

	/**
	 * Repository du service des inscriptions.
	 */
	@Autowired
	private InscriptionEspaceRepository inscriptionEspaceRepository;

	/**
	 * Tranformeur de la classe {@link DeclarantEntity}.
	 */
	@Autowired
	private DeclarantTransformer declarantTransformer;

	/**
	 * Tranformeur de la classe {@link InscriptionEspaceEntity}.
	 */
	@Autowired
	private InscriptionEspaceTransformer inscriptionEspaceTransformer;

	/**
	 * Nombre de jours avant expiration de l'adresse email.
	 */
	@Value(value = "${compte.delai.expiration.email.jours}")
	private int nombreDeJoursExpirationEmail;

	/**
	 * Mailer de gestion de compte déclarant.
	 */
	@Autowired
	private AccountMailer accountMailer;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<String> getAllDeclarantOrganisations(final Long id) {
		return this.inscriptionEspaceRepository.findAllByDeclarantId(id).stream().map(i -> i.getEspaceOrganisation().getOrganisation().getDenomination()).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public DeclarantDto switchActivationCompte(final DeclarantDto declarantDto) {
		final DeclarantEntity toto = this.declarantRepository.findOne(declarantDto.getId());

		final DeclarantEntity entity = new DeclarantEntity();
		RegistreBeanUtils.copyProperties(entity, toto);
		entity.setActivated(!entity.isActivated());

		final DeclarantEntity savedEntity = this.declarantRepository.saveAndFlush(entity);
		return this.declarantTransformer.modelToDto(savedEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public DeclarantDto updateEmailDeclarant(final DeclarantDto declarantDto) {

		final DeclarantEntity declarant = this.declarantRepository.findOne(declarantDto.getId());

		final String email = declarantDto.getEmail();
		// Rejeter le nouveau email s'il est le même que l'ancien.
		if (email.equals(declarant.getEmail())) {
			throw new BusinessGlobalException(ErrorMessageEnum.ADRESSE_EMAIL_SAISI_EST_CELLE_QUE_VOUS_UTILISEZ_DEJA.getMessage());
		}
		// Rejeter le nouveau email s'il existe déjà dans la bdd.
		if (this.declarantRepository.findByEmailIgnoreCase(email) != null) {
			throw new EntityExistsException(ErrorMessageEnum.ADRESSE_EMAIL_EXISTE_DEJA.getMessage());
		}
		// Rejeter le nouveau email s'il est en attente de validation par un autre
		// utilisateur.
		final DeclarantEntity declarantEntityTemp = this.declarantRepository.findByEmailTemp(email);
		// si c un autre utilisateur, vérifier si elle a expiré ou pas encore.
		if (declarantEntityTemp != null && !declarantEntityTemp.getId().equals(declarant.getId())) {
			if (!RegistreUtils.isDateExpire(declarantEntityTemp.getEmailCodeExpiryDate())) {
				throw new EntityExistsException(ErrorMessageEnum.EMAIL_EN_ATTENTE_DE_VALIDATION_PAR_UN_AUTRE_UTILISATEUR.getMessage());
			}
			declarantEntityTemp.setEmailTemp(null);
			this.declarantRepository.save(declarantEntityTemp);
		}

		final String confirmationCode = randomString(30);
		declarant.setEmailTemp(email);
		declarant.setEmailCodeExpiryDate(generateNewDate(this.nombreDeJoursExpirationEmail));
		declarant.setEmailConfirmationCode(confirmationCode);
		// enregistrer l'adresse email temporaire et sa date d'expiration.
		final DeclarantEntity declarantEntitySaved = this.declarantRepository.save(declarant);
		final DeclarantDto result = this.declarantTransformer.modelToDto(declarantEntitySaved);
		// Envoyer un email de confirmation de la nouvelle adresse email principale.
		result.setEmail(email);
		this.envoyerEmailDeConfirmationDeModificationDeMail(result, confirmationCode);
		// retourner les données du déclarant enregistré.
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public DeclarantDto validateDeclarantEmailUpdate(final DeclarantDto declarantDto) {

		final DeclarantEntity declarant = this.declarantRepository.findOne(declarantDto.getId());

		final String emailTemp = declarantDto.getEmailTemp();
		declarant.setEmail(emailTemp);
		declarant.setEmailTemp(null);
		// enregistrer l'adresse email temporaire et sa date d'expiration.
		final DeclarantEntity declarantEntitySaved = this.declarantRepository.save(declarant);
		final DeclarantDto result = this.declarantTransformer.modelToDto(declarantEntitySaved);
		result.setEmail(emailTemp);
		// retourner les données du déclarant enregistré.
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InscriptionEspaceDto> getAllDeclarantEspaceOrganisation(final Long declarantId) {
		return this.inscriptionEspaceRepository.findAllByDeclarantId(declarantId).stream().map(this.inscriptionEspaceTransformer::modelToDto)
				.sorted(Comparator.comparing(AbstractCommonDto::getId)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public DeclarantDto updateDeclarant(final DeclarantDto declarantDto) {
		final Optional<DeclarantEntity> optional = Optional.ofNullable(this.declarantRepository.findOne(declarantDto.getId()));

		if (optional.isPresent()) {
			final DeclarantEntity d = optional.get();
			// delete old contacts.
			this.declarantContactRepository.delete(d.getContacts());
			// Transformer le DTO reçu en entité.
			final DeclarantEntity newEntity = this.declarantTransformer.dtoToModel(declarantDto);
			// Mettre a jour le model initial pour récupérer les données que le front ne
			// peut envoyé.
			d.setNom(newEntity.getNom().toUpperCase());
			d.setPrenom(RegistreUtils.normaliser(newEntity.getPrenom()));
			d.setBirthDate(newEntity.getBirthDate());
			d.setCivility(newEntity.getCivility());
			newEntity.getContacts().forEach(dc -> dc.setDeclarant(d));
			d.setContacts(newEntity.getContacts());
			d.setTelephone(newEntity.getTelephone());

			// FIXME: Trouver un moyen pour détacher l'entité
			final DeclarantEntity entity = new DeclarantEntity();
			RegistreBeanUtils.copyProperties(entity, d);
			entity.setVersion(declarantDto.getVersion());

			final DeclarantEntity savedDeclarant = this.declarantRepository.saveAndFlush(entity);
			return this.declarantTransformer.modelToDto(savedDeclarant);
		}

		throw new EntityNotFoundException("Déclarant introuvable");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DeclarantDto> findAllDeclarant() {
		return this.declarantRepository.findAll().stream().map(u -> {
			u.setPassword(null);
			final DeclarantDto declarantDto = this.declarantTransformer.modelToDto(u);
			declarantDto.setListeOrganisationClient(this.getAllDeclarantOrganisations(u.getId()));
			return declarantDto;
		}).sorted(Comparator.comparing(AbstractCommonDto::getId)).collect(Collectors.toList());
	}

	@Override
	public PaginatedDto<DeclarantDto> findPaginatedDeclarant(Integer pageNumber, Integer resultPerPage) {

		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");

		Page<DeclarantEntity> pagedDeclarant = declarantRepository.findAll(pageRequest);

		List<DeclarantDto> declarantList = pagedDeclarant.getContent().stream().map(u -> {
			u.setPassword(null);
			final DeclarantDto declarantDto = this.declarantTransformer.modelToDto(u);
			declarantDto.setListeOrganisationClient(this.getAllDeclarantOrganisations(u.getId()));
			return declarantDto;
		}).sorted(Comparator.comparing(AbstractCommonDto::getId)).collect(Collectors.toList());

		return new PaginatedDto<>(declarantList, pagedDeclarant.getTotalElements());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<DeclarantDto> findPaginatedDeclarantParRecherche(String type, String keywords, Integer pageNumber, Integer resultPerPage) {

		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");

		Page<DeclarantEntity> pagedDeclarant;
		List<DeclarantEntity> listDeclarant;

		switch (type) {
		case "nom":
			listDeclarant = this.declarantRepository.findByNom("%" + keywords + "%", pageRequest.getPageSize(), pageRequest.getOffset());
			pagedDeclarant = new PageImpl<>(listDeclarant, pageRequest, declarantRepository.countDeclarantFilteredByNom("%" + keywords + "%"));
			break;
		case "prenom":
			listDeclarant = this.declarantRepository.findByPrenom("%" + keywords + "%", pageRequest.getPageSize(), pageRequest.getOffset());
			pagedDeclarant = new PageImpl<>(listDeclarant, pageRequest, declarantRepository.countDeclarantFilteredByPrenom("%" + keywords + "%"));
			break;
		case "email":
			listDeclarant = this.declarantRepository.findByEmail("%" + keywords + "%", pageRequest.getPageSize(), pageRequest.getOffset());
			pagedDeclarant = new PageImpl<>(listDeclarant, pageRequest, declarantRepository.countDeclarantFilteredByEmail("%" + keywords + "%"));
			break;
		case "organisations":
			listDeclarant = declarantRepository.findByOrganisation("%" + keywords + "%", pageRequest.getPageSize(), pageRequest.getOffset());
			pagedDeclarant = new PageImpl<>(listDeclarant, pageRequest, declarantRepository.countDeclarantFilteredByEspace("%" + keywords + "%"));
			break;
		default:
			pagedDeclarant = declarantRepository.findAll(pageRequest);
		}

		List<DeclarantDto> declarantList = pagedDeclarant.getContent().stream().map(u -> {
			u.setPassword(null);
			final DeclarantDto declarantDto = this.declarantTransformer.modelToDto(u);
			declarantDto.setListeOrganisationClient(this.getAllDeclarantOrganisations(u.getId()));
			return declarantDto;
		}).sorted(Comparator.comparing(AbstractCommonDto::getId)).collect(Collectors.toList());

		return new PaginatedDto<>(declarantList, pagedDeclarant.getTotalElements());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeclarantDto findOne(final Long declarantId) {
		final DeclarantEntity declarant = this.declarantRepository.findOne(declarantId);
		declarant.setPassword(null);
		final DeclarantDto declarantDto = this.declarantTransformer.modelToDto(declarant);
		declarantDto.setListeOrganisationClient(this.getAllDeclarantOrganisations(declarant.getId()));
		return declarantDto;
	}
	/**
	 * 
	 * @param email
	 * @param needPassword
	 * @return
	 */
	@Override
	public DeclarantDto loadDeclarantByEmail(final String email) {

		final DeclarantEntity declarant = this.declarantRepository.findByEmailIgnoreCase(email);
		if (declarant == null) {
			return null;
		}
		return this.declarantTransformer.modelToDto(declarant);
	}
	/**
	 * @param dto
	 *            le nouvel dto du déclarant.
	 * @param confirmationCode
	 *            le code de confirmation de l'dto.
	 */
	private void envoyerEmailDeConfirmationDeModificationDeMail(final DeclarantDto dto, final String confirmationCode) {
		this.accountMailer.sendEmailUpdateConfirmation(dto, RegistreUtils.codeTo64(confirmationCode));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<DeclarantEntity, Long> getRepository() {
		return this.declarantRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<DeclarantDto, DeclarantEntity> getTransformer() {
		return this.declarantTransformer;
	}

}
