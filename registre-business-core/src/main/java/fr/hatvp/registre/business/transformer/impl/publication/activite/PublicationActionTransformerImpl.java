/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication.activite;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationActionTransformer;
import fr.hatvp.registre.commons.dto.activite.ActionRepresentationInteretDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActionDto;
import fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ActionMeneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DecisionConcerneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ResponsablePublicEntity;

/**
 * Classe du transformeur entre {@link ActionRepresentationInteretDto} et {@link PublicationActionDto}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional(readOnly = true)
public class PublicationActionTransformerImpl
    implements PublicationActionTransformer {

    /**
     * {@inheritDoc}
     */
    @Override
    public PublicationActionDto actionToPublicationAction(final ActionRepresentationInteretEntity action) {
        final PublicationActionDto dto = new PublicationActionDto();

        List<String> actionsMenee = action.getActionsMenees().stream().map(
            ActionMeneeEntity::getLibelle
        ).collect(Collectors.toList());
        dto.setActionsMenees(actionsMenee);
        dto.setActionMeneeAutre(action.getActionMeneeAutre());

        List<String> decisions = action.getDecisionsConcernees().stream().map(
            DecisionConcerneeEntity::getLibelle
        ).collect(Collectors.toList());
        dto.setDecisionsConcernees(decisions);

        List<String> responsables = getResponsablesGroupes(action);

        dto.setReponsablesPublics(responsables);
        dto.setResponsablePublicAutre(action.getResponsablePublicAutre());

        List<String> tiers = action.getTiers().stream().map(d->
        	d.getId()== action.getActiviteRepresentationInteret().getExerciceComptable().getEspaceOrganisation().getOrganisation().getId()?d.getDenomination()+" (en propre)": d.getDenomination()
        ).collect(Collectors.toList());

        dto.setTiers(tiers);

        dto.setObservation(action.getObservation());
        dto.setId(null);
        dto.setVersion(null);
        return dto;
    }

    private List<String> getResponsablesGroupes(ActionRepresentationInteretEntity action) {
    	List<String> result = new ArrayList<>();
    	int countMinistres = 0;
    	int countAai = 0;
    	String ministres = "Membre du Gouvernement ou membre de cabinet ministériel - ";
    	String aai = "Directeur ou secrétaire général, ou leur adjoint, ou membre du collège ou d'une commission des sanctions d'une autorité administrative ou publique indépendante - ";
		for (ResponsablePublicEntity responsable : action.getReponsablesPublics()) {
			if (responsable.getIdCategorie()==1) {
				ministres+=responsable.getLibelle()+", ";
				countMinistres ++;
			}
			else if (responsable.getIdCategorie()==2) {
				aai+=responsable.getLibelle()+", ";
				countAai++;
			}
			else {
				result.add(responsable.getLibelle());
			}
			
		}
		if (countMinistres>0) result.add(ministres.substring(0,ministres.length()-2));
		if (countAai>0) result.add(aai.substring(0,aai.length()-2));
		return result;
	}
    /**
     * {@inheritDoc}
     */
    @Override
    public List<PublicationActionDto> actionToPublicationAction(final List<ActionRepresentationInteretEntity> actions) {
        return actions.stream().map(this::actionToPublicationAction).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PublicationActionDto> actionToPublicationAction(final Stream<ActionRepresentationInteretEntity> models) {
        return models.map(this::actionToPublicationAction).collect(Collectors.toList());
    }


}
