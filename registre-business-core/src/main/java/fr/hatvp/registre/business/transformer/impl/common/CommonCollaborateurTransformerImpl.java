/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.common;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import fr.hatvp.registre.business.transformer.proxy.common.CommonCollaborateurTransformer;
import fr.hatvp.registre.commons.dto.CollaborateurDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.common.CommonCollaborateurEntity;
import fr.hatvp.registre.persistence.entity.publication.PubCollaborateurEntity;
import fr.hatvp.registre.persistence.repository.publication.PubCollaborateurRepository;

/**
 * Classe de transformation commune uux collaborateurs;
 *
 * @version $Revision$ $Date${0xD}
 */
public abstract class CommonCollaborateurTransformerImpl<E extends CommonCollaborateurEntity>
    extends AbstractCommonTansformer<CollaborateurDto, E>
    implements CommonCollaborateurTransformer<E> {

    /**
     * Classe à définir dans les classe filles.
     */
    protected Class<E> entityClass;
    
    @Autowired
    private PubCollaborateurRepository pubCollaborateurRepository;
    /**
     * {@inheritDoc}
     */
    @Override
    public E dtoToModel(final CollaborateurDto dto) {
        final E model;

        try {
            model = this.entityClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new BusinessGlobalException(
                "Erreur d'instanciation dans le transformeur des collaborateurs");
        }

        RegistreBeanUtils.copyProperties(model, dto);
        model.setCivility(dto.getCivilite());
        model.setEnregistrementDate(LocalDateTime.now());

        if (Boolean.TRUE.equals(dto.getInscrit())) {
            final DeclarantEntity declarantEntity = new DeclarantEntity();
            declarantEntity.setId(dto.getDeclarantId());
            model.setDeclarant(declarantEntity);
        }

        final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(dto.getEspaceOrganisationId());
        model.setEspaceOrganisation(espaceOrganisationEntity);

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> dtoToModel(final List<CollaborateurDto> dtos) {
        return dtos.stream().map(this::dtoToModel).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CollaborateurDto> modelToDto(final List<E> models) {
        return models.stream().map(this::modelToDto).sorted(Comparator.comparing(CollaborateurDto::getSortOrder))
            .collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollaborateurDto modelToDto(final CommonCollaborateurEntity model) {
        final CollaborateurDto dto = new CollaborateurDto();
        RegistreBeanUtils.copyProperties(dto, model);

        // Si collaborateur inscrit
        if (model.getDeclarant() != null) {
            dto.setInscrit(true);
            dto.setDeclarantId(model.getDeclarant().getId());
            dto.setNom(model.getDeclarant().getNom());
            dto.setPrenom(model.getDeclarant().getPrenom());
            dto.setCivilite(model.getDeclarant().getCivility());
            dto.setEmail(model.getDeclarant().getEmail());
            
            PubCollaborateurEntity premierePubCollaborateur = this.pubCollaborateurRepository.findFirstByDeclarantAndEspaceOrganisationOrderByPublicationIdAsc(model.getDeclarant(), model.getEspaceOrganisation()); 
            if(premierePubCollaborateur != null) dto.setDatePublication(premierePubCollaborateur.getPublication().getCreationDate());
        } else {
            dto.setInscrit(false);
            dto.setCivilite(model.getCivility());
            
            PubCollaborateurEntity premierePubCollaborateur = this.pubCollaborateurRepository.findFirstByEspaceOrganisationAndNomAndPrenomOrderByPublicationIdAsc(model.getEspaceOrganisation(), model.getNom(), model.getPrenom()); 
            if(premierePubCollaborateur != null) dto.setDatePublication(premierePubCollaborateur.getPublication().getCreationDate());
        }

        dto.setEspaceOrganisationId(model.getEspaceOrganisation().getId());

        return dto;
    }

}
