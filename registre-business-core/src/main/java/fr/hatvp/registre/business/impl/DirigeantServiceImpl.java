/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.DirigeantService;
import fr.hatvp.registre.business.email.SurveillanceMailer;
import fr.hatvp.registre.business.transformer.proxy.DirigeantTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.persistence.entity.espace.DirigeantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import fr.hatvp.registre.persistence.repository.espace.DirigeantRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

/**
 * Service de gestion des dirigeants.
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional(readOnly = true)
@Service
public class DirigeantServiceImpl extends AbstractCommonService<DirigeantDto, DirigeantEntity> implements DirigeantService {

	/**
	 * Repository du service.
	 */
	@Autowired
	private DirigeantRepository dirigeantRepository;

	/**
	 * Transformer du service
	 */
	@Autowired
	private DirigeantTransformer dirigeantTransformer;
	/** Service des publications. */

	@Autowired
	private SurveillanceMailer surveillanceMailer;

	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DirigeantDto> getAllDirigeants(final Long espaceOrganisationId) {
		return this.dirigeantRepository.findByEspaceOrganisationId(espaceOrganisationId).stream()
				.map(this.dirigeantTransformer::modelToDto).sorted(Comparator.comparing(DirigeantDto::getSortOrder)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public DirigeantDto addDirigeant(final DirigeantDto dirigeant, final Long espaceOrganisationId) {
		
		dirigeant.setSortOrder(dirigeantRepository.findByEspaceOrganisationId(espaceOrganisationId).size());
		DirigeantEntity ent = this.dirigeantRepository.saveAndFlush(this.dirigeantTransformer.dtoToModel(dirigeant));

		EspaceOrganisationEntity esp = this.espaceOrganisationRepository.findOne(espaceOrganisationId);
        SurveillanceEntity surveillance = esp.getOrganisation().getSurveillanceEntity();
		if (surveillance!=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.AJOUT_DIRIGEANT)) {
			this.surveillanceMailer.sendSurveillanceMail(esp.getOrganisation(), esp, SurveillanceOrganisationEnum.AJOUT_DIRIGEANT,"");
		}

		return this.dirigeantTransformer.modelToDto(ent);
	}

	/**
	 * {@inheritDoc}
	 * Supprime et recalcul les sortOrder des autres dirigeants
	 */
	@Transactional
	@Override
	public void deleteDirigeant(final Long dirigeantId, final Long espaceOrganisationId) {
		DirigeantEntity entity = Optional.ofNullable(this.dirigeantRepository.findByIdAndEspaceOrganisationId(dirigeantId, espaceOrganisationId))
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.MSG_404.getMessage(), 404));
		
		
		// Recalcul des numéros d'ordre supérieur au collaborateur supprimé
		this.dirigeantRepository.findByEspaceOrganisationId(espaceOrganisationId).stream().filter(e -> e.getSortOrder() > entity.getSortOrder())
						.forEach(e -> e.setSortOrder(e.getSortOrder() - 1));

		this.dirigeantRepository.delete(entity);
		this.dirigeantRepository.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public DirigeantDto editDirigeant(final DirigeantDto dto, final Long espaceOrganisationId) {
		return this.dirigeantTransformer.modelToDto(this.dirigeantRepository.save(this.dirigeantTransformer.dtoToModel(dto)));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<DirigeantEntity, Long> getRepository() {
		return this.dirigeantRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<DirigeantDto, DirigeantEntity> getTransformer() {
		return this.dirigeantTransformer;
	}
	
	/**
	 * {@inheritDoc}
	 * SortOrder +1 du dirigeant que l'on veut faire remonter dans le tableau d'affichage
	 * SortOrder -1 du dirigeant juste au dessus pour le faire descencdre
	 */
	@Transactional
	@Override
	public void remonterDirigeant(final Long id, final Long espaceOrganisationId) {
		final List<DirigeantEntity> collabs = this.dirigeantRepository.findByEspaceOrganisationId(espaceOrganisationId);

		DirigeantEntity col = this.dirigeantRepository.findOne(id);
		
		if (col.getSortOrder() > 0) {
			collabs.stream().filter(filterInactiveAbove(col)).forEach(c -> {
				c.setSortOrder(col.getSortOrder());
				this.dirigeantRepository.save(c);
			});
			col.setSortOrder(col.getSortOrder() - 1);
			this.dirigeantRepository.save(col);
		}

	}

	private Predicate<? super DirigeantEntity> filterInactiveAbove(DirigeantEntity col) {
		return c -> c.getSortOrder().intValue() == col.getSortOrder().intValue() -1;
	}

	/**
	 * {@inheritDoc}
	 * SortOrder -1 du dirigeant que l'on veut faire descendre dans le tableau d'affichage
	 * SortOrder +1 du dirigeant juste en-dessous pour le faire remonter
	 */
	@Transactional
	@Override
	public void descendreDirigeant(final Long id, final Long espaceOrganisationId) {
		final  List<DirigeantEntity> collabs = this.dirigeantRepository.findByEspaceOrganisationId(espaceOrganisationId);

		DirigeantEntity col = dirigeantRepository.findOne(id);

		if (col.getSortOrder() < (collabs.size() - 1)) {
			collabs.stream().filter(filterInactiveUnder(col)).forEach(c -> {
				c.setSortOrder(col.getSortOrder());
				this.dirigeantRepository.save(c);
			});
			col.setSortOrder(col.getSortOrder() + 1);
			this.dirigeantRepository.save(col);
		}

	}

	private Predicate<? super DirigeantEntity> filterInactiveUnder(DirigeantEntity col) {
		return c -> c.getSortOrder().intValue() == col.getSortOrder().intValue() + 1;
	}
}
