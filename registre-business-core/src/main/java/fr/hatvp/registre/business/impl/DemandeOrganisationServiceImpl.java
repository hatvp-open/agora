/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.DemandeOrganisationService;
import fr.hatvp.registre.business.email.DemandeOrganisationBoMailer;
import fr.hatvp.registre.business.email.SurveillanceMailer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.DemandeOrganisationTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.DemandeOrganisationDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.commons.lists.backoffice.StatutDemandeOrganisationEnum;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.repository.espace.DemandeOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

/**
 * Service de gestion des Demandes d'ajout d'une Organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional(readOnly = true)
@Service
public class DemandeOrganisationServiceImpl extends AbstractCommonService<DemandeOrganisationDto, DemandeOrganisationEntity> implements DemandeOrganisationService {

	/** Repository du service. */
	@Autowired
	private DemandeOrganisationRepository demandeOrganisationRepository;

	/** Transformer du service */
	@Autowired
	private DemandeOrganisationTransformer demandeOrganisationTransformer;

	/** Service envoi de mails. */
	@Autowired
	private DemandeOrganisationBoMailer mailer;

	@Autowired
	private DeclarantTransformer declarantTransformer;

	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;

	@Autowired
	private SurveillanceMailer surveillanceMailer;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DemandeOrganisationDto> getDemandesOrganisation() {
		return this.demandeOrganisationRepository.findAll().stream().map(d -> this.demandeOrganisationTransformer.modelToDto(d)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<DemandeOrganisationDto> getPaginatedDemandesOrganisation(final Integer pageNumber, final Integer resultPerPage) {

		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "dateCreation");

		Page<DemandeOrganisationEntity> pagedDemandeOrganisations = demandeOrganisationRepository.findAll(pageRequest);

		List<DemandeOrganisationDto> demandeOrganisations = pagedDemandeOrganisations.getContent().stream()
				.map(demandeOrganisation -> demandeOrganisationTransformer.modelToDto(demandeOrganisation)).collect(Collectors.toList());

		return new PaginatedDto<>(demandeOrganisations, pagedDemandeOrganisations.getTotalElements());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<DemandeOrganisationDto> getDemandesOrganisationParRecherche(String type, String keywords, Integer pageNumber, Integer resultPerPage) {
		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		Page<DemandeOrganisationEntity> pageOrga;
		List<DemandeOrganisationEntity> demandes;
		switch (type) {
		case "denomination":
			demandes = this.demandeOrganisationRepository.findByDenomination("%" + keywords + "%", pageRequest.getPageSize(), pageRequest.getOffset());
			pageOrga = new PageImpl<>(demandes, pageRequest, this.demandeOrganisationRepository.countDemandeFilteredByDenomination("%" + keywords + "%"));
			break;
		case "demandeur":
			demandes = this.demandeOrganisationRepository.findByDemandeur("%" + keywords + "%", pageRequest.getPageSize(), pageRequest.getOffset());
			pageOrga = new PageImpl<>(demandes, pageRequest, this.demandeOrganisationRepository.countDemandeFilteredByDemandeur("%" + keywords + "%"));
			break;
		case "dateDemande":
			pageOrga = this.demandeOrganisationRepository.findByDateCreationBetweenOrderByDateCreationAsc(
					DateTime.parse(keywords, DateTimeFormat.forPattern("ddMMyyyy")).withTimeAtStartOfDay().toDate(),
					DateTime.parse(keywords, DateTimeFormat.forPattern("ddMMyyyy")).withTime(23, 59, 59, 0).toDate(), pageRequest);
			break;
		case "statut":
			switch (keywords) {
			case "A_TRAITER":
				pageOrga = this.demandeOrganisationRepository.findByStatut(StatutDemandeOrganisationEnum.A_TRAITER, pageRequest);
				break;
			case "TRAITEE_ORGANISATION_TROUVEE":
				pageOrga = this.demandeOrganisationRepository.findByStatut(StatutDemandeOrganisationEnum.TRAITEE_ORGANISATION_TROUVEE, pageRequest);
				break;
			case "TRAITEE_ORGANISATION_CREEE":
				pageOrga = this.demandeOrganisationRepository.findByStatut(StatutDemandeOrganisationEnum.TRAITEE_ORGANISATION_CREEE, pageRequest);
				break;
			case "TRAITEE_ORGANISATION_REFUSEE":
				pageOrga = this.demandeOrganisationRepository.findByStatut(StatutDemandeOrganisationEnum.TRAITEE_ORGANISATION_REFUSEE, pageRequest);
				break;
			case "COMPLEMENT_DEMANDE_ENVOYEE":
				pageOrga = this.demandeOrganisationRepository.findByStatut(StatutDemandeOrganisationEnum.COMPLEMENT_DEMANDE_ENVOYEE, pageRequest);
				break;
			case "TOUTES_LES_DEMANDES":
				pageOrga = this.demandeOrganisationRepository.findAll(pageRequest);
				break;
			default:
				pageOrga = this.demandeOrganisationRepository.findAll(pageRequest);
			}
			break;
		case "dernierAccess":
			demandes = this.demandeOrganisationRepository.findByDernierAcces("%" + keywords + "%", pageRequest.getPageSize(), pageRequest.getOffset());
			pageOrga = new PageImpl<>(demandes, pageRequest, this.demandeOrganisationRepository.countDemandeFilteredByDernierAcces("%" + keywords + "%"));

			break;
		default:
			pageOrga = this.demandeOrganisationRepository.findAll(pageRequest);
		}

		List<DemandeOrganisationDto> demandeOrganisations = pageOrga.getContent().stream().map(demandeOrganisation -> demandeOrganisationTransformer.modelToDto(demandeOrganisation))
				.collect(Collectors.toList());

		return new PaginatedDto<>(demandeOrganisations, pageOrga.getTotalElements());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<DemandeOrganisationDto> getPaginatedDemandesOrganisationByStatus(StatutDemandeOrganisationEnum status, Integer pageNumber, Integer resultPerPage) {

		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "dateCreation");

		Page<DemandeOrganisationEntity> pagedDemandeOrganisations = demandeOrganisationRepository.findByStatut(status, pageRequest);

		List<DemandeOrganisationDto> demandeOrganisations = pagedDemandeOrganisations.getContent().stream()
				.map(demandeOrganisation -> demandeOrganisationTransformer.modelToDto(demandeOrganisation)).collect(Collectors.toList());

		return new PaginatedDto<>(demandeOrganisations, pagedDemandeOrganisations.getTotalElements());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DemandeOrganisationDto getDemandeOrganisation(final Long id) {
		return Optional.ofNullable(this.demandeOrganisationRepository.findOne(id)).map(e -> this.demandeOrganisationTransformer.modelToDto(e))
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.MSG_404.getMessage()));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void cloturerDemande(final Long id, final StatutDemandeOrganisationEnum statut) {
		final DemandeOrganisationEntity demande = Optional.ofNullable(this.demandeOrganisationRepository.findOne(id))
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.MSG_404.getMessage()));

		demande.setStatut(statut);

		this.demandeOrganisationRepository.save(demande);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void saveDemandeOrganisation(final DemandeOrganisationDto demande) {
		final DemandeOrganisationEntity model = this.demandeOrganisationTransformer.dtoToModel(demande);
		this.demandeOrganisationRepository.save(model);

		if (demande.getEspaceOrganisationId() != null) {
			EspaceOrganisationEntity espace = this.espaceOrganisationRepository.findOne(demande.getEspaceOrganisationId());
            SurveillanceEntity surveillance = espace.getOrganisation().getSurveillanceEntity();
			if (surveillance !=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.DEMANDE_REFERENTIEL)) {
				this.surveillanceMailer.sendSurveillanceMail(espace.getOrganisation(), espace, SurveillanceOrganisationEnum.DEMANDE_REFERENTIEL,"");
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<DemandeOrganisationEntity, Long> getRepository() {
		return this.demandeOrganisationRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<DemandeOrganisationDto, DemandeOrganisationEntity> getTransformer() {
		return this.demandeOrganisationTransformer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void demandeComplementInformation(final Long id, final String message) {
		final DemandeOrganisationEntity demande = this.demandeOrganisationRepository.findOne(id);

		// statut complément demandé
		demande.setStatut(StatutDemandeOrganisationEnum.COMPLEMENT_DEMANDE_ENVOYEE);

		// envoi email demande complément
		this.mailer.sendDemandeComplementEspaceEmail(this.declarantTransformer.modelToDto(demande.getDeclarant()), message, demande.getDenomination());

		// sauvegarde du changement de statut
		this.demandeOrganisationRepository.save(demande);
	}

	@Override
	public Integer getNombreDemandesStatutNouvelle() {
		return this.demandeOrganisationRepository.findByStatut(StatutDemandeOrganisationEnum.A_TRAITER).size();
	}

}
