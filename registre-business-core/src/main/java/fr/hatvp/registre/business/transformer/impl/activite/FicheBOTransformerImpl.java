/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.activite;

import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.proxy.activite.FicheBOTransformer;
import fr.hatvp.registre.commons.dto.backoffice.activite.ActionBackDto;
import fr.hatvp.registre.commons.dto.backoffice.activite.FicheBackDto;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DecisionConcerneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ResponsablePublicEntity;

@Component
public class FicheBOTransformerImpl implements FicheBOTransformer {

	@Override
	public FicheBackDto modelToDto(ActiviteRepresentationInteretEntity entity) {

		FicheBackDto fiche = new FicheBackDto();
		fiche.setIdFiche(entity.getIdFiche());
		fiche.setId(entity.getId());
		fiche.setObjet(entity.getObjet());
		fiche.setDomaines(entity.getDomainesIntervention().stream()
				.collect(Collectors.groupingBy(DomaineInterventionEntity::getCategorie, Collectors.mapping(DomaineInterventionEntity::getLibelle, Collectors.toList()))));
		fiche.setStatut(entity.getStatut().getLabel());
		fiche.setIsPublication(entity.getPublicationEnabled());
		fiche.setActions(entity.getActionsRepresentationInteret().stream().map(ent -> {
			ActionBackDto action = new ActionBackDto();
			action.setTiers(ent.getTiers().stream().map(OrganisationEntity::getDenomination).collect(Collectors.toList()));
			action.setReponsablesPublics(ent.getReponsablesPublics().stream().collect(Collectors.groupingBy(ResponsablePublicEntity::getCategorie, Collectors.mapping(resp -> {
				return resp.getId().longValue() == 19 ? "Autre: " + ent.getResponsablePublicAutre() : resp.getLibelle();
			}, Collectors.toList()))));
			action.setDecisionsConcernees(ent.getDecisionsConcernees().stream().map(DecisionConcerneeEntity::getLibelle).collect(Collectors.toList()));
			action.setActionsMenees(ent.getActionsMenees().stream().map(act -> {
				return act.getId().longValue() == 10 ? "Autre: " + ent.getActionMeneeAutre() : act.getLibelle();
			}).collect(Collectors.toList()));
			action.setObservation(ent.getObservation() == null || ent.getObservation().isEmpty() ? "Aucune observation" : ent.getObservation());
			return action;
		}).collect(Collectors.toList()));

		return fiche;
	}

}
