/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.EspaceOrganisationLockBoTransformer;
import fr.hatvp.registre.business.transformer.backoffice.ValidationEspaceOrganisationTransformer;
import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationTransformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.OrganisationTransformer;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.backoffice.EspaceOrganisationLockBoDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceBoDto;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;

/**
 * Classe de transformation de la classe {@link EspaceOrganisationEntity}.
 * @version $Revision$ $Date${0xD}
 */
@Component
public class ValidationEspaceOrganisationTransformerImpl
    extends AbstractCommonTansformer<ValidationEspaceBoDto, EspaceOrganisationEntity>
    implements ValidationEspaceOrganisationTransformer {

    /** Transformer pour l'Organisation. */
    @Autowired
    private OrganisationTransformer organisationTransformer;

    /** Transformer pour le Lock Espace Organisation. */
    @Autowired
    private EspaceOrganisationLockBoTransformer espaceOrganisationLockBoTransformer;

    /** Repository des inscriptions. */
    @Autowired
    private InscriptionEspaceRepository inscriptionEspaceRepository;

    /** Transformeur des déclarants. */
    @Autowired
    private DeclarantTransformer declarantTransformer;

    /** Transformeur de l'espace organisation. */
    @Autowired
    private EspaceOrganisationTransformer espaceOrganisationTransformer;
    
    @Autowired
    private PublicationRepository publicationRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationEspaceBoDto modelToDto(final EspaceOrganisationEntity model) {
        final ValidationEspaceBoDto dto = new ValidationEspaceBoDto();
        RegistreBeanUtils.copyProperties(dto, model);

        final EspaceOrganisationDto espaceOrganisationDto = this.espaceOrganisationTransformer
                .modelToDto(model);

        espaceOrganisationDto.setCreateurId(model.getCreateurEspaceOrganisation().getId());
        espaceOrganisationDto
            .setCreateurEmail(model.getCreateurEspaceOrganisation().getEmail());
        espaceOrganisationDto.setCreateurNomComplet(
            model.getCreateurEspaceOrganisation().getCivility().getLibelleLong() + " "
                + model.getCreateurEspaceOrganisation().getNom() + " "
                + model.getCreateurEspaceOrganisation().getPrenom());

        // récupération des contacts opérationnels de l'espace organisation.
        final List<InscriptionEspaceEntity> espaceEntityDeclarantList = this.inscriptionEspaceRepository
                .findAllByActifTrueAndDateSuppressionIsNullAndEspaceOrganisationId(model.getId());
        espaceOrganisationDto.setAdministrateurs(espaceEntityDeclarantList
            .stream()
            .filter(i -> i.getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR))
            .map(InscriptionEspaceEntity::getDeclarant)
                .map(this.declarantTransformer::modelToDto)
            .collect(Collectors.toList()));

        espaceOrganisationDto.setNbrDeclarant(espaceEntityDeclarantList.size());
        dto.setEspaceOrganisation(espaceOrganisationDto);
        dto.setRelancesBloquees(model.isRelancesBloquees());
        dto.setOrganisationData(this.organisationTransformer.modelToDto(model.getOrganisation()));

        //Ajouter l'id de l'inscription en attente de validation
        final Optional<InscriptionEspaceEntity> optional = this.inscriptionEspaceRepository
                .findByDateSuppressionIsNullAndDeclarantIdAndEspaceOrganisationId(
                        model.getCreateurEspaceOrganisation().getId(), model.getId());
        optional.ifPresent(i -> dto.setValidationInscriptionId(i.getId()));

        final List<EspaceOrganisationLockBoDto> locks = model
            .getEspaceOrganisationLockBoEntityList().stream()
            .map(this.espaceOrganisationLockBoTransformer::modelToDto)
            .filter(lock -> lock.getLockTimeRemain() > 0).collect(Collectors.toList());
        if (!locks.isEmpty())
            dto.setLock(locks.get(0));        
        
        dto.setPublicationIdentiteList(getIdentityPublicationHistorique(model.getId()));
        
        return dto;
    }
    
    /**
     * Retourne l'historique des modifications dénominations et noms d'usage pour affichage BO
     * 1ère publi retournée
     * si nb publi > 1 => on compare dénomination et nom avec publi précédente
     * @param espaceOrganisationId
     * @return
     */
    private List<PublicationDto> getIdentityPublicationHistorique(Long espaceOrganisationId){
    	
    	final List<PublicationEntity> publicationsEspace = this.publicationRepository.findAllByEspaceOrganisationIdAndStatutNotOrderByIdDesc(espaceOrganisationId, StatutPublicationEnum.DEPUBLIEE);
        
    	if(publicationsEspace == null || publicationsEspace.isEmpty()) return null;
    	
    	publicationsEspace.sort(Comparator.comparing(PublicationEntity::getCreationDate));
    	
    	List<PublicationDto> publicationIdentiteList = new ArrayList<>();
    	
    	PublicationEntity publicationEntityPrecedente = new PublicationEntity();
        
        for(PublicationEntity publicationEntity : publicationsEspace) {
        	if(publicationEntityPrecedente.getId() == null 
        			|| !publicationEntityPrecedente.getDenomination().equals(publicationEntity.getDenomination())
        			|| (publicationEntityPrecedente.getNomUsage() != null && publicationEntity.getNomUsage() == null)
        			|| (publicationEntityPrecedente.getNomUsage() == null && publicationEntity.getNomUsage() != null)
        			|| (publicationEntityPrecedente.getNomUsage() != null && publicationEntity.getNomUsage() != null && !publicationEntityPrecedente.getNomUsage().equals(publicationEntity.getNomUsage()))) {
        		
        		PublicationDto publicationDto = new PublicationDto();
            	publicationDto.setCreationDate(publicationEntity.getCreationDate());
            	publicationDto.setDenomination(publicationEntity.getDenomination());
            	publicationDto.setNomUsage(publicationEntity.getNomUsage());
            	
            	publicationIdentiteList.add(publicationDto);
        	}
        	publicationEntityPrecedente = publicationEntity;
        	
        }
        
        return publicationIdentiteList;    	
    }
}
