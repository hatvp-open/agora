/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.transformer.proxy.publication.PublicationEspaceTransformer;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.publication.PubActiviteDto;
import fr.hatvp.registre.commons.dto.publication.PubAssoClientDto;
import fr.hatvp.registre.commons.dto.publication.PubCollaborateurDto;
import fr.hatvp.registre.commons.dto.publication.PubDirigeantDto;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.commons.dto.publication.PublicationFrontDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;

/**
 * Classe du transformeur entre {@link PublicationDto} et {@link EspaceOrganisationDto}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional(readOnly = true)
public class PublicationEspaceTransformerImpl implements PublicationEspaceTransformer {

    /**
     * {@inheritDoc}
     */
    @Override
    public PublicationFrontDto espaceToPublication(final EspaceOrganisationDto espaceOrganisationDto) {
        final PublicationFrontDto publicationDto = new PublicationFrontDto();
        RegistreBeanUtils.copyProperties(publicationDto, espaceOrganisationDto);
        
        // Map les collaborateurs d'un espace vers les collaborateurs pour la publication.
        List<PubCollaborateurDto> publicationCollaborateurs = espaceOrganisationDto.getCollaborateurs().stream().map(a -> 
            {
                PubCollaborateurDto dest = new PubCollaborateurDto();
                RegistreBeanUtils.copyProperties(dest, a);
                return dest;
            }
        ).collect(Collectors.toList());
        
        // On ne retient pour la publication que les collaborateurs marqués actifs
        publicationCollaborateurs = publicationCollaborateurs.stream().filter(
                a -> a.getActif()
        ).collect(Collectors.toList()); 
        
        // Map les dirigeants d'un espace vers les dirigeants pour la publication.
        List<PubDirigeantDto> publicationDirigeants = espaceOrganisationDto.getDirigeants().stream().map(a -> 
            {
                PubDirigeantDto dest = new PubDirigeantDto();
                RegistreBeanUtils.copyProperties(dest, a);
                return dest;
            }
        ).collect(Collectors.toList());
        
        // Map les clients d'un espace vers les clients pour la publication.
        List<PubAssoClientDto> publicationClients = espaceOrganisationDto.getClients().stream().map(c -> 
            {
                PubAssoClientDto dest = new PubAssoClientDto();
                RegistreBeanUtils.copyProperties(dest, c);
                return dest;
            }
        ).collect(Collectors.toList());
        
        // Map les assos d'un espace vers les assos pour la publication.
        List<PubAssoClientDto> publicationAffiliations = espaceOrganisationDto.getAssosAppartenance().stream().map(a -> 
            {
                PubAssoClientDto dest = new PubAssoClientDto();
                RegistreBeanUtils.copyProperties(dest, a);
                return dest;
            }
        ).collect(Collectors.toList());
        
        publicationDto.setCollaborateurs(publicationCollaborateurs);
        publicationDto.setDirigeants(publicationDirigeants);
        publicationDto.setClients(publicationClients);
        publicationDto.setAffiliations(publicationAffiliations);
        
        //Publier l'adresse
        publicationDto.setPublierMonAdressePhysique(!espaceOrganisationDto.getNonPublierMonAdressePhysique());
        if (Boolean.TRUE.equals(espaceOrganisationDto.getNonPublierMonAdressePhysique())) {
            publicationDto.setAdresse(null);
        }

        //Publier téléphone de contact
        publicationDto.setPublierMonTelephoneDeContact(!espaceOrganisationDto.getNonPublierMonTelephoneDeContact());
        if (Boolean.TRUE.equals(espaceOrganisationDto.getNonPublierMonTelephoneDeContact())) {
            publicationDto.setTelephoneDeContact(null);
        }

        //Publier email de contact
        publicationDto.setPublierMonAdresseEmail(!espaceOrganisationDto.getNonPublierMonAdresseEmail());
        if (Boolean.TRUE.equals(espaceOrganisationDto.getNonPublierMonAdresseEmail())) {
            publicationDto.setEmailDeContact(null);
        }

        //publier mes clients
        publicationDto.setDeclarationTiers(!espaceOrganisationDto.isNonDeclarationTiers());
        if (Boolean.TRUE.equals(espaceOrganisationDto.isNonDeclarationTiers())) {
            publicationDto.getClients().clear();
        }

        //publier mes associations
        publicationDto.setDeclarationOrgaAppartenance(!espaceOrganisationDto.isNonDeclarationOrgaAppartenance());
        if (espaceOrganisationDto.isNonDeclarationOrgaAppartenance()) {
            publicationDto.getAffiliations().clear();
        }

        final PubActiviteDto pubActiviteDto = new PubActiviteDto();
        pubActiviteDto.setListNiveauIntervention(espaceOrganisationDto.getListNiveauIntervention());
        pubActiviteDto.setListSecteursActivites(espaceOrganisationDto.getListSecteursActivites());
        publicationDto.setActivites(pubActiviteDto);

        publicationDto.setEspaceOrganisationId(espaceOrganisationDto.getId());

        return publicationDto;
    }
}
