/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.nomenclature;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.proxy.nomenclature.ResponsablePublicParentTransformer;
import fr.hatvp.registre.commons.dto.nomenclature.ResponsablePublicDto;
import fr.hatvp.registre.commons.dto.nomenclature.ResponsablePublicParentDto;
import fr.hatvp.registre.persistence.entity.nomenclature.ResponsablePublicEntity;

@Component
public class ResponsablePublicParentTransformerImpl extends AbstractCommonNomenclatureParentTransformer<ResponsablePublicDto, ResponsablePublicParentDto, ResponsablePublicEntity>
		implements ResponsablePublicParentTransformer {

	/** Init super DTO. */
	public ResponsablePublicParentTransformerImpl() {
		super.dtoClass = ResponsablePublicParentDto.class;
	}
}
