/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationBucketTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationTransformer;
import fr.hatvp.registre.commons.dto.publication.PublicationBucketDto;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Classe du transformeur entre {@link PublicationEntity} et {@link PublicationBucketDto}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class PublicationBucketTransformerImpl
        extends AbstractCommonTansformer<PublicationBucketDto, PublicationEntity>
        implements PublicationBucketTransformer {

    /** Transformeur des publications. */
    @Autowired
    private PublicationTransformer publicationTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    public PublicationBucketDto listPublicationToPublicationBucket(final List<PublicationEntity> publications) {

        final PublicationBucketDto bucketDto = new PublicationBucketDto();

        final Optional<PublicationEntity> optional = Optional.ofNullable(publications.get(0));
        if (optional.isPresent()) {
            bucketDto.setLogoType(publications.get(0).getLogoType());
            bucketDto.setLogo(publications.get(0).getLogo());
            bucketDto.setPublicationCourante(this.publicationTransformer.modelToDto(publications.get(0)));
        }

//        bucketDto.setHistorique(this.publicationTransformer.modelToDto(publications));

        return bucketDto;
    }
}
