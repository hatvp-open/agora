/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.nomenclature;

import fr.hatvp.registre.commons.dto.nomenclature.DomaineInterventionDto;
import fr.hatvp.registre.commons.dto.nomenclature.DomaineInterventionParentDto;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;

public interface DomaineInterventionParentTransformer extends CommonNomenclatureParentTransformer<DomaineInterventionDto, DomaineInterventionParentDto, DomaineInterventionEntity> {

}
