/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.activite;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ActionsRepresentationInteretTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ActiviteRepresentationInteretTransformer;
import fr.hatvp.registre.commons.dto.activite.ActiviteRepresentationInteretDto;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;

@Component
public class ActiviteRepresentationInteretTransformerImpl extends AbstractCommonTansformer<ActiviteRepresentationInteretDto, ActiviteRepresentationInteretEntity>
		implements ActiviteRepresentationInteretTransformer {

	@Autowired
	private ActionsRepresentationInteretTransformer actionsRepresentationInteretTransformer;

	@Override
	public ActiviteRepresentationInteretDto modelToDto(final ActiviteRepresentationInteretEntity entity) {
		final ActiviteRepresentationInteretDto dto = new ActiviteRepresentationInteretDto();

		dto.setId(entity.getId());
		dto.setIdFiche(entity.getIdFiche());
		dto.setObjet(entity.getObjet());
		dto.setStatut(entity.getStatut());
		dto.setDomainesIntervention(entity.getDomainesIntervention().stream().map(di -> {
			return di.getId();
		}).collect(Collectors.toSet()));
		dto.setActionsRepresentationInteret(actionsRepresentationInteretTransformer.modelToDto(entity.getActionsRepresentationInteret()));
		dto.setExerciceComptable(entity.getExerciceComptable().getId());
		dto.setDataToPublish(entity.getDataToPublish());

		return dto;
	}

	@Override
	public ActiviteRepresentationInteretEntity dtoToModel(final ActiviteRepresentationInteretDto dto) {
		final ActiviteRepresentationInteretEntity entity = new ActiviteRepresentationInteretEntity();

		entity.setId(dto.getId());
		entity.setObjet(dto.getObjet());
		entity.setStatut(dto.getStatut());
		entity.setDomainesIntervention(dto.getDomainesIntervention().stream().map(di -> {
			return new DomaineInterventionEntity(di);
		}).collect(Collectors.toSet()));
		entity.setActionsRepresentationInteret(actionsRepresentationInteretTransformer.dtoToModel(dto.getActionsRepresentationInteret()));

		return entity;
	}

}
