

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.CommonAssociationTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.AssociationProTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceCollaboratifTransformer;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.persistence.entity.espace.AssociationEntity;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;

/**
 * Classe de transformation de la classe {@link AssociationEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class AssociationTransformerImpl
		extends CommonAssociationTransformerImpl<AssociationEntity>
		implements AssociationProTransformer {
	
	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;
	
	@Autowired
	private PublicationRepository publicationRepository;
	
	@Autowired
	private EspaceCollaboratifTransformer espaceCollaboratifTransformer;
	
	@Autowired
	private DesinscriptionRepository desinscriptionRepository;
	
	
	/** Init superclass entity. */
	public AssociationTransformerImpl() {
		super.entityClass = AssociationEntity.class;
	}

	@Override
	public AssoClientDto modelToDto(AssociationEntity model) {
		AssoClientDto assoClientDto = new AssoClientDto();
		assoClientDto.setId(model.getId());
		assoClientDto.setOrganisationId(model.getOrganisation().getId());
		
		
//		on récupère l'espace ou l'espace non refusé quand il y en a plusieur
		EspaceOrganisationEntity espaceDeLorganisation = espaceOrganisationRepository.findByOrganisationIdUnSeulEspace(model.getOrganisation().getId());
		if(espaceDeLorganisation != null) {
			assoClientDto.setEspaceOrganisationIdDeOrdganisation(espaceDeLorganisation.getId());
			assoClientDto.setEspaceCollaboratifDto(this.espaceCollaboratifTransformer.modelToDto(espaceDeLorganisation));
			assoClientDto.setNomUsage(espaceDeLorganisation.getNomUsage());
		}else {
			assoClientDto.setNomUsage(model.getOrganisation().getNomUsageSiren());
		}
		
		assoClientDto.setEspaceOrganisationId(model.getEspaceOrganisation().getId());
		assoClientDto.setDenomination(model.getOrganisation().getDenomination());
		
		
		if(TypeIdentifiantNationalEnum.SIREN.equals(model.getOrganisation().getOriginNationalId())) {
			assoClientDto.setNationalId(model.getOrganisation().getSiren());
		}else if(TypeIdentifiantNationalEnum.RNA.equals(model.getOrganisation().getOriginNationalId())) {
			assoClientDto.setNationalId(model.getOrganisation().getRna());
		}else if(TypeIdentifiantNationalEnum.HATVP.equals(model.getOrganisation().getOriginNationalId())) {
			assoClientDto.setNationalId(model.getOrganisation().getHatvpNumber());
		}
		
		assoClientDto.setOriginNationalId(model.getOrganisation().getOriginNationalId());
		
		PublicationEntity premierePublication = publicationRepository.findPremierePublicationAssoPro(model.getEspaceOrganisation().getId(), model.getOrganisation().getId());
		
		if(premierePublication != null) {
			assoClientDto.setDatePremierePublication(premierePublication.getCreationDate());
		}
		
		EspaceOrganisationEntity espaceClientAssoEntity = espaceOrganisationRepository.findByOrganisationId(model.getOrganisation().getId());
		
		if(espaceClientAssoEntity != null) {
			DesinscriptionEntity desinscriptionEntity = desinscriptionRepository.findByEspaceOrganisation(espaceClientAssoEntity);
			
			if(desinscriptionEntity != null) assoClientDto.setDateCessation(Date.from(desinscriptionEntity.getCessationDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));
			
		}

		return assoClientDto;
	}

	@Override
	public List<AssoClientDto> modelToDto(List<AssociationEntity> models) {
		List<AssoClientDto> list = new ArrayList<>();
		
		for(AssociationEntity model : models) {
			AssoClientDto assoClientDto = modelToDto(model);
			list.add(assoClientDto);		
		}
		
		return list;
	}
	
	
}
