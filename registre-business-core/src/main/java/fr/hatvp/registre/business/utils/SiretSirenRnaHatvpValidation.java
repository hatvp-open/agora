/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @Link: http://codes-sources.commentcamarche.net/source/42857-verification-de-la-validite-des-codes-siret-et-siren
 * <p>
 * 1- numéro SIREN
 * <p>
 * Il est composé de 8 chiffres, plus un chiffre de contrôle qui permet de vérifier la validité du
 * numéro.
 * La clé de contrôle utilisée pour vérifier de l'exactitude d'un identifiant est une clé "1-2". Le
 * principe est
 * le suivant : on multiplie les chiffres de rang impair à partir de la droite par 1, ceux de rang
 * pair par 2 ; la
 * somme des chiffres obtenus doit être un multiple de 10.
 * <p>
 * exemple : soit le SIREN 732 829 320
 * <p>
 * pour vérifier : 7 3 2 8 2 9 3 2 0
 * rang pair x 2 : 6 16 18 4
 * rang impair x 1 : 7 2 2 3 0
 * <p>
 * ----------------------
 * <p>
 * somme : 7+6+2+1+6+2+1+8+3+4+0 = 40
 * <p>
 * Le numéro est exact.
 * <p>
 * 2- numéro SIRET
 * <p>
 * Le numéro d'identité d'établissement est articulé en deux parties : la première est le n° SIREN
 * de la
 * personne à laquelle appartient l'unité SIRET ; la seconde est un numéro d'ordre à 4 chiffres
 * attribué à
 * l'établissement suivi d'un chiffre de contrôle, qui permet de vérifier la validité de l'ensemble
 * du numéro
 * SIRET. Cette seconde partie est habituellement appelée NIC (numéro interne de classement).
 * <p>
 * Méthode de vérification :
 * Un SIRET est le résultat de la concaténation d'un numéro SIREN à 9 chiffres (dont la clé doit
 * être
 * vérifiée à part) et d'un numéro interne de classement (NIC) complété éventuellement à gauche par
 * des
 * zéros de façon que sa longueur soit de 4 chiffres. Le dernier et 14ème chiffre est une clé (1-2)
 * qui
 * porte sur les 13 premiers chiffres.
 * <p>
 * On multiplie les chiffres de rang impair à partir de la droite par 1, ceux de rang pair par 2 ;
 * la somme
 * des chiffres obtenus est un multiple de 10.
 * <p>
 * Exemple :
 * <p>
 * pour vérifier : 7 3 2 8 2 9 3 2 0 0 0 0 7 4
 * rang pair x 2 : 14 4 4 6 0 0 14
 * rang impair x 1 : 3 8 9 2 0 0 4
 * <p>
 * -----------------------------------
 * <p>
 * somme : 1+4+3+4+8+4+9+6+2+0+0+0+0+1+4+4=50
 * <p>
 * Le numéro est exact. Il correspond au SIRET du 7ème établissement immatriculé par l'entreprise.
 */
// FIXME déplacer dans module commons par soucis de cohérence.
public class SiretSirenRnaHatvpValidation
{

    public static final String SIREN = "SIREN";
    public static final String SIRET = "SIREN";
    public static final String RNA = "RNA";
    public static final String HATVP = "HATVP";
    public static final String WRONGNUMBER = "WRONGNUMBER";
    public static final String NOTVALID = "NOTVALID";

    private String nationalId;
    private String type;

    /**
     * Consctructeur vide.
     */
    public SiretSirenRnaHatvpValidation()
    {
    }

    /**
     * Constructeur avec paramètre.
     *
     * @param nationalId identifiant national en entrée.
     */
    public SiretSirenRnaHatvpValidation(final String nationalId)
    {
        this.setNationalId(nationalId);
    }

    /**
     * Vérifier si le siren en entrée respecte bien la syntaxe.
     *
     * @param siren siren à vérifier.
     * @return true si le SIREN est correct, false sinon.
     */
    private boolean isSirenSyntaxValide(final String siren)
    {
        int total = 0;
        int digit = 0;

        for (int i = 0; i < siren.length(); i++) {
            /*
             * Recherche les positions paires : 2ème, 4ème, 6ème et 8ème chiffre que l'on multiplie
             * par 2
             * petite différence avec la définition ci-dessus car ici on travail de gauche à droite.
             */

            if ((i % 2) == 1) {
                digit = Integer.parseInt(String.valueOf(siren.charAt(i))) * 2;
                /*
                 * si le résultat est >9 alors il est composé de deux digits tous les digits devant
                 * s'additionner et ne pouvant être >19 le calcule devient : 1 + (digit -10) ou :
                 * digit - 9
                 */

                if (digit > 9) {
                    digit -= 9;
                }
            }
            else {
                digit = Integer.parseInt(String.valueOf(siren.charAt(i)));
            }
            total += digit;
        }

        /* Si la somme est un multiple de 10 alors le SIREN est valide */
        return (total % 10) == 0;
    }

    /**
     * Vérifier si le code rna est valide.
     *
     * @param rna code rna à vérifier.
     * @return true si le code est valide, false sinon.
     */
    private boolean isRnaSyntaxValide(final String rna)
    {
        return (StringUtils.startsWith(rna, "w")
                || (StringUtils.startsWith(rna, "W") && (rna.length() == 10)));
    }

    /**
     * Vérifier si le code hatvp est valide.
     *
     * @param hatvp code hatvp à vérifier.
     * @return true si le code est valide, false sinon.
     */
    private boolean isHatvpSyntaxValide(final String hatvp)
    {
        return (StringUtils.startsWith(hatvp, "h")
                || (StringUtils.startsWith(hatvp, "H") && (hatvp.length() == 10)));
    }

    /**
     * Vérifier si la chane de caractère en entrée est bien un SIRET.
     *
     * @param siret siret d'entrée.
     * @return true si le SIRET est correct, sinon false.
     */
    private boolean isSiretSyntaxValide(final String siret)
    {
        int total = 0;
        int digit;

        for (int i = 0; i < siret.length(); i++) {
            /*
             * Recherche les positions impaires : 1er, 3è, 5è, etc... que l'on multiplie par 2
             * petite différence avec la définition ci-dessus car ici on travail de gauche à droite
             */

            if ((i % 2) == 0) {
                digit = Integer.parseInt(String.valueOf(siret.charAt(i))) * 2;
                /*
                 * si le résultat est >9 alors il est composé de deux digits tous les digits devant
                 * s'additionner et ne pouvant être >19 le calcule devient : 1 + (digit -10) ou :
                 * digit - 9
                 */

                if (digit > 9) {
                    digit -= 9;
                }
            }
            else {
                digit = Integer.parseInt(String.valueOf(siret.charAt(i)));
            }
            total += digit;
        }
        /* Si la somme est un multiple de 10 alors le SIRET est valide */
        return (total % 10) == 0;
    }

    /**
     * Méthode qui vérifie si la chaine passée est un nombre.
     * <p>
     * Si le parse réussi alors c'est un nombre sinon une exception est levée
     *
     * @param aString la chaine de caractère à convertir.
     * @return true si c'est un chiffre, false dans le cas contraire
     */
    private boolean isNumber(final String aString)
    {
        try {
            Long.parseLong(aString);
            return true;
        }
        catch (final NumberFormatException nfe) {
            return false;
        }
    }

    /**
     * Vérifie si le SIREN/SIRET passé, est un numérique à 9 ou 14 chiffres
     * Identifie si le num passé est un SIREN ou un SIRET
     *
     * @param nationalId (String)
     */

    public void setNationalId(final String nationalId)
    {
        this.nationalId = nationalId;

        if (!this.isNumber(nationalId)) {
            if (this.isRnaSyntaxValide(nationalId)) {
                this.type = SiretSirenRnaHatvpValidation.RNA;
            }
            else if (this.isHatvpSyntaxValide(nationalId)) {
                this.type = SiretSirenRnaHatvpValidation.HATVP;
            }
            else {
                this.type = SiretSirenRnaHatvpValidation.NOTVALID;
            }
        }
        else {
            if (nationalId.length() == 9) {
                if (this.isSirenSyntaxValide(nationalId)) {
                    this.type = SiretSirenRnaHatvpValidation.SIREN;
                }
                else {
                    this.type = SiretSirenRnaHatvpValidation.WRONGNUMBER;
                }
            }
            else if (nationalId.length() == 14) {
                if (this.isSiretSyntaxValide(nationalId)) {
                    this.type = SiretSirenRnaHatvpValidation.SIRET;
                }
                else {
                    this.type = SiretSirenRnaHatvpValidation.WRONGNUMBER;
                }
            }
            else {
                this.type = SiretSirenRnaHatvpValidation.WRONGNUMBER;
            }
        }
    }

    /**
     * Accesseur en lecture du champ <code>type</code>.
     *
     * @return le champ <code>type</code>.
     */
    public String getType()
    {
        return this.type;
    }

    /**
     * Accesseur en lecture du champ <code>nationalId</code>.
     *
     * @return le champ <code>nationalId</code>.
     */
    public String getNationalId()
    {
        return this.nationalId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return this.getNationalId().hashCode();
    }
}