/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.nomenclature;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.hatvp.registre.business.NomenclatureService;
import fr.hatvp.registre.business.transformer.proxy.nomenclature.ActionMeneeTransformer;
import fr.hatvp.registre.business.transformer.proxy.nomenclature.ChiffreAffaireTransformer;
import fr.hatvp.registre.business.transformer.proxy.nomenclature.DecisionConcerneeTransformer;
import fr.hatvp.registre.business.transformer.proxy.nomenclature.DomaineInterventionParentTransformer;
import fr.hatvp.registre.business.transformer.proxy.nomenclature.MontantDepenseTransformer;
import fr.hatvp.registre.business.transformer.proxy.nomenclature.ResponsablePublicParentTransformer;
import fr.hatvp.registre.commons.dto.nomenclature.ActionMeneeDto;
import fr.hatvp.registre.commons.dto.nomenclature.ChiffreAffaireDto;
import fr.hatvp.registre.commons.dto.nomenclature.DecisionConcerneeDto;
import fr.hatvp.registre.commons.dto.nomenclature.DomaineInterventionParentDto;
import fr.hatvp.registre.commons.dto.nomenclature.MontantDepenseDto;
import fr.hatvp.registre.commons.dto.nomenclature.NomenclatureDto;
import fr.hatvp.registre.commons.dto.nomenclature.ResponsablePublicParentDto;
import fr.hatvp.registre.commons.lists.NomenclatureEnum;
import fr.hatvp.registre.persistence.repository.nomenclature.ActionMeneeRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.ChiffreAffaireRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.DecisionConcerneeRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.DomaineInterventionRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.MontantDepenseRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.ResponsablePublicRepository;

@Service
public class NomenclatureServiceImpl implements NomenclatureService, InitializingBean {

	private static final Map<NomenclatureEnum, List<?>> NOMENCLATURE_MAP = new HashMap<>();

	private static final List<ActionMeneeDto> ACTION_MENEE = new ArrayList<>();

	private static final List<ChiffreAffaireDto> CHIFFRE_AFFAIRE = new ArrayList<>();

	private static final List<DecisionConcerneeDto> DECISION_CONCERNEE = new ArrayList<>();

	private static final List<DomaineInterventionParentDto> DOMAINE_INTERVENTION = new ArrayList<>();

	private static final List<MontantDepenseDto> MONTANT_DEPENSE = new ArrayList<>();

	private static final List<ResponsablePublicParentDto> RESPONSABLE_PUBLIC = new ArrayList<>();

	@Autowired
	private ActionMeneeRepository actionMeneeRepository;

	@Autowired
	private ChiffreAffaireRepository chiffreAffaireRepository;

	@Autowired
	private DecisionConcerneeRepository decisionConcerneeRepository;

	@Autowired
	private DomaineInterventionRepository domaineInterventionRepository;

	@Autowired
	private MontantDepenseRepository montantDepenseRepository;

	@Autowired
	private ResponsablePublicRepository responsablePublicRepository;

	@Autowired
	private ActionMeneeTransformer actionMeneeTransformer;

	@Autowired
	private ChiffreAffaireTransformer chiffreAffaireTransformer;

	@Autowired
	private DecisionConcerneeTransformer decisionConcerneeTransformer;

	@Autowired
	private DomaineInterventionParentTransformer domaineInterventionTransformer;

	@Autowired
	private MontantDepenseTransformer montantDepenseTransformer;

	@Autowired
	private ResponsablePublicParentTransformer responsablePublicTransformer;

	@Override
	public NomenclatureDto getNomenclatures() {
		NomenclatureDto nomenclature = new NomenclatureDto();

		nomenclature.getNomenclature().putAll(NOMENCLATURE_MAP);

		return nomenclature;
	}

	@Override
	public void afterPropertiesSet() throws Exception {

		ACTION_MENEE.addAll(getActionMenee());
		CHIFFRE_AFFAIRE.addAll(getChiffreAffaire());
		DECISION_CONCERNEE.addAll(getDecisionConcernee());
		DOMAINE_INTERVENTION.addAll(getDomaineIntervention());
		MONTANT_DEPENSE.addAll(getMontantDepense());
		RESPONSABLE_PUBLIC.addAll(getResponsablePublic());

		NOMENCLATURE_MAP.put(NomenclatureEnum.ACTION_MENEE, ACTION_MENEE);
		NOMENCLATURE_MAP.put(NomenclatureEnum.CHIFFRE_AFFAIRE, CHIFFRE_AFFAIRE);
		NOMENCLATURE_MAP.put(NomenclatureEnum.DECISION_CONCERNEE, DECISION_CONCERNEE);
		NOMENCLATURE_MAP.put(NomenclatureEnum.DOMAINE_INTERVENTION, DOMAINE_INTERVENTION);
		NOMENCLATURE_MAP.put(NomenclatureEnum.MONTANT_DEPENSE, MONTANT_DEPENSE);
		NOMENCLATURE_MAP.put(NomenclatureEnum.RESPONSABLE_PUBLIC, RESPONSABLE_PUBLIC);

	}

	private List<ActionMeneeDto> getActionMenee() {
		return actionMeneeTransformer.modelToDto(actionMeneeRepository.findAll());
	}

	private List<ChiffreAffaireDto> getChiffreAffaire() {
		return chiffreAffaireTransformer.modelToDto(chiffreAffaireRepository.findAll());
	}

	private List<DecisionConcerneeDto> getDecisionConcernee() {
		return decisionConcerneeTransformer.modelToDto(decisionConcerneeRepository.findAll());
	}

	private List<DomaineInterventionParentDto> getDomaineIntervention() {
		return domaineInterventionTransformer.modelToDto(domaineInterventionRepository.findAll());

	}

	private List<MontantDepenseDto> getMontantDepense() {
		return montantDepenseTransformer.modelToDto(montantDepenseRepository.findAll());
	}

	private List<ResponsablePublicParentDto> getResponsablePublic() {
		return responsablePublicTransformer.modelToDto(responsablePublicRepository.findAll());
	}

}
