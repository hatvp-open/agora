/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.activite.batch;

import fr.hatvp.registre.commons.dto.activite.batch.DeclarationReminderDto;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;

public interface DeclarationReminderTransformer {

	DeclarationReminderDto modelToDto(ExerciceComptableEntity entity);

}
