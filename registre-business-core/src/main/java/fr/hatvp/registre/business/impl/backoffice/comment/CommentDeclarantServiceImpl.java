/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice.comment;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.backoffice.comment.CommentDeclarantBoService;
import fr.hatvp.registre.business.transformer.backoffice.comment.CommentDeclarantBoTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentDeclarantEntity;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentDeclarantRepository;

/**
 * classe d'implémentation de gestion des commentaires des déclarants.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CommentDeclarantServiceImpl extends CommonCommentServiceImpl<CommentBoDto, CommentDeclarantEntity> implements CommentDeclarantBoService {

	/**
	 * Repository de la classe.
	 */
	@Autowired
	private CommentDeclarantRepository commentDeclarantRepository;

	/**
	 * Transformeur de la classe.
	 */
	@Autowired
	private CommentDeclarantBoTransformer commentDeclarantEntityCommentDeclarantBoTransformer;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CommentBoDto> getAllComment(final Long declarantId) {
		return this.commentDeclarantRepository.findByDeclarantEntityId(declarantId).stream().map(this.commentDeclarantEntityCommentDeclarantBoTransformer::commentModelToDto)
				.sorted(Comparator.comparing(CommentBoDto::getDateModification)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public CommentBoDto addComment(final CommentBoDto commentBoDto) {
		final CommentDeclarantEntity entity = this.getTransformer().dtoToModel(commentBoDto);
		entity.setDateCreation(new Date());
		return this.commentDeclarantEntityCommentDeclarantBoTransformer.commentModelToDto(this.commentDeclarantRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public CommentBoDto updateComment(final CommentBoDto commentBoDto) {
		final CommentDeclarantEntity entityT = this.getTransformer().dtoToModel(commentBoDto);
		final CommentDeclarantEntity entityR = this.getRepository().findOne(commentBoDto.getId());
		entityT.setEditeur(entityR.getEditeur());
		entityT.setDateCreation(entityR.getDateCreation());
		entityT.setDateModification(new Date());
		return this.commentDeclarantEntityCommentDeclarantBoTransformer.commentModelToDto(this.commentDeclarantRepository.save(entityT));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<CommentDeclarantEntity, Long> getRepository() {
		return this.commentDeclarantRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<CommentBoDto, CommentDeclarantEntity> getTransformer() {
		return this.commentDeclarantEntityCommentDeclarantBoTransformer;
	}
}
