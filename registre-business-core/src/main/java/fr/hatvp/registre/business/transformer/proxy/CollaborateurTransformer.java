
/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonCollaborateurTransformer;
import fr.hatvp.registre.persistence.entity.espace.CollaborateurEntity;

/**
 * Interface de transformation de la classe {@link CollaborateurEntity}
 * 
 * @version $Revision$ $Date${0xD}
 */
public interface CollaborateurTransformer
    extends CommonCollaborateurTransformer<CollaborateurEntity>
{

}
