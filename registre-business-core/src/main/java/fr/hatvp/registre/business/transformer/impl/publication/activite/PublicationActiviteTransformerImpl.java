/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication.activite;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationActionTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationActiviteTransformer;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActionDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActiviteDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;

/**
 * Classe du transformeur entre {@link PublicationActiviteEntity} et {@link PublicationActiviteDto}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional(readOnly = true)
public class PublicationActiviteTransformerImpl implements PublicationActiviteTransformer {

	private PublicationActionTransformer publicationActionTransformer;

	public PublicationActiviteTransformerImpl(PublicationActionTransformer publicationActionTransformer) {
		this.publicationActionTransformer = publicationActionTransformer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationActiviteDto activiteHistoriqueToPublication(final PublicationActiviteEntity pubActivite) {
		PublicationActiviteDto dto = new PublicationActiviteDto();		
		final ObjectMapper mapper = new ObjectMapper();

			try {
				dto = mapper.readValue(pubActivite.getPublicationJson(), PublicationActiviteDto.class);
			} catch (IOException e) {
				throw new BusinessGlobalException("Impossible de récupérer le json de la publication activite");
			}
			dto.setId(null);
			dto.setVersion(null);
			dto.setPublicationDate(pubActivite.getCreationDate());

		return dto;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationActiviteDto activiteToPublication(final ActiviteRepresentationInteretEntity activite) {
		final PublicationActiviteDto dto = new PublicationActiviteDto();

		dto.setObjet(activite.getObjet());

		List<String> domaines = activite.getDomainesIntervention().stream().map(DomaineInterventionEntity::getLibelle).collect(Collectors.toList());
		dto.setDomainesIntervention(domaines);

		List<PublicationActionDto> actions = this.publicationActionTransformer.actionToPublicationAction(activite.getActionsRepresentationInteret());
		dto.setActionsRepresentationInteret(actions);

		dto.setIdentifiantFiche(activite.getIdFiche());
		dto.setId(null);
		dto.setVersion(null);
		dto.setStatut(activite.getStatut());
		return dto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationActiviteEntity dtoToModel(final PublicationActiviteDto dto) {
		final PublicationActiviteEntity entity = new PublicationActiviteEntity();

		BeanUtils.copyProperties(dto, entity);

		/** convertir la grappe d'objet de publication en string json. */
		final ObjectMapper mapper = new ObjectMapper();
		final String jsonInString;
		try {
			entity.setCreationDate(entity.getCreationDate());
			jsonInString = mapper.writeValueAsString(dto);
			entity.setPublicationJson(jsonInString);
		} catch (final JsonProcessingException e) {
			throw new BusinessGlobalException("Impossible de créer le json de la publication activite");
		}

		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationActiviteDto modelToDto(final PublicationActiviteEntity model) {
		PublicationActiviteDto dto = new PublicationActiviteDto();
		final ObjectMapper mapper = new ObjectMapper();
		try {
			dto = mapper.readValue(model.getPublicationJson(), PublicationActiviteDto.class);
			dto.setId(null);
			dto.setVersion(null);
			dto.setPublicationDate(model.getCreationDate());
			return dto;
		} catch (IOException e) {
			throw new BusinessGlobalException("Impossible de récupérer le json de la publication activite");
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PublicationActiviteEntity> dtoToModel(final List<PublicationActiviteDto> dtos) {
		return dtos.stream().map(this::dtoToModel).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PublicationActiviteDto> modelToDto(final List<PublicationActiviteEntity> models) {
		return models.stream().map(this::modelToDto).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PublicationActiviteDto> modelToDto(final Stream<PublicationActiviteEntity> models) {
		return models.map(this::modelToDto).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PublicationActiviteDto> activitesHistoriqueToPublication(final List<PublicationActiviteEntity> models) {
		return models.stream().map(this::activiteHistoriqueToPublication).collect(Collectors.toList());
	}
}
