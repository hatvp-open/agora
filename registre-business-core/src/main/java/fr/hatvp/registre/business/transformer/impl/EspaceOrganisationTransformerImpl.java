/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.AssociationProTransformer;
import fr.hatvp.registre.business.transformer.proxy.ClientTransformer;
import fr.hatvp.registre.business.transformer.proxy.CollaborateurTransformer;
import fr.hatvp.registre.business.transformer.proxy.DirigeantTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationTransformer;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.batch.EspaceOrganisationPublicationBatchDto;
import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.espace.ClientEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.PeriodeActiviteClientEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

/**
 * Classe de transformation de la classe {@link EspaceOrganisationEntity} & {@link EspaceOrganisationDto}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional(readOnly = true)
public class EspaceOrganisationTransformerImpl extends AbstractCommonTansformer<EspaceOrganisationDto, EspaceOrganisationEntity> implements EspaceOrganisationTransformer {

	/** Transformer des dirigeants. */
	@Autowired
	private DirigeantTransformer dirigeantTransformer;

	/** Transformer des clients. */
	@Autowired
	private ClientTransformer clientTransformer;

	/** Transformer des associations. */
	@Autowired
	private AssociationProTransformer associationProTransformer;

	/** Transformer des collaborateurs. */
	@Autowired
	private CollaborateurTransformer collaborateurTransformer;

	private DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	private DateFormat dateFormatFr = new SimpleDateFormat("dd/MM/yyyy");

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EspaceOrganisationEntity dtoToModel(final EspaceOrganisationDto dto) {
		final EspaceOrganisationEntity model = new EspaceOrganisationEntity();
		RegistreBeanUtils.copyProperties(model, dto);

		model.setDirigeants(dto.getDirigeants().stream().map(this.dirigeantTransformer::dtoToModel).collect(Collectors.toList()));
		
		List<AssoClientDto> allClients = new ArrayList<>();
		allClients.addAll(dto.getClients());
		allClients.addAll(dto.getClientsAnciens());
		
		model.setClients(allClients.stream().map(this.clientTransformer::dtoToModel).collect(Collectors.toList()));

		model.setOrganisationProfessionnelles(dto.getAssosAppartenance().stream().map(this.associationProTransformer::dtoToModel).collect(Collectors.toList()));

		model.setListeDesCollaborateurs(dto.getCollaborateurs().stream().map(this.collaborateurTransformer::dtoToModel).collect(Collectors.toList()));

		model.setNiveauIntervention(RegistreUtils.implodeListWithComma(dto.getListNiveauIntervention()));
		model.setSecteursActivites(RegistreUtils.implodeListWithComma(dto.getListSecteursActivites()));

		return model;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EspaceOrganisationDto modelToDto(final EspaceOrganisationEntity model) {
		final EspaceOrganisationDto dto = new EspaceOrganisationDto();
		RegistreBeanUtils.copyProperties(dto, model);
		String identifiantNational = this.determineIdentifiantNational(model.getOrganisation());
		dto.setNationalId(identifiantNational);
		dto.setTypeIdentifiantNational(model.getOrganisation().getOriginNationalId());
		dto.setDenomination(model.getOrganisation().getDenomination());
		dto.setDirigeants(model.getDirigeants().stream().map(this.dirigeantTransformer::modelToDto).sorted(Comparator.comparing(DirigeantDto::getSortOrder)).collect(Collectors.toList()));
		
		List<ClientEntity> clientEnCours = new ArrayList<>();
		List<ClientEntity> clientAncien = new ArrayList<>();
		
		for(ClientEntity client : model.getClients()) {
			List<PeriodeActiviteClientEntity> periodeActiviteClientEntityListeTriee = client.getPeriodeActiviteClientList().stream().sorted(Comparator.comparing(PeriodeActiviteClientEntity::getDateAjout).reversed()).collect(Collectors.toList());

			if(periodeActiviteClientEntityListeTriee.get(0).getDateDesactivation() == null) {
				clientEnCours.add(client);
			}else {
				clientAncien.add(client);
			}
			
		}
		
		//recupération clients actifs
//		dto.setClients(this.clientTransformer.modelToDto(clientEnCours));
		dto.setClients(this.clientTransformer.modelToDto(model.getClients()));	
		//recupération anciens clients
		dto.setClientsAnciens(this.clientTransformer.modelToDto(clientAncien));
		
		dto.setAssosAppartenance(this.associationProTransformer.modelToDto(model.getOrganisationProfessionnelles()));
		dto.setCollaborateurs(this.collaborateurTransformer.modelToDto(model.getListeDesCollaborateurs()));
		dto.setPublication(model.isPublicationEnabled());
		dto.setHasPublication(!model.getPublications().isEmpty());
		Date d = model.getPublications().stream().max(Comparator.comparing(PublicationEntity::getCreationDate)).map(PublicationEntity::getCreationDate).orElse(null);
		dto.setDateDernierePublication(d == null ? null : dateFormat.format(d));

		if (StringUtils.hasText(model.getSecteursActivites())) {
			final String[] strings = model.getSecteursActivites().split(",");
			for (final String s : strings) {
				dto.getListSecteursActivites().add(SecteurActiviteEnum.valueOf(s));
			}
		}

		if (StringUtils.hasText(model.getNiveauIntervention())) {
			final String[] strings = model.getNiveauIntervention().split(",");
			for (final String s : strings) {
				dto.getListNiveauIntervention().add(NiveauInterventionEnum.valueOf(s));
			}
		}
		dto.setDatePremierePublication(model.getDatePremierePublication() == null ? null : dateFormatFr.format(model.getDatePremierePublication()));

		return dto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EspaceOrganisationPublicationBatchDto modelToPublicationDto(final EspaceOrganisationEntity model) {
		final EspaceOrganisationPublicationBatchDto dto = new EspaceOrganisationPublicationBatchDto();
		RegistreBeanUtils.copyProperties(dto, model);

		String identifiantNational = super.determineIdentifiantNational(model.getOrganisation());

		dto.setNationalId(identifiantNational);
		dto.setTypeIdentifiantNational(model.getOrganisation().getOriginNationalId());

		return dto;
	}

}
