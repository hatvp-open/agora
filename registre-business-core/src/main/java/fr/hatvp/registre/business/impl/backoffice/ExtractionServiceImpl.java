/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.backoffice.ExtractionService;
import fr.hatvp.registre.commons.dto.ChiffresSurUnePeriodeDto;
import fr.hatvp.registre.persistence.repository.activite.ExerciceComptableRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationActiviteRepository;

/**
 * Classe de service des utilisateurs back-office {@link ExtractionService} .
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional(readOnly = true)
public class ExtractionServiceImpl implements ExtractionService {
	
	private final ExerciceComptableRepository exerciceComptableRepository;


	private final PublicationActiviteRepository publicationActiviteRepository;


	@Autowired
	public ExtractionServiceImpl(final ExerciceComptableRepository exerciceComptableRepository, final PublicationActiviteRepository publicationActiviteRepository) {
		this.exerciceComptableRepository = exerciceComptableRepository;
		this.publicationActiviteRepository = publicationActiviteRepository;
	}
	/**
	 * Retourne:
	 *     - nombre de fiches d’activités déposées par les représentants d’intérêts
	 *     - nombre de représentants ayant déclaré des activités
	 * sur une même période date début inclue et date fin exclue
	 */
	@Override
	public ChiffresSurUnePeriodeDto getChiffreSurPeriode(ChiffresSurUnePeriodeDto chiffresSurUnePeriodeDto) throws ParseException {
		
		Integer nbRiActivite = exerciceComptableRepository.getNbRIAyantDeclareActiviteSurPeriode(chiffresSurUnePeriodeDto.getDateDebutPeriode(),chiffresSurUnePeriodeDto.getDateFinPeriode() );
		Integer nbActivitePubliees = publicationActiviteRepository.getNbFicheActiviteDeposeesSurPeriode(chiffresSurUnePeriodeDto.getDateDebutPeriode(),chiffresSurUnePeriodeDto.getDateFinPeriode() );
		
		chiffresSurUnePeriodeDto.setNbActiviteDeposeesRI(nbRiActivite);
		chiffresSurUnePeriodeDto.setNbRIDeclareActivite(nbActivitePubliees);
		
		return chiffresSurUnePeriodeDto;
	}

	
}
