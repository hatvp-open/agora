/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.referentiel;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.RnaReferencielTransformer;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielRnaEntity;

/**
 * Classe de transformation entre {@link OrganisationDto} et {@link ReferentielRnaEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class RnaReferencielTransformerImpl
    extends AbstractCommonTansformer<OrganisationDto, ReferentielRnaEntity>
    implements RnaReferencielTransformer
{

    /**
     * {@inheritDoc}
     */
    @Override
    public ReferentielRnaEntity dtoToModel(final OrganisationDto dto)
    {
        final ReferentielRnaEntity model = new ReferentielRnaEntity();
        RegistreBeanUtils.copyProperties(model, dto);

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrganisationDto modelToDto(final ReferentielRnaEntity model)
    {
        final OrganisationDto dto = new OrganisationDto();
        RegistreBeanUtils.copyProperties(dto, model);
        // Origine de la recherche.
        dto.setOriginNationalId(TypeIdentifiantNationalEnum.RNA);
        dto.setNationalId(model.getRna());
        dto.setOrganisationExist(false);
        dto.setOrganizationSpaceExist(false);

        return dto;
    }
}
