/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.referentiel;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.SireneReferencielTransformer;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielSireneEntity;

/**
 * Classe de transformation entre {@link OrganisationDto} et {@link ReferentielSireneEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class SireneReferencielTransformerImpl
    extends AbstractCommonTansformer<OrganisationDto, ReferentielSireneEntity>
    implements SireneReferencielTransformer
{

    /**
     * {@inheritDoc}
     */
    @Override
    public ReferentielSireneEntity dtoToModel(final OrganisationDto dto)
    {
        final ReferentielSireneEntity model = new ReferentielSireneEntity();
        RegistreBeanUtils.copyProperties(model, dto);

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrganisationDto modelToDto(final ReferentielSireneEntity model)
    {
        final OrganisationDto dto = new OrganisationDto();
        RegistreBeanUtils.copyProperties(dto, model);
        // Origine de la recherche.
        dto.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
        dto.setNationalId(model.getSiren());
        dto.setOrganizationSpaceExist(false);
        dto.setOrganisationExist(false);
        return dto;
    }
}
