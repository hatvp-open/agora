/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.nomenclature;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.proxy.nomenclature.ActionMeneeTransformer;
import fr.hatvp.registre.commons.dto.nomenclature.ActionMeneeDto;
import fr.hatvp.registre.persistence.entity.nomenclature.ActionMeneeEntity;

@Component
public class ActionMeneeTransformerImpl extends AbstractCommonNomenclatureTransformer<ActionMeneeDto, ActionMeneeEntity> implements ActionMeneeTransformer {

	/** Init super DTO. */
	public ActionMeneeTransformerImpl() {
		super.dtoClass = ActionMeneeDto.class;
	}
}
