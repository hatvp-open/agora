/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.activite;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.hatvp.registre.business.ExerciceComptableService;
import fr.hatvp.registre.business.pdf.activite.RapportActivitePdfTemplateData;
import fr.hatvp.registre.business.transformer.proxy.activite.ExerciceComptableEtDeclarationTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ExerciceComptableSimpleTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.batch.DeclarationReminderTransformer;
import fr.hatvp.registre.business.utils.PdfGeneratorUtil;
import fr.hatvp.registre.commons.dto.ListStatutPubDto;
import fr.hatvp.registre.commons.dto.activite.ActiviteRepresentationInteretJsonDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableDatesDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableEtDeclarationDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableJsonDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableSimpleDto;
import fr.hatvp.registre.commons.dto.activite.batch.ContactOpMailingDto;
import fr.hatvp.registre.commons.dto.activite.batch.DeclarationReminderDto;
import fr.hatvp.registre.commons.dto.activite.batch.PublicationRelanceDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActiviteHistoriqueDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationExerciceHistoriqueDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.exceptions.TechnicalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.PublicationRelanceNiveauEnum;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.utilities.LocalDateNowBuilder;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity;
import fr.hatvp.registre.persistence.entity.publication.batch.relance.PublicationRelanceEntity;
import fr.hatvp.registre.persistence.repository.activite.ActiviteRepresentationInteretRepository;
import fr.hatvp.registre.persistence.repository.activite.ExerciceComptableRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationActiviteRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationExerciceRepository;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.ChiffreAffaireRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.MontantDepenseRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;
import fr.hatvp.registre.persistence.repository.publication.batch.relance.PublicationRelanceRepository;

/**
 * Service de gestion des exercices comptables
 *
 */
@Service
@Transactional
public class ExerciceComptableServiceImpl implements ExerciceComptableService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExerciceComptableServiceImpl.class);

	private static final String RAPPORT_PDF_TEMPLATE = "pdf/rapport/pdf";

	@Autowired
	private ExerciceComptableRepository exerciceComptableRepository;

	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;

	@Autowired
	private ChiffreAffaireRepository chiffreAffaireRepository;

	@Autowired
	private MontantDepenseRepository montantDepenseRepository;

	@Autowired
	private DeclarantRepository declarantRepository;
	@Autowired
	private PublicationRepository publicationRepository;

	@Autowired
    private PublicationActiviteRepository publicationActiviteRepository;

	@Autowired
    private PublicationExerciceRepository publicationExerciceRepository;

	@Autowired
	private PublicationRelanceRepository publicationRelanceRepository;

    @Autowired
    private DesinscriptionRepository desinscriptionRepository;

    @Autowired
    private ActiviteRepresentationInteretRepository activiteRepresentationInteretRepository;

    @Autowired
	private ExerciceComptableSimpleTransformer exerciceComptableSimpleTransformer;

	@Autowired
	private ExerciceComptableEtDeclarationTransformer exerciceComptableEtDeclarationTransformer;

	@Autowired
	private DeclarationReminderTransformer declarationReminderTransformer;

	@Autowired
	private PdfGeneratorUtil pdfGenerator;

	@Autowired
	private LocalDateNowBuilder localDateNowBuilder;

	@Value("${batch.watchdog.date.month.after.cloture}")
	private long monthAfterCloture;
	@Value("${batch.watchdog.date.day.before.prealable1}")
	private long dayBeforeCloture1;
	@Value("${batch.watchdog.date.day.before.prealable2}")
	private long dayBeforeCloture2;
	@Value("${batch.watchdog.date.day.after.relance1}")
	private long dayAfterRelance1;
	@Value("${batch.watchdog.date.day.after.relance2}")
	private long dayAfterRelance2;
	@Value("${batch.watchdog.date.day.after.blacklist}")
	private long dayAfterBlacklist;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ExerciceComptableSimpleDto> findAllBasicByEspaceOrganisationId(final Long currentEspaceOrganisationId) {
		// verification que l'EC a bien déja fait une publication de son espace
		if (!this.publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(currentEspaceOrganisationId, StatutPublicationEnum.DEPUBLIEE).isPresent()) {
			throw new BusinessGlobalException(ErrorMessageEnum.ACCES_ACTIVITE_NO_PUBLICATION.getMessage(), 410);
		}
		
		final List<ExerciceComptableEntity> exerciceComptableEntities = checkExerciceComptableExistence(currentEspaceOrganisationId);

		return exerciceComptableSimpleTransformer.modelToDto(exerciceComptableEntities);
	}

    /**
	 * {@inheritDoc}
	 */
	@Override
	public List<ExerciceComptableEtDeclarationDto> findAllWithDeclarationByEspaceOrganisationId(final Long currentEspaceOrganisationId) {
		
		// verification que l'EC a bien déja fait une publication de son espace
		if (!this.publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(currentEspaceOrganisationId, StatutPublicationEnum.DEPUBLIEE).isPresent()) {
			throw new BusinessGlobalException(ErrorMessageEnum.ACCES_ACTIVITE_NO_PUBLICATION.getMessage(), 410);
		}

		final List<ExerciceComptableEntity> exerciceComptableEntities = checkExerciceComptableExistence(currentEspaceOrganisationId);

		return exerciceComptableEtDeclarationTransformer.modelToDtoSansActiviteSupprimee(exerciceComptableEntities);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ExerciceComptableEtDeclarationDto> findAllWithDeclarationByEspaceOrganisationIdBack(final Long currentEspaceOrganisationId) {
		 List<ExerciceComptableEtDeclarationDto> listECetDeclarations = new ArrayList<>();
		 
	    EspaceOrganisationEntity EO = espaceOrganisationRepository.findOne(currentEspaceOrganisationId);
       
	    //gestion cas des affiliations sans espace
        if(EO.getExercicesComptable() != null) {
        	listECetDeclarations = exerciceComptableEtDeclarationTransformer.modelToDto(EO.getExercicesComptable());
        }
        		
        for(ExerciceComptableEtDeclarationDto EcEtDeclarationDto : listECetDeclarations){
            EcEtDeclarationDto.setDateFirstCommMoyens(publicationExerciceRepository.findDateFirstCommMoyens(EcEtDeclarationDto.getId()));
            EcEtDeclarationDto.setDateLastChangeMoyens(publicationExerciceRepository.findDateLastChangeMoyens(EcEtDeclarationDto.getId()));
            EcEtDeclarationDto.getActiviteSimpleDto()
                .stream()
                .forEach(e-> {
                    e.setDateLastChange(publicationActiviteRepository.getLatestPublicationDate(e.getId()));
                    e.setDateFirstComm(publicationActiviteRepository.getFirstPublicationDate(e.getId()));
                    
                    List<PublicationActiviteHistoriqueDto> list = new ArrayList<>();
                    
                    List<PublicationActiviteEntity> pubList = publicationActiviteRepository.findByActiviteId(e.getId());
                    
                    for (PublicationActiviteEntity publicationActiviteEntity : pubList) {
                    	try {
                			// Conversion du JSON sauvegardé en DTO.
                			ObjectMapper mapper = new ObjectMapper();
                			ActiviteRepresentationInteretJsonDto ex =  mapper.readerFor(ActiviteRepresentationInteretJsonDto.class).readValue(publicationActiviteEntity.getPublicationJson());
                			
                			PublicationActiviteHistoriqueDto publicationHistoriqueDto = new PublicationActiviteHistoriqueDto();
                			
                			publicationHistoriqueDto.setCreationDate(publicationActiviteEntity.getCreationDate());
                			publicationHistoriqueDto.setPublicationActiviteId(publicationActiviteEntity.getId());
                			publicationHistoriqueDto.setObjet(ex.getObjet());
                			
                			list.add(publicationHistoriqueDto);
                			
                    	} catch (IOException exp) {
                			throw new TechnicalException(ErrorMessageEnum.TYPES_CATEGORIES_AS_JSON_IMPOSSIBLE.getMessage(), exp);
                		}
                    }
                    e.setActiviteHistoriquePublicationList(list);
                });
            
            //récupération historique des données publiées
            EcEtDeclarationDto.setExerciceHistoriquePublicationList(getHistoriqueExercice(EcEtDeclarationDto.getId()));            
        }
		// On parcours la liste et on va chercher la date de premiere comm et la date de last modif en fonction de l'id de l'activite et on set les variables du dto
        return listECetDeclarations;
	}

	/**
	 * Méthode permettant de calculer les exercices comptables à retourner
	 *
	 * @param currentEspaceOrganisationId
	 * @return
	 */
	private List<ExerciceComptableEntity> checkExerciceComptableExistence(final Long currentEspaceOrganisationId) {
		final EspaceOrganisationEntity espaceOrganisationEntity = espaceOrganisationRepository.findOne(currentEspaceOrganisationId);
        final Optional<DesinscriptionEntity> desinscriptionEntity = Optional.ofNullable(this.desinscriptionRepository.findByEspaceOrganisation(espaceOrganisationEntity));
		//date du jour
		LocalDate dateCourante = LocalDate.now();
		// on récupere le dernier exercice existant
		List<ExerciceComptableEntity> exerciceComptableEntities = espaceOrganisationEntity.getExercicesComptable();
		ExerciceComptableEntity lastExercice = exerciceComptableEntities.stream().max(Comparator.comparing(ExerciceComptableEntity::getDateFin)).get();

        DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String stringDateCloture = espaceOrganisationEntity.getFinExerciceFiscal();
        LocalDate dateClotureAnneeCourante = LocalDate.parse(stringDateCloture + "-" + new Integer(lastExercice.getDateFin().getYear()+1), dtFormatter);
		
        //cas d'une désincription: création d'un nouvel exercice comptable si la date courrante est entre la date de fin du dernier 
        //exercice existant et la date de cessation
        if(desinscriptionEntity.isPresent() && desinscriptionEntity.get().getCessationDate() != null){
            if (dateCourante.isAfter(lastExercice.getDateFin()) 
            		&& dateCourante.isBefore(desinscriptionEntity.get().getCessationDate())) {
                exerciceComptableEntities.add(new ExerciceComptableEntity(lastExercice.getDateFin().plusDays(1),dateClotureAnneeCourante,espaceOrganisationEntity));
                espaceOrganisationEntity.setNewPublication(Boolean.TRUE);
                espaceOrganisationRepository.save(espaceOrganisationEntity);
                espaceOrganisationRepository.flush();
            }
        }else{
            if (dateCourante.isAfter(lastExercice.getDateFin())) {
                exerciceComptableEntities.add(new ExerciceComptableEntity(lastExercice.getDateFin().plusDays(1),dateClotureAnneeCourante,espaceOrganisationEntity));
                espaceOrganisationEntity.setNewPublication(Boolean.TRUE);
                espaceOrganisationRepository.save(espaceOrganisationEntity);
                espaceOrganisationRepository.flush();
            }
        }

		return exerciceComptableEntities;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExerciceComptableSimpleDto updateExerciceComptable(final Long exerciceId, final ExerciceComptableSimpleDto exerciceComptable, final Long currentEspaceOrganisationId) {

		ExerciceComptableEntity exerciceComptableEntity = getExerciceComptable(exerciceId);

		checkEspaceOrganisation(currentEspaceOrganisationId, exerciceComptableEntity.getEspaceOrganisation().getId());

		checkChiffreAffaireData(exerciceComptable, exerciceComptableEntity);

		Optional.ofNullable(exerciceComptable.getMontantDepense()).ifPresent(ma -> exerciceComptableEntity.setMontantDepense(montantDepenseRepository.getOne(ma)));
		exerciceComptableEntity.setNombreSalaries(exerciceComptable.getNombreSalaries());

		exerciceComptableEntity.setDataToPublish(Boolean.TRUE);
		exerciceComptableEntity.getEspaceOrganisation().setNewPublication(Boolean.TRUE);
		exerciceComptableEntity.setCommentaire(exerciceComptable.getCommentaire());
		
		return exerciceComptableSimpleTransformer.modelToDto(exerciceComptableRepository.save(exerciceComptableEntity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExerciceComptableSimpleDto updateExerciceComptableNoActivite(final Long exerciceId, final ExerciceComptableSimpleDto exerciceComptable, final Long currentEspaceOrganisationId) {

		ExerciceComptableEntity exerciceComptableEntity = getExerciceComptable(exerciceId);

		checkEspaceOrganisation(currentEspaceOrganisationId, exerciceComptableEntity.getEspaceOrganisation().getId());

		if (!exerciceComptableEntity.getActivitesRepresentationInteret().isEmpty() || exerciceComptableEntity.getDateFin().isAfter(LocalDate.now())
				|| exerciceComptableEntity.isNoActivite()) {
			throw new BusinessGlobalException(ErrorMessageEnum.EXERCICE_PAS_DE_DECLARATION_IMPOSSIBLE.getMessage());
		}

		exerciceComptableEntity.setNoActivite(true);
		exerciceComptableEntity.setBlacklisted(false);
		exerciceComptableEntity.setAfficheBlacklist(false);
		exerciceComptableEntity.setDataToPublish(Boolean.TRUE);
		exerciceComptableEntity.getEspaceOrganisation().setNewPublication(Boolean.TRUE);

		return exerciceComptableSimpleTransformer.modelToDto(exerciceComptableRepository.save(exerciceComptableEntity));
	}

	@Override
	public ExerciceComptableSimpleDto updateDatesExerciceComptable(Long exerciceId, ExerciceComptableDatesDto exerciceComptable, Long espaceOrganisationId) {

		if (exerciceComptable.getDateDebut().isAfter(exerciceComptable.getDateFin())) {
			throw new BusinessGlobalException(ErrorMessageEnum.DATE_EXERCICE_ERROR.getMessage());
		}

		ExerciceComptableEntity exerciceComptableEntity = getExerciceComptable(exerciceId);
		exerciceComptableEntity.setDateDebut(exerciceComptable.getDateDebut());
		exerciceComptableEntity.setDateFin(exerciceComptable.getDateFin());

		return exerciceComptableSimpleTransformer.modelToDto(exerciceComptableRepository.save(exerciceComptableEntity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public Pair<byte[], String> buildPdf(Long exerciceId, Boolean withMoyens, List<StatutPublicationEnum> statuts, Long currentEspaceOrganisationId) {
		ExerciceComptableEntity exerciceComptable = getExerciceComptable(exerciceId);

		checkEspaceOrganisation(currentEspaceOrganisationId, exerciceComptable.getEspaceOrganisation().getId());

		int nbDeclarationsActivites = exerciceComptable.getActivitesRepresentationInteret().size();

		List<ActiviteRepresentationInteretEntity> activites = exerciceComptable.getActivitesRepresentationInteret().stream().filter(a -> statuts.contains(a.getStatut()))
				.sorted((a1, a2) -> Long.compare(a2.getId(), a1.getId())).collect(Collectors.toList());

		exerciceComptable.setActivitesRepresentationInteret(activites);

		RapportActivitePdfTemplateData data = new RapportActivitePdfTemplateData(exerciceComptable);

		Map<String, Object> pdfData = new HashMap<>();
		pdfData.put("exercice", data);
		pdfData.put("moyens", withMoyens);
		pdfData.put("nbActivites", nbDeclarationsActivites);

		byte[] pdf = pdfGenerator.createPdf(RAPPORT_PDF_TEMPLATE, pdfData);

		String title = "agora-export-periode-du-" + data.getDateDebut() + "-au-" + data.getDateFin() + "-le-" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy-HH-mm"));

		return Pair.of(pdf, title);
	}

	private ExerciceComptableEntity getExerciceComptable(final Long exerciceId) {
		return Optional.ofNullable(exerciceComptableRepository.findOne(exerciceId))
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.EXERCICE_COMPTABLE_INEXISTANT.getMessage()));
	}

	private void checkEspaceOrganisation(final Long currentEspaceOrganisationId, final Long testId) {
		if (currentEspaceOrganisationId.longValue() != testId.longValue()) {
			throw new BusinessGlobalException(ErrorMessageEnum.MAUVAIS_ESPACE.getMessage());
		}
	}

	private void checkChiffreAffaireData(final ExerciceComptableSimpleDto exerciceComptable, ExerciceComptableEntity exerciceComptableEntity) {
		if (Boolean.TRUE.equals(exerciceComptable.getHasNotChiffreAffaire())) {
			exerciceComptableEntity.setChiffreAffaire(null);
			exerciceComptableEntity.setHasNotChiffreAffaire(exerciceComptable.getHasNotChiffreAffaire());
		} else {
			Optional.ofNullable(exerciceComptable.getChiffreAffaire()).ifPresent(ca -> exerciceComptableEntity.setChiffreAffaire(chiffreAffaireRepository.getOne(ca)));
			exerciceComptableEntity.setHasNotChiffreAffaire(Boolean.FALSE);
		}

		if (!exerciceComptableEntity.getHasNotChiffreAffaire() && !Optional.ofNullable(exerciceComptableEntity.getChiffreAffaire()).isPresent()) {
			throw new BusinessGlobalException(ErrorMessageEnum.PAS_DE_RENSEIGNEMENT_CA.getMessage());
		}
	}

	@Override
	public ListStatutPubDto getListStatutPubDto() {

		final ListStatutPubDto dto = new ListStatutPubDto();
		for (final StatutPublicationEnum statutActiviteEnum : StatutPublicationEnum.values()) {
			dto.getStatutActiviteEnumList().add(statutActiviteEnum);
		}

		return dto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DeclarationReminderDto> getExerciceWithoutDeclarationByDate(LocalDate today) {
		int monthAdjustement = 0;
		// Ajuste en fonction du nombre de jours dans le mois (30 ou 31)
		if (today.lengthOfMonth() < today.minusMonths(this.monthAfterCloture).lengthOfMonth()) {
			monthAdjustement = today.minusMonths(this.monthAfterCloture).lengthOfMonth() - today.lengthOfMonth();
		}
		// liste des exercices comptable qui sont clos après la date de 1ere publication de l'EC pour les EC avec statut 'ACCEPTEE' et qui ne sont pas blackliste
		List<ExerciceComptableEntity> exs = exerciceComptableRepository
				.findLastExerciceForEachEspaceByDateFinBefore(today.minusMonths(this.monthAfterCloture).plusDays(this.dayBeforeCloture1).plusDays(monthAdjustement));
		List<DeclarationReminderDto> list = new ArrayList<>();
		for (ExerciceComptableEntity ex : exs) {
			boolean aucunePublicationMoyens = false;
			boolean aucunePublicationActivites = false;
			if (StatutPublicationEnum.NON_PUBLIEE.equals(ex.getStatut())) {
				aucunePublicationMoyens = true;
			}
			if (ex.getActivitesRepresentationInteret().stream().filter(act -> !StatutPublicationEnum.NON_PUBLIEE.equals(act.getStatut())).count() == 0 && !ex.isNoActivite()) {
				aucunePublicationActivites = true;
			}
			// si pas de moyens ou pas d'activite on construit les relances
			if (aucunePublicationMoyens || aucunePublicationActivites) {
				DeclarationReminderDto dto = this.declarationReminderTransformer.modelToDto(ex);
				// relance préalable 14 jours avant date cloture + 3 mois
				if (Boolean.TRUE.equals(needRelance(ex, today))) {
					if(ex.getPublicationRelance().isEmpty()) {
						dto.setNiveauRelance(PublicationRelanceNiveauEnum.P);
					}else {
						continue;
					}						
				}
				// si l'exercice a déja une relance de niveau R2 on récupère sa date d'envoi pour calculer si il faut creer relance Blacklist
//				else if (Boolean.TRUE.equals(hasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnum(ex, PublicationRelanceNiveauEnum.R2))) {
//					LocalDate minDate = getMinDate(ex, PublicationRelanceNiveauEnum.R2);
//					if (Boolean.TRUE.equals(checkDateRelanceNblR2(minDate, this.dayAfterRelance2, this.dayAfterBlacklist, today))) {
//						continue;
//					}
//					dto.setNiveauRelance(PublicationRelanceNiveauEnum.NBL);
//				}
				// si l'exercice a déja une relance de niveau R1 on récupère sa date d'envoi pour calculer si il faut creer relance R2
				else if (Boolean.TRUE.equals(hasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnum(ex, PublicationRelanceNiveauEnum.R1))) {
					LocalDate minDate = getMinDate(ex, PublicationRelanceNiveauEnum.R1);
					if (Boolean.TRUE.equals(checkDateRelanceNblR2(minDate, this.dayAfterRelance1, this.dayAfterRelance2, today))) {
						continue;
					}
					dto.setDateRelance(minDate);
					dto.setNiveauRelance(PublicationRelanceNiveauEnum.R2);
				} else {
					int endOfMonthAdjustment = getEndOfMonthAdjustment(ex);
					if (ex.getDateFin().plusMonths(this.monthAfterCloture).plusDays(endOfMonthAdjustment).plusDays(this.dayAfterRelance1).isAfter(today)) {
						continue;
					}
					dto.setNiveauRelance(PublicationRelanceNiveauEnum.R1);
				}
				dto.setAucunePublicationMoyens(aucunePublicationMoyens);
				dto.setAucunePublicationActivites(aucunePublicationActivites);
				list.add(dto);
			} else {
				ex.setDeclarationDone(Boolean.TRUE);
				exerciceComptableRepository.save(ex);
			}
		}
		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<Long, DeclarationReminderDto> getFailedReminderMail() {
		List<PublicationRelanceEntity> failedRelance = this.publicationRelanceRepository.findAllByEnvoyeFalse();
		Map<Long, DeclarationReminderDto> relanceToDo = failedRelance.stream().map(rel -> {
			DeclarationReminderDto reminder = new DeclarationReminderDto(rel.getNiveau(), rel.getType(),
					rel.getExerciceComptable().getEspaceOrganisation().getOrganisation().getDenomination(), rel.getExerciceComptable().getDateDebut(),
					rel.getExerciceComptable().getDateFin(), rel.getExerciceComptable().getActivitesRepresentationInteret().stream().count());
			if (rel.getNiveau().equals(PublicationRelanceNiveauEnum.R1)) {
				reminder.setDateRelance(rel.getCreationDate().toLocalDate());
			}
			reminder.setContactOp(Arrays.asList(new ContactOpMailingDto[] {
					new ContactOpMailingDto(rel.getContactOp().getCivility(), rel.getContactOp().getNom(), rel.getContactOp().getPrenom(), rel.getContactOp().getEmail()) }));
			return Pair.of(rel.getId(), reminder);
		}).collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
		return relanceToDo;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void savePublicationRelance(List<PublicationRelanceDto> relanceDtos, LocalDate today) {
		List<PublicationRelanceEntity> relanceEntities = relanceDtos.stream().map(dto -> {
			return new PublicationRelanceEntity(exerciceComptableRepository.findOne(dto.getExerciceComptableId()), declarantRepository.getOne(dto.getContactOpId()), dto.getNiveau(),
					dto.getType(), dto.isEnvoye(), LocalDateTime.of(today, LocalTime.now()));
		}).peek(rel -> {
//			if (rel.getNiveau().equals(PublicationRelanceNiveauEnum.NBL)) {
//				rel.getExerciceComptable().setBlacklisted(true);
//				rel.getExerciceComptable().setAfficheBlacklist(true);
//				rel.getExerciceComptable().getEspaceOrganisation().setNewPublication(Boolean.TRUE);
//			}
		}).collect(Collectors.toList());
		if (!relanceEntities.isEmpty()) {
			publicationRelanceRepository.save(relanceEntities);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updatePublicationRelance(List<Long> relanceId, LocalDate today) {
		List<PublicationRelanceEntity> relanceToUpdate = this.publicationRelanceRepository.findAll(relanceId);
		relanceToUpdate.forEach(rel -> {
			rel.setEnvoye(true);
			rel.setRejeuDate(LocalDateTime.of(today, LocalTime.now()));
		});
		this.publicationRelanceRepository.flush();
		LOGGER.info("");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void createExerciceComptableForAllEspace() {
		this.espaceOrganisationRepository.findIdEspacesValides().forEach(espaceId -> {
			try {
				checkExerciceComptableExistence(espaceId);
			} catch (BusinessGlobalException e) {
				LOGGER.info(e.getMessage());
			} catch (Exception e) {
				LOGGER.error("impossible de créer d'exercices pour l'espace " + espaceId + " : {}", e);
			}
		});
		espaceOrganisationRepository.flush();
	}

	/**
	 * Méthode initiant les exercices comptables. Appelée lors de la première publication de l'EC (qui est donc la date courante)
	 */
	@Override
	public List<ExerciceComptableSimpleDto> initExerciceComptable(final Long currentEspaceOrganisationId) {
		final EspaceOrganisationEntity espaceOrganisationEntity = espaceOrganisationRepository.findOne(currentEspaceOrganisationId);
		DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		LocalDate dateCourante = localDateNowBuilder.getLocalDateNow();
		List<ExerciceComptableEntity> exerciceComptableEntities = espaceOrganisationEntity.getExercicesComptable();
		String stringDateCloture = espaceOrganisationEntity.getFinExerciceFiscal();
		LocalDate dateClotureAnneePrecedente;
		LocalDate dateClotureAnneeCourante;
        for (int i = dateCourante.getYear(); i > 2017; i--) {
        	dateClotureAnneePrecedente = LocalDate.parse(stringDateCloture + "-" + (i-1), dtFormatter); 
        	if (dateClotureAnneePrecedente.isBefore(LocalDate.parse("31-12-2017", dtFormatter))) dateClotureAnneePrecedente = LocalDate.parse("31-12-2017", dtFormatter);
    		dateClotureAnneeCourante = LocalDate.parse(stringDateCloture + "-" + i, dtFormatter);
    		
        	exerciceComptableEntities.add(new ExerciceComptableEntity(dateClotureAnneePrecedente.plusDays(1),dateClotureAnneeCourante, espaceOrganisationEntity)) ;
        }
        exerciceComptableEntities.add(new ExerciceComptableEntity(LocalDate.parse("2017-07-01"), LocalDate.parse("2017-12-31"), espaceOrganisationEntity)) ;	
        List<ExerciceComptableSimpleDto>  dtos= exerciceComptableSimpleTransformer.modelToDto(exerciceComptableEntities);
        dtos.sort(Comparator.comparing(ExerciceComptableSimpleDto::getDateDebut).reversed());
        return dtos;
		
	}
	
	/**
	 * S'il existe 1 exercice comptable publié après la date de cessation retourne TRUE
	 * Sinon s'il existe 1 activité d'intérêt publié après la date de cessation retourne TRUE
	 * Sinon retourne FALSE
	 */
    @Override
    public Boolean hasPublishedAfterCessationDate(long espaceId, LocalDate cessationDate) {
        Date dateCessation = Date.from(cessationDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        
        List<StatutPublicationEnum> listStatutPublicationPublie = Arrays.asList(StatutPublicationEnum.PUBLIEE,StatutPublicationEnum.MAJ_PUBLIEE,StatutPublicationEnum.REPUBLIEE); 
        
        //On récupère le 1er exercice comptable débutant après date cessation et qui aurait été publié
        
        ExerciceComptableEntity exerciceComptableEntityPublie = exerciceComptableRepository.findFirstByEspaceOrganisationIdAndDateDebutAfterAndStatutIn(espaceId,cessationDate, listStatutPublicationPublie);
    	
        if(exerciceComptableEntityPublie != null) {
        	return true;
        }else {
        	//On regarde s'il y a des activités publié
        	ActiviteRepresentationInteretEntity activiteRepresentationInteretEntityPublie = activiteRepresentationInteretRepository.findActivitePublieeApresDateCessation(dateCessation, espaceId);
            
            if(activiteRepresentationInteretEntityPublie != null) {
            	return true;
            }
        }        
           
        return false;
    }
    
    
    /**
     *  relance préalable 14 jours avant date cloture + 3 mois
     * @param exerciceComptableEntity
     * @param today
     * @return
     */
    public Boolean needRelance(ExerciceComptableEntity exerciceComptableEntity, LocalDate today) {
    	return (today.isAfter(exerciceComptableEntity.getDateFin().plusMonths(this.monthAfterCloture).minusDays(this.dayBeforeCloture1).minusDays(1))
				&& today.isBefore(exerciceComptableEntity.getDateFin().plusMonths(this.monthAfterCloture).minusDays(this.dayBeforeCloture2).minusDays(1)));    	
    }
    
    
    /**
     * PublicationRelanceNiveauEnum.R2 ou PublicationRelanceNiveauEnum.R1
     * @param exerciceComptableEntity
     * @param publicationRelanceNiveauEnum
     * @return
     */
	public LocalDate getMinDate(ExerciceComptableEntity exerciceComptableEntity,PublicationRelanceNiveauEnum  publicationRelanceNiveauEnum) {
		return exerciceComptableEntity.getPublicationRelance().stream().filter(rel -> rel.getNiveau().equals(publicationRelanceNiveauEnum))
				.min(Comparator.comparing(PublicationRelanceEntity::getCreationDate)).map(PublicationRelanceEntity::getCreationDate).get().toLocalDate();		
	}
	/**
	 * 
	 * @param minDate
	 * @param dayMinus
	 * @param dayPlus
	 * @param today
	 * @return
	 */
	public Boolean checkDateRelanceNblR2(LocalDate minDate, Long dayMinus, Long dayPlus, LocalDate today ) {
		return minDate.minusDays(dayMinus).plusDays(dayPlus).isAfter(today);
	}
	/**
	 * Jour date de fin = dernier jour du mois de date de fin && dernier jour du mois < (date de fin + monthAfterCloture mois)
	 * @param exerciceComptableEntity
	 * @return
	 */
	public int getEndOfMonthAdjustment(ExerciceComptableEntity exerciceComptableEntity) {
		int endOfMonthAdjustment = 0;
		
		if (exerciceComptableEntity.getDateFin().getDayOfMonth() == exerciceComptableEntity.getDateFin().lengthOfMonth()
				&& exerciceComptableEntity.getDateFin().lengthOfMonth() < exerciceComptableEntity.getDateFin().plusMonths(this.monthAfterCloture).lengthOfMonth()) {
			endOfMonthAdjustment = exerciceComptableEntity.getDateFin().plusMonths(this.monthAfterCloture).lengthOfMonth() - exerciceComptableEntity.getDateFin().lengthOfMonth();
		}
		return endOfMonthAdjustment;
	}
	/**
	 * Vérifie si une relance a déjà été envoyée pour le type PublicationRelanceNiveauEnum
	 * @param exerciceComptableEntity
	 * @param publicationRelanceNiveauEnum
	 * @return
	 */
	public Boolean hasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnum(ExerciceComptableEntity exerciceComptableEntity, PublicationRelanceNiveauEnum publicationRelanceNiveauEnum) {
		return exerciceComptableEntity.getPublicationRelance().stream().filter(PublicationRelanceEntity::isEnvoye).anyMatch(rel -> rel.getNiveau().equals(publicationRelanceNiveauEnum));
	}
	
	/**
	 * Récupération e l'historique des moyens publiés
	 * contenu dans le pub_json
	 * @param exerciceComptableId
	 * @return
	 */
	private List<PublicationExerciceHistoriqueDto> getHistoriqueExercice(Long exerciceComptableId) {
		
		List<PublicationExerciceHistoriqueDto> listHistorique = new ArrayList<>();
        List<PublicationExerciceEntity> publicationExerciceEntityList = publicationExerciceRepository.findAllByExerciceComptableIdOrderByIdAsc(exerciceComptableId);
        
        for (PublicationExerciceEntity publicationExerciceEntity : publicationExerciceEntityList) {
        	try {
    			// Conversion du JSON sauvegardé en DTO.
    			ObjectMapper mapper = new ObjectMapper();
    			ExerciceComptableJsonDto ex =  mapper.readerFor(ExerciceComptableJsonDto.class).readValue(publicationExerciceEntity.getPublicationJson());
    			
    			PublicationExerciceHistoriqueDto publicationHistoriqueDto = new PublicationExerciceHistoriqueDto();
    			
    			publicationHistoriqueDto.setCreationDate(publicationExerciceEntity.getCreationDate());
    			publicationHistoriqueDto.setChiffreAffaire(String.valueOf(ex.getChiffreAffaire()));
    			publicationHistoriqueDto.setDateDebut(ex.getDateDebut());
    			publicationHistoriqueDto.setDateFin(ex.getDateFin());
    			publicationHistoriqueDto.setMontantDepense(String.valueOf(ex.getMontantDepense()));
    			publicationHistoriqueDto.setNombreSalaries(ex.getNombreSalaries());
    			
    			listHistorique.add(publicationHistoriqueDto);
    			
        	} catch (IOException e) {
    			throw new TechnicalException(ErrorMessageEnum.ERREUR_JSON.getMessage(), e);
    		}
        }
        
        return listHistorique;
		
	}
    
}
