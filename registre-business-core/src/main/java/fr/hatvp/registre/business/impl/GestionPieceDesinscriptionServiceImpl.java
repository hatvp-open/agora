/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.GestionPieceDesinscriptionService;
import fr.hatvp.registre.business.transformer.proxy.DesinscriptionPieceTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.DesinscriptionPieceDto;
import fr.hatvp.registre.persistence.entity.piece.DesinscriptionPieceEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionPieceRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

@Service
@Transactional
public class GestionPieceDesinscriptionServiceImpl extends AbstractCommonService<DesinscriptionPieceDto, DesinscriptionPieceEntity> implements GestionPieceDesinscriptionService {

    @Autowired
    DesinscriptionPieceRepository desinscriptionPieceRepository;

    @Autowired
    EspaceOrganisationRepository espaceOrganisationRepository;

    @Autowired
    DeclarantRepository declarantRepository;

    @Autowired
    DesinscriptionPieceTransformer desinscriptionPieceTransformer;

    @Override
    public JpaRepository<DesinscriptionPieceEntity, Long> getRepository() {
        return this.desinscriptionPieceRepository;
    }

    @Override
    public CommonTransformer<DesinscriptionPieceDto, DesinscriptionPieceEntity> getTransformer() {
        return this.desinscriptionPieceTransformer;
    }

}
