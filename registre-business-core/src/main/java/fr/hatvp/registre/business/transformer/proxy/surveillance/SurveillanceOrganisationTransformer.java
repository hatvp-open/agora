/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.surveillance;

import fr.hatvp.registre.commons.dto.backoffice.SurveillanceDetailDto;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;

public interface SurveillanceOrganisationTransformer{

	SurveillanceDetailDto modelToDto(SurveillanceEntity entity);
    SurveillanceEntity dtoToModel(SurveillanceDetailDto dto);
}
