
/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.DemandeOrganisationLockBoTransformer;
import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.DemandeOrganisationTransformer;
import fr.hatvp.registre.commons.dto.DemandeOrganisationDto;
import fr.hatvp.registre.commons.dto.backoffice.DemandeOrganisationLockBoDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * Classe de transformation de la classe {@link DemandeOrganisationEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class DemandeOrganisationTransformerImpl
    extends AbstractCommonTansformer<DemandeOrganisationDto, DemandeOrganisationEntity>
    implements DemandeOrganisationTransformer
{
    @Autowired
    private DemandeOrganisationLockBoTransformer demandeOrganisationLockBoTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    public DemandeOrganisationDto modelToDto(final DemandeOrganisationEntity model)
    {
        final DemandeOrganisationDto dto = new DemandeOrganisationDto();
        RegistreBeanUtils.copyProperties(dto, model);

        if (model.getDeclarant() != null) {
            dto.setDeclarantId(model.getDeclarant().getId());
            dto.setDeclarantFullname(
                    model.getDeclarant().getPrenom() + " " + model.getDeclarant().getNom());
        }

        if (model.getEspaceOrganisation() != null) {
            dto.setEspaceOrganisationId(model.getEspaceOrganisation().getId());
        }

        final List<DemandeOrganisationLockBoDto> lastAccess = model
                .getDemandeOrganisationLockBoEntities().stream()
                .map(this.demandeOrganisationLockBoTransformer::modelToDto)
                .sorted(Comparator.comparing(DemandeOrganisationLockBoDto::getLockTimeStart))
                .collect(Collectors.toList());

        if (!lastAccess.isEmpty())
            dto.setDernierAgentFullname(
                    lastAccess.get(lastAccess.size() - 1).getUtilisateurBoName());

        final List<DemandeOrganisationLockBoDto> locks = model
                .getDemandeOrganisationLockBoEntities().stream()
                .map(this.demandeOrganisationLockBoTransformer::modelToDto)
                .filter(lock -> lock.getLockTimeRemain() > 0).collect(Collectors.toList());

        if (!locks.isEmpty())
            dto.setLock(locks.get(0));

        return dto;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DemandeOrganisationEntity dtoToModel(final DemandeOrganisationDto dto)
    {
        final DemandeOrganisationEntity model = new DemandeOrganisationEntity();
        RegistreBeanUtils.copyProperties(model, dto);

        final DeclarantEntity declarantEntity = new DeclarantEntity();
        declarantEntity.setId(dto.getDeclarantId());
        model.setDeclarant(declarantEntity);

        if (dto.getEspaceOrganisationId() != null) {
            final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
            espaceOrganisationEntity.setId(dto.getEspaceOrganisationId());
            model.setEspaceOrganisation(espaceOrganisationEntity);
        }

        return model;
    }

}
