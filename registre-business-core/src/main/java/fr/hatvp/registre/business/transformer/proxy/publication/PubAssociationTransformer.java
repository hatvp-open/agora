/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication;

import fr.hatvp.registre.business.transformer.proxy.common.CommonAssociationTransformer;
import fr.hatvp.registre.persistence.entity.publication.PubAssociationEntity;

/**
 * Interface de transformation de la classe {@link fr.hatvp.registre.persistence.entity.publication.PubAssociationEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PubAssociationTransformer
		extends CommonAssociationTransformer<PubAssociationEntity> {
	
}
