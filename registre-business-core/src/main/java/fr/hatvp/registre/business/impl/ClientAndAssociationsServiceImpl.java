/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.ClientAndAssociationsService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.email.SurveillanceMailer;
import fr.hatvp.registre.business.transformer.proxy.AssociationProTransformer;
import fr.hatvp.registre.business.transformer.proxy.ClientTransformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.OrganisationTransformer;
import fr.hatvp.registre.commons.dto.AssoClientBatchDto;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.client.ClientSimpleDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.persistence.entity.espace.AssociationEntity;
import fr.hatvp.registre.persistence.entity.espace.ClientEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.PeriodeActiviteClientEntity;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import fr.hatvp.registre.persistence.repository.espace.AssoAppartenanceRepository;
import fr.hatvp.registre.persistence.repository.espace.ClientsRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.PeriodeActiviteClientRepository;

/**
 * Service de gestion des clients.
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional(readOnly = true)
@Component
public class ClientAndAssociationsServiceImpl implements ClientAndAssociationsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ClientAndAssociationsServiceImpl.class);
	/**
	 * Repository de l'espace organisation.
	 */
	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;

	/**
	 * Repository des clients.
	 */
	@Autowired
	private ClientsRepository clientsRepository;

	/**
	 * Repository des organisations.
	 */
	@Autowired
	private OrganisationRepository organisationRepository;

	/**
	 * Transformeur de l'entité client .
	 */
	@Autowired
	private ClientTransformer clientTransformer;

	/**
	 * Transformeur de l'entité organisation .
	 */
	@Autowired
	private OrganisationTransformer organisationTransformer;

	/**
	 * Repository des associations pro.
	 */
	@Autowired
	private AssoAppartenanceRepository associationRepository;

	/**
	 * Transformeur des organisations pro.
	 */
	@Autowired
	private AssociationProTransformer associationTransformer;

	@Autowired
	private SurveillanceMailer surveillanceMailer;
	
	@Autowired
	private PeriodeActiviteClientRepository periodeActiviteClientRepository;
	
	@Autowired
	private EspaceOrganisationService espaceOrganisationService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<ClientSimpleDto> getPaginatedClientsByEspaceOrganisation(String type, Integer pageNumber, Integer resultPerPage, Long currentEspaceOrganisationId) {

		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		List<ClientEntity> clients = new ArrayList<>();
		Long nbTotalClient = 0L;
		
		if("actif".equals(type)) {
			 clients = clientsRepository.findClientsActifsByEspaceId(currentEspaceOrganisationId, pageRequest.getPageSize(), pageRequest.getOffset());
			 nbTotalClient = clientsRepository.countClientsActifsByEspaceId(currentEspaceOrganisationId);
		}else if("ancien".equals(type)) {
			clients = clientsRepository.findAnciensClientsByEspaceId(currentEspaceOrganisationId, pageRequest.getPageSize(), pageRequest.getOffset());
			nbTotalClient = clientsRepository.countAnciensClientsByEspaceId(currentEspaceOrganisationId);
		}
		
		List<ClientSimpleDto> clientDtos = new ArrayList<>();
		for(ClientEntity client : clients) {
			List<PeriodeActiviteClientEntity> periodeActiviteClientEntityListeTriee = client.getPeriodeActiviteClientList().stream().sorted(Comparator.comparing(PeriodeActiviteClientEntity::getDateAjout).reversed()).collect(Collectors.toList());
			clientDtos.add(new ClientSimpleDto(client.getId(), client.getOrganisation().getDenomination(), client.getOrganisation().getNomUsageSiren(),
												determineIdentifiantNational(client.getOrganisation()), isAncienClient(client), 
												periodeActiviteClientEntityListeTriee.get(0).getDateDesactivation(), 
												periodeActiviteClientEntityListeTriee.get(0).getDateFinContrat(),
												periodeActiviteClientEntityListeTriee.get(0).getCommentaire()));		
		}

		return new PaginatedDto<>(clientDtos, nbTotalClient);
	}

	private String determineIdentifiantNational(final OrganisationEntity model) {
		String identifiantNational = "";
		switch (model.getOriginNationalId()) {
		case SIREN:
			identifiantNational = model.getSiren();
			break;
		case HATVP:
			identifiantNational = model.getHatvpNumber();
			break;
		case RNA:
			identifiantNational = model.getRna();
			break;
		default:
			break;
		}
		return identifiantNational;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void changeIsAncienClient(Long clientId, Long currentEspaceOrganisationId) {
		ClientEntity clientEntity = Optional.ofNullable(this.clientsRepository.findByIdAndEspaceOrganisationId(clientId, currentEspaceOrganisationId))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.CLIENT_INTROUVABLE.getMessage()));

		clientsRepository.flush();

	}

	@Override
	@Transactional
	public void changeIsAncienClientAllClients(Boolean isAncienClient, Long currentEspaceOrganisationId) {
		List<ClientEntity> clients = Optional.ofNullable(this.clientsRepository.findByEspaceOrganisationId(currentEspaceOrganisationId)).orElse(new ArrayList<>());
		clients.forEach(cl -> {
			//récupération de la période en cours
			PeriodeActiviteClientEntity periodeActiviteEnCours = cl.getPeriodeActiviteClientList().stream().sorted(Comparator.comparing(PeriodeActiviteClientEntity::getDateAjout).reversed()).collect(Collectors.toList()).get(0);
			
			//on désactive tous les clients actif à la date du jour
			if(Boolean.TRUE.equals(isAncienClient)) {
				
				if(periodeActiviteEnCours.getDateDesactivation() == null) {
					periodeActiviteEnCours.setDateDesactivation(LocalDateTime.now());
					periodeActiviteEnCours.setStatut(StatutPublicationEnum.MAJ_NON_PUBLIEE);
					
					periodeActiviteClientRepository.saveAndFlush(periodeActiviteEnCours);	
				}
				
			//on réactive tous les anciens clients en ajoutant une période d'activité commençant à la date du jour
			}else if(periodeActiviteEnCours.getDateDesactivation() != null){
				
				PeriodeActiviteClientEntity periodeActiviteClientEntity = new PeriodeActiviteClientEntity();
				periodeActiviteClientEntity.setClient(cl);
				periodeActiviteClientEntity.setDateAjout(LocalDateTime.now());
				periodeActiviteClientEntity.setStatut(StatutPublicationEnum.MAJ_NON_PUBLIEE);
				
				periodeActiviteClientRepository.saveAndFlush(periodeActiviteClientEntity);			
			}		
		});
		clients.stream().findFirst().get().getEspaceOrganisation().setNonDeclarationTiers(isAncienClient);
		clients.stream().findFirst().get().getEspaceOrganisation().setElementsAPublier(true);

		clientsRepository.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<OrganisationDto> getPaginatedAssociations(Integer pageNumber, Integer resultPerPage, Long currentEspaceOrganisationId) {

		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		Page<AssociationEntity> pagedAssociations = associationRepository.findByEspaceOrganisationId(currentEspaceOrganisationId, pageRequest);

		List<OrganisationDto> organisationDtos = pagedAssociations.getContent().stream()
				.map(associationEntity -> getAllOrganisationDto(associationEntity.getOrganisation(), associationEntity.getId())).collect(Collectors.toList());
		return new PaginatedDto<>(organisationDtos, pagedAssociations.getTotalElements());
	}

	/**
	 * Préparer les données de retour pour les clients et les organisation pro. {@link OrganisationDto}
	 */
	private OrganisationDto getAllOrganisationDto(final OrganisationEntity organisation, final Long id) {
		final OrganisationDto organisationDto = this.organisationTransformer.modelToDto(organisation);
		organisationDto.setId(id);

		return organisationDto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public AssoClientDto addClient(final Long espaceOrganisationId, final OrganisationDto organisationDto) {

		final OrganisationEntity organisation;

		final EspaceOrganisationEntity espaceOrganisation = findEspaceOrganisationIfExist(espaceOrganisationId);

		if (Boolean.TRUE.equals(organisationDto.getOrganisationExist())) {
			findClientIfExist(espaceOrganisationId, organisationDto.getId());
			// Vérifier si cette organisation existe dans la table organisation.
			organisation = findOrganisationIfExist(organisationDto.getId());
			// vérifier que cette organisation ne s'ajoute pas elle même
			checkIfSelfClient(organisation, espaceOrganisation);
		} else {
			// L'enregistrement de l'organisation dans le référentiel des organisation obligatoire pour la suite.
			organisation = saveNewOrganisationToReferentiel(organisationDto);
		}
		// Enregistrer le client
		final ClientEntity clientEntity = new ClientEntity();
		clientEntity.setEspaceOrganisation(espaceOrganisation);
		clientEntity.setOrganisation(organisation);
		ClientEntity savedClient = this.clientsRepository.save(clientEntity);
		
		//enregistrement de la période d'activité
		PeriodeActiviteClientEntity periodeActiviteClientEntity = new PeriodeActiviteClientEntity();
		periodeActiviteClientEntity.setClient(savedClient);
		periodeActiviteClientEntity.setDateAjout(LocalDateTime.now());
		periodeActiviteClientEntity.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		
		PeriodeActiviteClientEntity savedPeriodeActiviteClient = periodeActiviteClientRepository.save(periodeActiviteClientEntity);
		
		List<PeriodeActiviteClientEntity> list = new ArrayList<PeriodeActiviteClientEntity>();
		list.add(savedPeriodeActiviteClient);		
		savedClient.setPeriodeActiviteClientList(list);
		
        SurveillanceEntity surveillance = espaceOrganisation.getOrganisation().getSurveillanceEntity();
		if (surveillance!=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.AJOUT_CLIENT)) {
			this.surveillanceMailer.sendSurveillanceMail(espaceOrganisation.getOrganisation(), null, SurveillanceOrganisationEnum.AJOUT_CLIENT,savedClient.getOrganisation().getDenomination());
		}

		if (surveillance!=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.AJOUTE_COMME_CLIENT)) {
			this.surveillanceMailer.sendSurveillanceMail(espaceOrganisation.getOrganisation(), null, SurveillanceOrganisationEnum.AJOUTE_COMME_CLIENT,espaceOrganisation.getOrganisation().computeIdNational()+" - "+espaceOrganisation.getOrganisation().getDenomination());
		}

		return this.clientTransformer.modelToDto(savedClient);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public AssoClientDto addAssociation(final Long espaceOrganisationId, final OrganisationDto organisationDto) {

		final OrganisationEntity organisation;

		final EspaceOrganisationEntity espaceOrganisation = findEspaceOrganisationIfExist(espaceOrganisationId);

		if (Boolean.TRUE.equals(organisationDto.getOrganisationExist())) {
			// Vérifier si cette organisation existe dans la table organisation.
			organisation = findOrganisationIfExist(organisationDto.getId());
			// Vérifier si l'association est déja cliente
			findAssociationIfExist(espaceOrganisationId, organisationDto.getId());
			// vérifier que cette organisation ne s'ajoute pas elle même
			checkIfSelfAffiliation(organisation, espaceOrganisation);
		} else {
			// L'enregistrement de l'organisation dans le référentiel des organisation obligatoire pour la suite.
			organisation = saveNewOrganisationToReferentiel(organisationDto);
		}
		// Enregistrer l'association
		final AssociationEntity associationEntity = new AssociationEntity();
		associationEntity.setEspaceOrganisation(espaceOrganisation);
		associationEntity.setOrganisation(organisation);
		return this.associationTransformer.modelToDto(this.associationRepository.save(associationEntity));
	}

	private EspaceOrganisationEntity findEspaceOrganisationIfExist(final Long espaceOrganisationId) {
		return Optional.ofNullable(this.espaceOrganisationRepository.findOne(espaceOrganisationId))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));
	}

	private OrganisationEntity  findOrganisationIfExist(final Long organisationId) {
		return Optional.ofNullable(this.organisationRepository.findOne(organisationId))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ORGANISATION_INTROUVABLE.getMessage()));
	}

	private void findClientIfExist(final Long espaceOrganisationId, final Long organisationId) {
		Optional.ofNullable(this.clientsRepository.findByEspaceOrganisationIdAndOrganisationId(espaceOrganisationId, organisationId)).ifPresent(client -> {
			throw new BusinessGlobalException(ErrorMessageEnum.CE_CLIENT_EXISTE_DEJA.getMessage());
		});
	}

	private void checkIfSelfClient(final OrganisationEntity organisation, final EspaceOrganisationEntity espaceOrganisation) {
		if (organisation.getId().longValue() == espaceOrganisation.getOrganisation().getId().longValue()) {
			throw new BusinessGlobalException(ErrorMessageEnum.CLIENT_SOI_MEME.getMessage());
		}
	}

	private void findAssociationIfExist(final Long espaceOrganisationId, final Long organisationId) {
		Optional.ofNullable(this.associationRepository.findByEspaceOrganisationIdAndOrganisationId(espaceOrganisationId, organisationId)).ifPresent(asso -> {
			throw new BusinessGlobalException(ErrorMessageEnum.CE_ORGANISATION_EXISTE_DEJA.getMessage());
		});
	}

	private void checkIfSelfAffiliation(final OrganisationEntity organisation, final EspaceOrganisationEntity espaceOrganisation) {
		if (organisation.getId().longValue() == espaceOrganisation.getOrganisation().getId().longValue()) {
			throw new BusinessGlobalException(ErrorMessageEnum.AFFILIE_SOI_MEME.getMessage());
		}
	}

	private OrganisationEntity saveNewOrganisationToReferentiel(final OrganisationDto organisationDto) {
		final OrganisationEntity organisation;
		organisationDto.setId(null);
		organisationDto.setVersion(null);
		organisation = this.organisationRepository.save(this.organisationTransformer.dtoToModel(organisationDto));
		return organisation;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void deleteClient(final Long clientId, final Long espaceOrganisationId) {
		final ClientEntity clientsEntity = Optional.ofNullable(this.clientsRepository.findByIdAndEspaceOrganisationId(clientId, espaceOrganisationId))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.CLIENT_INTROUVABLE.getMessage()));
		clientsRepository.delete(clientsEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void deleteAssociation(final Long associationId, final Long espaceOrganisationId) {
		final AssociationEntity associationEntity = Optional.ofNullable(this.associationRepository.findByIdAndEspaceOrganisationId(associationId, espaceOrganisationId))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ORGANISATION_INTROUVABLE.getMessage()));

		this.associationRepository.delete(associationEntity);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AssoClientBatchDto verifyAssociationBatch(AssoClientBatchDto batchReport, final Long espaceOrganisationId) {

		final List<Long> orgaIdList = batchReport.getOrganisationList().stream().filter(orgaDto -> orgaDto.getOrganisationExist() != null ? orgaDto.getOrganisationExist() : false)// peek
				.map(OrganisationDto::getId).collect(Collectors.toList());

		final List<OrganisationEntity> organisationEntityList = this.organisationRepository.findByIdIn(orgaIdList);

		final Map<Long, OrganisationEntity> orgaEntityMap = organisationEntityList.stream().collect(Collectors.toMap(OrganisationEntity::getId, Function.identity()));

		OrganisationEntity organisation;

		final EspaceOrganisationEntity espaceOrganisation = findEspaceOrganisationIfExist(espaceOrganisationId);

		AssociationEntity associationEntity;

		AssoClientDto assoClient;

		for (OrganisationDto organisationDto : batchReport.getOrganisationList()) {
			try {
				if (orgaEntityMap.containsKey(organisationDto.getId())) {
					// Vérifier si cette organisation est déjà une association de l'espace.
					findAssociationIfExist(espaceOrganisationId, organisationDto.getId());
					organisation = orgaEntityMap.get(organisationDto.getId());
					checkIfSelfAffiliation(organisation, espaceOrganisation);
				} else {
					organisation = organisationTransformer.dtoToModel(organisationDto);
				}

				associationEntity = new AssociationEntity();
				associationEntity.setEspaceOrganisation(espaceOrganisation);
				associationEntity.setOrganisation(organisation);
				assoClient = this.associationTransformer.modelToDto(associationEntity);

				batchReport.addValidEntitiy(assoClient);

			} catch (BusinessGlobalException | PersistenceException e) {
				batchReport.addErrorMessage(organisationDto.getNationalId(), e.getMessage());
			}
		}
		return batchReport;
	}
	

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void addAssociationBatch(final Long espaceOrganisationId, final List<OrganisationDto> organisationDtos) {

		final List<Long> orgaIdList = organisationDtos.stream().filter(orgaDto -> orgaDto.getOrganisationExist() != null ? orgaDto.getOrganisationExist() : false).map(OrganisationDto::getId)
				.collect(Collectors.toList());

		final List<OrganisationEntity> organisationEntityList = this.organisationRepository.findByIdIn(orgaIdList);

		final Map<Long, OrganisationEntity> orgaEntityMap = organisationEntityList.stream().collect(Collectors.toMap(OrganisationEntity::getId, Function.identity()));

		OrganisationEntity organisation;

		final EspaceOrganisationEntity espaceOrganisation = findEspaceOrganisationIfExist(espaceOrganisationId);

		for (OrganisationDto organisationDto : organisationDtos) {

			if (orgaEntityMap.containsKey(organisationDto.getId())) {
				// Vérifier si cette organisation est déjà une organisation pro de l'espace.
				findAssociationIfExist(espaceOrganisationId, organisationDto.getId());
				organisation = orgaEntityMap.get(organisationDto.getId());
				checkIfSelfAffiliation(organisation, espaceOrganisation);
			} else {
				// L'enregistrement de l'organisation ds le référentiel des organisation obligatoire pour la suite.
				organisation = saveNewOrganisationToReferentiel(organisationDto);
			}
			// Enregistrer l'association
			AssociationEntity association = new AssociationEntity();
			association.setEspaceOrganisation(espaceOrganisation);
			association.setOrganisation(organisation);
			this.associationRepository.save(association);

		}
	}

	/**
	 * 
	 * @param clientEntity
	 * @return
	 */
	public Boolean isAncienClient(ClientEntity clientEntity) {
		clientEntity.getPeriodeActiviteClientList().stream().sorted(Comparator.comparing(PeriodeActiviteClientEntity::getDateAjout).reversed());
		if(clientEntity.getPeriodeActiviteClientList().get(0).getDateDesactivation() != null) return true;
		return false;
	}

	/**
	 * Désactivation d'un client: mise à jour de sa période d'activité en cours
	 */
	@Override
	@Transactional
	public void updatePeriodeActiviteClientRepository(ClientSimpleDto clientSimpleDto) {	
		PeriodeActiviteClientEntity periodeActiviteClientEntity = Optional.ofNullable(this.periodeActiviteClientRepository.findFirstByClientIdOrderByDateAjoutDesc(clientSimpleDto.getId()))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.CLIENT_INTROUVABLE.getMessage()));
		
		periodeActiviteClientEntity.setDateDesactivation(LocalDateTime.now());
		periodeActiviteClientEntity.setDateFinContrat(clientSimpleDto.getDateFinContrat());
		periodeActiviteClientEntity.setCommentaire(clientSimpleDto.getCommentaire());
		periodeActiviteClientEntity.setStatut(StatutPublicationEnum.MAJ_NON_PUBLIEE);
		
		periodeActiviteClientRepository.saveAndFlush(periodeActiviteClientEntity);
		
		espaceOrganisationService.updateFlagElementsAPublier(periodeActiviteClientEntity.getClient().getEspaceOrganisation().getId());
		
		LOGGER.info("Désactivation du client_id {}", clientSimpleDto.getId());		
	}
	
	/**
	 * Activation client: ajout d'une nouvelle ligne de période d'activité
	 */
	@Override
	@Transactional
	public void addPeriodeActiviteClientRepository(ClientSimpleDto clientSimpleDto) {
		PeriodeActiviteClientEntity periodeActiviteClientEntity = new PeriodeActiviteClientEntity();
		
		ClientEntity clientEntity = Optional.ofNullable(this.clientsRepository.findById(clientSimpleDto.getId()))
		.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.CLIENT_INTROUVABLE.getMessage()));
		
		periodeActiviteClientEntity.setClient(clientEntity);
		periodeActiviteClientEntity.setDateAjout(LocalDateTime.now());
		periodeActiviteClientEntity.setStatut(StatutPublicationEnum.MAJ_NON_PUBLIEE);
		
		periodeActiviteClientRepository.saveAndFlush(periodeActiviteClientEntity);
		
		espaceOrganisationService.updateFlagElementsAPublier(clientEntity.getEspaceOrganisation().getId());
		
		LOGGER.info("Activation du client_id {}", clientSimpleDto.getId());
		
	}
}
