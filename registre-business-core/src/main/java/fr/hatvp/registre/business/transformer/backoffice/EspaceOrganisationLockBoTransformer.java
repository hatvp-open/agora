/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.EspaceOrganisationLockBoDto;
import fr.hatvp.registre.persistence.entity.backoffice.EspaceOrganisationLockBoEntity;

/**
 * Interface de transformation de la classe {@link EspaceOrganisationLockBoEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
public interface EspaceOrganisationLockBoTransformer
    extends CommonTransformer<EspaceOrganisationLockBoDto, EspaceOrganisationLockBoEntity>
{

}
