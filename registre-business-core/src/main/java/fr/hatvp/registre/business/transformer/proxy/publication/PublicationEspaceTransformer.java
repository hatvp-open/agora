/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication;

import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.commons.dto.publication.PublicationFrontDto;

/**
 * Proxy du transformeur entre {@link PublicationDto} et {@link EspaceOrganisationDto} <p> Cette interface est utilisé
 * seulement pour transformer l'objet {@link EspaceOrganisationDto} en {@link PublicationDto} afind de pouvoir
 * l'enregistrer.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationEspaceTransformer {

    /**
     * Copier les données de l'espace organisation dans la publication.
     *
     * @param espaceOrganisationDto espace organisation à transformer.
     * @return une instance de {@link PublicationDto} contenant les données de l'espace.
     */
    PublicationFrontDto espaceToPublication(EspaceOrganisationDto espaceOrganisationDto);

}
