/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.nomenclature;

import fr.hatvp.registre.commons.dto.nomenclature.MontantDepenseDto;
import fr.hatvp.registre.persistence.entity.nomenclature.MontantDepenseEntity;

public interface MontantDepenseTransformer extends CommonNomenclatureTransformer<MontantDepenseDto, MontantDepenseEntity> {

}
