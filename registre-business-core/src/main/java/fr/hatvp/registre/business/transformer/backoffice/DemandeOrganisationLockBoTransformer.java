/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.DemandeOrganisationLockBoDto;
import fr.hatvp.registre.persistence.entity.backoffice.DemandeOrganisationLockBoEntity;

/**
 * Interface de transformation de la classe {@link DemandeOrganisationLockBoEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
public interface DemandeOrganisationLockBoTransformer
    extends CommonTransformer<DemandeOrganisationLockBoDto, DemandeOrganisationLockBoEntity>
{

}
