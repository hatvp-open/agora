/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication.activite;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationBucketExerciceTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationExerciceTransformer;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketExerciceDto;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity;

/**
 * Classe du transformeur entre {@link PublicationExerciceEntity} et {@link PublicationBucketExerciceDto}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional(readOnly = true)
public class PublicationExerciceBucketTransformerImpl extends AbstractCommonTansformer<PublicationBucketExerciceDto, PublicationExerciceEntity>
		implements PublicationBucketExerciceTransformer {

	/** Transformeur des publications. */
	@Autowired
	private PublicationExerciceTransformer publicationExerciceTransformer;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationBucketExerciceDto listPublicationToPublicationBucketExercice(final List<PublicationExerciceEntity> publications,boolean withHistorique) {

		final PublicationBucketExerciceDto bucketDto = new PublicationBucketExerciceDto();

		Optional.ofNullable(publications.get(0)).ifPresent(pub -> {
			bucketDto.setPublicationCourante(this.publicationExerciceTransformer.exerciceToPublication(pub));
		});
//		if (withHistorique) bucketDto.setHistorique(this.publicationExerciceTransformer.exercicesToPublication(publications));
		return bucketDto;
	}
}
