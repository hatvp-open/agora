/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.common;

import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

/**
 * Proxy du transformeur entre {@link PublicationEntity} et {@link PublicationDto}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface CommonPublicationTransformer<E extends PublicationDto>
        extends CommonTransformer<E, PublicationEntity> {

}