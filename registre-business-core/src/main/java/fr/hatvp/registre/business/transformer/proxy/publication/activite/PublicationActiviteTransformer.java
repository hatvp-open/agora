/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication.activite;

import fr.hatvp.registre.commons.dto.publication.activite.PublicationActiviteDto;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;

import java.util.List;
import java.util.stream.Stream;

/**
 * Proxy du transformeur entre {@link PublicationActiviteEntity} et {@link PublicationActiviteDto}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationActiviteTransformer
{
    /**
     * Map une entité d'actvité vers un dto de publication d'actvité
     *
     * @param activite entité d'actvité à transformer.
     * @return une instance de {@link PublicationActiviteDto} contenant les données de l'actvité.
     */
    PublicationActiviteDto activiteHistoriqueToPublication(final PublicationActiviteEntity activite);

    /**
     * Map une entité d'actvité vers un dto de publication d'actvité
     *
     * @param activite entité d'actvité à transformer.
     * @return une instance de {@link PublicationActiviteDto} contenant les données de l'actvité.
     */
    PublicationActiviteDto activiteToPublication(final ActiviteRepresentationInteretEntity activite);

    /**
     * Map un dto de publication d'actvité vers une entité de publication d'actvité
     *
     * @param dto publication actvité à transformer.
     * @return une instance de {@link PublicationActiviteEntity} contenant les données de l'actvité.
     */
    PublicationActiviteEntity dtoToModel(final PublicationActiviteDto dto);

    /**
     * Map une entité de publication d'actvité vers un dto de publication d'actvité
     *
     * @param entity entité actvité à transformer.
     * @return une instance de {@link PublicationActiviteDto} contenant les données de l'actvité.
     */
    PublicationActiviteDto modelToDto(final PublicationActiviteEntity entity);

    /**
     * Map une liste de dtos de publication d'actvité vers une liste d'entités de publication d'actvité
     *
     * @param dtos liste de dto actvité à transformer.
     * @return une instance de {@link List<PublicationActiviteEntity>} contenant les données de l'actvité.
     */
    List<PublicationActiviteEntity> dtoToModel(final List<PublicationActiviteDto> dtos);

    /**
     * Map une liste d'entités de publication d'actvité vers une liste de dto de publication d'actvité
     *
     * @param models liste d'entités actvité à transformer.
     * @return une instance de {@link List<PublicationActiviteDto>} contenant les données de l'actvité.
     */
    List<PublicationActiviteDto> modelToDto(final List<PublicationActiviteEntity> models);

    /**
     * Map un stream d'entités de publication d'actvité vers une liste de dto de publication d'actvité
     *
     * @param models liste d'entités actvité à transformer.
     * @return une instance de {@link List<PublicationActiviteDto>} contenant les données de l'actvité.
     */
    List<PublicationActiviteDto> modelToDto(final Stream<PublicationActiviteEntity> models);

    /**
     * Map une liste d'entités de publication d'actvité vers une liste de dto de publication d'actvité
     *
     * @param models liste d'entités actvité à transformer.
     * @return une instance de {@link List<PublicationActiviteDto>} contenant les données de l'actvité.
     */
    List<PublicationActiviteDto> activitesHistoriqueToPublication(final List<PublicationActiviteEntity> models);
}
