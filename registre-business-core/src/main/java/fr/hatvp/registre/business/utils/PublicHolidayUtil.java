/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PublicHolidayUtil {

	private LocalDate jourDeLAn;
	private LocalDate lundiDePaque;
	private LocalDate feteDuTravail;
	private LocalDate victoire1945;
	private LocalDate ascension;
	private LocalDate lundiDePentecote;
	private LocalDate feteNationale;
	private LocalDate assomption;
	private LocalDate toussaint;
	private LocalDate armistice;
	private LocalDate noel;

	public PublicHolidayUtil() {
		this(LocalDateTime.now().getYear());
	}

	public PublicHolidayUtil(int year) {
		this.jourDeLAn = LocalDate.of(year, 1, 1);

		this.feteDuTravail = LocalDate.of(year, 5, 1);
		this.victoire1945 = LocalDate.of(year, 5, 8);

		this.feteNationale = LocalDate.of(year, 7, 14);
		this.assomption = LocalDate.of(year, 8, 15);
		this.toussaint = LocalDate.of(year, 11, 1);
		this.armistice = LocalDate.of(year, 11, 11);
		this.noel = LocalDate.of(year, 12, 25);

		this.lundiDePaque = computeEasterDate(year).plusDays(1);
		this.ascension = this.lundiDePaque.plusDays(38);
		this.lundiDePentecote = this.lundiDePaque.plusDays(49);
	}

	public List<LocalDate> getPublicHolidays() {
		LocalDate[] arr = new LocalDate[] { jourDeLAn, lundiDePaque, feteDuTravail, victoire1945, ascension, lundiDePentecote, feteNationale, assomption, toussaint, armistice, noel };
		return new ArrayList(Arrays.asList(arr));
	}

	/**
	 * Adapted to Java from the algorithm of Ron Mallen
	 *
	 * Link to the original Basic code : https://www.assa.org.au/edm#Computer
	 *
	 * @param year
	 * @return Easter date for that year
	 */
	private static LocalDate computeEasterDate(int year) {
		int firstDig, remain19, temp; // intermediate results
		int tA, tB, tC, tD, tE; // table A to E results

		firstDig = year / 100; // first 2 digits of year
		remain19 = year % 19; // remainder of year / 19

		// calculate PFM date
		temp = (firstDig - 15) / 2 + 202 - 11 * remain19;

		if (Arrays.asList(new Integer[] { 21, 24, 25, 27, 28, 29, 30, 31, 32, 34, 35, 38 }).contains(firstDig)) {
			temp = temp - 1;
		} else if (Arrays.asList(new Integer[] { 33, 36, 37, 39, 40 }).contains(firstDig)) {
			temp = temp - 2;
		}
		temp = temp % 30;

		tA = temp + 21;
		if (temp == 29) {
			tA = tA - 1;
		}
		if (temp == 28 && remain19 > 10) {
			tA = tA - 1;
		}

		// find the next Sunday
		tB = (tA - 19) % 7;

		tC = (40 - firstDig) % 4;
		if (tC == 3) {
			tC = tC + 1;
		}
		if (tC > 1) {
			tC = tC + 1;
		}

		temp = year % 100;
		tD = (temp + temp / 4) % 7;

		tE = ((20 - tB - tC - tD) % 7) + 1;

		int day = tA + tE;
		int month;
		// return the date
		if (day > 31) {
			day = day - 31;
			month = 4;
		} else {
			month = 3;
		}
		return LocalDate.of(year, month, day);
	}

}
