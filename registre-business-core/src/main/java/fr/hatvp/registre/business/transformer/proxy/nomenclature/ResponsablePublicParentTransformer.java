/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.nomenclature;

import fr.hatvp.registre.commons.dto.nomenclature.ResponsablePublicDto;
import fr.hatvp.registre.commons.dto.nomenclature.ResponsablePublicParentDto;
import fr.hatvp.registre.persistence.entity.nomenclature.ResponsablePublicEntity;

public interface ResponsablePublicParentTransformer extends CommonNomenclatureParentTransformer<ResponsablePublicDto, ResponsablePublicParentDto, ResponsablePublicEntity> {

}
