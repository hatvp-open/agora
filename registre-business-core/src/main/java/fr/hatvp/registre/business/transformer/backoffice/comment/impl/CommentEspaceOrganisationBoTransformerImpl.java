/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment.impl;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.comment.CommentEspaceOrganisationBoTransformer;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentEspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

/**
 * Classe de transformation de la classe {@link CommentEspaceOrganisationEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CommentEspaceOrganisationBoTransformerImpl extends AbstractCommentBoTransformerImpl<CommentBoDto, CommentEspaceOrganisationEntity>
		implements CommentEspaceOrganisationBoTransformer {

	/**
	 * Repository des déclarants.
	 */
	@Autowired
	private EspaceOrganisationRepository organisationRepository;

	/**
	 * Constructeur de la classe.
	 */
	public CommentEspaceOrganisationBoTransformerImpl() {
		super.entityClass = CommentEspaceOrganisationEntity.class;
	}

	@Override
	public CommentEspaceOrganisationEntity dtoToModel(CommentBoDto dto) {
		return super.dtoToModel(dto);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	void dtoToModelComplement(final CommentBoDto dto, final CommentEspaceOrganisationEntity commentDeclarantEntity) {
		final EspaceOrganisationEntity declarantEntity = Optional.ofNullable(this.organisationRepository.findOne(dto.getContextId()))
				.orElseThrow(() -> new EntityNotFoundException("Espace organisation id introuvable"));
		commentDeclarantEntity.setEspaceOrganisationEntity(declarantEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentBoDto commentModelToDto(final CommentEspaceOrganisationEntity model) {
		return super.commentModelToDto(model);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	void modelToDtoComplement(final CommentEspaceOrganisationEntity model, final CommentBoDto commentBoDto) {
		commentBoDto.setContextId(model.getEspaceOrganisationEntity().getId());
	}
}
