/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.activite;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.activite.QualificationObjetTransformer;
import fr.hatvp.registre.commons.dto.activite.QualificationObjetDto;
import fr.hatvp.registre.persistence.entity.activite.QualificationObjetEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class QualificationObjetTransformerImpl
    extends AbstractCommonTansformer<QualificationObjetDto, QualificationObjetEntity>
		implements QualificationObjetTransformer
{

	@Override
	public List<QualificationObjetDto> modelToDto(final List<QualificationObjetEntity> entities) {
		final List<QualificationObjetDto> dtos = new ArrayList<>();

		entities.forEach(entity -> {
			dtos.add(modelToDto(entity));
		});

		return dtos;
	}

	@Override
	public QualificationObjetDto modelToDto(final QualificationObjetEntity entity) {
		final QualificationObjetDto dto = new QualificationObjetDto();

		dto.setId(entity.getId());
		dto.setObjet(entity.getObjet());
		dto.setDenomination(entity.getDenomination());
		dto.setNomUsage(entity.getNomUsage());
		dto.setCoefficiantConfiance(entity.getCoefficiantConfiance());
		dto.setDateEnregistrement(entity.getDateEnregistrement());
		dto.setIdentifiantNational(entity.getIdentifiantNational());
		dto.setIdFiche(entity.getIdFiche());
		dto.setNote(entity.isNote());
		dto.setReprise(entity.isReprise());

		return dto;
	}

	@Override
	public List<QualificationObjetEntity> dtoToModel(final List<QualificationObjetDto> dtos) {
		final List<QualificationObjetEntity> entities = new ArrayList<>();

		dtos.forEach(dto -> {
			entities.add(dtoToModel(dto));
		});

		return entities;
	}

	@Override
	public QualificationObjetEntity dtoToModel(final QualificationObjetDto dto) {
		final QualificationObjetEntity entity = new QualificationObjetEntity();

       entity.setId(dto.getId());
       entity.setObjet(dto.getObjet());
       entity.setDenomination(dto.getDenomination());
       entity.setNomUsage(dto.getNomUsage());
       entity.setCoefficiantConfiance(dto.getCoefficiantConfiance());
       entity.setDateEnregistrement(dto.getDateEnregistrement());
       entity.setIdentifiantNational(dto.getIdentifiantNational());
       entity.setIdFiche(dto.getIdFiche());
       entity.setNote(dto.isNote());
       entity.setReprise(dto.isReprise());

		return entity;
	}

}
