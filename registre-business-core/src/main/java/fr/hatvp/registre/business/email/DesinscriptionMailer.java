/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.email;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import fr.hatvp.registre.commons.lists.OrigineSaisieEnum;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

@Component
public class DesinscriptionMailer extends ApplicationMailer {

    /** Objet du mail. */
    @Value("Demande de désinscription")
    private String object;

    @Value("${mail.hatvp.registre.contact}")
    private String mail;
    
    @Value("${mail.hatvp.registre.dri}")
    private String mailDri;

    /** Template du mail de validation de désinscription*/
    private static final String DESINSCRIPTION_VALID_TEMPLATE = "email/desinscription/desinscription_valid";
    /** Template du mail lors saisie désinscription par agent cas pas publi activité/moyen */
    private static final String DESINSCRIPTION_SAISIE_AGENT_TEMPLATE = "email/desinscription/desinscription_saisie_agent";
    
    private static final String DESINSCRIPTION_DRI_TEMPLATE = "email/desinscription/desinscription_dri";
    
    private static final String DESINSCRIPTION_DRI_VALIDATION_TEMPLATE = "email/desinscription/desinscription_validation_dri";

    public DesinscriptionMailer(JavaMailSender sender, TemplateEngine templateEngine) {
        super(sender, templateEngine);
    }

    public void sendDesinscriptionMail(EspaceOrganisationEntity espaceOrgaEntity, DesinscriptionEntity desinscriptionEntity, DeclarantEntity declarantEntity){

        final Context ctx = new Context(Locale.FRANCE);
        final String datePattern = "dd.MM.yyyy";
        final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(datePattern);
        final String content;

        // informations concernant l'espace organisation
        ctx.setVariable("denomination", espaceOrgaEntity.getOrganisation().getDenomination());
        ctx.setVariable("nomUsage", espaceOrgaEntity.getNomUsage());
        if(espaceOrgaEntity.getOrganisation().getSiren() != null){
            ctx.setVariable("idNational", espaceOrgaEntity.getOrganisation().getSiren());
        }else{
            ctx.setVariable("idNational", espaceOrgaEntity.getOrganisation().getRna());
        }

        // informations concernant la désinscription
        ctx.setVariable("dateCessation", dateFormatter.format(desinscriptionEntity.getCessationDate()));
        ctx.setVariable("motif", desinscriptionEntity.getMotif());
        ctx.setVariable("observationDeclarant", desinscriptionEntity.getObservationDeclarant());

        // informations concernant le déclarant de la désinscription
        ctx.setVariable("nomDeclarant", declarantEntity.getNom());
        ctx.setVariable("prenomDeclarant", declarantEntity.getPrenom());
        ctx.setVariable("civilite", declarantEntity.getCivility());

        // autre informations
        ctx.setVariable("imageResourceName", this.imageResourceName);

        ctx.setVariable("dateDuJour", dateFormatter.format(LocalDateTime.now()));
        
        //Si saisie désincription par agent pour espace n'ayant pas publié d'activité et/ou moyen template = Annexe 1
        //sinon template dans spec VII Backoffice 5 Désinscription 5.1 Désinscription à l’initiative du déclarant
        
        if(OrigineSaisieEnum.AGENT.equals(desinscriptionEntity.getOrigineSaisie())) {
        	content = this.templateEngine.process(DESINSCRIPTION_SAISIE_AGENT_TEMPLATE, ctx);
        }else {
        	content = this.templateEngine.process(DESINSCRIPTION_VALID_TEMPLATE, ctx);
        }

       	

        super.prepareAndSendEmailWhithLogo(declarantEntity.getEmail(), this.object, content);
    }
    
    /**
     * Email de notification à la DRI lors demande désinscription par déclarant
     * @param espaceOrgaEntity
     * @param desinscriptionEntity
     * @param declarantEntity
     */
    public void sendDesinscriptionMailDRI(EspaceOrganisationEntity espaceOrgaEntity, DesinscriptionEntity desinscriptionEntity, DeclarantEntity declarantEntity){

        final Context ctx = new Context(Locale.FRANCE);
        final String datePattern = "dd.MM.yyyy";
        final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(datePattern);
        final String content;

        // informations concernant l'espace organisation
        ctx.setVariable("denomination", espaceOrgaEntity.getOrganisation().getDenomination());
        ctx.setVariable("nomUsage", espaceOrgaEntity.getNomUsage());
        if(espaceOrgaEntity.getOrganisation().getSiren() != null){
            ctx.setVariable("idNational", espaceOrgaEntity.getOrganisation().getSiren());
        }else{
            ctx.setVariable("idNational", espaceOrgaEntity.getOrganisation().getRna());
        }

        // informations concernant la désinscription
        ctx.setVariable("dateCessation", dateFormatter.format(desinscriptionEntity.getCessationDate()));
        ctx.setVariable("motif", desinscriptionEntity.getMotif());
        ctx.setVariable("observationDeclarant", desinscriptionEntity.getObservationDeclarant());

        // informations concernant le déclarant de la désinscription
        ctx.setVariable("nomDeclarant", declarantEntity.getNom());
        ctx.setVariable("prenomDeclarant", declarantEntity.getPrenom());
        ctx.setVariable("civilite", declarantEntity.getCivility());

        // autre informations
        ctx.setVariable("imageResourceName", this.imageResourceName);

        ctx.setVariable("dateDuJour", dateFormatter.format(LocalDateTime.now()));        
     
        
        content = this.templateEngine.process(DESINSCRIPTION_DRI_TEMPLATE, ctx);
       	

        super.prepareAndSendEmailWhithLogo(mailDri, this.object, content);
    }
    /**
     * envoi notification à la DRI lors validation d'un demande de désinscription ou quand celle-ci est saisie
     * directement par un agent
     * @param espaceOrgaEntity
     * @param desinscriptionEntity
     */
    public void sendDesinscriptionValidationMailDRI(EspaceOrganisationEntity espaceOrgaEntity, DesinscriptionEntity desinscriptionEntity){

        final Context ctx = new Context(Locale.FRANCE);
        final String datePattern = "dd.MM.yyyy";
        final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(datePattern);
        final String content;

        // informations concernant l'espace organisation
        ctx.setVariable("denomination", espaceOrgaEntity.getOrganisation().getDenomination());

        if(espaceOrgaEntity.getOrganisation().getSiren() != null){
            ctx.setVariable("idNational", espaceOrgaEntity.getOrganisation().getSiren());
        }else{
            ctx.setVariable("idNational", espaceOrgaEntity.getOrganisation().getRna());
        }

        // informations concernant la désinscription
        ctx.setVariable("dateCessation", dateFormatter.format(desinscriptionEntity.getCessationDate()));

        // autre informations
        ctx.setVariable("imageResourceName", this.imageResourceName);         
        
        content = this.templateEngine.process(DESINSCRIPTION_DRI_VALIDATION_TEMPLATE, ctx);       	

        super.prepareAndSendEmailWhithLogo(mailDri, this.object, content);
    }
}
