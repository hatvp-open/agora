/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import java.util.List;

import fr.hatvp.registre.commons.dto.backoffice.EspaceCollaboratifDto;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

public interface EspaceCollaboratifTransformer {

	EspaceCollaboratifDto modelToDto(EspaceOrganisationEntity entity);

	List<EspaceCollaboratifDto> modelToDto(List<EspaceOrganisationEntity> entities);

}
