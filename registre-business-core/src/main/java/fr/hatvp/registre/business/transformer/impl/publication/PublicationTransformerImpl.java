/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication;

import fr.hatvp.registre.business.transformer.impl.common.CommonPublicationTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationTransformer;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import org.springframework.stereotype.Component;

/**
 * Classe du transformeur entre {@link PublicationEntity} et {@link PublicationDto}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class PublicationTransformerImpl
        extends CommonPublicationTransformerImpl<PublicationDto>
        implements PublicationTransformer {

    /** Init super DTO. */
    public PublicationTransformerImpl() {
        super.dtoClass = PublicationDto.class;
    }
}
