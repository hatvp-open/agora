/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.email;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import fr.hatvp.registre.commons.dto.CollaborateurDto;

/**
 * Classe d'envoi de mails pour les Collaborateurs.
 *
 * @version $Revision$ $Date${0xD}
 **/
@Component
public class CollaboratorMailer
    extends ApplicationMailer
{

    /** Objet du mail. */
    @Value("${mail.collaborateur.ajout.subject}")
    private String addCollaboratorSubject;

    /** Template des emails des collaborateurs. */
    private static final String COLLABORATOR_TEMPLATE = "email/collaborateur/collaborator";

    /**
     * Initialisation du {@link JavaMailSender}
     */
    @Autowired
    protected CollaboratorMailer(final JavaMailSender sender, final TemplateEngine templateEngine)
    {
        super(sender, templateEngine);
    }

    /**
     * Notifier un collaborateur de l'action d'ajout faite sur son email.
     *
     * @param dto collaborateur à notifié.
     * @param denominationOrganisation denomination espace organisation.
     */
    public void sendNotificationEmail(final CollaborateurDto dto,
            final String denominationOrganisation)
    {
        final Context ctx = new Context(Locale.FRANCE);
        ctx.setVariable("civilite", dto.getCivilite().getLibelleLong());
        ctx.setVariable("denomination", denominationOrganisation);
        ctx.setVariable("imageResourceName", this.imageResourceName);

        final String content = this.templateEngine.process(COLLABORATOR_TEMPLATE, ctx);

        super.prepareAndSendEmailWhithLogo(dto.getEmail(), this.addCollaboratorSubject, content);

    }

}
