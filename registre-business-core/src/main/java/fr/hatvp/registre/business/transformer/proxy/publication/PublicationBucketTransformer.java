/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.publication.PublicationBucketDto;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

import java.util.List;

/**
 * Interface de transformation de la classe {@link PublicationBucketDto} & {@link PublicationEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationBucketTransformer
        extends CommonTransformer<PublicationBucketDto, PublicationEntity> {


    /**
     * Préparer le le bucket du livrable à partir d'une liste de publications;
     *
     * @param publications liste de publication.
     * @return le livrable pret à utiliser.
     */
    PublicationBucketDto listPublicationToPublicationBucket(List<PublicationEntity> publications);
}
