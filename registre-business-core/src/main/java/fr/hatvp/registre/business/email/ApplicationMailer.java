/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.email;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.thymeleaf.TemplateEngine;

import fr.hatvp.registre.commons.lists.ErrorMessageEnum;

/**
 * Classe mère d'envoi de mails.
 *
 * @version $Revision$ $Date${0xD}
 */
public abstract class ApplicationMailer {

	/** Encodage des emails. */
	private static final String CHARSET_NAME = "UTF-8";

	/** lien vers le logos à introduire dans le mail; */
	private static final String PATH_TO_LOGO = "email/logo.png";

	/** logos content type. */
	private static final String IMAGE_PNG = "image/png";

	/** Logger de la classe. */
	protected static Logger LOGGER = LoggerFactory.getLogger(ApplicationMailer.class);

	/** Java mail sender. */
	private final JavaMailSender sender;

	/** Moteur des templates. */
	protected final TemplateEngine templateEngine;

	/** URL du front. */
	@Value("${app.front.root}")
	protected String frontRootPath;

	/** Email sender. */
	@Value("${mail.smtp.automate.from}")
	protected String from;

	protected final String imageResourceName = "logo.png";

	/**
	 * Initialisation du {@link JavaMailSender}
	 */
	protected ApplicationMailer(final JavaMailSender sender, final TemplateEngine templateEngine) {
		this.sender = sender;
		this.templateEngine = templateEngine;
	}

	/**
	 * Méthode commune d'envoi de mail.
	 *
	 * @param to
	 *            à qui envoyer le mail.
	 * @param subject
	 *            sujet du mail.
	 * @param content
	 *            contenu du mail.
	 */
	protected void prepareAndSendEmailWhithLogo(final String to, final String subject, final String content) {
		final MimeMessage mail = this.sender.createMimeMessage();

		try {
			// Prepare message using a Spring helper
			final MimeMessageHelper helper = new MimeMessageHelper(mail, true, CHARSET_NAME);
			helper.setTo(to);
			helper.setFrom(this.from);
			helper.setSubject(subject);
			helper.setText(content, true);

			// Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
			final ClassLoader classLoader = this.getClass().getClassLoader();
			final File logo = new File(classLoader.getResource(PATH_TO_LOGO).getFile());
			final byte[] imageBytes = Files.readAllBytes(logo.toPath());
			final InputStreamSource imageSource = new ByteArrayResource(imageBytes);
			helper.addInline(this.imageResourceName, imageSource, IMAGE_PNG);

			// Send mail
			this.sender.send(mail);
		} catch (final MessagingException e) {
			LOGGER.error(ErrorMessageEnum.ERREUR_D_ENVOI_DE_MAIL.getMessage(), e);
		} catch (final IOException e) {
			LOGGER.error(ErrorMessageEnum.LOGO_INTROUVABLE.getMessage(), e);
		}
	}

	/**
	 * Méthode d'envoi de mail de relance.
	 *
	 * @param to
	 *            à qui envoyer le mail.
	 * @param subject
	 *            sujet du mail.
	 * @param content
	 *            contenu du mail.
	 * @throws MessagingException
	 */
	protected void prepareAndSendEmailWhithLogoBatch(final String to, final String subject, final String content, final byte[] logo) throws MessagingException {
		final MimeMessage mail = this.sender.createMimeMessage();

		// Prepare message using a Spring helper
		final MimeMessageHelper helper = new MimeMessageHelper(mail, true, CHARSET_NAME);
		helper.setTo(to);
		helper.setFrom(this.from);
		helper.setSubject(subject);
		helper.setText(content, true);

		// Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
		final byte[] imageBytes = logo;
		final InputStreamSource imageSource = new ByteArrayResource(imageBytes);
		helper.addInline(this.imageResourceName, imageSource, IMAGE_PNG);

		// Send mail
		this.sender.send(mail);
	}

	/**
	 * Envoi un simple email brut sans logo attaché.
	 *
	 * @param to
	 * @param subject
	 * @param content
	 */
	protected void prepareAndSendEmail(final String to, final String subject, final String content) {
		final MimeMessage mail = this.sender.createMimeMessage();

		try {
			// Prepare message using a Spring helper
			final MimeMessageHelper helper = new MimeMessageHelper(mail, true, CHARSET_NAME);
			helper.setTo(to);
			helper.setFrom(this.from);
			helper.setSubject(subject);
			helper.setText(content, true);

			// Send mail
			this.sender.send(mail);
		} catch (final MessagingException e) {
			LOGGER.error(ErrorMessageEnum.ERREUR_D_ENVOI_DE_MAIL.getMessage(), e);
		}
	}
	
	/**
	 * Méthode commune d'envoi de mail contenant le logo plus une pièce jointe
	 *
	 * @param to
	 *            à qui envoyer le mail.
	 * @param subject
	 *            sujet du mail.
	 * @param content
	 *            contenu du mail.
	 */
	protected void prepareAndSendEmailWhithLogoAndPieceJointe(final String recipient, final String subject, final String content, final String nomPieceJointe, InputStreamSource inputStreamSource) {
		final MimeMessage mail = this.sender.createMimeMessage();

		try {
			// Prepare message using a Spring helper
			final MimeMessageHelper helper = new MimeMessageHelper(mail, true, CHARSET_NAME);
			helper.setTo(recipient);
			helper.setFrom(this.from);
			helper.setSubject(subject);
			helper.setText(content, true);

			// Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
			final ClassLoader classLoader = this.getClass().getClassLoader();
			final File logo = new File(classLoader.getResource(PATH_TO_LOGO).getFile());
			final byte[] imageBytes = Files.readAllBytes(logo.toPath());
			final InputStreamSource imageSource = new ByteArrayResource(imageBytes);
			helper.addInline(this.imageResourceName, imageSource, IMAGE_PNG);
			helper.addAttachment(nomPieceJointe, inputStreamSource);

			// Send mail
			this.sender.send(mail);
		} catch (final MessagingException e) {
			LOGGER.error(ErrorMessageEnum.ERREUR_D_ENVOI_DE_MAIL.getMessage(), e);
		} catch (final IOException e) {
			LOGGER.error(ErrorMessageEnum.LOGO_INTROUVABLE.getMessage(), e);
		}
	}
}
