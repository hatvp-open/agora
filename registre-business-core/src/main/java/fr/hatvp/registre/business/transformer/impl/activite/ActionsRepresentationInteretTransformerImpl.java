/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.activite;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ActionsRepresentationInteretTransformer;
import fr.hatvp.registre.commons.dto.activite.ActionRepresentationInteretDto;
import fr.hatvp.registre.commons.dto.activite.TiersDto;
import fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ActionMeneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DecisionConcerneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ResponsablePublicEntity;

@Component
public class ActionsRepresentationInteretTransformerImpl extends AbstractCommonTansformer<ActionRepresentationInteretDto, ActionRepresentationInteretEntity>
		implements ActionsRepresentationInteretTransformer {

	@Override
	public List<ActionRepresentationInteretDto> modelToDto(final List<ActionRepresentationInteretEntity> entities) {
		final List<ActionRepresentationInteretDto> dtos = new ArrayList<>();

		entities.forEach(entity -> {
			dtos.add(modelToDto(entity));
		});

		return dtos;
	}

	@Override
	public ActionRepresentationInteretDto modelToDto(final ActionRepresentationInteretEntity entity) {
		final ActionRepresentationInteretDto dto = new ActionRepresentationInteretDto();

		dto.setId(entity.getId());
		dto.setReponsablesPublics(entity.getReponsablesPublics().stream().map(rs -> {
			return rs.getId();
		}).collect(Collectors.toSet()));
		dto.setResponsablePublicAutre(entity.getResponsablePublicAutre());
		dto.setDecisionsConcernees(entity.getDecisionsConcernees().stream().map(dc -> {
			return dc.getId();
		}).collect(Collectors.toSet()));
		dto.setActionsMenees(entity.getActionsMenees().stream().map(am -> {
			return am.getId();
		}).collect(Collectors.toSet()));
		dto.setActionMeneeAutre(entity.getActionMeneeAutre());
		dto.setTiers(entity.getTiers().stream().map(tier -> {
			return new TiersDto(tier.getId(), tier.getDenomination());
		}).collect(Collectors.toList()));
		dto.setObservation(entity.getObservation());

		return dto;
	}

	@Override
	public List<ActionRepresentationInteretEntity> dtoToModel(final List<ActionRepresentationInteretDto> dtos) {
		final List<ActionRepresentationInteretEntity> entities = new ArrayList<>();

		dtos.forEach(dto -> {
			entities.add(dtoToModel(dto));
		});

		return entities;
	}

	@Override
	public ActionRepresentationInteretEntity dtoToModel(final ActionRepresentationInteretDto dto) {
		final ActionRepresentationInteretEntity entity = new ActionRepresentationInteretEntity();

		entity.setId(dto.getId());
		entity.setReponsablesPublics(dto.getReponsablesPublics().stream().map(rs -> {
			return new ResponsablePublicEntity(rs);
		}).collect(Collectors.toSet()));
		entity.setResponsablePublicAutre(dto.getResponsablePublicAutre());
		entity.setDecisionsConcernees(dto.getDecisionsConcernees().stream().map(dc -> {
			return new DecisionConcerneeEntity(dc);
		}).collect(Collectors.toSet()));
		entity.setActionsMenees(dto.getActionsMenees().stream().map(am -> {
			return new ActionMeneeEntity(am);
		}).collect(Collectors.toSet()));
		entity.setActionMeneeAutre(dto.getActionMeneeAutre());
		entity.setTiers(dto.getTiers().stream().map(tier -> {
			return new OrganisationEntity(tier.getOrganisationId());
		}).collect(Collectors.toList()));
		entity.setObservation(dto.getObservation());

		return entity;
	}

}
