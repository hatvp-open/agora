/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.persistence.entity.piece.EspaceOrganisationPieceEntity;

/**
 * Interface pour le transformer des pieces de l'espace collaboratif.
 * 
 * @version $Revision$ $Date${0xD}
 */
public interface EspaceOrganisationPieceTransformer
    extends CommonTransformer<EspaceOrganisationPieceDto, EspaceOrganisationPieceEntity>
{
    
}
