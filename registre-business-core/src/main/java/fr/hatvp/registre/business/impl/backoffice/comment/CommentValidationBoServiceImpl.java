/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice.comment;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.backoffice.comment.CommentValidationBoService;
import fr.hatvp.registre.business.transformer.backoffice.comment.CommentValidationInscriptionBoTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentValidationEspaceEntity;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentValidationEspaceRepository;

/**
 * Classe d'implémentation de gestion des commentaires des validations de création d'espace..
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CommentValidationBoServiceImpl extends CommonCommentServiceImpl<CommentBoDto, CommentValidationEspaceEntity> implements CommentValidationBoService {

	/**
	 * Repository de la classe.
	 */
	private final CommentValidationEspaceRepository commentValidationEspaceRepository;
	/**
	 * Transformeur de la classe.
	 */
	private final CommentValidationInscriptionBoTransformer commentValidationInscriptionBoTransformer;

	/** Init dependances. */
	@Autowired
	public CommentValidationBoServiceImpl(final CommentValidationEspaceRepository commentValidationEspaceRepository,
			final CommentValidationInscriptionBoTransformer commentValidationInscriptionBoTransformer) {
		this.commentValidationEspaceRepository = commentValidationEspaceRepository;
		this.commentValidationInscriptionBoTransformer = commentValidationInscriptionBoTransformer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CommentBoDto> getAllComment(final Long validationId) {
		return this.commentValidationEspaceRepository.findByInscriptionEspaceEntityId(validationId).stream().map(this.commentValidationInscriptionBoTransformer::commentModelToDto)
				.sorted(Comparator.comparing(CommentBoDto::getDateModification)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public CommentBoDto addComment(final CommentBoDto commentBoDto) {
		final CommentValidationEspaceEntity entity = this.getTransformer().dtoToModel(commentBoDto);
		entity.setDateCreation(new Date());
		return this.commentValidationInscriptionBoTransformer.commentModelToDto(this.commentValidationEspaceRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public CommentBoDto updateComment(final CommentBoDto commentBoDto) {
		final CommentValidationEspaceEntity entityT = this.getTransformer().dtoToModel(commentBoDto);
		final CommentValidationEspaceEntity entityR = this.getRepository().findOne(commentBoDto.getId());
		entityT.setEditeur(entityR.getEditeur());
		entityT.setDateCreation(entityR.getDateCreation());
		entityT.setDateModification(new Date());
		return this.commentValidationInscriptionBoTransformer.commentModelToDto(this.commentValidationEspaceRepository.save(entityT));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<CommentValidationEspaceEntity, Long> getRepository() {
		return this.commentValidationEspaceRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<CommentBoDto, CommentValidationEspaceEntity> getTransformer() {
		return this.commentValidationInscriptionBoTransformer;
	}
}
