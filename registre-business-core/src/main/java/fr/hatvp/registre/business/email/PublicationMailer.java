/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.email;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity;

/**
 * Classe de mail pour les publications.
 *
 * @version $Revision$ $Date${0xD}
 **/
@Component
public class PublicationMailer extends ApplicationMailer {

	/** Objet du mail. */
	@Value("${mail.publication.objet}")
	private String object;

	/** Template des publications. */
	private static final String PUBLICATION_EMAIL = "email/publication/publication";

	/** Initialisation du {@link JavaMailSender} */
	@Autowired
	protected PublicationMailer(final JavaMailSender sender, final TemplateEngine templateEngine) {
		super(sender, templateEngine);
	}

	/**
	 * Envoyer un email à tout les publicateur de l'espace pour les notifier de la publication.
	 *
	 * @param publication
	 *            la publication
	 * @param publicateursEspace
	 *            liste des publicateurs de l'espace organisation.
	 */
	public void envoyerNotificationDePublication(final PublicationDto publication, final List<DeclarantDto> publicateursEspace, final boolean alreadyPublished) {
		final Context ctx = new Context(Locale.FRANCE);

		ctx.setVariable("denomination", publication.getDenomination());
		ctx.setVariable("nomComplet",
				publication.getPublisher().getCivility().getLibelleCourt() + " " + publication.getPublisher().getPrenom() + " " + publication.getPublisher().getNom());
		ctx.setVariable("date", new SimpleDateFormat("dd/MM/yyyy à HH:mm").format(publication.getCreationDate()));
		ctx.setVariable("imageResourceName", this.imageResourceName);
		ctx.setVariable("publication", publication);
		ctx.setVariable("typePublication", "ESPACE");
		if (alreadyPublished) {
			ctx.setVariable("typePublication", "ESPACE_REPUBLICATION");
		} else {
			ctx.setVariable("typePublication", "ESPACE_PUBLICATION");
		}
		ctx.setVariable("isIdentite", true);

		publicateursEspace.forEach(d -> {
			ctx.setVariable("civilite", d.getCivility().getLibelleLong());
			final String mailContent = this.templateEngine.process(PUBLICATION_EMAIL, ctx);
			super.prepareAndSendEmailWhithLogo(d.getEmail(), StringUtils.replace(this.object, "DENOMINATION", publication.getDenomination()), mailContent);
		});
	}

	/**
	 * Envoyer un email à tout les publicateur de l'espace pour les notifier de la publication d'un exercice.
	 *
	 * @param entity
	 *            la publication
	 * @param publicateursEspace
	 *            liste des publicateurs de l'espace organisation.
	 */
	public void envoyerNotificationDePublicationExercice(final PublicationExerciceEntity entity, final List<DeclarantDto> publicateursEspace, final boolean alreadyPublished) {
		final Context ctx = new Context(Locale.FRANCE);

		final String denomination = entity.getExerciceComptable().getEspaceOrganisation().getOrganisation().getDenomination();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String textDateDebutExercice = entity.getExerciceComptable().getDateDebut().format(formatter);
		String textDateFinExercice = entity.getExerciceComptable().getDateFin().format(formatter);

		ctx.setVariable("denomination", denomination);
		ctx.setVariable("nomComplet", entity.getPublicateur().getCivility().getLibelleCourt() + " " + entity.getPublicateur().getPrenom() + " " + entity.getPublicateur().getNom());
		ctx.setVariable("date", new SimpleDateFormat("dd/MM/yyyy à HH:mm").format(entity.getCreationDate()));
		ctx.setVariable("dateDebutExercice", textDateDebutExercice);
		ctx.setVariable("dateFinExercice", textDateFinExercice);
		ctx.setVariable("imageResourceName", this.imageResourceName);
		ctx.setVariable("publication", entity);
		if (alreadyPublished) {
			ctx.setVariable("typePublication", "EXERCICE_REPUBLICATION");
		} else {
			ctx.setVariable("typePublication", "EXERCICE_PUBLICATION");
		}
		ctx.setVariable("isIdentite", false);

		publicateursEspace.forEach(d -> {
			ctx.setVariable("civilite", d.getCivility().getLibelleLong());
			final String mailContent = this.templateEngine.process(PUBLICATION_EMAIL, ctx);
			super.prepareAndSendEmailWhithLogo(d.getEmail(), StringUtils.replace(this.object, "DENOMINATION", denomination), mailContent);
		});
	}

	/**
	 * Envoyer un email à tout les publicateur de l'espace pour les notifier de la publication d'un exercice.
	 *
	 * @param activite
	 *            la publication
	 * @param publicateursEspace
	 *            liste des publicateurs de l'espace organisation.
	 */
	public void envoyerNotificationDePublicationActivite(final PublicationActiviteEntity savedPublication, final ActiviteRepresentationInteretEntity activite,
			final List<DeclarantDto> publicateursEspace, final boolean alreadyPublished) {
		final Context ctx = new Context(Locale.FRANCE);

		final String denomination = activite.getExerciceComptable().getEspaceOrganisation().getOrganisation().getDenomination();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String textDateDebutExercice = activite.getExerciceComptable().getDateDebut().format(formatter);
		String textDateFinExercice = activite.getExerciceComptable().getDateFin().format(formatter);

		ctx.setVariable("denomination", denomination);
		ctx.setVariable("nomComplet", savedPublication.getPublicateur().getCivility().getLibelleCourt() + " " + savedPublication.getPublicateur().getPrenom() + " "
				+ savedPublication.getPublicateur().getNom());
		ctx.setVariable("date", new SimpleDateFormat("dd/MM/yyyy à HH:mm").format(savedPublication.getCreationDate()));
		ctx.setVariable("dateDebutExercice", textDateDebutExercice);
		ctx.setVariable("dateFinExercice", textDateFinExercice);
		ctx.setVariable("imageResourceName", this.imageResourceName);
		ctx.setVariable("publication", activite);
		if (alreadyPublished) {
			ctx.setVariable("typePublication", "ACTIVITE_REPUBLICATION");
		} else {
			ctx.setVariable("typePublication", "ACTIVITE_PUBLICATION");
		}
		ctx.setVariable("isIdentite", false);

		publicateursEspace.forEach(d -> {
			ctx.setVariable("civilite", d.getCivility().getLibelleLong());
			final String mailContent = this.templateEngine.process(PUBLICATION_EMAIL, ctx);
			super.prepareAndSendEmailWhithLogo(d.getEmail(), StringUtils.replace(this.object, "DENOMINATION", denomination), mailContent);
		});
	}

}
