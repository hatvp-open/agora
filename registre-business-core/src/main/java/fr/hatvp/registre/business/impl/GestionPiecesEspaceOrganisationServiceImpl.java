/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.GestionPiecesEspaceOrganisationService;
import fr.hatvp.registre.business.email.SurveillanceMailer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationPieceTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.business.utils.PdfGeneratorUtil;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import fr.hatvp.registre.persistence.entity.piece.EspaceOrganisationPieceEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationPiecesRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

/**
 * Implémentation du service de gestion des pièces attachées à l'espace collaboratif.
 */
@Service
@Transactional
public class GestionPiecesEspaceOrganisationServiceImpl extends AbstractCommonService<EspaceOrganisationPieceDto, EspaceOrganisationPieceEntity>
		implements GestionPiecesEspaceOrganisationService {
	/**
	 * Le transformer pour la gestion des pièces versées.
	 */
	@Autowired
	private EspaceOrganisationPieceTransformer espaceOrganisationPieceTransformer;

	/**
	 * Repository pour l'accès à la table des pièces complémentaires en BDD.
	 */
	@Autowired
	private EspaceOrganisationPiecesRepository espaceOrganisationPiecesRepository;

	/**
	 * Repository pour l'accès à l'espace organisation.
	 */
	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;

	/**
	 * Repository pour l'accès aux déclarants.
	 */
	@Autowired
	private DeclarantRepository declarantRepository;

	/** composant de mailing de surveillance */
	@Autowired
	private SurveillanceMailer surveillanceMailer;

    @Autowired
    private PdfGeneratorUtil pdfGenerator;

    /** Template pdf de l'attestation de désinscription*/
    private static final String ATTESTATION_PDF_TEMPLATE = "pdf/desinscription/attestationpdf";
    
    private static final String PIECE_JOINTE_MAIL = "pdf/desinscription/pieceJointeMailDesinscriptionNoActivite";

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void verserPiecesComplementaires(Long idEspaceOrganisation, Long idDeclarant, List<EspaceOrganisationPieceDto> pieces) {
		// On détermine l'espace pour lequel enregistrer les pièces.
		EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository.findOne(idEspaceOrganisation);

		// On détermine le déclarant qui enregistre les pièces.
		DeclarantEntity declarantEntity = this.declarantRepository.findOne(idDeclarant);

		// On vérifie que l'état de l'espace permette le chargement de pièces.
		if (!StatutEspaceEnum.COMPLEMENT_DEMANDE_ENVOYEE.equals(espaceOrganisationEntity.getStatut())) {
			throw new BusinessGlobalException(ErrorMessageEnum.AJOUT_COMPLEMENT_INTERDIT.getMessage());
		}

		for (EspaceOrganisationPieceDto piece : pieces) {
			// Transformation du DTO en entity
			EspaceOrganisationPieceEntity entity = this.getTransformer().dtoToModel(piece);

			entity.setDateVersementPiece(new Date());
			entity.setDeclarant(declarantEntity);
			entity.setEspaceOrganisation(espaceOrganisationEntity);
			entity.setOrigineVersement(OrigineVersementEnum.DEMANDE_COMPLEMENT_INSCRIPTION);

			// Persitence de la pièce versée.
			this.getRepository().save(entity);
		}

		// Mise à jour du statut de l'espace pour le BO.
		espaceOrganisationEntity.setStatut(StatutEspaceEnum.COMPLEMENT_RECU);
		this.espaceOrganisationRepository.save(espaceOrganisationEntity);
        SurveillanceEntity surveillance = espaceOrganisationEntity.getOrganisation().getSurveillanceEntity();
		if (surveillance!=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.COMPLEMENT_DEMANDE)) {
			this.surveillanceMailer.sendSurveillanceMail(espaceOrganisationEntity.getOrganisation(), espaceOrganisationEntity, SurveillanceOrganisationEnum.COMPLEMENT_DEMANDE,"");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<EspaceOrganisationPieceDto> getListePiecesComplementairesEspaceOrganisation(final Long espaceOrganisationId) {
		return this.getListePiecesEspaceOrganisation(espaceOrganisationId, OrigineVersementEnum.DEMANDE_COMPLEMENT_INSCRIPTION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<EspaceOrganisationPieceDto> getListePiecesCreationEspaceOrganisation(final Long espaceOrganisationId) {
		return this.getListePiecesEspaceOrganisation(espaceOrganisationId, OrigineVersementEnum.DEMANDE_CREATION_ESPACE);
	}

	/**
	 * Retourne la liste des pièces d'un espace filtrées selon leur origine.
	 *
	 * @param espaceOrganisationId
	 * @param origine
	 * @return la liste des pièces filtrée.
	 */
	protected List<EspaceOrganisationPieceDto> getListePiecesEspaceOrganisation(final Long espaceOrganisationId, final OrigineVersementEnum origine) {
		// On transforme la liste d'entity en dto en vidant le contenu du fichier qui n'est pas utile dans ce contexte.
		return this.espaceOrganisationPiecesRepository.findByEspaceOrganisationIdAndOrigineVersementOrderByDateVersementPieceDesc(espaceOrganisationId, origine).map(e -> {
			EspaceOrganisationPieceDto dto = this.getTransformer().modelToDto(e);
			// Inutile de renvoyer le contenu du fichier dans cette liste. Économisons la mémoire !
			dto.setContenuFichier(null);
			return dto;
		}).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<EspaceOrganisationPieceEntity, Long> getRepository() {
		return this.espaceOrganisationPiecesRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<EspaceOrganisationPieceDto, EspaceOrganisationPieceEntity> getTransformer() {
		return this.espaceOrganisationPieceTransformer;
	}

    @Override
    @Transactional(readOnly = true)
    public Pair<byte[], String> buildPdf(Long currentEspaceOrganisationId) {
        String fileName = "Attestation.pdf";
        Map<String, Object> pdfData = new HashMap<>();

        byte[] pdf = pdfGenerator.createPdf(ATTESTATION_PDF_TEMPLATE, pdfData);

        return Pair.of(pdf, fileName);
    }
    /**
     * Génère le PDF ajouté en PJ de l'email de désinscription d'une organisation n'ayant pas
     * publié d'activité ni de moyen
     */
    @Override
    @Transactional(readOnly = true)
    public byte[] buildPdfPieceJointeDesinscriptionNoActivite(EspaceOrganisationDto espaceOrganisationDto) {
    	final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Map<String, Object> pdfData = new HashMap<>();
		pdfData.put("denomination", espaceOrganisationDto.getDenomination());
		pdfData.put("dateCreation", formatter.format(espaceOrganisationDto.getCreationDate()));

        return pdfGenerator.createPdf(PIECE_JOINTE_MAIL, pdfData);
    }
}
