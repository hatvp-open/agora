/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.nomenclature;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.proxy.nomenclature.MontantDepenseTransformer;
import fr.hatvp.registre.commons.dto.nomenclature.MontantDepenseDto;
import fr.hatvp.registre.persistence.entity.nomenclature.MontantDepenseEntity;

@Component
public class MontantDepenseTransformerImpl extends AbstractCommonNomenclatureTransformer<MontantDepenseDto, MontantDepenseEntity> implements MontantDepenseTransformer {

	/** Init super DTO. */
	public MontantDepenseTransformerImpl() {
		super.dtoClass = MontantDepenseDto.class;
	}
}
