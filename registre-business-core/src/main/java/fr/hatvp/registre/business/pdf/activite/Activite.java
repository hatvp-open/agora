/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.pdf.activite;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;

public class Activite {

	private String idFiche;

	private String objet;

	private Map<String, List<String>> domaines;

	private String statut;

	private List<Action> actions;

	public Activite(ActiviteRepresentationInteretEntity activite) {
		this.idFiche = activite.getIdFiche();
		this.objet = activite.getObjet();
		this.domaines = activite.getDomainesIntervention().stream()
				.collect(Collectors.groupingBy(DomaineInterventionEntity::getCategorie, Collectors.mapping(DomaineInterventionEntity::getLibelle, Collectors.toList())));
		this.statut = activite.getStatut().getLabel();
		this.actions = activite.getActionsRepresentationInteret().stream().map(action -> new Action(action)).collect(Collectors.toList());
	}

	public String getIdFiche() {
		return idFiche;
	}

	public String getObjet() {
		return objet;
	}

	public Map<String, List<String>> getDomaines() {
		return domaines;
	}

	public String getStatut() {
		return statut;
	}

	public List<Action> getActions() {
		return actions;
	}

}
