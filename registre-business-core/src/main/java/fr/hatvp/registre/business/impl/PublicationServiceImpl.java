/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.io.IOException;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.ExerciceComptableService;
import fr.hatvp.registre.business.PublicationExerciceService;
import fr.hatvp.registre.business.PublicationService;
import fr.hatvp.registre.business.email.PublicationMailer;
import fr.hatvp.registre.business.email.SurveillanceMailer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationBucketTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationEspaceTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationFrontTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationMetaTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationRechercheTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationTransformer;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.publication.PublicationBucketDto;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.commons.dto.publication.PublicationFrontDto;
import fr.hatvp.registre.commons.dto.publication.PublicationMetaDto;
import fr.hatvp.registre.commons.dto.publication.PublicationRechercheBlacklistDto;
import fr.hatvp.registre.commons.dto.publication.PublicationRechercheDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActiviteRechercheDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketExerciceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.exceptions.TechnicalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.espace.ClientEntity;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.PeriodeActiviteClientEntity;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationTempEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;
import fr.hatvp.registre.persistence.repository.activite.ExerciceComptableRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationActiviteRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationExerciceRepository;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.PeriodeActiviteClientRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationTempRepository;

/**
 * Classe d'implémentation du service des publications.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class PublicationServiceImpl extends AbstractCommonService<PublicationFrontDto, PublicationEntity> implements PublicationService {	
	/**
	 * Service de l'espace collaboratif. Utile pour récupérer les données de l'espace à publier.
	 */
	@Autowired
	private EspaceOrganisationService espaceOrganisationService;
	
	@Autowired
	private ExerciceComptableService exerciceComptableService;
	/**
	 * Service de l'espace collaboratif. Utile pour récupérer les données de l'espace à publier.
	 */
	@Autowired
	private PublicationExerciceService publicationExerciceService;
	/**
	 * Repository des publications.
	 */
	@Autowired
	private PublicationRepository publicationRepository;

	@Autowired
    private DesinscriptionRepository desinscriptionRepository;
    /**
     * Repo pub activite
     */
	@Autowired
	private PublicationActiviteRepository publicationActiviteRepository;

    /**
     * Repo pub moyens
     */
    @Autowired
    private PublicationExerciceRepository publicationExerciceRepository;
	/**
	 * Repository pour stockage temporaire des publications.
	 */
	@Autowired
	private PublicationTempRepository publicationTempRepository;

	/**
	 * Repository des exercices.
	 */
	@Autowired
	private ExerciceComptableRepository exerciceRepository;

	/**
	 * Transformer des publications.
	 */
	@Autowired
	private PublicationFrontTransformer publicationTransformer;

	/**
	 * Transformer des meta informations de publications.
	 */
	@Autowired
	private PublicationMetaTransformer publicationMetaTransformer;

	/**
	 * Transformer les informations pour le document de recherche des publications.
	 */
	@Autowired
	private PublicationRechercheTransformer publicationRechercheTransformer;

	/**
	 * Transformer des espaces vers publications.
	 */
	@Autowired
	private PublicationEspaceTransformer publicationEspaceTransformer;

	/**
	 * Repository des declarants.
	 */
	@Autowired
	private DeclarantRepository declarantRepository;

	/**
	 * Transformeur des declarants.
	 */
	@Autowired
	private DeclarantTransformer declarantTransformer;

	/**
	 * Repository des espaces organisations.
	 */
	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;

	/**
	 * Repository des organisations.
	 */
	@Autowired
	private OrganisationRepository organisationRepository;

	/**
	 * Mailer des publications.
	 */
	@Autowired
	private PublicationMailer publicationMailer;

	/**
	 * Repository des inscriptions de l'espace organisation.
	 */
	@Autowired
	private InscriptionEspaceRepository inscriptionEspaceRepository;

	/**
	 * Transformeur de préparation de livrable;
	 */
	@Autowired
	private PublicationBucketTransformer publicationBucketTransformer;

	/**
	 * Transformeur de base pour les publications.
	 */
	@Autowired
	private PublicationTransformer publicationBaseTransformer;

	@Autowired
	private SurveillanceMailer surveillanceMailer;
	
	@Autowired
	private PeriodeActiviteClientRepository periodeActiviteClientRepository;

    /**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void savePublication(final Long connectedUser, final Long currentEspaceOrganisationId, final String httpSessionId) {

		// Récupération du DTO à publier enregistré pour la session de l'utilisateur
		EspaceOrganisationDto espaceOrganisationDto = getEspaceDtoForCurrentSession(httpSessionId);

		// Vérification que la publication stockée correspond à l'espace pour lequel on est connecté
		if (espaceOrganisationDto.getId().longValue() != currentEspaceOrganisationId.longValue()) {
			throw new BusinessGlobalException(ErrorMessageEnum.CONTENU_PUBLICATION_INTROUVABLE.getMessage(), 410);
		}

		// Conversion des données de l'espace récupéré en données de publication
		final PublicationFrontDto publicationDto = this.publicationEspaceTransformer.espaceToPublication(espaceOrganisationDto);

		// Enregistrement de l'identitée du publicateur
		final DeclarantDto publicateur = this.declarantTransformer.modelToDto(this.declarantRepository.findOne(connectedUser));
		publicationDto.setPublisher(publicateur);

		// Transformation en entity
		PublicationEntity publicationEntity = this.publicationTransformer.dtoToModel(publicationDto);

		// Obtenir la dernière publication de cette fiche
		final PublicationEntity alreadyPublished = this.publicationRepository.findFirstByEspaceOrganisationIdOrderByIdDesc(currentEspaceOrganisationId);

		// si 1ere publication on construit les exercices comptables rétroactivement jusqu'en 2017
		if (!Optional.ofNullable(alreadyPublished).isPresent()) {
			this.exerciceComptableService.initExerciceComptable(currentEspaceOrganisationId);
		}
		// Enregistrer la publication
		final PublicationEntity savedPublication = this.publicationRepository.save(publicationEntity);

		// Mise à jour du flag des publications sur l'espace organisation et du flag ElementsAPublier
		final EspaceOrganisationEntity espaceOrganisationEntity = updateFlagDePublicationTrue(currentEspaceOrganisationId, publicationEntity.getCreationDate());

		// Mise à jour du référentiel des organisations.
		updateReferencielOraganisation(savedPublication, espaceOrganisationEntity);
		
		miseAjourStatutPeriodeActivite(espaceOrganisationEntity);

		// Envoie de mail à tout les publicateurs de l'espace.
		envoyerUnEmailAuxPublicateursDeLespaceOrganisation(publicationDto, currentEspaceOrganisationId, Optional.ofNullable(alreadyPublished).isPresent());

		EspaceOrganisationEntity espace = this.espaceOrganisationRepository.findOne(currentEspaceOrganisationId);
        SurveillanceEntity surveillance = espaceOrganisationEntity.getOrganisation().getSurveillanceEntity();
		if (surveillance!=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.PUBLICATION_IDENTITE)) {
			this.surveillanceMailer.sendSurveillanceMail(espace.getOrganisation(), espace, SurveillanceOrganisationEnum.PUBLICATION_IDENTITE,"");
		}
	}

	/**
	 * Cette méthode récupère, dans la table publication temp, le document json à publier correspondant à la session de l'utilisateur courant.
	 *
	 * @return le DTO pour la publication à persister.
	 */
	protected EspaceOrganisationDto getEspaceDtoForCurrentSession(String httpSessionId) {

		// Récupération du contenu à publier selon la session.
		PublicationTempEntity entity = this.publicationTempRepository.findByHttpSessionId(httpSessionId);

		// Pas de publication à sauvegarder pour la session en cours ?
		if (entity == null) {
			// Impossible de poursuivre le traitement : on lance un exception avec message d'erreur
			throw new BusinessGlobalException(ErrorMessageEnum.CONTENU_PUBLICATION_INTROUVABLE.getMessage(), 410);
		}

		try {
			// Conversion du JSON sauvegardé en DTO.
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readerFor(EspaceOrganisationDto.class).readValue(entity.getJsonContent());
		} catch (IOException e) {
			throw new TechnicalException(ErrorMessageEnum.ERREUR_PUBLICATION.getMessage(), e);
		}
	}

	/**
	 * Mise à jour du référentiel des organisation après la publication.
	 *
	 * @param savedPublication
	 *            publication enregistré.
	 * @param espaceOrganisationEntity
	 *            entité de l'espace organisation qui vient d'être publié.
	 */
	public OrganisationEntity updateReferencielOraganisation(final PublicationEntity savedPublication, final EspaceOrganisationEntity espaceOrganisationEntity) {
		final OrganisationEntity organisationEntity = espaceOrganisationEntity.getOrganisation();
		organisationEntity.setAdresse(savedPublication.getAdresse());
		organisationEntity.setCodePostal(savedPublication.getCodePostal());
		organisationEntity.setVille(savedPublication.getVille());
		organisationEntity.setPays(savedPublication.getPays());
		return this.organisationRepository.save(organisationEntity);
	}

	/**
	 * Mise à jour du flag de publication de l'espace organisation pour signaler une nouvellle publication.
	 *
	 * @param espaceId
	 *            id de l'espace à mettre à jour.
	 * @return l'entité de l'espace mis à jour.
	 */
	protected EspaceOrganisationEntity updateFlagDePublicationTrue(final Long espaceId, Date datePublication) {
		final EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository.findOne(espaceId);
		espaceOrganisationEntity.setNewPublication(Boolean.TRUE);
		espaceOrganisationEntity.setElementsAPublier(Boolean.FALSE);
		
		if(espaceOrganisationEntity.getDatePremierePublication() == null) {
			espaceOrganisationEntity.setDatePremierePublication(datePublication);
		}
		this.espaceOrganisationRepository.save(espaceOrganisationEntity);
		return espaceOrganisationEntity;
	}

	/**
	 * Notifier tous les publicateurs et adminis de l'espace organisation du processus de publication.
	 *
	 * @param publication
	 *            DTO de la publication.
	 * @param espaceOrganisationId
	 *            Id de l'espace organisation
	 */
	private void envoyerUnEmailAuxPublicateursDeLespaceOrganisation(final PublicationDto publication, final Long espaceOrganisationId, final boolean alreadyPublished) {
		// Lister les destinataires (publicateurs et administrateurs actifs de l'espace).
		List<DeclarantDto> listeDesPublicateursEtAdmins = this.inscriptionEspaceRepository.findAllByActifTrueAndDateSuppressionIsNullAndEspaceOrganisationId(espaceOrganisationId).stream()
				.filter(i -> (i.getRolesFront().contains(RoleEnumFrontOffice.PUBLICATEUR) || i.getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR)))
				.map(i -> this.declarantTransformer.modelToDto(i.getDeclarant())).collect(Collectors.toList());

		this.publicationMailer.envoyerNotificationDePublication(publication, listeDesPublicateursEtAdmins, alreadyPublished);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void depublier(final Long id, final Long userId) {
		Optional.ofNullable(this.publicationRepository.findOne(id)).ifPresent(e -> {
			if (e.getStatut().equals(StatutPublicationEnum.PUBLIEE) || e.getStatut().equals(StatutPublicationEnum.REPUBLIEE)) {
				final UtilisateurBoEntity depublicateur = new UtilisateurBoEntity();
				depublicateur.setId(userId);
				e.setDepublicateur(depublicateur);
				e.setDepublicationDate(new Date());
				e.setStatut(StatutPublicationEnum.DEPUBLIEE);
				// On positionne le flag newPublication de l'espace pour activer la republication par le batch.
				EspaceOrganisationEntity espaceOrganisationEntity = e.getEspaceOrganisation();
				espaceOrganisationEntity.setNewPublication(Boolean.TRUE);
				this.publicationRepository.save(e);
				this.espaceOrganisationRepository.save(espaceOrganisationEntity);

			} else {
				throw new BusinessGlobalException(ErrorMessageEnum.DONNEES_INCORRECTES.getMessage());
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void republier(final Long id, final Long userId) {
		Optional.ofNullable(this.publicationRepository.findOne(id)).ifPresent(e -> {
			if (e.getStatut().equals(StatutPublicationEnum.DEPUBLIEE)) {
				final UtilisateurBoEntity republicateur = new UtilisateurBoEntity();
				republicateur.setId(userId);
				e.setRepublicateur(republicateur);
				e.setRepublicationDate(new Date());
				e.setStatut(StatutPublicationEnum.REPUBLIEE);
				e.getEspaceOrganisation().setNewPublication(Boolean.TRUE);
				this.publicationRepository.save(e);

			} else {
				throw new BusinessGlobalException(ErrorMessageEnum.DONNEES_INCORRECTES.getMessage());
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PublicationMetaDto> getPublicationsEspace(final Long espaceId) {
		List<PublicationEntity> pubEntity = this.publicationRepository.findByEspaceOrganisationId(espaceId);
	    return pubEntity.stream().map(this.publicationMetaTransformer::modelToDto).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationFrontDto getDernierePublication(final Long espaceId) {
		final Optional<PublicationEntity> optional = this.publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(espaceId, StatutPublicationEnum.DEPUBLIEE);
		return optional.map(p -> this.publicationTransformer.modelToDto(p)).orElseGet(PublicationFrontDto::new);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public PublicationFrontDto getNouvellePublication(final Long espaceId, final String httpSessionId) {

		// Récupération des données de l'espace courant
		EspaceOrganisationDto espaceOrganisationDto = this.espaceOrganisationService.getAllEspaceOrganisationInformations(espaceId);

		// On stocke temporairement le contenu de la publication pour la persister plus tard au moment du save.
		this.enregistrerSessionPublication(espaceOrganisationDto, httpSessionId);
		// Conversion des données de l'espace courant en données de publication
		return this.publicationEspaceTransformer.espaceToPublication(espaceOrganisationDto);
	}

	/**
	 * Sauvegarde temporairement en base de données (format JSON) le contenu de la publication lié à la session du publicateur.
	 *
	 * @param publicationEspace
	 *            la publication à enregistrer
	 * @param httpSessionId
	 *            la session du publicateur
	 */
	@Transactional
	protected void enregistrerSessionPublication(final EspaceOrganisationDto publicationEspace, final String httpSessionId) {
		// Mapper pour la sérialisation en JSON avant stockage
		ObjectMapper mapper = new ObjectMapper();

		// Conversion du DTO en JSON
		String jsonContent = null;
		try {
			jsonContent = mapper.writeValueAsString(publicationEspace);
		} catch (JsonProcessingException e) {
			throw new TechnicalException(ErrorMessageEnum.ERREUR_PUBLICATION.getMessage(), e);
		}

		// Recherche d'une éventuelle ancienne publication pour la session
		PublicationTempEntity publicationTempEntity = this.publicationTempRepository.findByHttpSessionId(httpSessionId);
		if (publicationTempEntity == null) {
			publicationTempEntity = new PublicationTempEntity();
		}

		// Constitution de l'entity à persister
		publicationTempEntity.setHttpSessionId(httpSessionId);
		publicationTempEntity.setJsonContent(jsonContent);

		// Sauvegarde
		this.publicationTempRepository.save(publicationTempEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void viderSessionPublication(final String httpSessionId) {
		this.publicationTempRepository.deleteByHttpSessionId(httpSessionId);
	}

	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public PublicationDto getDernierContenuPourPublication(final Long espaceId) {
		final Optional<PublicationEntity> optional = this.publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(espaceId, StatutPublicationEnum.DEPUBLIEE);
		// Il est possible qu'il n'y ai pas de publication
		if (optional.isPresent()) {
			// on recupere la premiere publication juste pour la date
			PublicationEntity premierePublicationEspace = this.publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdAsc(espaceId, StatutPublicationEnum.DEPUBLIEE);
			PublicationDto dto = this.publicationBaseTransformer.modelToDto(optional.get());
			// on ajoute les exercices
			dto.setExercices(getPublicationExercice(optional.get().getEspaceOrganisation(), false));
			// on recupére la date de la dernière activité publiée si il y en a
			if (this.publicationActiviteRepository.getLatestPublicationActivite(optional.get().getEspaceOrganisation().getId()) != null) {
				dto.setDateDernierePublicationActivite(this.publicationActiviteRepository.getLatestPublicationActivite(optional.get().getEspaceOrganisation().getId()).getCreationDate());
				dto.setIsActivitesPubliees(true);
			} else {
				dto.setIsActivitesPubliees(false);
			}
			dto.setPremierePublicationDate(premierePublicationEspace.getCreationDate());
			return dto;
		}

		return new PublicationDto();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public PublicationBucketDto getPublicationLivrable(final Long espaceId) {

		// on recupere les publications avec statut publié
		final List<PublicationEntity> publicationsEspace = this.publicationRepository.findAllByEspaceOrganisationIdAndStatutNotOrderByIdDesc(espaceId, StatutPublicationEnum.DEPUBLIEE);

		final EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository.findOne(espaceId);

		// Il est possible que cette liste soit vide si l'espace n'a fait aucune publication
		if (publicationsEspace != null && !publicationsEspace.isEmpty()) {
			PublicationBucketDto bucketDto = this.publicationBucketTransformer.listPublicationToPublicationBucket(publicationsEspace);
			// on ajoute les exercices
			bucketDto.setExercices(getPublicationExercice(espaceOrganisationEntity, true));
			return bucketDto;
		} else {
			return null;
		}
	}

	/**
	 * Permet d'obtenir les exercices publiés pour les json du site
	 *
	 * @param
	 * @return La liste des publications d'exercices pour un espace
	 */
	public List<PublicationBucketExerciceDto> getPublicationExercice(EspaceOrganisationEntity espace,boolean withHistorique) {
		return espace.getExercicesComptable().stream().filter(e -> e.getPublicationEnabled())
				.filter(e->(!e.getActivitesRepresentationInteret().stream().filter(a->!a.getStatut().equals(StatutPublicationEnum.NON_PUBLIEE) && !a.getStatut().equals(StatutPublicationEnum.SUPPRIMEE)).collect(Collectors.toList()).isEmpty())
				|| e.isNoActivite() || (!e.getStatut().equals(StatutPublicationEnum.NON_PUBLIEE)) || (e.getDateFin().isAfter(espace.getDateActionCreation().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())))
				.sorted(Comparator.comparing(ExerciceComptableEntity::getDateFin).reversed())
				.map(e -> this.publicationExerciceService.getPublicationLivrableExercice(e.getId(),withHistorique)).filter(Objects::nonNull).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PublicationRechercheDto> getPublicationsRechercheLivrable() {
		// try with resource pour fermer le stream après usage.
		try (final Stream<Object[]> publicationsRecherche = this.publicationRepository.getAllLatestPublications()) {
			List<PublicationRechercheDto> result = publicationsRecherche.map(object -> {
				// Premier élément du tableau : PublicationEntity
				PublicationEntity entity = (PublicationEntity) object[0];
				// Second élément du tableau : Date, la date de première publication
				Date datePremierePublication = (Date) object[1];
				// On doit déterminer pour chaque espace sa date de première publication.
				PublicationRechercheDto dto = this.publicationRechercheTransformer.modelToDto(entity);
				dto.setCreationDate(datePremierePublication);
				// on recupére la date de la dernière activité publiée si il y en a
				if (this.publicationActiviteRepository.getLatestPublicationActivite(entity.getEspaceOrganisation().getId()) != null) {
					dto.setDateDernierePublicationActivite(this.publicationActiviteRepository.getLatestPublicationActivite(entity.getEspaceOrganisation().getId()).getCreationDate());
					dto.setIsActivitesPubliees(true);
				} else {
					dto.setIsActivitesPubliees(false);
				}

				return dto;
			}).sorted((o1, o2) -> o2.getCreationDate().compareTo(o1.getCreationDate())).collect(Collectors.toList());
			return result;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PublicationRechercheDto> getEspacesAvecActivitesPubliees() {
		// try with resource pour fermer le stream après usage.
		try (final Stream<Object[]> publicationsRecherche = this.publicationActiviteRepository.getAllLatestPublicationsActivitesByEspace()) {

			List<PublicationRechercheDto> result = publicationsRecherche.map(object -> {
				// Premier élément du tableau : PublicationEntity
				PublicationActiviteEntity entity = (PublicationActiviteEntity) object[0];
				// Second élément du tableau : Date, la date de première publication
				Date datePremierePublication = (Date) object[1];
				// On doit déterminer pour chaque espace sa date de première publication.
				PublicationRechercheDto dto = this.publicationRechercheTransformer.modelActiviteToDtoPublicationRecherche(entity);
				dto.setCreationDate(datePremierePublication);
				return dto;
			}).sorted((o1, o2) -> o2.getCreationDate().compareTo(o1.getCreationDate())).collect(Collectors.toList());
			return result;
		}
	}

	/**
	 * {@inheritDoc}
	 */

//	public List<PublicationRechercheDto> getEspacesBlacklistes() {
//		// on recupere les exercices blacklistés
//		DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
//		List<EspaceOrganisationEntity> espacesBlacklistes = espaceOrganisationRepository.findByExerciceBlacklistes(LocalDate.parse("31-12-2017", dtFormatter));
//		List<PublicationRechercheDto> result = espacesBlacklistes.stream().map(e -> this.publicationRechercheTransformer.modelExerciceToDtoPublicationRechercheBlacklist(e))
//				.collect(Collectors.toList());
//		Collections.shuffle(result);
//		return result;
//
//	}
	
	@Override
	public List<PublicationRechercheBlacklistDto> getEspacesBlacklistes() {
		// on recupere les exercices blacklistés
		List<ExerciceComptableEntity> exercicesBlacklistes = exerciceRepository.findAllByBlacklistedAndAfficheBlacklist(true,true);
		List<PublicationRechercheBlacklistDto> result = exercicesBlacklistes.stream().filter(e->e.getDateDebut().getYear()!=2017).map(e -> this.publicationRechercheTransformer.modelExerciceToDtoPublicationRechercheBlacklist(e))
				.collect(Collectors.toList());
		Collections.shuffle(result);
		return result;
	}
	

	@Override
	public List<PublicationActiviteRechercheDto> getPublicationsActivitesRechercheLivrable() {
		try (final Stream<Object[]> publicationsRecherche = this.publicationActiviteRepository.getAllLatestPublicationsActivites().limit(100)) {
			List<PublicationActiviteRechercheDto> result = publicationsRecherche.map(object -> {
				// Premier élément du tableau : PublicationEntity
				PublicationActiviteEntity entity = (PublicationActiviteEntity) object[0];
				// Second élément du tableau : Date, la date de première publication
				Date datePremierePublication = (Date) object[1];
				// On doit déterminer pour chaque espace sa date de première publication.
				PublicationActiviteRechercheDto dto = this.publicationRechercheTransformer.modelActiviteToDtoPublicationActiviteRecherche(entity);
				dto.setPublicationDate(datePremierePublication);
				return dto;
			}).sorted((o1, o2) -> o2.getPublicationDate().compareTo(o1.getPublicationDate())).collect(Collectors.toList());

			return result;
		}
	}

    @Override
    public Integer getNbPublicationsActivites() {
	    return this.publicationActiviteRepository.getCountPublicationsActivites();
    }

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<PublicationEntity, Long> getRepository() {
		return this.publicationRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<PublicationFrontDto, PublicationEntity> getTransformer() {
		return this.publicationTransformer;
	}

	/**
	 * Retourne une liste des 4 informations affichées sur la page d'accueil : 1. La liste des espaces publiés avec la date de la première publication 3. La liste des espaces publiés
	 * qui ont publiés des activités, avec la date de la dernière activité publiée 2. La liste des activités publiées avec la date de la dernière publication 4. la liste des blackliste
	 */
	@Override
	public List<Object> getPublicationsGlobalesRechercheLivrable() {
		List<Object> result = new ArrayList<>();
        LOGGER.info("debut identites");
		List<PublicationRechercheDto> identites = this.getPublicationsRechercheLivrable();
        LOGGER.info("fin identites");
        LOGGER.info("debut espacesAvecActivites");
		List<PublicationRechercheDto> espacesAvecActivites = this.getEspacesAvecActivitesPubliees();
        LOGGER.info("fin espacesAvecActivites");
        LOGGER.info("debut activites");
		List<PublicationActiviteRechercheDto> activites = this.getPublicationsActivitesRechercheLivrable();
		Integer nbActivites = this.getNbPublicationsActivites();
        LOGGER.info("fin activites");
        LOGGER.info("debut blackliste");
		List<PublicationRechercheBlacklistDto> espacesBlacklistes = this.getEspacesBlacklistes();
        LOGGER.info("fin blackliste");
        LOGGER.info("debut desinscription");
        Long desinscritCount = this.countByStatut();
        LOGGER.info("fin desinscription");
		result.add(identites);
		result.add(activites);
		result.add(espacesBlacklistes);
		result.add(new Integer[] {Math.toIntExact(identites.size() - desinscritCount), espacesAvecActivites.size(), nbActivites, espacesBlacklistes.size() });
		return result;
	}

	@Override
    public Long countByStatut(){
        return this.espaceOrganisationRepository.countByStatut(StatutEspaceEnum.DESINSCRIT);
    }
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> countEspaceInscritWithDifferenceThroughTime() {

		Map<String, String> map = new HashMap<>();
		long total;

		try (final Stream<Object[]> publicationsRecherche = this.publicationRepository.getAllLatestPublications()) {
			total = publicationsRecherche.count();
			map.put("today", String.valueOf(total));
		}

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);

		try (final Stream<Object[]> publicationsRecherche = this.publicationRepository.getAllLatestPublicationsBefore(calendar.getTime())) {
			long intermediate = publicationsRecherche.count();
			map.put("yesterday", String.valueOf(total - intermediate));
		}

		calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -7);

		try (final Stream<Object[]> publicationsRecherche = this.publicationRepository.getAllLatestPublicationsBefore(calendar.getTime())) {
			long intermediate = publicationsRecherche.count();
			map.put("oneWeekAgo", String.valueOf(total - intermediate));
		}

		calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);

		try (final Stream<Object[]> publicationsRecherche = this.publicationRepository.getAllLatestPublicationsBefore(calendar.getTime())) {
			long intermediate = publicationsRecherche.count();
			map.put("oneMonthAgo", String.valueOf(total - intermediate));
		}

		return map;
	}

	/** Retourne true si l'espace à publier son identité */
    @Override
    public Boolean hasPublishedIdentite(long inscriptionEspaceId) {
        InscriptionEspaceEntity inscriptionEspaceEntity = this.inscriptionEspaceRepository.findFirstByIdOrderByIdDesc(inscriptionEspaceId);
        // verification que l'EC a bien déja fait une publication de son espace
        Optional <PublicationEntity> publicationEntity = this.publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(inscriptionEspaceEntity.getEspaceOrganisation().getId(), StatutPublicationEnum.DEPUBLIEE);
       return publicationEntity.isPresent();
    }

    /** Retourne 0 si aucune publi, 1 si publi identité, 2 si publi activite, 3 si désinscription effectué */
    @Override
    public Integer hasPublished(long inscriptionEspaceId) {
        InscriptionEspaceEntity inscriptionEspaceEntity = this.inscriptionEspaceRepository.findFirstByIdOrderByIdDesc(inscriptionEspaceId);
        DesinscriptionEntity desinscriptionEntity = this.desinscriptionRepository.findByEspaceOrganisation(inscriptionEspaceEntity.getEspaceOrganisation());
        //vérification que l'EC soit désinscrit
        if(desinscriptionEntity != null) return 3;
        // verification que l'EC a bien déja fait une publication d'activite
        Optional <PublicationActiviteEntity> publicationActiviteEntity = this.publicationActiviteRepository.findFirstByActivite_ExerciceComptable_EspaceOrganisationIdAndStatutNotOrderByIdDesc(inscriptionEspaceEntity.getEspaceOrganisation().getId(), StatutPublicationEnum.DEPUBLIEE);
        if (!publicationActiviteEntity.isPresent()) {
            return this.hasPublishedIdentite(inscriptionEspaceId)?1:0;
        }
        return 2;
    }

    @Override
    public Integer hasPublishedWithEspaceId(long espaceId) {
        InscriptionEspaceEntity inscriptionEspaceEntity = inscriptionEspaceRepository.findFirstByEspaceOrganisationIdOrderByIdDesc(espaceId);
        return this.hasPublished(inscriptionEspaceEntity.getId());
    }

    /**
     * Retourne une map contenant respectivement le nbre de publications d'activités et de moyens pour la période courante.
     * si Pair<> = (0,0) alors déclaration nulle
     */
    @Override
    public Map<String, String> getCountPublications(long inscriptionEspaceId) {
        HashMap<String, String> publicationsCountMap = new HashMap<>();
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        
        InscriptionEspaceEntity espace = this.inscriptionEspaceRepository.findFirstByIdOrderByIdDesc(inscriptionEspaceId);
        ExerciceComptableEntity exerciceComptableEntity = this.exerciceRepository.findFirstByEspaceOrganisationIdOrderByDateDebutDesc(espace.getEspaceOrganisation().getId());
        Integer countPublicationsActivites;
        Integer countPublicationsMoyens;
        if(!exerciceComptableEntity.isNoActivite()){
            countPublicationsActivites = this.publicationActiviteRepository.countByActivite_ExerciceComptableIdAndStatutNotOrderByIdDesc(exerciceComptableEntity.getId(), StatutPublicationEnum.DEPUBLIEE);
            countPublicationsMoyens = this.publicationExerciceRepository.countByExerciceComptableIdAndStatutNotOrderByIdDesc(exerciceComptableEntity.getId(), StatutPublicationEnum.DEPUBLIEE);
        }else{
            countPublicationsActivites = -1;
            countPublicationsMoyens = -1;
        }
        publicationsCountMap.put("nbrePubActivites", countPublicationsActivites.toString().trim());
        publicationsCountMap.put("nbrePubMoyens", countPublicationsMoyens.toString().trim());
        publicationsCountMap.put("periode", "du "+exerciceComptableEntity.getDateDebut().format(formatters)+ " au "+exerciceComptableEntity.getDateFin().format(formatters));
        return publicationsCountMap;
    }
    @Transactional
    public void miseAjourStatutPeriodeActivite(final EspaceOrganisationEntity espaceOrganisationEntity) {
		for(ClientEntity client : espaceOrganisationEntity.getClients()) {
			List<PeriodeActiviteClientEntity> periodeList = this.periodeActiviteClientRepository.findByClientIdAndStatut(client.getId(), StatutPublicationEnum.NON_PUBLIEE);
			if(!periodeList.isEmpty()) {
				
				for(PeriodeActiviteClientEntity periode : periodeList) {
					periode.setStatut(StatutPublicationEnum.PUBLIEE);
					 
					this.periodeActiviteClientRepository.saveAndFlush(periode);	
				}
				
			}else {
				periodeList = this.periodeActiviteClientRepository.findByClientIdAndStatut(client.getId(), StatutPublicationEnum.MAJ_NON_PUBLIEE);
				
				if(!periodeList.isEmpty()) {
					for(PeriodeActiviteClientEntity periode : periodeList) {
						periode.setStatut(StatutPublicationEnum.MAJ_PUBLIEE);
						this.periodeActiviteClientRepository.saveAndFlush(periode);	
					}
					
				}
				
			}
			
		}
		
	}
}
