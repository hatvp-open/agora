/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.utils;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.stereotype.Component;



@Component
public class SessionsUtil {
	
	
	@Resource
	SessionRegistry sessionRegistry;
	
	
    @Bean
    public SessionRegistry getSessionRegistry() {
        return new SessionRegistryImpl();
    }
	
    public  Integer getCountSessions() {
    	List<Object> principals = sessionRegistry.getAllPrincipals();
    	return principals.size();
    }
}
