/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.pdf.activite;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DecisionConcerneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ResponsablePublicEntity;

public class Action {

	private static final String AUTRES_RESPONSABLES_KEY = "Autres";

	private List<String> tiers;

	private Map<String, List<String>> reponsablesPublics;

	private List<String> decisionsConcernees;

	private List<String> actionsMenees;

	private String responsablePublicAutre;

	private String actionMeneeAutre;

	private String observation;

	public Action(ActionRepresentationInteretEntity action) {
		this.tiers = action.getTiers().stream().map(OrganisationEntity::getDenomination).collect(Collectors.toList());

		Map<String, List<String>> responsablesMap = action.getReponsablesPublics().stream().sorted(Comparator.comparing(ResponsablePublicEntity::getId))
				.collect(Collectors.groupingBy(ResponsablePublicEntity::getCategorie, Collectors.mapping(resp -> {
					return resp.getId().longValue() == 19 ? "Autre: " + action.getResponsablePublicAutre() : resp.getLibelle();
				}, Collectors.toList())));
		if (responsablesMap.containsKey(AUTRES_RESPONSABLES_KEY)) {
			for (String x : responsablesMap.get(AUTRES_RESPONSABLES_KEY)) {
				responsablesMap.put(x, null);
			}
			responsablesMap.remove(AUTRES_RESPONSABLES_KEY);
		}
		this.reponsablesPublics = responsablesMap;

		this.decisionsConcernees = action.getDecisionsConcernees().stream().map(DecisionConcerneeEntity::getLibelle).collect(Collectors.toList());
		this.actionsMenees = action.getActionsMenees().stream().map(act -> {
			return act.getId().longValue() == 10 ? "Autre: " + action.getActionMeneeAutre() : act.getLibelle();
		}).collect(Collectors.toList());
		this.responsablePublicAutre = action.getResponsablePublicAutre();
		this.actionMeneeAutre = action.getActionMeneeAutre();
		if (action.getObservation() == null || action.getObservation().isEmpty()) {
			this.observation = "Aucune observation";
		} else {
			this.observation = action.getObservation();
		}

	}

	public List<String> getTiers() {
		return tiers;
	}

	public Map<String, List<String>> getReponsablesPublics() {
		return reponsablesPublics;
	}

	public List<String> getDecisionsConcernees() {
		return decisionsConcernees;
	}

	public List<String> getActionsMenees() {
		return actionsMenees;
	}

	public String getResponsablePublicAutre() {
		return responsablePublicAutre;
	}

	public String getActionMeneeAutre() {
		return actionMeneeAutre;
	}

	public String getObservation() {
		return observation;
	}

}
