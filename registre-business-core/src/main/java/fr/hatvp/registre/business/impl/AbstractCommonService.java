/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.CommonService;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.exceptions.GlobalServerException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

/**
 * Classe abstraite responsable des traitements génériques.
 * @version $Revision$ $Date${0xD}
 */
@Transactional(readOnly = true)
public abstract class AbstractCommonService<T extends AbstractCommonDto, E extends AbstractCommonEntity>
    implements CommonService<T>
{

    /** LOGGER. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractCommonService.class);

    /**
     * Fournit le repository correspondant au service.
     *
     * @return le repository nécessaire au service
     */
    public abstract JpaRepository<E, Long> getRepository();

    /**
     * Fournit le transformer correspondant au service.
     *
     * @return le transformer associé
     */
    public abstract CommonTransformer<T, E> getTransformer();

    /**
     * {@inheritDoc}.
     */
    @Override
    @Transactional
    public T save(final T dto)
    {
        final E entity = this.getTransformer().dtoToModel(dto);
        this.getRepository().saveAndFlush(entity);
        final T dtoSaved = this.getTransformer().modelToDto(entity);
        return dtoSaved;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    @Transactional(readOnly = true)
    public T findOne(final Long id)
    {
        final E entity = this.getRepository().findOne(id);
        final T dto = this.getTransformer().modelToDto(entity);
        return dto;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    @Transactional(readOnly = true)
    public List<T> findAll()
    {
        final List<E> entityList = this.getRepository().findAll();
        return this.getTransformer().modelToDto(entityList);
    }

    /** {@inheritDoc}. */
    @Override
    @Transactional()
    public void delete(final Long id)
    {
        try {
            this.getRepository().delete(id);
        }
        catch (final Exception exception) {
            throw new GlobalServerException(ErrorMessageEnum.MSG_500.getMessage(), exception);
        }
    }



}
