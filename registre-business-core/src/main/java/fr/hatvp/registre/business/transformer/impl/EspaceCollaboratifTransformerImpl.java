/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.EspaceOrganisationLockBoTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceCollaboratifTransformer;
import fr.hatvp.registre.commons.dto.backoffice.ContactOperationnelSimpleDto;
import fr.hatvp.registre.commons.dto.backoffice.EspaceCollaboratifDto;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;

@Component
public class EspaceCollaboratifTransformerImpl implements EspaceCollaboratifTransformer {

	@Autowired
	private EspaceOrganisationLockBoTransformer espaceOrganisationLockBoTransformer;
	
	@Autowired
	private DesinscriptionRepository desinscriptionRepository;

	@Override
	public EspaceCollaboratifDto modelToDto(EspaceOrganisationEntity entity) {
		EspaceCollaboratifDto dto = new EspaceCollaboratifDto();

		dto.setId(entity.getId());
		dto.setInscriptionId(entity.getCreateurEspaceOrganisation().getInscriptionEspaces().stream().filter(i -> i.getEspaceOrganisation().getId().longValue() == entity.getId().longValue())
				.findAny().orElseGet(InscriptionEspaceEntity::new).getId());
		dto.setOrganisationId(entity.getOrganisation().getId());

		dto.setDenomination(entity.getOrganisation().getDenomination());
		dto.setIdentifiant(entity.getOrganisation().computeIdNational());
		dto.setNomUsage(entity.getNomUsage() != null ? entity.getNomUsage() : null);
		dto.setNomUsageHatvp(entity.getNomUsageHatvp());
		dto.setSigleHatvp(entity.getSigleHatvp());
		dto.setAncienNomHatvp(entity.getAncienNomHatvp());

		List<ContactOperationnelSimpleDto> contacts;
		if (StatutEspaceEnum.ACCEPTEE.equals(entity.getStatut())) {
			contacts = entity.getListeDesCollaborateurs().stream().map(col -> col.getDeclarant())
                    .filter(dec -> Optional.ofNullable(dec).isPresent()).map(dec -> dec.getInscriptionEspaces())
					.flatMap(ins -> ins.stream()).collect(Collectors.toSet()).stream().filter(ins -> ins.getEspaceOrganisation().getId().equals(entity.getId()))
					.filter(ins -> ins.getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR)).filter(ins -> !Optional.ofNullable(ins.getDateSuppression()).isPresent())
					.filter(ins -> Optional.ofNullable(ins.getDateValidation()).isPresent()).map(ins -> {
						return new ContactOperationnelSimpleDto(ins.getDeclarant().getId(), new StringBuilder(ins.getDeclarant().getCivility().toString()).append(" ")
								.append(ins.getDeclarant().getNom()).append(" ").append(ins.getDeclarant().getPrenom()).toString(),ins.isActif());
					}).collect(Collectors.toList());
		} else {
			contacts = new ArrayList<>();
			contacts.add(new ContactOperationnelSimpleDto(entity.getCreateurEspaceOrganisation().getId(), entity.getCreateurEspaceOrganisation().getCivility().toString() + " "
					+ entity.getCreateurEspaceOrganisation().getNom() + " " + entity.getCreateurEspaceOrganisation().getPrenom(), entity.getCreateurEspaceOrganisation().isActivated()));
		}
		dto.setContacts(contacts);

		dto.setCreationDate(entity.getCreationDate());
		dto.setValidationDate(entity.getDateActionCreation());
		dto.setStatut(entity.getStatut());

		dto.setFinExerciceFiscal(entity.getFinExerciceFiscal());

		dto.setIsPublication(entity.isPublicationEnabled());
		dto.setActif(entity.isEspaceActif());
		dto.setRelancesBloquees(entity.isRelancesBloquees());
		
		final DesinscriptionEntity desinscriptionEntity = this.desinscriptionRepository.findByEspaceOrganisation(entity);
		
		if(desinscriptionEntity != null) {
			ZoneId defaultZoneId = ZoneId.systemDefault();
			dto.setDateDesinscription(Date.from(desinscriptionEntity.getCessationDate().atStartOfDay(defaultZoneId).toInstant()));
		}

		dto.setLock(entity.getEspaceOrganisationLockBoEntityList().stream().map(this.espaceOrganisationLockBoTransformer::modelToDto).filter(lock -> lock.getLockTimeRemain() > 0).findFirst()
				.orElse(null));
		return dto;
	}

	@Override
	public List<EspaceCollaboratifDto> modelToDto(List<EspaceOrganisationEntity> entities) {
		return entities.stream().map(this::modelToDto).collect(Collectors.toList());
	}
}
