/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import fr.hatvp.registre.business.GestionPiecesEspaceDeclarantService;
import fr.hatvp.registre.business.transformer.proxy.EspaceDeclarantPieceTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.EspaceDeclarantPieceDto;
import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.piece.EspaceDeclarantPieceEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.declarant.EspaceDeclarantPiecesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implémentation du service de gestion des pièces attachées au déclarant.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Service
@Transactional
public class GestionPiecesEspaceDeclarantServiceImpl
    extends AbstractCommonService<EspaceDeclarantPieceDto, EspaceDeclarantPieceEntity>
    implements GestionPiecesEspaceDeclarantService
{
    /**
     * Le transformer pour la gestion des pièces versées.
     */
    @Autowired
    private EspaceDeclarantPieceTransformer espaceDeclarantPieceTransformer;
    
    /**
     * Repository pour l'accès à la table des pièces complémentaires en BDD.
     */
    @Autowired
    private EspaceDeclarantPiecesRepository espaceDeclarantPiecesRepository;
    
    /**
     * Repository pour l'accès au déclarant.
     */
    @Autowired
    private DeclarantRepository declarantRepository;
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public EspaceDeclarantPieceDto verserPiece(Long idDeclarant,
            EspaceDeclarantPieceDto piece,
        OrigineVersementEnum origine)  {
        // On détermine le déclarant qui enregistre les pièces.
        DeclarantEntity declarantEntity = this.declarantRepository.findOne(idDeclarant);
        
        // Transformation du DTO en entity
        EspaceDeclarantPieceEntity entity = this.getTransformer().dtoToModel(piece);

        entity.setDateVersementPiece(new Date());
        entity.setDeclarant(declarantEntity);
        entity.setOrigineVersement(origine);

        // Persitence de la pièce versée.
        EspaceDeclarantPieceEntity savedPieceEntity = this.getRepository().save(entity);

        return this.getTransformer().modelToDto(savedPieceEntity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<EspaceDeclarantPieceDto> getListePiecesAllEspaceDeclarant(final Long declarantId) {
        // On transforme la liste d'entity en dto en vidant le contenu du fichier qui n'est pas utile dans ce contexte.
        return this.espaceDeclarantPiecesRepository
            .findByDeclarantIdOrderByDateVersementPieceDesc(declarantId)
            .map(e -> {
                    EspaceDeclarantPieceDto dto = this.getTransformer().modelToDto(e);
                    // Inutile de renvoyer le contenu du fichier dans cette liste. Économisons la mémoire !
                    dto.setContenuFichier(null);
                    return dto;
                }
            )
            .collect(Collectors.toList());
    }

    /**
    * {@inheritDoc}
    */
    @Override
    public List<EspaceDeclarantPieceDto> getListePiecesEspaceDeclarant(final Long declarantId) {
        return this.getListePiecesEspaceDeclarant(declarantId, OrigineVersementEnum.INSCRIPTION_DECLARANT);
    }
    
    /**
     * Retourne la liste des pièces d'un espace filtrées selon leur origine.
     * @param declarantId
     * @param origine
     * @return la liste des pièces filtrée.
     */
    protected List<EspaceDeclarantPieceDto> getListePiecesEspaceDeclarant(final Long declarantId, final OrigineVersementEnum origine) {
        // On transforme la liste d'entity en dto en vidant le contenu du fichier qui n'est pas utile dans ce contexte.
        return this.espaceDeclarantPiecesRepository
            .findByDeclarantIdAndOrigineVersementOrderByDateVersementPieceDesc(declarantId, origine)
            .map(e -> {
                    EspaceDeclarantPieceDto dto = this.getTransformer().modelToDto(e);
                    // Inutile de renvoyer le contenu du fichier dans cette liste. Économisons la mémoire !
                    dto.setContenuFichier(null);
                    return dto;
                }
            )
            .collect(Collectors.toList());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public JpaRepository<EspaceDeclarantPieceEntity, Long> getRepository()
    {
        return this.espaceDeclarantPiecesRepository;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommonTransformer<EspaceDeclarantPieceDto, EspaceDeclarantPieceEntity> getTransformer()
    {
        return this.espaceDeclarantPieceTransformer;
    }

}
