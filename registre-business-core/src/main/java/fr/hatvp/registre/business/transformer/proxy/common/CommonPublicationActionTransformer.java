/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.common;

import fr.hatvp.registre.commons.dto.publication.activite.PublicationActionDto;
import fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity;
/**
 * Proxy du transformeur entre {@link fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity} et {@link fr.hatvp.registre.commons.dto.publication.activite.PublicationActionDto}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface CommonPublicationActionTransformer<E extends PublicationActionDto>
        extends CommonTransformer<E, ActionRepresentationInteretEntity> {

}