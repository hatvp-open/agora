/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.common;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.transformer.proxy.common.CommonDirigeantTransformer;
import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.common.CommonDirigeantEntity;

/**
 * Classe du transformeur entre {@link DirigeantDto} et
 * {@link fr.hatvp.registre.persistence.entity.espace.DirigeantEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
public abstract class CommonDirigeantTransformerImpl<E extends CommonDirigeantEntity>
    extends AbstractCommonTansformer<DirigeantDto, E>
    implements CommonDirigeantTransformer<E>
{

    /**
     * Classe à définir dans les classe filles.
     */
    protected Class<E> entityClass;

    /**
     * {@inheritDoc}
     */
    @Override
    public DirigeantDto modelToDto(final E model)
    {
        final DirigeantDto dto = new DirigeantDto();
        RegistreBeanUtils.copyProperties(dto, model);
        dto.setEspaceOrganisationId(model.getEspaceOrganisation().getId());

        return dto;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> dtoToModel(final List<DirigeantDto> dtos)
    {
        return dtos.stream().map(this::dtoToModel).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DirigeantDto> modelToDto(final List<E> models)
    {
        return models.stream().map(this::modelToDto).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Transactional(readOnly = true)
    @Override
    public E dtoToModel(final DirigeantDto dto)
    {
        final E model;

        try {
            model = this.entityClass.newInstance();
        }
        catch (InstantiationException | IllegalAccessException e) {
            throw new BusinessGlobalException(
                    "Erreur d'instanciation dans le transformeur des dirigeants");
        }

        dto.setNom(dto.getNom().toUpperCase());
        dto.setPrenom(RegistreUtils.normaliser(dto.getPrenom()));

        RegistreBeanUtils.copyProperties(model, dto);

        final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(dto.getEspaceOrganisationId());
        model.setEspaceOrganisation(espaceOrganisationEntity);

        return model;
    }
}
