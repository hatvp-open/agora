/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mifmif.common.regex.Generex;

import fr.hatvp.registre.business.backoffice.UtilisateurBoService;
import fr.hatvp.registre.business.impl.AbstractCommonService;
import fr.hatvp.registre.business.transformer.backoffice.UtilisateurBoTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoGestionDto;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.backoffice.RoleEnumBackOffice;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.commons.utils.ValidatePatternUtils;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.repository.backoffice.UtilisateurBoRepository;

/**
 * Classe de service des utilisateurs back-office {@link UtilisateurBoService} .
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional(readOnly = true)
public class UtilisateurBoServiceImpl extends AbstractCommonService<UtilisateurBoDto, UtilisateurBoEntity> implements UtilisateurBoService {

	private static final String UTILISATEUR_INTROUVABLE = "Utilisateur introuvable";

	/** Repository du service. */
	private final UtilisateurBoRepository utilisateurBoRepository;

	/** Tranformeur de la classe @link {@link UtilisateurBoEntity}. */
	private final UtilisateurBoTransformer utilisateurBoTransformer;

	/** Générateur de mot de passe de spring security. */
	private final PasswordEncoder passwordEncoder;

	/** Initialiser les services. */
	@Autowired
	public UtilisateurBoServiceImpl(final UtilisateurBoRepository utilisateurBoRepository, final UtilisateurBoTransformer utilisateurBoTransformer, final PasswordEncoder passwordEncoder) {
		this.utilisateurBoRepository = utilisateurBoRepository;
		this.utilisateurBoTransformer = utilisateurBoTransformer;
		this.passwordEncoder = passwordEncoder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UtilisateurBoDto findUtilisateurByEmail(final String email) {

		final Optional<UtilisateurBoEntity> utilisateurBoEntity = this.utilisateurBoRepository.findByEmail(email);

		return utilisateurBoEntity.map(this.utilisateurBoTransformer::modelToDto).orElse(null);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public UtilisateurBoGestionDto ajouterUtilisateurBo(final UtilisateurBoDto utilisateurBoDto) {

		if (this.utilisateurBoRepository.findByEmailIgnoreCase(utilisateurBoDto.getEmail()) != null) {
			throw new EntityExistsException(ErrorMessageEnum.ADRESSE_EMAIL_EXISTE_DEJA.getMessage());
		}

		utilisateurBoDto.setNom(utilisateurBoDto.getNom().toUpperCase());
		utilisateurBoDto.setPrenom(RegistreUtils.normaliser(utilisateurBoDto.getPrenom()));
		final UtilisateurBoEntity user = this.utilisateurBoTransformer.dtoToModel(utilisateurBoDto);
		final UtilisateurBoDto dtoProtected = this.getUtilisateurBoDto(user);
		final UtilisateurBoGestionDto dtoWithPasswordToFront = new UtilisateurBoGestionDto();
		RegistreBeanUtils.copyProperties(dtoWithPasswordToFront, dtoProtected);
		return dtoWithPasswordToFront;
	}

	/**
	 * persister l'utilisateur Bo et transformer le résultat en DTO afind e le renvoyer.
	 *
	 * @param user
	 *            entité utilisateur à compléter et transformer pour etre renvoyer ds le service rest.
	 * @return
	 */
	@Transactional
	UtilisateurBoDto getUtilisateurBoDto(final UtilisateurBoEntity user) {
		// A priori la génération du mot de passe BO n'est pas vouée à évoluer
		final Generex generex = new Generex(ValidatePatternUtils.getPasswordGenerationPattern());

		// générer un mot de passe.
		final String password = RegistreUtils.shuffle(generex.random());
		user.setPassword(this.passwordEncoder.encode(password));
		user.setCompteActive(true);
		user.setCompteSupprime(false);

		final UtilisateurBoDto dto = this.utilisateurBoTransformer.modelToDto(this.utilisateurBoRepository.save(user));
		dto.setPassword(password);
		return dto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void supprimerUtilisateur(final Long id) {
		final Optional<UtilisateurBoEntity> account = Optional.ofNullable(this.utilisateurBoRepository.findOne(id));

		account.ifPresent(ac -> {
			ac.setCompteSupprime(true);
			ac.setCompteActive(false);
			this.utilisateurBoRepository.save(ac);
		});

		account.orElseThrow(() -> new EntityNotFoundException(UTILISATEUR_INTROUVABLE));
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public UtilisateurBoDto majUtilisateurBo(final UtilisateurBoDto utilisateurBoDto) {
		final UtilisateurBoEntity storedUser = this.utilisateurBoRepository.findOne(utilisateurBoDto.getId());
		final UtilisateurBoEntity utilisateurBoEntity = this.utilisateurBoTransformer.dtoToModel(utilisateurBoDto);

		utilisateurBoEntity.setPassword(storedUser.getPassword());
		return this.utilisateurBoTransformer.modelToDto(this.utilisateurBoRepository.save(utilisateurBoEntity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public UtilisateurBoGestionDto updatePassword(final Long id) {

		final Optional<UtilisateurBoEntity> optional = Optional.ofNullable(this.utilisateurBoRepository.findOne(id));

		final UtilisateurBoEntity user = optional.orElseThrow(() -> new EntityNotFoundException(UTILISATEUR_INTROUVABLE));

		// initialiser le générateur de mot de passe.
		final UtilisateurBoDto dtoProtected = this.getUtilisateurBoDto(user);
		final UtilisateurBoGestionDto dtoWithPasswordToFront = new UtilisateurBoGestionDto();
		RegistreBeanUtils.copyProperties(dtoWithPasswordToFront, dtoProtected);
		return dtoWithPasswordToFront;

	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public UtilisateurBoDto switchActivationCompte(final UtilisateurBoDto utilisateurBoDto) {
		final UtilisateurBoEntity entity = this.utilisateurBoTransformer.dtoToModel(utilisateurBoDto);

		if (entity.getCompteActive().equals(Boolean.FALSE)) {
			entity.setCompteActive(Boolean.TRUE);
		} else {
			entity.setCompteActive(Boolean.FALSE);
			entity.getRoleEnumBackOffices().clear();
		}

		return this.utilisateurBoTransformer.modelToDto(this.utilisateurBoRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<UtilisateurBoDto> getAllBoUsersPaginated(Integer pageNumber, Integer resultPerPage) {
		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		final Page<UtilisateurBoEntity> pageUsersBo = this.utilisateurBoRepository.findAll(pageRequest);

		List<UtilisateurBoDto> usersBoDto = pageUsersBo.getContent().stream().map(u -> {
			u.setPassword(null);
			return this.utilisateurBoTransformer.modelToDto(u);
		}).filter(u -> u.getCompteSupprime().equals(Boolean.FALSE)).sorted(Comparator.comparing(AbstractCommonDto::getId)).collect(Collectors.toList());

		return new PaginatedDto<>(usersBoDto, (long) usersBoDto.size());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<UtilisateurBoDto> getAllBoUsersPaginatedParRecherche(String type, String keywords, Integer pageNumber, Integer resultPerPage) {
		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		Page<UtilisateurBoEntity> pageUsersBo;
		List<UtilisateurBoEntity> listUsersBo;
		switch (type) {
		case "identifiant":
			listUsersBo = this.utilisateurBoRepository.findByNomOrPrenom("%" + keywords + "%", pageRequest.getPageSize(), pageRequest.getOffset());
			pageUsersBo = new PageImpl<>(listUsersBo, pageRequest, this.utilisateurBoRepository.countUserBoFilteredByNomOrPrenom("%" + keywords + "%"));
			break;
		case "email":
			listUsersBo = this.utilisateurBoRepository.findByEmail("%" + keywords + "%", pageRequest.getPageSize(), pageRequest.getOffset());
			pageUsersBo = new PageImpl<>(listUsersBo, pageRequest, this.utilisateurBoRepository.countUserBoFilteredByEmail("%" + keywords + "%"));
			break;
		default:
			pageUsersBo = this.utilisateurBoRepository.findAll(pageRequest);
		}

		List<UtilisateurBoDto> usersBoDto = pageUsersBo.getContent().stream().map(u -> {
			u.setPassword(null);
			return this.utilisateurBoTransformer.modelToDto(u);
		}).filter(u -> u.getCompteSupprime().equals(Boolean.FALSE)).sorted(Comparator.comparing(AbstractCommonDto::getId)).collect(Collectors.toList());

		return new PaginatedDto<>(usersBoDto, (long) usersBoDto.size());
	}

	@Override
	public List<String> getMailOfUsersToNotify() {

		List<UtilisateurBoEntity> users = this.utilisateurBoRepository.findByRoleEnumBackOfficesContaining(RoleEnumBackOffice.ETAT_ACTIVITES);

		return users.stream().map(UtilisateurBoEntity::getEmail).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<UtilisateurBoEntity, Long> getRepository() {
		return this.utilisateurBoRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<UtilisateurBoDto, UtilisateurBoEntity> getTransformer() {
		return this.utilisateurBoTransformer;
	}
}
