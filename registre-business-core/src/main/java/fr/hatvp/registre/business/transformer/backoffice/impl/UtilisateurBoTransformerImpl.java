/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.impl;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.UtilisateurBoTransformer;
import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;


@Component
public class UtilisateurBoTransformerImpl
    extends AbstractCommonTansformer<UtilisateurBoDto, UtilisateurBoEntity>
    implements UtilisateurBoTransformer
{

    /**
     * {@inheritDoc}
     */
    @Override
    public UtilisateurBoEntity dtoToModel(final UtilisateurBoDto dto)
    {
        final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
        RegistreBeanUtils.copyProperties(utilisateurBoEntity, dto);
        utilisateurBoEntity.getRoleEnumBackOffices().addAll(dto.getRoles());
        return utilisateurBoEntity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UtilisateurBoDto modelToDto(final UtilisateurBoEntity model)
    {
        final UtilisateurBoDto utilisateurBoDto = new UtilisateurBoDto();
        RegistreBeanUtils.copyProperties(utilisateurBoDto, model);
        utilisateurBoDto.getRoles().addAll(model.getRoleEnumBackOffices());
        return utilisateurBoDto;
    }
}
