/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.InscriptionEspaceService;
import fr.hatvp.registre.business.email.EspaceOrganisationBoMailer;
import fr.hatvp.registre.business.email.SurveillanceMailer;
import fr.hatvp.registre.business.transformer.backoffice.ConsultationEspaceOrganisationSimpleTransformer;
import fr.hatvp.registre.business.transformer.backoffice.ValidationEspaceOrganisationDetailBoTransformer;
import fr.hatvp.registre.business.transformer.backoffice.ValidationEspaceOrganisationSimpleTransformer;
import fr.hatvp.registre.business.transformer.backoffice.ValidationEspaceOrganisationTransformer;
import fr.hatvp.registre.business.transformer.proxy.AssociationProTransformer;
import fr.hatvp.registre.business.transformer.proxy.ClientTransformer;
import fr.hatvp.registre.business.transformer.proxy.CollaborateurTransformer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.DirigeantTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceCollaboratifTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationBlacklistedTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationSimpleTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationTransformer;
import fr.hatvp.registre.business.transformer.proxy.InscriptionEspaceTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ExerciceComptableBlacklistedTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.dto.CollaborateurDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationAffichageFODto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationBlacklistedDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.dto.ListActiviteDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableBlacklistedDto;
import fr.hatvp.registre.commons.dto.backoffice.ConsultationEspaceBoSimpleDto;
import fr.hatvp.registre.commons.dto.backoffice.EspaceCollaboratifDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceBoDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceBoSimpleDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceDetailBoDto;
import fr.hatvp.registre.commons.dto.batch.EspaceOrganisationPublicationBatchDto;
import fr.hatvp.registre.commons.dto.client.ClientSimpleDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.PublicationRelanceNiveauEnum;
import fr.hatvp.registre.commons.lists.PublicationRelanceTypeEnum;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.commons.lists.backoffice.MotifComplementEspaceEnum;
import fr.hatvp.registre.commons.lists.backoffice.MotifRefusEspaceEnum;
import fr.hatvp.registre.commons.utils.PiecesUtils;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.commentaire.CommentEspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.commentaire.CommentValidationEspaceEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.PeriodeActiviteClientEntity;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity;
import fr.hatvp.registre.persistence.entity.publication.batch.relance.PublicationRelanceEntity;
import fr.hatvp.registre.persistence.repository.activite.PublicationActiviteRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationExerciceRepository;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentEspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentValidationEspaceRepository;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.AssoAppartenanceRepository;
import fr.hatvp.registre.persistence.repository.espace.ClientsRepository;
import fr.hatvp.registre.persistence.repository.espace.CollaborateurRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;
import fr.hatvp.registre.persistence.repository.espace.PeriodeActiviteClientRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;
import fr.hatvp.registre.persistence.repository.publication.batch.relance.PublicationRelanceRepository;

/**
 * Implémentation du service {@link EspaceOrganisationService}.
 */
@Transactional(readOnly = true)
@Service
public class EspaceOrganisationServiceImpl extends AbstractCommonService<ValidationEspaceBoDto, EspaceOrganisationEntity> implements EspaceOrganisationService {
	private static final String DDMMYYYY = "ddMMyyyy";
	/** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(EspaceOrganisationServiceImpl.class);


	/** Repository pour l'Espace Organisation. */
	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;
	
	/** Repository pour l'Espace Organisation. */
	@Autowired
	private DeclarantRepository declarantRepository;

	/** Repository pour l'inscription à l'Espace Organisation. */
	@Autowired
	private InscriptionEspaceRepository inscriptionEspaceRepository;

	@Autowired
    private ClientsRepository clientsRepository;

	@Autowired
    private CollaborateurRepository collaborateurRepository;

	/** Repository des commentaires de validation d'espace. */
	@Autowired
	private CommentValidationEspaceRepository commentValidationEspaceRepository;

	/** Repository des commentaires des espaces. */
	@Autowired
	private CommentEspaceOrganisationRepository commentEspaceOrganisationRepository;

    /** Repository pour des relances des publications. */
    @Autowired
    private PublicationRelanceRepository publicationRelanceRepository;
    
    @Autowired
    private PublicationRepository publicationRepository;
    
    /** Repository pour  des publications d'exercice. */
    @Autowired
    private PublicationExerciceRepository publicationExerciceRepository;
    
    /** Repository pour  des publications d'activite. */
    @Autowired
    private PublicationActiviteRepository publicationActiviteRepository;

	/** Service pour les inscriptions à l'espace corporate. */
	@Autowired
	private InscriptionEspaceService inscriptionEspaceService;

	/** Transformer pour l'Espace Organisation. */
	@Autowired
	private ValidationEspaceOrganisationTransformer validationEspaceOrganisationTransformer;

	/** Transformer pour le publicationComparatorDto. */
	@Autowired
	private ConsultationEspaceOrganisationSimpleTransformer consultationEspaceOrganisationSimpleTransformer;

	/** Transformer pour la liste de validation des ECs. */
	@Autowired
	private ValidationEspaceOrganisationSimpleTransformer validationEspaceOrganisationSimpleTransformer;

	/** Transformer pour la validation d'un EC. */
	@Autowired
	private ValidationEspaceOrganisationDetailBoTransformer validationEspaceOrganisationDetailBoTransformer;

    /** Transformer des exercice. */
    @Autowired
    private ExerciceComptableBlacklistedTransformer exerciceBlacklistedTransformer;
	/** Service envoi de mails. */
	@Autowired
	private EspaceOrganisationBoMailer espaceOrganisationBoMailer;

	/** Transformeur des inscriptions espace. */
	@Autowired
	private InscriptionEspaceTransformer inscriptionEspaceTransformer;

	@Autowired
    private ClientTransformer clientTransformer;

	@Autowired
    private CollaborateurTransformer collaborateurTransformer;

	/** Transformeur de l'espace organisation. */
	@Autowired
	private EspaceOrganisationTransformer espaceOrganisationTransformer;
	
	/** Transformeur de l'espace organisation sans les sous objets */
	@Autowired
	private EspaceOrganisationSimpleTransformer espaceOrganisationSimpleTransformer;

    /** Transformeur des espaces organisations blacklisté. */
    @Autowired
    private EspaceOrganisationBlacklistedTransformer espaceOrganisationBlacklistedTransformer;

	/** Tra,sformeur des déclarants. */
	@Autowired
	private DeclarantTransformer declarantTransformer;

	@Autowired
	private DirigeantTransformer dirigeantTransformer;

	@Autowired
	private SurveillanceMailer surveillanceMailer;

	@Autowired
	private EspaceCollaboratifTransformer espaceCollaboratifTransformer;
	
	@Autowired
    private AssoAppartenanceRepository assoAppartenanceRepository;
	
	@Autowired
	private AssociationProTransformer associationProTransformer;
	@Autowired
	private PeriodeActiviteClientRepository periodeActiviteClientRepository;

    /** Templates messages motifs. */
	/** Template 1 - Message de refus de création de l'espace. */
	@Value("${mail.bo.espace.refus.cree.message}")
	private String espaceRejetMessage1;
	/** Template 2 - Message de refus de création de l'espace. */
	@Value("${mail.bo.espace.refus.mandat.message}")
	private String espaceRejetMessage2;
	/** Template 3 - Message de refus de création de l'espace. */
	@Value("${mail.bo.espace.refus.organisation.message}")
	private String espaceRejetMessage3;
	/** Template 4 - Message de refus de création de l'espace. */
	@Value("${mail.bo.espace.refus.ineligible.message}")
	private String espaceRejetMessage4;
	/** Template 5 - Message de refus de création de l'espace. */
	@Value("${mail.bo.espace.refus.autre.message}")
	private String espaceRejetMessage5;

	/** Templates messages motifs demande complément. */
	/** Template 1 - Message de refus de création de l'espace. */
	@Value("${mail.bo.espace.complement.mandat.message}")
	private String espaceComplementMessageMandat;
	/** Template 2 - Message de refus de création de l'espace. */
	@Value("${mail.bo.espace.complement.cni.message}")
	private String espaceComplementMessageCni;
	/** Template 3 - Message de refus de création de l'espace. */
	@Value("${mail.bo.espace.complement.identite.message}")
	private String espaceComplementMessageIdentite;
	/** Template 4 - Message de refus de création de l'espace. */
	@Value("${mail.bo.espace.complement.autre.message}")
	private String espaceComplementMessageAutre;

	/** URL de téléchargement du mandat. */
	@Value("${app.mandat.download.url}")
	private String urlDownloadMandat;

	private final Map<String, StatutEspaceEnum> statutMap = new HashMap<>();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<EspaceOrganisationEntity, Long> getRepository() {
		return this.espaceOrganisationRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<ValidationEspaceBoDto, EspaceOrganisationEntity> getTransformer() {
		return this.validationEspaceOrganisationTransformer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void uploadLogo(final Long id, final byte[] logo_file, final String logo_type) {
		Optional.ofNullable(this.espaceOrganisationRepository.findOne(id)).ifPresent(e -> {
			e.setLogo(logo_file);
			e.setLogoType(logo_type);
			this.espaceOrganisationRepository.save(e);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void deleteLogo(final Long id) {
		Optional.ofNullable(this.espaceOrganisationRepository.findOne(id)).ifPresent(e -> {
			e.setLogo(null);
			e.setLogoType(null);
			this.espaceOrganisationRepository.save(e);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ValidationEspaceDetailBoDto getEspaceOrganisationEnCoursDeValidation(Long id) {
		final EspaceOrganisationEntity espace = Optional.ofNullable(this.espaceOrganisationRepository.findOne(id))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));
		if (StatutEspaceEnum.ACCEPTEE.equals(espace.getStatut())) {
			throw new BusinessGlobalException(ErrorMessageEnum.ESPACE_ORGANISATION_DEJA_VALIDE_OU_REFUSE.getMessage());
		}

		return this.validationEspaceOrganisationDetailBoTransformer.modelToDto(espace);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<ValidationEspaceBoSimpleDto> getEspacesOrganisationEnAttente(Integer pageNumber, Integer resultPerPage) {
		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		final Page<EspaceOrganisationEntity> pageEspaces = this.espaceOrganisationRepository.findByStatutNotAndStatutNotOrderByIdDesc(StatutEspaceEnum.ACCEPTEE, StatutEspaceEnum.REFUSEE,
				pageRequest);

		List<ValidationEspaceBoSimpleDto> espaceDtos = pageEspaces.getContent().stream().map(this.validationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());

		return new PaginatedDto<>(espaceDtos, pageEspaces.getTotalElements());
	}

    /**
     * {@inheritDoc}
     */
	@Transactional
	@Override
    public PaginatedDto<EspaceOrganisationBlacklistedDto> getEspacesOrganisationBlacklisted(Integer pageNumber, Integer resultPerPage) {
        PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
        //1. on recupere les Espaces orga qui ont l'exercice courant blackliste
        final Page<EspaceOrganisationEntity> pageEspaces = this.espaceOrganisationRepository.findByExerciceBlacklistes(pageRequest);

        //2. EC -> DTO
        List<EspaceOrganisationBlacklistedDto> espaceBlacklistedDtos = new ArrayList<>();
        for(EspaceOrganisationEntity espEntity : pageEspaces){
        	EspaceOrganisationBlacklistedDto espDto = this.espaceOrganisationBlacklistedTransformer.modelToDto(espEntity);    	
        	espDto.setListeExercices(this.getexerciceBlacklisteDto(espEntity));
        	espaceBlacklistedDtos.add(espDto);
        }
        return new PaginatedDto<>(espaceBlacklistedDtos, pageEspaces.getTotalElements());
    }

    /**
     * Récupère les espaces blacklisté en fonction d'un mot clé
     * @param type
     * @param keyword
     * @param page
     * @param occurrence
     * @return paginatedDTO des espaces filtrés
     */
	@Transactional
    @Override
    public PaginatedDto<EspaceOrganisationBlacklistedDto> getEspacesBlacklistedParRecherche(String type, String keyword, Integer page, Integer occurrence){
        PageRequest pageRequest = new PageRequest(page - 1, occurrence, Sort.Direction.ASC, "id");
        Page<EspaceOrganisationEntity> listEspaces = null;
        Long resultTotalNumber = null;

        switch (type) {
            case "denomination":
                keyword = "%"+keyword+"%";
                listEspaces = this.espaceOrganisationRepository.findByBlacklistRechercheDenomination(keyword.toUpperCase() , pageRequest);
                resultTotalNumber = listEspaces.getTotalElements();
                break;
            case "identifiant":
                keyword = "%"+keyword+"%";
                listEspaces = this.espaceOrganisationRepository.findByBlacklistRechercheIdentifiant(keyword, pageRequest);
                resultTotalNumber = listEspaces.getTotalElements();
                break;
            case "raisonBlacklist":
                PublicationRelanceTypeEnum pubRelanceType = PublicationRelanceTypeEnum.valueOf(keyword);
                listEspaces = this.publicationRelanceRepository.findByBlacklistRechercheRaisonBlacklist(pubRelanceType, pageRequest);
                resultTotalNumber = listEspaces.getTotalElements();
                break;
            default:
    			break;    
        }
        List<EspaceOrganisationBlacklistedDto> listEspacesFiltre = listEspaces.getContent().stream().map(this.espaceOrganisationBlacklistedTransformer::modelToDto).collect(Collectors.toList());
        for(int i=0; i< listEspacesFiltre.size(); i++){
            listEspacesFiltre.get(i).setListeExercices(this.getexerciceBlacklisteDto(listEspaces.getContent().get(i)));
        }
        return new PaginatedDto<>(listEspacesFiltre, resultTotalNumber);
    }

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<ValidationEspaceBoSimpleDto> getEspacesOrganisationEnAttenteParRecherche(String type, String keywords, Integer pageNumber, Integer resultPerPage) {
		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		Page<EspaceOrganisationEntity> pageEspaces;
		switch (type) {
		case "denomination":
			pageEspaces = this.espaceOrganisationRepository.findByOrganisationDenominationIgnoreCaseContainingAndStatutNotAndStatutNotOrderByIdAsc(keywords, StatutEspaceEnum.ACCEPTEE,
					StatutEspaceEnum.REFUSEE, pageRequest);
			break;
		case "demandeur":
			pageEspaces = this.espaceOrganisationRepository.findByCreateurEspaceOrganisationNomIgnoreCaseContainingAndStatutNotAndStatutNotOrderByIdAsc(keywords, StatutEspaceEnum.ACCEPTEE,
					StatutEspaceEnum.REFUSEE, pageRequest);
			break;
		case "dateDemande":
			pageEspaces = this.espaceOrganisationRepository.findByCreationDateBetweenAndStatutNotAndStatutNotOrderByCreationDateAsc(
					DateTime.parse(keywords, DateTimeFormat.forPattern(DDMMYYYY)).withTimeAtStartOfDay().toDate(),
					DateTime.parse(keywords, DateTimeFormat.forPattern(DDMMYYYY)).withTime(23, 59, 59, 0).toDate(), StatutEspaceEnum.ACCEPTEE, StatutEspaceEnum.REFUSEE, pageRequest);
			break;
		case "statut":
			switch (keywords) {
			case "NOUVELLE":
				pageEspaces = this.espaceOrganisationRepository.findByStatutAndStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.NOUVEAU, StatutEspaceEnum.ACCEPTEE,
						StatutEspaceEnum.REFUSEE, pageRequest);
				break;
			case "EN_TRAITEMENT":
				pageEspaces = this.espaceOrganisationRepository.findByStatutAndStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.EN_TRAITEMENT, StatutEspaceEnum.ACCEPTEE,
						StatutEspaceEnum.REFUSEE, pageRequest);
				break;
			case "EN_ATTENTE_VALIDATION":
				pageEspaces = this.espaceOrganisationRepository.findByStatutAndStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.EN_ATTENTE_VALIDATION, StatutEspaceEnum.ACCEPTEE,
						StatutEspaceEnum.REFUSEE, pageRequest);
				break;
			case "COMPLEMENT_DEMANDE_ENVOYEE":
				pageEspaces = this.espaceOrganisationRepository.findByStatutAndStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.COMPLEMENT_DEMANDE_ENVOYEE, StatutEspaceEnum.ACCEPTEE,
						StatutEspaceEnum.REFUSEE, pageRequest);
				break;
			case "COMPLEMENT_RECU":
				pageEspaces = this.espaceOrganisationRepository.findByStatutAndStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.COMPLEMENT_RECU, StatutEspaceEnum.ACCEPTEE,
						StatutEspaceEnum.REFUSEE, pageRequest);
				break;
			default:
				pageEspaces = this.espaceOrganisationRepository.findByStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.ACCEPTEE, StatutEspaceEnum.REFUSEE, pageRequest);
			}
			break;
		default:
			pageEspaces = this.espaceOrganisationRepository.findByStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.ACCEPTEE, StatutEspaceEnum.REFUSEE, pageRequest);
		}

		List<ValidationEspaceBoSimpleDto> espaceDtos = pageEspaces.getContent().stream().map(this.validationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());

		return new PaginatedDto<>(espaceDtos, pageEspaces.getTotalElements());
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void setPublicationState(final Long id, final boolean active) {
		Optional.ofNullable(this.espaceOrganisationRepository.findOne(id)).ifPresent(e -> {
			e.setPublicationEnabled(active);
			e.setNewPublication(Boolean.TRUE);
			if (!active) {
				e.getExercicesComptable().stream().forEach(ee->ee.setBlacklisted(false));
			}
			this.espaceOrganisationRepository.save(e);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void validateCreationRequest(final Long id, final Long userId) {
		final EspaceOrganisationEntity espaceOrganisation = this.espaceOrganisationRepository.findOne(id);

		// statut ACCEPTEE
		espaceOrganisation.setStatut(StatutEspaceEnum.ACCEPTEE);

		// utilisateur BO + date validation
		final UtilisateurBoEntity user = new UtilisateurBoEntity();
		user.setId(userId);
		espaceOrganisation.setUtilisateurActionCreation(user);
		espaceOrganisation.setDateActionCreation(new Date());
		espaceOrganisation.setEspaceActif(true);

		// ajout rôle Contact Opérationnel et Publicateur sur inscription
		this.inscriptionEspaceRepository
				.findByDateSuppressionIsNullAndDeclarantIdAndEspaceOrganisationId(espaceOrganisation.getCreateurEspaceOrganisation().getId(), espaceOrganisation.getId()).ifPresent(i -> {
					i.getRolesFront().add(RoleEnumFrontOffice.ADMINISTRATEUR);
					i.getRolesFront().add(RoleEnumFrontOffice.PUBLICATEUR);
					this.inscriptionEspaceRepository.save(i);
					// ajout des commentaires de validation à ceux de l'espace
					List<CommentValidationEspaceEntity> validationComment = this.commentValidationEspaceRepository.findByInscriptionEspaceEntityId(i.getId());
					List<CommentEspaceOrganisationEntity> espaceComment = validationComment.stream().map(val -> {
						CommentEspaceOrganisationEntity esp = new CommentEspaceOrganisationEntity();
						esp.setDateCreation(val.getDateCreation());
						esp.setEditeur(val.getEditeur());
						esp.setDateModification(val.getDateModification());
						esp.setRectificateur(val.getRectificateur());
						esp.setEspaceOrganisationEntity(espaceOrganisation);
						esp.setMessage(val.getMessage());
						return esp;
					}).collect(Collectors.toList());
					this.commentEspaceOrganisationRepository.save(espaceComment);
				});

		this.espaceOrganisationRepository.save(espaceOrganisation);

		// envoi mail validation
		this.espaceOrganisationBoMailer.sendValidationEspaceEmail(this.declarantTransformer.modelToDto(espaceOrganisation.getCreateurEspaceOrganisation()),
				espaceOrganisation.getOrganisation().getDenomination());

	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void rejectCreationRequest(final Long id, final Long userId, final String message) {
		final EspaceOrganisationEntity espaceOrganisation = this.espaceOrganisationRepository.findOne(id);

		// statut REFUSEE
		espaceOrganisation.setStatut(StatutEspaceEnum.REFUSEE);

		// utilisateur BO + date rejet création
		final UtilisateurBoEntity user = new UtilisateurBoEntity();
		user.setId(userId);
		espaceOrganisation.setUtilisateurActionCreation(user);
		espaceOrganisation.setDateActionCreation(new Date());
		espaceOrganisation.setEspaceActif(false);

		// rejeter demandes en attente sur l'espace organisation
		this.inscriptionEspaceRepository.findAllByEspaceOrganisationId(id)
				.forEach(e -> this.inscriptionEspaceService.setPendingRequestValidity(e.getId(), e.getEspaceOrganisation().getId(), false, false));

		// envoi mail refus
		this.espaceOrganisationBoMailer.sendRefusEspaceEmail(this.declarantTransformer.modelToDto(espaceOrganisation.getCreateurEspaceOrganisation()), message,
				espaceOrganisation.getOrganisation().getDenomination());

		this.espaceOrganisationRepository.save(espaceOrganisation);
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void demandeComplementInformation(final Long id, final String message) {
		final EspaceOrganisationEntity espaceOrganisation = this.espaceOrganisationRepository.findOne(id);

		// statut complément demandé
		espaceOrganisation.setStatut(StatutEspaceEnum.COMPLEMENT_DEMANDE_ENVOYEE);

		// envoi email demande complément
		this.espaceOrganisationBoMailer.sendDemandeComplementEspaceEmail(this.declarantTransformer.modelToDto(espaceOrganisation.getCreateurEspaceOrganisation()), message,
				espaceOrganisation.getOrganisation().getDenomination());

		// sauvegarde du changement de statut
		this.espaceOrganisationRepository.save(espaceOrganisation);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String generateRejectMessage(final Long id, final MotifRefusEspaceEnum idMotif) {
		String message = null;

		switch (idMotif) {
		case DEMANDE_NON_SERIEUSE:
			message = this.espaceRejetMessage1;
			break;
		case ID_CONFORME_MANDAT_NON_CONFORME:
			message = this.espaceRejetMessage2;
			break;
		case PIECE_ID_NON_CONFORME:
			message = this.espaceRejetMessage3;
			break;
		case ID_NON_CONFORME:
			message = this.espaceRejetMessage4;
			break;
		case AUTRE:
			message = this.espaceRejetMessage5;
			break;
		}

		if (message != null) {
			final EspaceOrganisationEntity model = this.espaceOrganisationRepository.findOne(id);

			message = StringUtils.replace(message, "[DENOMINATION]", model.getOrganisation().getDenomination());
			message = StringUtils.replace(message, "[MOTIF]", idMotif.toString());
			message = StringUtils.replace(message, "[DATE]", new SimpleDateFormat("dd/MM/yyyy à HH:mm").format(model.getCreationDate()));
			message = StringUtils.replace(message, "[LIEN_MANDAT]", this.urlDownloadMandat);
		}

		return message;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String generateDemandeComplementMessage(final Long id, final MotifComplementEspaceEnum motif) {
		String message = null;

		switch (motif) {
		case ID_CONFORME_MANDAT_NON_CONFORME:
			message = this.espaceComplementMessageMandat;
			break;
		case PIECE_ID_NON_CONFORME:
			message = this.espaceComplementMessageCni;
			break;
		case ID_NON_CONFORME:
			message = this.espaceComplementMessageIdentite;
			break;
		case AUTRE:
			message = this.espaceComplementMessageAutre;
			break;
		default:
			break;
		}

		if (message != null) {
			final EspaceOrganisationEntity model = this.espaceOrganisationRepository.findOne(id);

			message = StringUtils.replace(message, "[DENOMINATION]", model.getOrganisation().getDenomination());
			message = StringUtils.replace(message, "[MOTIF]", motif.toString());
			message = StringUtils.replace(message, "[DATE]", new SimpleDateFormat("dd/MM/yyyy à HH:mm").format(model.getCreationDate()));
			message = StringUtils.replace(message, "[LIEN_MANDAT]", this.urlDownloadMandat);
		}

		return message;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ValidationEspaceBoDto> getEspacesOrganisationsConfirmes() {
		return this.espaceOrganisationRepository.findByStatutOrderByIdAsc(StatutEspaceEnum.ACCEPTEE).map(this.validationEspaceOrganisationTransformer::modelToDto)
				.collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ValidationEspaceBoDto getEspacesOrganisationConfirmeById(Long id) {
		return this.validationEspaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.findOne(id));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<ConsultationEspaceBoSimpleDto> getEspacesOrganisationsConfirmes(Integer pageNumber, Integer resultPerPage) {
		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		final Page<EspaceOrganisationEntity> pageEspaces = this.espaceOrganisationRepository.findByStatutOrderByIdAsc(StatutEspaceEnum.ACCEPTEE, pageRequest);

		List<ConsultationEspaceBoSimpleDto> espaceDtos = pageEspaces.getContent().stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());

		return new PaginatedDto<>(espaceDtos, pageEspaces.getTotalElements());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<ConsultationEspaceBoSimpleDto> getEspacesOrganisationsConfirmesParRecherche(String type, String keywords, Integer pageNumber, Integer resultPerPage) {
		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		Page<EspaceOrganisationEntity> pageEspaces;
		List<ConsultationEspaceBoSimpleDto> espaceDtos;
		switch (type) {
		case "denomination":
			pageEspaces = this.espaceOrganisationRepository.findByOrganisationDenominationIgnoreCaseContainingAndStatutOrderByIdAsc(keywords, StatutEspaceEnum.ACCEPTEE, pageRequest);
			espaceDtos = pageEspaces.getContent().stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());
			break;
		case "identifiant":
			pageEspaces = this.espaceOrganisationRepository
					.findByStatutAndOrganisationSirenIgnoreCaseContainingOrStatutAndOrganisationRnaIgnoreCaseContainingOrStatutAndOrganisationDunsNumberIgnoreCaseContainingOrStatutAndOrganisationHatvpNumberIgnoreCaseContainingOrderByIdAsc(
							StatutEspaceEnum.ACCEPTEE, keywords, StatutEspaceEnum.ACCEPTEE, keywords, StatutEspaceEnum.ACCEPTEE, keywords, StatutEspaceEnum.ACCEPTEE, keywords, pageRequest);
			espaceDtos = pageEspaces.getContent().stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());
			break;
		case "administrateurs":
			List<EspaceOrganisationEntity> listEspaces = this.espaceOrganisationRepository.findByContactOperationnel("%" + keywords + "%", pageRequest.getPageSize(),
					pageRequest.getOffset());
			espaceDtos = listEspaces.stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());
			pageEspaces = new PageImpl<>(listEspaces, pageRequest, this.espaceOrganisationRepository.countEspaceFilteredByContactOperationnel("%" + keywords + "%"));
			break;
		case "nbrDeclarants":
			pageEspaces = this.espaceOrganisationRepository.findByStatutOrderByIdAsc(StatutEspaceEnum.ACCEPTEE, pageRequest);
			espaceDtos = pageEspaces.getContent().stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto)
					.filter(dto -> dto.getNombreDeclarant() == Integer.parseInt(keywords)).collect(Collectors.toList());
			break;
		case "dateCreation":
			pageEspaces = this.espaceOrganisationRepository.findByStatutAndCreationDateBetweenOrderByDateActionCreationAsc(StatutEspaceEnum.ACCEPTEE,
					DateTime.parse(keywords, DateTimeFormat.forPattern(DDMMYYYY)).withTimeAtStartOfDay().toDate(),
					DateTime.parse(keywords, DateTimeFormat.forPattern(DDMMYYYY)).withTime(23, 59, 59, 0).toDate(), pageRequest);
			espaceDtos = pageEspaces.getContent().stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());
			break;
		default:
			pageEspaces = this.espaceOrganisationRepository.findByStatutOrderByIdAsc(StatutEspaceEnum.ACCEPTEE, pageRequest);
			espaceDtos = pageEspaces.getContent().stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());
		}

		return new PaginatedDto<>(espaceDtos, pageEspaces.getTotalElements());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public ValidationEspaceBoDto switchActivationStatus(final Long espaceId) {
		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(espaceId))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));

		entity.setEspaceActif(!entity.isEspaceActif());
		if (!entity.isEspaceActif()) {
			entity.getExercicesComptable().stream().forEach(e->e.setBlacklisted(false));
			entity.setNewPublication(Boolean.TRUE);
		}

		return this.validationEspaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.save(entity));

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InscriptionEspaceDto> getEspaceOrganisationDeclarant(final Long espaceId) {
		// 1 On récupère les informations clients puis on transforme en DTO
		return this.inscriptionEspaceRepository.findAllByEspaceOrganisationId(espaceId).stream().map(this.inscriptionEspaceTransformer::modelToDto)
				.sorted(Comparator.comparing(AbstractCommonDto::getId)).collect(Collectors.toList());
	}

    /**
     * Retourne la liste de clienst triés par ordre alphabetique des dénominations
     * {@inheritDoc}
     */
	@Override
    public List<AssoClientDto> getEspaceOrganisationClients(final Long espaceId){
        return this.clientsRepository.findByEspaceOrganisationIdOrderByOrganisationDenominationAsc(espaceId).stream().map(clientTransformer::modelToDto)
                .collect(Collectors.toList());        
    }

    @Override
    public List<CollaborateurDto> getEspaceOrganisationCollaborateurs(final Long espaceId){
        return this.collaborateurRepository.findByEspaceOrganisationId(espaceId).stream().map(collaborateurTransformer::modelToDto)
            .sorted(Comparator.comparing(AbstractCommonDto::getId)).collect(Collectors.toList());
    }

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EspaceOrganisationDto getAllEspaceOrganisationInformations(final Long espaceOrganisationId) {

		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(espaceOrganisationId))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));
		EspaceOrganisationDto dto = this.espaceOrganisationTransformer.modelToDto(entity);

		// mise a jour du boolean ElementsAPublier
		dto.setElementsAPublier(entity.getElementsAPublier());

		// On ne renvoie pas la liste d'éventuelles anciennes associations si on déclare ne pas en avoir.
		if (dto.isNonDeclarationOrgaAppartenance()) {
			dto.getAssosAppartenance().clear();
		}

		return dto;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public EspaceOrganisationAffichageFODto getEspaceOrganisationInformations(final Long espaceOrganisationId) {

		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(espaceOrganisationId))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));
		EspaceOrganisationAffichageFODto dto = this.espaceOrganisationSimpleTransformer.modelToDto(entity);

		// mise a jour du boolean ElementsAPublier
		dto.setElementsAPublier(entity.getElementsAPublier());


		return dto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public EspaceOrganisationDto updateDirigeants(final EspaceOrganisationDto dto) {
		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(dto.getId()))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));

		entity.setDirigeants(dirigeantTransformer.dtoToModel(dto.getDirigeants()));
		entity.setElementsAPublier(true);
		return this.espaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public EspaceOrganisationDto updateDonnees(final EspaceOrganisationDto dto) {
		/**
		 * Cette méthode est utilisée pour les deux anglets: Données financières et Champs des actuvités de représentation d'interets
		 */
		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(dto.getId()))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));

		// Catégorie de l'orgaisation
		entity.setCategorieOrganisation(dto.getCategorieOrganisation());
		// Chiffre d'affaire
		entity.setChiffreAffaire(dto.getChiffreAffaire());
		entity.setAnneeChiffreAffaire(dto.getAnneeChiffreAffaire());
		// Dépenses
		entity.setAnneeDepenses(dto.getAnneeDepenses());
		entity.setDepenses(dto.getDepenses());
		// budget
		entity.setAnneeBudget(dto.getAnneeBudget());
		entity.setBudgetTotal(dto.getBudgetTotal());
		// personnes employées
		entity.setnPersonnesEmployees(dto.getnPersonnesEmployees());
		// update des éctivités de représentation d'intéret.
		entity.setChiffreAffaireRepresentationInteret(dto.getChiffreAffaireRepresentationInteret());
		entity.setElementsAPublier(true);
		
		return this.espaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public EspaceOrganisationDto updateInfosContact(final EspaceOrganisationDto dto) {
		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(dto.getId()))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));

		entity.setTelephoneDeContact(dto.getTelephoneDeContact());
		entity.setNonPublierMonTelephoneDeContact(dto.getNonPublierMonTelephoneDeContact());
		entity.setEmailDeContact(dto.getEmailDeContact());
		entity.setNonPublierMonAdresseEmail(dto.getNonPublierMonAdresseEmail());
		entity.setElementsAPublier(true);
		return this.espaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public EspaceOrganisationDto updateLocalisation(final EspaceOrganisationDto dto) {
		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(dto.getId()))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));

		entity.setAdresse(dto.getAdresse());
		entity.setCodePostal(dto.getCodePostal());
		entity.setVille(dto.getVille());
		entity.setPays(dto.getPays());
		entity.setNonPublierMonAdressePhysique(dto.getNonPublierMonAdressePhysique());
		entity.setElementsAPublier(true);
		return this.espaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public EspaceOrganisationDto updateProfileOrga(final EspaceOrganisationDto dto, boolean fromBO) {
		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(dto.getId()))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));
 
		// TODO controle date
		//Le type d'orga n'est pas in input coté BO => pas retourné
		if(!fromBO)entity.setCategorieOrganisation(dto.getCategorieOrganisation());
		entity.setNomUsage((dto.getNomUsage() == null || dto.getNomUsage().trim().isEmpty()) ? null : dto.getNomUsage().toUpperCase());

		if(fromBO) {
		    //		à la mise à jour de la dénomination coté BO on force la republication
			if(dto.getDenomination() != null && !"".equals(dto.getDenomination())) {
				entity.getOrganisation().setDenomination(dto.getDenomination().toUpperCase());				
				
//				on récupère la derniere publication si elle existe pour la mettre à jour et on force la publi de la nouvelle dénomination
				PublicationEntity dernierePublication = publicationRepository.findFirstByEspaceOrganisationIdOrderByIdDesc(dto.getId());
				if(dernierePublication != null) {
					dernierePublication.setDenomination(dto.getDenomination().toUpperCase());
					publicationRepository.save(dernierePublication);
					entity.setNewPublication(true);
					LOGGER.info("Modification denomination espace organisation : {}", dto.getId()) ;
				}
			}
			entity.setNomUsageHatvp((dto.getNomUsageHatvp() == null || dto.getNomUsageHatvp().trim().isEmpty()) ? null : dto.getNomUsageHatvp().toUpperCase());
			entity.setAncienNomHatvp((dto.getAncienNomHatvp() == null || dto.getAncienNomHatvp().trim().isEmpty()) ? null : dto.getAncienNomHatvp().toUpperCase());
			entity.setSigleHatvp((dto.getSigleHatvp() == null || dto.getSigleHatvp().trim().isEmpty()) ? null : dto.getSigleHatvp().toUpperCase());
			entity.setFinExerciceFiscal(dto.getFinExerciceFiscal());
		}else {
			entity.setElementsAPublier(true);
		}
		
		return this.espaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.save(entity));	
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public EspaceOrganisationDto updateWeb(final EspaceOrganisationDto dto) {
		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(dto.getId()))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));

		entity.setLienSiteWeb(dto.getLienSiteWeb());
		entity.setLienListeTiers(dto.getLienListeTiers());
		entity.setLienPageFacebook(dto.getLienPageFacebook());
		entity.setLienPageLinkedin(dto.getLienPageLinkedin());
		entity.setLienPageTwitter(dto.getLienPageTwitter());
		entity.setElementsAPublier(true);
		return this.espaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public EspaceOrganisationDto updateLienMembres(EspaceOrganisationDto espaceOrganisation, Long currentEspaceOrganisationId) {
		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(currentEspaceOrganisationId))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));

		entity.setLienListeTiers(espaceOrganisation.getLienListeTiers());

		entity.setElementsAPublier(true);
		return this.espaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public ValidationEspaceDetailBoDto updateEspaceOrganisationEnCoursDeValidation(final ValidationEspaceDetailBoDto espaceOrganisationDto, final Long connectedUserId) {
		final EspaceOrganisationEntity espaceOrganisationEntity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(espaceOrganisationDto.getId()))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));

		espaceOrganisationEntity.getOrganisation().setDenomination(espaceOrganisationDto.getDenomination());
		espaceOrganisationEntity.setNomUsage(espaceOrganisationDto.getNomUsage());
		espaceOrganisationEntity.setNomUsageHatvp(espaceOrganisationDto.getNomUsageHatvp());
		espaceOrganisationEntity.setSigleHatvp(espaceOrganisationDto.getSigleHatvp());
		espaceOrganisationEntity.setAncienNomHatvp(espaceOrganisationDto.getAncienNomHatvp());
		espaceOrganisationEntity.setAdresse(espaceOrganisationDto.getAdresse());
		espaceOrganisationEntity.setCodePostal(espaceOrganisationDto.getCodePostal());
		espaceOrganisationEntity.setVille(espaceOrganisationDto.getVille());
		espaceOrganisationEntity.setPays(espaceOrganisationDto.getPays());
		espaceOrganisationEntity.setStatut(espaceOrganisationDto.getStatut());

		return this.validationEspaceOrganisationDetailBoTransformer.modelToDto(this.espaceOrganisationRepository.save(espaceOrganisationEntity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public EspaceOrganisationDto updateActivites(final EspaceOrganisationDto dto) {
		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(dto.getId()))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));

		dto.getListSecteursActivites().forEach(s -> {
			boolean secteurExist = false;
			for (final SecteurActiviteEnum secteurActiviteEnum : SecteurActiviteEnum.values()) {
				if (s.name().equals(secteurActiviteEnum.name())) {
					secteurExist = true;
				}
			}
			if (!secteurExist) {
				throw new BusinessGlobalException(ErrorMessageEnum.CODE_SECTEUR_ACTIVITE_NOT_FOUND.getMessage());
			}
		});
		entity.setSecteursActivites(RegistreUtils.implodeListWithComma(dto.getListSecteursActivites()));

		dto.getListNiveauIntervention().forEach(n -> {
			boolean interventionExist = false;
			for (final NiveauInterventionEnum intervention : NiveauInterventionEnum.values()) {
				if (n.name().equals(intervention.name())) {
					interventionExist = true;
				}
			}
			if (!interventionExist) {
				throw new BusinessGlobalException(ErrorMessageEnum.CODE_NIVEAU_INTERVENTION_NOT_FOUND.getMessage());
			}
		});
		entity.setNiveauIntervention(RegistreUtils.implodeListWithComma(dto.getListNiveauIntervention()));
		entity.setElementsAPublier(Boolean.TRUE);

		EspaceOrganisationEntity savedEntity = this.espaceOrganisationRepository.save(entity);
        SurveillanceEntity surveillance = savedEntity.getOrganisation().getSurveillanceEntity();
		if (surveillance!=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.AJOUT_SECTEUR_ACTIVITE)) {
			this.surveillanceMailer.sendSurveillanceMail(savedEntity.getOrganisation(), savedEntity, SurveillanceOrganisationEnum.AJOUT_SECTEUR_ACTIVITE,"");
		}

		return this.espaceOrganisationTransformer.modelToDto(savedEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public EspaceOrganisationDto updateExerciceComptable(final EspaceOrganisationDto dto) {
		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(dto.getId()))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));
		entity.setFinExerciceFiscal(dto.getFinExerciceFiscal());
		entity.setNonExerciceComptable(dto.getNonExerciceComptable());
		entity.setElementsAPublier(true);
		return this.espaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ListActiviteDto getListeDesActiviteDeRepresentationDinteret() {

		final ListActiviteDto dto = new ListActiviteDto();

		for (final SecteurActiviteEnum secteurActiviteEnum : SecteurActiviteEnum.values()) {
			dto.getSecteurActiviteEnumList().add(secteurActiviteEnum);
		}

		for (final NiveauInterventionEnum niveauInterventionEnum : NiveauInterventionEnum.values()) {
			dto.getNiveauInterventionEnums().add(niveauInterventionEnum);
		}

		return dto;
	}

	/**
	 * true & client => desactivation des clients à la date du jour
	 * false & client => clients restent désactivés
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public EspaceOrganisationDto setAttestationClient(final Long currentEspaceId, final EspaceOrganisationDto espaceOrganisationDto) {
		final EspaceOrganisationEntity entity = this.espaceOrganisationRepository.findOne(currentEspaceId);
		entity.setNonDeclarationTiers(espaceOrganisationDto.isNonDeclarationTiers());
		if (entity.isNonDeclarationTiers()) {
			entity.getClients().stream().forEach(client -> {
				//récupération de la période en cours pour y mettre la date de désactivation
				PeriodeActiviteClientEntity periodeActiviteEnCours = client.getPeriodeActiviteClientList().stream().sorted(Comparator.comparing(PeriodeActiviteClientEntity::getDateAjout).reversed()).collect(Collectors.toList()).get(0);
				
				if(periodeActiviteEnCours.getDateDesactivation() == null) {
					periodeActiviteEnCours.setDateDesactivation(LocalDateTime.now());
					periodeActiviteEnCours.setStatut(StatutPublicationEnum.MAJ_NON_PUBLIEE);
					
					periodeActiviteClientRepository.saveAndFlush(periodeActiviteEnCours);
				}				
			});
		}
		entity.setElementsAPublier(true);
		return this.espaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public EspaceOrganisationDto setAttestationAssociation(final Long currentEspaceId, final EspaceOrganisationDto espaceOrganisationDto) {
		final EspaceOrganisationEntity entity = this.espaceOrganisationRepository.findOne(currentEspaceId);
		entity.setNonDeclarationOrgaAppartenance(espaceOrganisationDto.isNonDeclarationOrgaAppartenance());
		entity.setElementsAPublier(true);
		return this.espaceOrganisationTransformer.modelToDto(this.espaceOrganisationRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<EspaceOrganisationPublicationBatchDto> listeEspacesAPublier() {
		return this.espaceOrganisationRepository.findByNewPublicationTrueAndPublicationEnabledTrue().map(this.espaceOrganisationTransformer::modelToPublicationDto)
				.collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<EspaceOrganisationPublicationBatchDto> listeEspacesADepublier() {
		return this.espaceOrganisationRepository.findByNewPublicationTrueAndPublicationEnabledFalse().map(this.espaceOrganisationTransformer::modelToPublicationDto)
				.collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Long> listeIdEspacesPublies() {
		return this.espaceOrganisationRepository.findIdEspacesPublies();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ClientSimpleDto> getClientEspaceOrganisation(Long currentEspaceOrganisationId) {
		final EspaceOrganisationEntity entity = this.espaceOrganisationRepository.findOne(currentEspaceOrganisationId);
		final List<ClientSimpleDto> clients = entity.getClients().stream().map(client -> {
			return new ClientSimpleDto(client.getOrganisation().getId(), client.getOrganisation().getDenomination(), client.getOrganisation().getNomUsageSiren());
		}).collect(Collectors.toList());
		return clients;
	}

	@Override
	@Transactional
	public void updateFlagElementsAPublier(Long currentEspaceOrganisationId) {
		final EspaceOrganisationEntity entity = this.espaceOrganisationRepository.findOne(currentEspaceOrganisationId);

		entity.setElementsAPublier(true);
		this.espaceOrganisationRepository.save(entity);
        this.espaceOrganisationRepository.flush();

	}

	@Override
	@Transactional
	public void bloquerRelances(Long espaceId, Long userId) {
		EspaceOrganisationEntity entity = this.espaceOrganisationRepository.findOne(espaceId);
		entity.setRelancesBloquees(true);
		this.espaceOrganisationRepository.save(entity);
		this.espaceOrganisationRepository.flush();

	}

	@Override
	@Transactional
	public void debloquerRelances(Long espaceId, Long userId) {
		EspaceOrganisationEntity entity = this.espaceOrganisationRepository.findOne(espaceId);
		entity.setRelancesBloquees(false);
		this.espaceOrganisationRepository.save(entity);
		this.espaceOrganisationRepository.flush();

	}

	public PaginatedDto<ValidationEspaceBoSimpleDto> getEspacesOrganisationEnAttente2(Integer pageNumber, Integer resultPerPage) {
		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		final Page<EspaceOrganisationEntity> pageEspaces = this.espaceOrganisationRepository.findByStatutNotAndStatutNotOrderByIdDesc(StatutEspaceEnum.ACCEPTEE, StatutEspaceEnum.REFUSEE,
				pageRequest);

		List<ValidationEspaceBoSimpleDto> espaceDtos = pageEspaces.getContent().stream().map(this.validationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());

		return new PaginatedDto<>(espaceDtos, pageEspaces.getTotalElements());
	}

	public PaginatedDto<ValidationEspaceBoSimpleDto> getEspacesOrganisationEnAttenteParRecherche2(String type, String keywords, Integer pageNumber, Integer resultPerPage) {
		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		Page<EspaceOrganisationEntity> pageEspaces;
		switch (type) {
		case "denomination":
			pageEspaces = this.espaceOrganisationRepository.findByOrganisationDenominationIgnoreCaseContainingAndStatutNotAndStatutNotOrderByIdAsc(keywords, StatutEspaceEnum.ACCEPTEE,
					StatutEspaceEnum.REFUSEE, pageRequest);
			break;
		case "demandeur":
			pageEspaces = this.espaceOrganisationRepository.findByCreateurEspaceOrganisationNomIgnoreCaseContainingAndStatutNotAndStatutNotOrderByIdAsc(keywords, StatutEspaceEnum.ACCEPTEE,
					StatutEspaceEnum.REFUSEE, pageRequest);
			break;
		case "dateDemande":
			pageEspaces = this.espaceOrganisationRepository.findByCreationDateBetweenAndStatutNotAndStatutNotOrderByCreationDateAsc(
					DateTime.parse(keywords, DateTimeFormat.forPattern(DDMMYYYY)).withTimeAtStartOfDay().toDate(),
					DateTime.parse(keywords, DateTimeFormat.forPattern(DDMMYYYY)).withTime(23, 59, 59, 0).toDate(), StatutEspaceEnum.ACCEPTEE, StatutEspaceEnum.REFUSEE, pageRequest);
			break;
		case "statut":
			switch (keywords) {
			case "NOUVELLE":
				pageEspaces = this.espaceOrganisationRepository.findByStatutAndStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.NOUVEAU, StatutEspaceEnum.ACCEPTEE,
						StatutEspaceEnum.REFUSEE, pageRequest);
				break;
			case "EN_TRAITEMENT":
				pageEspaces = this.espaceOrganisationRepository.findByStatutAndStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.EN_TRAITEMENT, StatutEspaceEnum.ACCEPTEE,
						StatutEspaceEnum.REFUSEE, pageRequest);
				break;
			case "EN_ATTENTE_VALIDATION":
				pageEspaces = this.espaceOrganisationRepository.findByStatutAndStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.EN_ATTENTE_VALIDATION, StatutEspaceEnum.ACCEPTEE,
						StatutEspaceEnum.REFUSEE, pageRequest);
				break;
			case "COMPLEMENT_DEMANDE_ENVOYEE":
				pageEspaces = this.espaceOrganisationRepository.findByStatutAndStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.COMPLEMENT_DEMANDE_ENVOYEE, StatutEspaceEnum.ACCEPTEE,
						StatutEspaceEnum.REFUSEE, pageRequest);
				break;
			case "COMPLEMENT_RECU":
				pageEspaces = this.espaceOrganisationRepository.findByStatutAndStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.COMPLEMENT_RECU, StatutEspaceEnum.ACCEPTEE,
						StatutEspaceEnum.REFUSEE, pageRequest);
				break;
			default:
				pageEspaces = this.espaceOrganisationRepository.findByStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.ACCEPTEE, StatutEspaceEnum.REFUSEE, pageRequest);
			}
			break;
		default:
			pageEspaces = this.espaceOrganisationRepository.findByStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum.ACCEPTEE, StatutEspaceEnum.REFUSEE, pageRequest);
		}

		List<ValidationEspaceBoSimpleDto> espaceDtos = pageEspaces.getContent().stream().map(this.validationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());

		return new PaginatedDto<>(espaceDtos, pageEspaces.getTotalElements());
	}

	@Override
	public Integer getNombreDemandesParStatut(StatutEspaceEnum statut) {
		return this.espaceOrganisationRepository.findByStatut(statut).size();
	}

	@Override
	public long countNombreEspace() {
		return this.espaceOrganisationRepository.count();
	}

	@Override
	public PaginatedDto<EspaceCollaboratifDto> getNotValidatedEspacePaginated(Integer page, Integer occurrence) {
		PageRequest pageRequest = new PageRequest(page - 1, occurrence, Sort.Direction.DESC, "id");
		Page<EspaceOrganisationEntity> entities = this.espaceOrganisationRepository.findByStatutNotAndStatutNotOrderByIdDesc(StatutEspaceEnum.ACCEPTEE, StatutEspaceEnum.REFUSEE,
				pageRequest);
		List<EspaceCollaboratifDto> dtos;
		dtos = this.espaceCollaboratifTransformer.modelToDto(entities.getContent());
		return new PaginatedDto<>(dtos, entities.getTotalElements());
	}

	public PaginatedDto<ConsultationEspaceBoSimpleDto> get(String type, String keywords, Integer pageNumber, Integer resultPerPage) {
		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		Page<EspaceOrganisationEntity> pageEspaces;
		List<ConsultationEspaceBoSimpleDto> espaceDtos;
		switch (type) {
		case "denomination":
			pageEspaces = this.espaceOrganisationRepository.findByOrganisationDenominationIgnoreCaseContainingAndStatutOrderByIdAsc(keywords, StatutEspaceEnum.ACCEPTEE, pageRequest);
			espaceDtos = pageEspaces.getContent().stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());
			break;
		case "identifiant":
			pageEspaces = this.espaceOrganisationRepository
					.findByStatutAndOrganisationSirenIgnoreCaseContainingOrStatutAndOrganisationRnaIgnoreCaseContainingOrStatutAndOrganisationDunsNumberIgnoreCaseContainingOrStatutAndOrganisationHatvpNumberIgnoreCaseContainingOrderByIdAsc(
							StatutEspaceEnum.ACCEPTEE, keywords, StatutEspaceEnum.ACCEPTEE, keywords, StatutEspaceEnum.ACCEPTEE, keywords, StatutEspaceEnum.ACCEPTEE, keywords, pageRequest);
			espaceDtos = pageEspaces.getContent().stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());
			break;
		case "administrateurs":
			List<EspaceOrganisationEntity> listEspaces = this.espaceOrganisationRepository.findByContactOperationnel("%" + keywords + "%", pageRequest.getPageSize(),
					pageRequest.getOffset());
			espaceDtos = listEspaces.stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());
			pageEspaces = new PageImpl<>(listEspaces, pageRequest, this.espaceOrganisationRepository.countEspaceFilteredByContactOperationnel("%" + keywords + "%"));
			break;
		case "nbrDeclarants":
			pageEspaces = this.espaceOrganisationRepository.findByStatutOrderByIdAsc(StatutEspaceEnum.ACCEPTEE, pageRequest);
			espaceDtos = pageEspaces.getContent().stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto)
					.filter(dto -> dto.getNombreDeclarant() == Integer.parseInt(keywords)).collect(Collectors.toList());
			break;
		case "dateCreation":
			pageEspaces = this.espaceOrganisationRepository.findByStatutAndCreationDateBetweenOrderByDateActionCreationAsc(StatutEspaceEnum.ACCEPTEE,
					DateTime.parse(keywords, DateTimeFormat.forPattern(DDMMYYYY)).withTimeAtStartOfDay().toDate(),
					DateTime.parse(keywords, DateTimeFormat.forPattern(DDMMYYYY)).withTime(23, 59, 59, 0).toDate(), pageRequest);
			espaceDtos = pageEspaces.getContent().stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());
			break;
		default:
			pageEspaces = this.espaceOrganisationRepository.findByStatutOrderByIdAsc(StatutEspaceEnum.ACCEPTEE, pageRequest);
			espaceDtos = pageEspaces.getContent().stream().map(this.consultationEspaceOrganisationSimpleTransformer::modelToDto).collect(Collectors.toList());
		}

		return new PaginatedDto<>(espaceDtos, pageEspaces.getTotalElements());
	}

	@Override
	public PaginatedDto<EspaceCollaboratifDto> getEspacesBySearch(String type, String keyword, Integer page, Integer occurrence) {
		PageRequest pageRequest = new PageRequest(page - 1, occurrence, Sort.Direction.DESC, "id");
		Page<EspaceOrganisationEntity> pageEspaces = null;
		List<EspaceOrganisationEntity> listEspaces = null;
		switch (type) {
		case "denomination":
			listEspaces = this.espaceOrganisationRepository.findByDenominationOrNomUsage("%" + keyword + "%", pageRequest.getPageSize(), pageRequest.getOffset());
			pageEspaces = new PageImpl<>(listEspaces, pageRequest, this.espaceOrganisationRepository.countEspaceFilteredByDenominationOrNomUsage("%" + keyword + "%"));
			break;
		case "identifiant":
			pageEspaces = this.espaceOrganisationRepository
					.findByOrganisationSirenIgnoreCaseContainingOrOrganisationRnaIgnoreCaseContainingOrOrganisationHatvpNumberIgnoreCaseContainingOrderByIdDesc(keyword, keyword, keyword,
							pageRequest);
			break;
		case "contacts":
			listEspaces = this.espaceOrganisationRepository.findByContactOperationnelOrCreateur("%" + keyword + "%", pageRequest.getPageSize(), pageRequest.getOffset());
			pageEspaces = new PageImpl<>(listEspaces, pageRequest, this.espaceOrganisationRepository.countEspaceFilteredByContactOperationnelAndCreateur("%" + keyword + "%"));
			break;
		case "creationDate":
			pageEspaces = this.espaceOrganisationRepository.findByCreationDateBetweenOrderByCreationDateDesc(
					DateTime.parse(keyword, DateTimeFormat.forPattern(DDMMYYYY)).withTimeAtStartOfDay().toDate(),
					DateTime.parse(keyword, DateTimeFormat.forPattern(DDMMYYYY)).withTime(23, 59, 59, 0).toDate(), pageRequest);
			break;
		case "validationDate":
			pageEspaces = this.espaceOrganisationRepository.findByDateActionCreationBetweenOrderByDateActionCreationDesc(
					DateTime.parse(keyword, DateTimeFormat.forPattern(DDMMYYYY)).withTimeAtStartOfDay().toDate(),
					DateTime.parse(keyword, DateTimeFormat.forPattern(DDMMYYYY)).withTime(23, 59, 59, 0).toDate(), pageRequest);
			break;
		case "statut":
			pageEspaces = this.espaceOrganisationRepository.findByStatutOrderByIdDesc(this.statutMap.get(keyword), pageRequest);
			break;
		default:
			break;
		}
		return new PaginatedDto<>(this.espaceCollaboratifTransformer.modelToDto(pageEspaces.getContent()), pageEspaces.getTotalElements());
	}

	{
		this.statutMap.put("NOUVEAU", StatutEspaceEnum.NOUVEAU);
		this.statutMap.put("EN_TRAITEMENT", StatutEspaceEnum.EN_TRAITEMENT);
		this.statutMap.put("EN_ATTENTE_VALIDATION", StatutEspaceEnum.EN_ATTENTE_VALIDATION);
		this.statutMap.put("COMPLEMENT_DEMANDE_ENVOYEE", StatutEspaceEnum.COMPLEMENT_DEMANDE_ENVOYEE);
		this.statutMap.put("COMPLEMENT_RECU", StatutEspaceEnum.COMPLEMENT_RECU);
		this.statutMap.put("ACCEPTEE", StatutEspaceEnum.ACCEPTEE);
		this.statutMap.put("REFUSEE", StatutEspaceEnum.REFUSEE);
		this.statutMap.put("DESINSCRIT", StatutEspaceEnum.DESINSCRIT);
		this.statutMap.put("DESINSCRIPTION_DEMANDEE", StatutEspaceEnum.DESINSCRIPTION_DEMANDEE);
		this.statutMap.put("DESINSCRIPTION_VALIDEE_HATVP", StatutEspaceEnum.DESINSCRIPTION_VALIDEE_HATVP);
		this.statutMap.put("COMPLEMENTS_DEMANDE_DESINSCRIPTION_RECU", StatutEspaceEnum.COMPLEMENTS_DEMANDE_DESINSCRIPTION_RECU);
		this.statutMap.put("COMPLEMENTS_DEMANDE_DESINSCRIPTION", StatutEspaceEnum.COMPLEMENTS_DEMANDE_DESINSCRIPTION);
	}
	

	public List<ExerciceComptableBlacklistedDto> getexerciceBlacklisteDto(EspaceOrganisationEntity espEntity) {
		List<ExerciceComptableBlacklistedDto> listeExerciceDto = new ArrayList<ExerciceComptableBlacklistedDto>();
		//recuperer le dernier exercice blacklisté et l'exercice d'avant 
        List<ExerciceComptableEntity> listeExercicesEntity =  espEntity.getExercicesComptable();
        listeExercicesEntity.sort(Comparator.comparing(ExerciceComptableEntity::getDateDebut).reversed());
        boolean exBlacklisteAjoute = false;
        for (ExerciceComptableEntity ex : listeExercicesEntity ) {
        	if (exBlacklisteAjoute) {
        		listeExerciceDto.add(this.getExerciceComptableBlacklistedDto(ex));
        		break;
        	}
        	if (ex.isBlacklisted()) {
        		listeExerciceDto.add(this.getExerciceComptableBlacklistedDto(ex));
        		exBlacklisteAjoute = true;
        	}
        }
        return listeExerciceDto;
	}
    public ExerciceComptableBlacklistedDto getExerciceComptableBlacklistedDto(ExerciceComptableEntity exercice) { 
    	//recupere le DTO
    	ExerciceComptableBlacklistedDto dto = this.exerciceBlacklistedTransformer.modelToDto(exercice);
    	 //recupere la date de debut de blacklistage 
    	Optional<PublicationRelanceEntity> relance = exercice.getPublicationRelance().stream().filter(p->p.getNiveau().equals(PublicationRelanceNiveauEnum.NBL)).findAny();
    	//Si pas de relance par défaut à zéro
    	dto.setNbJoursBlacklistMA(0);
		dto.setNbJoursBlacklistFA(0);
		//Si jamais relance on met à jour setNbJoursBlacklistMA et/ou  setNbJoursBlacklistFA
    	if (relance.isPresent()) {
    		dto = parametrageJourBlacklistEnFonctiontRaisonBlacklistage(dto, exercice.getId(), relance.get());
//    		LocalDateTime debutBlacklistage = relance.get().getCreationDate();
//    		LocalDateTime finBlacklistage = LocalDateTime.now();
//    		PublicationExerciceEntity publicationExercice = publicationExerciceRepository.findFirstByExerciceComptableIdAndStatutNotOrderByIdDesc(exercice.getId(),StatutPublicationEnum.NON_PUBLIEE);
//    		PublicationActiviteEntity publicationActivite = publicationActiviteRepository.getFirstPublicationActivite(exercice.getId());
//    		// recupere le type de blacklistage
//    		PublicationRelanceTypeEnum type = relance.get().getType();
//    		dto.setRaisonBlacklistage(type.getLabel());
//    		switch (type) {
//    		case FA:    			
//    			if (publicationActivite != null) finBlacklistage = publicationActivite.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
//    			dto.setNbJoursBlacklistFA(nombreJourBlacklist(debutBlacklistage, finBlacklistage));
//        		break;
//    		case MA:
//    			if (publicationExercice != null) finBlacklistage = publicationExercice.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(); 
//    			dto.setNbJoursBlacklistMA(nombreJourBlacklist(debutBlacklistage, finBlacklistage));
//    			break;
//    		case FAMA:    			
//    			//finBlacklistage = publicationActivite == null || publicationExercice ==null ? LocalDateTime.now(): (publicationExercice.getCreationDate().after(publicationActivite.getCreationDate())? publicationExercice.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime() : publicationActivite.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
//    			if(publicationActivite != null && publicationExercice != null) {
//    				if(publicationExercice.getCreationDate().after(publicationActivite.getCreationDate())){
//	    				finBlacklistage = publicationExercice.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
//	    			}else {
//	    				finBlacklistage = publicationActivite.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
//	    			}
//    			}
//    			dto.setNbJoursBlacklistMA(nombreJourBlacklist(debutBlacklistage, finBlacklistage));
//    			dto.setNbJoursBlacklistFA(nombreJourBlacklist(debutBlacklistage, finBlacklistage));
//    			
//    			break;
//    		}
    	}

    	return dto;
    }

    @Override
    @Transactional
    public void ajouterContactOperationnel(MultipartFile file1, MultipartFile file2, MultipartFile file3,final Long espaceOrganisationId, final Long declarantId, final String message) {
        DeclarantEntity declarant = this.declarantRepository.findOne(declarantId);

            if (file1 !=null) PiecesUtils.checkConformitePiece(file1);
            if (file2 !=null) PiecesUtils.checkConformitePiece(file2);
            if (file3 !=null) PiecesUtils.checkConformitePiece(file3);
            // Transformation des pièces récupérées
            List<EspaceOrganisationPieceDto> pieces = new ArrayList<>();
            EspaceOrganisationPieceDto piece1 = PiecesUtils.mapToDto(file1);
            EspaceOrganisationPieceDto piece2 = PiecesUtils.mapToDto(file2);
            EspaceOrganisationPieceDto piece3 = PiecesUtils.mapToDto(file3);
            if (piece1 != null) {pieces.add(piece1);}
            if (piece2 != null) {pieces.add(piece2);}
            if (piece3 != null) {pieces.add(piece3);}

            EspaceOrganisationEntity espaceOrganisation = this.espaceOrganisationRepository.findOne(espaceOrganisationId);

            inscriptionEspaceService.devenirContactOperationnel(declarantId, espaceOrganisationId, pieces);

            this.espaceOrganisationBoMailer.sendConfirmationChangementContactOperationel(this.declarantTransformer.modelToDto(declarant), message, espaceOrganisation.getOrganisation().getDenomination());

    }

    @Override
    public List<EspaceOrganisationBlacklistedDto> getAllSpaceNotPaginated(){
        // returns all blacklisted spaces for download
        List<EspaceOrganisationEntity> listEntity = this.espaceOrganisationRepository.findByExerciceBlacklistedNotPaginated();
        List<EspaceOrganisationBlacklistedDto> listEspaces = this.espaceOrganisationBlacklistedTransformer.modelToDto(listEntity);
        for(int i=0; i<listEspaces.size();i++){
            listEspaces.get(i).setListeExercices(this.getexerciceBlacklisteDto(listEntity.get(i)));
        }
        return listEspaces;
    }

	
	public void setEspaceRejetMessage5(String espaceRejetMessage5) {
        this.espaceRejetMessage5 = espaceRejetMessage5;
    }
	
	@Override
	public EspaceOrganisationDto getEspaceOrganisationDtoById (final Long espaceOrganisationId) {
		final EspaceOrganisationEntity entity = Optional.ofNullable(this.espaceOrganisationRepository.findOne(espaceOrganisationId))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));

		return this.espaceOrganisationTransformer.modelToDto(entity);
	}
	
	/**
	 * 
	 * @param debutBlacklistage
	 * @param finBlacklistage
	 * @return le nombre de jours entre la date de début de blacklistage et la date de fin de blacklistahe
	 */
	public Integer nombreJourBlacklist(LocalDateTime debutBlacklistage, LocalDateTime finBlacklistage ) {

		return (int)ChronoUnit.DAYS.between(debutBlacklistage.toLocalDate(), finBlacklistage.toLocalDate());
	}
	
	/**
	 * Mise à jour de setNbJoursBlacklistMA et/ou setNbJoursBlacklistFA (par défaut à zéro) quand l'exercice compatble blacklisté 
	 * a une relance de type NBL
	 * @param exerciceComptableBlacklistedDto
	 * @param exerciceId
	 * @param relance
	 * @return
	 */
	public ExerciceComptableBlacklistedDto parametrageJourBlacklistEnFonctiontRaisonBlacklistage(ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto, Long exerciceId, PublicationRelanceEntity relance) {

		LocalDateTime debutBlacklistage = relance.getCreationDate();
		LocalDateTime finBlacklistage = LocalDateTime.now();
		PublicationExerciceEntity publicationExercice = publicationExerciceRepository.findFirstByExerciceComptableIdAndStatutNotOrderByIdDesc(exerciceId,StatutPublicationEnum.NON_PUBLIEE);
		PublicationActiviteEntity publicationActivite = publicationActiviteRepository.getFirstPublicationActivite(exerciceId);
		// recupere le type de blacklistage
		PublicationRelanceTypeEnum type = relance.getType();
		exerciceComptableBlacklistedDto.setRaisonBlacklistage(type.getLabel());
		switch (type) {
		case FA:    			
			if (publicationActivite != null) finBlacklistage = publicationActivite.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			exerciceComptableBlacklistedDto.setNbJoursBlacklistFA(nombreJourBlacklist(debutBlacklistage, finBlacklistage));
    		break;
		case MA:
			if (publicationExercice != null) finBlacklistage = publicationExercice.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(); 
			exerciceComptableBlacklistedDto.setNbJoursBlacklistMA(nombreJourBlacklist(debutBlacklistage, finBlacklistage));
			break;
		case FAMA:    			
			//finBlacklistage = publicationActivite == null || publicationExercice ==null ? LocalDateTime.now(): (publicationExercice.getCreationDate().after(publicationActivite.getCreationDate())? publicationExercice.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime() : publicationActivite.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
			if(publicationActivite != null && publicationExercice != null) {
				if(publicationExercice.getCreationDate().after(publicationActivite.getCreationDate())){
    				finBlacklistage = publicationExercice.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    			}else {
    				finBlacklistage = publicationActivite.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    			}
			}
			exerciceComptableBlacklistedDto.setNbJoursBlacklistMA(nombreJourBlacklist(debutBlacklistage, finBlacklistage));
			exerciceComptableBlacklistedDto.setNbJoursBlacklistFA(nombreJourBlacklist(debutBlacklistage, finBlacklistage));			
			break;
		}		
		return exerciceComptableBlacklistedDto;		
	}
	
	@Override
    public List<AssoClientDto> getEspaceOrganisationAffiliations(final Long espaceId){
	    // 1 On récupère les informations affiliation puis on transforme en DTO
	
        return this.assoAppartenanceRepository.findByEspaceOrganisationId(espaceId).stream().map(associationProTransformer::modelToDto)
            .sorted(Comparator.comparing(AbstractCommonDto::getId)).collect(Collectors.toList());
    }

	
	@Transactional
	@Override
	public void forcePublicationEspace(Long espaceOrganisationId) {
		Optional.ofNullable(this.espaceOrganisationRepository.findOne(espaceOrganisationId)).ifPresent(e -> {
			e.setNewPublication(Boolean.TRUE);
			this.espaceOrganisationRepository.save(e);
		});
		
	}

}