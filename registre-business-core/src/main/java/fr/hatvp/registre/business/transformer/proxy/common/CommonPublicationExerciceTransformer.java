/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.common;

import fr.hatvp.registre.commons.dto.publication.activite.PublicationExerciceDto;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

/**
 * Proxy du transformeur entre {@link fr.hatvp.registre.persistence.entity.publication.activite.common.PublicationEntity} et {@link fr.hatvp.registre.commons.dto.publication.activite.PublicationExerciceDto}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface CommonPublicationExerciceTransformer<E extends PublicationExerciceDto>
        extends CommonTransformer<E, PublicationEntity> {

}