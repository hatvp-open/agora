/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication;

import fr.hatvp.registre.business.transformer.impl.common.CommonClientTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.publication.PubClientTransformer;
import fr.hatvp.registre.persistence.entity.publication.PubClientEntity;
import org.springframework.stereotype.Component;

/**
 * Classe de transformation de la classe {@link fr.hatvp.registre.persistence.entity.publication.PubClientEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class PubClientTransformerImpl
		extends CommonClientTransformerImpl<PubClientEntity>
		implements PubClientTransformer {
	
	/** Init super class. */
	public PubClientTransformerImpl() {
		super.entityClass = PubClientEntity.class;
	}
}