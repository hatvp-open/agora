/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.nomenclature;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.nomenclature.CommonNomenclatureParentTransformer;
import fr.hatvp.registre.business.transformer.proxy.nomenclature.CommonNomenclatureTransformer;
import fr.hatvp.registre.commons.dto.nomenclature.CommonNomenclatureDto;
import fr.hatvp.registre.commons.dto.nomenclature.CommonNomenclatureParentDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.persistence.entity.nomenclature.CommonNomenclatureParentEntity;

public abstract class AbstractCommonNomenclatureParentTransformer<K extends CommonNomenclatureDto, T extends CommonNomenclatureParentDto<K>, E extends CommonNomenclatureParentEntity>
		extends AbstractCommonTansformer<T, E> implements CommonNomenclatureParentTransformer<K, T, E> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCommonNomenclatureParentTransformer.class);
	
	@Autowired
	private CommonNomenclatureTransformer<K, E> commonNomenclatureTransformer;

	protected Class<T> dtoClass;

	@Override
	public List<T> modelToDto(List<E> entities) {

		List<T> dtos = new ArrayList<>();

		Map<Long, List<E>> rand = entities.stream().collect(Collectors.groupingBy(CommonNomenclatureParentEntity::getIdCategorie, Collectors.toList()));

		rand.forEach((s, t) -> {
			T dto;
			try {
				dto = dtoClass.newInstance();

			} catch (InstantiationException | IllegalAccessException e) {
				LOGGER.error("Erreur d'instanciation dans le transformeur des commentaires : {}", e);
				throw new BusinessGlobalException("Erreur d'instanciation dans le transformeur des commentaires");
			}

			dto.setId(s);
			dto.setCategorie(t.stream().findAny().get().getCategorie());

			List<K> test = commonNomenclatureTransformer.modelToDto(t);

			dto.setNomenclature(test);

			dtos.add(dto);
		});

		return dtos;
	}

}
