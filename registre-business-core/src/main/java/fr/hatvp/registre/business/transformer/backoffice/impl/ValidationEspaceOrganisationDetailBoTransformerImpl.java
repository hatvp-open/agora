/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.impl;

import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.ValidationEspaceOrganisationDetailBoTransformer;
import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceDetailBoDto;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;

@Component
public class ValidationEspaceOrganisationDetailBoTransformerImpl extends AbstractCommonTansformer<ValidationEspaceDetailBoDto, EspaceOrganisationEntity>
		implements ValidationEspaceOrganisationDetailBoTransformer {

	@Override
	public ValidationEspaceDetailBoDto modelToDto(EspaceOrganisationEntity entity) {
		final ValidationEspaceDetailBoDto dto = new ValidationEspaceDetailBoDto();

		dto.setId(entity.getId());
		dto.setValidationInscriptionId(entity.getCreateurEspaceOrganisation().getInscriptionEspaces().stream()
				.filter(i -> i.getEspaceOrganisation().getId().longValue() == entity.getId().longValue()).findAny().orElseGet(InscriptionEspaceEntity::new).getId());
		dto.setDenomination(entity.getOrganisation().getDenomination());
		dto.setNomUsage(entity.getNomUsage());
		dto.setNomUsageHatvp(entity.getNomUsageHatvp());
		dto.setSigleHatvp(entity.getSigleHatvp());
		dto.setAncienNomHatvp(entity.getAncienNomHatvp());
		dto.setCreationDate(entity.getCreationDate());
		dto.setCreateurNomComplet(entity.getCreateurEspaceOrganisation().getCivility().getLibelleLong() + " " + entity.getCreateurEspaceOrganisation().getNom() + " "
				+ entity.getCreateurEspaceOrganisation().getPrenom());
		dto.setCreateurEmail(entity.getCreateurEspaceOrganisation().getEmail());
		dto.setNationalId(this.determineIdentifiantNational(entity.getOrganisation()));
		dto.setTypeIdentifiantNational(entity.getOrganisation().getOriginNationalId());
		dto.setAdresse(entity.getAdresse());
		dto.setCodePostal(entity.getCodePostal());
		dto.setVille(entity.getVille());
		dto.setPays(entity.getPays());
		dto.setDirigeants(entity.getDirigeants().stream().map(dir -> {
			return dir.getNom() + " " + dir.getPrenom();
		}).collect(Collectors.toList()));
		dto.setRepresentantLegal(entity.isRepresentantLegal());
		dto.setStatut(entity.getStatut());
		dto.setCreateurId(entity.getCreateurEspaceOrganisation().getId());
		dto.setOrganisationId(entity.getOrganisation().getId());
		
		dto.setNonPublierMonAdresseEmail(entity.getNonPublierMonAdresseEmail());
		dto.setNonPublierMonAdressePhysique(entity.getNonPublierMonAdressePhysique());
		dto.setNonPublierMonTelephoneDeContact(entity.getNonPublierMonTelephoneDeContact());
		dto.setEmailDeContact(entity.getEmailDeContact());
		dto.setTelephoneDeContact(entity.getTelephoneDeContact());
		dto.setLienPageFacebook(entity.getLienPageFacebook());
		dto.setLienPageLinkedin(entity.getLienPageLinkedin());
		dto.setLienPageTwitter(entity.getLienPageTwitter());
		dto.setLienSiteWeb(entity.getLienSiteWeb());
		dto.setFinExerciceFiscal(entity.getFinExerciceFiscal());
		dto.setNonExerciceComptable(entity.getNonExerciceComptable());

		return dto;
	}

}
