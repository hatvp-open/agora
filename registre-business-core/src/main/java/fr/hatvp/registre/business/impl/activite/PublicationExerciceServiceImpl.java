/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.activite;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.PublicationActiviteService;
import fr.hatvp.registre.business.PublicationExerciceService;
import fr.hatvp.registre.business.email.PublicationMailer;
import fr.hatvp.registre.business.email.SurveillanceMailer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationBucketExerciceTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationExerciceTransformer;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketActiviteDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketExerciceDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationExerciceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity;
import fr.hatvp.registre.persistence.repository.activite.ExerciceComptableRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationExerciceRepository;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;

/**
 * Classe d'implémentation du service des publications d'exercice.
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional
@Service
public class PublicationExerciceServiceImpl implements PublicationExerciceService {

	/** Repository des publications. */
	@Autowired
	private PublicationExerciceRepository publicationExerciceRepository;

	@Autowired
	private PublicationBucketExerciceTransformer publicationBucketTransformer;

	@Autowired
	private PublicationExerciceTransformer publicationExerciceTransformer;

	@Autowired
	private PublicationActiviteService publicationActiviteService;

	@Autowired
	private ExerciceComptableRepository exerciceComptableRepository;

	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;

	@Autowired
	private PublicationRepository publicationRepository;

	@Autowired
	private DeclarantTransformer declarantTransformer;

	@Autowired
	private PublicationMailer publicationMailer;

	@Autowired
	private InscriptionEspaceRepository inscriptionEspaceRepository;

	@Autowired
	private DeclarantRepository declarantRepository;

	@Autowired
	private SurveillanceMailer surveillanceMailer;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationBucketExerciceDto getPublicationLivrableExercice(final Long exerciceId, boolean withHistorique) {
		// on construit un PublicationBucketExerciceDto (qu'il y ait des publications ou pas)
		final ExerciceComptableEntity exercice = exerciceComptableRepository.findOne(exerciceId);
		PublicationBucketExerciceDto bucketDto = new PublicationBucketExerciceDto();
		PublicationExerciceDto pubExDto = new PublicationExerciceDto();
		pubExDto.setDateDebut(exercice.getDateDebut());
		pubExDto.setDateFin(exercice.getDateFin());
		pubExDto.setBlacklisted(exercice.isBlacklisted());
		pubExDto.setNoActivite(exercice.isNoActivite());
		pubExDto.setExerciceId(exerciceId);
		pubExDto.setCommentaire(exercice.getCommentaire());

		bucketDto.setPublicationCourante(pubExDto);

		// on recupere les publications avec statut publié
		final List<PublicationExerciceEntity> publicationsExercice = this.publicationExerciceRepository.findAllByExerciceComptableIdAndStatutNotOrderByIdDesc(exerciceId,
				StatutPublicationEnum.DEPUBLIEE);

		// Il est possible que cette liste soit vide si l'exercice n'a fait aucune publication
		if (!publicationsExercice.isEmpty()) {

			PublicationBucketExerciceDto dto = this.publicationBucketTransformer.listPublicationToPublicationBucketExercice(publicationsExercice,withHistorique);

			List<PublicationBucketActiviteDto> activites = new ArrayList<>();

			for (ActiviteRepresentationInteretEntity activite : publicationsExercice.get(0).getExerciceComptable().getActivitesRepresentationInteret()) {
				if (!StatutPublicationEnum.NON_PUBLIEE.equals(activite.getStatut()) 
						&& !StatutPublicationEnum.DEPUBLIEE.equals(activite.getStatut())
						&& !StatutPublicationEnum.SUPPRIMEE.equals(activite.getStatut())) {
					PublicationBucketActiviteDto pubActivite = this.publicationActiviteService.getPublicationLivrableActivite(activite.getId(), withHistorique);
					if (pubActivite != null) {
						activites.add(pubActivite);
					}
				}
			}

			dto.getPublicationCourante().setActivites(activites);

			return dto;
		} else {
			// si l'exercice n'a aucune publication, on vérifie si des activités ont été publiées pour cet exercice

			List<PublicationBucketActiviteDto> activites = new ArrayList<>();

			for (ActiviteRepresentationInteretEntity activite : exercice.getActivitesRepresentationInteret()) {
				if (!StatutPublicationEnum.NON_PUBLIEE.equals(activite.getStatut()) && !StatutPublicationEnum.DEPUBLIEE.equals(activite.getStatut())) {
					PublicationBucketActiviteDto pubActivite = this.publicationActiviteService.getPublicationLivrableActivite(activite.getId(),withHistorique);
					if (pubActivite != null) {
						activites.add(pubActivite);
					}
				}
			}
			// si des activités ont été publiées on les insére dans une publiction d'exercice avec les dates de l'exercice
			if (!activites.isEmpty()) {
				pubExDto.setActivites(activites);
			}

			return bucketDto;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void savePublicationExercice(final Long connectedUser, final Long currentEspaceOrganisationId, final Long exerciceId) {

		// Récupération du DTO à publier enregistré
		ExerciceComptableEntity exercice = exerciceComptableRepository.findOne(exerciceId);

		// Vérification que la publication de l'exercice correspond à l'espace pour lequel on est connecté
		if (exercice.getEspaceOrganisation().getId().longValue() != currentEspaceOrganisationId.longValue()) {
			throw new BusinessGlobalException(ErrorMessageEnum.CONTENU_PUBLICATION_INTROUVABLE.getMessage(), 410);
		}
		// verification que l'EC a bien déja fait une publication de son espace
		if (!this.publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(currentEspaceOrganisationId, StatutPublicationEnum.DEPUBLIEE).isPresent()) {
			throw new BusinessGlobalException(ErrorMessageEnum.PUBLICATION_EXERCICE_NO_PUBLICATION.getMessage(), 410);
		}

		// Conversion des données de l'exercice récupéré en données de publication
		final PublicationExerciceDto publicationDto = publicationExerciceTransformer.exerciceToPublication(exercice);

		// Transformation en entity
		PublicationExerciceEntity publicationEntity = this.publicationExerciceTransformer.dtoToModel(publicationDto);
		publicationEntity.setExerciceComptable(exercice);
		publicationEntity.setVersionExercice(exercice.getVersion());

		// Enregistrement de l'identitée du publicateur
		final DeclarantEntity publicateur = this.declarantRepository.findOne(connectedUser);
		publicationEntity.setPublicateur(publicateur);

		// Obtenir la dernière publication de cet exercice
		final PublicationExerciceEntity alreadyPublished = this.publicationExerciceRepository.findFirstByExerciceComptableIdOrderByCreationDateDesc(exerciceId);

		// Mise Ã  jour du flag des publications sur l'espace organisation
		this.updateFlagDePublicationTrue(exercice, publicationEntity);

		// Mise Ã  jour du flag de blacklistage
		exercice.setBlacklisted(false);
		exercice.setAfficheBlacklist(false);
		// Enregistrer la publication
		final PublicationExerciceEntity savedPublication = this.publicationExerciceRepository.save(publicationEntity);

		this.exerciceComptableRepository.save(exercice);
		// Mise à jour du flag de l'espace pour déclancher une publication de celui ci
		final EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository.findOne(currentEspaceOrganisationId);
		espaceOrganisationEntity.setNewPublication(Boolean.TRUE);
		this.espaceOrganisationRepository.save(espaceOrganisationEntity);

		// Envoie de mail à tout les publicateurs de l'espace.
		envoyerUnEmailAuxPublicateursDeLespaceOrganisation(savedPublication, currentEspaceOrganisationId, Optional.ofNullable(alreadyPublished).isPresent());
		
		// Envoie mail aux RI ayant mise sous surveillance l'organisation
        SurveillanceEntity surveillance = espaceOrganisationEntity.getOrganisation().getSurveillanceEntity();
		
		if(surveillance != null) {
			if(surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.DECLARATION_NULLE) && exercice.isNoActivite()) {
				this.surveillanceMailer.sendSurveillanceMail(espaceOrganisationEntity.getOrganisation(), espaceOrganisationEntity, SurveillanceOrganisationEnum.DECLARATION_NULLE,"");
			}else if(surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.PUBLICATION_MOYEN)) {
				this.surveillanceMailer.sendSurveillanceMail(espaceOrganisationEntity.getOrganisation(), espaceOrganisationEntity, SurveillanceOrganisationEnum.PUBLICATION_MOYEN,exercice.getDateDebut().toString());
			}			
		}
	}

	void updateFlagDePublicationTrue(final ExerciceComptableEntity exercice, PublicationExerciceEntity savedPublication) {

		if (StatutPublicationEnum.MAJ_NON_PUBLIEE.equals(exercice.getStatut())
				|| StatutPublicationEnum.PUBLIEE.equals(exercice.getStatut())
						|| StatutPublicationEnum.MAJ_PUBLIEE.equals(exercice.getStatut())) {
			exercice.setStatut(StatutPublicationEnum.MAJ_PUBLIEE);
			savedPublication.setStatut(StatutPublicationEnum.MAJ_PUBLIEE);
		}else{
            exercice.setStatut(StatutPublicationEnum.PUBLIEE);
            savedPublication.setStatut(StatutPublicationEnum.PUBLIEE);
        }

		exercice.setDataToPublish(Boolean.FALSE);

	}
	/**
	 * Notifier tous les publicateurs et adminis de l'espace organisation du processus de publication.
	 *
	 * @param publication
	 *            DTO de la publication.
	 * @param espaceOrganisationId
	 *            Id de l'espace organisation
	 * @param alreadyPublished
	 */
	private void envoyerUnEmailAuxPublicateursDeLespaceOrganisation(final PublicationExerciceEntity publication, final Long espaceOrganisationId, boolean alreadyPublished) {
		// Lister les destinataires (publicateurs et administrateurs actifs de l'espace).
		List<DeclarantDto> listeDesPublicateursEtAdmins = this.inscriptionEspaceRepository.findAllByActifTrueAndDateSuppressionIsNullAndEspaceOrganisationId(espaceOrganisationId).stream()
				.filter(i -> (i.getRolesFront().contains(RoleEnumFrontOffice.PUBLICATEUR) || i.getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR)))
				.map(i -> this.declarantTransformer.modelToDto(i.getDeclarant())).collect(Collectors.toList());

		this.publicationMailer.envoyerNotificationDePublicationExercice(publication, listeDesPublicateursEtAdmins, alreadyPublished);
	}

	@Override
	public void depublierExercice(Long exerciceId, Long userId) {
		Optional.ofNullable(this.exerciceComptableRepository.findOne(exerciceId)).ifPresent(e -> {
			e.setPublicationEnabled(false);
			e.getEspaceOrganisation().setNewPublication(Boolean.TRUE);
			this.exerciceComptableRepository.save(e);
			this.espaceOrganisationRepository.save(e.getEspaceOrganisation());
		});

	}

	@Override
	public void republierExercice(Long exerciceId, Long userId) {
		Optional.ofNullable(this.exerciceComptableRepository.findOne(exerciceId)).ifPresent(e -> {
			e.setPublicationEnabled(true);
			e.getEspaceOrganisation().setNewPublication(Boolean.TRUE);
			this.exerciceComptableRepository.save(e);
			this.espaceOrganisationRepository.save(e.getEspaceOrganisation());
		});

	}
}
