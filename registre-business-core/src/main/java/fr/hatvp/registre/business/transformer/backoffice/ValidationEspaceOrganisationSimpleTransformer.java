/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceBoSimpleDto;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

public interface ValidationEspaceOrganisationSimpleTransformer extends CommonTransformer<ValidationEspaceBoSimpleDto, EspaceOrganisationEntity> {

}
