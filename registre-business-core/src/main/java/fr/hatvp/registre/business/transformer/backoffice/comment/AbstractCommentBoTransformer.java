/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

/**
 * Interface de transformation de la classe
 * {@link fr.hatvp.registre.persistence.entity.commentaire.AbstractCommentEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
public interface AbstractCommentBoTransformer<T extends AbstractCommonDto, E extends AbstractCommonEntity>
    extends CommonTransformer<T, E>
{

    /**
     * @param entity entité à transformer.
     * @return le dto commentaire de l'entité.
     */
    CommentBoDto commentModelToDto(final E entity);
}
