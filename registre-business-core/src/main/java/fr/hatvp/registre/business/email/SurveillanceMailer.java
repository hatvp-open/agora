/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.email;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;

@Component
public class SurveillanceMailer extends ApplicationMailer {

	/** Objet du mail. */
	@Value("${mail.surveillance.objet}")
	private String object;

	/** Template du mail */
	private static final String SURVEILLANCE_TEMPLATE = "email/surveillance/surveillance";

	protected SurveillanceMailer(JavaMailSender sender, TemplateEngine templateEngine) {
		super(sender, templateEngine);
	}

	public void sendSurveillanceMail(OrganisationEntity organisation, EspaceOrganisationEntity espace, SurveillanceOrganisationEnum type, Object data) {
		final Context ctx = new Context(Locale.FRANCE);
		ctx.setVariable("actePrimaire", type.getCategorie());
		ctx.setVariable("acteSecondaire", type.getLabel());
		ctx.setVariable("denomination", organisation.getDenomination());
		ctx.setVariable("idNational", organisation.computeIdNational());
		ctx.setVariable("motif", organisation.getSurveillanceEntity().getMotif());

		if(type.equals(SurveillanceOrganisationEnum.AJOUT_CLIENT)){
		    // get newly added client's denomination
            // data = client denomination
            ctx.setVariable("denominationClient", data);
        }else if(type.equals(SurveillanceOrganisationEnum.AJOUTE_COMME_CLIENT)){
		    ctx.setVariable("denominationCommeTiers",data);
        }else if(type.equals(SurveillanceOrganisationEnum.CREATION_FICHE) || type.equals(SurveillanceOrganisationEnum.PUBLICATION_FICHE)){
            ctx.setVariable("activite", data);
        }


		if (espace != null) {
			ctx.setVariable("nomUsage", espace.getNomUsage() != null ? espace.getNomUsage() : espace.getNomUsageHatvp() != null ? espace.getNomUsageHatvp() : null);
		}
		ctx.setVariable("date", LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")).concat(" à ").concat(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm"))));
		ctx.setVariable("imageResourceName", this.imageResourceName);
        ArrayList<String> listSurveillant = new ArrayList<>();
		organisation.getSurveillanceEntity().getSurveillants().forEach(surveillant -> listSurveillant.add(surveillant.getPrenom()));
		
		ctx.setVariable("listSurveillant",listSurveillant);
		if (!organisation.getSurveillanceEntity().getSurveillantsGroupes().isEmpty()) {
			
			listSurveillant.add(", et le(s) groupe(s) :");
		}
		organisation.getSurveillanceEntity().getSurveillantsGroupes().forEach(groupe ->  listSurveillant.add(groupe.getLibelle()));
		organisation.getSurveillanceEntity().getSurveillants().forEach(surveillant -> {
			ctx.setVariable("civilite", surveillant.getPrenom());
			final String content = this.templateEngine.process(SURVEILLANCE_TEMPLATE, ctx);
			super.prepareAndSendEmailWhithLogo(surveillant.getEmail(), this.object, content);
		});
		organisation.getSurveillanceEntity().getSurveillantsGroupes().forEach(groupe -> {
			groupe.getUtilisateurBoEntity().forEach(surveillant -> {
				ctx.setVariable("civilite", surveillant.getPrenom());
				final String content = this.templateEngine.process(SURVEILLANCE_TEMPLATE, ctx);
				super.prepareAndSendEmailWhithLogo(surveillant.getEmail(), this.object, content);
			});
		});
		

	}

}
