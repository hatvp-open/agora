/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication.activite.common;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.common.PublicationMetaExerciceActiviteTransformer;
import fr.hatvp.registre.commons.dto.publication.activite.common.PublicationMetaDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.publication.activite.common.PublicationEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Classe du transformeur entre {@link fr.hatvp.registre.persistence.entity.publication.activite.common.PublicationEntity} et {@link fr.hatvp.registre.commons.dto.publication.activite.common.PublicationMetaDto}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional(readOnly = true)
public class PublicationMetaExerciceActiviteTransformerImpl
    extends AbstractCommonTansformer<PublicationMetaDto, PublicationEntity>
    implements PublicationMetaExerciceActiviteTransformer
{
    /**
     * {@inheritDoc}
     */
    @Override
    public PublicationMetaDto modelToDto(final PublicationEntity model)
    {
        final PublicationMetaDto meta = new PublicationMetaDto();
        RegistreBeanUtils.copyProperties(meta, model);
        meta.setStatut(model.getStatut());
        meta.setPublicationDate(model.getCreationDate());

        if (model.getPublicateur() != null)
            meta.setPublicateurNom(
                    model.getPublicateur().getNom() + " " + model.getPublicateur().getPrenom());

        if (model.getDepublicateur() != null) {
            meta.setDepublicationDate(model.getDepublicationDate());
            meta.setDepublicateurNom(
                    model.getDepublicateur().getNom() + " " + model.getDepublicateur().getPrenom());
        }

        if (model.getRepublicateur() != null) {
            meta.setRepublicationDate(model.getRepublicationDate());
            meta.setRepublicateurNom(
                    model.getRepublicateur().getNom() + " " + model.getRepublicateur().getPrenom());
        }

        return meta;
    }
}
