
/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.DemandeOrganisationDto;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;

/**
 * Interface de transformation de la classe {@link DemandeOrganisationEntity}
 * 
 * @version $Revision$ $Date${0xD}
 */
public interface DemandeOrganisationTransformer
    extends CommonTransformer<DemandeOrganisationDto, DemandeOrganisationEntity>
{

}
