/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.activite;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ExerciceComptableBlacklistedTransformer;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableBlacklistedDto;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;

@Component
public class ExerciceComptableBlacklistedTransformerImpl extends AbstractCommonTansformer<ExerciceComptableBlacklistedDto, ExerciceComptableEntity>
		implements ExerciceComptableBlacklistedTransformer {

	@Override
	public List<ExerciceComptableBlacklistedDto> modelToDto(List<ExerciceComptableEntity> entities) {
		List<ExerciceComptableBlacklistedDto> dtos = new ArrayList<>();

		entities.forEach(entity -> {
			dtos.add(modelToDto(entity));
		});

		return dtos;
	}

	@Override
	public ExerciceComptableBlacklistedDto modelToDto(ExerciceComptableEntity entity) {
		ExerciceComptableBlacklistedDto dto = new ExerciceComptableBlacklistedDto();

		dto.setId(entity.getId());
		dto.setDateDebut(entity.getDateDebut());
		dto.setDateFin(entity.getDateFin());
		dto.setChiffreAffaire(entity.getChiffreAffaire() == null ? null : entity.getChiffreAffaire().getId());
		dto.setHasNotChiffreAffaire(entity.getHasNotChiffreAffaire());
		dto.setMontantDepense(entity.getMontantDepense() == null ? null : entity.getMontantDepense().getId());
		dto.setNombreSalaries(entity.getNombreSalaries());
		dto.setEspaceOrganisationId(entity.getEspaceOrganisation().getId());
		dto.setDataToPublish(entity.getDataToPublish());
	

		return dto;
	}

}
