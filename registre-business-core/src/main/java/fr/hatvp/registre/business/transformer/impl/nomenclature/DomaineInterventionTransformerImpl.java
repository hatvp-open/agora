/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.nomenclature;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.proxy.nomenclature.DomaineInterventionTransformer;
import fr.hatvp.registre.commons.dto.nomenclature.DomaineInterventionDto;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;

@Component
public class DomaineInterventionTransformerImpl extends AbstractCommonNomenclatureTransformer<DomaineInterventionDto, DomaineInterventionEntity>
		implements DomaineInterventionTransformer {

	/** Init super DTO. */
	public DomaineInterventionTransformerImpl() {
		super.dtoClass = DomaineInterventionDto.class;
	}

}
