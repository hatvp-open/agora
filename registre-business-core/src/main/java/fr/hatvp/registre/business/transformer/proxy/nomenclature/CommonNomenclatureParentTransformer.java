/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.nomenclature;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.nomenclature.CommonNomenclatureParentDto;
import fr.hatvp.registre.commons.dto.nomenclature.CommonNomenclatureDto;
import fr.hatvp.registre.persistence.entity.nomenclature.CommonNomenclatureEntity;

public interface CommonNomenclatureParentTransformer<K extends CommonNomenclatureDto, T extends CommonNomenclatureParentDto<K>, E extends CommonNomenclatureEntity>
		extends CommonTransformer<T, E> {

}
