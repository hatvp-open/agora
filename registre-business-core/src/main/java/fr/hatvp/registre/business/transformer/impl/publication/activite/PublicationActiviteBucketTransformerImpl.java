/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication.activite;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationActiviteTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationBucketActiviteTransformer;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActiviteDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketActiviteDto;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;

/**
 * Classe du transformeur entre {@link PublicationActiviteEntity} et {@link PublicationBucketActiviteDto}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class PublicationActiviteBucketTransformerImpl
        extends AbstractCommonTansformer<PublicationBucketActiviteDto, PublicationActiviteEntity>
        implements PublicationBucketActiviteTransformer
{

    /** Transformeur des publications. */
    @Autowired
    private PublicationActiviteTransformer publicationActiviteTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    public PublicationBucketActiviteDto listPublicationToPublicationBucketActivite(final List<PublicationActiviteEntity> publications,boolean withHistorique ) {

        final PublicationBucketActiviteDto bucketDto = new PublicationBucketActiviteDto();

        final Optional<PublicationActiviteEntity> optional = Optional.ofNullable(publications.get(0));
        if (optional.isPresent()) {
            PublicationActiviteDto activite = this.publicationActiviteTransformer.modelToDto(publications.get(0));
            bucketDto.setPublicationCourante(activite);
        }
        
//        if (withHistorique) bucketDto.setHistorique(this.publicationActiviteTransformer.activitesHistoriqueToPublication(publications));

        return bucketDto;
    }
}
