/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.nomenclature;

import fr.hatvp.registre.commons.dto.nomenclature.DecisionConcerneeDto;
import fr.hatvp.registre.persistence.entity.nomenclature.DecisionConcerneeEntity;

public interface DecisionConcerneeTransformer extends CommonNomenclatureTransformer<DecisionConcerneeDto, DecisionConcerneeEntity> {

}
