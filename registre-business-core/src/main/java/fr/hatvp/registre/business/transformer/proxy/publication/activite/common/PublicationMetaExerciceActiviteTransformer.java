/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication.activite.common;
import fr.hatvp.registre.commons.dto.publication.activite.common.PublicationMetaDto;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.persistence.entity.publication.activite.common.PublicationEntity;

/**
 * Proxy du transformeur pour entre {@link fr.hatvp.registre.persistence.entity.publication.activite.common.PublicationEntity} et {@link fr.hatvp.registre.commons.dto.publication.activite.common.PublicationMetaDto}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationMetaExerciceActiviteTransformer
    extends CommonTransformer<PublicationMetaDto, PublicationEntity>
{

}

