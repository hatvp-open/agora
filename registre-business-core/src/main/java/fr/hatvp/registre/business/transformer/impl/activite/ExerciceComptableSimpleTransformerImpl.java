/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.activite;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ExerciceComptableSimpleTransformer;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableSimpleDto;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;

@Component
public class ExerciceComptableSimpleTransformerImpl extends AbstractCommonTansformer<ExerciceComptableSimpleDto, ExerciceComptableEntity>
		implements ExerciceComptableSimpleTransformer {

	@Override
	public List<ExerciceComptableSimpleDto> modelToDto(List<ExerciceComptableEntity> entities) {
		List<ExerciceComptableSimpleDto> dtos = new ArrayList<>();

		entities.forEach(entity -> {
			dtos.add(modelToDto(entity));
		});

		return dtos;
	}

	@Override
	public ExerciceComptableSimpleDto modelToDto(ExerciceComptableEntity entity) {
		ExerciceComptableSimpleDto dto = new ExerciceComptableSimpleDto();

		dto.setId(entity.getId());
		dto.setDateDebut(entity.getDateDebut());
		dto.setDateFin(entity.getDateFin());
		dto.setChiffreAffaire(entity.getChiffreAffaire() == null ? null : entity.getChiffreAffaire().getId());
		dto.setHasNotChiffreAffaire(entity.getHasNotChiffreAffaire());
		dto.setMontantDepense(entity.getMontantDepense() == null ? null : entity.getMontantDepense().getId());
		dto.setNombreSalaries(entity.getNombreSalaries());
		dto.setEspaceOrganisationId(entity.getEspaceOrganisation().getId());
		dto.setDataToPublish(entity.getDataToPublish());
		dto.setCommentaire(entity.getCommentaire());

		return dto;
	}

}
