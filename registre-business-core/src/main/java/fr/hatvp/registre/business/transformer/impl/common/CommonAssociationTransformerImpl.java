/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.common;

import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.proxy.common.CommonAssociationTransformer;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.common.CommonAssociationEntity;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;

/**
 * Classe de transformation commune aux associations;
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
abstract public class CommonAssociationTransformerImpl<E extends CommonAssociationEntity>
        extends AbstractCommonTansformer<AssoClientDto, E>
        implements CommonAssociationTransformer<E> {


    /**
     * Classe à définir dans les classe filles.
     */
    protected Class<E> entityClass;
    
    @Autowired
    private DesinscriptionRepository desinscriptionRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public AssoClientDto modelToDto(final E model) {

        final AssoClientDto dto = new AssoClientDto();
        RegistreBeanUtils.copyProperties(dto, model);

        dto.setDenomination(model.getOrganisation().getDenomination());

        // Définir l'identifiant national suivant son origine dans la table organisation.
        dto.setNationalId(super.determineIdentifiantNational(model.getOrganisation()));
        dto.setOriginNationalId(model.getOrganisation().getOriginNationalId());

        dto.setEspaceOrganisationId(model.getEspaceOrganisation().getId());
        dto.setOrganisationId(model.getOrganisation().getId());
        
        EspaceOrganisationEntity espaceClientAssoEntity = espaceOrganisationRepository.findByOrganisationId(model.getOrganisation().getId());
		
		if(espaceClientAssoEntity != null) {
			DesinscriptionEntity desinscriptionEntity = desinscriptionRepository.findByEspaceOrganisation(espaceClientAssoEntity);
			
			if(desinscriptionEntity != null) dto.setDateCessation(Date.from(desinscriptionEntity.getCessationDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));
			
		}

        return dto;

    }

    /** Repositoy des organisations. */
    @Autowired
    private OrganisationRepository organisationRepository;

    /** Repositoy des espaces organisations. */
    @Autowired
    private EspaceOrganisationRepository espaceOrganisationRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public E dtoToModel(final AssoClientDto dto) {
        final E model;
        try {
            model = this.entityClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new BusinessGlobalException("Erreur d'instanciation dans le transformeur des commentaires");
        }
        RegistreBeanUtils.copyProperties(model, dto);

        model.setEspaceOrganisation(this.espaceOrganisationRepository.findOne(dto.getEspaceOrganisationId()));
        model.setOrganisation(this.organisationRepository.findOne(dto.getOrganisationId()));
        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> dtoToModel(final List<AssoClientDto> dtos) {
        return dtos.stream().map(this::dtoToModel).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AssoClientDto> modelToDto(final List<E> models) {
        return models.stream().map(this::modelToDto).collect(Collectors.toList());
    }
}
