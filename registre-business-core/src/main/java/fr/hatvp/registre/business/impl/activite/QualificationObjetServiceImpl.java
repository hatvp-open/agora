/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.activite;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.QualificationObjetService;
import fr.hatvp.registre.business.email.EspaceOrganisationBoMailer;
import fr.hatvp.registre.business.transformer.proxy.activite.QualificationObjetTransformer;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.activite.QualificationObjetDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.utilities.LocalDateNowBuilder;
import fr.hatvp.registre.persistence.api.client.ApiHatvp;
import fr.hatvp.registre.persistence.entity.activite.QualificationObjetEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.repository.activite.QualificationObjetRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

/**
 * Service de gestion des qualifications d'objets
 *
 */
@Transactional
@Service
public class QualificationObjetServiceImpl
    implements QualificationObjetService
{

	private static final Logger LOGGER = LoggerFactory.getLogger(QualificationObjetServiceImpl.class);

	@Autowired
	private QualificationObjetRepository qualificationObjetRepository;

	@Autowired
	private QualificationObjetTransformer qualificationObjetTransformer;

	@Autowired
    private EspaceOrganisationRepository espaceOrganisationRepository;
    
	@Autowired
	private EspaceOrganisationBoMailer espaceOrganisationBoMailer;

	@Autowired
	LocalDateNowBuilder localDateNowBuilder;

    @Autowired
    ApiHatvp apiHatvp;

	/**
	 * {@inheritDoc}
	 */
    @Override
	public QualificationObjetDto createQualificationObjet(final QualificationObjetDto qualificationObjetDto, final Long currentEspaceOrganisationId) {

        final EspaceOrganisationEntity espaceOrganisationEntity = Optional.ofNullable(espaceOrganisationRepository.findOne(currentEspaceOrganisationId))
            .orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()));

        qualificationObjetDto.setIdentifiantNational(espaceOrganisationEntity.getOrganisation().computeIdNational());
        qualificationObjetDto.setDenomination(espaceOrganisationEntity.getOrganisation().getDenomination());
        qualificationObjetDto.setNomUsage(espaceOrganisationEntity.getNomUsage());
	    LocalDate dateEnregistrement = localDateNowBuilder.getLocalDateNow();
        qualificationObjetDto.setDateEnregistrement(dateEnregistrement);
		QualificationObjetEntity qualificationObjetEntity = qualificationObjetTransformer.dtoToModel(qualificationObjetDto);
		qualificationObjetEntity = qualificationObjetRepository.save(qualificationObjetEntity);
		return qualificationObjetTransformer.modelToDto(qualificationObjetEntity);
	}

	/**
	 * {@inheritDoc}
	 */
    @Override
	public QualificationObjetDto updateQualificationObjet(final Long qualifObjetId, final String idFiche, final Long currentEspaceOrganisationId) {
        QualificationObjetEntity qualificationObjetEntity = qualificationObjetRepository.findOne(qualifObjetId);
        qualificationObjetEntity.setIdFiche(idFiche);
        qualificationObjetEntity = qualificationObjetRepository.save(qualificationObjetEntity);
		return qualificationObjetTransformer.modelToDto(qualificationObjetEntity);
	}

    /**
     * {@inheritDoc}
     */
    @Override
    public QualificationObjetDto getLastQualiObjetByFicheId(final String idFiche) {
        QualificationObjetEntity qualificationObjetEntity = Optional.ofNullable(qualificationObjetRepository.findTopByIdFiche(idFiche))
            .orElse(new QualificationObjetEntity());
        return qualificationObjetTransformer.modelToDto(qualificationObjetEntity);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public PaginatedDto<QualificationObjetDto> getQualif(Integer pageNumber, Integer resultPerPage) {
        PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.DESC, "id");
        return getPaginatedResult(pageRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public PaginatedDto<QualificationObjetDto> getAllQualif() {
        PageRequest pageRequest = new PageRequest(0, Integer.MAX_VALUE, Sort.Direction.DESC, "id");
        return getPaginatedResult(pageRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public PaginatedDto<QualificationObjetDto> getQualifParRecherche(String type, String searchTerm, Integer pageNumber, Integer resultPerPage) {

        PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.DESC, "id");
        Page<QualificationObjetEntity> pageQualif;

        switch (type) {
            case "denomination":
                pageQualif = this.qualificationObjetRepository.findByDenominationIgnoreCaseContainingOrNomUsageIgnoreCaseContaining(searchTerm, searchTerm, pageRequest);
                break;
            case "idOrganisation":
                pageQualif = this.qualificationObjetRepository.findByIdentifiantNationalIgnoreCaseContaining(searchTerm, pageRequest);
                break;
            case "idFiche":
                pageQualif = this.qualificationObjetRepository.findByIdFicheIgnoreCaseContaining(searchTerm, pageRequest);
                break;
            case "objet":
                pageQualif = this.qualificationObjetRepository.findByObjetIgnoreCaseContaining(searchTerm, pageRequest);
                break;
            default:
                pageQualif = this.qualificationObjetRepository.findAll(pageRequest);
        }

        List<QualificationObjetDto> espaceDtos = pageQualif.getContent().stream().map(this.qualificationObjetTransformer::modelToDto).collect(Collectors.toList());

        return new PaginatedDto<>(espaceDtos, pageQualif.getTotalElements());
    }
    
	@Override
	public QualificationObjetDto qualifObjetActivite(Long currentEspaceOrganisationId, QualificationObjetDto qualificationObjetDto) {

        try {
        	qualificationObjetDto = apiHatvp.consumeAntoinetteApi(qualificationObjetDto.getObjet());
        }
        catch(Exception e) {
        	LOGGER.error("Qualification de l'objet : '"+qualificationObjetDto.getObjet()+"' impossible", e);
    		// envoi mail erreur
    		this.espaceOrganisationBoMailer.sendErreurAntoinetteEmail();
        }

        return this.createQualificationObjet(qualificationObjetDto, currentEspaceOrganisationId);
	}

	private PaginatedDto getPaginatedResult(PageRequest pageRequest) {
        final Page<QualificationObjetEntity> pageQualif = this.qualificationObjetRepository.findAll(pageRequest);

        List<QualificationObjetDto> qualificationObjetDtos = new ArrayList<>();
        for(QualificationObjetEntity qualificationObjetEntity : pageQualif){
            QualificationObjetDto qualificationObjetDto = this.qualificationObjetTransformer.modelToDto(qualificationObjetEntity);
            qualificationObjetDtos.add(qualificationObjetDto);
        }
        return new PaginatedDto<>(qualificationObjetDtos, pageQualif.getTotalElements());
    }

    @Override
    public void repriseQualif() {

        final List<QualificationObjetEntity> dtos = this.qualificationObjetRepository.findByCoefficiantConfianceIsNull();

        for(QualificationObjetEntity qualificationObjetEntity : dtos){
            QualificationObjetDto qualificationObjetDto = new QualificationObjetDto();

            try {
                qualificationObjetDto = apiHatvp.consumeAntoinetteApi(qualificationObjetEntity.getObjet());
                qualificationObjetEntity.setNote(qualificationObjetDto.isNote());
                qualificationObjetEntity.setCoefficiantConfiance(qualificationObjetDto.getCoefficiantConfiance());
                qualificationObjetEntity.setReprise(true);
                qualificationObjetRepository.save(qualificationObjetEntity);
                LOGGER.info("Re qualification de l'objet [ID (BD) {}] : {}", qualificationObjetEntity.getId(), qualificationObjetEntity.getObjet());
            }
            catch(Exception e) {
                LOGGER.error("Qualification de l'objet : '"+qualificationObjetDto.getObjet()+"' impossible", e);
            }
        }
    }
}
