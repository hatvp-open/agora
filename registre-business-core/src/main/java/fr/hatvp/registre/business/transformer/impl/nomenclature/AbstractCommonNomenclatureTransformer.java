/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.nomenclature.CommonNomenclatureTransformer;
import fr.hatvp.registre.commons.dto.nomenclature.CommonNomenclatureDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.persistence.entity.nomenclature.CommonNomenclatureEntity;

public class AbstractCommonNomenclatureTransformer<T extends CommonNomenclatureDto, E extends CommonNomenclatureEntity> extends AbstractCommonTansformer<T, E>
		implements CommonNomenclatureTransformer<T, E> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCommonNomenclatureTransformer.class);
	
	protected Class<T> dtoClass;

	@Override
	public List<T> modelToDto(List<E> entities) {
		List<T> dtos = new ArrayList<>();
		entities.forEach(entity -> {
			dtos.add(modelToDto(entity));
		});
		return dtos;
	}

	@Override
	public T modelToDto(E entity) {

		T dto;
		try {
			dto = dtoClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			LOGGER.error("Erreur d'instanciation dans le transformeur des commentaires : {}", e);
			throw new BusinessGlobalException("Erreur d'instanciation dans le transformeur des commentaires");
		}

		dto.setId(entity.getId());
		dto.setLibelle(entity.getLibelle());

		return dto;
	}

}
