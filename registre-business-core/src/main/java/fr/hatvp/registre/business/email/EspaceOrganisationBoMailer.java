/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.email;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import fr.hatvp.registre.commons.dto.DeclarantDto;

/**
 * Classe d'envoi de mails pour les espaces organisations.
 **/
@Component
public class EspaceOrganisationBoMailer
    extends ApplicationMailer
{

    @Autowired
    public EspaceOrganisationBoMailer(final JavaMailSender sender,
            final TemplateEngine templateEngine)
    {
        super(sender, templateEngine);
    }

    /** Objet du mail de validation des espaces organisations. */
    @Value("${mail.bo.changementMandant.changementCO.subject}")
    private String espaceChangementSubject;

    /** Objet du mail de validation des espaces organisations. */
    @Value("${mail.bo.espace.validation.subject}")
    private String espaceValidationSubject;

    /** Objet du mail de refus de validation des espaces organisations. */
    @Value("${mail.bo.espace.refus.subject}")
    private String espaceRejetSubject;
    
    /** Objet du mail de demande de complément pour espace collaboratif. */
    @Value("${mail.bo.espace.complement.subject}")
    private String espaceComplementSubject;
    
    /** Template de refus de création d'espace. */
    private static final String templateRefuse = "email/espace_refus";
    /** Template de validation de création d'espace. */
    private static final String templateValidate = "email/espace_valider";
    /** Template de demande de complément pour un espace. */
    private static final String templateComplement = "email/espace_complement";
    /** Template de confirmation de changement de contact opérationnel */
    private static final String templateChangement = "email/espace_changementCo";
    
    private static final String DENOMINATION = "[DENOMINATION]";

    @Value("${app.front.root}")
    private String frontAppLink;
    
    
    /**
     * Envoi de mail en cas d'erreur
     *
     * @param denomination raison sociale
     * @param to email destinataire
     */
    public void sendErreurAntoinetteEmail()
    {
        final String subject = "antoinette est morte";
        final String content = "antoinette ne réponds plus. Merci de la réanimer !!";
    }
    
    /**
     * Envoi de mail de confirmation de création de l'espace organisation
     *
     * @param denomination raison sociale
     * @param to email destinataire
     */
    public void sendValidationEspaceEmail(final DeclarantDto to, final String denomination)
    {
        final String subject;
        final String link = frontAppLink;
        subject = StringUtils.replace(this.espaceValidationSubject, DENOMINATION, denomination);
        this.prepareAndSendEspaceEmail(to, subject, denomination, templateValidate, link, null);
    }

    /**
     * Envoi de mail de refus de création de l'espace organisation
     *
     * @param to email destinataire
     */
    public void sendRefusEspaceEmail(final DeclarantDto to, final String message,
            final String denomination)
    {
        this.prepareAndSendEspaceEmail(to,
                StringUtils.replace(this.espaceRejetSubject, DENOMINATION, denomination),
                denomination, templateRefuse, null, message);
    }

    /**
     * Envoi de mail de demande de complément pour la création de l'espace organisation
     *
     * @param to email destinataire
     */
    public void sendDemandeComplementEspaceEmail(final DeclarantDto to, final String message,
            final String denomination)
    {
        this.prepareAndSendEspaceEmail(to,
                StringUtils.replace(this.espaceComplementSubject, DENOMINATION, denomination),
                denomination, templateComplement, null, message);
    }

    /**
     * Envoie de mail de confirmation de changement de contact opérationnel
     * @param to
     * @param message
     * @param denomination
     */
    public void sendConfirmationChangementContactOperationel(final DeclarantDto to, String message, final String denomination){

        message = "Vous êtes désormais le contact opérationnel de "+denomination;
        this.prepareAndSendEspaceEmail(to, StringUtils.replace(this.espaceChangementSubject, DENOMINATION,
            denomination),denomination, templateChangement, null, message);
    }
    
    /**
     * Préparer et envoyer le mail.
     */
    private void prepareAndSendEspaceEmail(final DeclarantDto to, final String object, final  String denomination,
                                           final String template, final String link, final String message)
    {

        final Context ctx = new Context(Locale.FRANCE);
        ctx.setVariable("civilite", to.getCivility().getLibelleLong());
        ctx.setVariable("denomination", denomination);
        ctx.setVariable("message", message);
        ctx.setVariable("link", link);
        ctx.setVariable("imageResourceName", this.imageResourceName);

        final String mailContent = this.templateEngine.process(template, ctx);
        super.prepareAndSendEmailWhithLogo(to.getEmail(), object, mailContent);
    }

}
