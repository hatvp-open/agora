/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication;

import fr.hatvp.registre.business.transformer.proxy.common.CommonPublicationTransformer;
import fr.hatvp.registre.commons.dto.publication.PublicationFrontDto;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

/**
 * Proxy du transformeur entre {@link PublicationEntity} et {@link PublicationFrontDto}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationFrontTransformer
        extends CommonPublicationTransformer<PublicationFrontDto> {
}
