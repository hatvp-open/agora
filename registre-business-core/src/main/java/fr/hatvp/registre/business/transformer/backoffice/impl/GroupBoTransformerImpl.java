/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.impl;

import fr.hatvp.registre.business.transformer.backoffice.GroupBoTransformer;
import fr.hatvp.registre.business.transformer.backoffice.UtilisateurBoTransformer;
import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.commons.dto.backoffice.GroupBoDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.backoffice.GroupBoEntity;
import fr.hatvp.registre.persistence.repository.backoffice.GroupBoRepository;
import fr.hatvp.registre.persistence.repository.backoffice.UtilisateurBoRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GroupBoTransformerImpl extends AbstractCommonTansformer<GroupBoDto, GroupBoEntity> implements GroupBoTransformer {

    private final UtilisateurBoTransformer utilisateurBoTransformer;
    private final UtilisateurBoRepository utilisateurBoRepository;
    private final GroupBoRepository groupBoRepository;

    public GroupBoTransformerImpl(UtilisateurBoTransformer utilisateurBoTransformer, UtilisateurBoRepository utilisateurBoRepository, GroupBoRepository groupBoRepository) {
        this.utilisateurBoTransformer = utilisateurBoTransformer;
        this.utilisateurBoRepository = utilisateurBoRepository;
        this.groupBoRepository = groupBoRepository;
    }

    /**
     *
     * @param model
     *            La {@link List} d'objets model
     * @return
     */
    @Override
    public GroupBoDto modelToDto(GroupBoEntity model) {

        final GroupBoDto dto = new GroupBoDto();
        RegistreBeanUtils.copyProperties(dto, model);
        dto.setUtilisateurBoDto(this.utilisateurBoTransformer.modelToDto(model.getUtilisateurBoEntity()));
        return dto;
    }

    @Override
    public GroupBoEntity dtoToModel(GroupBoDto dto){
        final GroupBoEntity model;
        if(this.groupBoRepository.findById(dto.getId()) == null){
            model = new GroupBoEntity();
        }else{
            model = this.groupBoRepository.findById(dto.getId());
        }
        RegistreBeanUtils.copyProperties(model,dto);
        model.setUtilisateurBoEntity(dto.getUtilisateurBoDto().stream().map(e-> this.utilisateurBoRepository.findById(e.getId())).collect(Collectors.toList()));
        return model;
    }
}
