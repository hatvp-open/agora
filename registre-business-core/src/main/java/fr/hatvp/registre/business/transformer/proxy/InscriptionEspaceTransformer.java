/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;

/**
 * Proxy pour le transformeur entre {@link InscriptionEspaceDto} et
 * {@link InscriptionEspaceEntity}.
 * 
 * @version $Revision$ $Date${0xD}
 */
public interface InscriptionEspaceTransformer
    extends CommonTransformer<InscriptionEspaceDto, InscriptionEspaceEntity>
{

}
