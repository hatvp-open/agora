/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentDemandeChangementMandatEntity;
import fr.hatvp.registre.persistence.entity.commentaire.CommentDemandeEntity;

/**
 * Interface de transformation de la classe
 * {@link CommentDemandeEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
public interface CommentDemandeChangementMandantBoTransformer
    extends AbstractCommentBoTransformer<CommentBoDto, CommentDemandeChangementMandatEntity>
{
}
