/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.OrganisationService;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.OrganisationTransformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.RnaReferencielTransformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.SireneApiTransformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.SireneReferencielTransformer;
import fr.hatvp.registre.business.utils.SiretSirenRnaHatvpValidation;
import fr.hatvp.registre.commons.dto.AssoClientBatchDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.TypeOrganisationDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;
import fr.hatvp.registre.commons.utils.RegistreRandomUtils;
import fr.hatvp.registre.persistence.api.client.ApiInsee;
import fr.hatvp.registre.persistence.apiSirene.Etablissement;
import fr.hatvp.registre.persistence.apiSirene.ListeEtablissements;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielRnaEntity;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielSireneEntity;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
import fr.hatvp.registre.persistence.repository.referenciel.ReferencielRnaRepository;
import fr.hatvp.registre.persistence.repository.referenciel.ReferencielSirenRepository;


/**
 * Implémentation du service Organisation {@link OrganisationService}
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
@Transactional(readOnly = true)
public class OrganisationServiceImpl extends AbstractCommonService<OrganisationDto, OrganisationEntity> implements OrganisationService {

	/** Repository pour l'entité Organisation. */
	@Autowired
	private OrganisationRepository organisationRepository;

	/** Transformer de l'Organisation. */
	@Autowired
	private OrganisationTransformer organisationTransformer;

	/** Repository pour la table Référentiel Sirene. */
	@Autowired
	private ReferencielSirenRepository referencielSirenRepository;

	/** Transformer des sirenes. */
	@Autowired
	private SireneReferencielTransformer sireneReferencielTransformer;

	/** Transformer des sirenes via l'api */
	@Autowired
    private SireneApiTransformer sireneApiTransformer;

	/** Repository pour la table Référentiel Rna. */
	@Autowired
	private ReferencielRnaRepository referencielRnaRepository;

	/** Transformer du référentiel Rna. */
	@Autowired
	private RnaReferencielTransformer rnaReferencielTransformer;

	/** Utilitaire de génération d'éléments Random */
	@Autowired
	private RegistreRandomUtils registreRandomUtil;

	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<OrganisationEntity, Long> getRepository() {
		return this.organisationRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<OrganisationDto, OrganisationEntity> getTransformer() {
		return this.organisationTransformer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<OrganisationDto> getHatvpOrganisations() {
		return this.organisationRepository.findByHatvpNumberIsNotNull().stream().map(o -> this.organisationTransformer.modelToDto(o)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public String creerOrganisation(final OrganisationDto organisation) {

		if ((StringUtils.isBlank(organisation.getSiren()))) {
			organisation.setSiren(null);
		}
		if ((StringUtils.isBlank(organisation.getRna()))) {
			organisation.setRna(null);
		}
		if ((StringUtils.isBlank(organisation.getDunsNumber()))) {
			organisation.setDunsNumber(null);
		}

		if ((!StringUtils.isBlank(organisation.getSiren()) || !StringUtils.isBlank(organisation.getRna()) || !StringUtils.isBlank(organisation.getDunsNumber()))
				&& !this.organisationRepository.findBySirenOrRnaOrDunsNumber(organisation.getSiren() != null ? organisation.getSiren() : "",
						organisation.getRna() != null ? organisation.getRna() : "", organisation.getDunsNumber() != null ? organisation.getDunsNumber() : "").isEmpty()) {
			throw new BusinessGlobalException(ErrorMessageEnum.CE_ORGANISATION_EXISTE_DEJA.getMessage());
		}

		final OrganisationEntity organisationEntity = this.organisationTransformer.dtoToModel(organisation);

		organisationEntity.setOriginNationalId(TypeIdentifiantNationalEnum.HATVP);
		String hatvpNumber = "H" + String.format("%09d", this.registreRandomUtil.generateRandomNumber(9));
		OrganisationEntity entity = this.organisationRepository.findByHatvpNumber(hatvpNumber);
		if (entity != null) {
			throw new BusinessGlobalException(ErrorMessageEnum.ECHEC_CREATION_ESPACE_ORGA.getMessage());
		}
		organisationEntity.setHatvpNumber(hatvpNumber);
		return this.organisationRepository.save(organisationEntity).getHatvpNumber();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<OrganisationDto> lookUpByNationalNumber(final String nationalId) {

		List<OrganisationDto> orgaDto = null;

		// On consulte le référentiel des organisations, rna, siren, hatvp, ...
		final List<OrganisationEntity> listOrganisations = this.organisationRepository.findBySirenOrRnaOrHatvpNumber(nationalId, nationalId, nationalId);

		if (!listOrganisations.isEmpty()) {
			orgaDto = listOrganisations.stream().map(this.organisationTransformer::modelToDto).collect(Collectors.toList());
		} else {
			final SiretSirenRnaHatvpValidation orgaId = new SiretSirenRnaHatvpValidation(nationalId);
			// SINON, On consulte le référenciel des siren, si le format de l’identifiant saisi est conforme au format d’un numéro SIREN (9 chiffres qui valident formule de Luhn) Ou RNA.
			if (orgaId.getType().equals(SiretSirenRnaHatvpValidation.SIREN) || orgaId.getType().equals(SiretSirenRnaHatvpValidation.RNA)) {
				// Consulter l'api SIREN.
				ApiInsee apiInsee = new ApiInsee();
                ListeEtablissements listeEtablissements = apiInsee.consumeSireneApi("siren",nationalId);
				if (!listeEtablissements.getEtablissements().isEmpty()) {
                    // On récupère l'établissement du siège social
                    Etablissement etablissement = new Etablissement();
                    String nicSiegeUniteLegale = "";
                    for(Etablissement etab : listeEtablissements.getEtablissements()){
                        nicSiegeUniteLegale = etab.getUniteLegale().getNicSiegeUniteLegale();
                        if(etab.getEtablissementSiege().equals("true")){
                            etablissement = etab;
                        }
                    }
                    if(etablissement.getSiren() == null){
                        listeEtablissements = apiInsee.consumeSireneApi("siret",nationalId+nicSiegeUniteLegale);
                        for(Etablissement etabSiege : listeEtablissements.getEtablissements()){
                            etablissement = etabSiege;
                        }
                    }
					orgaDto = Arrays.asList(sireneApiTransformer.modelToDto(etablissement));
					// SINON, on consulte le référenciel des rna, si le format de l’identifiant saisi est conforme au format d’un numéro RNA (w + ...)
				} else if (orgaId.getType().equals(SiretSirenRnaHatvpValidation.RNA)) {
					orgaDto = this.referencielRnaRepository.findByRna(nationalId).stream().map(this.rnaReferencielTransformer::modelToDto).collect(Collectors.toList());
				}
				} else {
				// Format incorrect
				throw new BusinessGlobalException(ErrorMessageEnum.FORMAT_IDENTIFIANT_INCORRECT.getMessage());
			}

			if (orgaDto != null) {
				if (orgaId.getType().equals(SiretSirenRnaHatvpValidation.RNA)) {
					orgaDto.forEach(org -> {
						org.setOriginNationalId(TypeIdentifiantNationalEnum.RNA);
					});
				}
				return orgaDto;
			}
		}

		return Optional.ofNullable(orgaDto).orElse(new ArrayList<>());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AssoClientBatchDto lookUpByNationalNumberBatch(final List<String> nationalIdList) {

		// TODO associerle rna ou siren recu avec l'entité correspondante au moment de la transformation
		// Instantiation du DTO de retour
		final AssoClientBatchDto retDto = new AssoClientBatchDto();

		// On consulte le référentiel des organisations, rna, siren, hatvp, ...
		final List<OrganisationEntity> listOrganisations = this.organisationRepository.findBySirenOrRnaOrHatvpNumberBatch(nationalIdList, nationalIdList, nationalIdList);

		Set<String> rnaSirenToRemove = new HashSet<>();

		// 1/ on s'occupe des organisation connues en base
		if (!listOrganisations.isEmpty()) {
			listOrganisations.forEach(or -> {
				rnaSirenToRemove.add(or.getSiren());
				rnaSirenToRemove.add(or.getRna());
			});
			retDto.getOrganisationList().addAll(listOrganisations.stream().map(this.organisationTransformer::modelToDto).collect(Collectors.toList()));
		}

		// 2/ SINON On rapatrie du référentiel ce qui manque
		nationalIdList.removeAll(rnaSirenToRemove); // SINON => DONC => removeAll

		if (!nationalIdList.isEmpty()) {
			final List<SiretSirenRnaHatvpValidation> orgaIdList = nationalIdList.stream().map(SiretSirenRnaHatvpValidation::new).collect(Collectors.toList());

			orgaIdList.forEach(idItem -> {
				if (SiretSirenRnaHatvpValidation.WRONGNUMBER.equals(idItem.getType()) || SiretSirenRnaHatvpValidation.NOTVALID.equals(idItem.getType())) {
					retDto.addErrorMessage(idItem.getNationalId(), ErrorMessageEnum.FORMAT_ID_NATIONAL_INCORRECT.getMessage());
				}
			});

			// SINON, On consulte le référenciel des siren, si le format de l’identifiant saisi est
			// conforme
			// au format d’un numéro SIREN (9 chiffres qui valident formule de Luhn) Ou RNA.
			final List<String> rnaIdList = orgaIdList.stream().filter(orgaId -> SiretSirenRnaHatvpValidation.RNA.equals(orgaId.getType())).map(SiretSirenRnaHatvpValidation::getNationalId)
					.collect(Collectors.toList());

			final List<String> sirenIdList = orgaIdList.stream().filter(orgaId -> SiretSirenRnaHatvpValidation.SIREN.equals(orgaId.getType()))
					.map(SiretSirenRnaHatvpValidation::getNationalId).collect(Collectors.toList());

			final List<ReferentielSireneEntity> sireneEntityList = this.referencielSirenRepository.findBySirenOrRnaBatch(!sirenIdList.isEmpty() ? sirenIdList : Arrays.asList("null"),
					!rnaIdList.isEmpty() ? rnaIdList : Arrays.asList("null"));

			if (!sireneEntityList.isEmpty()) {
				sireneEntityList.forEach(or -> {
					rnaSirenToRemove.add(or.getRna());
					rnaSirenToRemove.add(or.getSiren());
				});

				retDto.getOrganisationList().addAll(sireneEntityList.stream().map(this.sireneReferencielTransformer::modelToDto).collect(Collectors.toList()));

				nationalIdList.removeAll(rnaSirenToRemove); // SINON => DONC => removeAll ici avant de continuer => ne reste plus que les id à traiter
				rnaIdList.removeAll(rnaSirenToRemove);
			}

			// SINON, on consulte le référenciel des rna, si le format de l’identifiant
			// saisi est conforme
			// au format d’un numéro RNA (w + ...)
			final List<ReferentielRnaEntity> rnaEntityList = this.referencielRnaRepository.findByRnaBatch(!rnaIdList.isEmpty() ? rnaIdList : Arrays.asList("null"));

			if (!rnaEntityList.isEmpty()) {
				rnaEntityList.forEach(or -> {
					rnaSirenToRemove.add(or.getRna());
					rnaSirenToRemove.add(or.getSiren());
				});
			}

			retDto.getOrganisationList().addAll(rnaEntityList.stream().map(this.rnaReferencielTransformer::modelToDto).collect(Collectors.toList()));

			// On verifie les erreurs
			nationalIdList.stream().forEach(idItem -> {
				if (!rnaSirenToRemove.contains(idItem)) {
					retDto.addErrorMessage(idItem, ErrorMessageEnum.ORGANISATION_INTROUVABLE.getMessage());
				}
			});

		}

		return retDto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OrganisationDto getOrganizationDataById(final Long organisationId) {
		return this.organisationRepository.findById(organisationId).map(this.organisationTransformer::modelToDto)
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.ORGANISATION_INTROUVABLE.getMessage(), 404));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public OrganisationDto saveSearchResult(final OrganisationDto organisationDto) {
		final Optional<OrganisationEntity> optional;
		final OrganisationEntity entity;
		// Organisation existe déjà, récupérer ses informations;
		switch (organisationDto.getOriginNationalId()) {
		case RNA:
			optional = this.organisationRepository.findByRna(organisationDto.getRna());
			if (optional.isPresent()) {
				return this.organisationTransformer.modelToDto(optional.get());
			}
			break;
		case HATVP:
			entity = this.organisationRepository.findByHatvpNumber(organisationDto.getHatvpNumber());
			if (Optional.of(entity).isPresent()) {
				return this.organisationTransformer.modelToDto(entity);
			}
			break;
		case SIREN:
			optional = this.organisationRepository.findBySiren(organisationDto.getSiren());
			if (optional.isPresent()) {
				return this.organisationTransformer.modelToDto(optional.get());
			}
			break;
		default:
			throw new BusinessGlobalException(ErrorMessageEnum.ORIGIN_IDENTIFIANT_INCORRECT.getMessage());
		}

		// L'organisation n'existe pas, l'enregistrer.
		return this.organisationTransformer.modelToDto(this.organisationRepository.save(this.organisationTransformer.dtoToModel(organisationDto)));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TypeOrganisationDto> getListeTypesOrganisations() {
		final List<TypeOrganisationDto> result = new ArrayList<>();

		for (final TypeOrganisationEnum value : TypeOrganisationEnum.values()) {
			final TypeOrganisationDto dto = new TypeOrganisationDto();
			dto.setCode(value.name());
			dto.setLabel(value.getLabel());
			dto.setGroup(value.getGroup());
			result.add(dto);
		}

		return result;
	}





}