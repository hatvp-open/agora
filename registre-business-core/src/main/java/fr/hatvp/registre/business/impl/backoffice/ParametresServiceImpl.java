/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice;

import fr.hatvp.registre.business.backoffice.ParametresService;
import fr.hatvp.registre.business.impl.AbstractCommonService;
import fr.hatvp.registre.business.transformer.proxy.ParametresTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.ParametresDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.limitation.ParametresEntity;
import fr.hatvp.registre.persistence.repository.limitation.ParametresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Classe de service d'activation du mode {@link fr.hatvp.registre.business.backoffice.ParametresService} .
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional
public class ParametresServiceImpl
    extends AbstractCommonService<ParametresDto, ParametresEntity>
    implements ParametresService
{

    /**
     * Repository du service parametres.
     */
    @Autowired
    private ParametresRepository parametresRepository;

    /**
     * Tranformeur de la classe {@link ParametresEntity}.
     */
    @Autowired
    private ParametresTransformer parametresTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Boolean switchActivation() {
        final ParametresEntity oldEntity = this.parametresRepository.findOne(1L);

        final ParametresEntity entity = new ParametresEntity();
        RegistreBeanUtils.copyProperties(entity, oldEntity);

        if (entity.getActive()) {
            entity.setActive(false);
        } else {
            entity.setActive(true);
        }

        final ParametresEntity savedEntity = this.parametresRepository.save(entity);
        return savedEntity.getActive();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Integer setLimite(Integer number) {
        final ParametresEntity oldEntity = this.parametresRepository.findOne(1L);

        final ParametresEntity entity = new ParametresEntity();
        RegistreBeanUtils.copyProperties(entity, oldEntity);
        entity.setNombreLimite(number);
        final ParametresEntity savedEntity = this.parametresRepository.save(entity);
        return savedEntity.getNombreLimite();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public ParametresDto getState() {
        final ParametresEntity entity = this.parametresRepository.findOne(1L);
        return this.parametresTransformer.modelToDto(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JpaRepository<ParametresEntity, Long> getRepository() {
        return this.parametresRepository;
    }
    @Override public CommonTransformer<ParametresDto, ParametresEntity> getTransformer()
    {
        return this.parametresTransformer;
    }
}
