/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication;

import fr.hatvp.registre.business.transformer.proxy.common.CommonCollaborateurTransformer;
import fr.hatvp.registre.persistence.entity.publication.PubCollaborateurEntity;

/**
 * Interface de transformation de la classe {@link PubCollaborateurEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PubCollaborateurTransformer
        extends CommonCollaborateurTransformer<PubCollaborateurEntity>
{

}
