/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationFrontTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationMetaTransformer;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.commons.dto.publication.PublicationFrontDto;
import fr.hatvp.registre.commons.dto.publication.PublicationMetaDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

/**
 * Classe du transformeur entre {@link PublicationEntity} et {@link PublicationDto}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional(readOnly = true)
public class PublicationMetaTransformerImpl
    extends AbstractCommonTansformer<PublicationMetaDto, PublicationEntity>
    implements PublicationMetaTransformer
{
    /** Transformeur des publications. */
    @Autowired
    private PublicationFrontTransformer publicationFrontTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    public PublicationMetaDto modelToDto(final PublicationEntity model)
    {
        final PublicationFrontDto dto = this.publicationFrontTransformer.modelToDto(model);

        final PublicationMetaDto meta = new PublicationMetaDto();
        RegistreBeanUtils.copyProperties(meta, model);
        meta.setStatut(model.getStatut());
        meta.setPublication(dto);
        meta.setPublicationDate(model.getCreationDate());

        if (model.getPublicateur() != null)
            meta.setPublicateurNom(
                    model.getPublicateur().getNom() + " " + model.getPublicateur().getPrenom());

        meta.setEspaceOrganisationId(model.getEspaceOrganisation().getId());

        if (model.getDepublicateur() != null) {
            meta.setDepublicationDate(model.getDepublicationDate());
            meta.setDepublicateurNom(
                    model.getDepublicateur().getNom() + " " + model.getDepublicateur().getPrenom());
        }

        if (model.getRepublicateur() != null) {
            meta.setRepublicationDate(model.getRepublicationDate());
            meta.setRepublicateurNom(
                    model.getRepublicateur().getNom() + " " + model.getRepublicateur().getPrenom());
        }

        return meta;
    }
}
