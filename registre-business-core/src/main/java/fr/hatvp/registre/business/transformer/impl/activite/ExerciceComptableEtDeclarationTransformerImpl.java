/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.activite;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ActiviteRepresentationInteretSimpleTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ExerciceComptableEtDeclarationTransformer;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableEtDeclarationDto;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;

@Component
public class ExerciceComptableEtDeclarationTransformerImpl extends AbstractCommonTansformer<ExerciceComptableEtDeclarationDto, ExerciceComptableEntity>
		implements ExerciceComptableEtDeclarationTransformer {

	@Autowired
	private ActiviteRepresentationInteretSimpleTransformer activiteRepresentationInteretSimpleTransformer;

	@Override
	public List<ExerciceComptableEtDeclarationDto> modelToDto(List<ExerciceComptableEntity> entities) {
		List<ExerciceComptableEtDeclarationDto> dtos = new ArrayList<>();

		entities.forEach(entity -> {
			dtos.add(modelToDto(entity));
		});

		return dtos;
	}

	@Override
	public ExerciceComptableEtDeclarationDto modelToDto(ExerciceComptableEntity entity) {
		ExerciceComptableEtDeclarationDto dto = new ExerciceComptableEtDeclarationDto();

		dto.setId(entity.getId());
		dto.setDateDebut(entity.getDateDebut());
		dto.setDateFin(entity.getDateFin());
		dto.setChiffreAffaire(entity.getChiffreAffaire() == null ? null : entity.getChiffreAffaire().getId());
		dto.setHasNotChiffreAffaire(entity.getHasNotChiffreAffaire());
		dto.setMontantDepense(entity.getMontantDepense() == null ? null : entity.getMontantDepense().getId());
		dto.setNombreSalaries(entity.getNombreSalaries());
		dto.setActiviteSimpleDto(activiteRepresentationInteretSimpleTransformer.modelToDto(entity.getActivitesRepresentationInteret()));
		dto.setEspaceOrganisationId(entity.getEspaceOrganisation().getId());
		dto.setStatut(entity.getStatut());
		dto.setIsPublication(entity.getPublicationEnabled());
		dto.setNoActivite(entity.isNoActivite());
		dto.setCommentaire(entity.getCommentaire());

		return dto;
	}
	public List<ExerciceComptableEtDeclarationDto> modelToDtoSansActiviteSupprimee(List<ExerciceComptableEntity> entities) {
		List<ExerciceComptableEtDeclarationDto> dtos = new ArrayList<>();

		entities.forEach(entity -> {
			dtos.add(modelToDtoSansActiviteSupprimee(entity));
		});

		return dtos;
	}
	
	@Override
	public ExerciceComptableEtDeclarationDto modelToDtoSansActiviteSupprimee(ExerciceComptableEntity entity) {
		ExerciceComptableEtDeclarationDto dto = new ExerciceComptableEtDeclarationDto();

		dto.setId(entity.getId());
		dto.setDateDebut(entity.getDateDebut());
		dto.setDateFin(entity.getDateFin());
		dto.setChiffreAffaire(entity.getChiffreAffaire() == null ? null : entity.getChiffreAffaire().getId());
		dto.setHasNotChiffreAffaire(entity.getHasNotChiffreAffaire());
		dto.setMontantDepense(entity.getMontantDepense() == null ? null : entity.getMontantDepense().getId());
		dto.setNombreSalaries(entity.getNombreSalaries());
		dto.setActiviteSimpleDto(activiteRepresentationInteretSimpleTransformer.modelToDtoSansStatutSupprimee(entity.getActivitesRepresentationInteret()));
		dto.setEspaceOrganisationId(entity.getEspaceOrganisation().getId());
		dto.setStatut(entity.getStatut());
		dto.setIsPublication(entity.getPublicationEnabled());
		dto.setNoActivite(entity.isNoActivite());
		dto.setCommentaire(entity.getCommentaire());

		return dto;
	}
}
