/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationSimpleTransformer;
import fr.hatvp.registre.commons.dto.EspaceOrganisationAffichageFODto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

/**
 * Classe de transformation de la classe {@link EspaceOrganisationEntity} & {@link EspaceOrganisationDto}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional(readOnly = true)
public class EspaceOrganisationSimpleTransformerImpl 
	extends AbstractCommonTansformer<EspaceOrganisationAffichageFODto, EspaceOrganisationEntity> 
	implements EspaceOrganisationSimpleTransformer {

	private DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	private DateFormat dateFormatFr = new SimpleDateFormat("dd/MM/yyyy");

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EspaceOrganisationEntity dtoToModel(final EspaceOrganisationAffichageFODto dto) {
		final EspaceOrganisationEntity model = new EspaceOrganisationEntity();
		RegistreBeanUtils.copyProperties(model, dto);

		return model;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EspaceOrganisationAffichageFODto modelToDto(final EspaceOrganisationEntity model) {
		final EspaceOrganisationAffichageFODto dto = new EspaceOrganisationAffichageFODto();
		RegistreBeanUtils.copyProperties(dto, model);
		String identifiantNational = this.determineIdentifiantNational(model.getOrganisation());
		dto.setNationalId(identifiantNational);
		dto.setTypeIdentifiantNational(model.getOrganisation().getOriginNationalId());
		dto.setDenomination(model.getOrganisation().getDenomination());

		dto.setPublication(model.isPublicationEnabled());
		dto.setHasPublication(!model.getPublications().isEmpty());
		Date d = model.getPublications().stream().max(Comparator.comparing(PublicationEntity::getCreationDate)).map(PublicationEntity::getCreationDate).orElse(null);
		dto.setDateDernierePublication(d == null ? null : dateFormat.format(d));

		dto.setDatePremierePublication(model.getDatePremierePublication() == null ? null : dateFormatFr.format(model.getDatePremierePublication()));
		
		if (StringUtils.hasText(model.getSecteursActivites())) {
			final String[] strings = model.getSecteursActivites().split(",");
			for (final String s : strings) {
				dto.getListSecteursActivites().add(SecteurActiviteEnum.valueOf(s));
			}
		}

		if (StringUtils.hasText(model.getNiveauIntervention())) {
			final String[] strings = model.getNiveauIntervention().split(",");
			for (final String s : strings) {
				dto.getListNiveauIntervention().add(NiveauInterventionEnum.valueOf(s));
			}
		}

		return dto;
	}


}
