/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.nomenclature;

import fr.hatvp.registre.commons.dto.nomenclature.DomaineInterventionDto;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;

public interface DomaineInterventionTransformer extends CommonNomenclatureTransformer<DomaineInterventionDto, DomaineInterventionEntity> {

}
