/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static fr.hatvp.registre.commons.utils.RegistreUtils.decode64ToString;
import static fr.hatvp.registre.commons.utils.RegistreUtils.generateNewDate;
import static fr.hatvp.registre.commons.utils.RegistreUtils.generateOldDate;
import static fr.hatvp.registre.commons.utils.RegistreUtils.randomString;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityExistsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import fr.hatvp.registre.business.DeclarantService;
import fr.hatvp.registre.business.email.AccountMailer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantContactTransformer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantContactEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;

/**
 * Description de la classe
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional(readOnly = true)
@Service
public class DeclarantServiceImpl extends AbstractCommonService<DeclarantDto, DeclarantEntity> implements DeclarantService {
	/**
	 * Repository des {@link DeclarantEntity} .
	 */
	@Autowired
	private DeclarantRepository declarantRepository;

	/**
	 * Tranformer des déclarants .
	 */
	@Autowired
	private DeclarantTransformer declarantTransformer;

	/**
	 * Tranformer des contacts de déclarants .
	 */
	@Autowired
	private DeclarantContactTransformer declarantContactTransformer;

	/**
	 * Générateur de mot de passe de spring security.
	 */
	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * Mailer de gestion de compte déclarant.
	 */
	@Autowired
	private AccountMailer accountMailer;

	@Value(value = "${compte.delai.expiration.email.jours}")
	private int nombreDeJoursExpirationEmail;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeclarantDto loadDeclarantByEmail(final String email, final boolean needPassword) {

		final DeclarantEntity declarant = this.declarantRepository.findByEmailIgnoreCase(email);

		if (declarant == null) {
			return null;
		}

		LOGGER.info("Le déclarant avec l'email: ({}) a bien été envoyé: {}",email, declarant);

		if (!needPassword) {
			declarant.setPassword(null);
		}

		return this.declarantTransformer.modelToDto(declarant);
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public DeclarantDto saveDeclarant(final DeclarantDto declarantDto) {

		// Générer un code de confirmation.
		final String confirmationCode = randomString(30);

		declarantDto.setActivationEmailKeyCode(confirmationCode);
		declarantDto.setPassword(this.passwordEncoder.encode(declarantDto.getPassword()));

		// Transformer le DTO déclarant en model.
		final DeclarantEntity declarant = this.declarantTransformer.dtoToModel(declarantDto);

		// récupérer la date d'expiration de compte.
		final Date validationExpiryDate = generateNewDate(this.nombreDeJoursExpirationEmail);
		declarant.setEmailCodeExpiryDate(validationExpiryDate);

		// vérifier le mail avec tous ses états avant d'enregistrer un nv compte.
		this.verifierSiLeCompteExiste(declarantDto);

		// enregistrer le déclarant.
		if (declarant.getContacts() != null) {
			declarant.getContacts().forEach(c -> c.setDeclarant(declarant));
		}

		// normaliser le nom et prénom
		declarant.setNom(declarantDto.getNom().toUpperCase());
		declarant.setPrenom(RegistreUtils.normaliser(declarantDto.getPrenom()));

		final DeclarantEntity savedDeclarantEntity = this.declarantRepository.save(declarant);

		// Envoyer le mail de création de compte.
		if (savedDeclarantEntity != null) {
			this.envoyerLeMailDeValidationDeCompte(declarantDto, confirmationCode);
		}

		return this.declarantTransformer.modelToDto(savedDeclarantEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeclarantDto loadDeclarantById(final Long id) {

		final DeclarantEntity declarant = this.declarantRepository.findOne(id);

		if (declarant == null) {
			throw new BusinessGlobalException(ErrorMessageEnum.UTILISATEUR_INTROUVABLE.getMessage(), 404);
		}

		declarant.setPassword(null);
		return this.declarantTransformer.modelToDto(declarant);
	}

	/**
	 * {@inheritDoc}
	 */
	// FIXME: après la bêta. Réutiliser les méthodes de mise à jour du déclarant comme développé
	// dans
	// fr.hatvp.registre.business.backoffice.DeclarantBoService
	@Transactional
	@Override
	public DeclarantDto updateDeclarant(final DeclarantDto declarantDto) {
		final String password = declarantDto.getPassword();

		// récupérer le déclrant de la bdd.
		final DeclarantEntity declarant = this.declarantRepository.findOne(declarantDto.getId());

		if (declarant == null) {
			throw new BusinessGlobalException(ErrorMessageEnum.DECLARANT_INTROUVABLE.getMessage(), 404);
		}

		// Mise à jour de l'@ email principale.
		if (!StringUtils.isEmpty(declarantDto.getEmail())) {
			return this.updateEmailPrincipalDeclarant(declarantDto, declarant);
		}

		// Mise à jour du téléphone principale.
		else if (!StringUtils.isEmpty(declarantDto.getTelephone())) {
			declarant.setTelephone(declarantDto.getTelephone());
			return this.declarantTransformer.modelToDto(this.declarantRepository.save(declarant));
		}

		// Mise à jour du mot de passe venant du compte déclarant.
		else if ((password != null) && !password.isEmpty() && (declarantDto.getPreviousPassword() != null) && !declarantDto.getPreviousPassword().isEmpty()) {
			return this.updateMotDePasseCompteDeclarant(declarant, declarantDto);
		}
		// Mise à jour des informations de contact du déclarant.
		else {
			return this.updateDonneesContactDeclarant(declarantDto, declarant);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public DeclarantDto activationDeCompte(final String key, final String password) {
		final DeclarantEntity declarantEntity = this.verifierLeTokenEtLeMotDePasse(key, password);

		// Si compte déjà validé
		if (declarantEntity.isActivated()) {
			LOGGER.warn(ErrorMessageEnum.COMPTE_DEJA_ACTIVE.getMessage());
			throw new BusinessGlobalException(ErrorMessageEnum.COMPTE_DEJA_ACTIVE.getMessage(), 422);
		}

		// Activé le compte.
		declarantEntity.setActivated(true);
		// Rendre le token inutilisable.
		declarantEntity.setEmailCodeExpiryDate(generateOldDate(1));
		// Mettre à jour le déclarant.
		final DeclarantEntity savedDeclarant = this.declarantRepository.save(declarantEntity);

		return this.declarantTransformer.modelToDto(savedDeclarant);
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public DeclarantDto activationDeLaNouvelleAdresseEmail(final String key, final String password) {
		final DeclarantEntity declarantEntity = this.verifierLeTokenEtLeMotDePasse(key, password);

		// tester si l'adresse n'a pas été utilisé entre temps pour une nouvelle inscription.
		final Optional<DeclarantEntity> optional = Optional.ofNullable(this.declarantRepository.findByEmailIgnoreCase(declarantEntity.getEmailTemp()));
		if (optional.isPresent()) {
			final DeclarantEntity d = optional.get();
			if (d.isActivated() || (!d.isActivated() && !RegistreUtils.isDateExpire(d.getEmailCodeExpiryDate()))) {
				throw new EntityExistsException(ErrorMessageEnum.ADRESSE_EMAIL_EST_DEJA_ACTIVE.getMessage());
			} else if (!d.isActivated() && RegistreUtils.isDateExpire(d.getEmailCodeExpiryDate())) {
				// supprimer le compte pour permettre la mise à jour de l'adresse email.
				this.declarantRepository.delete(d);
				// si on commit pas les changement, une exception d'unicité sera levé
				this.declarantRepository.flush();
			}
		}

		// Rendre la nouvelle adresse email utilisable.
		declarantEntity.setEmail(declarantEntity.getEmailTemp());
		// vider l'adresse email temporaire.
		declarantEntity.setEmailTemp(null);
		// Rendre le token inutilisable.
		declarantEntity.setEmailCodeExpiryDate(generateOldDate(1));

		// Mettre à jour le déclarant.
		final DeclarantEntity savedDeclarant = this.declarantRepository.save(declarantEntity);

		return this.declarantTransformer.modelToDto(savedDeclarant);
	}

	@Transactional
	@Override
	public DeclarantDto updateMotDePasseRecuperationDeCompte(final DeclarantDto declarant) {
		final DeclarantEntity declarantEntity = this.declarantRepository.findByRecoverConfirmationCode(decode64ToString(declarant.getRecuperationPasswordKeyCode()));

		if (declarantEntity == null) {
			throw new BusinessGlobalException(ErrorMessageEnum.DECLARANT_INTROUVABLE.getMessage(), 404);
		}

		declarantEntity.setPassword(this.passwordEncoder.encode(declarant.getPassword()));
		declarantEntity.setRecoverConfirmationCode(null);

		if (RegistreUtils.isDateExpire(declarantEntity.getRecoverCodeExpiryDate())) {
			throw new BusinessGlobalException(ErrorMessageEnum.LE_TOKEN_DE_RECUPERATION_A_EXPIRE.getMessage(), 400);
		}
		declarantEntity.setRecoverCodeExpiryDate(null);

		// Mettre à jour le déclarant.
		final DeclarantEntity declarantEntitySaved = this.declarantRepository.save(declarantEntity);

		// retourner les données du déclarant enregistré.
		return this.declarantTransformer.modelToDto(declarantEntitySaved);
	}

	/**
	 * Mise à jour des données contact du déclarant.
	 *
	 * @param declarantDto
	 *            les informations envoyés par le service.
	 * @param declarantEntity
	 *            les informationq récupérées de la bdd.
	 */
	@Transactional
	private DeclarantDto updateDonneesContactDeclarant(final DeclarantDto declarantDto, final DeclarantEntity declarantEntity) {
		final Set<DeclarantContactEntity> contacts = new HashSet<>();

		contacts.addAll(this.declarantContactTransformer.dtoToModel(declarantDto.getEmailComplement()));
		contacts.addAll(this.declarantContactTransformer.dtoToModel(declarantDto.getPhone()));

		contacts.forEach(c -> c.setDeclarant(declarantEntity));
		declarantEntity.setContacts(contacts);

		// Mettre à jour le déclarant.
		final DeclarantEntity declarantEntitySaved = this.declarantRepository.save(declarantEntity);

		// retourner les données du déclarant enregistré.
		return this.declarantTransformer.modelToDto(declarantEntitySaved);
	}

	/**
	 * Modifier le mot de passe du déclarant.
	 *
	 * @param declarantDto
	 *            contient le nouveau et l'ancien mot de pase.
	 * @param declarantEntity
	 *            entité contenant les ancienne informations en bdd.
	 */
	@Transactional
	public DeclarantDto updateMotDePasseCompteDeclarant(final DeclarantEntity declarantEntity, final DeclarantDto declarantDto) {
		// Rejeter le mot de passe si l'ancien mot de passe envoyé ne correspond pas à celui dans la
		// bdd.
		if (!this.passwordEncoder.matches(declarantDto.getPreviousPassword(), declarantEntity.getPassword())) {
			throw new BusinessGlobalException(ErrorMessageEnum.ANCIEN_MOT_DE_PASSE_SAISI_INCORRRECT.getMessage(), 422);
		}
		// Rejeter le mot de passe s'il est identique à l'ancien.
		if (this.passwordEncoder.matches(declarantDto.getPassword(), declarantEntity.getPassword())) {
			throw new BusinessGlobalException(ErrorMessageEnum.MOT_DE_PASSE_INVALIDE.getMessage(), 422);
		}

		// add new password
		declarantEntity.setPassword(this.passwordEncoder.encode(declarantDto.getPassword()));
		// Mettre à jour le déclarant.
		final DeclarantEntity declarantEntitySaved = this.declarantRepository.save(declarantEntity);
		// retourner les données du déclarant enregistré.
		return this.declarantTransformer.modelToDto(declarantEntitySaved);
	}

	/**
	 * Mise à jour de l'email pricipale du déclarant.
	 *
	 * @param declarantDto
	 *            declarant DTO.
	 * @param declarant
	 *            declarant entitée.
	 */
	@Transactional
	public DeclarantDto updateEmailPrincipalDeclarant(final DeclarantDto declarantDto, final DeclarantEntity declarant) {

		final String email = declarantDto.getEmail();
		// Rejeter le nouveau email s'il est le même que l'ancien.
		if (email.equals(declarant.getEmail())) {
			throw new BusinessGlobalException(ErrorMessageEnum.ADRESSE_EMAIL_SAISI_EST_CELLE_QUE_VOUS_UTILISEZ_DEJA.getMessage(), 400);
		}

		// Rejeter le nouveau email s'il existe déjà dans la bdd.
		if (this.declarantRepository.findByEmailIgnoreCase(email) != null) {
			throw new BusinessGlobalException(ErrorMessageEnum.ADRESSE_EMAIL_EXISTE_DEJA.getMessage(), 409);
		}

		// Rejeter le nouveau email s'il est en attente de validation par un autre utilisateur.
		final DeclarantEntity declarantEntityTemp = this.declarantRepository.findByEmailTemp(email);
		// si c un autre utilisateur, vérifier si elle a expiré ou pas encore.
		if (declarantEntityTemp != null && !declarantEntityTemp.getId().equals(declarant.getId())) {
			if (!RegistreUtils.isDateExpire(declarantEntityTemp.getEmailCodeExpiryDate())) {
				throw new BusinessGlobalException(ErrorMessageEnum.EMAIL_EN_ATTENTE_DE_VALIDATION_PAR_UN_AUTRE_UTILISATEUR.getMessage(), 409);
			} else {
				declarantEntityTemp.setEmailTemp(null);
				this.declarantRepository.save(declarantEntityTemp);
			}
		}

		final String confirmationCode = randomString(30);

		declarant.setEmailTemp(email);
		declarant.setEmailCodeExpiryDate(generateNewDate(this.nombreDeJoursExpirationEmail));
		declarant.setEmailConfirmationCode(confirmationCode);

		// enregistrer l'adresse email temporaire et sa date d'expiration.
		final DeclarantEntity declarantEntitySaved = this.declarantRepository.save(declarant);

		final DeclarantDto result = this.declarantTransformer.modelToDto(declarantEntitySaved);

		// Envoyer un email de confirmation de la nouvelle adresse email principale.
		result.setEmail(email);
		this.envoyerEmailDeConfirmationDeModificationDeMail(result, confirmationCode);

		// retourner les données du déclarant enregistré.
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public DeclarantDto recuperationDeCompte(final String email) {

		final DeclarantEntity declarant = this.declarantRepository.findByEmailIgnoreCase(email);

		// vérifier que la clé est bien associé avec un compte.
		if (declarant == null) {
			LOGGER.error(ErrorMessageEnum.COMPTE_INTROUVABLE.getMessage() + " - " + email);
			return null;
		}
		// vérifier que le compte est déjà activé.
		else if (!declarant.isActivated()) {
			LOGGER.error(ErrorMessageEnum.VOTRE_COMPTE_N_EST_PAS_ENCORE_ACTIVE.getMessage() + " - " + email);
			return null;
		}

		// générer une clé d'activation.
		final String confirmationCode = randomString(40);
		// Ajouter une date d'expiration de la clé de validation.
		final Date recoverExpiryDate = generateNewDate(this.nombreDeJoursExpirationEmail);

		declarant.setRecoverConfirmationCode(confirmationCode);
		declarant.setRecoverCodeExpiryDate(recoverExpiryDate);

		final DeclarantEntity declarantEntity = this.declarantRepository.save(declarant);

		final DeclarantDto result = this.declarantTransformer.modelToDto(declarantEntity);
		// Envoyer le mail de récupération
		this.envoyerEmailDeRcuperationDeCompte(result, confirmationCode);

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void updateLastConnexionDate(final Long id) {
		final DeclarantEntity entity = this.declarantRepository.findOne(id);
		entity.setLastConnexionDate(new Date());
		this.declarantRepository.save(entity);
	}

	@Transactional
    @Override
    public void updateFirstConn(Long id) {
        final DeclarantEntity entity = this.declarantRepository.findOne(id);
        entity.setFirstConn(false);
        this.declarantRepository.save(entity);
    }

    /**
	 * Méthode d'envoi de mail de récupération de compte.
	 *
	 * @param dto
	 *            dto du déclarant.
	 * @param confirmationCode
	 *            le token de validation.s
	 */
	private void envoyerEmailDeRcuperationDeCompte(final DeclarantDto dto, final String confirmationCode) {
		this.accountMailer.sendRecoverPasswordEmail(dto, RegistreUtils.codeTo64(confirmationCode));
	}

	/**
	 * @param dto
	 *            le nouvel dto du déclarant.
	 * @param confirmationCode
	 *            le code de confirmation de l'dto.
	 */
	private void envoyerEmailDeConfirmationDeModificationDeMail(final DeclarantDto dto, final String confirmationCode) {
		this.accountMailer.sendEmailUpdateConfirmation(dto, RegistreUtils.codeTo64(confirmationCode));
	}

	/**
	 * Envoyer le mail de confirmation de compte à l'utilisateur.
	 *
	 * @param dto
	 *            information du déclarant.
	 * @param confirmationCode
	 *            code de confirmation à envoyer.
	 */
	private void envoyerLeMailDeValidationDeCompte(final DeclarantDto dto, final String confirmationCode) {
		this.accountMailer.sendAccountConfirmationEmail(dto, RegistreUtils.codeTo64(confirmationCode));
	}

	/**
	 * Si un compte existe déjà pour cette adresse email: 1- Compte déjà validée, renvoyer une erreur. 2- Date de validation de l'email n'a pas expiré => Renvoyer une erreur. 3-
	 * Date de validation a expiré => supprimer le compte.
	 *
	 * @param declarantDto
	 *            l'utilisateur à vérifier.
	 */
	public boolean verifierSiLeCompteExiste(final DeclarantDto declarantDto) {
		final DeclarantEntity oldDeclarant = this.declarantRepository.findByEmailIgnoreCaseOrEmailTempIgnoreCase(declarantDto.getEmail(), declarantDto.getEmail());
		if (oldDeclarant != null) {

			// le mail déjà pris par un autre déclarant
			if (oldDeclarant.getEmail().equalsIgnoreCase(declarantDto.getEmail())) {
				if (oldDeclarant.isActivated()) {
					throw new EntityExistsException(ErrorMessageEnum.ADRESSE_EMAIL_EST_DEJA_ACTIVE.getMessage());
				}

				if (RegistreUtils.isDateExpire(oldDeclarant.getEmailCodeExpiryDate())) {
					// le code validation a expiré, supprimer le compte.
					this.declarantRepository.delete(oldDeclarant);
					this.declarantRepository.flush();
				} else {
					// le compte n'a pas expiré, renvoyer une erreur.
					throw new EntityExistsException(ErrorMessageEnum.ADRESSE_EMAIL_EXISTE_DEJA.getMessage());
				}
			}
			// email en attente de validation de modification par un autre déclarant.
			else if (oldDeclarant.getEmailTemp().equalsIgnoreCase(declarantDto.getEmail())
					&& !RegistreUtils.isDateExpire(oldDeclarant.getEmailCodeExpiryDate())) {
				// le compte est sans doute valide
				// tester si la modification a expiré
					// le compte n'a pas expiré, renvoyer une erreur.
				throw new EntityExistsException(ErrorMessageEnum.ADRESSE_EMAIL_En_ATTENTE_DE_VALIDATION.getMessage());
			}
		}
		return true;
	}

	/**
	 * Méthode mutualisée pour l'activation de compte et la validation d'une nouvelle adresse email. Permet de valider que le token n'a pas expiré et que le mot de passe saisi est
	 * corrrect.
	 *
	 * @param key
	 *            token de validation.
	 * @param password
	 *            mot de passe du déclarant.
	 */
	private DeclarantEntity verifierLeTokenEtLeMotDePasse(final String key, final String password) {

		final String decodedKey = decode64ToString(key);

		final DeclarantEntity declarant = this.declarantRepository.findByEmailConfirmationCode(decodedKey);

		if (declarant == null) {
			LOGGER.warn("Aucun déclarant avec ce token n'a été trouvé: {}", key);
			throw new BusinessGlobalException(ErrorMessageEnum.COMPTE_INTROUVABLE.getMessage(), 422);
		}

		if (!this.passwordEncoder.matches(password, declarant.getPassword())) {
			LOGGER.warn("Le mot de passe saisi ne correspond pas à l'ancien mot de passe. {}", declarant);
			throw new BusinessGlobalException(ErrorMessageEnum.MOT_DE_PASSE_INVALIDE.getMessage(), 422);
		}

		// check if the validation link has been expired
		if (RegistreUtils.isDateExpire(declarant.getEmailCodeExpiryDate())) {
			LOGGER.warn(ErrorMessageEnum.LE_TOKEN_ACTIVATION_DE_COMPTE_EXPIRE.getMessage() + ": " + key);
			throw new BusinessGlobalException(ErrorMessageEnum.LE_TOKEN_ACTIVATION_DE_COMPTE_EXPIRE.getMessage(), 422);
		}

		return declarant;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<DeclarantEntity, Long> getRepository() {
		return this.declarantRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<DeclarantDto, DeclarantEntity> getTransformer() {
		return this.declarantTransformer;
	}

}
