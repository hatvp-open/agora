/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.impl;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.ParametresTransformer;
import fr.hatvp.registre.commons.dto.backoffice.ParametresDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.limitation.ParametresEntity;

/**
 * Classe de transformation de la classe {@link fr.hatvp.registre.business.transformer.proxy.ParametresTransformer}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class ParametresTransformerImpl
    extends AbstractCommonTansformer<ParametresDto, ParametresEntity>
    implements ParametresTransformer
{
    /**
     * {@inheritDoc}
     */
    @Override
    public ParametresDto modelToDto(final ParametresEntity model)
    {
        final ParametresDto dto = new ParametresDto();
        RegistreBeanUtils.copyProperties(dto, model);
        return dto;
    }
}
