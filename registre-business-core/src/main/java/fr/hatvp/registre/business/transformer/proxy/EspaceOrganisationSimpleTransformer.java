/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.EspaceOrganisationAffichageFODto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * Interface de transformation de la classe {@link EspaceOrganisationEntity} & {@link EspaceOrganisationDto}.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface EspaceOrganisationSimpleTransformer
    extends CommonTransformer<EspaceOrganisationAffichageFODto, EspaceOrganisationEntity> {

}
