/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice.comment;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.backoffice.comment.CommentDemandeBoService;
import fr.hatvp.registre.business.transformer.backoffice.comment.CommentDemandeBoTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentDemandeEntity;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentDemandeRepository;

/**
 * classe d'implémentation de gestion des commentaires des demandes.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CommentDemandeServiceImpl extends CommonCommentServiceImpl<CommentBoDto, CommentDemandeEntity> implements CommentDemandeBoService {

	/**
	 * Repository de la classe.
	 */
	@Autowired
	private CommentDemandeRepository commentDemandeRepository;

	/**
	 * Transformeur de la classe.
	 */
	@Autowired
	private CommentDemandeBoTransformer commentDemandeBoTransformer;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CommentBoDto> getAllComment(final Long demandeId) {
		return this.commentDemandeRepository.findByDemandeOrganisationEntityId(demandeId).stream().map(this.commentDemandeBoTransformer::commentModelToDto)
				.sorted(Comparator.comparing(CommentBoDto::getDateModification)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public CommentBoDto addComment(final CommentBoDto commentBoDto) {
		final CommentDemandeEntity entity = this.getTransformer().dtoToModel(commentBoDto);
		entity.setDateCreation(new Date());
		return this.commentDemandeBoTransformer.commentModelToDto(this.commentDemandeRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public CommentBoDto updateComment(final CommentBoDto commentBoDto) {
		final CommentDemandeEntity entityT = this.getTransformer().dtoToModel(commentBoDto);
		final CommentDemandeEntity entityR = this.getRepository().findOne(commentBoDto.getId());
		entityT.setEditeur(entityR.getEditeur());
		entityT.setDateCreation(entityR.getDateCreation());
		entityT.setDateModification(new Date());
		return this.commentDemandeBoTransformer.commentModelToDto(this.commentDemandeRepository.save(entityT));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<CommentDemandeEntity, Long> getRepository() {
		return this.commentDemandeRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<CommentBoDto, CommentDemandeEntity> getTransformer() {
		return this.commentDemandeBoTransformer;
	}
}
