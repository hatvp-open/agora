/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.nomenclature;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.proxy.nomenclature.DecisionConcerneeTransformer;
import fr.hatvp.registre.commons.dto.nomenclature.DecisionConcerneeDto;
import fr.hatvp.registre.persistence.entity.nomenclature.DecisionConcerneeEntity;

@Component
public class DecisionConcerneeTransformerParentImpl extends AbstractCommonNomenclatureTransformer<DecisionConcerneeDto, DecisionConcerneeEntity> implements DecisionConcerneeTransformer {

	/** Init super DTO. */
	public DecisionConcerneeTransformerParentImpl() {
		super.dtoClass = DecisionConcerneeDto.class;
	}
}
