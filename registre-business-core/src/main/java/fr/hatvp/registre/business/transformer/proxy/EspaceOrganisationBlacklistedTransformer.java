/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.EspaceOrganisationBlacklistedDto;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * Interface de transformation de la classe {@link EspaceOrganisationEntity} & {@link EspaceOrganisationBlacklistedDto}.
 */

public interface EspaceOrganisationBlacklistedTransformer extends CommonTransformer<EspaceOrganisationBlacklistedDto, EspaceOrganisationEntity> {

}
