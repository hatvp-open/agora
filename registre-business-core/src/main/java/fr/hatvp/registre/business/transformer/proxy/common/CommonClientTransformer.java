/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.common;

import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.persistence.entity.espace.common.CommonClientEntity;

/**
 * Proxy de transformation commun pour les clients.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface CommonClientTransformer<E extends CommonClientEntity>
		extends CommonTransformer<AssoClientDto, E> {
}
