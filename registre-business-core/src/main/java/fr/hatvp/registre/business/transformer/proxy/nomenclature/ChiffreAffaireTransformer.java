/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.nomenclature;

import fr.hatvp.registre.commons.dto.nomenclature.ChiffreAffaireDto;
import fr.hatvp.registre.persistence.entity.nomenclature.ChiffreAffaireEntity;

public interface ChiffreAffaireTransformer extends CommonNomenclatureTransformer<ChiffreAffaireDto, ChiffreAffaireEntity> {

}
