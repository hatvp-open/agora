/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication;

import java.text.ParseException;
import java.time.LocalDate;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationRechercheTransformer;
import fr.hatvp.registre.commons.dto.publication.PublicationRechercheBlacklistDto;
import fr.hatvp.registre.commons.dto.publication.PublicationRechercheDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActiviteRechercheDto;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;

/**
 * Classe du transformeur entre {@link PublicationEntity} et {@link PublicationRechercheDto}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class PublicationRechercheTransformerImpl extends AbstractCommonTansformer<PublicationRechercheDto, PublicationEntity> implements PublicationRechercheTransformer {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationEntity dtoToModel(final PublicationRechercheDto dto) {
		final PublicationEntity entity = new PublicationEntity();
		RegistreBeanUtils.copyProperties(entity, dto);
		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationRechercheDto modelToDto(final PublicationEntity model) {
		final PublicationRechercheDto dto = new PublicationRechercheDto();
		RegistreBeanUtils.copyProperties(dto, model);
		dto.setId(null);
		dto.setVersion(null);
		dto.setNomUsageHatvp(model.getEspaceOrganisation().getNomUsageHatvp());
		dto.setSigleHatvp(model.getEspaceOrganisation().getSigleHatvp());
		dto.setAncienNomHatvp(model.getEspaceOrganisation().getAncienNomHatvp());


		return dto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PublicationEntity> dtoToModel(final List<PublicationRechercheDto> dtos) {
		return dtos.stream().map(this::dtoToModel).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PublicationRechercheDto> modelToDto(final List<PublicationEntity> models) {
		return models.stream().map(this::modelToDto).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PublicationRechercheDto> modelToDto(final Stream<PublicationEntity> models) {
		return models.map(this::modelToDto).collect(Collectors.toList());
	}

	@Override
	public PublicationRechercheDto modelActiviteToDtoPublicationRecherche(PublicationActiviteEntity model) {
		final PublicationRechercheDto dto = new PublicationRechercheDto();
		dto.setDenomination(model.getActivite().getExerciceComptable().getEspaceOrganisation().getOrganisation().getDenomination());
		dto.setNomUsage(model.getActivite().getExerciceComptable().getEspaceOrganisation().getNomUsage());
		dto.setNationalId(this.determineIdentifiantNational(model.getActivite().getExerciceComptable().getEspaceOrganisation().getOrganisation()));
		dto.setCategorieOrganisation(model.getActivite().getExerciceComptable().getEspaceOrganisation().getCategorieOrganisation());
		dto.setId(null);
		dto.setVersion(null);

		return dto;
	}

	@Override
	public PublicationRechercheDto modelExerciceToDtoPublicationRecherche(ExerciceComptableEntity entity) {
		final PublicationRechercheDto dto = new PublicationRechercheDto();
		dto.setDenomination(entity.getEspaceOrganisation().getOrganisation().getDenomination());
		dto.setNomUsage(entity.getEspaceOrganisation().getNomUsage());
		dto.setNationalId(this.determineIdentifiantNational(entity.getEspaceOrganisation().getOrganisation()));
		dto.setCategorieOrganisation(entity.getEspaceOrganisation().getCategorieOrganisation());
		dto.setId(null);
		dto.setVersion(null);

		return dto;
	}
	@Override
	public PublicationRechercheDto modelExerciceToDtoPublicationRechercheBlacklist(EspaceOrganisationEntity entity) {
		final PublicationRechercheDto dto = new PublicationRechercheBlacklistDto();
		dto.setDenomination(entity.getOrganisation().getDenomination());
		dto.setNomUsage(entity.getNomUsage());
		dto.setNationalId(this.determineIdentifiantNational(entity.getOrganisation()));
		dto.setCategorieOrganisation(entity.getCategorieOrganisation());

		dto.setId(null);
		dto.setVersion(null);

		return dto;
	}
	@Override
	public PublicationRechercheBlacklistDto modelExerciceToDtoPublicationRechercheBlacklist(ExerciceComptableEntity entity) {
		final PublicationRechercheBlacklistDto dto = new PublicationRechercheBlacklistDto();
		dto.setDenomination(entity.getEspaceOrganisation().getOrganisation().getDenomination());
		dto.setNomUsage(entity.getEspaceOrganisation().getNomUsage());
		dto.setNationalId(this.determineIdentifiantNational(entity.getEspaceOrganisation().getOrganisation()));
		dto.setCategorieOrganisation(entity.getEspaceOrganisation().getCategorieOrganisation());
		dto.setExerciceBlackliste("Période du "+entity.getDateDebut().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+" au "+entity.getDateFin().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		dto.setEcheance(Date.from(entity.getDateFin().plusMonths(3).atStartOfDay(ZoneId.systemDefault()).toInstant()));
		//verrue cracra pour la blacklist 2019 : on doit mettre lq date d'échéance au 24/08/2020 pour les exercices se cloturant au 31/12/2019
        if (entity.getDateFin().isEqual( LocalDate.of(2019, 12,31 ))) {
            try {
                dto.setEcheance(new SimpleDateFormat("dd/MM/yyyy").parse("24/08/2020"));
            } catch (ParseException e) {
                throw new BusinessGlobalException(ErrorMessageEnum.ERREUR_FORMAT_DATE.getMessage());
            }
        }

        Boolean pasActivites = entity.getActivitesRepresentationInteret().stream().filter(a->!StatutPublicationEnum.NON_PUBLIEE.equals(a.getStatut()) && !StatutPublicationEnum.SUPPRIMEE.equals(a.getStatut())).count()==0 && !entity.isNoActivite();
		Boolean pasMoyens = StatutPublicationEnum.NON_PUBLIEE.equals(entity.getStatut());
		dto.setInfosNonDeclarees(pasMoyens? pasActivites ?" Activités et moyens":"Moyens":"Activités" );
		dto.setId(null);
		dto.setVersion(null);

		return dto;
	}
	
	@Override
	public PublicationActiviteRechercheDto modelActiviteToDtoPublicationActiviteRecherche(PublicationActiviteEntity entity) {
		final PublicationActiviteRechercheDto dto = new PublicationActiviteRechercheDto();
		dto.setObjet(entity.getActivite().getObjet());
		dto.setDomainesIntervention(entity.getActivite().getDomainesIntervention().stream().map(DomaineInterventionEntity::getLibelle).collect(Collectors.toList()));
		dto.setNom_organisation(entity.getActivite().getExerciceComptable().getEspaceOrganisation().getOrganisation().getDenomination());
		dto.setNum_organisation(entity.getActivite().getExerciceComptable().getEspaceOrganisation().getOrganisation().getSiren());
		dto.setIdentifiantFiche(entity.getActivite().getIdFiche());
		return dto;
	}
}
