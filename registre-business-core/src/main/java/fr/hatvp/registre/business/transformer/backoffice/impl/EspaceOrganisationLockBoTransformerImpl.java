/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.EspaceOrganisationLockBoTransformer;
import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.commons.dto.backoffice.EspaceOrganisationLockBoDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.backoffice.EspaceOrganisationLockBoEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * Classe de transformation de la classe {@link EspaceOrganisationLockBoEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class EspaceOrganisationLockBoTransformerImpl
    extends AbstractCommonTansformer<EspaceOrganisationLockBoDto, EspaceOrganisationLockBoEntity>
    implements EspaceOrganisationLockBoTransformer
{
    /**
     * Durée Timeout lock d'entité (en secondes)
     */
    @Value("${app.lock.espaceorganisation.timeout}")
    private long lockTimeout;

    /**
     * {@inheritDoc}
     */
    @Override
    public EspaceOrganisationLockBoEntity dtoToModel(final EspaceOrganisationLockBoDto dto)
    {
        final EspaceOrganisationLockBoEntity model = new EspaceOrganisationLockBoEntity();
        RegistreBeanUtils.copyProperties(model, dto);

        final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(dto.getEspaceOrganisationId());
        model.setEspaceOrganisation(espaceOrganisationEntity);

        model.setLockedTime(dto.getLockTimeStart());

        final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
        utilisateurBoEntity.setId(dto.getUtilisateurBoId());
        model.setUtilisateurBo(utilisateurBoEntity);

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EspaceOrganisationLockBoDto modelToDto(final EspaceOrganisationLockBoEntity model)
    {
        final EspaceOrganisationLockBoDto dto = new EspaceOrganisationLockBoDto();
        RegistreBeanUtils.copyProperties(dto, model);

        dto.setEspaceOrganisationId(model.getEspaceOrganisation().getId());
        dto.setLockTimeStart(model.getLockedTime());

        final Long remainingTime = this.lockTimeout
                - ((new Date().getTime() - model.getLockedTime().getTime()) / 1000);
        dto.setLockTimeRemain(remainingTime > 0 ? remainingTime : 0);

        dto.setUtilisateurBoId(model.getUtilisateurBo().getId());
        dto.setUtilisateurBoName(
                model.getUtilisateurBo().getNom() + " " + model.getUtilisateurBo().getPrenom());

        return dto;
    }
}
