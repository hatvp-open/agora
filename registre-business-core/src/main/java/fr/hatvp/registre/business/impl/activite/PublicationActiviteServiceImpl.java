/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.activite;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.PublicationActiviteService;
import fr.hatvp.registre.business.email.PublicationMailer;
import fr.hatvp.registre.business.email.SurveillanceMailer;
import fr.hatvp.registre.business.pdf.activite.Activite;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationActiviteTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationBucketActiviteTransformer;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActiviteDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketActiviteDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;
import fr.hatvp.registre.persistence.repository.activite.ActiviteRepresentationInteretRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationActiviteRepository;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;

/**
 * Classe d'implémentation du service des publications d'exercice.
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional
@Service
public class PublicationActiviteServiceImpl implements PublicationActiviteService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PublicationActiviteServiceImpl.class);

	/** Repository des publications. */
	@Autowired
	private PublicationActiviteRepository publicationActiviteRepository;

	@Autowired
	private ActiviteRepresentationInteretRepository activiteRepository;

	@Autowired
	private PublicationBucketActiviteTransformer publicationBucketTransformer;

	@Autowired
	private ActiviteRepresentationInteretRepository activiteRepresentationInteretRepository;

	@Autowired
	private PublicationRepository publicationRepository;

	/**
	 * Repository des espaces organisations.
	 */
	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;
	@Autowired
	private PublicationActiviteTransformer publicationActiviteTransformer;

	@Autowired
	private DeclarantRepository declarantRepository;

	@Autowired
	private DeclarantTransformer declarantTransformer;

	@Autowired
	private InscriptionEspaceRepository inscriptionEspaceRepository;

	@Autowired
	private PublicationMailer publicationMailer;

	@Autowired
	private SurveillanceMailer surveillanceMailer;
	


	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationBucketActiviteDto getPublicationLivrableActivite(final Long activiteId,boolean withHistorique) {
		final List<PublicationActiviteEntity> publicationsActivite = this.publicationActiviteRepository.findAllByActiviteIdAndStatutNotOrderByIdDesc(activiteId,
				StatutPublicationEnum.DEPUBLIEE);
		// Il est possible que cette liste soit vide si l'exercice n'a fait aucune publication
		if (!publicationsActivite.isEmpty()) {
			return this.publicationBucketTransformer.listPublicationToPublicationBucketActivite(publicationsActivite,withHistorique);
		} else {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void savePublicationActivite(final Long connectedUser, final Long currentEspaceOrganisationId, final Long activiteId) {

		// Récupération du DTO à publier enregistré
		ActiviteRepresentationInteretEntity activite = activiteRepresentationInteretRepository.findOne(activiteId);

		// Vérification que la publication de l'exercice correspond à l'espace pour lequel on est connecté
		if (activite.getExerciceComptable().getEspaceOrganisation().getId().longValue() != currentEspaceOrganisationId.longValue()) {
			throw new BusinessGlobalException(ErrorMessageEnum.CONTENU_PUBLICATION_INTROUVABLE.getMessage(), 410);
		}

		// verification que l'EC a bien déja fait une publication de son espace
		if (!this.publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(currentEspaceOrganisationId, StatutPublicationEnum.DEPUBLIEE).isPresent()) {
			throw new BusinessGlobalException(ErrorMessageEnum.PUBLICATION_ACTIVITE_NO_PUBLICATION.getMessage(), 410);
		}

		// Conversion des données de l'activite récupéré en données de publication
		final PublicationActiviteDto publicationDto = publicationActiviteTransformer.activiteToPublication(activite);

		// Transformation en entity
		PublicationActiviteEntity publicationEntity = this.publicationActiviteTransformer.dtoToModel(publicationDto);

		// Rattachement a l'activite
		publicationEntity.setActivite(activite);
		publicationEntity.setVersionActivite(activite.getVersion());

		// Enregistrement de l'identitée du publicateur
		final DeclarantEntity publicateur = this.declarantRepository.findOne(connectedUser);
		publicationEntity.setPublicateur(publicateur);

		// Mise à jour du flag de l'espace pour déclancher une publication de celui ci
		final EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository.findOne(currentEspaceOrganisationId);
		espaceOrganisationEntity.setNewPublication(Boolean.TRUE);
		this.espaceOrganisationRepository.save(espaceOrganisationEntity);

		// Obtenir la dernière publication de cette fiche
		final PublicationActiviteEntity alreadyPublished = this.publicationActiviteRepository.findFirstByActiviteIdOrderByCreationDateDesc(activiteId);

		// Mise à jour du flag des publications sur l'espace organisation et du flag ElementsAPublier
		this.updateFlagDePublicationTrue(activite, publicationEntity);
		// Mise Ã  jour du flag de blacklistage
		activite.getExerciceComptable().setBlacklisted(false);
		activite.getExerciceComptable().setAfficheBlacklist(false);
		// Enregistrer la publication et l'activite
		final PublicationActiviteEntity savedPublication = this.publicationActiviteRepository.save(publicationEntity);
		this.activiteRepresentationInteretRepository.save(activite);

		// Mise à jour des boolean de déclaration nulle
        activite.getExerciceComptable().setDataToPublish(false);
        activite.getExerciceComptable().setNoActivite(false);
		// Envoie de mail à tout les publicateurs de l'espace.
		envoyerUnEmailAuxPublicateursDeLespaceOrganisation(savedPublication, activite, currentEspaceOrganisationId, Optional.ofNullable(alreadyPublished).isPresent());
        SurveillanceEntity surveillance = espaceOrganisationEntity.getOrganisation().getSurveillanceEntity();
		if (surveillance!=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.PUBLICATION_FICHE)) {
			this.surveillanceMailer.sendSurveillanceMail(espaceOrganisationEntity.getOrganisation(), espaceOrganisationEntity, SurveillanceOrganisationEnum.PUBLICATION_FICHE,new Activite(savedPublication.getActivite()));
		}
	}

	/**
	 * Mise à jour du statut d'une publication
	 *
	 * @param savedPublication
	 *
	 * @param activiteId
	 *            id de l'activite à mettre à jour.
	 * @return l'entité de l'activite mis à jour.
	 */
	protected void updateFlagDePublicationTrue(final ActiviteRepresentationInteretEntity activite, PublicationActiviteEntity savedPublication) {

		if ((StatutPublicationEnum.MAJ_NON_PUBLIEE).equals(activite.getStatut())) {
			activite.setStatut(StatutPublicationEnum.MAJ_PUBLIEE);
			savedPublication.setStatut(StatutPublicationEnum.MAJ_PUBLIEE);
		} else {
			activite.setStatut(StatutPublicationEnum.PUBLIEE);
			savedPublication.setStatut(StatutPublicationEnum.PUBLIEE);
		}

		activite.setDataToPublish(Boolean.FALSE);

	}
	/**
	 * Notifier tous les publicateurs et adminis de l'espace organisation du processus de publication.
	 *
	 * @param activite
	 *            DTO de la publication.
	 * @param espaceOrganisationId
	 *            Id de l'espace organisation
	 */
	private void envoyerUnEmailAuxPublicateursDeLespaceOrganisation(final PublicationActiviteEntity savedPublication, final ActiviteRepresentationInteretEntity activite,
			final Long espaceOrganisationId, final boolean alreadyPublished) {
		// Lister les destinataires (publicateurs et administrateurs actifs de l'espace).
		List<DeclarantDto> listeDesPublicateursEtAdmins = this.inscriptionEspaceRepository.findAllByActifTrueAndDateSuppressionIsNullAndEspaceOrganisationId(espaceOrganisationId).stream()
				.filter(i -> (i.getRolesFront().contains(RoleEnumFrontOffice.PUBLICATEUR) || i.getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR)))
				.map(i -> this.declarantTransformer.modelToDto(i.getDeclarant())).collect(Collectors.toList());

		this.publicationMailer.envoyerNotificationDePublicationActivite(savedPublication, activite, listeDesPublicateursEtAdmins, alreadyPublished);
	}

	@Override
	@Transactional
	public void depublierActivite(Long id, Long userId) {
		Optional.ofNullable(this.activiteRepository.findOne(id)).ifPresent(a -> {
			a.setPublicationEnabled(false);
			a.setStatut(StatutPublicationEnum.DEPUBLIEE);
			a.getExerciceComptable().getEspaceOrganisation().setNewPublication(Boolean.TRUE);
			this.activiteRepository.save(a);
			this.espaceOrganisationRepository.save(a.getExerciceComptable().getEspaceOrganisation());

		});
		LOGGER.info("Dépublication activité : {}", id);
	}

	@Override
	@Transactional
	public void republierActivite(Long id, Long userId) {
		Optional.ofNullable(this.activiteRepository.findOne(id)).ifPresent(a -> {
			a.setPublicationEnabled(true);
			if(StatutPublicationEnum.NON_PUBLIEE.equals(a.getStatut())) {
				//cas republication en masse donc certaines jamais publiées
				a.setStatut(StatutPublicationEnum.PUBLIEE);
			}else {
				a.setStatut(StatutPublicationEnum.REPUBLIEE);
			}			
			a.getExerciceComptable().getEspaceOrganisation().setNewPublication(Boolean.TRUE);
			this.activiteRepository.save(a);
			this.espaceOrganisationRepository.save(a.getExerciceComptable().getEspaceOrganisation());
		});
		LOGGER.info("Republication activité : {}", id);
	}

	@Override
	public void republierListActivite(Long[] activiteIdList, Long userId) {
		LOGGER.info("Début republication en masse activités : {}", activiteIdList);
		for (int i=0; i<activiteIdList.length; i++) {
			this.republierActivite(activiteIdList[i], userId);
		}
		
	}
	
	@Override
	public void depublierListActivite(Long[] activiteIdList, Long userId) {
		LOGGER.info("Début dépublication en masse activités : {}", activiteIdList);
		for (int i=0; i<activiteIdList.length; i++) {
			this.depublierActivite(activiteIdList[i], userId);
		}

	}

}
