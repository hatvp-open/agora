

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.CommonDirigeantTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.DirigeantTransformer;
import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.persistence.entity.espace.DirigeantEntity;

/**
 * Classe de transformation de la classe {@link DirigeantEntity} &
 * {@link DirigeantDto}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class DirigeantTransformerImpl
    extends CommonDirigeantTransformerImpl<DirigeantEntity>
    implements DirigeantTransformer
{
    /** Init class.*/
    public DirigeantTransformerImpl() {
        super.entityClass = DirigeantEntity.class;
    }
}
