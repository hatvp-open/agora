/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice.comment;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.backoffice.comment.CommentDemandeChangementMandantBoService;
import fr.hatvp.registre.business.transformer.backoffice.comment.CommentDemandeChangementMandantBoTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentDemandeChangementMandatEntity;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentDemandeChangementMandantRepository;

/**
 * classe d'implémentation de gestion des commentaires des demandes.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CommentDemandeChangementMandantServiceImpl extends CommonCommentServiceImpl<CommentBoDto, CommentDemandeChangementMandatEntity>
		implements CommentDemandeChangementMandantBoService {

	/**
	 * Repository de la classe.
	 */
	@Autowired
	private CommentDemandeChangementMandantRepository repository;

	/**
	 * Transformeur de la classe.
	 */
	@Autowired
	private CommentDemandeChangementMandantBoTransformer commentDemandeBoTransformer;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CommentBoDto> getAllComment(final Long demandeId) {
		return this.repository.findByDemandeChangementMandantEntityId(demandeId).stream().map(this.commentDemandeBoTransformer::commentModelToDto).sorted(Comparator.comparing(CommentBoDto::getDateModification)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public CommentBoDto addComment(final CommentBoDto commentBoDto) {
		final CommentDemandeChangementMandatEntity entity = this.getTransformer().dtoToModel(commentBoDto);
		entity.setDateCreation(new Date());
		return this.commentDemandeBoTransformer.commentModelToDto(this.repository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public CommentBoDto updateComment(CommentBoDto commentBoDto) {
		final CommentDemandeChangementMandatEntity entityT = this.getTransformer().dtoToModel(commentBoDto);
		final CommentDemandeChangementMandatEntity entityR = this.getRepository().findOne(commentBoDto.getId());
		entityT.setEditeur(entityR.getEditeur());
		entityT.setDateCreation(entityR.getDateCreation());
		entityT.setDateModification(new Date());
		return this.commentDemandeBoTransformer.commentModelToDto(this.repository.save(entityT));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<CommentDemandeChangementMandatEntity, Long> getRepository() {
		return this.repository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<CommentBoDto, CommentDemandeChangementMandatEntity> getTransformer() {
		return this.commentDemandeBoTransformer;
	}

}
