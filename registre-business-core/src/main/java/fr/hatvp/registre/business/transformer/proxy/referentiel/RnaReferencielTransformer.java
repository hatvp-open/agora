/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.referentiel;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielRnaEntity;


/**
 * Proxy du transformeur entre {@link OrganisationDto} et {@link ReferentielRnaEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface RnaReferencielTransformer
        extends CommonTransformer<OrganisationDto, ReferentielRnaEntity>
{

}