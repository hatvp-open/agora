/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.DesinscriptionPieceDto;
import fr.hatvp.registre.persistence.entity.piece.DesinscriptionPieceEntity;

public interface DesinscriptionPieceTransformer extends CommonTransformer<DesinscriptionPieceDto, DesinscriptionPieceEntity> {

}
