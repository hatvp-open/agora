/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.common;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.exceptions.GlobalServerException;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;

/**
 * Classe abstraite de traitement des transformations de l'application.
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional(readOnly = true)
public abstract class AbstractCommonTansformer<T extends AbstractCommonDto, E extends AbstractCommonEntity> implements CommonTransformer<T, E> {
	/**
	 * {@inheritDoc}.
	 */
	@Override
	public List<E> dtoToModel(final List<T> dtos) {
		final List<E> models = new ArrayList<>();
		if ((dtos != null) && !dtos.isEmpty()) {
			dtos.forEach(dto -> models.add(this.dtoToModel(dto)));
		}
		return models;
	}

	/**
	 * {@inheritDoc}.
	 */
	@Override
	public List<T> modelToDto(final List<E> models) {
		final List<T> dtos = new ArrayList<>();
		if ((models != null) && !models.isEmpty()) {
			models.forEach(model -> dtos.add(this.modelToDto(model)));
		}
		return dtos;
	}

	/** {@inheritDoc}. */
	@Override
	public E dtoToModel(final T dto) {
		final ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
		final String className = type.getActualTypeArguments()[1].getTypeName();

		final E model;

		try {
			final Class<E> clazz = (Class<E>) Class.forName(className);
			model = clazz.newInstance();
		} catch (final ClassNotFoundException | InstantiationException | IllegalAccessException exception) {
			throw new GlobalServerException("Erreur lors de l'instantiation de l'entité Java par réflexion.", exception);
		}
		if (dto != null) {
			RegistreBeanUtils.copyProperties(model, dto);
		}
		return model;
	}

	/** {@inheritDoc}. */
	@Override
	public T modelToDto(final E model) {
		final ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
		final String className = type.getActualTypeArguments()[0].getTypeName();

		final T dto;
		try {
			final Class<T> clazz = (Class<T>) Class.forName(className);
			dto = clazz.newInstance();
		} catch (final ClassNotFoundException | InstantiationException | IllegalAccessException exception) {
			throw new GlobalServerException("Erreur lors de l'instantiation du dto Java par réflexion.", exception);
		}

		if (model != null) {
			// FIXME : c'est l'inverse ici on écrase les propriétés de l'entité par un objet vide !!
			// RegistreBeanUtils.copyProperties(model, dto);
			// FIXED comme ci-dessous => tous les tests passent, et là ça m'arrange donc on fait quoi ? => Je dirais que c'est bon comme ça...
			RegistreBeanUtils.copyProperties(dto, model);
		}
		return dto;
	}

	/**
	 * {@inheritDoc}
	 */
	// TODO Refactoring
	@Override
	public String determineIdentifiantNational(final OrganisationEntity model) {
		String identifiantNational = "";
		switch (model.getOriginNationalId()) {
		case SIREN:
			identifiantNational = model.getSiren();
			break;
		case HATVP:
			identifiantNational = model.getHatvpNumber();
			break;
		case RNA:
			identifiantNational = model.getRna();
			break;
		default:
			break;
		}

		return identifiantNational;
	}
}
