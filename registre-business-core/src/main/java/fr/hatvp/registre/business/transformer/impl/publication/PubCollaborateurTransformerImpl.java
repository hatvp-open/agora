/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.CommonCollaborateurTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.publication.PubCollaborateurTransformer;
import fr.hatvp.registre.persistence.entity.espace.CollaborateurEntity;
import fr.hatvp.registre.persistence.entity.publication.PubCollaborateurEntity;

/**
 * Classe de transformation de la classe {@link CollaborateurEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class PubCollaborateurTransformerImpl
    extends CommonCollaborateurTransformerImpl<PubCollaborateurEntity>
    implements PubCollaborateurTransformer
{

    /** Init class. */
    public PubCollaborateurTransformerImpl()
    {
        super.entityClass = PubCollaborateurEntity.class;
    }
}