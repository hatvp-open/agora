/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.activite;

import fr.hatvp.registre.commons.dto.backoffice.activite.FicheBackDto;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;

public interface FicheBOTransformer {

	FicheBackDto modelToDto(ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity);

}
