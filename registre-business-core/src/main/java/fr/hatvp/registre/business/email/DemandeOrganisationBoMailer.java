/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.email;

import fr.hatvp.registre.commons.dto.DeclarantDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Locale;

/**
 * Classe d'envoi de mails pour les demandes de changement de mandats
 * 
 * @version $Revision$ $Date${0xD}
 **/
@Component
public class DemandeOrganisationBoMailer
    extends ApplicationMailer
{

    @Autowired
    public DemandeOrganisationBoMailer(final JavaMailSender sender,
            final TemplateEngine templateEngine)
    {
        super(sender, templateEngine);
    }


    
    /** Objet du mail de demande de complément . */
    @Value("${mail.bo.demandeOrganisation.complement.subject}")
    private String espaceComplementSubject;


    /** Template de demande de complément pour un espace. */
    private static final String templateComplement = "email/espace_complement";

    @Value("${app.front.root}")
    private String frontAppLink;




    /**
     * Envoi de mail de demande de complément 
     *
     * @param to email destinataire
     */
    public void sendDemandeComplementEspaceEmail(final DeclarantDto to, final String message,
            final String denomination)
    {
        this.prepareAndSendEspaceEmail(to,
                StringUtils.replace(this.espaceComplementSubject, "[DENOMINATION]", denomination),
                denomination, templateComplement, null, message);
    }
    
    /**
     * Préparer et envoyer le mail.
     */
    private void prepareAndSendEspaceEmail(final DeclarantDto to, final String object, final  String denomination,
                                           final String template, final String link, final String message)
    {

        final Context ctx = new Context(Locale.FRANCE);
        ctx.setVariable("civilite", to.getCivility().getLibelleLong());
        ctx.setVariable("denomination", denomination);
        ctx.setVariable("message", message);
        ctx.setVariable("link", link);
        ctx.setVariable("imageResourceName", this.imageResourceName);

        final String mailContent = this.templateEngine.process(template, ctx);
        super.prepareAndSendEmailWhithLogo(to.getEmail(), object, mailContent);
    }

}
