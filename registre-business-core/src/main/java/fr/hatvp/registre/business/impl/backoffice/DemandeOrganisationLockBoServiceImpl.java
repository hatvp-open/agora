

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice;

import fr.hatvp.registre.business.backoffice.DemandeOrganisationLockBoService;
import fr.hatvp.registre.business.impl.AbstractCommonService;
import fr.hatvp.registre.business.transformer.backoffice.DemandeOrganisationLockBoTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.DemandeOrganisationLockBoDto;
import fr.hatvp.registre.persistence.entity.backoffice.DemandeOrganisationLockBoEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;
import fr.hatvp.registre.persistence.repository.backoffice.DemandeOrganisationLockBoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service de gestion des locks sur les demandes d'organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional(readOnly = true)
@Service
public class DemandeOrganisationLockBoServiceImpl
    extends AbstractCommonService<DemandeOrganisationLockBoDto, DemandeOrganisationLockBoEntity>
    implements DemandeOrganisationLockBoService {
    /** Repository du service. */
    @Autowired
    private DemandeOrganisationLockBoRepository demandeOrganisationLockBoRepository;

    /** Transformer du service */
    @Autowired
    private DemandeOrganisationLockBoTransformer demandeOrganisationLockBoTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    public DemandeOrganisationLockBoDto getLockOn(final Long demandeOrganisationId) {
        final List<DemandeOrganisationLockBoDto> result = this.demandeOrganisationLockBoRepository
            .findByDemandeOrganisationId(demandeOrganisationId).stream()
            .map(this.demandeOrganisationLockBoTransformer::modelToDto)
            .filter(dto -> dto.getLockTimeRemain() > 0).collect(Collectors.toList());

        if (!result.isEmpty()) {
            return result.get(0);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void setLockOn(final Long demandeOrganisationId, final Long utilisateurBoId) {
        final DemandeOrganisationLockBoEntity demandeOrganisationLockBoEntity = new DemandeOrganisationLockBoEntity();

        final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
        utilisateurBoEntity.setId(utilisateurBoId);
        demandeOrganisationLockBoEntity.setUtilisateurBo(utilisateurBoEntity);

        demandeOrganisationLockBoEntity.setLockedTime(new Date());

        final DemandeOrganisationEntity demandeOrganisationEntity = new DemandeOrganisationEntity();
        demandeOrganisationEntity.setId(demandeOrganisationId);
        demandeOrganisationLockBoEntity.setDemandeOrganisation(demandeOrganisationEntity);

        this.demandeOrganisationLockBoRepository.save(demandeOrganisationLockBoEntity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public DemandeOrganisationLockBoDto updateLockOnDemande(final DemandeOrganisationLockBoDto dto,
                                                            final Long connectedUserId) {
        final List<DemandeOrganisationLockBoEntity> result = this.demandeOrganisationLockBoRepository
            .findByDemandeOrganisationId(dto.getDemandeOrganisationId());

        if (!result.isEmpty()) {
            final DemandeOrganisationLockBoEntity entity = result.get(0);
            entity.setVersion(dto.getVersion());
            entity.setLockedTime(new Date());
            return this.demandeOrganisationLockBoTransformer
                .modelToDto(this.demandeOrganisationLockBoRepository.save(entity));

        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JpaRepository<DemandeOrganisationLockBoEntity, Long> getRepository() {
        return this.demandeOrganisationLockBoRepository;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommonTransformer<DemandeOrganisationLockBoDto, DemandeOrganisationLockBoEntity> getTransformer() {
        return this.demandeOrganisationLockBoTransformer;
    }
}
