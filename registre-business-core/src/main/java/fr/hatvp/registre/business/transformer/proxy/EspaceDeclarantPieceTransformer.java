/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.EspaceDeclarantPieceDto;
import fr.hatvp.registre.persistence.entity.piece.EspaceDeclarantPieceEntity;

/**
 * Interface pour le transformer des pieces du déclarants.
 * 
 * @version $Revision$ $Date${0xD}
 */
public interface EspaceDeclarantPieceTransformer
    extends CommonTransformer<EspaceDeclarantPieceDto, EspaceDeclarantPieceEntity>
{
    
}
