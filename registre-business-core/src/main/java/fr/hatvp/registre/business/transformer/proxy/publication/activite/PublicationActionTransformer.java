/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication.activite;

import java.util.List;
import java.util.stream.Stream;

import fr.hatvp.registre.commons.dto.publication.activite.PublicationActionDto;
import fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity;

/**
 * Proxy du transformeur entre {@link fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity} et {@link fr.hatvp.registre.commons.dto.publication.activite.PublicationActionDto}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationActionTransformer
{

    /**
     * Map une entité d'actions vers un dto de publication d'action
     *
     * @param action entité d'action à transformer.
     * @return une instance de {@link PublicationActionDto} contenant les données de l'action.
     */
    PublicationActionDto actionToPublicationAction(final ActionRepresentationInteretEntity action);

    /**
     * Map une liste d'entité d'actions vers une liste de dto de publication d'action
     *
     * @param actions liste dtos d'actions à transformer.
     * @return une instance de {@link List<PublicationActionDto>} contenant les données de l'action.
     */
    List<PublicationActionDto> actionToPublicationAction(final List<ActionRepresentationInteretEntity> actions);

    /**
     * Map un stream d'entité d'actions vers une liste de dto de publication d'action
     *
     * @param actions liste entité d'actions à transformer.
     * @return une instance de {@link List<PublicationActionDto>} contenant les données de l'action.
     */
    List<PublicationActionDto> actionToPublicationAction(final Stream<ActionRepresentationInteretEntity> actions);

}
