/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.activite.batch;

import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.proxy.activite.batch.DeclarationReminderTransformer;
import fr.hatvp.registre.commons.dto.activite.batch.ContactOpMailingDto;
import fr.hatvp.registre.commons.dto.activite.batch.DeclarationReminderDto;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;

@Component
public class DeclarationReminderTransformerImpl implements DeclarationReminderTransformer {

	@Override
	public DeclarationReminderDto modelToDto(ExerciceComptableEntity entity) {
		DeclarationReminderDto dto = new DeclarationReminderDto();

		dto.setEspaceOrganisationId(entity.getEspaceOrganisation().getId());
		dto.setExerciceCompatbleId(entity.getId());
		dto.setDenomination(entity.getEspaceOrganisation().getOrganisation().getDenomination());
		dto.setDateDebut(entity.getDateDebut());
		dto.setDateFin(entity.getDateFin());

		dto.setNombreFiche(entity.getActivitesRepresentationInteret().stream().count());

		dto.setContactOp(entity.getEspaceOrganisation().getListeDesCollaborateurs().stream().map(col -> {
			if (col.getDeclarant()!=null) {
				return col.getDeclarant().getInscriptionEspaces().stream().filter(ins -> ins.getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR)
						&& ins.getEspaceOrganisation().getId().longValue() == entity.getEspaceOrganisation().getId().longValue()).map(ins -> {
							ContactOpMailingDto contactOp = new ContactOpMailingDto();
							contactOp.setId(ins.getDeclarant().getId());
							contactOp.setCivilite(ins.getDeclarant().getCivility());
							contactOp.setNom(ins.getDeclarant().getNom());
							contactOp.setPrenom(ins.getDeclarant().getPrenom());
							contactOp.setEmail(ins.getDeclarant().getEmail());
							return contactOp;
						}).collect(Collectors.toList());
			}
			return null;
		}).filter(Objects::nonNull).flatMap(x -> x.stream()).collect(Collectors.toList()));

		return dto;
	}
}
