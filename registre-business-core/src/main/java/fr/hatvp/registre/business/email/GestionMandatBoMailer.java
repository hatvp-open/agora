/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.email;

import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.persistence.entity.espace.DemandeChangementMandantEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Classe d'envoi de mails pour les demandes de changement de mandats
 * 
 * @version $Revision$ $Date${0xD}
 **/
@Component
public class GestionMandatBoMailer
    extends ApplicationMailer
{

    @Autowired
    public GestionMandatBoMailer(final JavaMailSender sender,
            final TemplateEngine templateEngine)
    {
        super(sender, templateEngine);
    }

    /** Objet du mail de validation des espaces organisations. */
    @Value("${mail.bo.changementMandant.validation.subject}")
    private String espaceValidationSubject;

    /** Objet du mail de refus de validation des espaces organisations. */
    @Value("${mail.bo.changementMandant.refus.subject}")
    private String espaceRejetSubject;
    
    /** Objet du mail de demande de complément pour espace collaboratif. */
    @Value("${mail.bo.changementMandant.complement.subject}")
    private String espaceComplementSubject;

    /** Objet du mail de validation des espaces organisations. */
    @Value("${mail.bo.changementMandant.validation.message}")
    private String validationMessage;
    
    /** Objet du mail de validation des espaces organisations. */
    @Value("${mail.bo.changementMandant.refus.message}")
    private String refusMessage;
    
    /** Template de refus de création d'espace. */
    private static final String templateRefuse = "email/espace_refus";
    /** Template de validation de création d'espace. */
    private static final String templateMandat = "email/changementMandant_template";
    /** Template de demande de complément pour un espace. */
    private static final String templateComplement = "email/espace_complement";
    
    private static final String DENOMINATION = "[DENOMINATION]";
    private static final String TYPE = "[TYPE]";

    @Value("${app.front.root}")
    private String frontAppLink;




    /**
     * Envoi de mail de demande de complément pour la création de l'espace organisation
     *
     * @param to email destinataire
     */
    public void sendDemandeComplementEspaceEmail(final DeclarantDto to, final String message,
            final String denomination)
    {
        this.prepareAndSendEspaceEmail(to,
                StringUtils.replace(this.espaceComplementSubject, DENOMINATION, denomination),
                denomination, templateComplement, null, message);
    }
    
    /**
     * Préparer et envoyer le mail.
     */
    private void prepareAndSendEspaceEmail(final DeclarantDto to, final String object, final  String denomination,
                                           final String template, final String link, final String message)
    {

        final Context ctx = new Context(Locale.FRANCE);
        ctx.setVariable("civilite", to.getCivility().getLibelleLong());
        ctx.setVariable("denomination", denomination);
        ctx.setVariable("message", message);
        ctx.setVariable("link", link);
        ctx.setVariable("imageResourceName", this.imageResourceName);

        final String mailContent = this.templateEngine.process(template, ctx);
        super.prepareAndSendEmailWhithLogo(to.getEmail(), object, mailContent);
    }
    
    /**
     * Envoi de mail de confirmation de création de l'espace organisation
     * @param declarantDto 
     *
     * @param denomination raison sociale
     * @param to email destinataire
     */
    public void sendValidationEspaceEmail(DeclarantDto declarantDto, DemandeChangementMandantEntity demande)
    {
    	String denomination =  demande.getEspaceOrganisation().getOrganisation().getDenomination();
    	String message = StringUtils.replace(validationMessage, "[DATE]", new SimpleDateFormat("dd/MM/yyyy à HH:mm").format(demande.getDateDemande()));
    	message = this.replaceTypeDemande(demande,message);
        final String subject;
        final String link = frontAppLink;
        subject = StringUtils.replace(this.espaceValidationSubject, DENOMINATION,denomination);
        this.prepareAndSendEspaceEmail(declarantDto, subject, denomination, templateMandat, link, message);
    }



	/**
     * Envoi de mail de refus de création de l'espace organisation
     *
     * @param to email destinataire
     */
    public void sendRefusEspaceEmail(final DeclarantDto to, final DemandeChangementMandantEntity demande)
    {
    	String denomination =  demande.getEspaceOrganisation().getOrganisation().getDenomination();
    	String message = StringUtils.replace(refusMessage, "[DATE]", new SimpleDateFormat("dd/MM/yyyy à HH:mm").format(demande.getDateDemande()));
    	message = this.replaceTypeDemande(demande,message);
        this.prepareAndSendEspaceEmail(to,StringUtils.replace(this.espaceRejetSubject, DENOMINATION, denomination),
        		denomination, templateMandat, null, message);
    }
    
    private String replaceTypeDemande(DemandeChangementMandantEntity demande, String message) {
		switch (demande.getTypeDemande()) {
		case CHANGEMENT_REPRESENTANT:
			return StringUtils.replace(message,TYPE,"de changement de représentant légal");
		case AJOUT_OPERATIONNEL:
			return StringUtils.replace(message,TYPE,"d'ajout de contact opérationnel");
		case REMPLACEMENT_OPERATIONNEL:
			return StringUtils.replace(message,TYPE,"de remplacement de contact opérationnel");
		default:
			break;
		}
		return message;
	}


}
