/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.CommonClientTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.ClientTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceCollaboratifTransformer;
import fr.hatvp.registre.business.transformer.proxy.PeriodeActiviteClientTransformer;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.persistence.entity.espace.ClientEntity;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.PeriodeActiviteClientEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.publication.PubClientRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;

/**
 * Classe de transformation de la classe {@link ClientEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class ClientTransformerImpl
		extends CommonClientTransformerImpl<ClientEntity>
		implements ClientTransformer {
	
	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;
	
	@Autowired
	private EspaceCollaboratifTransformer espaceCollaboratifTransformer;
	
	@Autowired
	private PublicationRepository publicationRepository;
	
	@Autowired
	private PeriodeActiviteClientTransformer periodeActiviteClientTransformer;
	
	@Autowired
	private DesinscriptionRepository desinscriptionRepository;

	@Autowired
	private PubClientRepository pubClientRepository;
	
	
	/** Init superclass entity. */
	public ClientTransformerImpl() {
		super.entityClass = ClientEntity.class;
	}

	@Override
	public AssoClientDto modelToDto(ClientEntity model) {
		AssoClientDto assoClientDto = new AssoClientDto();
		assoClientDto.setId(model.getId());
		assoClientDto.setVersion(model.getVersion());
		assoClientDto.setOrganisationId(model.getOrganisation().getId());
		
//		on récupère l'espace ou l'espace non refusé quand il y en a plusieurs
		EspaceOrganisationEntity espaceDeLorganisation = espaceOrganisationRepository.findByOrganisationIdUnSeulEspace(model.getOrganisation().getId());
		if(espaceDeLorganisation != null) {
			assoClientDto.setEspaceOrganisationIdDeOrdganisation(espaceDeLorganisation.getId());
			assoClientDto.setEspaceCollaboratifDto(this.espaceCollaboratifTransformer.modelToDto(espaceDeLorganisation));
			assoClientDto.setNomUsage(espaceDeLorganisation.getNomUsage());
		}else {
			assoClientDto.setNomUsage(model.getOrganisation().getNomUsageSiren());
		}
		
		assoClientDto.setEspaceOrganisationId(model.getEspaceOrganisation().getId());
		assoClientDto.setDenomination(model.getOrganisation().getDenomination());
		
		
		if(TypeIdentifiantNationalEnum.SIREN.equals(model.getOrganisation().getOriginNationalId())) {
			assoClientDto.setNationalId(model.getOrganisation().getSiren());
		}else if(TypeIdentifiantNationalEnum.RNA.equals(model.getOrganisation().getOriginNationalId())) {
			assoClientDto.setNationalId(model.getOrganisation().getRna());
		}else if(TypeIdentifiantNationalEnum.HATVP.equals(model.getOrganisation().getOriginNationalId())) {
			assoClientDto.setNationalId(model.getOrganisation().getHatvpNumber());
		}
		
		assoClientDto.setOriginNationalId(model.getOrganisation().getOriginNationalId());
		
		List<PeriodeActiviteClientEntity> periodeActiviteClientEntityListeTriee = model.getPeriodeActiviteClientList().stream().sorted(Comparator.comparing(PeriodeActiviteClientEntity::getDateAjout).reversed()).collect(Collectors.toList());
		
		if(periodeActiviteClientEntityListeTriee != null) {
			assoClientDto.setPeriodeActiviteClientEnCours(this.periodeActiviteClientTransformer.modelToDto(periodeActiviteClientEntityListeTriee.get(0)));
			assoClientDto.setDateAjout(periodeActiviteClientEntityListeTriee.get(periodeActiviteClientEntityListeTriee.size() -1).getDateAjout());
		}		
		
		assoClientDto.setPeriodeActiviteClientList(this.periodeActiviteClientTransformer.modelToDto(model.getPeriodeActiviteClientList()));		
		
		assoClientDto.setDateDesactivation(assoClientDto.getPeriodeActiviteClientEnCours().getDateFinContrat() != null ? assoClientDto.getPeriodeActiviteClientEnCours().getDateFinContrat().atTime(00, 00, 00) : assoClientDto.getPeriodeActiviteClientEnCours().getDateDesactivation());
		assoClientDto.setAncienClient(assoClientDto.getPeriodeActiviteClientEnCours().getDateDesactivation() != null);
		
		PublicationEntity premierePublicationClient = this.publicationRepository.findPremierePublicationClient(model.getEspaceOrganisation().getId(), model.getOrganisation().getId());
	
		if(premierePublicationClient != null) assoClientDto.setDatePremierePublication(premierePublicationClient.getCreationDate());
		
		PublicationEntity dernierePublicationClient = this.publicationRepository.findLastPublicationClient(model.getEspaceOrganisation().getId(), model.getOrganisation().getId());
		
		if(dernierePublicationClient != null) assoClientDto.setDateDernierePublication(dernierePublicationClient.getCreationDate());
		
		EspaceOrganisationEntity espaceClientAssoEntity = espaceOrganisationRepository.findByOrganisationId(model.getOrganisation().getId());
		
		if(espaceClientAssoEntity != null) {
			DesinscriptionEntity desinscriptionEntity = desinscriptionRepository.findByEspaceOrganisation(espaceClientAssoEntity);
			
			if(desinscriptionEntity != null) assoClientDto.setDateCessation(Date.from(desinscriptionEntity.getCessationDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));
			
		}

		return assoClientDto;
	}

	@Override
	public List<AssoClientDto> modelToDto(List<ClientEntity> models) {
		List<AssoClientDto> list = new ArrayList<>();
		
		for(ClientEntity model : models) {
			AssoClientDto assoClientDto = modelToDto(model);
			list.add(assoClientDto);		
		}		
		return list;
	}
	
	
}
