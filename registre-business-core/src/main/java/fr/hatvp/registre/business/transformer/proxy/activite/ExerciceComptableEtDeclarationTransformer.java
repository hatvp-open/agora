/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.activite;

import java.util.List;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableEtDeclarationDto;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;

public interface ExerciceComptableEtDeclarationTransformer extends CommonTransformer<ExerciceComptableEtDeclarationDto, ExerciceComptableEntity> {
	List<ExerciceComptableEtDeclarationDto> modelToDtoSansActiviteSupprimee(List<ExerciceComptableEntity> entities);
	/**
	 * @param entity
	 * @return
	 */
	ExerciceComptableEtDeclarationDto modelToDtoSansActiviteSupprimee(ExerciceComptableEntity entity);

}
