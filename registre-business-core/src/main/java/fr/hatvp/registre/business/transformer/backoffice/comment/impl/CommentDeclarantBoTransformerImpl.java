/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment.impl;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.comment.CommentDeclarantBoTransformer;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentDeclarantEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;

/**
 * Classe de transformation de la classe {@link CommentDeclarantEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CommentDeclarantBoTransformerImpl extends AbstractCommentBoTransformerImpl<CommentBoDto, CommentDeclarantEntity> implements CommentDeclarantBoTransformer {

	/**
	 * Repository des déclarants.
	 */
	@Autowired
	private DeclarantRepository declarantRepository;

	/** Constructeur de la classe. */
	public CommentDeclarantBoTransformerImpl() {
		super.entityClass = CommentDeclarantEntity.class;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	void dtoToModelComplement(final CommentBoDto dto, final CommentDeclarantEntity commentDeclarantEntity) {
		// Ajout du déclatant commenté.
		final DeclarantEntity declarantEntity = Optional.ofNullable(this.declarantRepository.findOne(dto.getContextId()))
				.orElseThrow(() -> new EntityNotFoundException("Declarant id introuvable"));
		commentDeclarantEntity.setDeclarantEntity(declarantEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	void modelToDtoComplement(final CommentDeclarantEntity model, final CommentBoDto commentBoDto) {
		commentBoDto.setContextId(model.getDeclarantEntity().getId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentDeclarantEntity dtoToModel(CommentBoDto dto) {
		return super.dtoToModel(dto);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentBoDto commentModelToDto(CommentDeclarantEntity model) {
		return super.commentModelToDto(model);
	}
}