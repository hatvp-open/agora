/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;

@Component
public class CsvGeneratorUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(CsvGeneratorUtil.class);


	public File createCsv(final String templateName, final Map<String, Object> context) {
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator('\n');
		File csvFile;
		CSVPrinter csvFilePrinter;
		FileWriter fs;
		try {
			csvFile = File.createTempFile("blacklistes", ".csv");
			fs = new FileWriter(csvFile);
			csvFilePrinter = new CSVPrinter(fs, csvFileFormat);
			csvFilePrinter.printRecord(context.keySet().toArray());
			csvFilePrinter.printRecord(context.values().toArray());
	        csvFilePrinter.close();
	        fs.close();
		} catch (IOException e) {
			LOGGER.error("Echec de génération du csv : {}", e);
			throw new BusinessGlobalException(ErrorMessageEnum.ECHEC_GENERATION_CSV.getMessage());
		}

		return csvFile;
	}

}
