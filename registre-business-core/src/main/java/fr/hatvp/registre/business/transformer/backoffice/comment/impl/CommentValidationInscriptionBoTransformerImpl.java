/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment.impl;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.comment.CommentValidationInscriptionBoTransformer;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentValidationEspaceEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;

/**
 *
 * Classe de transformation de la classe {@link CommentValidationEspaceEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CommentValidationInscriptionBoTransformerImpl extends AbstractCommentBoTransformerImpl<CommentBoDto, CommentValidationEspaceEntity>
		implements CommentValidationInscriptionBoTransformer {

	/**
	 * Repository des déclarants.
	 */
	@Autowired
	private InscriptionEspaceRepository inscriptionEspaceRepository;

	/**
	 * Constructeur de la classe.
	 */
	public CommentValidationInscriptionBoTransformerImpl() {
		super.entityClass = CommentValidationEspaceEntity.class;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentValidationEspaceEntity dtoToModel(CommentBoDto dto) {
		return super.dtoToModel(dto);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentBoDto commentModelToDto(CommentValidationEspaceEntity model) {
		return super.commentModelToDto(model);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	void dtoToModelComplement(final CommentBoDto dto, final CommentValidationEspaceEntity commentDeclarantEntity) {
		final InscriptionEspaceEntity declarantEntity = Optional.ofNullable(this.inscriptionEspaceRepository.findOne(dto.getContextId()))
				.orElseThrow(() -> new EntityNotFoundException("Inscription id introuvable"));
		commentDeclarantEntity.setInscriptionEspaceEntity(declarantEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	void modelToDtoComplement(final CommentValidationEspaceEntity model, final CommentBoDto commentBoDto) {
		commentBoDto.setContextId(model.getInscriptionEspaceEntity().getId());
	}
}
