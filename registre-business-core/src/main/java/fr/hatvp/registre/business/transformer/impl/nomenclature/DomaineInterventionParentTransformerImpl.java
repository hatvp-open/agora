/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.nomenclature;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.proxy.nomenclature.DomaineInterventionParentTransformer;
import fr.hatvp.registre.commons.dto.nomenclature.DomaineInterventionDto;
import fr.hatvp.registre.commons.dto.nomenclature.DomaineInterventionParentDto;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;

@Component
public class DomaineInterventionParentTransformerImpl
		extends AbstractCommonNomenclatureParentTransformer<DomaineInterventionDto, DomaineInterventionParentDto, DomaineInterventionEntity>
		implements DomaineInterventionParentTransformer {

	/** Init super DTO. */
	public DomaineInterventionParentTransformerImpl() {
		super.dtoClass = DomaineInterventionParentDto.class;
	}
}
