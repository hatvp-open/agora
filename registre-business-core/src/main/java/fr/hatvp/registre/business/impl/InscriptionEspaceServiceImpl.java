/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import fr.hatvp.registre.business.email.DesinscriptionMailer;
import fr.hatvp.registre.business.transformer.proxy.*;
import fr.hatvp.registre.commons.dto.*;
import fr.hatvp.registre.commons.lists.*;
import fr.hatvp.registre.persistence.entity.espace.*;
import fr.hatvp.registre.persistence.entity.piece.DesinscriptionPieceEntity;
import fr.hatvp.registre.persistence.repository.espace.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.InscriptionEspaceService;
import fr.hatvp.registre.business.email.OrganisationEspaceMailer;
import fr.hatvp.registre.business.email.SurveillanceMailer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.piece.EspaceOrganisationPieceEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;

/**
 * Implémentation du service {@link InscriptionEspaceService}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
@Transactional(readOnly = true)
public class InscriptionEspaceServiceImpl extends AbstractCommonService<InscriptionEspaceDto, InscriptionEspaceEntity> implements InscriptionEspaceService {

	/** Inscription espace repository. */
	@Autowired
	private InscriptionEspaceRepository inscriptionEspaceRepository;

	/** Inscription espace transformer. */
	@Autowired
	private InscriptionEspaceTransformer inscriptionEspaceTransformer;

	/** Repository des declarants. */
	@Autowired
	private DeclarantRepository declarantRepository;

	/** Repository des organisations. */
	@Autowired
	private OrganisationRepository organisationRepository;

	/** Mailer de l'espace organisation. */
	@Autowired
	private OrganisationEspaceMailer organisationEspaceMailer;

	/** Repository de l'espace organisation. */
	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;

	/** Repository pour les pièces de l'espace organisation. */
	@Autowired
	private EspaceOrganisationPiecesRepository espaceOrganisationPiecesRepository;

    @Autowired
    private DesinscriptionRepository desinscriptionRepository;

	/** Le transformer pour la gestion des pièces de l'espace organisation. */
	@Autowired
	private EspaceOrganisationPieceTransformer espaceOrganisationPieceTransformer;

	/** Repository pour la gestion des collaborateurs. */
	@Autowired
	private CollaborateurRepository collaborateurRepository;

	/** Transformer des declarants. */
	@Autowired
	private DeclarantTransformer declarantTransformer;

    @Autowired
    private DesinscriptionTransformer desinscriptionTransformer;

    @Autowired
    DesinscriptionPieceTransformer desinscriptionPieceTransformer;

    @Autowired
    DesinscriptionPieceRepository desinscriptionPieceRepository;

	/** Composant de mailing de surveillance. */
	@Autowired
	private SurveillanceMailer surveillanceMailer;

    /** Composant de mailing de désinscription. */
    @Autowired
    private DesinscriptionMailer desinscriptionMailer;

	@Value(value = "${espace.organisation.limite.creation}")
	private int limiteCreationEspaces;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<InscriptionEspaceEntity, Long> getRepository() {
		return this.inscriptionEspaceRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<InscriptionEspaceDto, InscriptionEspaceEntity> getTransformer() {
		return this.inscriptionEspaceTransformer;
	}

	/**
	 * {@inheritdoc}
	 */
	@Override
	public List<InscriptionEspaceDto> getAllByDeclarantId(final Long declarantId) {
		return this.inscriptionEspaceRepository.findAllByDateSuppressionIsNullAndDeclarantId(declarantId).stream().map(e -> this.inscriptionEspaceTransformer.modelToDto(e))
				.collect(Collectors.toList());
	}

	/**
	 * {@inheritdoc}
	 */
	@Override
	public List<InscriptionEspaceDto> getAllPendingRequestsByEspaceOrganisationId(final Long espaceOrganisationId) {
		return this.inscriptionEspaceRepository.findAllByDateSuppressionIsNullAndDateValidationIsNullAndEspaceOrganisationId(espaceOrganisationId).stream()
				.map(e -> this.inscriptionEspaceTransformer.modelToDto(e)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InscriptionEspaceDto> getAllResgitrationsByEspaceOrganisationId(final Long espaceOrganisationId) {
		return this.inscriptionEspaceRepository.findAllByActifTrueAndDateSuppressionIsNullAndEspaceOrganisationId(espaceOrganisationId).stream()
				.map(e -> this.inscriptionEspaceTransformer.modelToDto(e)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InscriptionEspaceDto> getAllAdminRegistrationsByEspaceOrganisationId(final Long espaceOrganisationId) {
		return this.inscriptionEspaceRepository.findAllByActifTrueAndDateSuppressionIsNullAndRolesFrontAndEspaceOrganisationId(RoleEnumFrontOffice.ADMINISTRATEUR, espaceOrganisationId)
				.stream().map(e -> this.inscriptionEspaceTransformer.modelToDto(e)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InscriptionEspaceDto> getAllNonAdminRegistrationsByEspaceOrganisationId(final Long espaceOrganisationId) {
		return this.inscriptionEspaceRepository
				.findDistinctDeclarantIdByActifTrueAndDateSuppressionIsNullAndRolesFrontNotAndEspaceOrganisationIdAndDeclarantIdNotIn(RoleEnumFrontOffice.ADMINISTRATEUR,
						espaceOrganisationId,
						this.inscriptionEspaceRepository
								.findAllByActifTrueAndDateSuppressionIsNullAndRolesFrontAndEspaceOrganisationId(RoleEnumFrontOffice.ADMINISTRATEUR, espaceOrganisationId).stream()
								.map(e -> e.getDeclarant().getId()).collect(Collectors.toList()))
				.stream().map(e -> this.inscriptionEspaceTransformer.modelToDto(e)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void setPendingRequestValidity(final Long inscriptionId, final Long currentEspaceOrganisationId, final boolean validate, final boolean envoiEmail) {
		InscriptionEspaceEntity inscriptionEspaceEntity = Optional
				.ofNullable(this.inscriptionEspaceRepository.findByDateSuppressionIsNullAndIdAndEspaceOrganisationId(inscriptionId, currentEspaceOrganisationId))
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.MSG_404.getMessage(), 404));

		// Valider demande
		if (validate) {
			// Enregistrer date validation et activer inscription
			inscriptionEspaceEntity.setDateValidation(new Date());
			inscriptionEspaceEntity.setActif(true);

			// Attribuer rôle Contributeur
			inscriptionEspaceEntity.getRolesFront().add(RoleEnumFrontOffice.CONTRIBUTEUR);

			// Devenir collaborateur de l'espace
			this.devenirCollaborateur(inscriptionEspaceEntity.getDeclarant(), inscriptionEspaceEntity.getEspaceOrganisation());

			// Envoi mail validation
			if (envoiEmail) {
				this.sendEmailValidRequest(this.declarantTransformer.modelToDto(inscriptionEspaceEntity.getDeclarant()),
						this.inscriptionEspaceTransformer.modelToDto(inscriptionEspaceEntity), inscriptionEspaceEntity.getEspaceOrganisation().getOrganisation().getDenomination());
			}
            SurveillanceEntity surveillance = inscriptionEspaceEntity.getEspaceOrganisation().getOrganisation().getSurveillanceEntity();
			if (surveillance!=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.AJOUT_COLLABORATEUR)) {
				this.surveillanceMailer.sendSurveillanceMail(inscriptionEspaceEntity.getEspaceOrganisation().getOrganisation(), inscriptionEspaceEntity.getEspaceOrganisation(),
						SurveillanceOrganisationEnum.AJOUT_COLLABORATEUR,"");
			}
		}
		// Rejeter demande
		else {
			// Enregistrer date suppression logique et désactiver inscription
			inscriptionEspaceEntity.setDateSuppression(new Date());
			inscriptionEspaceEntity.setActif(false);

			// Si Favori, choisir l'inscription faite juste après et non refusée
			// comme favori
			if (inscriptionEspaceEntity.isFavori()) {
				inscriptionEspaceEntity.setFavori(false);
				inscriptionEspaceEntity.getDeclarant().getInscriptionEspaces().stream()
						.filter(inscription -> !inscription.getId().equals(inscriptionEspaceEntity.getId()) && (inscription.getDateSuppression() == null))
						.min(Comparator.comparing(InscriptionEspaceEntity::getDateDemande)).ifPresent(inscription -> {
							inscription.setFavori(true);
							this.inscriptionEspaceRepository.save(inscription);
						});
			}

			// Envoi mail refus
			if (envoiEmail) {
				this.sendEmailRejectedRequest(this.declarantTransformer.modelToDto(inscriptionEspaceEntity.getDeclarant()),
						this.inscriptionEspaceTransformer.modelToDto(inscriptionEspaceEntity), inscriptionEspaceEntity.getEspaceOrganisation().getOrganisation().getDenomination());
			}
		}
		this.inscriptionEspaceRepository.save(inscriptionEspaceEntity);
	}

	/**
	 * {@inheritdoc}
	 */
	@Override
	@Transactional
	public void setFavoriteByDeclarantId(final Long id, final Long declarantId) {
		final InscriptionEspaceEntity inscriptionEspace = this.inscriptionEspaceRepository.findOne(id);

		if (inscriptionEspace == null) {
			LOGGER.error("Entité inscription espace (id: {}) non-existante en base!", id);
			throw new BusinessGlobalException(ErrorMessageEnum.MSG_404.getMessage(), 404);
		}

		if (!inscriptionEspace.getDeclarant().getId().equals(declarantId)) {
			LOGGER.error("Entité inscription espace (id: {}) inaccessible par le declarant {}" ,id,  declarantId);
			throw new BusinessGlobalException(ErrorMessageEnum.MSG_403.getMessage(), 403);
		}

		if (!inscriptionEspace.isFavori()) {
			this.inscriptionEspaceRepository.findByDateSuppressionIsNullAndFavoriTrueAndDeclarantId(declarantId).ifPresent(e -> {
				e.setFavori(false);
				this.inscriptionEspaceRepository.save(e);
			});

			inscriptionEspace.setFavori(true);
			this.inscriptionEspaceRepository.save(inscriptionEspace);
		}
	}

	/**
	 * {@inheritdoc}
	 */
	@Override
	@Transactional
	public void setDeclarantRoles(final Long inscriptionId, final Long currentEspaceOrganisationId, final Set<RoleEnumFrontOffice> roles, final boolean locked) {
		final InscriptionEspaceEntity inscriptionEspace = this.inscriptionEspaceRepository.findByDateSuppressionIsNullAndIdAndEspaceOrganisationId(inscriptionId,
				currentEspaceOrganisationId);

		// Si l'inscription du déclarant n'est pas validée alors refuser la modification
		// des droits
		if (inscriptionEspace.getDateValidation() == null || inscriptionEspace.getDateSuppression() != null) {
			final String message = ErrorMessageEnum.ECHEC_MODIF_ROLE_DECLARANT_NOT_ACTIVATED.getMessage();
			throw new BusinessGlobalException(message);
		}

		if (!inscriptionEspace.getRolesFront().containsAll(roles) || !roles.containsAll(inscriptionEspace.getRolesFront())) {
			inscriptionEspace.setRolesFront(roles);
		}

		inscriptionEspace.setVerrouille(locked);

		this.inscriptionEspaceRepository.save(inscriptionEspace);

		// Envoyer Mail
		final DeclarantDto declarantDto = this.declarantTransformer.modelToDto(inscriptionEspace.getDeclarant());

		this.sendEmailEditRoles(declarantDto, this.inscriptionEspaceTransformer.modelToDto(inscriptionEspace), inscriptionEspace.getEspaceOrganisation().getOrganisation().getDenomination());
	}

	/**
	 * {@inheritdoc}
	 */
	@Transactional
	@Override
	public InscriptionEspaceDto inscriptionNouvelEspace(final Long declarantId, final InscriptionEspaceDto inscriptionEspaceDto, final List<EspaceOrganisationPieceDto> pieces) {

		// Autorisation du déclarant à créer un espace
		Long nbCreationsEnAttente = this.espaceOrganisationRepository.countByCreateurEspaceOrganisationIdAndDateActionCreationIsNull(declarantId);
		if (nbCreationsEnAttente.longValue() >= this.limiteCreationEspaces) {
			throw new BusinessGlobalException(ErrorMessageEnum.LIMITE_CREATION_ESPACE_DEPASSEE.getMessage());
		}

		// Validation de la possibilité de s'inscrire pour le déclarant
		this.checkValiditeDemandeInscriptionEspace(declarantId, inscriptionEspaceDto.getOrganizationId());

		// On détermine s'il existe un espace non refusé pour cette organisation
		EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository
				.findEspaceOrganisationByOrganisationIdAndStatutNotRefusee(inscriptionEspaceDto.getOrganizationId());

		// On récupère le déclarant et l'organisation à inscrire
		final DeclarantEntity declarant = this.declarantRepository.findOne(declarantId);
		final OrganisationEntity organisationEntity = this.organisationRepository.findOne(inscriptionEspaceDto.getOrganizationId());

		// Si l'espace de l'organisation non refusé existe déjà on lance une exception
		if (espaceOrganisationEntity != null) {
			final String message = ErrorMessageEnum.ESPACE_ORGANISATION_EXISTE_DEJA.getMessage().replace("DENOMINATION", organisationEntity.getDenomination());
			throw new BusinessGlobalException(message);
		}

		InscriptionEspaceDto ieDto = null;
		InscriptionEspaceEntity inscriptionEspaceEntity = this.createSpaceAndJoinIt(declarant, organisationEntity, pieces, inscriptionEspaceDto);
		if (inscriptionEspaceEntity != null) {
			ieDto = this.inscriptionEspaceTransformer.modelToDto(inscriptionEspaceEntity);
			// Déclarant DTO utilisé pour l'envoi de l'email
			final DeclarantDto declarantDto = this.declarantTransformer.modelToDto(declarant);
			this.sendEmailCreateSpace(declarantDto, ieDto, organisationEntity.getDenomination());
		}

		// mailling de surveillance
        SurveillanceEntity surveillance = organisationEntity.getSurveillanceEntity();
		if (surveillance!=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.DEMANDE_INSCRIPTION)) {
			this.surveillanceMailer.sendSurveillanceMail(organisationEntity, espaceOrganisationEntity, SurveillanceOrganisationEnum.DEMANDE_INSCRIPTION,"");
		}

		return ieDto;
	}

	/**
	 * {@inheritdoc}
	 */
	@Transactional
	@Override
	public void inscriptionEspaceExistant(final Long declarantId, final InscriptionEspaceDto inscriptionEspaceDto) {

		// Validation de la possibilité de s'inscrire pour le déclarant
		this.checkValiditeDemandeInscriptionEspace(declarantId, inscriptionEspaceDto.getOrganizationId());

		// On détermine s'il existe un espace non refusé pour cette organisation
		EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository
				.findEspaceOrganisationByOrganisationIdAndStatutNotRefusee(inscriptionEspaceDto.getOrganizationId());

		// On récupère le déclarant et l'organisation à inscrire
		final DeclarantEntity declarant = this.declarantRepository.findOne(declarantId);
		final OrganisationEntity organisationEntity = this.organisationRepository.findOne(inscriptionEspaceDto.getOrganizationId());

		// S'il n'existe pas d'espace collaboratif à rejoindre pour l'organisation
		if (espaceOrganisationEntity == null) {
			final String message = ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage().replace("DENOMINATION", organisationEntity.getDenomination());
			throw new BusinessGlobalException(message);
		}

		// Ajouter l'inscription.
		final InscriptionEspaceDto ieDto = this.inscriptionEspaceTransformer
				.modelToDto(this.inscriptionEspaceRepository.save(this.getInscriptionEspace(declarant, espaceOrganisationEntity, false)));

		// Envoyer l'email au déclarant.
		final DeclarantDto declarantDto = this.declarantTransformer.modelToDto(declarant);
		this.sendEmailJoinSpace(declarantDto, ieDto, organisationEntity.getDenomination());

		// Envoyer l'email au contact opérationnel.
		ieDto.getAdministrateurs().forEach(admin -> this.sendEmailJoinSpaceAdmin(admin, ieDto, organisationEntity.getDenomination()));
	}

	/**
	 * Cette procédure valide que le déclarant peut faire la demande d'inscription. Elle lance une {@link BusinessGlobalException} si ce n'est pas le cas.
	 *
	 * @param declarantId
	 * @param organisationId
	 * @throws BusinessGlobalException
	 */
	protected void checkValiditeDemandeInscriptionEspace(final Long declarantId, final Long organisationId) {
		// On détermine si le déclarant est déjà inscrit à l'espace de l'organisation
		InscriptionEspaceEntity inscriptionEspace = this.inscriptionEspaceRepository.findByDateSuppressionIsNullAndDeclarantIdAndEspaceOrganisationOrganisationId(declarantId,
				organisationId);

		// Si tel est le cas on rejete la demande avec le bon motif
		if (inscriptionEspace != null && (inscriptionEspace.getDateSuppression() == null)) {
			if (!inscriptionEspace.isActif()) {
				// Cas 1 : la demande du déclarant n'a pas été validée
				throw new BusinessGlobalException(ErrorMessageEnum.DEMANDE_EN_ATTENTE_DE_VALIDATION.getMessage());
			} else if (inscriptionEspace.isVerrouille()) {
				// Cas 2 : l'accès à l'espace a été verrouillé pour ce déclarant
				throw new BusinessGlobalException(ErrorMessageEnum.ACCES_VEROUILLE.getMessage());
			}
			// Cas générique : le déclarant ne peut pas se réinscrire à un espace auquel il
			// est déjà inscrit
			throw new BusinessGlobalException(ErrorMessageEnum.VOUS_ETES_DEJA_INSCRIS_A_CET_ESPACE_ORGANISATION.getMessage());
		}
	}

	/**
	 * Cette methode envoie un email de modification des rôles.
	 *
	 * @param dto
	 *            email du déclarant.
	 * @param organisationNom
	 *            nom de l'organisation.
	 */
	private void sendEmailEditRoles(final DeclarantDto dto, final InscriptionEspaceDto inscriptionEspaceDto, final String organisationNom) {
		this.organisationEspaceMailer.sendEditRolesEmail(dto, inscriptionEspaceDto, organisationNom);
	}

	/**
	 * Cette methode envoie un email de validation de demande de rejoindre un espace.
	 *
	 * @param dto
	 *            email du déclarant.
	 * @param organisationNom
	 *            nom de l'organisation.
	 */
	private void sendEmailValidRequest(final DeclarantDto dto, final InscriptionEspaceDto inscriptionEspaceDto, final String organisationNom) {
		this.organisationEspaceMailer.sendJoinEspaceEmailValidated(dto, inscriptionEspaceDto, organisationNom);
	}

	/**
	 * Cette methode envoie un email de rejet de demande de rejoindre un espace.
	 *
	 * @param dto
	 *            email du déclarant.
	 * @param organisationNom
	 *            nom de l'organisation.
	 */
	private void sendEmailRejectedRequest(final DeclarantDto dto, final InscriptionEspaceDto inscriptionEspaceDto, final String organisationNom) {
		this.organisationEspaceMailer.sendJoinEspaceEmailRejected(dto, inscriptionEspaceDto, organisationNom);
	}

	/**
	 * Cette methode envoie un email de confirmation d'inscription à un espace corporate.
	 *
	 * @param dto
	 *            email du déclarant.
	 * @param organisationNom
	 *            nom de l'organisation.
	 */
	private void sendEmailJoinSpace(final DeclarantDto dto, final InscriptionEspaceDto inscriptionEspaceDto, final String organisationNom) {
		this.organisationEspaceMailer.sendJoinEspaceEmail(dto, inscriptionEspaceDto, organisationNom);
	}

	/**
	 * Cette methode envoie un email de demande d'inscription à un espace corporate.
	 *
	 * @param dto
	 *            email du contact opérationnel.
	 * @param organisationNom
	 *            nom de l'organisation.
	 */
	private void sendEmailJoinSpaceAdmin(final DeclarantDto dto, final InscriptionEspaceDto inscriptionEspaceDto, final String organisationNom) {
		this.organisationEspaceMailer.sendJoinEspaceAdminEmail(dto, inscriptionEspaceDto, organisationNom);
	}

	/**
	 * Cette methode envoie un email de confirmation de création et d'espace corporate.
	 *
	 * @param dto
	 *            email du déclarant.
	 * @param organisationNom
	 *            nom de l'organisation.
	 */
	private void sendEmailCreateSpace(final DeclarantDto dto, final InscriptionEspaceDto inscriptionEspaceDto, final String organisationNom) {
		this.organisationEspaceMailer.sendCreateEspaceEmail(dto, inscriptionEspaceDto, organisationNom);
	}

	/**
	 * Cette méthode crée un espace organisation et inscit son créateur autant que contributeur à cet espace
	 *
	 * @param declarant
	 *            entitée déclarant.
	 * @param organisation
	 *            entitée organisation.
	 * @param inscriptionEspaceDto
	 *            inscription espace DTO.
	 */
	private InscriptionEspaceEntity createSpaceAndJoinIt(final DeclarantEntity declarant, final OrganisationEntity organisation, final List<EspaceOrganisationPieceDto> pieces,
			final InscriptionEspaceDto inscriptionEspaceDto) {
		// create space
		final EspaceOrganisationEntity espaceOrganisation = new EspaceOrganisationEntity();
		espaceOrganisation.setOrganisation(organisation);

		espaceOrganisation.setRepresentantLegal(inscriptionEspaceDto.isRepresentantLegal());

		// Données complémentaires de l'organisation.
		espaceOrganisation.setAdresse(organisation.getAdresse());
		espaceOrganisation.setCodePostal(organisation.getCodePostal());
		espaceOrganisation.setVille(organisation.getVille());
		espaceOrganisation.setPays(organisation.getPays());
		espaceOrganisation.setLienSiteWeb(organisation.getSiteWeb());
		espaceOrganisation.setNomUsage(organisation.getNomUsageSiren());

		espaceOrganisation.setCreateurEspaceOrganisation(declarant);

		// Enregistrer l'espace organisation de l'organisation.
		final EspaceOrganisationEntity savedEspaceOrganisationEntity = this.espaceOrganisationRepository.save(espaceOrganisation);

		// Enregistrer le collaborateur.
		this.devenirCollaborateur(declarant, savedEspaceOrganisationEntity);

		final InscriptionEspaceEntity inscriptionEspace = this.getInscriptionEspace(declarant, savedEspaceOrganisationEntity, true);

		// Enregistrement des pièces
		for (EspaceOrganisationPieceDto piece : pieces) {
			// Transformation du DTO en entity
			EspaceOrganisationPieceEntity entity = this.espaceOrganisationPieceTransformer.dtoToModel(piece);

			entity.setDateVersementPiece(savedEspaceOrganisationEntity.getCreationDate());
			entity.setDeclarant(declarant);
			entity.setEspaceOrganisation(savedEspaceOrganisationEntity);
			entity.setOrigineVersement(OrigineVersementEnum.DEMANDE_CREATION_ESPACE);
			this.espaceOrganisationPiecesRepository.save(entity);
		}

		// Ajouter l'inscription.
		return this.inscriptionEspaceRepository.save(inscriptionEspace);
	}

	/**
	 * @param declarant
	 *            entitée déclarant.
	 * @param espaceOrganisation
	 *            entitée espace organisation.
	 * @return Les informations de l'inscription à l'espace organisation de l'organisation passée en paramètre.
	 */
	private InscriptionEspaceEntity getInscriptionEspace(final DeclarantEntity declarant, final EspaceOrganisationEntity espaceOrganisation, final boolean isCreateSpace) {
		final InscriptionEspaceEntity inscriptionEspace = new InscriptionEspaceEntity();
		inscriptionEspace.setDeclarant(declarant);

		if (this.inscriptionEspaceRepository.findAllByDateSuppressionIsNullAndDeclarantId(declarant.getId()).isEmpty()) {
			inscriptionEspace.setFavori(true);
		}

		inscriptionEspace.setEspaceOrganisation(espaceOrganisation);

		if (isCreateSpace) {
			final Set<RoleEnumFrontOffice> roleEnumFrontOffices = new HashSet<>();
			roleEnumFrontOffices.add(RoleEnumFrontOffice.CONTRIBUTEUR);
			inscriptionEspace.setRolesFront(roleEnumFrontOffices);
			inscriptionEspace.setActif(true);
			inscriptionEspace.setDateValidation(new Date());
		}

		return inscriptionEspace;
	}

	/**
	 * Cette méthode permet d'ajouter un declarant dès lors qu'il à rejoins un espace organisation à la liste des collaborateurs inscrit
	 *
	 * @param declarantEntity
	 *            entitée déclarant.
	 * @param espaceOrganisationEntity
	 *            entitée espace organisation.
	 */
	private CollaborateurEntity devenirCollaborateur(final DeclarantEntity declarantEntity, final EspaceOrganisationEntity espaceOrganisationEntity) {
		final CollaborateurEntity collaborateur = new CollaborateurEntity();
		collaborateur.setEspaceOrganisation(espaceOrganisationEntity);
		collaborateur.setDeclarant(declarantEntity);
		collaborateur.setActif(true);
		// Affecter ordre par défaut
		collaborateur.setSortOrder(this.collaborateurRepository.findByEspaceOrganisationId(espaceOrganisationEntity.getId()).size());

		return this.collaborateurRepository.save(collaborateur);
	}
	
	/**
	 * Cette méthode permet d'ajouter un declarant à la liste des contacts opérationnel inscrit
	 *
	 *
	 */
	@Override
	public void devenirContactOperationnel(final Long declarantId, final Long espaceOrganisationId,final List<EspaceOrganisationPieceDto> pieces) {
		final DeclarantEntity declarant = this.declarantRepository.findOne(declarantId);
		final EspaceOrganisationEntity espaceOrganisation = this.espaceOrganisationRepository.findOne(espaceOrganisationId);
		final InscriptionEspaceEntity inscriptionEspace;
		boolean dejaInscrit = declarant.getInscriptionEspaces()!=null && declarant.getInscriptionEspaces().stream().filter(i->i.getEspaceOrganisation().equals(espaceOrganisation)).findFirst().isPresent();
		
        if (!dejaInscrit){
        	inscriptionEspace = this.getInscriptionEspace(declarant, espaceOrganisation, true);
        	inscriptionEspace.getRolesFront().add(RoleEnumFrontOffice.PUBLICATEUR);
        	inscriptionEspace.getRolesFront().add(RoleEnumFrontOffice.ADMINISTRATEUR);
        	inscriptionEspace.setFavori(true);
        	
        	
        }
        else {
        	inscriptionEspace = declarant.getInscriptionEspaces().stream().filter(i->i.getEspaceOrganisation().equals(espaceOrganisation)).findFirst().get();
    		//ON AJOUTE QUE SI PAS DEJA CO 
    		if(!inscriptionEspace.getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR)){
    			inscriptionEspace.getRolesFront().add(RoleEnumFrontOffice.ADMINISTRATEUR);
    			inscriptionEspace.setActif(true);
    			if (inscriptionEspace.getDateValidation()== null) inscriptionEspace.setDateValidation(inscriptionEspace.getDateDemande());

            }else{
                throw new BusinessGlobalException(ErrorMessageEnum.DUPLICATE_CO.getMessage());
            }
        }
		

		// Enregistrement des pièces
		for (EspaceOrganisationPieceDto piece : pieces) {
			// Transformation du DTO en entity
			EspaceOrganisationPieceEntity entity = this.espaceOrganisationPieceTransformer.dtoToModel(piece);

			entity.setDateVersementPiece(espaceOrganisation.getCreationDate());
			entity.setDeclarant(declarant);
			entity.setEspaceOrganisation(espaceOrganisation);
			entity.setOrigineVersement(OrigineVersementEnum.DEMANDE_CREATION_ESPACE);
			this.espaceOrganisationPiecesRepository.save(entity);
		}
		//ajout a la liste des collab
		if (!espaceOrganisation.getListeDesCollaborateurs().stream().anyMatch(c->c.getDeclarant()!=null && c.getDeclarant().equals(declarant))) {
			this.devenirCollaborateur(declarant, espaceOrganisation);
		}
		// Ajouter l'inscription.
		this.inscriptionEspaceRepository.save(inscriptionEspace);

	}


    /**
	 * {@inheritDoc}
	 */
	@Override
	public InscriptionEspaceDto getRegistrationByIdAndDeclarantId(final Long registrationId, final Long declarantId) {
		final InscriptionEspaceEntity inscriptionEspaceEntity = this.inscriptionEspaceRepository.findByDateSuppressionIsNullAndIdAndDeclarantId(registrationId, declarantId);
		
		if (inscriptionEspaceEntity == null) {
			throw new BusinessGlobalException(ErrorMessageEnum.INSCRIPTION_INTROUVABLE.getMessage(), 404);
		}
		return this.inscriptionEspaceTransformer.modelToDto(inscriptionEspaceEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InscriptionEspaceDto getRegistrationByEspaceOrganisationIdAndDeclarantId(final Long espaceOrganisationId, final Long declarantId) {
		final Optional<InscriptionEspaceEntity> inscriptionEspaceEntity = this.inscriptionEspaceRepository.findByDateSuppressionIsNullAndDeclarantIdAndEspaceOrganisationId(declarantId,
				espaceOrganisationId);

		if (!inscriptionEspaceEntity.isPresent()) {
			throw new BusinessGlobalException(ErrorMessageEnum.INSCRIPTION_INTROUVABLE.getMessage(), 404);
		}

		return this.inscriptionEspaceTransformer.modelToDto(inscriptionEspaceEntity.get());
	}

	/**
	 * Changer les roles de l'utilisateur suivant l'espace organisation sélectionné.
	 */
	@Override
	public void changeRolesInContext(final Set<String> roleEnums, final boolean isEspaceDisabled) {

		final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		// On nettoie d'abord les rôle éventuellement chargés
		final List<GrantedAuthority> updatedAuthorities = new ArrayList<>(auth.getAuthorities());
		updatedAuthorities.clear();

		// Pas de chargement de nouveaux rôles si espace désactivé
		if (!isEspaceDisabled) {
			roleEnums.forEach(r -> updatedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + r)));

			final Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(), updatedAuthorities);

			SecurityContextHolder.getContext().setAuthentication(newAuth);
		}
	}

    @Override
    @Transactional
    public void demanderDesinscription(Long idEspaceOrganisation, Long idDeclarant, DesinscriptionDto desinscriptionDto, ArrayList<DesinscriptionPieceDto> pieces) {
        DeclarantEntity declarantEntity = this.declarantRepository.findOne(idDeclarant);
        EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository.findOne(idEspaceOrganisation);
        DesinscriptionEntity entity = desinscriptionRepository.findByEspaceOrganisation(espaceOrganisationEntity);

        // Si la désinscription de l'espace de l'organisation non refusé existe déjà on lance une exception
        if (entity != null) {
            final String message = ErrorMessageEnum.ESPACE_ORGANISATION_DEJA_DESINSCRIT.getMessage();
            throw new BusinessGlobalException(message);
        }else{
            entity = desinscriptionTransformer.dtoToModel(desinscriptionDto);
            entity.setEspaceOrganisation(espaceOrganisationEntity);
            entity.setOrigineSaisie(OrigineSaisieEnum.DECLARANT);
        }

        // on set les informations complémentaires
        espaceOrganisationEntity.setStatut(StatutEspaceEnum.DESINSCRIPTION_DEMANDEE);
        if (pieces !=null) {
            for(DesinscriptionPieceDto piece : pieces){
                DesinscriptionPieceEntity e = desinscriptionPieceTransformer.dtoToModel(piece);
                e.setDateVersementPiece(new Date());
                e.setOrigineVersement(OrigineVersementEnum.DEMANDE_DESINSCRIPTION_ESPACE);
                e.setDesinscription(entity);
                this.desinscriptionPieceRepository.save(e);
            }
        }

        // on save l'entity
        desinscriptionRepository.save(entity);
        
        //envoi notif de la demande de désinscription à la DRI
        this.desinscriptionMailer.sendDesinscriptionMailDRI(espaceOrganisationEntity, entity, declarantEntity);

        // on envoie le mail de désinscription
        if(espaceOrganisationEntity.getStatut().equals(StatutEspaceEnum.DESINSCRIPTION_VALIDEE_HATVP)) {
        	this.desinscriptionMailer.sendDesinscriptionMail(espaceOrganisationEntity, entity, declarantEntity);
        }
        
	}
}
