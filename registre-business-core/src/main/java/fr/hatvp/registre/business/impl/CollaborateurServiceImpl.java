/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.CollaborateurService;
import fr.hatvp.registre.business.email.CollaboratorMailer;
import fr.hatvp.registre.business.email.SurveillanceMailer;
import fr.hatvp.registre.business.transformer.proxy.CollaborateurTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.CollaborateurDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.espace.CollaborateurEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import fr.hatvp.registre.persistence.repository.espace.CollaborateurRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

/**
 * Service de gestion des collaborateurs.
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional(readOnly = true)
@Service
public class CollaborateurServiceImpl extends AbstractCommonService<CollaborateurDto, CollaborateurEntity> implements CollaborateurService {

	/** Repository du service. */
	@Autowired
	private CollaborateurRepository collaborateurRepository;

	/** Repository des espaces organisation. */
	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;

	/** Transformer du service */
	@Autowired
	private CollaborateurTransformer collaborateurTransformer;

	/** Mailer des collaborateurs. */
	@Autowired
	private CollaboratorMailer collaboratorMailer;

	@Autowired
	private SurveillanceMailer surveillanceMailer;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CollaborateurDto> getAllCollaborateurs(final Long espaceOrganisationId) {

		List<CollaborateurEntity> collaborateurs = this.collaborateurRepository.findByEspaceOrganisationId(espaceOrganisationId).stream()
				.sorted(Comparator.comparing(CollaborateurEntity::getSortOrder)).collect(Collectors.toList());

		int size = collaborateurs.size(), last = size - 1, first = 0;
		for (int i = 0; i < size; i++) {
			if (collaborateurs.get(i).getActif()) {
				collaborateurs.get(i).setSortOrder(first++);
			} else {
				collaborateurs.get(i).setSortOrder(last--);
			}
		}

		return collaborateurs.stream().map(this.collaborateurTransformer::modelToDto).collect(Collectors.toList());

	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public CollaborateurDto addCollaborateurAdditionel(final CollaborateurDto collaborateur, final Long espaceOrganisationId) {
		// On s'assure qu'on ne persistera pas un identifiant de déclarant pour un collaborateur additionel
		collaborateur.setDeclarantId(null);
		collaborateur.setEspaceOrganisationId(espaceOrganisationId);
		// Affecter ordre par défaut
		collaborateur.setSortOrder(this.collaborateurRepository.findByEspaceOrganisationId(espaceOrganisationId).size());

		collaborateur.setNom(collaborateur.getNom().toUpperCase());
		collaborateur.setPrenom(RegistreUtils.normaliser(collaborateur.getPrenom()));
		collaborateur.setActif(Boolean.TRUE);

		final CollaborateurDto dto = this.collaborateurTransformer.modelToDto(this.collaborateurRepository.save(this.collaborateurTransformer.dtoToModel(collaborateur)));

		final EspaceOrganisationEntity espace = this.espaceOrganisationRepository.findOne(espaceOrganisationId);

		final String denomination = espace.getOrganisation().getDenomination();

		this.collaboratorMailer.sendNotificationEmail(dto, denomination);
        SurveillanceEntity surveillance = espace.getOrganisation().getSurveillanceEntity();
		if (surveillance!=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.AJOUT_COLLABORATEUR)) {
			this.surveillanceMailer.sendSurveillanceMail(espace.getOrganisation(), espace, SurveillanceOrganisationEnum.AJOUT_COLLABORATEUR,"");
		}

		return dto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void setCollaborateurActif(final Long id, final Long espaceOrganisationId, final boolean actif) {
		final CollaborateurEntity collaborateurEntity = Optional.ofNullable(this.collaborateurRepository.findByIdAndEspaceOrganisationId(id, espaceOrganisationId))
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.MSG_404.getMessage(), 404));

		Optional.ofNullable(collaborateurEntity.getDeclarant()).orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.STATUT_ACTIF_COLLABORATEUR_ADDITIONNEL.getMessage(), 422));

		if(!actif){
		    collaborateurEntity.setDateDesactivation(LocalDateTime.now());
        }else{
		    collaborateurEntity.setDateDesactivation(null);
        }
		collaborateurEntity.setActif(actif);

		this.collaborateurRepository.save(collaborateurEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void remonterCollaborateur(final Long id, final Long espaceOrganisationId) {
		final List<CollaborateurEntity> collabs = this.collaborateurRepository.findByEspaceOrganisationId(espaceOrganisationId);

		CollaborateurEntity col = this.collaborateurRepository.findOne(id);

		int jump = 1;
		while (!collabs.stream().filter(filterInactiveAbove(col, jump)).findFirst().get().getActif()) {
			jump++;
		}

		if (col.getSortOrder() > 0) {
			collabs.stream().filter(filterInactiveAbove(col, jump)).forEach(c -> {
				c.setSortOrder(col.getSortOrder());
				this.collaborateurRepository.save(c);
			});
			col.setSortOrder(col.getSortOrder() - jump);
			this.collaborateurRepository.save(col);
		}

	}

	private Predicate<? super CollaborateurEntity> filterInactiveAbove(CollaborateurEntity col, int jump) {
		return c -> c.getSortOrder().intValue() == col.getSortOrder().intValue() - jump;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void descendreCollaborateur(final Long id, final Long espaceOrganisationId) {
		final List<CollaborateurEntity> collabs = this.collaborateurRepository.findByEspaceOrganisationId(espaceOrganisationId);

		CollaborateurEntity col = collaborateurRepository.findOne(id);

		int jump = 1;
		while (!collabs.stream().filter(filterInactiveUnder(col, jump)).findFirst().get().getActif()) {
			jump++;
		}

		if (col.getSortOrder() < (collabs.size() - 1)) {
			collabs.stream().filter(filterInactiveUnder(col, jump)).forEach(c -> {
				c.setSortOrder(col.getSortOrder());
				this.collaborateurRepository.save(c);
			});
			col.setSortOrder(col.getSortOrder() + jump);
			this.collaborateurRepository.save(col);
		}

	}

	private Predicate<? super CollaborateurEntity> filterInactiveUnder(CollaborateurEntity col, int jump) {
		return c -> c.getSortOrder().intValue() == col.getSortOrder().intValue() + jump;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void deleteCollaborateur(final Long collaborateurId, final Long espaceOrganisationId) {
		final CollaborateurEntity collaborateurEntity = Optional.ofNullable(this.collaborateurRepository.findByIdAndEspaceOrganisationId(collaborateurId, espaceOrganisationId))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.MSG_404.getMessage()));

		Optional.ofNullable(collaborateurEntity.getDeclarant()).ifPresent(dec -> {
			throw new BusinessGlobalException(ErrorMessageEnum.SUPPRESSION_COLLABORATEUR_INSCRIT.getMessage(), 422);
		});

		// Recalcul des numéros d'ordre supérieur au collaborateur supprimé
		this.collaborateurRepository.findByEspaceOrganisationId(espaceOrganisationId).stream().filter(e -> e.getSortOrder() > collaborateurEntity.getSortOrder())
				.forEach(e -> e.setSortOrder(e.getSortOrder() - 1));
		this.collaborateurRepository.delete(collaborateurEntity);
	}

	/**
	 * Enregistrement des modifications de mise à jour des données des collaborateurs
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public CollaborateurDto editFonctionCollaborateur(final CollaborateurDto dto, final Long espaceOrganisationId) {
		final CollaborateurEntity entity = Optional.ofNullable(this.collaborateurRepository.findByIdAndEspaceOrganisationId(dto.getId(), espaceOrganisationId))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.COLLABORATEUR_INTROUVABLE.getMessage()));

		entity.setVersion(dto.getVersion());
		entity.setFonction(dto.getFonction());
		entity.setNom(dto.getNom());
		entity.setPrenom(dto.getPrenom());
		entity.setCivility(dto.getCivilite());
		entity.setEmail(dto.getEmail());
		return this.collaborateurTransformer.modelToDto(this.collaborateurRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<CollaborateurEntity, Long> getRepository() {
		return this.collaborateurRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<CollaborateurDto, CollaborateurEntity> getTransformer() {
		return this.collaborateurTransformer;
	}
}
