/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.activite;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableSimpleDto;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;

public interface ExerciceComptableSimpleTransformer extends CommonTransformer<ExerciceComptableSimpleDto, ExerciceComptableEntity> {

}
