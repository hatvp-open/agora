/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment.impl;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.comment.CommentDemandeChangementMandantBoTransformer;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentDemandeChangementMandatEntity;
import fr.hatvp.registre.persistence.entity.espace.DemandeChangementMandantEntity;
import fr.hatvp.registre.persistence.repository.espace.DemandeChangementMandantRepository;

/**
 * Classe de transformation de la classe {@link fr.hatvp.registre.persistence.entity.commentaire.CommentDemandeEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CommentDemandeChangementMandantBoTransformerImpl extends AbstractCommentBoTransformerImpl<CommentBoDto, CommentDemandeChangementMandatEntity>
		implements CommentDemandeChangementMandantBoTransformer {

	/**
	 * Repository des demandes.
	 */
	@Autowired
	private DemandeChangementMandantRepository repository;

	/** Constructeur de la classe. */
	public CommentDemandeChangementMandantBoTransformerImpl() {
		super.entityClass = CommentDemandeChangementMandatEntity.class;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentDemandeChangementMandatEntity dtoToModel(final CommentBoDto dto) {
		return super.dtoToModel(dto);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentBoDto commentModelToDto(final CommentDemandeChangementMandatEntity model) {
		return super.commentModelToDto(model);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	void dtoToModelComplement(final CommentBoDto dto, final CommentDemandeChangementMandatEntity commentDemandeChangementMandatEntity) {
		final DemandeChangementMandantEntity demandeEntity = Optional.ofNullable(this.repository.findOne(dto.getContextId()))
				.orElseThrow(() -> new EntityNotFoundException("Demande id introuvable"));
		commentDemandeChangementMandatEntity.setDemandeChangementMandantEntity(demandeEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	void modelToDtoComplement(final CommentDemandeChangementMandatEntity model, final CommentBoDto commentBoDto) {
		commentBoDto.setContextId(model.getDemandeChangementMandantEntity().getId());
	}

}