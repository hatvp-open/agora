/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.common;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonPublicationTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PubAssociationTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PubClientTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PubCollaborateurTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PubDirigeantTransformer;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.dto.CollaborateurDto;
import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.commons.dto.publication.PubActiviteDto;
import fr.hatvp.registre.commons.dto.publication.PubAssoClientDto;
import fr.hatvp.registre.commons.dto.publication.PubCollaborateurDto;
import fr.hatvp.registre.commons.dto.publication.PubDirigeantDto;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.PeriodeActiviteClientEntity;
import fr.hatvp.registre.persistence.entity.publication.PubAssociationEntity;
import fr.hatvp.registre.persistence.entity.publication.PubClientEntity;
import fr.hatvp.registre.persistence.entity.publication.PubCollaborateurEntity;
import fr.hatvp.registre.persistence.entity.publication.PubDirigeantEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;

/**
 * Classe du transformeur entre {@link PublicationEntity} et {@link PublicationDto}
 * @version $Revision$ $Date${0xD}
 */
@Component
public abstract class CommonPublicationTransformerImpl<E extends PublicationDto> extends AbstractCommonTansformer<E, PublicationEntity> implements CommonPublicationTransformer<E> {

	/** Type du DTO de publication à utiliser. */
	protected Class<E> dtoClass;

	/** Transformeur des collaborateurs. */
	@Autowired
	private PubCollaborateurTransformer collaborateurTransformer;
	/** Transformeur des dirigeants. */
	@Autowired
	private PubDirigeantTransformer dirigeantTransformer;
	/** Transformeur des associations. */
	@Autowired
	private PubAssociationTransformer associationTransformer;
	/** Transformeur des clients. */
	@Autowired
	private PubClientTransformer clientTransformer;
	/** Tarnsformer des déclarants. */
	@Autowired
	private DeclarantTransformer declarantTransformer;
	@Autowired
	private DesinscriptionRepository desinscriptionRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationEntity dtoToModel(final PublicationDto dto) {
		final PublicationEntity model = new PublicationEntity();
		RegistreBeanUtils.copyProperties(model, dto);
		model.setCreationDate(new Date());

		// Éviter l'optimistic lock en copiant les données de l'espace vers l'entité publication.
		model.setVersion(null);

		// Map les collaborateurs pour la publication vers les colaborateurs d'un espace.
		List<CollaborateurDto> collaborateurs = dto.getCollaborateurs().stream().map(c -> {
			CollaborateurDto dest = new CollaborateurDto();
			RegistreBeanUtils.copyProperties(dest, c);
			return dest;
		}).collect(Collectors.toList());

		final List<PubCollaborateurEntity> collaborateurEntities = this.collaborateurTransformer.dtoToModel(collaborateurs);
		collaborateurEntities.forEach(c -> {
			c.setPublication(model);
			c.setVersion(null);
		});
		model.setCollaborateurs(collaborateurEntities);

		List<DirigeantDto> dirigeants = dto.getDirigeants().stream().map(c -> {
			DirigeantDto dest = new DirigeantDto();
			RegistreBeanUtils.copyProperties(dest, c);
			return dest;
		}).collect(Collectors.toList());

		final List<PubDirigeantEntity> dirigeantEntityList = this.dirigeantTransformer.dtoToModel(dirigeants);
		dirigeantEntityList.forEach(c -> {
			c.setPublication(model);
			c.setVersion(null);
		});
		model.setDirigeants(dirigeantEntityList);

		// Map les clients pour la publication vers les clients d'un espace.
		List<AssoClientDto> clients = dto.getClients().stream().map(c -> {
			AssoClientDto dest = new AssoClientDto();
			RegistreBeanUtils.copyProperties(dest, c);
			return dest;
		}).collect(Collectors.toList());

		final List<PubClientEntity> clientEntities = this.clientTransformer.dtoToModel(clients);
		clientEntities.forEach(c -> {
			c.setPublication(model);
			c.setVersion(null);
			//dpnnées de la période en cours
			List<PeriodeActiviteClientEntity> periodeActiviteClientEntityListeTriee = c.getPeriodeActiviteClientList().stream().sorted(Comparator.comparing(PeriodeActiviteClientEntity::getDateAjout).reversed()).collect(Collectors.toList());
			c.setDateAjout(periodeActiviteClientEntityListeTriee.get(0).getDateAjout());
			c.setDateDesactivation(periodeActiviteClientEntityListeTriee.get(0).getDateDesactivation());
			c.setDateFinContrat(periodeActiviteClientEntityListeTriee.get(0).getDateFinContrat());
			c.setCommentaire(periodeActiviteClientEntityListeTriee.get(0).getCommentaire());
			c.setIsAncienClient(periodeActiviteClientEntityListeTriee.get(0).getDateDesactivation() != null);

		});
		model.setClients(clientEntities);

		// Map les assos pour la publication vers les assos d'un espace.
		List<AssoClientDto> affiliations = dto.getAffiliations().stream().map(a -> {
			AssoClientDto dest = new AssoClientDto();
			RegistreBeanUtils.copyProperties(dest, a);
			return dest;
		}).collect(Collectors.toList());

		final List<PubAssociationEntity> associationEntityList = this.associationTransformer.dtoToModel(affiliations);
		associationEntityList.forEach(c -> {
			c.setPublication(model);
			c.setVersion(null);
		});
		model.setAssociations(associationEntityList);

		model.setPublicateur(this.declarantTransformer.dtoToModel(dto.getPublisher()));

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(dto.getEspaceOrganisationId());
		model.setEspaceOrganisation(espaceOrganisationEntity);

		if (dto.getActivites() != null) {
			model.setNiveauIntervention(RegistreUtils.implodeListWithComma(dto.getActivites().getListNiveauIntervention()));
			model.setSecteursActivites(RegistreUtils.implodeListWithComma(dto.getActivites().getListSecteursActivites()));
		}

		model.setNonDeclarationOrgaAppartenance(dto.isDeclarationOrgaAppartenance() == null ? null : !dto.isDeclarationOrgaAppartenance());
		model.setNonDeclarationTiers(dto.isDeclarationTiers() == null ? null : !dto.isDeclarationTiers());
		model.setNonPublierMonAdresseEmail(dto.getPublierMonAdresseEmail() == null ? null : !dto.getPublierMonAdresseEmail());
		model.setNonPublierMonAdressePhysique(dto.getPublierMonAdressePhysique() == null ? null : !dto.getPublierMonAdressePhysique());
		model.setNonPublierMonTelephoneDeContact(dto.getPublierMonTelephoneDeContact() == null ? null : !dto.getPublierMonTelephoneDeContact());

		return model;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public E modelToDto(final PublicationEntity model) {
		final E dto;
		try {
			dto = this.dtoClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new BusinessGlobalException("Erreur d'instanciation dans le transformeur des publications");
		}

		RegistreBeanUtils.copyProperties(dto, model);
		dto.setId(null);
		dto.setVersion(null);

        List<PubCollaborateurDto> collaborateurDtos = new ArrayList<>();
        List<PubDirigeantDto> dirigeantDtoList = new ArrayList<>();
        
        if(!model.getEspaceOrganisation().getStatut().equals(StatutEspaceEnum.DESINSCRIT )) {
            // Traiter la liste des collaborateurs
            collaborateurDtos = this.collaborateurTransformer.modelToDto(model.getCollaborateurs()).stream()
                .sorted(Comparator.comparing(CollaborateurDto::getSortOrder)).map(c -> {
                    PubCollaborateurDto collaborateurDto = new PubCollaborateurDto();
                    if (c.getDeclarantId() != null) {
                        collaborateurDto.setId(null);
                        collaborateurDto.setDeclarantId(null);
                    }
                    collaborateurDto.setPrenom(c.getPrenom());
                    collaborateurDto.setNom(c.getNom());
                    collaborateurDto.setEmail(c.getEmail());
                    collaborateurDto.setCivilite(c.getCivilite());
                    collaborateurDto.setFonction(c.getFonction());
                    collaborateurDto.setInscrit(c.getInscrit());
                    collaborateurDto.setActif(c.getActif());
                    collaborateurDto.setVersion(null);
                    collaborateurDto.setEspaceOrganisationId(null);
                    collaborateurDto.setSortOrder(null);
                    return collaborateurDto;
                }).collect(Collectors.toList());
            
            // traiter la liste des dirigeants.
            dirigeantDtoList = this.dirigeantTransformer.modelToDto(model.getDirigeants()).stream().map(d -> {
                PubDirigeantDto dirigeantDto = new PubDirigeantDto();
                dirigeantDto.setPrenom(d.getPrenom());
                dirigeantDto.setNom(d.getNom());
                dirigeantDto.setCivilite(d.getCivilite());
                dirigeantDto.setFonction(d.getFonction());
                return dirigeantDto;
            }).collect(Collectors.toList());
            
        }
        
        dto.setCollaborateurs(collaborateurDtos);
        dto.setDirigeants(dirigeantDtoList);

		// Traiter la liste des clients.
		final List<PubAssoClientDto> clients =new ArrayList<>();
		
		for(PubClientEntity pubClient : model.getClients()) {
			PubAssoClientDto assoClientDto = new PubAssoClientDto();
			
			AssoClientDto c = this.clientTransformer.modelToDto(pubClient);			
			
			assoClientDto.setDenomination(c.getDenomination());
			assoClientDto.setNationalId(c.getNationalId());
			assoClientDto.setOriginNationalId(c.getOriginNationalId());
			assoClientDto.setAncienClient(pubClient.getIsAncienClient());
			assoClientDto.setDateDesactivation(pubClient.getDateDesactivation());
			assoClientDto.setDateAjout(pubClient.getDateAjout());
			assoClientDto.setDateCessation(c.getDateCessation());
			
			clients.add(assoClientDto);
		}
		clients.sort(Comparator.comparing(PubAssoClientDto::getDenomination));
		dto.setClients(clients);

		// Traiter la liste des associations.
		final List<PubAssoClientDto> associations = this.associationTransformer.modelToDto(model.getAssociations()).stream().map(a -> {
			PubAssoClientDto assoClientDto = new PubAssoClientDto();
			assoClientDto.setDenomination(a.getDenomination());
			assoClientDto.setNationalId(a.getNationalId());
			assoClientDto.setOriginNationalId(a.getOriginNationalId());
			assoClientDto.setDateCessation(a.getDateCessation());
			return assoClientDto;
		}).collect(Collectors.toList());
		dto.setAffiliations(associations);

		final PubActiviteDto pubActiviteDto = new PubActiviteDto();
		if (StringUtils.hasText(model.getSecteursActivites())) {
			final String[] strings = model.getSecteursActivites().split(",");
			for (final String s : strings) {
				pubActiviteDto.getListSecteursActivites().add(SecteurActiviteEnum.valueOf(s));
			}
		}

		if (StringUtils.hasText(model.getNiveauIntervention())) {
			final String[] strings = model.getNiveauIntervention().split(",");
			for (final String s : strings) {
				pubActiviteDto.getListNiveauIntervention().add(NiveauInterventionEnum.valueOf(s));
			}
		}
		dto.setActivites(pubActiviteDto);

		dto.setPublierMonAdresseEmail(model.getNonPublierMonAdresseEmail() == null ? null : !model.getNonPublierMonAdresseEmail());
		dto.setPublierMonAdressePhysique(model.getNonPublierMonAdressePhysique() == null ? null : !model.getNonPublierMonAdressePhysique());
		dto.setPublierMonTelephoneDeContact(model.getNonPublierMonTelephoneDeContact() == null ? null : !model.getNonPublierMonTelephoneDeContact());
		dto.setDeclarationTiers(model.isNonDeclarationTiers() == null ? null : !model.isNonDeclarationTiers());
		dto.setDeclarationOrgaAppartenance(model.isNonDeclarationOrgaAppartenance() == null ? null : !model.isNonDeclarationOrgaAppartenance());
		dto.setNomUsageHatvp(model.getEspaceOrganisation().getNomUsageHatvp());
		dto.setSigleHatvp(model.getEspaceOrganisation().getSigleHatvp());
		dto.setAncienNomHatvp(model.getEspaceOrganisation().getAncienNomHatvp());
		dto.setPremierePublicationDate(model.getEspaceOrganisation().getDatePremierePublication());
		

		if(model.getEspaceOrganisation().getStatut().equals(StatutEspaceEnum.DESINSCRIT )) {
            DesinscriptionEntity desinscription = desinscriptionRepository.findByEspaceOrganisation(model.getEspaceOrganisation());
		    dto.setDateCessation(Date.from(desinscription.getCessationDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));
			dto.setMotifDesinscription(desinscription.getMotif());
			dto.setMotivationDesinscription(desinscription.getObservationAgent());
		}
				
		return dto;
	}
}