/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.conf;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * classe de configuration d'envoi des emails.
 *
 * @version $Revision$ $Date${0xD}
 */
@Configuration
public class EmailConfiguration {

	@Value("${mail.transport.protocol}")
	private String mailTransportProtocol;

	@Value("${mail.smtp.host}")
	private String mailSmtpHost;

	@Value("${mail.smtp.port}")
	private String mailSmtpPort;

	@Value("${mail.smtp.auth}")
	private String mailSmtpAuth;

	@Value("${mail.smtp.automate.user}")
	private String mailSmtpAutomateUser;

	@Value("${mail.smtp.automate.password}")
	private String mailSmtpAutomatePassword;

	@Value("${mail.smtp.starttls.enable}")
	private String mailSmtpStarttlsEnable;

	@Value("${mail.smtp.debug}")
	private String mailSmtpDebug;

	@Value("${mail.mime.charset}")
	private String mailMimeCharset;

	/** Initialisation du bean d'envoi de mail. */
	@Bean
	public JavaMailSender javaMailService() {
		final JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
		javaMailSender.setJavaMailProperties(this.javaMailProperties());

		javaMailSender.setUsername(this.javaMailProperties().getProperty("mail.smtp.user"));
		javaMailSender.setPassword(this.javaMailProperties().getProperty("mail.smtp.pass"));

		return javaMailSender;
	}

	/** Chargement des propriétés des emails. */
	private Properties javaMailProperties() {
		final Properties properties = new Properties();
		properties.put("mail.transport.protocol", this.mailTransportProtocol);
		properties.put("mail.smtp.host", this.mailSmtpHost);
		properties.put("mail.smtp.port", this.mailSmtpPort);
		properties.put("mail.smtp.auth", this.mailSmtpAuth);
		properties.put("mail.smtp.user", this.mailSmtpAutomateUser);
		properties.put("mail.smtp.pass", this.mailSmtpAutomatePassword);
		properties.put("mail.smtp.starttls.enable", this.mailSmtpStarttlsEnable);
		properties.put("mail.debug", this.mailSmtpDebug);
		properties.put("mail.mime.charset", this.mailMimeCharset);

		return properties;
	}

}
