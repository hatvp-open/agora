

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonDirigeantTransformer;
import fr.hatvp.registre.persistence.entity.espace.DirigeantEntity;

/**
 * Interface de transformation de la classe {@link DirigeantEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface DirigeantTransformer
    extends CommonDirigeantTransformer<DirigeantEntity>
{


}
