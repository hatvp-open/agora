/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment.impl;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.transformer.backoffice.comment.AbstractCommentBoTransformer;
import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.commentaire.AbstractCommentEntity;
import fr.hatvp.registre.persistence.repository.backoffice.UtilisateurBoRepository;

/**
 *
 * Classe de transformation de la classe {@link AbstractCommentEntity}.
 *
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional(readOnly = true)
public abstract class AbstractCommentBoTransformerImpl<T extends CommentBoDto, E extends AbstractCommentEntity> extends AbstractCommonTansformer<T, E>
		implements AbstractCommentBoTransformer<T, E> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCommentBoTransformerImpl.class);

	/**
	 * Classe à définir dans les classe filles.
	 */
	protected Class<E> entityClass;

	/**
	 * Repository des utilisateurs backoffice.
	 */
	@Autowired
	private UtilisateurBoRepository utilisateurBoRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public E dtoToModel(final T dto) {
		final E entity;
		try {
			entity = this.entityClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			LOGGER.error("Erreur d'instanciation dans le transformeur des commentaires : {}", e);
			throw new BusinessGlobalException("Erreur d'instanciation dans le transformeur des commentaires");
		}

		RegistreBeanUtils.copyProperties(entity, dto);

		// Ajout de l'editeur du commentaire.
		final Authentication auth = Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
				.orElseThrow(() -> new EntityNotFoundException("Utilisateur Connecté introuvable"));

		final UtilisateurBoEntity utilisateurBoEntity = this.utilisateurBoRepository.findByEmail(((UserDetails) auth.getPrincipal()).getUsername())
				.orElseThrow(() -> new EntityNotFoundException("Utilisateur Connecté introuvable"));

		if (dto.getAuteur() == null) {
			entity.setEditeur(utilisateurBoEntity);
		} else {
			entity.setRectificateur(utilisateurBoEntity);
		}

		this.dtoToModelComplement(dto, entity);

		return entity;
	}

	abstract void dtoToModelComplement(T dto, E entity);

	abstract void modelToDtoComplement(E model, CommentBoDto dto);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentBoDto commentModelToDto(final E model) {
		final CommentBoDto commentBoDto = new CommentBoDto();
		RegistreBeanUtils.copyProperties(commentBoDto, model);
		commentBoDto.setAuteur(model.getEditeur().getNom() + " " + model.getEditeur().getPrenom());
		Optional.ofNullable(model.getRectificateur()).ifPresent(rect -> commentBoDto.setModificateur(rect.getNom() + " " + rect.getPrenom()));
		if (model.getDateModification() == null) {
			commentBoDto.setDateModification(model.getDateCreation());
		}
		this.modelToDtoComplement(model, commentBoDto);
		return commentBoDto;

	}
}