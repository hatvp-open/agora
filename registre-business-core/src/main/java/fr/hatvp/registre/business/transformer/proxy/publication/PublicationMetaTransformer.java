/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.publication.PublicationMetaDto;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

/**
 * Proxy du transformeur entre {@link PublicationEntity} et {@link PublicationMetaDto}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationMetaTransformer
    extends CommonTransformer<PublicationMetaDto, PublicationEntity>
{

}
