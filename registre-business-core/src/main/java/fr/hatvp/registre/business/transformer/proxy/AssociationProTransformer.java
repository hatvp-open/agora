

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonAssociationTransformer;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.persistence.entity.espace.AssociationEntity;

/**
 * Interface de transformation de la classe {@link AssociationEntity} &
 * {@link AssoClientDto}.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface AssociationProTransformer
		extends CommonAssociationTransformer<AssociationEntity> {
}
