/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.referentiel;

import java.util.Date;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.SireneApiTransformer;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.persistence.apiSirene.Etablissement;

/**
 * Classe de transformation de {@link OrganisationDto} et {@link Etablissement}
 */
@Component
public class SireneApiTransformerImpl
    extends AbstractCommonTansformer<OrganisationDto, Etablissement>
    implements SireneApiTransformer{

    public OrganisationDto modelToDto(final Etablissement sireneEntity){

        // Copie des champs de l'entité vers le dto
        final OrganisationDto orgaDto =  new OrganisationDto();

        orgaDto.setSiren(sireneEntity.getSiren());
        orgaDto.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
        // Ajout du RNA if exists

        orgaDto.setDenomination(sireneEntity.getUniteLegale().getCategorieJuridiqueUniteLegale().equals("1000")?sireneEntity.getUniteLegale().getNomUniteLegale()+" "+sireneEntity.getUniteLegale().getPrenom1UniteLegale():sireneEntity.getUniteLegale().getDenominationUniteLegale());
        orgaDto.setNomUsageSiren(sireneEntity.getUniteLegale().getNomUsageUniteLegale());
        orgaDto.setCodePostal(sireneEntity.getAdresseEtablissement().getCodePostalEtablissement());
        orgaDto.setVille(sireneEntity.getAdresseEtablissement().getLibelleCommuneEtablissement());
        orgaDto.setNationalId(sireneEntity.getSiren());

        if(sireneEntity.getAdresseEtablissement().getLibellePaysEtrangerEtablissement()!=null){
            orgaDto.setPays(sireneEntity.getAdresseEtablissement().getLibellePaysEtrangerEtablissement());
            if(sireneEntity.getAdresseEtablissement().getLibelleVoieEtablissement()!=null){
                orgaDto.setAdresse(sireneEntity.getAdresseEtablissement().getLibelleVoieEtablissement());
            }else{
                orgaDto.setAdresse(sireneEntity.getAdresseEtablissement().getLibelleCommuneEtrangerEtablissement());
            }

        }else{
            orgaDto.setPays("FRANCE");
            orgaDto.setAdresse(sireneEntity.getAdresseEtablissement().toString());
        }

	    orgaDto.setOrganizationSpaceExist(false);
	    orgaDto.setOrganisationExist(false);


        orgaDto.setDunsNumber(null);
        orgaDto.setHatvpNumber(null);
        orgaDto.setCreationDate(new Date());

        return orgaDto;
    }
}
