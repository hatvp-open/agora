/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantContactTransformer;
import fr.hatvp.registre.commons.dto.DeclarantContactDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantContactEntity;

/**
 * Classe de transformation de la classe {@link DeclarantContactEntity} &
 * {@link DeclarantContactDto}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class DeclarantContactTransformerImpl
        extends AbstractCommonTansformer<DeclarantContactDto, DeclarantContactEntity>
        implements DeclarantContactTransformer
{

    /**
     * {@inheritDoc}
     */
    @Override
    public DeclarantContactEntity dtoToModel(final DeclarantContactDto dto)
    {
        final DeclarantContactEntity model = new DeclarantContactEntity();
        RegistreBeanUtils.copyProperties(model, dto);
        model.setCategory(dto.getCategorie());
        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DeclarantContactDto modelToDto(final DeclarantContactEntity model)
    {
        final DeclarantContactDto dto = new DeclarantContactDto();
        RegistreBeanUtils.copyProperties(dto, model);
        dto.setCategorie(model.getCategory());
        return dto;
    }

}
