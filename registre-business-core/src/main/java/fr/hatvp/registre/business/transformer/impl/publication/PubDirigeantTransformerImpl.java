/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.CommonDirigeantTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.publication.PubDirigeantTransformer;
import fr.hatvp.registre.persistence.entity.publication.PubDirigeantEntity;


@Component
public class PubDirigeantTransformerImpl
    extends CommonDirigeantTransformerImpl<PubDirigeantEntity>
    implements PubDirigeantTransformer
{

    /** Init class. */
    public PubDirigeantTransformerImpl() {
        this.entityClass = PubDirigeantEntity.class;
    }
}
