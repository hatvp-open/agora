/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.security;

import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.InscriptionEspaceService;
import fr.hatvp.registre.business.security.EspaceOrganisationSecurityService;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implémentation du service {@link EspaceOrganisationService}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional(readOnly = true)
@Service
public class EspaceOrganisationSecurityServiceImpl
    implements EspaceOrganisationSecurityService
{

    /** Service pour les inscriptions à l'espace corporate. */
    @Autowired
    private InscriptionEspaceService inscriptionEspaceService;

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public void checkUserAuthorization(final Long currentEspaceOrganisationId, final Long currentUserId) {
        final InscriptionEspaceDto res = this.inscriptionEspaceService
            .getRegistrationByEspaceOrganisationIdAndDeclarantId(currentEspaceOrganisationId, currentUserId);

        if (res.getVerrouille() || res.getDateValidation() == null) {
            throw new BusinessGlobalException(ErrorMessageEnum.INTERDIT_ACCESS_INFOS_ESPACE.getMessage(),
                401);
        }
    }

}
