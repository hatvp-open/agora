/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;

/**
 * Interface de transformation de la classe {@link DeclarantEntity} & {@link DeclarantDto}.
 * 
 * @version $Revision$ $Date${0xD}
 */
public interface DeclarantTransformer
    extends CommonTransformer<DeclarantDto, DeclarantEntity>
{

}
