/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.DemandeChangementMandantTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationPieceTransformer;
import fr.hatvp.registre.commons.dto.DemandeChangementMandantDto;
import fr.hatvp.registre.persistence.entity.espace.DemandeChangementMandantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;

/**
 * Classe de transformation de la classe {@link DemandeChangementMandantEntity} & {@link DemandeChangementMandantDto}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class DemandeChangementMandantTransformerImpl extends AbstractCommonTansformer<DemandeChangementMandantDto, DemandeChangementMandantEntity>
		implements DemandeChangementMandantTransformer {

	/**
	 * Le transformer pour les pieces
	 */
	@Autowired
	private EspaceOrganisationPieceTransformer espaceOrganisationPieceTransformer;

	/**
	 * Le transformer pour les pieces
	 */
	@Autowired
	private DeclarantTransformer declarantTransformer;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DemandeChangementMandantEntity dtoToModel(final DemandeChangementMandantDto dto) {

		final DemandeChangementMandantEntity model = super.dtoToModel(dto);
		model.setDeclarant(declarantTransformer.dtoToModel(dto.getDeclarantDemande()));
		model.setMandatRepresentantLegal(
				dto.getMandatPieceRepresentantLegal() == null ? null : espaceOrganisationPieceTransformer.dtoToModel(dto.getMandatPieceRepresentantLegal()));
		model.setPieceIdentiteRepresentantLegal(
				dto.getIdentitePieceRepresentantLegal() == null ? null : espaceOrganisationPieceTransformer.dtoToModel(dto.getIdentitePieceRepresentantLegal()));
		model.setComplementPieceRepresentantLegal(Optional.ofNullable(dto.getComplementPieceRepresentantLegal()).orElse(new ArrayList<>()).stream()
				.map(piece -> espaceOrganisationPieceTransformer.dtoToModel(piece)).collect(Collectors.toList()));
		model.setEspaceOrganisation(new EspaceOrganisationEntity(dto.getEspaceOrganisationId()));
		model.setDeclarantDechu(new InscriptionEspaceEntity(dto.getDeclarantDechuInscriptionEspaceId()));
		model.setDeclarantPromu(new InscriptionEspaceEntity(dto.getDeclarantPromuInscriptionEspaceId()));

		return model;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public DemandeChangementMandantDto modelToDto(final DemandeChangementMandantEntity model) {

		if (model == null) {
			return null;
		}

		final DemandeChangementMandantDto result = super.modelToDto(model);
		result.setDeclarantDemande(model.getDeclarant() == null ? null : declarantTransformer.modelToDto(model.getDeclarant()));
		result.setMandatPieceRepresentantLegal(
				model.getMandatRepresentantLegal() == null ? null : espaceOrganisationPieceTransformer.modelToDto(model.getMandatRepresentantLegal()));
		result.setIdentitePieceRepresentantLegal(
				model.getPieceIdentiteRepresentantLegal() == null ? null : espaceOrganisationPieceTransformer.modelToDto(model.getPieceIdentiteRepresentantLegal()));
		result.setComplementPieceRepresentantLegal(
				model.getComplementPieceRepresentantLegal().stream().map(piece -> espaceOrganisationPieceTransformer.modelToDto(piece)).collect(Collectors.toList()));
		result.setEspaceOrganisationId(model.getEspaceOrganisation().getId());
		result.setRaisonSociale(model.getEspaceOrganisation() == null ? null
				: (model.getEspaceOrganisation().getOrganisation() == null ? null : model.getEspaceOrganisation().getOrganisation().getDenomination()));
		result.setDeclarantDechuInscriptionEspaceId(model.getDeclarantDechu() != null ? model.getDeclarantDechu().getId() : null);
		result.setMotifDemande(model.getMotifDemande());
		result.setStatut(model.getStatut());
		result.setDeclarantPromuInscriptionEspaceId(model.getDeclarantPromu() != null ? model.getDeclarantPromu().getId() : null);

		return result;
	}

}
