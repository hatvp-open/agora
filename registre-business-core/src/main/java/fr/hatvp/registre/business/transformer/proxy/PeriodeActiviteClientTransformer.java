/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.PeriodeActiviteClientDto;
import fr.hatvp.registre.persistence.entity.espace.PeriodeActiviteClientEntity;

/**
 * Proxy de transformation commun pour les PeriodeActiviteClient.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PeriodeActiviteClientTransformer extends CommonTransformer<PeriodeActiviteClientDto, PeriodeActiviteClientEntity> {
}
