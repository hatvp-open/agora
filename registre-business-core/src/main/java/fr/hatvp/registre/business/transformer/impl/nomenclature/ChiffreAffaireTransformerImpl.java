/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.nomenclature;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.proxy.nomenclature.ChiffreAffaireTransformer;
import fr.hatvp.registre.commons.dto.nomenclature.ChiffreAffaireDto;
import fr.hatvp.registre.persistence.entity.nomenclature.ChiffreAffaireEntity;

@Component
public class ChiffreAffaireTransformerImpl extends AbstractCommonNomenclatureTransformer<ChiffreAffaireDto, ChiffreAffaireEntity> implements ChiffreAffaireTransformer {

	/** Init super DTO. */
	public ChiffreAffaireTransformerImpl() {
		super.dtoClass = ChiffreAffaireDto.class;
	}
}
