/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment.impl;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.comment.CommentDemandeBoTransformer;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentDemandeEntity;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;
import fr.hatvp.registre.persistence.repository.espace.DemandeOrganisationRepository;

/**
 * Classe de transformation de la classe {@link fr.hatvp.registre.persistence.entity.commentaire.CommentDemandeEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CommentDemandeBoTransformerImpl extends AbstractCommentBoTransformerImpl<CommentBoDto, CommentDemandeEntity> implements CommentDemandeBoTransformer {

	/**
	 * Repository des demandes.
	 */
	@Autowired
	private DemandeOrganisationRepository demandeOrganisationRepository;

	/** Constructeur de la classe. */
	public CommentDemandeBoTransformerImpl() {
		super.entityClass = CommentDemandeEntity.class;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	void dtoToModelComplement(final CommentBoDto dto, final CommentDemandeEntity commentDemandeEntity) {
		// Ajout du déclatant commenté.
		final DemandeOrganisationEntity demandeEntity = Optional.ofNullable(this.demandeOrganisationRepository.findOne(dto.getContextId()))
				.orElseThrow(() -> new EntityNotFoundException("Demande id introuvable"));
		commentDemandeEntity.setDemandeOrganisationEntity(demandeEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	void modelToDtoComplement(final CommentDemandeEntity model, final CommentBoDto commentBoDto) {
		commentBoDto.setContextId(model.getDemandeOrganisationEntity().getId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentDemandeEntity dtoToModel(final CommentBoDto dto) {
		return super.dtoToModel(dto);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentBoDto commentModelToDto(final CommentDemandeEntity model) {
		return super.commentModelToDto(model);
	}
}