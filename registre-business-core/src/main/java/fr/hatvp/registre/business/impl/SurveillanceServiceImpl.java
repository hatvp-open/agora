/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.SurveillanceService;
import fr.hatvp.registre.business.transformer.backoffice.GroupBoTransformer;
import fr.hatvp.registre.business.transformer.backoffice.UtilisateurBoTransformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.OrganisationTransformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.RnaReferencielTransformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.SireneApiTransformer;
import fr.hatvp.registre.business.transformer.proxy.surveillance.SurveillanceOrganisationTransformer;
import fr.hatvp.registre.business.utils.SiretSirenRnaHatvpValidation;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.SurveillanceDetailDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.persistence.api.client.ApiInsee;
import fr.hatvp.registre.persistence.apiSirene.Etablissement;
import fr.hatvp.registre.persistence.apiSirene.ListeEtablissements;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielRnaEntity;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.SurveillanceRepository;
import fr.hatvp.registre.persistence.repository.referenciel.ReferencielRnaRepository;

@Service
public class SurveillanceServiceImpl  implements SurveillanceService {
	private static final Logger LOGGER = LoggerFactory.getLogger(SurveillanceServiceImpl.class);

    @Autowired
    private SurveillanceRepository surveillanceRepository;

    @Autowired
    private OrganisationRepository organisationRepository;

    @Autowired
    private EspaceOrganisationRepository espaceOrganisationRepository;

    @Autowired
    private ReferencielRnaRepository referencielRnaRepository;

    @Autowired
    private SurveillanceOrganisationTransformer surveillanceOrganisationTransformer;

    @Autowired
    private OrganisationTransformer organisationTransformer;

    @Autowired
    private RnaReferencielTransformer rnaReferencielTransformer;

    @Autowired
    private UtilisateurBoTransformer utilisateurBoTransformer;

    @Autowired
    private GroupBoTransformer groupBoTransformer;

    @Autowired
    private SireneApiTransformer sireneApiTransformer;
    
    
    @Override
    @Transactional
    public void saveSurveillance(SurveillanceDetailDto surveillance) {
        OrganisationEntity orga = this.organisationRepository.findOne(surveillance.getOrganisationId());
        SurveillanceEntity entity = this.surveillanceOrganisationTransformer.dtoToModel(surveillance);
        entity.setOrganisationEntity(orga);
        entity.setDateSurveillance(LocalDateTime.now());
        surveillanceRepository.save(entity);

    }

    @Override
    @Transactional
    public SurveillanceDetailDto updateSurveillance(SurveillanceDetailDto surveillance) {
        SurveillanceEntity entity = Optional.ofNullable(this.surveillanceRepository.findOne(surveillance.getId()))
            .orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.SURVEILLANCE_INTROUVABLE.getMessage()));
        entity.setMotif(surveillance.getMotif());
        entity.setTypeSurveillance(surveillance.getTypeSurveillance());
        entity.setSurveillants(new HashSet<>(this.utilisateurBoTransformer.dtoToModel(surveillance.getSurveillants())));
        entity.setSurveillantsGroupes(new HashSet<>(this.groupBoTransformer.dtoToModel(surveillance.getSurveillantsGroupes())));
        entity.setDateSurveillance(LocalDateTime.now());
        return this.surveillanceOrganisationTransformer.modelToDto(surveillanceRepository.save(entity));

    }
    @Override
    @Transactional
    public void deleteSurveillance(Long surveillanceId) {

        SurveillanceEntity entity = this.surveillanceRepository.findOne(surveillanceId);

        surveillanceRepository.delete(entity);

    }

    @Override
    @Transactional
    public PaginatedDto<SurveillanceDetailDto> getSurveillancesPaginated(final Integer page, final Integer occurence) {
        PageRequest pageRequest = new PageRequest(page - 1, occurence, Sort.Direction.ASC, "id");
        Page<SurveillanceEntity> entities = this.surveillanceRepository.findAll(pageRequest);
        LOGGER.info("surveillance chargées");
        List<SurveillanceDetailDto> dtos = entities.getContent().stream().map(this.surveillanceOrganisationTransformer::modelToDto).collect(Collectors.toList());
        dtos.stream().forEach(e -> e.setStatut(this.espaceOrganisationRepository.findStatutByOrganisationId(e.getOrganisationId())));
        return new PaginatedDto<>(dtos, entities.getTotalElements());
    }

    @Override
    @Transactional
    public List<SurveillanceDetailDto> getSurveillances() {
        List<SurveillanceEntity> entities = this.surveillanceRepository.findAll();
        List<SurveillanceDetailDto> dtos = entities.stream().map(this.surveillanceOrganisationTransformer::modelToDto).collect(Collectors.toList());
        dtos.stream().forEach(e -> e.setStatut(this.espaceOrganisationRepository.findStatutByOrganisationId(e.getOrganisationId())));
        return dtos;
    }
    @Override
    @Transactional
    public PaginatedDto<SurveillanceDetailDto> getSurveillancePaginatedBySearch(String type, String searchTerm, Integer pageNumber, Integer resultPerPage) {
        PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
        Page<SurveillanceEntity> pageResult;
        switch (type) {
            case "denomination":
                pageResult = this.surveillanceRepository.findByOrganisationEntityDenominationIgnoreCaseContaining("%" + searchTerm + "%", pageRequest);

                break;
            case "nomUsage":
                pageResult = this.surveillanceRepository.findByOrganisationEntityNomUsageIgnoreCaseContaining("%" + searchTerm + "%", pageRequest);
                break;
            case "dateSurveillance":
                LocalDateTime LDT = LocalDateTime.parse(searchTerm+" 00:00:00", DateTimeFormatter.ofPattern("ddMMyyyy HH:mm:ss"));
                LocalDateTime LDT1 = LocalDateTime.parse(searchTerm+" 23:59:59", DateTimeFormatter.ofPattern("ddMMyyyy HH:mm:ss"));
                pageResult = this.surveillanceRepository.findByDateSurveillance(LDT,LDT1,pageRequest);

                break;
            case "statut":
                if (StatutEspaceEnum.valueOf(searchTerm) == StatutEspaceEnum.ORGANISATION_SANS_ESPACE){
                    pageResult = this.surveillanceRepository.findByOrganisationEntityAndEspaceIsNull(pageRequest);
                }else{
                    pageResult = this.surveillanceRepository.findByOrganisationEntityStatut(StatutEspaceEnum.valueOf(searchTerm), pageRequest);
                }
                break;
            case "motif":
                pageResult = this.surveillanceRepository.findByMotifIgnoreCaseContaining("%"+searchTerm+"%",pageRequest);

                break;
            default:
                pageResult =this.surveillanceRepository.findAll(pageRequest);
        }
        List<SurveillanceDetailDto>  listeDto = pageResult.getContent().stream().map(this.surveillanceOrganisationTransformer::modelToDto).collect(Collectors.toList());
        listeDto.stream().forEach(e -> e.setStatut(this.espaceOrganisationRepository.findStatutByOrganisationId(e.getOrganisationId())));
        return new PaginatedDto<>(listeDto, pageResult.getTotalElements());
    }


    @Override
    @Transactional
    public List<SurveillanceDetailDto> verifyListOrganisation(List<String> nationalIds) {
        List<SurveillanceDetailDto> surveillanceList = new ArrayList<>();
        for (String nationalId : nationalIds) {
            nationalId = StringUtils.deleteWhitespace(nationalId);
            OrganisationDto orgaDto;
            OrganisationEntity orgaEntity;
            SurveillanceDetailDto surveillance = null;
            List<OrganisationEntity> o = this.organisationRepository.findBySirenOrRnaOrHatvpNumber(nationalId, nationalId, nationalId);
            if (o.isEmpty()) {
                SiretSirenRnaHatvpValidation validator = new SiretSirenRnaHatvpValidation(nationalId);
                switch (validator.getType()) {
                    case "SIREN":
                    	ApiInsee apiInsee = new ApiInsee();
                        ListeEtablissements listeEtablissements = apiInsee.consumeSireneApi("siren",nationalId);
                        
                        if (listeEtablissements.getEtablissements().isEmpty()) {
                        	throw new BusinessGlobalException("Le numero SIREN n'existe pas", 404);
                        }
                        
                     // On récupère l'établissement du siège social
                        Etablissement etablissement = new Etablissement();
                        String nicSiegeUniteLegale = "";
                        for(Etablissement etab : listeEtablissements.getEtablissements()){
                            nicSiegeUniteLegale = etab.getUniteLegale().getNicSiegeUniteLegale();
                            if(etab.getEtablissementSiege().equals("true")){
                                etablissement = etab;
                            }
                        }
                        if(etablissement.getSiren() == null){
                            listeEtablissements = apiInsee.consumeSireneApi("siret",nationalId+nicSiegeUniteLegale);
                            for(Etablissement etabSiege : listeEtablissements.getEtablissements()){
                                etablissement = etabSiege;
                            }
                        }
    					orgaDto = Arrays.asList(sireneApiTransformer.modelToDto(etablissement)).get(0);                    
                        orgaEntity = this.organisationTransformer.dtoToModel(orgaDto);
                        orgaEntity.setId(null);
                        orgaEntity = this.organisationRepository.save(orgaEntity);
                        surveillance = this.getSurveillance(orgaEntity.getId());
                        break;
                    case "RNA":
                        List<ReferentielRnaEntity> rna = this.referencielRnaRepository.findByRna(nationalId);
                        if (rna.isEmpty()) {
                            throw new BusinessGlobalException("Le numero RNA n'existe pas", 404);
                        }
                        orgaDto = this.rnaReferencielTransformer.modelToDto(rna.get(0));
                        orgaEntity = this.organisationTransformer.dtoToModel(orgaDto);
                        orgaEntity.setId(null);
                        orgaEntity = this.organisationRepository.save(orgaEntity);
                        surveillance = this.getSurveillance(orgaEntity.getId());
                        break;
                    case "HATVP":
                        // Should never happen
                        throw new BusinessGlobalException("Identifiant HATVP non valide", 404);
                    case "WRONGNUMBER":
                        throw new BusinessGlobalException("Identifiant SIREN non valide", 404);
                    case "NOTVALID":
                        throw new BusinessGlobalException("Identifiant RNA non valide", 404);
                    default:
                        break;
                }
            } else {
                orgaEntity = o.get(0);
                surveillance = this.getSurveillance(orgaEntity.getId());
            }

            // should never happen
            if (surveillance == null) {
                throw new BusinessGlobalException("erreur inconnue", 404);
            }

            surveillanceList.add(surveillance);
        }
        return surveillanceList;
    }

    @Override
    @Transactional
    public void saveListeSurveillance(List<SurveillanceDetailDto> listSurveillance) {
        for (SurveillanceDetailDto surveillance : listSurveillance) {
            if (surveillance.getId()!=null) this.updateSurveillance(surveillance);
            else this.saveSurveillance(surveillance);
            LOGGER.info("Mise sous surveillance OrganisationId = {} ", surveillance.getOrganisationId());
        }
        this.organisationRepository.flush();
    }

    @Override
    @Transactional
    public SurveillanceDetailDto getSurveillance(Long organisationId) {
        OrganisationEntity orga = this.organisationRepository.findOne(organisationId);
        return this.surveillanceRepository.findByOrganisationEntity(orga)!=null? this.surveillanceOrganisationTransformer.modelToDto(this.surveillanceRepository.findByOrganisationEntityId(organisationId)): new SurveillanceDetailDto(organisationId ,orga.getDenomination() ,orga.getNomUsageSiren()) ;
    }


}
