/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceDeclarantPieceTransformer;
import fr.hatvp.registre.commons.dto.EspaceDeclarantPieceDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.piece.EspaceDeclarantPieceEntity;
import org.springframework.stereotype.Component;

/**
 * Implémentation du transformer pour les pièces du déclarant.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Component
public class EspaceDeclarantPieceTransformerImpl
    extends AbstractCommonTansformer<EspaceDeclarantPieceDto, EspaceDeclarantPieceEntity>
    implements EspaceDeclarantPieceTransformer
{

    /**
     * {@inheritDoc}
     */
    @Override
    public EspaceDeclarantPieceEntity dtoToModel(final EspaceDeclarantPieceDto dto)
    {
        final EspaceDeclarantPieceEntity model = new EspaceDeclarantPieceEntity();
        RegistreBeanUtils.copyProperties(model, dto);
        
        return model;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public EspaceDeclarantPieceDto modelToDto(final EspaceDeclarantPieceEntity model)
    {
        final EspaceDeclarantPieceDto dto = new EspaceDeclarantPieceDto();
        RegistreBeanUtils.copyProperties(dto, model);
        dto.setDeclarantId(model.getDeclarant().getId());
        return dto;
    }
}
