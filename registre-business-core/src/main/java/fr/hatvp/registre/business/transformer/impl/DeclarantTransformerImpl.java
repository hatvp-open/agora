/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantContactTransformer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantContactEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;

/**
 * Classe de transformation de la classe {@link DeclarantEntity} &
 * {@link DeclarantDto}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class DeclarantTransformerImpl
    extends AbstractCommonTansformer<DeclarantDto, DeclarantEntity>
    implements DeclarantTransformer
{

    /**
     * Transformer des déclarants contacts.
     */
    @Autowired
    private DeclarantContactTransformer declarantContactTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    public DeclarantEntity dtoToModel(final DeclarantDto dto)
    {
        final DeclarantEntity model = new DeclarantEntity();

        model.setId(dto.getId());
        model.setVersion(dto.getVersion());
        model.setPrenom(dto.getPrenom());
        model.setNom(dto.getNom());
        model.setBirthDate(dto.getBirthDate());
        model.setEmail(dto.getEmail());
        model.setEmailTemp(dto.getEmailTemp());
        model.setTelephone(dto.getTelephone());
        model.setCivility(dto.getCivility());
        model.setPassword(dto.getPassword());
        model.setActivated(dto.isActivated());
        model.setEmailConfirmationCode(dto.getActivationEmailKeyCode());
        model.setRecoverConfirmationCode(dto.getRecuperationPasswordKeyCode());

        // Récupération des contacts
        final List<DeclarantContactEntity> contacts = new ArrayList<>();
        contacts.addAll(this.declarantContactTransformer.dtoToModel(dto.getEmailComplement()));
        contacts.addAll(this.declarantContactTransformer.dtoToModel(dto.getPhone()));
        model.setContacts(new HashSet<>(contacts));

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public DeclarantDto modelToDto(final DeclarantEntity model)
    {
        if (model == null) {
            return null;
        }
        final DeclarantDto result = new DeclarantDto();

        result.setId(model.getId());
        result.setVersion(model.getVersion());
        result.setPrenom(model.getPrenom());
        result.setNom(model.getNom());
        result.setBirthDate(model.getBirthDate());
        result.setEmail(model.getEmail());
        result.setEmailTemp(model.getEmailTemp());
        result.setTelephone(model.getTelephone());
        result.setCivility(model.getCivility());
        result.setPassword(model.getPassword());
        result.setActivated(model.isActivated());
        result.setFirstConn(model.getFirstConn());
        result.setActivationEmailKeyCode(model.getEmailConfirmationCode());
        result.setRecuperationPasswordKeyCode(model.getRecoverConfirmationCode());
        result.setLastConnexionDate(model.getLastConnexionDate());

        /*
         * Gestion des contacts
         */
        final Set<DeclarantContactEntity> list = model.getContacts();

        // Récupération de la liste des email
        final List<DeclarantContactEntity> emails = list.stream()
                .filter(elt -> elt.getEmail() != null).collect(Collectors.toList());
        result.setEmailComplement(this.declarantContactTransformer.modelToDto(emails));

        // Récupération de la liste des email
        final List<DeclarantContactEntity> phones = list.stream()
                .filter(elt -> elt.getTelephone() != null).collect(Collectors.toList());
        result.setPhone(this.declarantContactTransformer.modelToDto(phones));

        if (model.getInscriptionEspaces() != null) {
            // Récupération des roles de l'espace favori
            final Optional<InscriptionEspaceEntity> roleEnumsFavEspace = model
                    .getInscriptionEspaces().stream().filter(InscriptionEspaceEntity::isFavori)
                    .findFirst();

            roleEnumsFavEspace.ifPresent(inscriptionEspaceEntity -> result
                    .setInscriptionEspaceFavRoles(inscriptionEspaceEntity.getRolesFront()));
        }

        return result;
    }

}
