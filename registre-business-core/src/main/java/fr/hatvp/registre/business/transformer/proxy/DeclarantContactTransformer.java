/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.DeclarantContactDto;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantContactEntity;

/**
 * Interface de transformation de la classe {@link DeclarantContactEntity} &
 * {@link DeclarantContactDto}.
 * 
 * @version $Revision$ $Date${0xD}
 */
public interface DeclarantContactTransformer
    extends CommonTransformer<DeclarantContactDto, DeclarantContactEntity>
{

}
