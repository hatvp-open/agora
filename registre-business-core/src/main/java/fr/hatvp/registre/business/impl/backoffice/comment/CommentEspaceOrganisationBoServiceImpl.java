/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice.comment;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.backoffice.comment.CommentEspaceOrganisationBoService;
import fr.hatvp.registre.business.transformer.backoffice.comment.CommentEspaceOrganisationBoTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentEspaceOrganisationEntity;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentEspaceOrganisationRepository;

/**
 * Classe d'implémentation de gestion des commentaires des espace organisations.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CommentEspaceOrganisationBoServiceImpl extends CommonCommentServiceImpl<CommentBoDto, CommentEspaceOrganisationEntity> implements CommentEspaceOrganisationBoService {

	/**
	 * Repository de la classe.
	 */
	private final CommentEspaceOrganisationRepository commentEspaceOrganisationRepository;

	/**
	 * Transformeur de la classe.
	 */
	private final CommentEspaceOrganisationBoTransformer commentEspaceOrganisationBoTransformer;

	/**
	 * Initialisation des dépendances.
	 */
	@Autowired
	public CommentEspaceOrganisationBoServiceImpl(final CommentEspaceOrganisationRepository commentEspaceOrganisationRepository,
			final CommentEspaceOrganisationBoTransformer commentEspaceOrganisationBoTransformer) {
		this.commentEspaceOrganisationRepository = commentEspaceOrganisationRepository;
		this.commentEspaceOrganisationBoTransformer = commentEspaceOrganisationBoTransformer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CommentBoDto> getAllComment(final Long espaceId) {
		return this.commentEspaceOrganisationRepository.findByEspaceOrganisationEntityId(espaceId).stream().map(this.commentEspaceOrganisationBoTransformer::commentModelToDto)
				.sorted(Comparator.comparing(CommentBoDto::getDateModification)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public CommentBoDto addComment(final CommentBoDto commentBoDto) {
		final CommentEspaceOrganisationEntity entity = this.getTransformer().dtoToModel(commentBoDto);
		entity.setDateCreation(new Date());
		return this.commentEspaceOrganisationBoTransformer.commentModelToDto(this.commentEspaceOrganisationRepository.save(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public CommentBoDto updateComment(final CommentBoDto commentBoDto) {
		final CommentEspaceOrganisationEntity entityT = this.getTransformer().dtoToModel(commentBoDto);
		final CommentEspaceOrganisationEntity entityR = this.getRepository().findOne(commentBoDto.getId());
		entityT.setEditeur(entityR.getEditeur());
		entityT.setDateCreation(entityR.getDateCreation());
		entityT.setDateModification(new Date());
		return this.commentEspaceOrganisationBoTransformer.commentModelToDto(this.commentEspaceOrganisationRepository.save(entityT));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<CommentEspaceOrganisationEntity, Long> getRepository() {
		return this.commentEspaceOrganisationRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<CommentBoDto, CommentEspaceOrganisationEntity> getTransformer() {
		return this.commentEspaceOrganisationBoTransformer;
	}
}
