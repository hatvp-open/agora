/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.InscriptionEspaceTransformer;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.commons.lists.StatutEspaceAffichageEnum;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;

/**
 * Classe de transformation entre {@link InscriptionEspaceDto} et {@link InscriptionEspaceEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class InscriptionEspaceTransformerImpl extends AbstractCommonTansformer<InscriptionEspaceDto, InscriptionEspaceEntity> implements InscriptionEspaceTransformer {

	/**
	 * Transformer de l'entité {@link fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity}.
	 */
	private final DeclarantTransformer declarantTransformer;

	/**
	 * Transformer de l'entité {@link fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity}.
	 */
	private final InscriptionEspaceRepository inscriptionEspaceRepository;
	
	private final DesinscriptionRepository desinscriptionRepository;
	
	private final PublicationRepository publicationRepository;
	

	/**
	 * Initialisation des services.
	 */
	@Autowired
	public InscriptionEspaceTransformerImpl(final DeclarantTransformer declarantTransformer, final InscriptionEspaceRepository inscriptionEspaceRepository, 
			DesinscriptionRepository desinscriptionRepository, PublicationRepository publicationRepository) {
		this.declarantTransformer = declarantTransformer;
		this.inscriptionEspaceRepository = inscriptionEspaceRepository;
		this.desinscriptionRepository = desinscriptionRepository;
		this.publicationRepository = publicationRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InscriptionEspaceEntity dtoToModel(final InscriptionEspaceDto dto) {
		final InscriptionEspaceEntity model = new InscriptionEspaceEntity();
		RegistreBeanUtils.copyProperties(model, dto);

		return model;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InscriptionEspaceDto modelToDto(final InscriptionEspaceEntity model) {
		InscriptionEspaceDto dto = new InscriptionEspaceDto();
		RegistreBeanUtils.copyProperties(dto, model);
		dto.setEspaceOrganisationId(model.getEspaceOrganisation().getId());
		dto.setOrganisationDenomination(model.getEspaceOrganisation().getOrganisation().getDenomination());
		dto.setNomUsage(model.getEspaceOrganisation().getNomUsage());

		dto.setDeclarantId(model.getDeclarant().getId());
		dto.setDeclarantNomComplet(model.getDeclarant().getCivility().getLibelleCourt() + " " + model.getDeclarant().getPrenom() + " " + model.getDeclarant().getNom());
		dto.setDeclarantEmail(model.getDeclarant().getEmail());
		dto.setDeclarantTelephone(model.getDeclarant().getTelephone());

		// Définir l'identifiant national suivant son origine dans la table organisation.
		dto.setOrganisationIdentifiantNational(super.determineIdentifiantNational(model.getEspaceOrganisation().getOrganisation()));

		dto.setOrganizationId(model.getEspaceOrganisation().getOrganisation().getId());

		if (model.getRolesFront() != null) {
			dto.setRoles(model.getRolesFront().stream().map(Enum::name).collect(Collectors.toSet()));
		}

		dto.setDeclareForTiers(model.getEspaceOrganisation().isNonDeclarationTiers());
		dto.setEspaceOrganisationActif(model.getEspaceOrganisation().isEspaceActif());
		dto.setEspaceOrganisationDateCreation(model.getEspaceOrganisation().getCreationDate());
		dto.setFinExerciceFiscal(model.getEspaceOrganisation().getFinExerciceFiscal());

		// mandat download URL
		dto.setEspaceOrganisationMandat("/corporate/" + model.getEspaceOrganisation().getId() + "/mandat");

		// récupération des contacts opérationnels de l'espace organisation.
		final List<InscriptionEspaceEntity> espaceEntityDeclarantList = this.inscriptionEspaceRepository
				.findAllByActifTrueAndDateSuppressionIsNullAndEspaceOrganisationId(model.getEspaceOrganisation().getId());	
		
		dto.setSatutDesinscription(getSatutDesinscription(model));
		
		dto = getStatutEspaceAffichage(model.getEspaceOrganisation(), dto);
		
		// Contacts opérationnels de l'espace organisation.
		dto.setAdministrateurs(espaceEntityDeclarantList.stream().filter(i -> i.getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR))
				.map(InscriptionEspaceEntity::getDeclarant).map(this.declarantTransformer::modelToDto).collect(Collectors.toList()));
		
		dto.setIdentiteIsValide(getValiditeIdentite(model));
		dto.setDirigeantIsValide(getValiditeDirigeant(model));
		dto.setEquipeIsValide(getValiditeEquipe(model));
		dto.setClientIsValide(getValiditeClient(model));
		dto.setAffiliationIsValide(getValiditeAffiliation(model));
		dto.setChampActiviteIsValide(getValiditeChampActivite(model));
		dto.setInformationManquante(getInformationManquante(dto));

		return dto;
	}
	
	/**
	 * VALIDEE si: DESINSCRIPTION_VALIDEE_HATVP
	 * DESINSCRIT si: DESINSCRIT
	 * DEMANDEE si: DESINCRIPTION_DEMANDEE ou COMPLEMENTS_DEMANDE_DESINSCRIPTION_RECU ou COMPLEMENTS_DEMANDE_DESINSCRIPTION
	 * @param model
	 * @return
	 */
	public String getSatutDesinscription(InscriptionEspaceEntity model) {
		final DesinscriptionEntity desinscriptionEntity = this.desinscriptionRepository.findByEspaceOrganisation(model.getEspaceOrganisation());
		
		if(desinscriptionEntity == null) return null;
		
		if(model.getEspaceOrganisation().getStatut().equals(StatutEspaceEnum.DESINSCRIPTION_VALIDEE_HATVP)){
			return "VALIDEE";
		}else if(model.getEspaceOrganisation().getStatut().equals(StatutEspaceEnum.DESINSCRIT)) {
			return "DESINSCRIT";			
		}else {
			return "DEMANDEE";
		}	
	
	}
	
	public boolean getValiditeIdentite(InscriptionEspaceEntity model) {
		
		if(model.getEspaceOrganisation().getCategorieOrganisation() == null) return false;
		if((model.getEspaceOrganisation().getFinExerciceFiscal() == null || "".equals(model.getEspaceOrganisation().getFinExerciceFiscal()))
				&& (Boolean.FALSE.equals(model.getEspaceOrganisation().getNonExerciceComptable()) || model.getEspaceOrganisation().getNonExerciceComptable() == null )) return false;
		if(model.getEspaceOrganisation().getAdresse() == null) return false;
		if(model.getEspaceOrganisation().getCodePostal() == null) return false;
		if(model.getEspaceOrganisation().getVille() == null) return false;
		if(model.getEspaceOrganisation().getPays() == null) return false;
		
		return true;
	}
	public boolean getValiditeDirigeant(InscriptionEspaceEntity model) {
		
		return !model.getEspaceOrganisation().getDirigeants().isEmpty();

	}
	public boolean getValiditeEquipe(InscriptionEspaceEntity model) {
		
		return !model.getEspaceOrganisation().getListeDesCollaborateurs().isEmpty();

	}
	
	public boolean getValiditeClient(InscriptionEspaceEntity model) {
		
		return !(model.getEspaceOrganisation().getClients().isEmpty() && Boolean.FALSE.equals(model.getEspaceOrganisation().isNonDeclarationTiers()));

	}
	
	public boolean getValiditeAffiliation(InscriptionEspaceEntity model) {
		
		return !(model.getEspaceOrganisation().getOrganisationProfessionnelles().isEmpty() && Boolean.FALSE.equals(model.getEspaceOrganisation().isNonDeclarationOrgaAppartenance()));

	}
	public boolean getValiditeChampActivite(InscriptionEspaceEntity model) {
		
		return model.getEspaceOrganisation().getSecteursActivites() != null && model.getEspaceOrganisation().getNiveauIntervention() != null;

	}
	
	public boolean getInformationManquante(InscriptionEspaceDto inscriptionEspaceDto) {		
		return !(inscriptionEspaceDto.isIdentiteIsValide() && inscriptionEspaceDto.isDirigeantIsValide() 
				&& inscriptionEspaceDto.isEquipeIsValide() && inscriptionEspaceDto.isClientIsValide()
				&& inscriptionEspaceDto.isAffiliationIsValide() && inscriptionEspaceDto.isChampActiviteIsValide());
	}
	
	/**
	 * Calcule le satut de l'espace à afficher coté FO hors désinscription
	 * @param espaceOrganisationEntity
	 * @param dto
	 * @return
	 */
	public InscriptionEspaceDto getStatutEspaceAffichage(EspaceOrganisationEntity espaceOrganisationEntity,InscriptionEspaceDto dto) {
		if(StatutEspaceEnum.ACCEPTEE.equals(espaceOrganisationEntity.getStatut())) {
			
			List<PublicationEntity> listPublication = this.publicationRepository.findAllByEspaceOrganisationIdOrderByIdDesc(espaceOrganisationEntity.getId());
			
			if(listPublication.isEmpty()) {
				dto.setStatutEspaceAffichage(StatutEspaceAffichageEnum.ACCEPTEE_NON_PUBLIEE);
				
			}else if(listPublication.size() == 1 && Boolean.FALSE.equals(espaceOrganisationEntity.getNewPublication())) {
				dto.setStatutEspaceAffichage(StatutEspaceAffichageEnum.ACCEPTEE_PUBLIEE);
				dto.setDatePublicationAffichage(listPublication.get(0).getCreationDate());
				
			}else {
				if(Boolean.FALSE.equals(espaceOrganisationEntity.getNewPublication())) {
					dto.setStatutEspaceAffichage(StatutEspaceAffichageEnum.MAJ_PUBLIEE);
					dto.setDatePublicationAffichage(listPublication.get(0).getCreationDate());
					
				}else {
					dto.setStatutEspaceAffichage(StatutEspaceAffichageEnum.MAJ_NON_PUBLIEE);
				}
			}			
		}
		return dto;	
	}

}