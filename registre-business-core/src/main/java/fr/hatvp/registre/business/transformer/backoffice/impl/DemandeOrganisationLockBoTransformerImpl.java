/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.DemandeOrganisationLockBoTransformer;
import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.commons.dto.backoffice.DemandeOrganisationLockBoDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.backoffice.DemandeOrganisationLockBoEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;

/**
 * Classe de transformation de la classe {@link DemandeOrganisationLockBoEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class DemandeOrganisationLockBoTransformerImpl
    extends AbstractCommonTansformer<DemandeOrganisationLockBoDto, DemandeOrganisationLockBoEntity>
    implements DemandeOrganisationLockBoTransformer
{
    /**
     * Durée Timeout lock d'entité (en secondes)
     */
    @Value("${app.lock.demandeorganisation.timeout}")
    private long lockTimeout;

    /**
     * {@inheritDoc}
     */
    @Override
    public DemandeOrganisationLockBoEntity dtoToModel(final DemandeOrganisationLockBoDto dto)
    {
        final DemandeOrganisationLockBoEntity model = new DemandeOrganisationLockBoEntity();
        RegistreBeanUtils.copyProperties(model, dto);

        final DemandeOrganisationEntity demandeOrganisationEntity = new DemandeOrganisationEntity();
        demandeOrganisationEntity.setId(dto.getDemandeOrganisationId());
        model.setDemandeOrganisation(demandeOrganisationEntity);

        model.setLockedTime(dto.getLockTimeStart());

        final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
        utilisateurBoEntity.setId(dto.getUtilisateurBoId());
        model.setUtilisateurBo(utilisateurBoEntity);

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DemandeOrganisationLockBoDto modelToDto(final DemandeOrganisationLockBoEntity model)
    {
        final DemandeOrganisationLockBoDto dto = new DemandeOrganisationLockBoDto();
        RegistreBeanUtils.copyProperties(dto, model);

        dto.setDemandeOrganisationId(model.getDemandeOrganisation().getId());
        dto.setLockTimeStart(model.getLockedTime());

        final Long remainingTime = this.lockTimeout
                - ((new Date().getTime() - model.getLockedTime().getTime()) / 1000);
        dto.setLockTimeRemain(remainingTime > 0 ? remainingTime : 0);

        dto.setUtilisateurBoId(model.getUtilisateurBo().getId());
        dto.setUtilisateurBoName(
                model.getUtilisateurBo().getNom() + " " + model.getUtilisateurBo().getPrenom());

        return dto;
    }
}
