/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.activite;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.hatvp.registre.business.DeclarationActiviteService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.email.SurveillanceMailer;
import fr.hatvp.registre.business.pdf.activite.Activite;
import fr.hatvp.registre.business.transformer.proxy.activite.ActiviteRepresentationInteretTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.FicheBOTransformer;
import fr.hatvp.registre.business.utils.HumanReadableUniqueIdUtil;
import fr.hatvp.registre.commons.dto.activite.ActionRepresentationInteretJsonDto;
import fr.hatvp.registre.commons.dto.activite.ActiviteRepresentationInteretDto;
import fr.hatvp.registre.commons.dto.activite.ActiviteRepresentationInteretJsonDto;
import fr.hatvp.registre.commons.dto.activite.TiersDto;
import fr.hatvp.registre.commons.dto.backoffice.activite.ActionBackDto;
import fr.hatvp.registre.commons.dto.backoffice.activite.FicheBackDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.exceptions.TechnicalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.commons.utils.CompareBeans;
import fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ResponsablePublicEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;
import fr.hatvp.registre.persistence.repository.activite.ActiviteRepresentationInteretRepository;
import fr.hatvp.registre.persistence.repository.activite.ExerciceComptableRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationActiviteRepository;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.DomaineInterventionRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.ResponsablePublicRepository;

/**
 * Service de gestion des déclarations d'activitée.
 *
 */
@Transactional
@Service
public class DeclarationActiviteServiceImpl implements DeclarationActiviteService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DeclarationActiviteServiceImpl.class);

	@Autowired
	private ActiviteRepresentationInteretRepository activiteRepresentationInteretRepository;

	@Autowired
	private ExerciceComptableRepository exerciceComptableRepository;

	@Autowired
	private DeclarantRepository declarantRepository;

	@Autowired
	private ActiviteRepresentationInteretTransformer activiteRepresentationInteretTransformer;

	@Autowired
	private FicheBOTransformer ficheBOTransformer;

	@Autowired
	private SurveillanceMailer surveillanceMailer;
	
	@Autowired
	private EspaceOrganisationService espaceOrganisationService;
	
	@Autowired
	private DomaineInterventionRepository domaineInterventionRepository;
	
	@Autowired
	private ResponsablePublicRepository responsablePublicRepository;
	
	@Autowired
	private PublicationActiviteRepository publicationActiviteRepository;

	private HumanReadableUniqueIdUtil idUtil;
	// ne pas modifier les valeurs de génération d'id
	private static final String KEYWORD = "HATVP fiche";
	private static final int MIN_LENGTH = 8;
	private static final String DICTIONARY = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	private static final Long ACTION_MENEE_AUTRE = 10L;
	private static final Long RESPONSABLE_PUBLIC_AUTRE = 19L;

	public DeclarationActiviteServiceImpl() {
		this.idUtil = new HumanReadableUniqueIdUtil(KEYWORD, MIN_LENGTH, DICTIONARY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ActiviteRepresentationInteretDto getDeclarationActivite(final Long ficheId) {
		final ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = Optional.ofNullable(activiteRepresentationInteretRepository.findOne(ficheId))
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.DECLARATION_INTROUVABLE.getMessage()));

		return activiteRepresentationInteretTransformer.modelToDto(activiteRepresentationInteretEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ActiviteRepresentationInteretDto createDeclarationActivite(final ActiviteRepresentationInteretDto activiteRepresentationInteret, Long currentUserId,
			final Long currentEspaceOrganisationId) {
		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = activiteRepresentationInteretTransformer.dtoToModel(activiteRepresentationInteret);

		activiteRepresentationInteretEntity.setId(null);

		for (ActionRepresentationInteretEntity action : activiteRepresentationInteretEntity.getActionsRepresentationInteret()) {
			action.setId(null);
			action.setActiviteRepresentationInteret(activiteRepresentationInteretEntity);
		}

		ExerciceComptableEntity exerciceComptable = exerciceComptableRepository.getOne(activiteRepresentationInteret.getExerciceComptable());
		checkEspaceOrganisation(currentEspaceOrganisationId, exerciceComptable.getEspaceOrganisation().getId());
		activiteRepresentationInteretEntity.setExerciceComptable(exerciceComptable);

		activiteRepresentationInteretEntity.setCreateurDeclaration(declarantRepository.getOne(currentUserId));
		// par défaut une activité a un statut non publié
		activiteRepresentationInteretEntity.setStatut(StatutPublicationEnum.NON_PUBLIEE);

		activiteRepresentationInteretEntity = activiteRepresentationInteretRepository.save(activiteRepresentationInteretEntity);
		activiteRepresentationInteretEntity.setIdFiche(idUtil.encode(activiteRepresentationInteretEntity.getId()));

		activiteRepresentationInteretEntity.setDataToPublish(Boolean.TRUE);

		activiteRepresentationInteretRepository.flush();
        SurveillanceEntity surveillance = activiteRepresentationInteretEntity.getExerciceComptable().getEspaceOrganisation().getOrganisation().getSurveillanceEntity();

		if (surveillance!=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.CREATION_FICHE)) {
			this.surveillanceMailer.sendSurveillanceMail(activiteRepresentationInteretEntity.getExerciceComptable().getEspaceOrganisation().getOrganisation(),
					activiteRepresentationInteretEntity.getExerciceComptable().getEspaceOrganisation(), SurveillanceOrganisationEnum.CREATION_FICHE,new Activite(activiteRepresentationInteretEntity));
		}

		return activiteRepresentationInteretTransformer.modelToDto(activiteRepresentationInteretEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ActiviteRepresentationInteretDto updateDeclarationActivite(final ActiviteRepresentationInteretDto activiteRepresentationInteret, final Long currentEspaceOrganisationId) {

		checkTiersDuplicates(activiteRepresentationInteret);
		checkMissingOtherAction(activiteRepresentationInteret);
		checkMissingOtherPublicResponsable(activiteRepresentationInteret);

		final ActiviteRepresentationInteretEntity entityDB = Optional.ofNullable(activiteRepresentationInteretRepository.findOne(activiteRepresentationInteret.getId()))
				.orElseThrow(() -> new EntityNotFoundException(ErrorMessageEnum.NO_ACTIVITE.getMessage()));

		final ActiviteRepresentationInteretDto dtoDB = activiteRepresentationInteretTransformer.modelToDto(entityDB);
		String[] array = { "denomination", "idFiche", "statut" };
		boolean similar = false;
		try {
			similar = CompareBeans.compareModelObjects(dtoDB, activiteRepresentationInteret, array, array);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
			LOGGER.error("failed to compare fiche beans : {}", ex);
		}

		if (!similar) {
			checkEspaceOrganisation(currentEspaceOrganisationId, entityDB.getExerciceComptable().getEspaceOrganisation().getId());
			
			//la modification de l'exercice autorisée pour les activités NON_PUBLIEE et DEPUBLIEE
			if ((StatutPublicationEnum.SUPPRIMEE.equals(entityDB.getStatut()) ||StatutPublicationEnum.NON_PUBLIEE.equals(entityDB.getStatut()) || StatutPublicationEnum.DEPUBLIEE.equals(entityDB.getStatut())) 
					&& entityDB.getExerciceComptable().getId().longValue() != activiteRepresentationInteret.getExerciceComptable().longValue()) {
				entityDB.setExerciceComptable(exerciceComptableRepository.getOne(activiteRepresentationInteret.getExerciceComptable()));
			}
			
			if (!(StatutPublicationEnum.NON_PUBLIEE).equals(entityDB.getStatut()) && !(StatutPublicationEnum.SUPPRIMEE).equals(entityDB.getStatut())) {
				entityDB.setStatut(StatutPublicationEnum.MAJ_NON_PUBLIEE);
			}

			final ActiviteRepresentationInteretEntity entityRest = activiteRepresentationInteretTransformer.dtoToModel(activiteRepresentationInteret);

			final Map<Long, ActionRepresentationInteretEntity> mapToUpdate = new HashMap<>();
			final List<ActionRepresentationInteretEntity> listToAdd = new ArrayList<>();
			final List<ActionRepresentationInteretEntity> listToRemove = new ArrayList<>();

			entityRest.getActionsRepresentationInteret().stream().forEach(rest -> {
				if (rest.getId() != null) {
					rest.setActiviteRepresentationInteret(entityDB);
					mapToUpdate.put(rest.getId(), rest);
				} else {
					listToAdd.add(rest);
					rest.setActiviteRepresentationInteret(entityDB);
				}
			});

			entityDB.getActionsRepresentationInteret().stream().forEach(db -> {
				if (mapToUpdate.containsKey(db.getId())) {
					db.setActionsMenees(mapToUpdate.get(db.getId()).getActionsMenees());
					db.setActionMeneeAutre(mapToUpdate.get(db.getId()).getActionMeneeAutre());
					db.setDecisionsConcernees(mapToUpdate.get(db.getId()).getDecisionsConcernees());
					db.setReponsablesPublics(mapToUpdate.get(db.getId()).getReponsablesPublics());
					db.setResponsablePublicAutre(mapToUpdate.get(db.getId()).getResponsablePublicAutre());
					db.setTiers(mapToUpdate.get(db.getId()).getTiers());
					db.setObservation(mapToUpdate.get(db.getId()).getObservation());
				} else {
					listToRemove.add(db);
				}
			});

			entityDB.getActionsRepresentationInteret().removeAll(listToRemove);
			entityDB.getActionsRepresentationInteret().addAll(listToAdd);

			entityDB.setDomainesIntervention(entityRest.getDomainesIntervention());
			entityDB.setObjet(entityRest.getObjet());

			entityDB.setDataToPublish(Boolean.TRUE);

			return activiteRepresentationInteretTransformer.modelToDto(activiteRepresentationInteretRepository.save(entityDB));
		} else {
			return activiteRepresentationInteret;
		}
	}

	private void checkMissingOtherPublicResponsable(final ActiviteRepresentationInteretDto activiteRepresentationInteret) {
		activiteRepresentationInteret.getActionsRepresentationInteret().stream().filter(ar -> ar.getReponsablesPublics().contains(RESPONSABLE_PUBLIC_AUTRE)).forEach(ar -> {
			if (ar.getResponsablePublicAutre() == null || ar.getResponsablePublicAutre().isEmpty()) {
				throw new BusinessGlobalException(ErrorMessageEnum.RESPONSABLE_PUBLIC_AUTRE_NON_RENSEIGNEE.getMessage());
			}
		});
	}

	private void checkEspaceOrganisation(final Long currentEspaceOrganisationId, final Long exerciceComptableEspaceId) {
		if (currentEspaceOrganisationId.longValue() != exerciceComptableEspaceId.longValue()) {
			throw new BusinessGlobalException(ErrorMessageEnum.MAUVAIS_ESPACE.getMessage());
		}
	}

	private void checkTiersDuplicates(final ActiviteRepresentationInteretDto activiteRepresentationInteret) {
		// vérification des tiers en doublons si plusieurs actions ont été déclarées
		if (activiteRepresentationInteret.getActionsRepresentationInteret().size() > 1) {
			List<TiersDto> allTiers = activiteRepresentationInteret.getActionsRepresentationInteret().stream().map(tiers -> tiers.getTiers()).flatMap(tier -> tier.stream())
					.collect(Collectors.toList());

			Set<TiersDto> duplicated = allTiers.stream().filter(tierA -> allTiers.stream().filter(tierB -> tierB.getOrganisationId().equals(tierA.getOrganisationId())).count() > 1)
					.collect(Collectors.toSet());

			if (!duplicated.isEmpty()) {
				throw new BusinessGlobalException(ErrorMessageEnum.DUPLICATE_TIERS.getMessage());
			}
		}
	}

	private void checkMissingOtherAction(final ActiviteRepresentationInteretDto activiteRepresentationInteret) {
		activiteRepresentationInteret.getActionsRepresentationInteret().stream().filter(ar -> ar.getActionsMenees().contains(ACTION_MENEE_AUTRE)).forEach(ar -> {
			if (ar.getActionMeneeAutre() == null || ar.getActionMeneeAutre().isEmpty()) {
				throw new BusinessGlobalException(ErrorMessageEnum.ACTION_AUTRE_NON_RENSEIGNEE.getMessage());
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteDeclarationActivite(final Long ficheId) {
		final ActiviteRepresentationInteretEntity entityDB = activiteRepresentationInteretRepository.findOne(ficheId);
		if (entityDB.getStatut().equals(StatutPublicationEnum.NON_PUBLIEE)) {
			activiteRepresentationInteretRepository.delete(entityDB);
		} else {
			throw new BusinessGlobalException(ErrorMessageEnum.SUPPRESSION_ACTIVITE_IMPOSSIBLE.getMessage());
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FicheBackDto getFicheBO(Long ficheId) {
		final ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = Optional.ofNullable(activiteRepresentationInteretRepository.findOne(ficheId))
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.DECLARATION_INTROUVABLE.getMessage()));

		return ficheBOTransformer.modelToDto(activiteRepresentationInteretEntity);
	}

	/**
	 * Suppression d'une activité via le BO quelque soit son statut 
	 * Si activité non_publiee => suppression activité
	 * Si activité <> non_publiee => changement du statutPublication à SUPPRIMEE 
	 * avec dépublication et affichage qu'en BO
	 */
	@Transactional
	@Override
	public void deleteActiviteBO(Long activiteId) {

		final ActiviteRepresentationInteretEntity entityDB = activiteRepresentationInteretRepository.findOne(activiteId);
		
		Long espaceOrganisationId = entityDB.getExerciceComptable().getEspaceOrganisation().getId();
		
		if (entityDB.getStatut().equals(StatutPublicationEnum.NON_PUBLIEE)) {
			activiteRepresentationInteretRepository.delete(entityDB);
		} else {
			//Statut SUPPRIMEE pour conservation historique		
			entityDB.setStatut(StatutPublicationEnum.SUPPRIMEE);
			activiteRepresentationInteretRepository.saveAndFlush(entityDB);
			
			//force la publication du l'espace
			espaceOrganisationService.forcePublicationEspace(espaceOrganisationId);
		}
		
		LOGGER.info("Suppression de l'activité : {}", activiteId);	
		
	}

	@Override
	public void deleteListActiviteBO(Long[] activiteIdList) {
		LOGGER.info("Début suppression lot activité : {}", activiteIdList);	
		for(int i=0; i<activiteIdList.length; i++) {
			this.deleteActiviteBO(activiteIdList[i]);
		}
		
	}
	/**
	 * Extraction du pubjson des données d'un fiche pour affichage en BO
	 */
	@Override
	public FicheBackDto getFicheHistoriseBO(Long publicationFicheId) {
		final PublicationActiviteEntity publicationActivite = Optional.ofNullable(publicationActiviteRepository.findOne(publicationFicheId))
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.DECLARATION_INTROUVABLE.getMessage()));
		
		ObjectMapper mapper = new ObjectMapper();
		ActiviteRepresentationInteretJsonDto ex;
		try {
			ex = mapper.readerFor(ActiviteRepresentationInteretJsonDto.class).readValue(publicationActivite.getPublicationJson());
			FicheBackDto ficheBackDto = new FicheBackDto();
			
			ficheBackDto.setIdFiche(ex.getIdentifiantFiche());
			ficheBackDto.setObjet(ex.getObjet());
			ficheBackDto.setDomaines(getDomainesInterventionComplet(ex.getDomainesIntervention()));
			ficheBackDto.setActions(getListeActionClomplet(ex.getActionsRepresentationInteret()));
			
			return ficheBackDto;
			
		} catch (IOException e) {
			throw new TechnicalException(ErrorMessageEnum.ERREUR_EXTRACTION_JSON.getMessage(), e);
		}
	}
	
	/**
	 * Retourne la list des catégories de domaines et leurs domaines
	 * présents dans le pubJson
	 * @param domainesIntervention
	 * @return
	 */
	private Map<String, List<String>> getDomainesInterventionComplet(Set<String> domainesIntervention){
		
		Map<String, List<String>> domainesMap = new HashMap<>();		
		List<DomaineInterventionEntity> domaineList = new ArrayList<>();
		
		//Récupération de la liste des entity
		for(String libelle : domainesIntervention) {
			DomaineInterventionEntity domaineInterventionEntity = this.domaineInterventionRepository.findByLibelle(libelle);
			domaineList.add(domaineInterventionEntity);			
		}
		//Trie de la liste des entity par catégorie
		domaineList.sort(Comparator.comparing(DomaineInterventionEntity::getIdCategorie));
		
		String categorieDomaine = "";
		List<String> list = new ArrayList<>();
		
		// parcours de la liste triées pour définir les couples catégorie-liste domaines
		for (DomaineInterventionEntity domaine : domaineList) {
			if("".equalsIgnoreCase(categorieDomaine)) {
				categorieDomaine = domaine.getCategorie();
			}
			//si la catégorie est différente de la précédent, pousse dans la map le couple catégorie-liste domaines
			if(!categorieDomaine.equalsIgnoreCase(domaine.getCategorie())) {
				domainesMap.put(categorieDomaine, list);
				categorieDomaine = domaine.getCategorie();
				list = new ArrayList<>();				
			}
			list.add(domaine.getLibelle());			
		}
		domainesMap.put(categorieDomaine, list);
		return domainesMap;
	}
	
	/**
	 * Retourne la liste des action avec la liste des RP et leur catégorie
	 * @param actionsRepresentationInteretList
	 * @return
	 */
	private List<ActionBackDto> getListeActionClomplet(List<ActionRepresentationInteretJsonDto> actionsRepresentationInteretList){
		
		List<ActionBackDto> actionBackDtoList = new ArrayList<>();
		
		for(ActionRepresentationInteretJsonDto action : actionsRepresentationInteretList) {
			ActionBackDto actionBackDto = new ActionBackDto();
			List<ResponsablePublicEntity> rpList = new ArrayList<>();
			Map<String, List<String>> responsablePublicMap = new HashMap<>();
			
			//Récupération de la liste des entity
			for(String libelleJsonResponsablePublic : action.getReponsablesPublics()) {					
				
				ResponsablePublicEntity responsablePublicEntity = this.responsablePublicRepository.findByLibelle(getLibelleRP(libelleJsonResponsablePublic));
				rpList.add(responsablePublicEntity);				
			}
			
			//Trie de la liste des entity par catégorie
			rpList.sort(Comparator.comparing(ResponsablePublicEntity::getIdCategorie));
			
			String categorieRp = "";
			List<String> list = new ArrayList<>();
			
			// parcours de la liste triées pour définir les couples catégorie-liste domaines
			for (ResponsablePublicEntity resp : rpList) {
				if("".equalsIgnoreCase(categorieRp)) {
					categorieRp = resp.getCategorie();
				}
				//si la catégorie est différente de la précédent, pousse dans la map le couple catégorie-liste domaines
				if(!categorieRp.equalsIgnoreCase(resp.getCategorie())) {
					responsablePublicMap.put(categorieRp, list);
					categorieRp = resp.getCategorie();
					list= new ArrayList<>();
					
				}
				list.add(resp.getLibelle());
				
			}
			responsablePublicMap.put(categorieRp, list);
			actionBackDto.setReponsablesPublics(responsablePublicMap);			
			
			actionBackDto.setObservation(action.getObservation());
			actionBackDto.setActionMeneeAutre(action.getActionMeneeAutre());
			actionBackDto.setResponsablePublicAutre(action.getResponsablePublicAutre());
			actionBackDto.setActionsMenees(new ArrayList<>(action.getActionsMenees()));
			actionBackDto.setDecisionsConcernees(new ArrayList<>(action.getDecisionsConcernees()));
			actionBackDto.setTiers(action.getTiers());
			
			actionBackDtoList.add(actionBackDto);
		}		
		
		return actionBackDtoList;
	}
	/**
	 * Retourne le libellé du RP à partir du libellé du jsonformé de "categorie - libellé"
	 * @param libelleJson
	 * @return
	 */
	private String getLibelleRP(String libelleJson) {
		
		String libelle = "";
		if(libelleJson.contains(" - ")) {					
			String[] libelleDecompose = libelleJson.split(" - ");
			libelle = libelleDecompose[1];					
		}else {
			libelle = libelleJson;
		}
		
		return libelle;
		
	}

}
