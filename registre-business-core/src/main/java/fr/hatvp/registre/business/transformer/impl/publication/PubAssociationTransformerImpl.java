/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication;

import fr.hatvp.registre.business.transformer.impl.common.CommonAssociationTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.publication.PubAssociationTransformer;
import fr.hatvp.registre.persistence.entity.publication.PubAssociationEntity;
import org.springframework.stereotype.Component;

/**
 * Classe de transformation de la classe {@link fr.hatvp.registre.persistence.entity.publication.PubAssociationEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class PubAssociationTransformerImpl
		extends CommonAssociationTransformerImpl<PubAssociationEntity>
		implements PubAssociationTransformer {
	
	/** Init super class. */
	public PubAssociationTransformerImpl() {
		super.entityClass = PubAssociationEntity.class;
	}
}