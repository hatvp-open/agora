/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentEspaceOrganisationEntity;

/**
 * Interface de transformation de la classe CommentEspaceOrganisationEntity.
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
public interface CommentEspaceOrganisationBoTransformer
    extends AbstractCommentBoTransformer<CommentBoDto, CommentEspaceOrganisationEntity>
{
}
