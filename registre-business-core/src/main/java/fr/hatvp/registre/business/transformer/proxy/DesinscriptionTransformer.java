/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.DesinscriptionDto;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;

public interface DesinscriptionTransformer extends CommonTransformer<DesinscriptionDto, DesinscriptionEntity> {
}
