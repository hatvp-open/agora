/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.DesinscriptionPieceTransformer;
import fr.hatvp.registre.commons.dto.DesinscriptionPieceDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.piece.DesinscriptionPieceEntity;
import org.springframework.stereotype.Component;

@Component
public class DesinscriptionPieceTransformerImpl extends AbstractCommonTansformer<DesinscriptionPieceDto, DesinscriptionPieceEntity>
    implements DesinscriptionPieceTransformer {

    @Override
    public DesinscriptionPieceEntity dtoToModel(DesinscriptionPieceDto dto) {
        DesinscriptionPieceEntity desinscriptionPieceEntity = new DesinscriptionPieceEntity();
        RegistreBeanUtils.copyProperties(desinscriptionPieceEntity, dto);
        return desinscriptionPieceEntity;
    }

    @Override
    public DesinscriptionPieceDto modelToDto(DesinscriptionPieceEntity model) {
        DesinscriptionPieceDto desinscriptionPieceDto = new DesinscriptionPieceDto();
        RegistreBeanUtils.copyProperties(desinscriptionPieceDto, model);
        return desinscriptionPieceDto;
    }
}
