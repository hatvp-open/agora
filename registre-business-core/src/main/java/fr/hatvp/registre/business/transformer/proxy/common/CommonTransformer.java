/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.common;

import java.util.List;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;

/**
 * Interface des transformer de l'application.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface CommonTransformer<T extends AbstractCommonDto, E extends AbstractCommonEntity> {
	/**
	 * Transformation d'un objet DTO en sont objet model.
	 *
	 * @param dto
	 *            Le DTO.
	 * @return Le model correspondant.
	 */
	E dtoToModel(final T dto);

	/**
	 * Transformation d'un objet model en sont objet DTO.
	 *
	 * @param model
	 *            Le model
	 * @return Le DTO correspondant.
	 */
	T modelToDto(final E model);

	/**
	 * Transformation d'une {@link List} d'objets DTO en une {@link List} de leurs objets model.
	 *
	 * @param dtos
	 *            La {@link List} d'objets DTO.
	 * @return La {@link List} d'objet models correspondant.
	 */
	List<E> dtoToModel(final List<T> dtos);

	/**
	 * Transformation d'une {@link List} d'objets model en une {@link List} de leurs objets DTO.
	 *
	 * @param models
	 *            La {@link List} d'objets model
	 * @return La {@link List} d'objet DTO correspondant.
	 */
	List<T> modelToDto(final List<E> models);

	/**
	 * Détermine l'identifiant national (SIREN, RNA, HATVP) d'un espace organisation selon son type identifiant national.
	 * 
	 * @param l'entité
	 *            organisation à transformer.
	 * @return l'identifiant national de l'espace.
	 */
	String determineIdentifiantNational(final OrganisationEntity model);
}
