/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationTransformer;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationPieceTransformer;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.piece.EspaceOrganisationPieceEntity;

/**
 * Implémentation du transformer pour les pièces de l'espace collaboratif.
 * @version $Revision$ $Date${0xD}
 */
@Component
public class EspaceOrganisationPieceTransformerImpl 
    extends AbstractCommonTansformer<EspaceOrganisationPieceDto, EspaceOrganisationPieceEntity>
    implements EspaceOrganisationPieceTransformer 
{

    /**
     * Le transformer pour l'espac Orga
     */
    @Autowired
    EspaceOrganisationTransformer espaceOrganisationTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    public EspaceOrganisationPieceEntity dtoToModel(final EspaceOrganisationPieceDto dto)
    {
        final EspaceOrganisationPieceEntity model = new EspaceOrganisationPieceEntity();
        RegistreBeanUtils.copyProperties(model, dto);

        model.setEspaceOrganisation(new EspaceOrganisationEntity(dto.getEspaceOrganisationId()));
        
        return model;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public EspaceOrganisationPieceDto modelToDto(final EspaceOrganisationPieceEntity model)
    {
        final EspaceOrganisationPieceDto dto = new EspaceOrganisationPieceDto();
        RegistreBeanUtils.copyProperties(dto, model);

        dto.setEspaceOrganisationId(model.getEspaceOrganisation().getId());
        
        return dto;
    }
}
