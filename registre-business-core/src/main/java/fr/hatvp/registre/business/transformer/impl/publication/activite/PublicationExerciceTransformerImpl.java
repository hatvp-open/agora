/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication.activite;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationExerciceTransformer;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationExerciceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity;

/**
 * Classe du transformeur entre {@link PublicationExerciceEntity} et {@link PublicationExerciceDto}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@Transactional(readOnly = true)
public class PublicationExerciceTransformerImpl implements PublicationExerciceTransformer {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationExerciceDto exerciceToPublication(final PublicationExerciceEntity pubExercice) {
//
//		PublicationExerciceDto publicationDto = new PublicationExerciceDto();		
//		final ObjectMapper mapper = new ObjectMapper();
		
		final PublicationExerciceDto publicationDto = new PublicationExerciceDto();
		final ExerciceComptableEntity exercice = pubExercice.getExerciceComptable();
		publicationDto.setDateDebut(exercice.getDateDebut());
		publicationDto.setDateFin(exercice.getDateFin());
		
		if (!exercice.isNonDeclarationMoyens()) {
			if (Boolean.FALSE.equals(exercice.getHasNotChiffreAffaire()) && exercice.getChiffreAffaire() != null) {
			publicationDto.setChiffreAffaire(exercice.getChiffreAffaire().getLibelle());
			}
			publicationDto.setHasNotChiffreAffaire(exercice.getHasNotChiffreAffaire());
			publicationDto.setMontantDepense(exercice.getMontantDepense().getLibelle());
			publicationDto.setNombreSalaries(exercice.getNombreSalaries());
			publicationDto.setCommentaire(exercice.getCommentaire());
		}
//		try {
//			publicationDto = mapper.readValue(pubExercice.getPublicationJson(), PublicationExerciceDto.class);
//		} catch (IOException e) {
//			throw new BusinessGlobalException("Impossible de récupérer le json de la publication activite");
//		}
		publicationDto.setStatut(StatutPublicationEnum.PUBLIEE);
		publicationDto.setExerciceId(exercice.getId());
		
		publicationDto.setPublicationDate(pubExercice.getCreationDate());
		publicationDto.setId(null);
		publicationDto.setVersion(null);
		
		publicationDto.setNoActivite(exercice.isNoActivite());
		publicationDto.setNombreActivite((int) exercice.getActivitesRepresentationInteret().stream()
		.filter(act -> !StatutPublicationEnum.NON_PUBLIEE.equals(act.getStatut()) && !StatutPublicationEnum.DEPUBLIEE.equals(act.getStatut()) && !StatutPublicationEnum.SUPPRIMEE.equals(act.getStatut())).count());
		publicationDto.setBlacklisted(exercice.isBlacklisted());

		return publicationDto;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationExerciceDto exerciceToPublication(final ExerciceComptableEntity exercice) {

		final PublicationExerciceDto publicationDto = new PublicationExerciceDto();

		publicationDto.setDateDebut(exercice.getDateDebut());
		publicationDto.setDateFin(exercice.getDateFin());

		if (!exercice.isNonDeclarationMoyens()) {
			if (Boolean.FALSE.equals(exercice.getHasNotChiffreAffaire())) {
				publicationDto.setChiffreAffaire(exercice.getChiffreAffaire().getLibelle());
			}
			publicationDto.setHasNotChiffreAffaire(exercice.getHasNotChiffreAffaire());
			publicationDto.setMontantDepense(exercice.getMontantDepense().getLibelle());
			publicationDto.setNombreSalaries(exercice.getNombreSalaries());
			publicationDto.setCommentaire(exercice.getCommentaire());
		}
		publicationDto.setStatut(StatutPublicationEnum.PUBLIEE);
		publicationDto.setExerciceId(exercice.getId());
		publicationDto.setId(null);
		publicationDto.setVersion(null);

		publicationDto.setNoActivite(exercice.isNoActivite());
		publicationDto.setNombreActivite((int) exercice.getActivitesRepresentationInteret().stream()
				.filter(act -> !StatutPublicationEnum.NON_PUBLIEE.equals(act.getStatut()) && !StatutPublicationEnum.DEPUBLIEE.equals(act.getStatut()) && !StatutPublicationEnum.SUPPRIMEE.equals(act.getStatut())).count());
		publicationDto.setBlacklisted(exercice.isBlacklisted());

		return publicationDto;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PublicationExerciceDto> exercicesToPublication(final List<PublicationExerciceEntity> models) {
		return models.stream().map(this::exerciceToPublication).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationExerciceEntity dtoToModel(PublicationExerciceDto dto) {
		final PublicationExerciceEntity entity = new PublicationExerciceEntity();
		BeanUtils.copyProperties(dto, entity);

		/** convertir la grappe d'objet de publication en string json. */
		final ObjectMapper mapper = new ObjectMapper();
		final String jsonInString;
		try {
			entity.setCreationDate(entity.getCreationDate());
			jsonInString = mapper.writeValueAsString(dto);
			entity.setPublicationJson(jsonInString);
		} catch (final JsonProcessingException e) {
			throw new BusinessGlobalException("Impossible de créer le json de la publication exercice");
		}

		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublicationExerciceDto modelToDto(final PublicationExerciceEntity model) {
		final PublicationExerciceDto dto = new PublicationExerciceDto();
		// TODO
		/*
		 * RegistreBeanUtils.copyProperties(dto, model); dto.setId(null); dto.setVersion(null);
		 */
		return dto;
	}
}
