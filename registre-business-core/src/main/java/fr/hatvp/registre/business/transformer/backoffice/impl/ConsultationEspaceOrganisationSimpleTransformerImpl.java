/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.impl;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.ConsultationEspaceOrganisationSimpleTransformer;
import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.commons.dto.backoffice.ConsultationEspaceBoSimpleDto;
import fr.hatvp.registre.commons.dto.backoffice.ContactOperationnelSimpleDto;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;

@Component
public class ConsultationEspaceOrganisationSimpleTransformerImpl extends AbstractCommonTansformer<ConsultationEspaceBoSimpleDto, EspaceOrganisationEntity>
		implements ConsultationEspaceOrganisationSimpleTransformer {

	@Override
	public ConsultationEspaceBoSimpleDto modelToDto(final EspaceOrganisationEntity model) {
		final ConsultationEspaceBoSimpleDto dto = new ConsultationEspaceBoSimpleDto();

		dto.setId(model.getId());
		dto.setDenomination(model.getOrganisation().getDenomination());
		dto.setNomUsage(model.getNomUsage());
		dto.setNomUsageHatvp(model.getNomUsageHatvp());
		dto.setSigleHatvp(model.getSigleHatvp());
		dto.setAncienNomHatvp(model.getAncienNomHatvp());
		dto.setNationalId(this.determineIdentifiantNational(model.getOrganisation()));
		dto.setRelancesBloquees(model.isRelancesBloquees());
		dto.setOrganisationId(model.getOrganisation().getId());

		Set<InscriptionEspaceEntity> inscriptionEspaceSet = model.getListeDesCollaborateurs().stream().map(col -> col.getDeclarant()).filter(dec -> Optional.ofNullable(dec).isPresent())
				.map(dec -> dec.getInscriptionEspaces()).flatMap(ins -> ins.stream()).collect(Collectors.toSet());

		List<ContactOperationnelSimpleDto> administrateurs = inscriptionEspaceSet.stream().filter(ins -> ins.getEspaceOrganisation().getId().equals(model.getId()))
				.filter(ins -> ins.getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR)).filter(ins -> !Optional.ofNullable(ins.getDateSuppression()).isPresent())
				.filter(ins -> Optional.ofNullable(ins.getDateValidation()).isPresent()).map(ins -> {
					return new ContactOperationnelSimpleDto(ins.getDeclarant().getId(), new StringBuilder(ins.getDeclarant().getCivility().toString()).append(" ")
							.append(ins.getDeclarant().getNom()).append(" ").append(ins.getDeclarant().getPrenom()).toString(), ins.getDeclarant().isActivated());
				}).collect(Collectors.toList());

		dto.setAdministrateurs(administrateurs);

		int nombreDeclarant = (int) inscriptionEspaceSet.stream().filter(i -> i.getEspaceOrganisation().getId().equals(model.getId())).filter(i -> i.getDateSuppression() == null)
				.filter(i -> i.getDateValidation() != null).count();

		dto.setNombreDeclarant(nombreDeclarant);

		dto.setCreationDate(model.getCreationDate());

		dto.setIsPublication(model.isPublicationEnabled());
		dto.setFinExerciceFiscal(model.getFinExerciceFiscal());
		dto.setActif(model.isEspaceActif());
		return dto;
	}

}
