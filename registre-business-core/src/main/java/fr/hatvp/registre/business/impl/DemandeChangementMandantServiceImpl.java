/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import fr.hatvp.registre.business.DemandeChangementMandantService;
import fr.hatvp.registre.business.email.GestionMandatBoMailer;
import fr.hatvp.registre.business.email.SurveillanceMailer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.DemandeChangementMandantTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationPieceTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.DemandeChangementMandantDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ChangementContactEnum;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.commons.lists.StatutDemandeEnum;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.commons.lists.backoffice.MotifComplementEspaceEnum;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.DemandeChangementMandantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.piece.EspaceOrganisationPieceEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.DemandeChangementMandantRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationPiecesRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;

/**
 * Service de gestion des Demandes d'ajout d'une Organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional
@Service
public class DemandeChangementMandantServiceImpl extends AbstractCommonService<DemandeChangementMandantDto, DemandeChangementMandantEntity> implements DemandeChangementMandantService {

	/** Repository du service. */
	@Autowired
	private DemandeChangementMandantRepository demandeChangementMandantRepository;

	/** Repository pour les pièces de l'espace organisation. */
	@Autowired
	private EspaceOrganisationPiecesRepository espaceOrganisationPiecesRepository;

	/** Service envoi de mails. */
	@Autowired
	private GestionMandatBoMailer gestionMandatBoMailer;

	/** Repository des inscriptions à l'espace organisation. */
	@Autowired
	private InscriptionEspaceRepository inscriptionEspaceRepository;

	/** Repository des declarants. */
	@Autowired
	private DeclarantRepository declarantRepository;

	/** Repository de l'espace organisation. */
	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;

	/** Transformer du service */
	@Autowired
	private DemandeChangementMandantTransformer demandeChangementMandantTransformer;

	/** Le transformer pour la gestion des pièces de l'espace organisation. */
	@Autowired
	private EspaceOrganisationPieceTransformer espaceOrganisationPieceTransformer;

	/**
	 * Le transformer pour les pieces
	 */
	@Autowired
	private DeclarantTransformer declarantTransformer;

	/** Composant de mailing de surveillance */
	@Autowired
	private SurveillanceMailer surveillanceMailer;

	/** Templates messages motifs demande complément. */

	@Value("${mail.bo.changementMandant.ajoutCO.complement.mandat.message}")
	private String ajoutCOMessageMandat;

	@Value("${mail.bo.changementMandant.changementCO.complement.mandat.message}")
	private String changementCOMessageMandat;

	/** Template 2 - Message de refus de création de l'espace. */
	@Value("${mail.bo.changementMandant.changementRepresentant.complement.cni.message}")
	private String changementRepresentantMessageCni;
	/** Template 3 - Message de refus de création de l'espace. */
	@Value("${mail.bo.changementMandant.changementRepresentant.complement.mandat.message}")
	private String changementRepresentantMessageMandat;
	/** Template 4 - Message de refus de création de l'espace. */
	@Value("${mail.bo.changementMandant.changementRepresentant.complement.identite.message}")
	private String changementRepresentantMessageIdentite;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<DemandeChangementMandantDto> getPaginatedDemandesChangementMandant(final Integer pageNumber, final Integer resultPerPage) {

		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "dateDemande");

		return getPaginatedDemandesChangementMandantAll(pageRequest);

	}

	/**
	 * Récupérer toutes les demandes de changement de mandant.
	 *
	 * @return liste des demandes de changement de mandant
	 */
	private PaginatedDto<DemandeChangementMandantDto> getPaginatedDemandesChangementMandantAll(PageRequest pageRequest) {

		this.demandeChangementMandantRepository.findAll(pageRequest);

		Page<DemandeChangementMandantEntity> demandeChangementMandantPage = this.demandeChangementMandantRepository.findAll(pageRequest);

		List<DemandeChangementMandantDto> demandeChangementMandantDtos = demandeChangementMandantPage.getContent().stream().map(d -> this.demandeChangementMandantTransformer.modelToDto(d))
				.collect(Collectors.toList());

		return new PaginatedDto<>(demandeChangementMandantDtos, demandeChangementMandantPage.getTotalElements());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DemandeChangementMandantDto> getAllDemandesChangementByEspaceOrganisation(Long espaceOrganisationId) {
		return this.demandeChangementMandantRepository.findAllByEspaceOrganisationIdOrderByDateDemandeDesc(espaceOrganisationId).stream()
				.map(d -> this.demandeChangementMandantTransformer.modelToDto(d)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaginatedDto<DemandeChangementMandantDto> getDemandesParRecherche(String type, String keywords, Integer pageNumber, Integer resultPerPage) {
		PageRequest pageRequest = new PageRequest(pageNumber - 1, resultPerPage, Sort.Direction.ASC, "id");
		Page<DemandeChangementMandantEntity> pageOrga = null;
		List<DemandeChangementMandantDto> demandes = new ArrayList<>();
		Integer total = null;
		switch (type) {
		case "denomination":
			demandes = this.demandeChangementMandantRepository.findByDenomination("%" + keywords + "%", pageRequest.getPageSize(), pageRequest.getOffset()).stream()
					.map(demandeChangementMandantTransformer::modelToDto).collect(Collectors.toList());
			total = this.demandeChangementMandantRepository.countDemandeFilteredByDenomination("%" + keywords + "%");
			break;
		case "declarantFullName":
			demandes = this.demandeChangementMandantRepository.findByNomOrPrenom("%" + keywords + "%", pageRequest.getPageSize(), pageRequest.getOffset()).stream()
					.map(demandeChangementMandantTransformer::modelToDto).collect(Collectors.toList());
			total = this.demandeChangementMandantRepository.countDemandeFilteredByNomOrPrenom("%" + keywords + "%");
			break;
		case "typeDemande":
			switch (keywords) {
			case "AJOUT_OPERATIONNEL":
				pageOrga = this.demandeChangementMandantRepository.findByTypeDemande(ChangementContactEnum.AJOUT_OPERATIONNEL, pageRequest);
				break;
			case "REMPLACEMENT_OPERATIONNEL":
				pageOrga = this.demandeChangementMandantRepository.findByTypeDemande(ChangementContactEnum.REMPLACEMENT_OPERATIONNEL, pageRequest);
				break;
			case "CHANGEMENT_REPRESENTANT":
				pageOrga = this.demandeChangementMandantRepository.findByTypeDemande(ChangementContactEnum.CHANGEMENT_REPRESENTANT, pageRequest);
				break;
			default:
				pageOrga = this.demandeChangementMandantRepository.findAll(pageRequest);
			}
			demandes = pageOrga.getContent().stream().map(demande -> demandeChangementMandantTransformer.modelToDto(demande)).collect(Collectors.toList());
			break;
		case "dateDemande":
			pageOrga = this.demandeChangementMandantRepository.findByDateDemandeBetweenOrderByDateDemandeAsc(
					DateTime.parse(keywords, DateTimeFormat.forPattern("ddMMyyyy")).withTimeAtStartOfDay().toDate(),
					DateTime.parse(keywords, DateTimeFormat.forPattern("ddMMyyyy")).withTime(23, 59, 59, 0).toDate(), pageRequest);
			demandes = pageOrga.getContent().stream().map(demande -> demandeChangementMandantTransformer.modelToDto(demande)).collect(Collectors.toList());
			break;
		case "statut":
			switch (keywords) {
			case "NOUVELLE":
				pageOrga = this.demandeChangementMandantRepository.findByStatutOrderByIdAsc(StatutDemandeEnum.NOUVELLE, pageRequest);
				break;
			case "ACCEPTEE":
				pageOrga = this.demandeChangementMandantRepository.findByStatutOrderByIdAsc(StatutDemandeEnum.ACCEPTEE, pageRequest);
				break;
			case "REFUSEE":
				pageOrga = this.demandeChangementMandantRepository.findByStatutOrderByIdAsc(StatutDemandeEnum.REFUSEE, pageRequest);
				break;
			case "COMPLEMENT_DEMANDE":
				pageOrga = this.demandeChangementMandantRepository.findByStatutOrderByIdAsc(StatutDemandeEnum.COMPLEMENT_DEMANDE_ENVOYEE, pageRequest);
				break;
			case "COMPLEMENT_RECU":
				pageOrga = this.demandeChangementMandantRepository.findByStatutOrderByIdAsc(StatutDemandeEnum.COMPLEMENT_RECU, pageRequest);
				break;
			default:
				pageOrga = this.demandeChangementMandantRepository.findAll(pageRequest);
			}
			demandes = pageOrga.getContent().stream().map(demande -> demandeChangementMandantTransformer.modelToDto(demande)).collect(Collectors.toList());
			break;
		default:
			pageOrga = this.demandeChangementMandantRepository.findAll(pageRequest);
			demandes = pageOrga.getContent().stream().map(demande -> demandeChangementMandantTransformer.modelToDto(demande)).collect(Collectors.toList());
		}

		return new PaginatedDto<>(demandes, pageOrga != null ? pageOrga.getTotalElements() : total);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void rejeterDemande(Long id) {
		DemandeChangementMandantEntity demandeChangementMandantEntity = Optional.ofNullable(demandeChangementMandantRepository.findOne(id))
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.MSG_404.getMessage()));
		demandeChangementMandantEntity.setDateRejet(new Date());
		demandeChangementMandantEntity.setStatut(StatutDemandeEnum.REFUSEE);
		demandeChangementMandantRepository.save(demandeChangementMandantEntity);

		// envoi mail refus
		this.gestionMandatBoMailer.sendRefusEspaceEmail(this.declarantTransformer.modelToDto(demandeChangementMandantEntity.getDeclarant()), demandeChangementMandantEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validerDemande(Long id) {
		DemandeChangementMandantEntity demandeChangementMandantEntity = Optional.ofNullable(demandeChangementMandantRepository.findOne(id))
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.MSG_404.getMessage()));

		if (demandeChangementMandantEntity.getTypeDemande().equals(ChangementContactEnum.AJOUT_OPERATIONNEL)) {
			demandeChangementMandantEntity.getDeclarantPromu().getRolesFront().add(RoleEnumFrontOffice.ADMINISTRATEUR);
		} else if (demandeChangementMandantEntity.getTypeDemande().equals(ChangementContactEnum.REMPLACEMENT_OPERATIONNEL)) {
			demandeChangementMandantEntity.getDeclarantPromu().getRolesFront().add(RoleEnumFrontOffice.ADMINISTRATEUR);
			demandeChangementMandantEntity.getDeclarantDechu().getRolesFront().remove(RoleEnumFrontOffice.ADMINISTRATEUR);
		}
		demandeChangementMandantEntity.setDateValidation(new Date());
		demandeChangementMandantEntity.setStatut(StatutDemandeEnum.ACCEPTEE);
		demandeChangementMandantRepository.save(demandeChangementMandantEntity);
		// envoi mail validation
		this.gestionMandatBoMailer.sendValidationEspaceEmail(this.declarantTransformer.modelToDto(demandeChangementMandantEntity.getDeclarant()), demandeChangementMandantEntity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DemandeChangementMandantDto getDemandeChangementMandant(final Long id) {
		DemandeChangementMandantDto demandeChangementMandantDto = Optional.ofNullable(this.demandeChangementMandantRepository.findOne(id))
				.map(e -> this.demandeChangementMandantTransformer.modelToDto(e)).orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.MSG_404.getMessage()));
		Optional.ofNullable(demandeChangementMandantDto.getDeclarantPromuInscriptionEspaceId()).ifPresent(
				declarantId -> demandeChangementMandantDto.setDeclarantPromotion(declarantTransformer.modelToDto(inscriptionEspaceRepository.findOne(declarantId).getDeclarant())));
		return demandeChangementMandantDto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void saveDemandeChangementMandant(final DemandeChangementMandantDto demande, EspaceOrganisationPieceDto identite, EspaceOrganisationPieceDto mandat) {
		final DemandeChangementMandantEntity model = this.demandeChangementMandantTransformer.dtoToModel(demande);

		// On récupére l'espace de cette organisation
		EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository.findOne(demande.getEspaceOrganisationId());

		// On récupère le déclarant et l'organisation à inscrire
		final DeclarantEntity declarant = this.declarantRepository.findOne(demande.getDeclarantDemande().getId());

		// Enregistrement des pièces
		EspaceOrganisationPieceEntity identiteEntity = null;
		if (identite != null) {
			// Transformation du DTO en entity
			identiteEntity = this.espaceOrganisationPieceTransformer.dtoToModel(identite);

			identiteEntity.setDateVersementPiece(new Date());
			identiteEntity.setDeclarant(declarant);
			identiteEntity.setEspaceOrganisation(espaceOrganisationEntity);
			identiteEntity.setOrigineVersement(OrigineVersementEnum.DEMANDE_CHANGEMENT_MANDANT);
			this.espaceOrganisationPiecesRepository.save(identiteEntity);
		}
		// Transformation du DTO en entity
		EspaceOrganisationPieceEntity mandatEntity = this.espaceOrganisationPieceTransformer.dtoToModel(mandat);

		mandatEntity.setDateVersementPiece(new Date());
		mandatEntity.setDeclarant(declarant);
		mandatEntity.setEspaceOrganisation(espaceOrganisationEntity);
		mandatEntity.setOrigineVersement(OrigineVersementEnum.DEMANDE_CHANGEMENT_MANDANT);
		this.espaceOrganisationPiecesRepository.save(mandatEntity);

		model.setMandatRepresentantLegal(mandatEntity);
		model.setPieceIdentiteRepresentantLegal(identiteEntity);
		model.setEspaceOrganisation(espaceOrganisationEntity);
		model.setDeclarant(declarant);
		model.setDeclarantPromu(demande.getDeclarantPromuInscriptionEspaceId() == null ? null : this.inscriptionEspaceRepository.findOne(demande.getDeclarantPromuInscriptionEspaceId()));
		model.setDeclarantDechu(demande.getDeclarantDechuInscriptionEspaceId() == null ? null : this.inscriptionEspaceRepository.findOne(demande.getDeclarantDechuInscriptionEspaceId()));
		model.setStatut(StatutDemandeEnum.NOUVELLE);
		this.demandeChangementMandantRepository.save(model);
        SurveillanceEntity surveillance = espaceOrganisationEntity.getOrganisation().getSurveillanceEntity();
		if (surveillance !=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.DEMANDE_MANDAT)) {
			this.surveillanceMailer.sendSurveillanceMail(espaceOrganisationEntity.getOrganisation(), espaceOrganisationEntity, SurveillanceOrganisationEnum.DEMANDE_MANDAT,"");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void demandeComplementInformation(final Long id, final String message) {
		final DemandeChangementMandantEntity demande = this.demandeChangementMandantRepository.findOne(id);

		// statut complément demandé
		demande.setStatut(StatutDemandeEnum.COMPLEMENT_DEMANDE_ENVOYEE);

		// envoi email demande complément
		this.gestionMandatBoMailer.sendDemandeComplementEspaceEmail(this.declarantTransformer.modelToDto(demande.getDeclarant()), message,
				demande.getEspaceOrganisation().getOrganisation().getDenomination());

		// sauvegarde du changement de statut
		this.demandeChangementMandantRepository.save(demande);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void verserPiecesComplementaires(Long idEspaceOrganisation, Long idDeclarant, List<EspaceOrganisationPieceDto> pieces, Long demandeId) {
		DemandeChangementMandantEntity demandeEntity = demandeChangementMandantRepository.findOne(demandeId);
		DeclarantEntity declarantEntity = this.declarantRepository.findOne(idDeclarant);
		EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository.findOne(idEspaceOrganisation);

		List<EspaceOrganisationPieceEntity> piecesEntities = pieces.stream().map(p -> {
			EspaceOrganisationPieceEntity e = espaceOrganisationPieceTransformer.dtoToModel(p);
			e.setDateVersementPiece(new Date());
			e.setDeclarant(declarantEntity);
			e.setEspaceOrganisation(espaceOrganisationEntity);
			e.setOrigineVersement(OrigineVersementEnum.DEMANDE_COMPLEMENT_CHANGEMENT_MANDANT);
			return e;
		}).collect(Collectors.toList());

		demandeEntity.getComplementPieceRepresentantLegal().addAll(piecesEntities);
		demandeEntity.setStatut(StatutDemandeEnum.COMPLEMENT_RECU);

		demandeChangementMandantRepository.save(demandeEntity);
        SurveillanceEntity surveillance = espaceOrganisationEntity.getOrganisation().getSurveillanceEntity();
		if (surveillance!=null && surveillance.getTypeSurveillance().contains(SurveillanceOrganisationEnum.COMPLEMENT_MANDAT)) {
			this.surveillanceMailer.sendSurveillanceMail(espaceOrganisationEntity.getOrganisation(), espaceOrganisationEntity, SurveillanceOrganisationEnum.COMPLEMENT_MANDAT,"");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JpaRepository<DemandeChangementMandantEntity, Long> getRepository() {
		return this.demandeChangementMandantRepository;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommonTransformer<DemandeChangementMandantDto, DemandeChangementMandantEntity> getTransformer() {
		return this.demandeChangementMandantTransformer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String generateDemandeComplementMessage(final Long id, final MotifComplementEspaceEnum motif) {
		DemandeChangementMandantEntity demandeChangementMandantEntity = Optional.ofNullable(demandeChangementMandantRepository.findOne(id))
				.orElseThrow(() -> new BusinessGlobalException(ErrorMessageEnum.MSG_404.getMessage()));
		String message = null;
		switch (demandeChangementMandantEntity.getTypeDemande()) {
		case AJOUT_OPERATIONNEL:
			message = this.ajoutCOMessageMandat;
			break;
		case REMPLACEMENT_OPERATIONNEL:
			message = this.changementCOMessageMandat;
			break;
		case CHANGEMENT_REPRESENTANT:
			switch (motif) {
			case ID_CONFORME_MANDAT_NON_CONFORME:
				message = this.changementRepresentantMessageMandat;
				break;
			case PIECE_ID_NON_CONFORME:
				message = this.changementRepresentantMessageCni;
				break;
			case ID_NON_CONFORME:
				message = this.changementRepresentantMessageIdentite;
				break;

			default:
				break;
			}
			break;
		default:
			break;
		}

		if (message != null) {

			message = StringUtils.replace(message, "[DENOMINATION]", demandeChangementMandantEntity.getEspaceOrganisation().getOrganisation().getDenomination());
			message = StringUtils.replace(message, "[MOTIF]", motif.toString());
			message = StringUtils.replace(message, "[DATE]", new SimpleDateFormat("dd/MM/yyyy à HH:mm").format(demandeChangementMandantEntity.getDateDemande()));
		}

		return message;
	}

	@Override
	public Integer getNombreDemandesParStatut(StatutDemandeEnum statut) {
		return this.demandeChangementMandantRepository.findByStatut(statut).size();

	}

}
