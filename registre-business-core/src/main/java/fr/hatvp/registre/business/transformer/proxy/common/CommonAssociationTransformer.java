/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.common;

import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.persistence.entity.espace.common.CommonAssociationEntity;

/**
 * Proxy de transformation commun pour les associations.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface CommonAssociationTransformer<E extends CommonAssociationEntity>
		extends CommonTransformer<AssoClientDto, E> {
}
