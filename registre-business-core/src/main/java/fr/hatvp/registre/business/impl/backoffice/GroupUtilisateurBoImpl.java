/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.backoffice.GroupUtilisateurBoService;
import fr.hatvp.registre.business.impl.AbstractCommonService;
import fr.hatvp.registre.business.transformer.backoffice.GroupBoTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.GroupBoDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.persistence.entity.backoffice.GroupBoEntity;
import fr.hatvp.registre.persistence.repository.backoffice.GroupBoRepository;
import fr.hatvp.registre.persistence.repository.espace.SurveillanceRepository;

@Component
@Transactional
public class GroupUtilisateurBoImpl extends AbstractCommonService<GroupBoDto, GroupBoEntity> implements GroupUtilisateurBoService {
	
	@Autowired
    private  GroupBoRepository groupBoRepository;
	@Autowired
	private SurveillanceRepository surveillanceRepository;
	@Autowired
	private  GroupBoTransformer groupBoTransformer;


    @Override
    public GroupBoDto addOrUpdateGroup(GroupBoDto groupBoDto) {
        GroupBoEntity groupBoEntity = this.groupBoTransformer.dtoToModel(groupBoDto);
        this.groupBoRepository.save(groupBoEntity);
        return groupBoDto;
    }

    @Override
    public List<GroupBoDto> getAllGroupAndUsers(){
        final List<GroupBoEntity> pageEspaces = this.groupBoRepository.findAll();

        return pageEspaces.stream().map(this.groupBoTransformer::modelToDto).collect(Collectors.toList());
    }

    @Override
    public PaginatedDto<GroupBoDto> getGroupsAndusersBySearchPaginated(String type, String keyword, int page, int occurrence){
        PageRequest pageRequest = new PageRequest(page - 1, occurrence, Sort.Direction.ASC, "id");
        Page<GroupBoEntity> pageEspaces;
        switch(type){
            case "libelle":
                pageEspaces = this.groupBoRepository.findByLibelle('%'+keyword+'%', pageRequest);
                break;
            case "userBo":
                pageEspaces = this.groupBoRepository.findByUserBo('%'+keyword+'%', pageRequest);
                break;
            default:
                pageEspaces = this.groupBoRepository.findAll(pageRequest);
        }
        List<GroupBoDto> groupBoDtos = pageEspaces.getContent().stream().map(this.groupBoTransformer::modelToDto).collect(Collectors.toList());
        return new PaginatedDto<>(groupBoDtos,pageEspaces.getTotalElements());
    }

    @Override
    public void deleteGroup(Long id) {
    	if (!this.surveillanceRepository.findBysurveillantsGroupes(this.groupBoRepository.findById(id)).isEmpty()) {
    		throw new BusinessGlobalException("Vous ne pouvez pas supprimer ce groupe car des fonctions de surveillance d'organisations lui sont affectées.");
    	}
    	
        this.groupBoRepository.delete(id);
    }

    @Override
    public JpaRepository<GroupBoEntity, Long> getRepository() {
        return null;
    }

    @Override
    public CommonTransformer<GroupBoDto, GroupBoEntity> getTransformer() {
        return null;
    }
}
