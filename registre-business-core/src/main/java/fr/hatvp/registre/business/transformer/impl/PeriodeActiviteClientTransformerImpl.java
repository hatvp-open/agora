/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.PeriodeActiviteClientTransformer;
import fr.hatvp.registre.commons.dto.PeriodeActiviteClientDto;
import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.espace.ClientEntity;
import fr.hatvp.registre.persistence.entity.espace.PeriodeActiviteClientEntity;
import fr.hatvp.registre.persistence.entity.publication.PubClientEntity;
import fr.hatvp.registre.persistence.repository.publication.PubClientRepository;

/**
 * Classe de transformation de la classe {@link ClientEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class PeriodeActiviteClientTransformerImpl extends AbstractCommonTansformer<PeriodeActiviteClientDto, PeriodeActiviteClientEntity> implements PeriodeActiviteClientTransformer {
	private static final Logger LOGGER = LoggerFactory.getLogger(PeriodeActiviteClientTransformerImpl.class);
	
	@Autowired
	private PubClientRepository pubClientRepository;
	
	@Override
	public PeriodeActiviteClientEntity dtoToModel(final PeriodeActiviteClientDto dto) {
		final PeriodeActiviteClientEntity model = new PeriodeActiviteClientEntity();
		RegistreBeanUtils.copyProperties(model, dto);

		return model;
	}

	@Override
	public PeriodeActiviteClientDto modelToDto(PeriodeActiviteClientEntity model) {
		PeriodeActiviteClientDto periodeActiviteClientDto = new PeriodeActiviteClientDto();				
		RegistreBeanUtils.copyProperties(periodeActiviteClientDto, model);

		//Récupération liste publication du client pour une période d'activité donnée (même date d'ajout)
		List<PubClientEntity> pubClientList = 
				this.pubClientRepository.findByEspaceOrganisationIdAndOrganisationIdAndDateAjoutOrderByIdAsc(model.getClient().getEspaceOrganisation().getId(), model.getClient().getOrganisation().getId(), model.getDateAjout().truncatedTo(ChronoUnit.MINUTES));

		if(!pubClientList.isEmpty()) {
			
			periodeActiviteClientDto.setDatePublicationActivation(pubClientList.get(0).getPublication().getCreationDate());
			//on récupère la 1ère ligne de publication contenant la date de publication
			for(PubClientEntity pub : pubClientList) {
				if(pub.getDateDesactivation() != null ) periodeActiviteClientDto.setDatePublicationDesactivation(pub.getPublication().getCreationDate());
			}		
		}
		
		return periodeActiviteClientDto;
	}

	@Override
	public List<PeriodeActiviteClientDto> modelToDto(List<PeriodeActiviteClientEntity> models) {
		List<PeriodeActiviteClientDto> list = new ArrayList<>();
		
		for(PeriodeActiviteClientEntity model : models) {
			PeriodeActiviteClientDto periodeActiviteClientDto = modelToDto(model);
			list.add(periodeActiviteClientDto);		
		}		
		return list;
	}
	
	
}
