/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.common;

import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.persistence.entity.espace.common.CommonDirigeantEntity;

/**
 * Interface de transformation commune des dirigeants.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface CommonDirigeantTransformer<E extends CommonDirigeantEntity>
        extends CommonTransformer<DirigeantDto, E>
{
}