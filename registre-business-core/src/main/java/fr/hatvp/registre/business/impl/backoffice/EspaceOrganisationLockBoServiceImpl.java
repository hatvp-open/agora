

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice;

import fr.hatvp.registre.business.backoffice.EspaceOrganisationLockBoService;
import fr.hatvp.registre.business.impl.AbstractCommonService;
import fr.hatvp.registre.business.transformer.backoffice.EspaceOrganisationLockBoTransformer;
import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.EspaceOrganisationLockBoDto;
import fr.hatvp.registre.persistence.entity.backoffice.EspaceOrganisationLockBoEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.repository.backoffice.EspaceOrganisationLockBoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service de gestion des locks des validations d'espaces.
 *
 * @version $Revision$ $Date${0xD}
 */
@Transactional(readOnly = true)
@Service
public class EspaceOrganisationLockBoServiceImpl
    extends AbstractCommonService<EspaceOrganisationLockBoDto, EspaceOrganisationLockBoEntity>
    implements EspaceOrganisationLockBoService {
    /** Repository du service. */
    @Autowired
    private EspaceOrganisationLockBoRepository espaceOrganisationLockBoRepository;

    /** Transformer du service */
    @Autowired
    private EspaceOrganisationLockBoTransformer espaceOrganisationLockBoTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    public EspaceOrganisationLockBoDto getLockOn(final Long espaceOrganisationId) {
        final List<EspaceOrganisationLockBoDto> result = this.espaceOrganisationLockBoRepository
            .findByEspaceOrganisationId(espaceOrganisationId).stream()
            .map(this.espaceOrganisationLockBoTransformer::modelToDto)
            .filter(dto -> dto.getLockTimeRemain() > 0).collect(Collectors.toList());

        if (!result.isEmpty()) {
            return result.get(0);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void setLockOn(final Long espaceOrganisationId, final Long utilisateurBoId) {
        final EspaceOrganisationLockBoEntity espaceOrganisationLockBoEntity = new EspaceOrganisationLockBoEntity();

        final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
        utilisateurBoEntity.setId(utilisateurBoId);
        espaceOrganisationLockBoEntity.setUtilisateurBo(utilisateurBoEntity);

        espaceOrganisationLockBoEntity.setLockedTime(new Date());

        final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(espaceOrganisationId);
        espaceOrganisationLockBoEntity.setEspaceOrganisation(espaceOrganisationEntity);

        this.espaceOrganisationLockBoRepository.save(espaceOrganisationLockBoEntity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public EspaceOrganisationLockBoDto updateLockOnEspace(final EspaceOrganisationLockBoDto dto,
                                                          final Long connectedUserId) {
        final List<EspaceOrganisationLockBoEntity> result = this.espaceOrganisationLockBoRepository
            .findByEspaceOrganisationId(dto.getEspaceOrganisationId());

        if (!result.isEmpty()) {
            final EspaceOrganisationLockBoEntity entity = result.get(0);
            entity.setVersion(dto.getVersion());
            entity.setLockedTime(new Date());
            return this.espaceOrganisationLockBoTransformer
                .modelToDto(this.espaceOrganisationLockBoRepository.save(entity));

        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JpaRepository<EspaceOrganisationLockBoEntity, Long> getRepository() {
        return this.espaceOrganisationLockBoRepository;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommonTransformer<EspaceOrganisationLockBoDto, EspaceOrganisationLockBoEntity> getTransformer() {
        return this.espaceOrganisationLockBoTransformer;
    }
}
