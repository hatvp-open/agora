/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.pdf.activite;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;

public class RapportActivitePdfTemplateData {

	private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

	private String denomination;

	private String dateDebut;

	private String dateFin;

	private String chiffreAffaire;

	private Boolean hasNotChiffreAffaire;

	private String montantDepense;

	private String nombreSalaries;

	private List<Activite> activites;

	private boolean noActivite;

	public RapportActivitePdfTemplateData(ExerciceComptableEntity exerciceComptable) {
		this.dateDebut = dateFormatter.format(exerciceComptable.getDateDebut());
		this.dateFin = dateFormatter.format(exerciceComptable.getDateFin());
		this.chiffreAffaire = Optional.ofNullable(exerciceComptable.getHasNotChiffreAffaire()).orElse(Boolean.FALSE) ? "Organisation sans chiffre d'affaires"
				: Optional.ofNullable(exerciceComptable.getChiffreAffaire()).isPresent() ? exerciceComptable.getChiffreAffaire().getLibelle() : "Pas de chiffre d'affaire déclaré";
		this.hasNotChiffreAffaire = exerciceComptable.getHasNotChiffreAffaire();
		this.montantDepense = Optional.ofNullable(exerciceComptable.getMontantDepense()).isPresent() ? exerciceComptable.getMontantDepense().getLibelle()
				: "Pas de montant des dépenses déclaré";
		this.nombreSalaries = exerciceComptable.getNombreSalaries() == null ? "Nombre d'employés non déclaré" : exerciceComptable.getNombreSalaries().toString();
		this.activites = exerciceComptable.getActivitesRepresentationInteret().stream().map(activite -> new Activite(activite)).collect(Collectors.toList());
		this.denomination = exerciceComptable.getEspaceOrganisation().getNomUsage() != null ? exerciceComptable.getEspaceOrganisation().getNomUsage()
				: exerciceComptable.getEspaceOrganisation().getOrganisation().getDenomination();
		this.noActivite = exerciceComptable.isNoActivite();
	}

	public String getDateDebut() {
		return dateDebut;
	}

	public String getDateFin() {
		return dateFin;
	}

	public String getChiffreAffaire() {
		return chiffreAffaire;
	}

	public Boolean getHasNotChiffreAffaire() {
		return hasNotChiffreAffaire;
	}

	public String getMontantDepense() {
		return montantDepense;
	}

	public String getNombreSalaries() {
		return nombreSalaries;
	}

	public List<Activite> getActivites() {
		return activites;
	}

	public String getDenomination() {
		return denomination;
	}

	public boolean isNoActivite() { return noActivite; }

}
