/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication.activite;

import java.util.List;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketActiviteDto;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;

/**
 * Interface de transformation de la classe {@link fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketActiviteDto} & {@link fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationBucketActiviteTransformer
        extends CommonTransformer<PublicationBucketActiviteDto, PublicationActiviteEntity> {


    /**
     * Préparer le le bucket du livrable à partir d'une liste de publications;
     *
     * @param publications liste de publication.
     * @return le livrable pret à utiliser.
     */
    PublicationBucketActiviteDto listPublicationToPublicationBucketActivite(List<PublicationActiviteEntity> publications,boolean withHistorique);
}
