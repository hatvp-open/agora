/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication.activite;

import java.util.List;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketExerciceDto;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity;

/**
 * Interface de transformation de la classe {@link fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketExerciceDto} & {@link fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationBucketExerciceTransformer
        extends CommonTransformer<PublicationBucketExerciceDto, PublicationExerciceEntity> {


    /**
     * Préparer le le bucket du livrable à partir d'une liste de publications;
     *
     * @param publications liste de publication.
     * @return le livrable pret à utiliser.
     */
    PublicationBucketExerciceDto listPublicationToPublicationBucketExercice(List<PublicationExerciceEntity> publications,boolean withHistorique);
}
