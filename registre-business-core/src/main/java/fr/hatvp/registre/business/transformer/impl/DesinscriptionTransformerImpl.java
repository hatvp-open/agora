/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.DesinscriptionPieceTransformer;
import fr.hatvp.registre.business.transformer.proxy.DesinscriptionTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationTransformer;
import fr.hatvp.registre.commons.dto.DesinscriptionDto;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DesinscriptionTransformerImpl extends AbstractCommonTansformer<DesinscriptionDto, DesinscriptionEntity>
    implements DesinscriptionTransformer {

    @Autowired
    EspaceOrganisationTransformer espaceOrganisationTransformer;
    @Autowired
    DesinscriptionPieceTransformer desinscriptionPieceTransformer;
    @Autowired
    DesinscriptionRepository desinscriptionRepository;

    @Override
    public DesinscriptionDto modelToDto(DesinscriptionEntity model) {
        DesinscriptionDto dto = new DesinscriptionDto();
        if(model != null){
            dto.setId(model.getId());
            dto.setCessationDate(model.getCessationDate());
            dto.setMotif(model.getMotif());
            dto.setObservation(model.getObservationAgent());
            dto.setObservationDeclarant(model.getObservationDeclarant());
            dto.setPieces(this.desinscriptionPieceTransformer.modelToDto(model.getPieces()));
            dto.setEspaceOrganisation(this.espaceOrganisationTransformer.modelToDto(model.getEspaceOrganisation()));
            dto.setOrigineSaisie(model.getOrigineSaisie());
        }
        return dto;
    }

    @Override
    public DesinscriptionEntity dtoToModel(DesinscriptionDto dto) {
        DesinscriptionEntity model = dto.getId() != null ? this.desinscriptionRepository.findOne(dto.getId()) : new DesinscriptionEntity();
        if(dto.getEspaceOrganisation() != null){
            model.setEspaceOrganisation(this.espaceOrganisationTransformer.dtoToModel(dto.getEspaceOrganisation()));
        }
        model.setCessationDate(dto.getCessationDate());
        model.setMotif(dto.getMotif());
        model.setObservationAgent(dto.getObservation());
        model.setObservationDeclarant(dto.getObservationDeclarant());
        model.setOrigineSaisie(dto.getOrigineSaisie());
        return model;
    }
}
