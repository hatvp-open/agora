/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.backoffice.EspaceOrganisationLockBoTransformer;
import fr.hatvp.registre.business.transformer.backoffice.ValidationEspaceOrganisationSimpleTransformer;
import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceBoSimpleDto;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;

@Component
public class ValidationEspaceOrganisationSimpleTransformerImpl extends AbstractCommonTansformer<ValidationEspaceBoSimpleDto, EspaceOrganisationEntity>
		implements ValidationEspaceOrganisationSimpleTransformer {

	/** Transformer pour le Lock Espace Organisation. */
	@Autowired
	private EspaceOrganisationLockBoTransformer espaceOrganisationLockBoTransformer;

	@Override
	public ValidationEspaceBoSimpleDto modelToDto(EspaceOrganisationEntity entity) {
		final ValidationEspaceBoSimpleDto dto = new ValidationEspaceBoSimpleDto();

		dto.setId(entity.getId());
		dto.setValidationInscriptionId(entity.getCreateurEspaceOrganisation().getInscriptionEspaces().stream()
				.filter(i -> i.getEspaceOrganisation().getId().longValue() == entity.getId().longValue()).findAny().orElseGet(InscriptionEspaceEntity::new).getId());
		dto.setDenomination(entity.getOrganisation().getDenomination());
		dto.setCreateurNomComplet(entity.getCreateurEspaceOrganisation().getCivility().getLibelleLong() + " " + entity.getCreateurEspaceOrganisation().getNom() + " "
				+ entity.getCreateurEspaceOrganisation().getPrenom());
		dto.setCreationDate(entity.getCreationDate());
		dto.setStatut(entity.getStatut());
		dto.setLock(entity.getEspaceOrganisationLockBoEntityList().stream().map(this.espaceOrganisationLockBoTransformer::modelToDto).filter(lock -> lock.getLockTimeRemain() > 0)
				.findFirst().orElse(null));

		return dto;
	}

}
