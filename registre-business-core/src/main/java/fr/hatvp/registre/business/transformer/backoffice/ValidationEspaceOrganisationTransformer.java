/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceBoDto;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * Interface de transformation de la classe {@link EspaceOrganisationEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
public interface ValidationEspaceOrganisationTransformer
    extends CommonTransformer<ValidationEspaceBoDto, EspaceOrganisationEntity>
{

}
