
/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.CommonCollaborateurTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.CollaborateurTransformer;
import fr.hatvp.registre.persistence.entity.espace.CollaborateurEntity;

/**
 * Classe de transformation de la classe {@link CollaborateurEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class CollaborateurTransformerImpl
    extends CommonCollaborateurTransformerImpl<CollaborateurEntity>
    implements CollaborateurTransformer
{

    /**
     * Init class;
     */
    public CollaborateurTransformerImpl() {
        super.entityClass = CollaborateurEntity.class;
    }
}
