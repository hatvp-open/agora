/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.email;

import java.util.Locale;

import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import fr.hatvp.registre.commons.dto.DeclarantDto;

@Component
public class EspaceOrganisationMailer extends ApplicationMailer {
    /**
     * Initialisation du {@link JavaMailSender}
     *
     * @param sender
     * @param templateEngine
     */
    protected EspaceOrganisationMailer(JavaMailSender sender, TemplateEngine templateEngine) {
        super(sender, templateEngine);
    }

    /** Template de refus de création d'espace. */
    private static final String desinscriptionNoActivite = "email/desinscription/desinscriptionNoActivite";

    /**
     *
     * @param to
     * @param denomination
     */
    public void sendDesinscriptionNoActiviteMail(final DeclarantDto to, final String denomination, InputStreamSource inputStreamSource){
        this.prepareAndSendEspaceEmailAvecPieceJointe(to, "Demande de désinscription", denomination, desinscriptionNoActivite, "demande_desinscription.pdf", inputStreamSource);
    }

    /**
     * Préparer et envoyer le mail.
     */
    private void prepareAndSendEspaceEmail(final DeclarantDto to, final String object, final  String denomination,
                                           final String template)
    {

        final Context ctx = new Context(Locale.FRANCE);
        ctx.setVariable("civilite", to.getCivility().getLibelleLong());
        ctx.setVariable("denomination", denomination);
        ctx.setVariable("imageResourceName", this.imageResourceName);

        final String mailContent = this.templateEngine.process(template, ctx);
        super.prepareAndSendEmailWhithLogo(to.getEmail(), object, mailContent);
    }
    
    /**
     * Préparer et envoyer le mail avec une pièce jointe
     * 
     */
    private void prepareAndSendEspaceEmailAvecPieceJointe(final DeclarantDto to, final String object, final  String denomination,
                                           final String template, String nomPieceJointe, InputStreamSource inputStreamSource)
    {

        final Context ctx = new Context(Locale.FRANCE);
        ctx.setVariable("civilite", to.getCivility().getLibelleLong());
        ctx.setVariable("denomination", denomination);
        ctx.setVariable("imageResourceName", this.imageResourceName);

        final String mailContent = this.templateEngine.process(template, ctx);
        super.prepareAndSendEmailWhithLogoAndPieceJointe(to.getEmail(), object, mailContent, nomPieceJointe, inputStreamSource);
    }
}
