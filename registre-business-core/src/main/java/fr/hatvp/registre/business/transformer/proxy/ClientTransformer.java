/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonClientTransformer;
import fr.hatvp.registre.persistence.entity.espace.ClientEntity;

/**
 * Interface de transformation de la classe {@link ClientEntity}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface ClientTransformer
		extends CommonClientTransformer<ClientEntity> {
	
}
