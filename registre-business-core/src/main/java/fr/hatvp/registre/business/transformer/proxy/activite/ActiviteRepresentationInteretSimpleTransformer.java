/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.activite;

import java.util.List;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.activite.ActiviteRepresentationInteretSimpleDto;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;

public interface ActiviteRepresentationInteretSimpleTransformer extends CommonTransformer<ActiviteRepresentationInteretSimpleDto, ActiviteRepresentationInteretEntity> {
	
	List<ActiviteRepresentationInteretSimpleDto> modelToDtoSansStatutSupprimee(List<ActiviteRepresentationInteretEntity> entities);
}
