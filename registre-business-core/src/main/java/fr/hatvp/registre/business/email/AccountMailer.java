/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.email;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import fr.hatvp.registre.commons.dto.DeclarantDto;

/**
 * Classe d'envoi de mails pour les déclarants.
 * 
 * 1- Activation de compte
 * 2- Récupération de compte
 * 3- Confirmation du changement d'adresse email principale
 **/
@Component
public class AccountMailer
    extends ApplicationMailer
{

    /** Initialisation de la classe. */
    @Autowired
    public AccountMailer(final JavaMailSender sender, final TemplateEngine templateEngine)
    {
        super(sender, templateEngine);
    }

    /** Email récupération de compte. */
    @Value("${mail.account.recover.link}")
    private String recoverAccountLink;
    @Value("${mail.account.recover.subject}")
    private String recoverAccountObject;

    /** Email création de compte. */
    @Value("${mail.account.create.url}")
    private String createAccountValidationLinkUrl;
    @Value("${mail.account.create.subject}")
    private String createAccountObject;

    /** Email modification de l'email principale. */
    @Value("${mail.account.update.email.url}")
    private String updateAccountValidationLink;
    @Value("${mail.account.update.email.subject}")
    private String updateAccountEmailObject;

    /** Template ajout d'un compte */
    private static final String ACCOUNT_ADD_TEMPLATE = "email/account/add_account";
    /** Template mise à jour de l'email principale */
    private static final String ACCOUNT_UPDATE_TEMPLATE = "email/account/confirm_new_email";
    /** Template récupération de compte */
    private static final String ACCOUNT_RECOVER_TEMPLATE = "email/account/recover_account";

    /**
     * @param to récepteur.
     * @param confirmationCode Send email to user to confirm account
     */
    public void sendAccountConfirmationEmail(final DeclarantDto to, final String confirmationCode)
    {
        final String accountActivationLick = this.frontRootPath
                + this.createAccountValidationLinkUrl + confirmationCode + "&type="
                + TypeVallidation.COMPTE.getValue();

        this.sendEmail(to, accountActivationLick, ACCOUNT_ADD_TEMPLATE, this.createAccountObject);

    }

    /**
     * @param to récepteur.
     * @param confirmationCode send email to user to confirm the new email
     */
    public void sendEmailUpdateConfirmation(final DeclarantDto to, final String confirmationCode)
    {

        final String activateLink = this.frontRootPath + this.updateAccountValidationLink
                + confirmationCode + "&type=" + TypeVallidation.EMAIl.getValue();

        this.sendEmail(to, activateLink, ACCOUNT_UPDATE_TEMPLATE, this.updateAccountEmailObject);

    }

    /**
     * @param to récepteur.
     * @param confirmationCode Send email to use with a token to recover the account
     */
    public void sendRecoverPasswordEmail(final DeclarantDto to, final String confirmationCode)
    {

        final String activateLink = this.frontRootPath + this.recoverAccountLink + confirmationCode;

        this.sendEmail(to, activateLink, ACCOUNT_RECOVER_TEMPLATE, this.recoverAccountObject);

    }

    /**
     * Méthode commune d'envoi des emails de cette classe.
     */
    private void sendEmail(final DeclarantDto to, final String activateLink, final String template, final String object)
    {

        final Context ctx = new Context(Locale.FRANCE);
        ctx.setVariable("civilite", to.getCivility().getLibelleLong());
        ctx.setVariable("urlActivation", activateLink);
        ctx.setVariable("imageResourceName", this.imageResourceName);

        final String mailContent = this.templateEngine.process(template, ctx);
        super.prepareAndSendEmailWhithLogo(to.getEmail(), object, mailContent);
    }

    /**
     * Les types de validation.
     */
    enum TypeVallidation {

        COMPTE(1), EMAIl(2);

        private final int type;

        TypeVallidation(final int type)
        {
            this.type = type;
        }

        public int getValue()
        {
            return this.type;
        }
    }

}
