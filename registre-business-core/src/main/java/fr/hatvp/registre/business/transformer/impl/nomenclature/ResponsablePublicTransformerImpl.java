/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.nomenclature;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.proxy.nomenclature.ResponsablePublicTransformer;
import fr.hatvp.registre.commons.dto.nomenclature.ResponsablePublicDto;
import fr.hatvp.registre.persistence.entity.nomenclature.ResponsablePublicEntity;

@Component
public class ResponsablePublicTransformerImpl extends AbstractCommonNomenclatureTransformer<ResponsablePublicDto, ResponsablePublicEntity> implements ResponsablePublicTransformer {

	/** Init super DTO. */
	public ResponsablePublicTransformerImpl() {
		super.dtoClass = ResponsablePublicDto.class;
	}

}
