/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.itextpdf.text.DocumentException;

import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;

@Component
public class PdfGeneratorUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(PdfGeneratorUtil.class);

	@Autowired
	private TemplateEngine templateEngine;

	public byte[] createPdf(final String templateName, final Map<String, Object> context) {
		byte[] pdf;
		try {
			final String html = createHtmlTemplate(templateName, context);
			final String xhtml = convertHtmlToXhtml(html);

			pdf = generatePdf(xhtml.replaceAll("[\\u0000-\\u001f]", " "));
		} catch (Exception ex) {
			LOGGER.error("Echec de génération du pdf : {}", ex);
			throw new BusinessGlobalException(ErrorMessageEnum.ECHEC_GENERATION_PDF.getMessage());
		}

		return pdf;
	}

	private String createHtmlTemplate(final String templateName, final Map<String, Object> data) {

		final Context context = new Context(Locale.FRANCE);

		data.forEach(context::setVariable);

        return templateEngine.process(templateName, context);
	}

	private String convertHtmlToXhtml(String html) throws UnsupportedEncodingException {
		final Tidy tidy = new Tidy();
		tidy.setInputEncoding(StandardCharsets.UTF_8.name());
		tidy.setOutputEncoding(StandardCharsets.UTF_8.name());
		tidy.setXHTML(true);
		ByteArrayInputStream inputStream = new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8));
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		tidy.parseDOM(inputStream, outputStream);
		return outputStream.toString(StandardCharsets.UTF_8.name());

	}

	private byte[] generatePdf(String xhtml) throws DocumentException, IOException {
		ITextRenderer renderer = new ITextRenderer();
		renderer.setDocumentFromString(xhtml);
		renderer.layout();

		try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
			renderer.createPDF(bos);
			return bos.toByteArray();
		}
	}

}
