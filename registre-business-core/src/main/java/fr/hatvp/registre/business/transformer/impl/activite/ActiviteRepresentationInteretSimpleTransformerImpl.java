/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.activite;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.transformer.impl.common.AbstractCommonTansformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ActiviteRepresentationInteretSimpleTransformer;
import fr.hatvp.registre.commons.dto.activite.ActiviteRepresentationInteretSimpleDto;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;

@Component
public class ActiviteRepresentationInteretSimpleTransformerImpl extends AbstractCommonTansformer<ActiviteRepresentationInteretSimpleDto, ActiviteRepresentationInteretEntity>
		implements ActiviteRepresentationInteretSimpleTransformer {

	@Override
	public List<ActiviteRepresentationInteretSimpleDto> modelToDto(List<ActiviteRepresentationInteretEntity> entities) {
		List<ActiviteRepresentationInteretSimpleDto> dtos = new ArrayList<>();

		entities.forEach(entity -> {
			dtos.add(modelToDto(entity));
		});

		return dtos;
	}

	@Override
	public ActiviteRepresentationInteretSimpleDto modelToDto(ActiviteRepresentationInteretEntity entity) {
		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
		ActiviteRepresentationInteretSimpleDto dto = new ActiviteRepresentationInteretSimpleDto();

		dto.setId(entity.getId());
		dto.setIdFiche(entity.getIdFiche());
		dto.setObjet(entity.getObjet());
		dto.setStatut(entity.getStatut());
		dto.setDomainesIntervention(entity.getDomainesIntervention().stream().map(di -> {
			return di.getLibelle();
		}).collect(Collectors.toList()));
		dto.setNomCreateur(entity.getCreateurDeclaration().getNom() + " " + entity.getCreateurDeclaration().getPrenom());
		dto.setExerciceComptable(entity.getExerciceComptable().getId());
		dto.setDateCreation(formater.format(entity.getCreationDate()));

		return dto;
	}
	/**
	 * Suppression des activités supprimées pour affichage FO
	 */
	@Override
	public List<ActiviteRepresentationInteretSimpleDto> modelToDtoSansStatutSupprimee(List<ActiviteRepresentationInteretEntity> entities) {
		List<ActiviteRepresentationInteretSimpleDto> dtos = new ArrayList<>();

		entities.forEach(entity -> {
			if(!entity.getStatut().equals(StatutPublicationEnum.SUPPRIMEE)) {
				dtos.add(modelToDto(entity));
			}
			
		});

		return dtos;
	}

}
