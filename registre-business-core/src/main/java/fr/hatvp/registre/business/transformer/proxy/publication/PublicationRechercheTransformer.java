/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.publication;

import java.util.List;
import java.util.stream.Stream;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.publication.PublicationRechercheBlacklistDto;
import fr.hatvp.registre.commons.dto.publication.PublicationRechercheDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActiviteRechercheDto;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;

/**
 * Proxy du transformeur entre {@link PublicationEntity} et {@link PublicationRechercheDto}
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationRechercheTransformer
        extends CommonTransformer<PublicationRechercheDto, PublicationEntity> {
	
	/**
     * Convertit un Stream de PublicationEntity en liste de PublicationRechercheDto.
     * Permet de générer le document de recherche des publications sans charger les résultats de la base de données depuis une List.
     * 
     * @return la liste à convertir en JSON
     */
    List<PublicationRechercheDto> modelToDto(final Stream<PublicationEntity> models);

	PublicationRechercheDto modelActiviteToDtoPublicationRecherche(PublicationActiviteEntity entity);
	
	PublicationActiviteRechercheDto modelActiviteToDtoPublicationActiviteRecherche(PublicationActiviteEntity entity);

	PublicationRechercheDto modelExerciceToDtoPublicationRecherche(ExerciceComptableEntity entity);

	PublicationRechercheDto modelExerciceToDtoPublicationRechercheBlacklist(EspaceOrganisationEntity entity);
	PublicationRechercheBlacklistDto modelExerciceToDtoPublicationRechercheBlacklist(ExerciceComptableEntity entity);
}
