/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy;

import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.backoffice.ParametresDto;
import fr.hatvp.registre.persistence.entity.limitation.ParametresEntity;

/**
 * Interface de transformation de la classe {@link fr.hatvp.registre.persistence.entity.limitation.ParametresEntity} & {@link fr.hatvp.registre.commons.dto.backoffice.ParametresDto}.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface ParametresTransformer
    extends CommonTransformer<ParametresDto, ParametresEntity>
{

}
