/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.publication;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.hatvp.registre.business.transformer.impl.common.CommonPublicationTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationFrontTransformer;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.commons.dto.publication.PublicationFrontDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import org.springframework.stereotype.Component;

/**
 * Classe du transformeur entre {@link PublicationEntity} et {@link PublicationFrontDto}
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class PublicationFrontTransformerImpl
    extends CommonPublicationTransformerImpl<PublicationFrontDto>
    implements PublicationFrontTransformer {

    /** Init super DTO. */
    public PublicationFrontTransformerImpl() {
        super.dtoClass = PublicationFrontDto.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PublicationEntity dtoToModel(final PublicationDto dto) {
        final PublicationEntity entity = super.dtoToModel(dto);

        /** convertir la grappe d'objet de publication en string json. */
        final ObjectMapper mapper = new ObjectMapper();
        final String jsonInString;
        try {
            dto.setCreationDate(entity.getCreationDate());
            jsonInString = mapper.writeValueAsString(dto);
            entity.setPublicationJson(jsonInString);
        } catch (final JsonProcessingException e) {
            throw new BusinessGlobalException("Impossible de créer le json de la publication");
        }

        return entity;
    }
}
