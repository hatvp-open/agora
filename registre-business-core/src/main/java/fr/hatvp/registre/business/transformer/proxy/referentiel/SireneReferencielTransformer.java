/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.proxy.referentiel;


import fr.hatvp.registre.business.transformer.proxy.common.CommonTransformer;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielSireneEntity;

/**
 * Proxy du transformeur entre {@link OrganisationDto} et {@link ReferentielSireneEntity}.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface SireneReferencielTransformer
        extends CommonTransformer<OrganisationDto, ReferentielSireneEntity>
{

}
