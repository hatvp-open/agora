/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.publication;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * Test de la classe {@link PublicationTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class PublicationTransformerTest {

    /** Transformer des publications. */
    @Autowired
    private PublicationTransformer publicationTransformer;

    /**
     * Test de la métode modelToDto de {@link PublicationTransformer#dtoToModel(AbstractCommonDto)}
     */
    @Test
    public void testModelToDto() {
        final PublicationEntity entity = CommonTestClass.getPublicationEntity();

        final PublicationDto dto = this.publicationTransformer.modelToDto(entity);

        Assert.assertNotNull(dto);

        Assert.assertNull(dto.getId());
        Assert.assertNull(dto.getVersion());
        Assert.assertNotEquals(entity.getId(), dto.getId());
        Assert.assertNotEquals(entity.getVersion(), dto.getVersion());

        Assert.assertEquals(entity.getAdresse(), dto.getAdresse());
        Assert.assertEquals(entity.getCodePostal(), dto.getCodePostal());
        Assert.assertEquals(entity.getVille(), dto.getVille());
        Assert.assertEquals(entity.getPays(), dto.getPays());

        Assert.assertEquals(entity.getTelephoneDeContact(), dto.getTelephoneDeContact());
        Assert.assertEquals(entity.getEmailDeContact(), dto.getEmailDeContact());
        Assert.assertEquals(entity.getLienSiteWeb(), dto.getLienSiteWeb());
        Assert.assertEquals(entity.getLienPageTwitter(), dto.getLienPageTwitter());
        Assert.assertEquals(entity.getLienPageLinkedin(), dto.getLienPageLinkedin());
        Assert.assertEquals(entity.getLienListeTiers(), dto.getLienListeTiers());
        Assert.assertEquals(entity.getLienPageFacebook(), dto.getLienPageFacebook());

        Assert.assertEquals(2, dto.getDirigeants().size());
        Assert.assertEquals(2, dto.getCollaborateurs().size());
        Assert.assertEquals(2, dto.getClients().size());
        Assert.assertEquals(2, dto.getAffiliations().size());

        Assert.assertEquals(entity.getNomUsage(), dto.getNomUsage());
    }

    /**
     * Test de la métode modelToDto de {@link PublicationTransformer#modelToDto(AbstractCommonEntity)}
     */
    @Test
    public void testDtoToModel() {

        final PublicationDto dto = CommonTestClass.getPublicationDto();

        final PublicationEntity entity = this.publicationTransformer.dtoToModel(dto);

        Assert.assertNotNull(entity);

        Assert.assertEquals(dto.getId(), entity.getId());
        Assert.assertNull(entity.getVersion());

        Assert.assertEquals(dto.getAdresse(), entity.getAdresse());
        Assert.assertEquals(dto.getCodePostal(), entity.getCodePostal());
        Assert.assertEquals(dto.getVille(), entity.getVille());
        Assert.assertEquals(dto.getPays(), entity.getPays());

        Assert.assertEquals(dto.getTelephoneDeContact(), entity.getTelephoneDeContact());
        Assert.assertEquals(dto.getEmailDeContact(), entity.getEmailDeContact());
        Assert.assertFalse(entity.getNonPublierMonAdresseEmail());
        Assert.assertFalse(entity.getNonPublierMonTelephoneDeContact());

        Assert.assertNull(entity.getLienSiteWeb());
        Assert.assertNull(entity.getLienPageTwitter());
        Assert.assertNull(entity.getLienPageLinkedin());
        Assert.assertNull(entity.getLienListeTiers());
        Assert.assertNull(entity.getLienPageFacebook());

        Assert.assertEquals(1, entity.getDirigeants().size());
        Assert.assertEquals(1, entity.getCollaborateurs().size());
        Assert.assertEquals(0, entity.getClients().size());
        Assert.assertEquals(0, entity.getAssociations().size());

        Assert.assertEquals(dto.getNomUsage(), entity.getNomUsage());

    }

}
