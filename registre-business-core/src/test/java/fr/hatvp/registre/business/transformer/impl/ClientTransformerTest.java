/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.ClientTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.espace.ClientEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.PeriodeActiviteClientEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * Test de la classe {@link fr.hatvp.registre.business.transformer.proxy.ClientTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestBusinessConfig.class },
		loader = AnnotationConfigContextLoader.class)
public class ClientTransformerTest {
	
	/** Transformer des clients de l'espace organisation. */
	@Autowired
	private ClientTransformer clientTransformer;

	
	/**
	 * Test de la métode modelToDto de {@link ClientTransformer#dtoToModel(AbstractCommonDto)}
	 */
	@Test
	public void testDtoToModel() {
		final AssoClientDto assoClientDto = getClientDto();
		
		final ClientEntity entity = this.clientTransformer.dtoToModel(assoClientDto);
		
		Assert.assertNotNull(entity);
		Assert.assertEquals(assoClientDto.getId(), entity.getId());
		Assert.assertEquals(assoClientDto.getVersion(), entity.getVersion());
	}
	
	/**
	 * Test de la métode modelToDto de {@link ClientTransformer#modelToDto(AbstractCommonEntity)}
	 */
	@Test
	public void testModelToDtoSansEspace() {
		final ClientEntity clientEntity = getClientEntity();
		
		final AssoClientDto dto = this.clientTransformer.modelToDto(clientEntity);
		
		Assert.assertNotNull(dto);
		Assert.assertEquals(clientEntity.getId(), dto.getId());
		Assert.assertEquals(clientEntity.getVersion(), dto.getVersion());
		Assert.assertEquals(clientEntity.getEspaceOrganisation().getId(), dto.getEspaceOrganisationId());
		Assert.assertEquals(clientEntity.getOrganisation().getId(), dto.getOrganisationId());
		Assert.assertEquals(clientEntity.getOrganisation().getDenomination(), dto.getDenomination());
		Assert.assertEquals(clientEntity.getOrganisation().getSiren(), dto.getNationalId());
		Assert.assertNull(dto.getEspaceCollaboratifDto());
	}
		
	/**
	 * Récupérer une instance de {@link ClientEntity}
	 */
	private ClientEntity getClientEntity() {
		final ClientEntity clientEntity = new ClientEntity();
		clientEntity.setId(RegistreUtils.randomLong(1));
		clientEntity.setVersion(RegistreUtils.randomLong(1).intValue());
		
		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(RegistreUtils.randomLong(1));
		organisationEntity.setSiren(RegistreUtils.randomString(10));
		organisationEntity.setDenomination(RegistreUtils.randomString(30));
		organisationEntity.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
		clientEntity.setOrganisation(organisationEntity);
		
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(RegistreUtils.randomLong(1));
		espaceOrganisationEntity.setAdresse(RegistreUtils.randomString(30));
		clientEntity.setEspaceOrganisation(espaceOrganisationEntity);
		
		final PeriodeActiviteClientEntity periode = new PeriodeActiviteClientEntity();
		periode.setClient(clientEntity);
		periode.setId(1L);
		periode.setDateAjout(LocalDateTime.now());
		periode.setDateDesactivation(null);
		periode.setDateFinContrat(null);
		periode.setCommentaire(null);
		
		List<PeriodeActiviteClientEntity> periodeList = new ArrayList<PeriodeActiviteClientEntity>();
		periodeList.add(periode);
		
		clientEntity.setPeriodeActiviteClientList(periodeList);
		
		return clientEntity;
		
	}
	
	/**
	 * Récupérer une instance de {@link AssoClientDto}
	 */
	private AssoClientDto getClientDto() {
		final AssoClientDto assoClientDto = new AssoClientDto();
		assoClientDto.setDenomination(RegistreUtils.randomString(30));
		assoClientDto.setNationalId(RegistreUtils.randomString(30));
		assoClientDto.setEspaceOrganisationId(RegistreUtils.randomLong(1));
		assoClientDto.setVersion(RegistreUtils.randomLong(2).intValue());
		assoClientDto.setId(RegistreUtils.randomLong(1));
		assoClientDto.setOrganisationId(RegistreUtils.randomLong(2));
		return assoClientDto;
	}
}
