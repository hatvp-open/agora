/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.referentiel;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.referentiel.SireneReferencielTransformer;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielSireneEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * Test de la classe {@link SireneReferencielTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
    loader = AnnotationConfigContextLoader.class)
public class ReferencielSirenTransformerTest {

    /**
     * Transformer des référenciels RNA.
     */
    @Autowired
    private SireneReferencielTransformer sireneReferencielTransformer;

    /**
     * Test de la métode modelToDto.
     */
    @Test
    public void testModelToDto() {
        final ReferentielSireneEntity referentielSireneEntity = new ReferentielSireneEntity();
        referentielSireneEntity.setSiren(RegistreUtils.randomString(9));
        referentielSireneEntity.setRna(RegistreUtils.randomString(10));
        referentielSireneEntity.setDenomination(RegistreUtils.randomString(23));
        referentielSireneEntity.setCodePostal(RegistreUtils.randomString(5));
        referentielSireneEntity.setVille(RegistreUtils.randomString(12));
        referentielSireneEntity.setAdresse(RegistreUtils.randomString(15));

        final OrganisationDto organisationDto = this.sireneReferencielTransformer.modelToDto(referentielSireneEntity);

        Assert.assertNotNull(organisationDto);
        Assert.assertEquals(referentielSireneEntity.getSiren(), organisationDto.getSiren());
        Assert.assertEquals(referentielSireneEntity.getRna(), organisationDto.getRna());
        Assert.assertEquals(referentielSireneEntity.getDenomination(), organisationDto.getDenomination());
        Assert.assertEquals(referentielSireneEntity.getCodePostal(), organisationDto.getCodePostal());
        Assert.assertEquals(referentielSireneEntity.getVille(), organisationDto.getVille());
        Assert.assertEquals(referentielSireneEntity.getAdresse(), organisationDto.getAdresse());
    }
}
