/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.hatvp.registre.business.transformer.proxy.referentiel.OrganisationTransformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.RnaReferencielTransformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.SireneReferencielTransformer;
import fr.hatvp.registre.commons.dto.AssoClientBatchDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.TypeOrganisationDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;
import fr.hatvp.registre.commons.utils.RegistreRandomUtils;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielRnaEntity;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielSireneEntity;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
import fr.hatvp.registre.persistence.repository.referenciel.ReferencielRnaRepository;
import fr.hatvp.registre.persistence.repository.referenciel.ReferencielSirenRepository;

/**
 * Test du service {@link fr.hatvp.registre.business.OrganisationService}
 *
 * @version $Revision$ $Date${0xD}
 */

public class OrganisationServiceTest {

	/** Repository organisation. */
	@Mock
	OrganisationRepository organisationRepository;

	/** Repository referentiel SIREN/RNA. */
	@Mock
	ReferencielSirenRepository referencielSirenRepository;

	/** Repository referentiel RNA. */
	@Mock
	ReferencielRnaRepository referencielRnaRepository;

	/** Transformer des sirenes. */
	@Mock
	SireneReferencielTransformer sireneReferencielTransformer;

	/** Transformer du référentiel Rna. */
	@Mock
	RnaReferencielTransformer rnaReferencielTransformer;

	/** Transformeur de l'entitée organisation. */
	@Mock
	OrganisationTransformer organisationTransformer;

	/* Utilitaires de générations d'éléments aléatoires */
	@Mock
	RegistreRandomUtils registreRandomUtils;

	/** Injection des mock. */
	@InjectMocks
	OrganisationServiceImpl organisationService;

	/** Id de l'organisation utilisé pour les tests. */
	private final Long ID_ORGANISATION_TEST = 1L;

	/** Id de l'espace organisation utilisé pour les tests. */
	private final Long ID_ESPACE_ORGANISATION_TEST = 1L;

	/** Id national utilisé pour les test. */
	private final String ID_NATIONAL_TEST = "145236987";

	/**
	 * Initialisation des tests.
	 *
	 * @throws Exception
	 */
	@Before
	public void before() throws Exception {
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test de la méthode avec une organisation introuvable.
	 * <p>
	 * Test de la méthode: {@link fr.hatvp.registre.business.OrganisationService#getOrganizationDataById(Long)}
	 */
	@Test
	public void testCheckCorporateSpaceExistenceAndGetOrganizationAvecOrganisationIntrouvable() {
		Mockito.doReturn(Optional.empty()).when(this.organisationRepository).findById(this.ID_ORGANISATION_TEST);	
		
		try {
			this.organisationService.getOrganizationDataById(this.ID_ORGANISATION_TEST);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Test de la méthode avec une organisation existante.
	 * <p>
	 * Test de la méthode: {@link fr.hatvp.registre.business.OrganisationService#getOrganizationDataById(Long)}
	 */
	@Test
	public void testCheckCorporateSpaceExistenceAndGetOrganizationAvecOrganisationExistante() {
		final OrganisationDto organisationDto = new OrganisationDto();
		organisationDto.setId(this.ID_ORGANISATION_TEST);
		organisationDto.setNationalId(this.ID_NATIONAL_TEST);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(organisationDto.getId());

		Mockito.doReturn(Optional.of(organisationEntity)).when(this.organisationRepository).findById(this.ID_ORGANISATION_TEST);
		Mockito.doReturn(organisationDto).when(this.organisationTransformer).modelToDto(organisationEntity);

		final OrganisationDto foundOrganization = this.organisationService.getOrganizationDataById(this.ID_ORGANISATION_TEST);

		Assert.assertNotNull(organisationDto);
		Assert.assertNotNull(organisationEntity);
		Assert.assertNotNull(foundOrganization);
		Assert.assertEquals(organisationEntity.getId(), foundOrganization.getId());
	}

	/**
	 * Test de la méthode de création d'une nouvelle organisation.
	 * <p>
	 * Test de la méthode: {@link fr.hatvp.registre.business.OrganisationService#creerOrganisation(OrganisationDto)}
	 */
	@Test
	public void testCreerOrganisation() {
		final OrganisationDto dto = new OrganisationDto();
		dto.setDenomination(RegistreUtils.randomString(20));
		dto.setSiren(RegistreUtils.randomString(9));
		dto.setRna(RegistreUtils.randomString(10));
		dto.setDunsNumber(RegistreUtils.randomString(10));
		dto.setAdresse(RegistreUtils.randomString(50));
		dto.setCodePostal(RegistreUtils.randomString(10));
		dto.setVille(RegistreUtils.randomString(30));
		dto.setPays(RegistreUtils.randomString(20));
		dto.setSiteWeb(RegistreUtils.randomString(100));

		final OrganisationEntity entity = new OrganisationEntity();
		entity.setDenomination(RegistreUtils.randomString(20));
		entity.setSiren(RegistreUtils.randomString(9));
		entity.setRna(RegistreUtils.randomString(10));
		entity.setDunsNumber(RegistreUtils.randomString(10));
		entity.setAdresse(RegistreUtils.randomString(50));
		entity.setCodePostal(RegistreUtils.randomString(10));
		entity.setVille(RegistreUtils.randomString(30));
		entity.setPays(RegistreUtils.randomString(20));
		entity.setSiteWeb(RegistreUtils.randomString(100));

		Mockito.doReturn(entity).when(this.organisationTransformer).dtoToModel(dto);
		Mockito.doReturn(entity).when(this.organisationRepository).save(entity);
		Mockito.doReturn(new ArrayList<OrganisationEntity>()).when(this.organisationRepository).findBySirenOrRnaOrDunsNumber(dto.getSiren(), dto.getRna(), dto.getDunsNumber());
		Mockito.doReturn(new ArrayList<OrganisationEntity>()).when(this.organisationRepository).findByHatvpNumberIsNotNull();

		Mockito.doReturn(123456789).when(this.registreRandomUtils).generateRandomNumber(9);

		String hatvpNumber = this.organisationService.creerOrganisation(dto);
		Integer digits = Integer.parseInt(hatvpNumber.replace("H", ""));
		Assert.assertTrue(this.organisationService.creerOrganisation(dto).matches("^H+[0-9]{9}$"));
	}

	/**
	 * Test de collision pour la méthode de création d'une nouvelle organisation (identifiant déjà existant).
	 * <p>
	 * Test de la méthode: {@link fr.hatvp.registre.business.OrganisationService#creerOrganisation(OrganisationDto)}
	 */
	@Test(expected = BusinessGlobalException.class)
	public void testCreerOrganisationAvecIdExistant() {
		final OrganisationDto dto = new OrganisationDto();
		dto.setDenomination(RegistreUtils.randomString(20));
		dto.setSiren(RegistreUtils.randomString(9));
		dto.setRna(RegistreUtils.randomString(10));
		dto.setDunsNumber(RegistreUtils.randomString(10));
		dto.setAdresse(RegistreUtils.randomString(50));
		dto.setCodePostal(RegistreUtils.randomString(10));
		dto.setVille(RegistreUtils.randomString(30));
		dto.setPays(RegistreUtils.randomString(20));
		dto.setSiteWeb(RegistreUtils.randomString(100));

		final OrganisationEntity entity = new OrganisationEntity();
		entity.setDenomination(RegistreUtils.randomString(20));
		entity.setSiren(RegistreUtils.randomString(9));
		entity.setRna(RegistreUtils.randomString(10));
		entity.setDunsNumber(RegistreUtils.randomString(10));
		entity.setAdresse(RegistreUtils.randomString(50));
		entity.setCodePostal(RegistreUtils.randomString(10));
		entity.setVille(RegistreUtils.randomString(30));
		entity.setPays(RegistreUtils.randomString(20));
		entity.setSiteWeb(RegistreUtils.randomString(100));

		Mockito.doReturn(entity).when(this.organisationTransformer).dtoToModel(dto);
		Mockito.doReturn(entity).when(this.organisationRepository).save(entity);
		Mockito.doReturn(new ArrayList<OrganisationEntity>()).when(this.organisationRepository).findBySirenOrRnaOrDunsNumber(dto.getSiren(), dto.getRna(), dto.getDunsNumber());
		Mockito.doReturn(new ArrayList<OrganisationEntity>()).when(this.organisationRepository).findByHatvpNumberIsNotNull();

		Mockito.doReturn(123456789).when(this.registreRandomUtils).generateRandomNumber(9);

		Mockito.doReturn(entity).when(this.organisationRepository).findByHatvpNumber(Mockito.anyString());

		this.organisationService.creerOrganisation(dto);
	}

	/**
	 * Test du service de recherche par lot d'identifiants nationaux
	 * <p>
	 * Test de la méthode: {@link fr.hatvp.registre.business.OrganisationService#lookUpByNationalNumberBatch(List)}
	 */
	@Test
	public void testLookupByNationalNumberBatch() {

		List<OrganisationDto> orgs = this.getMockedOrganizationDtosForBatchOperations();

		List<String> nationalIdList = orgs.stream().map(OrganisationDto::getNationalId).collect(Collectors.toList());

		AssoClientBatchDto resultToVerify = this.organisationService.lookUpByNationalNumberBatch(nationalIdList);

		System.out.println("Result ===========> " + resultToVerify.getOrganisationList());
		Assert.assertEquals(4, resultToVerify.getOrganisationList().size());

	}

	@Test
	public void testGetTypesOrganisations() {

		final List<TypeOrganisationDto> organisationDtos = this.organisationService.getListeTypesOrganisations();

		Assert.assertEquals(organisationDtos.size(), TypeOrganisationEnum.values().length);
	}

	/**
	 * Reusable Mock object getter method
	 *
	 * @return A locked list of Organization entities
	 */
	public List<OrganisationDto> getMockedOrganizationDtosForBatchOperations() {
		// ====================== DTOs for Transformer layer ================================
		final OrganisationDto dtoOrg1_1 = new OrganisationDto();
		dtoOrg1_1.setDenomination(RegistreUtils.randomString(20));
		dtoOrg1_1.setSiren("387837032");
		dtoOrg1_1.setNationalId("387837032");
		dtoOrg1_1.setId(0L);
		dtoOrg1_1.setVersion(0);
		dtoOrg1_1.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
		dtoOrg1_1.setAdresse(RegistreUtils.randomString(50));
		dtoOrg1_1.setCodePostal(RegistreUtils.randomString(10));
		dtoOrg1_1.setVille(RegistreUtils.randomString(30));
		dtoOrg1_1.setPays(RegistreUtils.randomString(20));
		dtoOrg1_1.setSiteWeb(RegistreUtils.randomString(100));
		dtoOrg1_1.setOrganisationExist(true);

		final OrganisationDto dtoOrg1_2 = new OrganisationDto();
		dtoOrg1_2.setDenomination(RegistreUtils.randomString(20));
		dtoOrg1_2.setHatvpNumber("H348975087");
		dtoOrg1_2.setNationalId("H348975087");
		dtoOrg1_2.setId(1L);
		dtoOrg1_2.setVersion(0);
		dtoOrg1_2.setOriginNationalId(TypeIdentifiantNationalEnum.HATVP);
		dtoOrg1_2.setAdresse(RegistreUtils.randomString(50));
		dtoOrg1_2.setCodePostal(RegistreUtils.randomString(10));
		dtoOrg1_2.setVille(RegistreUtils.randomString(30));
		dtoOrg1_2.setPays(RegistreUtils.randomString(20));
		dtoOrg1_2.setSiteWeb(RegistreUtils.randomString(100));
		dtoOrg1_2.setOrganisationExist(true);

		final OrganisationDto dtoOrg2 = new OrganisationDto();
		dtoOrg2.setDenomination(RegistreUtils.randomString(20));
		dtoOrg2.setRna("H123456789");
		dtoOrg2.setNationalId("H123456789");
		dtoOrg2.setId(2L);
		dtoOrg2.setVersion(0);
		dtoOrg2.setOriginNationalId(TypeIdentifiantNationalEnum.RNA);
		dtoOrg2.setAdresse(RegistreUtils.randomString(50));
		dtoOrg2.setCodePostal(RegistreUtils.randomString(10));
		dtoOrg2.setVille(RegistreUtils.randomString(30));
		dtoOrg2.setPays(RegistreUtils.randomString(20));
		dtoOrg2.setSiteWeb(RegistreUtils.randomString(100));
		dtoOrg2.setOrganisationExist(false);

		final OrganisationDto dtoOrg3 = new OrganisationDto();
		dtoOrg3.setDenomination(RegistreUtils.randomString(20));
		dtoOrg3.setHatvpNumber("W345678901");
		dtoOrg3.setNationalId("W345678901");
		dtoOrg3.setId(3L);
		dtoOrg3.setVersion(0);
		dtoOrg2.setOriginNationalId(TypeIdentifiantNationalEnum.HATVP);
		dtoOrg3.setAdresse(RegistreUtils.randomString(50));
		dtoOrg3.setCodePostal(RegistreUtils.randomString(10));
		dtoOrg3.setVille(RegistreUtils.randomString(30));
		dtoOrg3.setPays(RegistreUtils.randomString(20));
		dtoOrg3.setSiteWeb(RegistreUtils.randomString(100));
		dtoOrg3.setOrganisationExist(false);

		// ====================== Entities for Repository layer ================================
		final OrganisationEntity entityOrg1_1 = new OrganisationEntity();
		entityOrg1_1.setDenomination(RegistreUtils.randomString(20));
		entityOrg1_1.setSiren("387837032");
		entityOrg1_1.setId(0L);
		entityOrg1_1.setVersion(0);
		entityOrg1_1.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
		entityOrg1_1.setAdresse(RegistreUtils.randomString(50));
		entityOrg1_1.setCodePostal(RegistreUtils.randomString(10));
		entityOrg1_1.setVille(RegistreUtils.randomString(30));
		entityOrg1_1.setPays(RegistreUtils.randomString(20));
		entityOrg1_1.setSiteWeb(RegistreUtils.randomString(100));

		final OrganisationEntity entityOrg1_2 = new OrganisationEntity();
		entityOrg1_2.setDenomination(RegistreUtils.randomString(20));
		entityOrg1_2.setHatvpNumber("H348975087");
		entityOrg1_2.setId(1L);
		entityOrg1_2.setVersion(0);
		entityOrg1_2.setOriginNationalId(TypeIdentifiantNationalEnum.HATVP);
		entityOrg1_2.setAdresse(RegistreUtils.randomString(50));
		entityOrg1_2.setCodePostal(RegistreUtils.randomString(10));
		entityOrg1_2.setVille(RegistreUtils.randomString(30));
		entityOrg1_2.setPays(RegistreUtils.randomString(20));
		entityOrg1_2.setSiteWeb(RegistreUtils.randomString(100));

		final ReferentielSireneEntity entityOrg2 = new ReferentielSireneEntity();
		entityOrg2.setDenomination(RegistreUtils.randomString(20));
		entityOrg2.setSiren("H123456789");
		entityOrg2.setId(2L);
		entityOrg2.setVersion(0);
		entityOrg2.setAdresse(RegistreUtils.randomString(50));
		entityOrg2.setCodePostal(RegistreUtils.randomString(10));
		entityOrg2.setVille(RegistreUtils.randomString(30));
		entityOrg2.setPays(RegistreUtils.randomString(20));

		final ReferentielRnaEntity entityOrg3 = new ReferentielRnaEntity();
		entityOrg3.setDenomination(RegistreUtils.randomString(20));
		entityOrg3.setRna("W345678901");
		entityOrg3.setId(3L);
		entityOrg3.setVersion(0);
		entityOrg3.setAdresse(RegistreUtils.randomString(50));
		entityOrg3.setCodePostal(RegistreUtils.randomString(10));
		entityOrg3.setVille(RegistreUtils.randomString(30));
		entityOrg3.setPays(RegistreUtils.randomString(20));
		entityOrg3.setSiteWeb(RegistreUtils.randomString(100));

		List<String> nationalIdList = Stream.of(dtoOrg1_1.getNationalId(), dtoOrg1_2.getNationalId(), entityOrg2.getSiren(), entityOrg3.getRna()).collect(Collectors.toList());
		List<OrganisationEntity> orgEntityList = Stream.of(entityOrg1_1, entityOrg1_2).collect(Collectors.toList());
		setOrgEntityList(orgEntityList);
		List<OrganisationDto> dtoList = Stream.of(dtoOrg1_1, dtoOrg1_2, dtoOrg2, dtoOrg3).collect(Collectors.toList());
		List<ReferentielSireneEntity> sireneEntityList = Stream.of(entityOrg2).collect(Collectors.toList());
		List<ReferentielRnaEntity> rnaEntityList = Stream.of(entityOrg3).collect(Collectors.toList());

		// ====================== MOCKING ================================

		Mockito.doReturn(entityOrg1_1).when(this.organisationTransformer).dtoToModel(dtoOrg1_1);
		Mockito.doReturn(entityOrg1_2).when(this.organisationTransformer).dtoToModel(dtoOrg1_2);
		Mockito.doReturn(dtoOrg1_1).when(this.organisationTransformer).modelToDto(entityOrg1_1);
		Mockito.doReturn(dtoOrg1_2).when(this.organisationTransformer).modelToDto(entityOrg1_2);
		Mockito.doReturn(entityOrg1_1).when(this.organisationRepository).save(entityOrg1_1);
		Mockito.doReturn(entityOrg1_2).when(this.organisationRepository).save(entityOrg1_2);
		Mockito.doReturn(entityOrg1_1).when(this.organisationRepository).findOne(entityOrg1_1.getId());
		Mockito.doReturn(entityOrg1_2).when(this.organisationRepository).findOne(entityOrg1_2.getId());
		Mockito.doReturn(orgEntityList).when(this.organisationRepository).findBySirenOrRnaOrHatvpNumberBatch(nationalIdList, nationalIdList, nationalIdList);

		Mockito.doReturn(entityOrg2).when(this.sireneReferencielTransformer).dtoToModel(dtoOrg2);
		Mockito.doReturn(dtoOrg2).when(this.sireneReferencielTransformer).modelToDto(entityOrg2);
		Mockito.doReturn(entityOrg2).when(this.referencielSirenRepository).save(entityOrg2);
		Mockito.doReturn(sireneEntityList).when(this.referencielSirenRepository).findBySirenOrRnaBatch(Mockito.anyListOf(String.class), Mockito.anyListOf(String.class));

		Mockito.doReturn(entityOrg3).when(this.rnaReferencielTransformer).dtoToModel(dtoOrg3);
		Mockito.doReturn(dtoOrg3).when(this.rnaReferencielTransformer).modelToDto(entityOrg3);
		Mockito.doReturn(entityOrg3).when(this.referencielRnaRepository).save(entityOrg3);
		Mockito.doReturn(rnaEntityList).when(this.referencielRnaRepository).findByRnaBatch(Mockito.anyListOf(String.class));

		Mockito.doReturn(123456789).when(this.registreRandomUtils).generateRandomNumber(9);

		return dtoList;
	}

	public List<OrganisationEntity> getOrgEntityList() {
		return orgEntityList;
	}

	public void setOrgEntityList(List<OrganisationEntity> orgEntityList) {
		this.orgEntityList = orgEntityList;
	}

	private List<OrganisationEntity> orgEntityList;

}
