/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.InscriptionEspaceTransformer;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.lists.CategoryTelephoneMail;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantContactEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.DirigeantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;

/**
 * Test de la classe {@link InscriptionEspaceTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestBusinessConfig.class }, loader = AnnotationConfigContextLoader.class)
public class InscriptionEspaceTransformerTest {

	/** Transformer à tester. */
	@Autowired
	private InscriptionEspaceTransformer inscriptionEspaceTransformer;

	/**
	 * Test de la métode modelToDto.
	 */
	@Test
	public void testModelToDto() {
		/*
		 * Création d'une entité
		 */
		final InscriptionEspaceEntity model = new InscriptionEspaceEntity();
		model.setId(RegistreUtils.randomLong(1));
		model.setVersion(1);
		model.setDateDemande(new Date());
		model.setDateSuppression(new Date());
		model.setDateValidation(new Date());
		model.setDeclarant(this.createDeclarant());
		model.setEspaceOrganisation(this.getEspaceOrganisation());
		model.setActif(true);
		model.setFavori(true);
		model.setVerrouille(false);
		model.setRolesFront(this.createRoleEnumSet());

		/*
		 * Transformation
		 */
		final InscriptionEspaceDto dto = this.inscriptionEspaceTransformer.modelToDto(model);

		/*
		 * Assertions
		 */
		Assert.assertNotNull(dto);
		Assert.assertEquals(model.getDeclarant().getEmail(), dto.getDeclarantEmail());
		Assert.assertEquals(model.getEspaceOrganisation().getId(), dto.getEspaceOrganisationId());
		Assert.assertEquals(model.getEspaceOrganisation().getOrganisation().getId(), dto.getOrganizationId());
		Assert.assertEquals(model.getEspaceOrganisation().getOrganisation().getDenomination(), dto.getOrganisationDenomination());
		Assert.assertEquals(model.getDeclarant().getCivility().getLibelleCourt() + " " + model.getDeclarant().getPrenom() + " " + model.getDeclarant().getNom(), dto.getDeclarantNomComplet());
	}

	/**
	 * Création d'un nouvel Espace Organisation.
	 * 
	 * @return entité espace organisation
	 */
	private EspaceOrganisationEntity getEspaceOrganisation() {
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();

		espaceOrganisationEntity.setId(RegistreUtils.randomLong(1));
		espaceOrganisationEntity.setVersion(1);
		espaceOrganisationEntity.setCreationDate(new Date());
		espaceOrganisationEntity.setNonDeclarationTiers(true);
		espaceOrganisationEntity.setOrganisation(this.createOrganisation());

		return espaceOrganisationEntity;
	}

	/**
	 * Création d'une nouvelle Organisation.
	 * 
	 * @return entité Organisation
	 */
	private OrganisationEntity createOrganisation() {
		final List<DirigeantEntity> dirigeants = new ArrayList<>();
		final DirigeantEntity dirigeant = new DirigeantEntity();
		dirigeant.setPrenom(RegistreUtils.randomString(10));
		dirigeant.setNom(RegistreUtils.randomString(10));
		dirigeants.add(dirigeant);

		final OrganisationEntity organisation = new OrganisationEntity();
		organisation.setId(RegistreUtils.randomLong(1));
		organisation.setVersion(1);
		organisation.setAdresse(RegistreUtils.randomString(20));
		organisation.setCreationDate(new Date());
		organisation.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
		organisation.setSiren(RegistreUtils.randomString(10));
		organisation.setDenomination(RegistreUtils.randomString(10));
		return organisation;
	}

	/**
	 * Création d'un Déclarant.
	 * 
	 * @return entité déclarant
	 */
	private DeclarantEntity createDeclarant() {
		final DeclarantEntity model = new DeclarantEntity();
		model.setId(RegistreUtils.randomLong(1));
		model.setVersion(1);
		model.setPrenom(RegistreUtils.randomString(10));
		model.setNom(RegistreUtils.randomString(10));
		model.setBirthDate(new Date());
		model.setEmail(RegistreUtils.randomString(10));
		model.setCivility(CiviliteEnum.M);
		model.setPassword(RegistreUtils.randomString(33));
		model.setActivated(true);
		model.setRecoverConfirmationCode(RegistreUtils.randomString(40));
		model.setContacts(new HashSet<DeclarantContactEntity>());
		model.getContacts().add(this.createDeclarantContact(true));
		model.getContacts().add(this.createDeclarantContact(true));
		model.getContacts().add(this.createDeclarantContact(true));
		model.getContacts().add(this.createDeclarantContact(false));
		model.getContacts().add(this.createDeclarantContact(false));

		return model;
	}

	/**
	 * Création d'un EnumSet de Rôles pour le déclarant.
	 * 
	 * @return Collection de RoleEnums
	 */
	private Set<RoleEnumFrontOffice> createRoleEnumSet() {
		final Set<RoleEnumFrontOffice> roleSet = new TreeSet<>();

		roleSet.add(RoleEnumFrontOffice.CONTRIBUTEUR);

		return roleSet;
	}

	/**
	 * Création d'un contact de déclarant avec un email ou un numéro de téléphone.
	 * 
	 * @param isMail
	 *            marqueur contact mail
	 * @return si le marqueur est à true, on crée un contact avec un email, sinon on
	 *         en crée un avec un téléphone
	 */
	private DeclarantContactEntity createDeclarantContact(final boolean isMail) {
		final DeclarantContactEntity model = new DeclarantContactEntity();
		model.setCategory(CategoryTelephoneMail.PERSO);
		model.setDeclarant(new DeclarantEntity());
		if (isMail) {
			model.setEmail(RegistreUtils.randomString(15));
		} else {
			model.setTelephone(RegistreUtils.randomString(10));
		}
		model.setId(RegistreUtils.randomLong(10));
		return model;
	}

}
