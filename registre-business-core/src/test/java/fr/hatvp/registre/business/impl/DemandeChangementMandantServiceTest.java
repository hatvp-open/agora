/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static fr.hatvp.registre.commons.utils.RegistreUtils.randomLong;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import fr.hatvp.registre.business.email.GestionMandatBoMailer;
import fr.hatvp.registre.business.transformer.impl.DeclarantTransformerImpl;
import fr.hatvp.registre.business.transformer.impl.DemandeChangementMandantTransformerImpl;
import fr.hatvp.registre.business.transformer.impl.EspaceOrganisationPieceTransformerImpl;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.DemandeChangementMandantDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ChangementContactEnum;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.commons.lists.backoffice.MotifComplementEspaceEnum;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.DemandeChangementMandantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.piece.EspaceOrganisationPieceEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.DemandeChangementMandantRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationPiecesRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

/**
 * DemandeChangementMandantServiceImpl Tester.
 *
 */
public class DemandeChangementMandantServiceTest {

	@Mock
	private DemandeChangementMandantRepository demandeChangementMandantRepository;

	@Mock
	private EspaceOrganisationRepository espaceOrganisationRepository;

	@Mock
	private EspaceOrganisationPiecesRepository espaceOrganisationPiecesRepository;

	@Mock
	private DeclarantRepository declarantRepository;

	@Spy
	@InjectMocks
	private DemandeChangementMandantTransformerImpl demandeChangementMandantTransformer = new DemandeChangementMandantTransformerImpl();

	@Spy
	private EspaceOrganisationPieceTransformerImpl espaceOrganisationPieceTransformer = new EspaceOrganisationPieceTransformerImpl();

	@Mock
	private DeclarantTransformerImpl declarantTransformer;

	@Mock
	private GestionMandatBoMailer gestionMandatBoMailer;
	@InjectMocks
	private DemandeChangementMandantServiceImpl demandeChangementMandantService;

	/** Identifiant. */
	private final Long DEMANDE_ID = 1L;

	@Before
	public void setup() throws Exception {
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test de la méthode getDemandeChangementMandant {@link DemandeChangementMandantServiceImpl#getDemandeChangementMandant(Long)}.
	 */
	@Test
	public void testLoadDemandeById() {

		final DemandeChangementMandantDto demandeDto = this.getDemande();
		final DemandeChangementMandantEntity demandeEntity = this.getDeclarant(demandeDto);

		Mockito.doReturn(demandeEntity).when(this.demandeChangementMandantRepository).findOne(DEMANDE_ID);

		Mockito.doReturn(demandeDto).when(this.demandeChangementMandantTransformer).modelToDto(demandeEntity);

		final DemandeChangementMandantDto demandeDtoSaved = this.demandeChangementMandantService.getDemandeChangementMandant(DEMANDE_ID);

		Assert.assertNotNull(demandeDto);
		Assert.assertEquals(demandeDto.getTypeDemande(), demandeDtoSaved.getTypeDemande());
	}

	/**
	 * Test de la méthode getAllDemandesChangementByEspaceOrganisation {@link DemandeChangementMandantServiceImpl#getAllDemandesChangementByEspaceOrganisation()}.
	 */
	@Test
	public void testGetAllDemandesChangementByEspaceOrganisation() {
		List<DemandeChangementMandantDto> changementMandantDtos = demandeChangementMandantService.getAllDemandesChangementByEspaceOrganisation(Mockito.anyLong());

		verify(demandeChangementMandantRepository, times(1)).findAllByEspaceOrganisationIdOrderByDateDemandeDesc(Mockito.anyLong());

		assertTrue(changementMandantDtos.isEmpty());
	}

	/**
	 * Test de la méthode rejeterDemande {@link DemandeChangementMandantServiceImpl#rejeterDemande()}.
	 */
	@Test
	public void testRejeterDemande() {

		final DemandeChangementMandantDto demandeDto = this.getDemande();

		final DemandeChangementMandantEntity demandeEntity = this.getDeclarant(demandeDto);
		DeclarantEntity declarantEntity = new DeclarantEntity(1L);
		declarantEntity.setNom("Bla");
		declarantEntity.setPrenom("Blo");
		declarantEntity.setEmail("test@hatvp.fr");

		// init un DTO declarant
		final DeclarantDto declarantDto = new DeclarantDto();
		declarantDto.setId(1L);
		declarantDto.setEmail("test@hatvp.fr");

		demandeEntity.setDeclarant(declarantEntity);
		when(demandeChangementMandantRepository.findOne(Mockito.anyLong())).thenReturn(demandeEntity);
		// Mock de la transformation du model en DTO
		Mockito.doReturn(declarantDto).when(this.declarantTransformer).modelToDto(declarantEntity);
		demandeChangementMandantService.rejeterDemande(Mockito.anyLong());

		verify(demandeChangementMandantRepository, times(1)).findOne(Mockito.anyLong());
		verify(demandeChangementMandantRepository, times(1)).save(Mockito.any(DemandeChangementMandantEntity.class));
	}

	/**
	 * Test de la méthode validerDemande avec ajout d'un contact opérationnel {@link DemandeChangementMandantServiceImpl#validerDemande()}.
	 */
	@Test
	public void testValiderDemandeAjoutCO() {

		DemandeChangementMandantEntity demandeChangementMandantEntity = new DemandeChangementMandantEntity(DEMANDE_ID);
		demandeChangementMandantEntity.setTypeDemande(ChangementContactEnum.AJOUT_OPERATIONNEL);

		InscriptionEspaceEntity inscriptionEspaceEntity = new InscriptionEspaceEntity(DEMANDE_ID);
		inscriptionEspaceEntity.setRolesFront(new HashSet<>(Arrays.asList(RoleEnumFrontOffice.CONTRIBUTEUR)));

		demandeChangementMandantEntity.setDeclarantPromu(inscriptionEspaceEntity);

		when(demandeChangementMandantRepository.findOne(Mockito.anyLong())).thenReturn(demandeChangementMandantEntity);

		demandeChangementMandantService.validerDemande(Mockito.anyLong());

		verify(demandeChangementMandantRepository, times(1)).findOne(Mockito.anyLong());
		verify(demandeChangementMandantRepository, times(1)).save(Mockito.any(DemandeChangementMandantEntity.class));

		assertTrue(demandeChangementMandantEntity.getDeclarantPromu().getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR));
	}

	/**
	 * Test de la méthode validerDemande avec changement de contact opérationnel {@link DemandeChangementMandantServiceImpl#validerDemande()}.
	 */
	@Test
	public void testValiderDemandeChangementCO() {

		DemandeChangementMandantEntity demandeChangementMandantEntity = new DemandeChangementMandantEntity(DEMANDE_ID);
		demandeChangementMandantEntity.setTypeDemande(ChangementContactEnum.REMPLACEMENT_OPERATIONNEL);

		InscriptionEspaceEntity inscriptionEspaceEntityPromu = new InscriptionEspaceEntity(DEMANDE_ID);
		inscriptionEspaceEntityPromu.setRolesFront(new HashSet<>());
		inscriptionEspaceEntityPromu.getRolesFront().add(RoleEnumFrontOffice.CONTRIBUTEUR);

		demandeChangementMandantEntity.setDeclarantPromu(inscriptionEspaceEntityPromu);

		InscriptionEspaceEntity inscriptionEspaceEntityDechu = new InscriptionEspaceEntity(DEMANDE_ID);
		inscriptionEspaceEntityDechu.setRolesFront(new HashSet<>());
		inscriptionEspaceEntityDechu.getRolesFront().add(RoleEnumFrontOffice.CONTRIBUTEUR);
		inscriptionEspaceEntityDechu.getRolesFront().add(RoleEnumFrontOffice.ADMINISTRATEUR);

		demandeChangementMandantEntity.setDeclarantDechu(inscriptionEspaceEntityDechu);

		when(demandeChangementMandantRepository.findOne(Mockito.anyLong())).thenReturn(demandeChangementMandantEntity);

		demandeChangementMandantService.validerDemande(Mockito.anyLong());

		verify(demandeChangementMandantRepository, times(1)).findOne(Mockito.anyLong());
		verify(demandeChangementMandantRepository, times(1)).save(Mockito.any(DemandeChangementMandantEntity.class));

		assertTrue(demandeChangementMandantEntity.getDeclarantPromu().getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR));
		assertFalse(demandeChangementMandantEntity.getDeclarantDechu().getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR));
	}

	/**
	 *
	 */
	@Test
	public void testSaveDemandeChangementMandant() {

		EspaceOrganisationEntity espace = new EspaceOrganisationEntity(DEMANDE_ID);
		espace.setOrganisation(new OrganisationEntity());

		when(espaceOrganisationRepository.findOne(Mockito.anyLong())).thenReturn(espace);
		when(declarantRepository.findOne(Mockito.anyLong())).thenReturn(new DeclarantEntity());

		DeclarantDto declarantDto = new DeclarantDto();
		declarantDto.setId(Mockito.anyLong());

		DemandeChangementMandantDto demandeChangementMandantDto = getDemande();
		demandeChangementMandantDto.setDeclarantDemande(declarantDto);

		demandeChangementMandantService.saveDemandeChangementMandant(demandeChangementMandantDto, new EspaceOrganisationPieceDto(), new EspaceOrganisationPieceDto());

		verify(demandeChangementMandantRepository, times(1)).save(Mockito.any(DemandeChangementMandantEntity.class));
		verify(espaceOrganisationPiecesRepository, times(2)).save(Mockito.any(EspaceOrganisationPieceEntity.class));
		verify(espaceOrganisationPieceTransformer, times(4)).dtoToModel(Mockito.any(EspaceOrganisationPieceDto.class));
		verify(declarantRepository, times(1)).findOne(Mockito.anyLong());
		verify(espaceOrganisationRepository, times(1)).findOne(Mockito.anyLong());

	}

	/**
	 * @return Renvoi un déclarant DTO.
	 */
	private DemandeChangementMandantDto getDemande() {
		final DemandeChangementMandantDto demandeToSave = new DemandeChangementMandantDto();
		demandeToSave.setId(DEMANDE_ID);
		EspaceOrganisationPieceDto pieceIdentite = new EspaceOrganisationPieceDto();
		pieceIdentite.setId(DEMANDE_ID);
		EspaceOrganisationPieceDto mandat = new EspaceOrganisationPieceDto();
		pieceIdentite.setId(2L);
		demandeToSave.setIdentitePieceRepresentantLegal(pieceIdentite);
		demandeToSave.setMandatPieceRepresentantLegal(mandat);
		demandeToSave.setDateDemande(new Date(randomLong(9)));
		demandeToSave.setDateValidation(new Date(randomLong(9)));
		demandeToSave.setDateRejet(new Date(randomLong(9)));
		demandeToSave.setRepresentantLegal(false);
		demandeToSave.setTypeDemande(ChangementContactEnum.REMPLACEMENT_OPERATIONNEL);

		return demandeToSave;
	}

	/**
	 * @param demandeDto
	 *            dto à transformer.
	 * @return Renvoi une entitée demande avec les informations du DTO.
	 */
		private DemandeChangementMandantEntity getDeclarant(final DemandeChangementMandantDto demandeDto) {

		final DemandeChangementMandantEntity demande = new DemandeChangementMandantEntity();
		demande.setId(demandeDto.getId());
		demande.setTypeDemande(demandeDto.getTypeDemande());
		demande.setEspaceOrganisation(new EspaceOrganisationEntity(demandeDto.getId()));
		demande.setDateDemande(demandeDto.getDateDemande());
		demande.setDateRejet(demandeDto.getDateRejet());
		demande.setDateValidation(demandeDto.getDateValidation());
		demande.setDeclarantDechu(new InscriptionEspaceEntity(demandeDto.getDeclarantDechuInscriptionEspaceId()));
		demande.setDeclarantPromu(new InscriptionEspaceEntity(demandeDto.getDeclarantPromuInscriptionEspaceId()));
		demande.setRepresentantLegal(demandeDto.getRepresentantLegal());

		return demande;
	}

	/**
	 * Test de la méthode {@link DemandeChangementMandantServiceImpl#generateDemandeComplementMessage(final Long id, final MotifComplementEspaceEnum motif)}
	 */
	@Test
	public void testGenerateDemandeComplementMessageException() {
		Long id = 1L;
		Mockito.doReturn(null).when(this.demandeChangementMandantRepository).findOne(id);
		
		try {
			demandeChangementMandantService.generateDemandeComplementMessage(id, MotifComplementEspaceEnum.AUTRE);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.MSG_404.getMessage()).isEqualTo(ex.getMessage());
		}		
	}
	
	/**
	 * Test de la méthode {@link DemandeChangementMandantServiceImpl#generateDemandeComplementMessage(final Long id, final MotifComplementEspaceEnum motif)}
	 * @throws ParseException 
	 */
	@Test
	public void testGenerateDemandeComplementMessageCasAjoutOperationel() throws ParseException {
		OrganisationEntity organisationEntity = new OrganisationEntity(3L);
		organisationEntity.setDenomination("Dénomination test");
		
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity(2L);
		espaceOrganisationEntity.setOrganisation(organisationEntity);
		
		DemandeChangementMandantEntity demandeChangementMandantEntity = new DemandeChangementMandantEntity();
		demandeChangementMandantEntity.setId(1L);
		demandeChangementMandantEntity.setTypeDemande(ChangementContactEnum.AJOUT_OPERATIONNEL);
		demandeChangementMandantEntity.setEspaceOrganisation(espaceOrganisationEntity);	
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dateInString = "15/03/2020";
		Date dateDemande = sdf.parse(dateInString);
		demandeChangementMandantEntity.setDateDemande(dateDemande);		
	
		
		Mockito.doReturn(demandeChangementMandantEntity).when(this.demandeChangementMandantRepository).findOne(demandeChangementMandantEntity.getId());
		//on force ajoutCOMessageMandat
        org.springframework.test.util.ReflectionTestUtils.setField(demandeChangementMandantService, "ajoutCOMessageMandat", "test [DENOMINATION] message [MOTIF] [DATE]");
		

		String retourMessage = demandeChangementMandantService.generateDemandeComplementMessage(demandeChangementMandantEntity.getId(), MotifComplementEspaceEnum.AUTRE);
	
		Assert.assertNotSame("test [DENOMINATION] message [MOTIF] [DATE]", retourMessage);
		assert(retourMessage.contains("Dénomination test"));
		assert(retourMessage.contains("15/03/2020"));
	}
	
	/**
	 * Test de la méthode {@link DemandeChangementMandantServiceImpl#generateDemandeComplementMessage(final Long id, final MotifComplementEspaceEnum motif)}
	 * @throws ParseException 
	 */
	@Test
	public void testGenerateDemandeComplementMessageCasChangementRepresentantMotifAutre() throws ParseException {
		OrganisationEntity organisationEntity = new OrganisationEntity(3L);
		organisationEntity.setDenomination("Dénomination test");
		
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity(2L);
		espaceOrganisationEntity.setOrganisation(organisationEntity);
		
		DemandeChangementMandantEntity demandeChangementMandantEntity = new DemandeChangementMandantEntity();
		demandeChangementMandantEntity.setId(1L);
		demandeChangementMandantEntity.setTypeDemande(ChangementContactEnum.CHANGEMENT_REPRESENTANT);
		demandeChangementMandantEntity.setEspaceOrganisation(espaceOrganisationEntity);	
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dateInString = "15/03/2020";
		Date dateDemande = sdf.parse(dateInString);
		demandeChangementMandantEntity.setDateDemande(dateDemande);	
		
		Mockito.doReturn(demandeChangementMandantEntity).when(this.demandeChangementMandantRepository).findOne(demandeChangementMandantEntity.getId());
		
		String retourMessage = demandeChangementMandantService.generateDemandeComplementMessage(demandeChangementMandantEntity.getId(), MotifComplementEspaceEnum.AUTRE);
	
		Assert.assertNull(retourMessage);
	}
	
	/**
	 * Test de la méthode {@link DemandeChangementMandantServiceImpl#generateDemandeComplementMessage(final Long id, final MotifComplementEspaceEnum motif)}
	 * @throws ParseException 
	 */
	@Test
	public void testGenerateDemandeComplementMessageCasChangementRepresentantMotifIdNC() throws ParseException {
		OrganisationEntity organisationEntity = new OrganisationEntity(3L);
		organisationEntity.setDenomination("Dénomination test");
		
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity(2L);
		espaceOrganisationEntity.setOrganisation(organisationEntity);
		
		DemandeChangementMandantEntity demandeChangementMandantEntity = new DemandeChangementMandantEntity();
		demandeChangementMandantEntity.setId(1L);
		demandeChangementMandantEntity.setTypeDemande(ChangementContactEnum.CHANGEMENT_REPRESENTANT);
		demandeChangementMandantEntity.setEspaceOrganisation(espaceOrganisationEntity);	
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dateInString = "15/03/2020";
		Date dateDemande = sdf.parse(dateInString);
		demandeChangementMandantEntity.setDateDemande(dateDemande);		
	
		
		Mockito.doReturn(demandeChangementMandantEntity).when(this.demandeChangementMandantRepository).findOne(demandeChangementMandantEntity.getId());
		//on force ajoutCOMessageMandat
        org.springframework.test.util.ReflectionTestUtils.setField(demandeChangementMandantService, "changementRepresentantMessageIdentite", "test [DENOMINATION] message [MOTIF] [DATE]");
		
        String retourMessage = demandeChangementMandantService.generateDemandeComplementMessage(demandeChangementMandantEntity.getId(), MotifComplementEspaceEnum.ID_NON_CONFORME);
	
		Assert.assertNotSame("test [DENOMINATION] message [MOTIF] [DATE]", retourMessage);
		assert(retourMessage.contains("Dénomination test"));
		assert(retourMessage.contains("15/03/2020"));
	}
}
