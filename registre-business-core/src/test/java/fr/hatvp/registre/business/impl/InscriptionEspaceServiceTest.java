/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import fr.hatvp.registre.business.InscriptionEspaceService;
import fr.hatvp.registre.business.email.DesinscriptionMailer;
import fr.hatvp.registre.business.email.OrganisationEspaceMailer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.DesinscriptionTransformer;
import fr.hatvp.registre.business.transformer.proxy.InscriptionEspaceTransformer;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.DesinscriptionDto;
import fr.hatvp.registre.commons.dto.DesinscriptionPieceDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.CollaborateurEntity;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.CollaborateurRepository;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionPieceRepository;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
/***
 *
 * Test pour le service {@link InscriptionEspaceService}
 *
 * @version $Revision$ $Date${0xD}
 */

public class InscriptionEspaceServiceTest {
	
	private final static Long ID_ESPACE_ORGANISATION = 1L;
    private final static Long ID_DECLARANT = 2L;

	/** Repository Inscription Espace */
	@Mock
	InscriptionEspaceRepository inscriptionEspaceRepository;

	/** Repository des collaborateurs d'un espace. */
	@Mock
	private CollaborateurRepository collaborateurRepository;

	/** Repository Espace Organisation */
	@Mock
	EspaceOrganisationRepository espaceOrganisationRepository;

	/** Transformer. */
	@Mock
	InscriptionEspaceTransformer inscriptionEspaceTransformer;

	/** Repository Declarant */
	@Mock
	DeclarantRepository declarantRepository;

	/** Repository Organisation */
	@Mock
	OrganisationRepository organisationRepository;

	/** Service Mailer Organisation */
	@Mock
	OrganisationEspaceMailer organisationEspaceMailer;

	/** Transformer DeclarantDto */
	@Mock
	DeclarantTransformer declarantTransformer;

	/** Service Inscription Espace */
	@InjectMocks
	InscriptionEspaceServiceImpl inscriptionEspaceService;
	
	@Mock
	DesinscriptionRepository desinscriptionRepository;
	
	@Mock
	DesinscriptionMailer desinscriptionMailer;
	
	@Mock
	DesinscriptionPieceRepository desinscriptionPieceRepository;
	
	@Mock
	DesinscriptionTransformer desinscriptionTransformer;
	/**
	 * Initialisation des tests.
	 *
	 * @throws Exception
	 */
	@Before
	public void before() throws Exception {
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method: getAllByDeclarantId(Long declarantId) Avec résultat
	 */
	@Test
	public void testGetAllByDeclarantId() throws Exception {
		final List<InscriptionEspaceEntity> inscriptions = new ArrayList<>();

		final InscriptionEspaceEntity model = new InscriptionEspaceEntity();
		model.setFavori(false);

		final DeclarantEntity declarant = new DeclarantEntity();
		declarant.setId(1L);
		model.setDeclarant(declarant);

		inscriptions.add(model);
		Mockito.doReturn(inscriptions).when(this.inscriptionEspaceRepository).findAllByDateSuppressionIsNullAndDeclarantId(1L);

		final List<InscriptionEspaceDto> res = this.inscriptionEspaceService.getAllByDeclarantId(1L);

		Assert.assertNotNull(res);
		Assert.assertEquals(1, res.size());
	}

	/**
	 * Method: getAllByDeclarantId(Long declarantId) Avec aucun résultat Pour lignes inscriptions supprimées logiquement
	 */
	@Test
	public void testGetAllByDeclarantIdVide() throws Exception {
		Mockito.doReturn(new ArrayList<InscriptionEspaceEntity>()).when(this.inscriptionEspaceRepository).findAllByDateSuppressionIsNullAndDeclarantId(1L);

		final List<InscriptionEspaceDto> res = this.inscriptionEspaceService.getAllByDeclarantId(1L);

		Assert.assertNotNull(res);
		Assert.assertEquals(0, res.size());
	}

	/**
	 * Method: getAllPendingRequestsByEspaceOrganisationId(Long espaceOrganisationId) Avec résultat
	 */
	@Test
	public void testGetAllPendingRequestsByEspaceOrganisationId() throws Exception {
		final List<InscriptionEspaceEntity> inscriptions = new ArrayList<>();

		final InscriptionEspaceEntity model = new InscriptionEspaceEntity();
		model.setFavori(false);

		final DeclarantEntity declarant = new DeclarantEntity();
		declarant.setId(1L);
		model.setDeclarant(declarant);

		inscriptions.add(model);
		Mockito.doReturn(inscriptions).when(this.inscriptionEspaceRepository).findAllByDateSuppressionIsNullAndDateValidationIsNullAndEspaceOrganisationId(1L);

		final List<InscriptionEspaceDto> res = this.inscriptionEspaceService.getAllPendingRequestsByEspaceOrganisationId(1L);

		Assert.assertNotNull(res);
		Assert.assertEquals(1, res.size());
	}

	/**
	 * Method: getAllPendingRequestsByEspaceOrganisationId(Long espaceOrganisationId) Avec aucun résultat Pour lignes inscriptions supprimées logiquement
	 */
	@Test
	public void testGetAllPendingRequestsByEspaceOrganisationIdVide() throws Exception {
		Mockito.doReturn(new ArrayList<InscriptionEspaceEntity>()).when(this.inscriptionEspaceRepository).findAllByDateSuppressionIsNullAndDateValidationIsNullAndEspaceOrganisationId(1L);

		final List<InscriptionEspaceDto> res = this.inscriptionEspaceService.getAllPendingRequestsByEspaceOrganisationId(1L);

		Assert.assertNotNull(res);
		Assert.assertEquals(0, res.size());
	}

	/**
	 * Method: getAllResgitrationsByEspaceOrganisationId(Long espaceOrganisationId) Avec résultat
	 */
	@Test
	public void testGetAllResgitrationsByEspaceOrganisationId() throws Exception {
		final List<InscriptionEspaceEntity> inscriptions = new ArrayList<>();

		final InscriptionEspaceEntity model = new InscriptionEspaceEntity();
		model.setFavori(false);

		final DeclarantEntity declarant = new DeclarantEntity();
		declarant.setId(1L);
		model.setDeclarant(declarant);

		inscriptions.add(model);
		Mockito.doReturn(inscriptions).when(this.inscriptionEspaceRepository).findAllByActifTrueAndDateSuppressionIsNullAndEspaceOrganisationId(1L);

		final List<InscriptionEspaceDto> res = this.inscriptionEspaceService.getAllResgitrationsByEspaceOrganisationId(1L);

		Assert.assertNotNull(res);
		Assert.assertEquals(1, res.size());
	}

	/**
	 * Method: getAllResgitrationsByEspaceOrganisationId(Long espaceOrganisationId) Avec aucun résultat Pour lignes inscriptions supprimées logiquement
	 */
	@Test
	public void testGetAllResgitrationsByEspaceOrganisationIdVide() throws Exception {
		Mockito.doReturn(new ArrayList<InscriptionEspaceEntity>()).when(this.inscriptionEspaceRepository).findAllByActifTrueAndDateSuppressionIsNullAndEspaceOrganisationId(1L);

		final List<InscriptionEspaceDto> res = this.inscriptionEspaceService.getAllResgitrationsByEspaceOrganisationId(1L);

		Assert.assertNotNull(res);
		Assert.assertEquals(0, res.size());
	}

	/**
	 * Method: setFavoriteByDeclarantId(Long id, Long declarantId) Validation du changement de favori d'un déclarant.
	 */
	@Test
	public void testSetFavoriteByDeclarantIdOK() throws Exception {
		final Long id = 1L, userId = 1L;

		final InscriptionEspaceEntity model = new InscriptionEspaceEntity();
		model.setFavori(false);

		final DeclarantEntity declarant = new DeclarantEntity();
		declarant.setId(userId);
		model.setDeclarant(declarant);

		Mockito.doReturn(model).when(this.inscriptionEspaceRepository).findOne(id);
		Mockito.doReturn(Optional.of(model)).when(this.inscriptionEspaceRepository).findByDateSuppressionIsNullAndFavoriTrueAndDeclarantId(userId);

		this.inscriptionEspaceService.setFavoriteByDeclarantId(id, userId);

		final InscriptionEspaceEntity result = this.inscriptionEspaceRepository.findOne(id);

		Assert.assertNotNull(model);
		Assert.assertTrue(result.isFavori());
	}

	/**
	 * Method: setFavoriteByDeclarantId(Long id, Long declarantId) Changement de favori d'un déclarant : cas ou le favori n'appartient pas au déclarant.
	 */
	@Test
	public void testSetFavoriteByDeclarantIdWrongUser() throws Exception {
		final Long id = 1L, userId = 1L, wrongUserId = 2L;

		final InscriptionEspaceEntity model = new InscriptionEspaceEntity();
		model.setFavori(false);

		final DeclarantEntity declarant = new DeclarantEntity();
		declarant.setId(userId);
		model.setDeclarant(declarant);

		Mockito.doReturn(model).when(this.inscriptionEspaceRepository).findOne(id);
		Mockito.doReturn(Optional.of(model)).when(this.inscriptionEspaceRepository).findByDateSuppressionIsNullAndFavoriTrueAndDeclarantId(userId);

		try {
			this.inscriptionEspaceService.setFavoriteByDeclarantId(id, wrongUserId);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.MSG_403.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	
	/**
	 * Method: setFavoriteByDeclarantId(Long id, Long declarantId) Changement de favori d'un déclarant : cas ou le favori n'appartient pas au déclarant.
	 */
	@Test
	public void testSetFavoriteByDeclarantIdNoInscriptionEspace() throws Exception {
		final Long id = 1L, wrongUserId = 2L;

		Mockito.doReturn(null).when(this.inscriptionEspaceRepository).findOne(id);
		
		try {
			this.inscriptionEspaceService.setFavoriteByDeclarantId(id, wrongUserId);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.MSG_404.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Inscription à un espace corporate sur lequel j'ai une demande en attente
	 */
	@Test
	public void testJoinCreateRegistrationInscriptionExist() {

		final Long organisationId = 1L;
		final Long declarantId = 1L;

		final InscriptionEspaceDto inscriptionEspaceDto = new InscriptionEspaceDto();
		inscriptionEspaceDto.setOrganizationId(organisationId);

		Mockito.doReturn(new InscriptionEspaceEntity()).when(this.inscriptionEspaceRepository).findByDateSuppressionIsNullAndDeclarantIdAndEspaceOrganisationOrganisationId(declarantId,
				organisationId);		
		try {
			this.inscriptionEspaceService.inscriptionEspaceExistant(declarantId, inscriptionEspaceDto);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.DEMANDE_EN_ATTENTE_DE_VALIDATION.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Method: setPendingRequestValidity Test de la Validation de l'inscription d'un déclarant (inscription acceptée).
	 */
	@Test
	public void testSetPendingRequestValidityAccepte() throws Exception {
		final Long inscriptionId = 1L, currentEspaceOrganisationId = 1L, userId = 1L;

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(currentEspaceOrganisationId);
		espaceOrganisationEntity.setOrganisation(new OrganisationEntity());

		final DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(userId);

		final InscriptionEspaceEntity model = new InscriptionEspaceEntity();
		model.setId(inscriptionId);
		model.setActif(false);
		model.setDateValidation(null);
		model.setRolesFront(new HashSet<RoleEnumFrontOffice>());
		model.setDeclarant(declarantEntity);
		model.setEspaceOrganisation(espaceOrganisationEntity);

		Mockito.doReturn(model).when(this.inscriptionEspaceRepository).findByDateSuppressionIsNullAndIdAndEspaceOrganisationId(inscriptionId, currentEspaceOrganisationId);
		Mockito.doReturn(model).when(this.inscriptionEspaceRepository).findOne(inscriptionId);
		Mockito.doReturn(new ArrayList<>()).when(this.collaborateurRepository).findByEspaceOrganisationId(currentEspaceOrganisationId);

		// Test sans envoi de mail
		this.inscriptionEspaceService.setPendingRequestValidity(inscriptionId, currentEspaceOrganisationId, true, false);

		final InscriptionEspaceEntity result = this.inscriptionEspaceRepository.findOne(inscriptionId);

		Assert.assertNotNull(result);
		// Date de traitement renseignée
		Assert.assertNotNull(result.getDateValidation());
		// Pas de date de suppression (car demande validée)
		Assert.assertNull(result.getDateSuppression());
		// Línscription est activée
		Assert.assertTrue(result.isActif());
		// Un seul rôle a été attribué
		Assert.assertEquals(1, result.getRolesFront().size());
		// Il s'agit d'un rôle contributeur
		Assert.assertTrue(result.getRolesFront().contains(RoleEnumFrontOffice.CONTRIBUTEUR));

	}

	/**
	 * Method: setPendingRequestValidity Test de la Validation de l'inscription d'un déclarant (inscription refusee).
	 */
	@Test
	public void testSetPendingRequestValidityRefusee() throws Exception {
		final Long inscriptionId = 1L, currentEspaceOrganisationId = 1L, userId = 1L;

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(currentEspaceOrganisationId);

		final DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(userId);

		final InscriptionEspaceEntity model = new InscriptionEspaceEntity();
		model.setId(inscriptionId);
		model.setActif(false);
		model.setDateValidation(null);
		model.setRolesFront(new HashSet<RoleEnumFrontOffice>());
		model.setDeclarant(declarantEntity);
		model.setEspaceOrganisation(espaceOrganisationEntity);

		Mockito.doReturn(model).when(this.inscriptionEspaceRepository).findByDateSuppressionIsNullAndIdAndEspaceOrganisationId(inscriptionId, currentEspaceOrganisationId);
		Mockito.doReturn(model).when(this.inscriptionEspaceRepository).findOne(inscriptionId);
		Mockito.doReturn(new ArrayList<>()).when(this.collaborateurRepository).findByEspaceOrganisationId(currentEspaceOrganisationId);

		// Test sans envoi de mail
		this.inscriptionEspaceService.setPendingRequestValidity(inscriptionId, currentEspaceOrganisationId, false, false);

		final InscriptionEspaceEntity result = this.inscriptionEspaceRepository.findOne(inscriptionId);

		Assert.assertNotNull(result);
		// Pas de date de validation renseignée (car demande refusée)
		Assert.assertNull(result.getDateValidation());
		// Date de suppression renseignée (car demande refusée)
		Assert.assertNotNull(result.getDateSuppression());
		// Línscription est désactivée
		Assert.assertFalse(result.isActif());
		// Aucun rôle n'a été attribué
		Assert.assertEquals(0, result.getRolesFront().size());

	}

	/**
	 * Method: setPendingRequestValidity Test de la Validation de l'inscription d'un déclarant : erreur de l'inscription qui n'appartient pas à l'espace passé en paramètre.
	 */
	@Test
	public void testSetPendingRequestValidityKO() throws Exception {
		final Long inscriptionId = 1L, currentEspaceOrganisationId = 1L, userId = 1L, wrongEspaceOrganisationId = 2L;

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(currentEspaceOrganisationId);

		final DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(userId);

		final InscriptionEspaceEntity model = new InscriptionEspaceEntity();
		model.setId(inscriptionId);
		model.setActif(false);
		model.setDateValidation(null);
		model.setRolesFront(new HashSet<RoleEnumFrontOffice>());
		model.setDeclarant(declarantEntity);
		model.setEspaceOrganisation(espaceOrganisationEntity);

		Mockito.doReturn(model).when(this.inscriptionEspaceRepository).findByDateSuppressionIsNullAndIdAndEspaceOrganisationId(inscriptionId, currentEspaceOrganisationId);
		Mockito.doReturn(model).when(this.inscriptionEspaceRepository).findOne(inscriptionId);
		Mockito.doReturn(new ArrayList<>()).when(this.collaborateurRepository).findByEspaceOrganisationId(currentEspaceOrganisationId);

		// Test sans envoi de mail		
		try {
			this.inscriptionEspaceService.setPendingRequestValidity(inscriptionId, wrongEspaceOrganisationId, false, false);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.MSG_404.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Method: setDeclarantRole Test de la Validation de l'inscription d'un déclarant (inscription acceptée).
	 */
	@Test
	public void testSetDeclarantRole() throws Exception {
		final Long inscriptionId = 1L, currentEspaceOrganisationId = 1L, userId = 1L;

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setDenomination("TEST");

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(currentEspaceOrganisationId);
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		final DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(userId);

		final Set<RoleEnumFrontOffice> roles = new HashSet<RoleEnumFrontOffice>();
		roles.add(RoleEnumFrontOffice.ADMINISTRATEUR);
		roles.add(RoleEnumFrontOffice.CONTRIBUTEUR);

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		final Date dateDeValidation = sdf.parse("18/06/1994");

		final InscriptionEspaceEntity model = new InscriptionEspaceEntity();
		model.setId(inscriptionId);
		model.setActif(false);
		model.setDateValidation(dateDeValidation);
		model.setDeclarant(declarantEntity);
		model.setEspaceOrganisation(espaceOrganisationEntity);
		model.setVerrouille(false);
		model.setRolesFront(roles);

		Mockito.doReturn(model).when(this.inscriptionEspaceRepository).findByDateSuppressionIsNullAndIdAndEspaceOrganisationId(inscriptionId, currentEspaceOrganisationId);
		Mockito.doReturn(model).when(this.inscriptionEspaceRepository).findOne(inscriptionId);
		Mockito.doReturn(new DeclarantDto()).when(this.declarantTransformer).modelToDto(declarantEntity);

		Set<RoleEnumFrontOffice> newRoles = new HashSet<>(Arrays.asList(RoleEnumFrontOffice.ADMINISTRATEUR, RoleEnumFrontOffice.PUBLICATEUR));

		this.inscriptionEspaceService.setDeclarantRoles(declarantEntity.getId(), currentEspaceOrganisationId, newRoles, model.isVerrouille());

		final InscriptionEspaceEntity result = this.inscriptionEspaceRepository.findOne(inscriptionId);

		Assert.assertTrue(!result.getRolesFront().contains(RoleEnumFrontOffice.CONTRIBUTEUR) && result.getRolesFront().contains(RoleEnumFrontOffice.PUBLICATEUR));
	}

	/**
	 * Method: setDeclarantRole Test ECHEC de l'inscription d'un déclarant (inscription acceptée) pour un déclarant dont l'inscription n'est pas validée.
	 */
	@Test(expected = BusinessGlobalException.class)
	public void testSetDeclarantRoleEchec() {
		final Long inscriptionId = 1L, currentEspaceOrganisationId = 1L, userId = 1L;

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setDenomination("TEST");

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(currentEspaceOrganisationId);
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		final DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(userId);

		final Set<RoleEnumFrontOffice> roles = new HashSet<RoleEnumFrontOffice>();
		roles.add(RoleEnumFrontOffice.ADMINISTRATEUR);
		roles.add(RoleEnumFrontOffice.CONTRIBUTEUR);

		final InscriptionEspaceEntity model = new InscriptionEspaceEntity();
		model.setId(inscriptionId);
		model.setActif(false);
		model.setDateValidation(null);
		model.setDateSuppression(null);
		model.setDeclarant(declarantEntity);
		model.setEspaceOrganisation(espaceOrganisationEntity);
		model.setVerrouille(false);
		model.setRolesFront(roles);

		Mockito.doReturn(model).when(this.inscriptionEspaceRepository).findByDateSuppressionIsNullAndIdAndEspaceOrganisationId(inscriptionId, currentEspaceOrganisationId);
		Mockito.doReturn(model).when(this.inscriptionEspaceRepository).findOne(inscriptionId);
		Mockito.doReturn(new DeclarantDto()).when(this.declarantTransformer).modelToDto(declarantEntity);

		Set<RoleEnumFrontOffice> newRoles = new HashSet<>(Arrays.asList(RoleEnumFrontOffice.ADMINISTRATEUR, RoleEnumFrontOffice.PUBLICATEUR));

		this.inscriptionEspaceService.setDeclarantRoles(declarantEntity.getId(), currentEspaceOrganisationId, newRoles, model.isVerrouille());
	}

	/**
	 * Method: inscriptionNouvelEspace Test de la creation d'un nouvel espace collaboratif
	 */
	@Test
	public void testInscriptionNouvelEspaceEchecLimiteAtteinte() throws Exception {

		ReflectionTestUtils.setField(inscriptionEspaceService, "limiteCreationEspaces", 2);

		final Long userId = 1L, currentEspaceOrganisationId = 1L;

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setDenomination("TEST");

		final EspaceOrganisationEntity espaceOrganisationEntity1 = new EspaceOrganisationEntity();
		espaceOrganisationEntity1.setId(currentEspaceOrganisationId);
		espaceOrganisationEntity1.setOrganisation(organisationEntity);

		final EspaceOrganisationEntity espaceOrganisationEntity2 = new EspaceOrganisationEntity();
		espaceOrganisationEntity2.setId(currentEspaceOrganisationId);
		espaceOrganisationEntity2.setOrganisation(organisationEntity);

		final EspaceOrganisationEntity espaceOrganisationEntity3 = new EspaceOrganisationEntity();
		espaceOrganisationEntity3.setId(currentEspaceOrganisationId);
		espaceOrganisationEntity3.setOrganisation(organisationEntity);

		List<EspaceOrganisationEntity> espaces = new ArrayList<>();
		espaces.add(espaceOrganisationEntity1);
		espaces.add(espaceOrganisationEntity2);
		espaces.add(espaceOrganisationEntity3);

		final InscriptionEspaceDto inscriptionEspace = new InscriptionEspaceDto();

		final List<EspaceOrganisationPieceDto> pieces = new ArrayList<>();

		Mockito.doReturn(Long.valueOf(espaces.size())).when(this.espaceOrganisationRepository).countByCreateurEspaceOrganisationIdAndDateActionCreationIsNull(userId);

		try {
			this.inscriptionEspaceService.inscriptionNouvelEspace(userId, inscriptionEspace, pieces);
		} catch (BusinessGlobalException e) {
			Assert.assertEquals(e.getMessage(), ErrorMessageEnum.LIMITE_CREATION_ESPACE_DEPASSEE.getMessage());
		}
	}
	
	/**
	 * Test la demande de désinscription faire par un CO
	 * demanderDesinscription(Long idEspaceOrganisation, Long idDeclarant, DesinscriptionDto desinscriptionDto, ArrayList<DesinscriptionPieceDto> pieces)
	 * 
	 * désinscription <> null on vérifie que l'on catch bien l'exception
	 */
	@Test
	public void demanderDesinscriptionDejaDesinscrit() {
		
		DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(ID_DECLARANT);
		
        EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
        
        DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
        desinscriptionEntity.setId(1L);
        
        DesinscriptionDto desinscriptionDto = new DesinscriptionDto();
        
        ArrayList<DesinscriptionPieceDto> pieces = new ArrayList<DesinscriptionPieceDto>();
		
        Mockito.doReturn(declarantEntity).when(this.declarantRepository).findOne(ID_DECLARANT);
        Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
        Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionRepository).findByEspaceOrganisation(espaceOrganisationEntity);
        Mockito.doReturn(null).when(this.desinscriptionRepository).save(desinscriptionEntity);
        Mockito.doNothing().when(this.desinscriptionMailer).sendDesinscriptionMail(espaceOrganisationEntity, desinscriptionEntity, declarantEntity);      

        try {
        	this.inscriptionEspaceService.demanderDesinscription(ID_ESPACE_ORGANISATION, ID_DECLARANT, desinscriptionDto, pieces);
		} catch (BusinessGlobalException e) {
			Assert.assertEquals(e.getMessage(), ErrorMessageEnum.ESPACE_ORGANISATION_DEJA_DESINSCRIT.getMessage());
		}
	}
	
	/**
	 * Test de la méthode: {@link InscriptionEspaceServiceImpll#inscriptionNouvelEspace(final Long declarantId, final InscriptionEspaceDto inscriptionEspaceDto, final List<EspaceOrganisationPieceDto> pieces)}
	 */
	@Test
	public void testInscriptionNouvelEspaceOrganisationExisteDeja() throws Exception {

		ReflectionTestUtils.setField(inscriptionEspaceService, "limiteCreationEspaces", 10);

		final Long declarantId = 1L;
		
		DeclarantEntity declarantEntity = new DeclarantEntity(declarantId);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setDenomination("TEST");
		organisationEntity.setAdresse("Adresse");		
		organisationEntity.setCodePostal("75000");
		organisationEntity.setVille("ville");
		organisationEntity.setPays("FRANCE");

		final EspaceOrganisationEntity espaceOrganisationEntity1 = new EspaceOrganisationEntity();
		espaceOrganisationEntity1.setId(11L);
		espaceOrganisationEntity1.setOrganisation(organisationEntity);

		final InscriptionEspaceDto inscriptionEspaceDto = new InscriptionEspaceDto();
		inscriptionEspaceDto.setOrganizationId(10L);

		final List<EspaceOrganisationPieceDto> pieces = new ArrayList<>();

		Mockito.doReturn(3L).when(this.espaceOrganisationRepository).countByCreateurEspaceOrganisationIdAndDateActionCreationIsNull(declarantId);
		Mockito.doReturn(espaceOrganisationEntity1).when(this.espaceOrganisationRepository).findEspaceOrganisationByOrganisationIdAndStatutNotRefusee(inscriptionEspaceDto.getOrganizationId());
		Mockito.doReturn(declarantEntity).when(this.declarantRepository).findOne(declarantId);
		Mockito.doReturn(organisationEntity).when(this.organisationRepository).findOne(inscriptionEspaceDto.getOrganizationId());
		
		try {
			this.inscriptionEspaceService.inscriptionNouvelEspace(declarantId, inscriptionEspaceDto, pieces);
		} catch (BusinessGlobalException e) {
			assertThat(e.getMessage()).contains(organisationEntity.getDenomination());
			assertThat(e.getMessage()).contains("a déjà été créé. Merci de vous adresser à son contact opérationnel");
		}
	}
	
	/**
	 * Test de la méthode: {@link InscriptionEspaceServiceImpll#inscriptionNouvelEspace(final Long declarantId, final InscriptionEspaceDto inscriptionEspaceDto, final List<EspaceOrganisationPieceDto> pieces)}
	 */
	@Test
	public void testInscriptionNouvelEspaceOrganisationOK() throws Exception {

		ReflectionTestUtils.setField(inscriptionEspaceService, "limiteCreationEspaces", 10);

		final Long declarantId = 1L;
		
		DeclarantEntity declarantEntity = new DeclarantEntity(declarantId);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setDenomination("TEST");
		organisationEntity.setAdresse("Adresse");		
		organisationEntity.setCodePostal("75000");
		organisationEntity.setVille("ville");
		organisationEntity.setPays("FRANCE");
		
		final EspaceOrganisationEntity espaceOrganisationNew = new EspaceOrganisationEntity();
		espaceOrganisationNew.setId(null);
		espaceOrganisationNew.setOrganisation(organisationEntity);
		
		final EspaceOrganisationEntity espaceOrganisationNewSaved = new EspaceOrganisationEntity();
		espaceOrganisationNewSaved.setId(999L);
		espaceOrganisationNewSaved.setOrganisation(organisationEntity);
		
		final InscriptionEspaceDto inscriptionEspaceDto = new InscriptionEspaceDto();
		inscriptionEspaceDto.setOrganizationId(10L);

		final List<EspaceOrganisationPieceDto> pieces = new ArrayList<>();
		
		List<CollaborateurEntity> listCollaborateurEntity = new ArrayList<CollaborateurEntity>();		

		Mockito.doReturn(3L).when(this.espaceOrganisationRepository).countByCreateurEspaceOrganisationIdAndDateActionCreationIsNull(declarantId);
		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findEspaceOrganisationByOrganisationIdAndStatutNotRefusee(inscriptionEspaceDto.getOrganizationId());
		Mockito.doReturn(declarantEntity).when(this.declarantRepository).findOne(declarantId);
		Mockito.doReturn(organisationEntity).when(this.organisationRepository).findOne(inscriptionEspaceDto.getOrganizationId());	
		Mockito.doReturn(espaceOrganisationNewSaved).when(this.espaceOrganisationRepository).save(espaceOrganisationNew);
		
		Mockito.doReturn(listCollaborateurEntity).when(this.collaborateurRepository).findByEspaceOrganisationId(espaceOrganisationNewSaved.getId());		
		
		InscriptionEspaceDto retourInscriptionEspaceDto = this.inscriptionEspaceService.inscriptionNouvelEspace(declarantId, inscriptionEspaceDto, pieces);

		Assert.assertNull(retourInscriptionEspaceDto);
	}
	
	/**
	 * Test de la méthode: {@link InscriptionEspaceServiceImpll#inscriptionEspaceExistant(final Long declarantId, final InscriptionEspaceDto inscriptionEspaceDto)}
	 */
	@Test
	public void testInscriptionEspaceExistantAccesVerouille() {

		final Long organisationId = 1L;
		final Long declarantId = 1L;

		final InscriptionEspaceDto inscriptionEspaceDto = new InscriptionEspaceDto();
		inscriptionEspaceDto.setOrganizationId(organisationId);
		
		InscriptionEspaceEntity inscriptionEspace = new InscriptionEspaceEntity(2L);
		inscriptionEspace.setActif(true);
		inscriptionEspace.setVerrouille(true);

		Mockito.doReturn(inscriptionEspace).when(this.inscriptionEspaceRepository).findByDateSuppressionIsNullAndDeclarantIdAndEspaceOrganisationOrganisationId(declarantId,
				organisationId);		
		try {
			this.inscriptionEspaceService.inscriptionEspaceExistant(declarantId, inscriptionEspaceDto);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.ACCES_VEROUILLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	
	/**
	 * Test de la méthode: {@link InscriptionEspaceServiceImpll#inscriptionEspaceExistant(final Long declarantId, final InscriptionEspaceDto inscriptionEspaceDto)}
	 */
	@Test
	public void testInscriptionEspaceExistantDejaInscrit() {

		final Long organisationId = 1L;
		final Long declarantId = 1L;

		final InscriptionEspaceDto inscriptionEspaceDto = new InscriptionEspaceDto();
		inscriptionEspaceDto.setOrganizationId(organisationId);
		
		InscriptionEspaceEntity inscriptionEspace = new InscriptionEspaceEntity(2L);
		inscriptionEspace.setActif(true);
		inscriptionEspace.setVerrouille(false);

		Mockito.doReturn(inscriptionEspace).when(this.inscriptionEspaceRepository).findByDateSuppressionIsNullAndDeclarantIdAndEspaceOrganisationOrganisationId(declarantId,
				organisationId);		
		try {
			this.inscriptionEspaceService.inscriptionEspaceExistant(declarantId, inscriptionEspaceDto);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.VOUS_ETES_DEJA_INSCRIS_A_CET_ESPACE_ORGANISATION.getMessage()).isEqualTo(ex.getMessage());
		}
	}

}
