/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.publication;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationActionTransformer;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActionDto;
import fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ActionMeneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DecisionConcerneeEntity;

/**
 * Test de la classe {@link PublicationTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class PublicationActionTransformerTest
{

    /** Transformer des publications. */
    @Autowired
    private PublicationActionTransformer publicationActionTransformer;

    /**
     * Test de la métode modelToDto de {@link PublicationActionTransformer#actionToPublicationAction(ActionRepresentationInteretEntity)}
     */
    @Test
    public void testActionToPublicationAction() {
        final ActionRepresentationInteretEntity entity = CommonTestClass.getActionRepresentationInteretEntity();
        entity.setActiviteRepresentationInteret(CommonTestClass.getActiviteRepresentationInteretEntity());
        entity.getTiers().stream().forEach(tier->{
        	tier.setId(3L);
        	tier.setDenomination("truc");
        });
        final PublicationActionDto action = this.publicationActionTransformer.actionToPublicationAction(entity);

        assertThat(action.getActionsMenees()).containsAll(entity.getActionsMenees().stream().map(ActionMeneeEntity::getLibelle).collect(Collectors.toSet()));
        
        Assert.assertEquals(action.getActionMeneeAutre(), entity.getActionMeneeAutre());

        assertThat(action.getDecisionsConcernees()).containsAll(entity.getDecisionsConcernees().stream().map(DecisionConcerneeEntity::getLibelle).collect(Collectors.toSet()));
            
   
        Assert.assertEquals(action.getResponsablePublicAutre(), entity.getResponsablePublicAutre());

       
//        assertThat(action.getTiers().stream().filter(tier->!tier.equals("(en propre)")).collect(Collectors.toList())).containsAll(entity.getTiers().stream().map(OrganisationEntity::getDenomination).collect(Collectors.toSet()));

        Assert.assertEquals(action.getObservation(), entity.getObservation());

    }
}
