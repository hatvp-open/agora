/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice;

import com.mifmif.common.regex.Generex;
import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.backoffice.DemandeOrganisationLockBoTransformer;
import fr.hatvp.registre.commons.dto.backoffice.DemandeOrganisationLockBoDto;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.commons.utils.ValidatePatternUtils;
import fr.hatvp.registre.persistence.entity.backoffice.DemandeOrganisationLockBoEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;
import fr.hatvp.registre.persistence.repository.backoffice.DemandeOrganisationLockBoRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe de test su service
 * {@link fr.hatvp.registre.business.backoffice.DemandeOrganisationLockBoService}
 *
 * @version $Revision$ $Date${0xD}
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
    loader = AnnotationConfigContextLoader.class)
public class DemandeOrganisationLockBoServiceTest {
    /** Durée Timeout lock d'entité (en secondes). */
    @Value("${app.lock.demandeorganisation.timeout}")
    private long lockTimeout;

    /** Repository des demandes organisations locks. */
    @Mock
    private DemandeOrganisationLockBoRepository demandeOrganisationLockBoRepository;

    /** Transformer de l'entité DemandeOrganisationLockBoEntity. */
    @Mock
    private DemandeOrganisationLockBoTransformer demandeOrganisationLockBoTransformer;

    /** Injections des mocks. */
    @InjectMocks
    private DemandeOrganisationLockBoServiceImpl demandeOrganisationLockBoService;

    /**
     * Initialisation des tests..
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test de la méthode
     * {@link fr.hatvp.registre.business.backoffice.DemandeOrganisationLockBoService#getLockOn(Long)}}
     * Cas d'un lock actif
     */
    @Test
    public void testGetLockOnWithResult() {
        final Long demandeOrganisationId = 2L;

        final DemandeOrganisationEntity demandeOrganisationEntity = new DemandeOrganisationEntity();
        demandeOrganisationEntity.setId(demandeOrganisationId);
        demandeOrganisationEntity.setDateCreation(new Date());

        final UtilisateurBoEntity utilisateurBoEntity = getUtilisateurBoEntity();

        final DemandeOrganisationLockBoEntity demandeOrganisationLockBoEntity = getDemandeOrganisationLockBoEntity(
            demandeOrganisationEntity, utilisateurBoEntity,
            Date.from(LocalDateTime.now().minus(this.lockTimeout / 2, ChronoUnit.SECONDS)
                .atZone(ZoneId.systemDefault()).toInstant()));

        final List<DemandeOrganisationLockBoEntity> result = new ArrayList<>();
        result.add(demandeOrganisationLockBoEntity);

        final DemandeOrganisationLockBoDto demandeOrganisationLockBoDto = new DemandeOrganisationLockBoDto();
        demandeOrganisationLockBoDto.setId(demandeOrganisationLockBoEntity.getId());
        demandeOrganisationLockBoDto
            .setUtilisateurBoId(demandeOrganisationLockBoEntity.getUtilisateurBo().getId());
        demandeOrganisationLockBoDto.setDemandeOrganisationId(
            demandeOrganisationLockBoEntity.getDemandeOrganisation().getId());
        demandeOrganisationLockBoDto
            .setLockTimeStart(demandeOrganisationLockBoEntity.getLockedTime());

        final Long remainingTime = this.lockTimeout
            - (new Date().getTime() - demandeOrganisationLockBoEntity.getLockedTime().getTime())
            / 1000;
        demandeOrganisationLockBoDto.setLockTimeRemain(remainingTime > 0 ? remainingTime : 0);

        Mockito.doReturn(demandeOrganisationLockBoDto)
            .when(this.demandeOrganisationLockBoTransformer)
            .modelToDto(demandeOrganisationLockBoEntity);

        Mockito.doReturn(result).when(this.demandeOrganisationLockBoRepository)
            .findByDemandeOrganisationId(demandeOrganisationId);

        final DemandeOrganisationLockBoDto res = this.demandeOrganisationLockBoService
            .getLockOn(demandeOrganisationId);

        Assert.assertNotNull(res);
        Assert.assertTrue(res.getLockTimeRemain() > 0);
    }

    /**
     * Test de la méthode
     * {@link fr.hatvp.registre.business.backoffice.DemandeOrganisationLockBoService#getLockOn(Long)}}
     * Cas d'un lock inactif
     */
    @Test
    public void testGetLockOnNoResult() {
        final Long demandeOrganisationId = 2L;

        final DemandeOrganisationEntity demandeOrganisationEntity = new DemandeOrganisationEntity();
        demandeOrganisationEntity.setId(demandeOrganisationId);
        demandeOrganisationEntity.setDateCreation(new Date());

        final UtilisateurBoEntity utilisateurBoEntity = getUtilisateurBoEntity();

        final DemandeOrganisationLockBoEntity demandeOrganisationLockBoEntity = getDemandeOrganisationLockBoEntity(
            demandeOrganisationEntity, utilisateurBoEntity,
            Date.from(LocalDateTime.now().minus(this.lockTimeout * 2, ChronoUnit.SECONDS)
                .atZone(ZoneId.systemDefault()).toInstant()));

        final List<DemandeOrganisationLockBoEntity> result = new ArrayList<>();
        result.add(demandeOrganisationLockBoEntity);

        final DemandeOrganisationLockBoDto demandeOrganisationLockBoDto = new DemandeOrganisationLockBoDto();
        demandeOrganisationLockBoDto.setId(demandeOrganisationLockBoEntity.getId());
        demandeOrganisationLockBoDto
            .setUtilisateurBoId(demandeOrganisationLockBoEntity.getUtilisateurBo().getId());
        demandeOrganisationLockBoDto.setDemandeOrganisationId(
            demandeOrganisationLockBoEntity.getDemandeOrganisation().getId());
        demandeOrganisationLockBoDto
            .setLockTimeStart(demandeOrganisationLockBoEntity.getLockedTime());

        final Long remainingTime = this.lockTimeout
            - (new Date().getTime() - demandeOrganisationLockBoEntity.getLockedTime().getTime())
            / 1000;
        demandeOrganisationLockBoDto.setLockTimeRemain(remainingTime > 0 ? remainingTime : 0);

        Mockito.doReturn(demandeOrganisationLockBoDto)
            .when(this.demandeOrganisationLockBoTransformer)
            .modelToDto(demandeOrganisationLockBoEntity);

        Mockito.doReturn(result).when(this.demandeOrganisationLockBoRepository)
            .findByDemandeOrganisationId(demandeOrganisationId);

        final DemandeOrganisationLockBoDto res = this.demandeOrganisationLockBoService
            .getLockOn(demandeOrganisationId);

        Assert.assertNull(res);
    }

    /**
     * Récupérer uen instance de {@link DemandeOrganisationLockBoEntity}
     */
    DemandeOrganisationLockBoEntity getDemandeOrganisationLockBoEntity(
        final DemandeOrganisationEntity demandeOrganisationEntity,
        final UtilisateurBoEntity utilisateurBoEntity, final Date from) {
        final DemandeOrganisationLockBoEntity demandeOrganisationLockBoEntity = new DemandeOrganisationLockBoEntity();
        demandeOrganisationLockBoEntity.setId(1L);
        demandeOrganisationLockBoEntity.setUtilisateurBo(utilisateurBoEntity);
        demandeOrganisationLockBoEntity.setDemandeOrganisation(demandeOrganisationEntity);
        demandeOrganisationLockBoEntity.setLockedTime(from);
        return demandeOrganisationLockBoEntity;
    }

    /**
     * @return entité utilisateur bo.
     */
    private UtilisateurBoEntity getUtilisateurBoEntity() {
        final Generex generex = new Generex(ValidatePatternUtils.getPasswordGenerationPattern());
        final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
        utilisateurBoEntity.setEmail("test@test.test");
        utilisateurBoEntity.setId(3L);
        utilisateurBoEntity.setPassword(generex.random());
        utilisateurBoEntity.setNom(RegistreUtils.randomString(10));
        utilisateurBoEntity.setPrenom(RegistreUtils.randomString(10));
        return utilisateurBoEntity;
    }
}
