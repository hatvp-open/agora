/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.hatvp.registre.business.impl.activite.QualificationObjetServiceImpl;
import fr.hatvp.registre.business.transformer.proxy.activite.QualificationObjetTransformer;
import fr.hatvp.registre.commons.dto.activite.QualificationObjetDto;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.utilities.LocalDateNowBuilder;
import fr.hatvp.registre.commons.utils.RegistreRandomUtils;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.activite.QualificationObjetEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.repository.activite.QualificationObjetRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

/**
 * Test du service {@link fr.hatvp.registre.business.QualificationObjetService}
 *
 * @version $Revision$ $Date${0xD}
 */

public class QualificationObjetServiceTest
{
    /** Repository qualification. */
    @Mock QualificationObjetRepository qualificationObjetRepository;

    /** Espace Organisation repository */
    @Mock EspaceOrganisationRepository espaceOrganisationRepository;

    /** Transformer */
    @Mock QualificationObjetTransformer qualificationObjetTransformer;

    /** Local Date Now Builder */
    @Mock LocalDateNowBuilder localDateNowBuilder;

    /* Utilitaires de générations d'éléments aléatoires */
    @Mock RegistreRandomUtils registreRandomUtils;

    /** Injection des mock. */
    @InjectMocks QualificationObjetServiceImpl qualificationObjetService;

    private final String ID_FICHE = "CPT2018SFV";

    /**
     * Initialisation des tests.
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        // Process mock annotations
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test de la méthode avec une fiche existante.
     * <p>
     * Test de la méthode: {@link fr.hatvp.registre.business.QualificationObjetService#getLastQualiObjetByFicheId(String)}
     */
    @Test
    public void testGetLastQualiObjetByFicheIdAvecFicheIdExistante() {
        final QualificationObjetDto qualificationObjetDto = new QualificationObjetDto();
        qualificationObjetDto.setId(1L);
        qualificationObjetDto.setIdFiche(this.ID_FICHE);

        final QualificationObjetEntity qualificationObjetEntity = new QualificationObjetEntity();
        qualificationObjetEntity.setId(qualificationObjetDto.getId());
        qualificationObjetEntity.setIdFiche(qualificationObjetDto.getIdFiche());

        Mockito.doReturn(qualificationObjetEntity).when(this.qualificationObjetRepository).findTopByIdFiche(this.ID_FICHE);
        Mockito.doReturn(qualificationObjetDto).when(this.qualificationObjetTransformer).modelToDto(qualificationObjetEntity);
        Mockito.doReturn(LocalDate.now()).when(this.localDateNowBuilder).getLocalDateNow();

        final QualificationObjetDto foundQualif = this.qualificationObjetService.getLastQualiObjetByFicheId(this.ID_FICHE);

        Assert.assertNotNull(qualificationObjetDto);
        Assert.assertNotNull(qualificationObjetEntity);
        Assert.assertNotNull(foundQualif);
        Assert.assertEquals(qualificationObjetEntity.getId(), foundQualif.getId());
        Assert.assertEquals(qualificationObjetEntity.getIdFiche(), foundQualif.getIdFiche());
    }

    /**
     * Test de la méthode de creation d'une fiche existante.
     * <p>
     * Test de la méthode: {@link fr.hatvp.registre.business.QualificationObjetService#createQualificationObjet(QualificationObjetDto, Long)}
     */
    @Test
    public void testCreateDeclarationActivite() {

        ArgumentCaptor<QualificationObjetEntity> argumentCaptor = ArgumentCaptor.forClass(QualificationObjetEntity.class);

        Long currentEspaceOrganisationId = 1L;

        OrganisationEntity organisation = new OrganisationEntity(new Random().nextLong());
        organisation.setDenomination(RegistreUtils.randomString(20));
        organisation.setSiren(RegistreUtils.randomString(11));
        organisation.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);

        EspaceOrganisationEntity espace = new EspaceOrganisationEntity(currentEspaceOrganisationId);
        espace.setOrganisation(organisation);

        QualificationObjetDto qualificationObjetDto = new QualificationObjetDto();
        qualificationObjetDto.setNomUsage(espace.getNomUsage());
        qualificationObjetDto.setDenomination(espace.getOrganisation().getDenomination());
        qualificationObjetDto.setIdentifiantNational(espace.getOrganisation().computeIdNational());
        qualificationObjetDto.setDateEnregistrement(LocalDate.now());
        qualificationObjetDto.setObjet(RegistreUtils.randomString(50));
        qualificationObjetDto.setNote(RegistreUtils.randomBoolean());
        qualificationObjetDto.setCoefficiantConfiance(RegistreUtils.randomFloat(1));

        QualificationObjetEntity persistedEntity = new QualificationObjetEntity();
        persistedEntity.setNomUsage(qualificationObjetDto.getNomUsage());
        persistedEntity.setDenomination(qualificationObjetDto.getDenomination());
        persistedEntity.setIdentifiantNational(qualificationObjetDto.getIdentifiantNational());
        persistedEntity.setDateEnregistrement(qualificationObjetDto.getDateEnregistrement());
        persistedEntity.setObjet(qualificationObjetDto.getObjet());
        persistedEntity.setNote(qualificationObjetDto.isNote());
        persistedEntity.setCoefficiantConfiance(qualificationObjetDto.getCoefficiantConfiance());

        Mockito.doReturn(espace).when(this.espaceOrganisationRepository).findOne(currentEspaceOrganisationId);
        Mockito.doReturn(persistedEntity).when(this.qualificationObjetTransformer).dtoToModel(qualificationObjetDto);
        when(this.qualificationObjetRepository.save(Mockito.any(QualificationObjetEntity.class))).thenReturn(persistedEntity);
        Mockito.doReturn(qualificationObjetDto).when(this.qualificationObjetTransformer).modelToDto(persistedEntity);

        QualificationObjetDto dto = qualificationObjetService.createQualificationObjet(qualificationObjetDto, currentEspaceOrganisationId);

        verify(espaceOrganisationRepository, times(1)).findOne(currentEspaceOrganisationId);
        verify(qualificationObjetRepository, times(1)).save(argumentCaptor.capture());

        assertThat(dto.getCoefficiantConfiance()).isEqualTo(qualificationObjetDto.getCoefficiantConfiance());
        assertThat(dto.getIdentifiantNational()).isEqualTo(qualificationObjetDto.getIdentifiantNational());
        assertThat(dto.getDateEnregistrement()).isEqualTo(qualificationObjetDto.getDateEnregistrement());
        assertThat(dto.getDenomination()).isEqualTo(qualificationObjetDto.getDenomination());
        assertThat(dto.getNomUsage()).isEqualTo(qualificationObjetDto.getNomUsage());
        assertThat(dto.getObjet()).isEqualTo(qualificationObjetDto.getObjet());
        assertThat(dto.isNote()).isEqualTo(qualificationObjetDto.isNote());
    }

    /**
     * Test de la méthode de maj d'une qualif
     * <p>
     * Test de la méthode: {@link fr.hatvp.registre.business.QualificationObjetService#updateQualificationObjet(Long, String, Long)}
     */
    @Test
    public void testUpdateQualificationObjet() {
        Long currentEspaceOrganisationId = new Random().nextLong();
        Long qualifObjetId = new Random().nextLong();
        String qualifObjetIdFiche = RegistreUtils.randomString(10);

        QualificationObjetEntity qualificationObjetEntity = new QualificationObjetEntity();
        qualificationObjetEntity.setNomUsage("RANDOM NOM USAGE");
        qualificationObjetEntity.setDenomination("RANDOM DENOMINATION");
        qualificationObjetEntity.setCoefficiantConfiance(RegistreUtils.randomFloat(1));
        qualificationObjetEntity.setDateEnregistrement(LocalDate.now());
        qualificationObjetEntity.setNote(RegistreUtils.randomBoolean());
        qualificationObjetEntity.setObjet(RegistreUtils.randomString(50));
        qualificationObjetEntity.setIdentifiantNational(RegistreUtils.randomString(10));

        when(qualificationObjetRepository.findOne(qualifObjetId)).thenReturn(qualificationObjetEntity);
        QualificationObjetEntity qualificationObjetEntity2 = qualificationObjetEntity;
        qualificationObjetEntity2.setIdFiche(qualifObjetIdFiche);
        when(qualificationObjetRepository.save(Mockito.any(QualificationObjetEntity.class))).thenReturn(qualificationObjetEntity2);

        QualificationObjetDto qualificationObjetDto = new QualificationObjetDto();
        qualificationObjetDto.setNomUsage(qualificationObjetEntity2.getNomUsage());
        qualificationObjetDto.setDenomination(qualificationObjetEntity2.getDenomination());
        qualificationObjetDto.setCoefficiantConfiance(qualificationObjetEntity2.getCoefficiantConfiance());
        qualificationObjetDto.setDateEnregistrement(qualificationObjetEntity2.getDateEnregistrement());
        qualificationObjetDto.setNote(qualificationObjetEntity2.isNote());
        qualificationObjetDto.setObjet(qualificationObjetEntity2.getObjet());
        qualificationObjetDto.setIdentifiantNational(qualificationObjetEntity2.getIdentifiantNational());
        qualificationObjetDto.setIdFiche(qualificationObjetEntity2.getIdFiche());

        Mockito.doReturn(qualificationObjetDto).when(this.qualificationObjetTransformer).modelToDto(qualificationObjetEntity2);

        QualificationObjetDto dto = this.qualificationObjetService.updateQualificationObjet(qualifObjetId,  qualifObjetIdFiche, currentEspaceOrganisationId);

        verify(qualificationObjetRepository, times(1)).findOne(qualifObjetId);
        verify(qualificationObjetRepository, times(1)).save(Mockito.any(QualificationObjetEntity.class));

        assertThat(dto.getIdFiche()).isEqualTo(qualifObjetIdFiche);

    }
}
