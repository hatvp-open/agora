

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.DemandeOrganisationTransformer;
import fr.hatvp.registre.commons.dto.DemandeOrganisationDto;
import fr.hatvp.registre.commons.lists.ContextDemandeOrganisationEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * Test de la classe
 * {@link fr.hatvp.registre.business.transformer.impl.DemandeOrganisationTransformerImpl}.
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
    loader = AnnotationConfigContextLoader.class)
public class DemandeOrganisationTransformerTest {

    /** Transformer à tester. */
    @Autowired
    private DemandeOrganisationTransformer demandeOrganisationTransformer;

    /**
     * Test de la métode modelToDto.
     */
    @Test
    public void testDtoToModel() {
        /*
         * Création du Dto
         */
        final DemandeOrganisationDto dto = new DemandeOrganisationDto();
        dto.setId(1L);
        dto.setVersion(0);

        dto.setContext(ContextDemandeOrganisationEnum.Ajout_Espace);
        dto.setType(TypeOrganisationEnum.ASSOC);

        dto.setEspaceOrganisationId(2L);
        dto.setDeclarantId(3L);

        dto.setAdresse(RegistreUtils.randomString(20));
        dto.setCodePostal(RegistreUtils.randomString(5));
        dto.setCommentaire(RegistreUtils.randomString(500));
        dto.setDenomination(RegistreUtils.randomString(10));
        dto.setDirigeant(RegistreUtils.randomString(20));
        dto.setIdentifiant(RegistreUtils.randomString(9));
        dto.setPays(RegistreUtils.randomString(15));
        dto.setVille(RegistreUtils.randomString(10));

        /*
         * Transformation
         */
        final DemandeOrganisationEntity model = this.demandeOrganisationTransformer.dtoToModel(dto);

        /*
         * Assertions
         */
        Assert.assertNotNull(model);
        Assert.assertEquals(dto.getId(), model.getId());
        Assert.assertEquals(dto.getVersion(), model.getVersion());

        Assert.assertEquals(dto.getType(), model.getType());
        Assert.assertEquals(dto.getContext(), model.getContext());

        Assert.assertNotNull(model.getDeclarant());
        Assert.assertNotNull(model.getEspaceOrganisation());
        Assert.assertEquals(dto.getDeclarantId(), model.getDeclarant().getId());
        Assert.assertEquals(dto.getEspaceOrganisationId(), model.getEspaceOrganisation().getId());

        Assert.assertEquals(dto.getAdresse(), model.getAdresse());
        Assert.assertEquals(dto.getCodePostal(), model.getCodePostal());
        Assert.assertEquals(dto.getCommentaire(), model.getCommentaire());
        Assert.assertEquals(dto.getDenomination(), model.getDenomination());
        Assert.assertEquals(dto.getDirigeant(), model.getDirigeant());
        Assert.assertEquals(dto.getIdentifiant(), model.getIdentifiant());
        Assert.assertEquals(dto.getPays(), model.getPays());
        Assert.assertEquals(dto.getVille(), model.getVille());
    }

}
