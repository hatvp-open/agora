/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.EspaceDeclarantPieceTransformer;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.EspaceDeclarantPieceDto;
import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.piece.EspaceDeclarantPieceEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.declarant.EspaceDeclarantPiecesRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import fr.hatvp.registre.business.GestionPiecesEspaceDeclarantService;

import java.util.Date;
/**
 * Test du service GestionPiecesEspaceDeclarantService
 * {@link GestionPiecesEspaceDeclarantService}.
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
    loader = AnnotationConfigContextLoader.class)
public class GestionPiecesEspaceDeclarantServiceTest
{
    /**
     * Le transformer pour la gestion des pièces versées.
     */
    @Mock
    private EspaceDeclarantPieceTransformer espaceDeclarantPieceTransformer;

    /**
     * Repository pour l'accès à la table des pièces complémentaires en BDD.
     */
    @Mock
    private EspaceDeclarantPiecesRepository espaceDeclarantPiecesRepository;

    /**
     * Repository pour l'accès au déclarant.
     */
    @Mock
    private DeclarantRepository declarantRepository;

    /** Injecter les mock dans le service. */
    @InjectMocks
    private GestionPiecesEspaceDeclarantServiceImpl gestionPiecesEspaceDeclarantService;

    private final String NOM_PIECE = "fichier.extension";
    private final OrigineVersementEnum ORIGINE_VERSEMENT = OrigineVersementEnum.INSCRIPTION_DECLARANT;
    private final byte[] CONTENT_FICHIER = { 0x1, 0x2, 0x3, 0x4, 0x5 };
    private final Date DATE_VERSEMENT = new Date();
    private final String MEDIA_TYPE = "image/jpeg";

    /**
     * Initialiser les mocks avant de lancer les test.
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        // Process mock annotations
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test du versement de pièce d'identité pour un déclarant
     * Method: verserPiece(Long idDeclarant, EspaceDeclarantPieceDto piece, OrigineVersementEnum origine)
     */
    @Test
    public void testVerserPiece() {

        final DeclarantEntity declarantEntity = new DeclarantEntity();
        declarantEntity.setId(1L);

        final DeclarantDto declarantDto = new DeclarantDto();
        declarantDto.setId(1L);

        final EspaceDeclarantPieceEntity pieceEntity = new EspaceDeclarantPieceEntity();
        pieceEntity.setId(1L);
        pieceEntity.setVersion(0);
        pieceEntity.setNomFichier(NOM_PIECE);
        pieceEntity.setDeclarant(declarantEntity);
        pieceEntity.setContenuFichier(CONTENT_FICHIER);
        pieceEntity.setDateVersementPiece(DATE_VERSEMENT);
        pieceEntity.setMediaType(MEDIA_TYPE);
        pieceEntity.setOrigineVersement(ORIGINE_VERSEMENT);

        final EspaceDeclarantPieceDto piece = new EspaceDeclarantPieceDto();
        piece.setId(1L);
        piece.setVersion(0);
        piece.setNomFichier(NOM_PIECE);
        piece.setDeclarantId(declarantDto.getId());
        piece.setContenuFichier(CONTENT_FICHIER);
        piece.setDateVersementPiece(DATE_VERSEMENT);
        piece.setMediaType(MEDIA_TYPE);
        piece.setOrigineVersement(ORIGINE_VERSEMENT);

        Mockito.doReturn(declarantEntity).when(this.declarantRepository)
            .findOne(1L);

        Mockito.doReturn(pieceEntity).when(this.espaceDeclarantPieceTransformer)
            .dtoToModel(piece);

        Mockito.doReturn(pieceEntity).when(espaceDeclarantPiecesRepository).save(pieceEntity);

        Mockito.doReturn(piece).when(espaceDeclarantPieceTransformer).modelToDto(pieceEntity);

        final EspaceDeclarantPieceDto pieceSaved = this.gestionPiecesEspaceDeclarantService.verserPiece(
            1L,
            piece,
            ORIGINE_VERSEMENT);

        Assert.assertEquals(piece.getNomFichier(), pieceSaved.getNomFichier());
        Assert.assertEquals(piece.getContenuFichier(), pieceSaved.getContenuFichier());

    }
}
