/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.activite;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.pdf.activite.RapportActivitePdfTemplateData;
import fr.hatvp.registre.business.transformer.impl.activite.ActiviteRepresentationInteretSimpleTransformerImpl;
import fr.hatvp.registre.business.transformer.impl.activite.ExerciceComptableEtDeclarationTransformerImpl;
import fr.hatvp.registre.business.transformer.impl.activite.ExerciceComptableSimpleTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.activite.ActiviteRepresentationInteretSimpleTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ExerciceComptableEtDeclarationTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ExerciceComptableSimpleTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.batch.DeclarationReminderTransformer;
import fr.hatvp.registre.business.utils.HumanReadableUniqueIdUtil;
import fr.hatvp.registre.business.utils.PdfGeneratorUtil;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableDatesDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableEtDeclarationDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableSimpleDto;
import fr.hatvp.registre.commons.dto.activite.batch.DeclarationReminderDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.PublicationRelanceNiveauEnum;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.utilities.LocalDateNowBuilder;
import fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ActionMeneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ChiffreAffaireEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DecisionConcerneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.MontantDepenseEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ResponsablePublicEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.entity.publication.batch.relance.PublicationRelanceEntity;
import fr.hatvp.registre.persistence.repository.activite.ActiviteRepresentationInteretRepository;
import fr.hatvp.registre.persistence.repository.activite.ExerciceComptableRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationActiviteRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationExerciceRepository;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.ChiffreAffaireRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.MontantDepenseRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestBusinessConfig.class }, loader = AnnotationConfigContextLoader.class)
public class ExerciceComptableServiceTest {

	@Mock
	private EspaceOrganisationRepository espaceOrganisationRepository;

    @Mock
    private DesinscriptionRepository desinscriptionRepository;
	@Mock
	private PublicationRepository publicationRepository;
	@Mock
	private ExerciceComptableRepository exerciceComptableRepository;
	
	@Mock
	private ActiviteRepresentationInteretRepository activiteRepresentationInteretRepository;

	@Mock
	private ChiffreAffaireRepository chiffreAffaireRepository;

	@Mock
	private MontantDepenseRepository montantDepenseRepository;

	@Spy
	private ExerciceComptableSimpleTransformer exerciceComptableSimpleTransformer = new ExerciceComptableSimpleTransformerImpl();

	@Spy
	@InjectMocks
	private ExerciceComptableEtDeclarationTransformer exerciceComptableEtDeclarationTransformer = new ExerciceComptableEtDeclarationTransformerImpl();

	@Spy
	private ActiviteRepresentationInteretSimpleTransformer activiteRepresentationInteretSimpleTransformer = new ActiviteRepresentationInteretSimpleTransformerImpl();

	@Spy
	private LocalDateNowBuilder localDateNowBuilder;

	@Mock
	private PdfGeneratorUtil pdfGeneratorUtil;
	
	@Mock
	private PublicationExerciceRepository publicationExerciceRepository;
	
	@Mock
	private PublicationActiviteRepository publicationActiviteRepository;

	@InjectMocks
	private ExerciceComptableServiceImpl exerciceComptableService;
	
	@Mock
	private DeclarationReminderTransformer declarationReminderTransformer;

	private static final Long ESPACE_ORGANISATION_ID = 1L;	

	private static final List<StatutPublicationEnum> LIST_STATUT_PUBLICATION_PUBLIE = Arrays.asList(StatutPublicationEnum.PUBLIEE,StatutPublicationEnum.MAJ_PUBLIEE,StatutPublicationEnum.REPUBLIEE); 
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}



	@Test
	/**
	 * Test avec date de cloture au 01/02 et date du jour au 05/03/2019
	 * 01/07/17 au 31/12/17
	 * 01/01/18 au 01/02/18
	 * 02/02/18 au 01/02/19

	 */
	public void testCheckExercices1() {
		//1. on pose lee postulat de depart
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setFinExerciceFiscal("01-02");
		espaceOrganisationEntity.setDateActionCreation(java.util.Date.from(LocalDate.parse("01-01-2018", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		localDateNowBuilder.setClock(Clock.fixed(LocalDate.of(2019, 03, 05).atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault()));

		when(espaceOrganisationRepository.findOne(Mockito.anyLong())).thenReturn(espaceOrganisationEntity);
		//2. on appelle la methode a tester
		List<ExerciceComptableSimpleDto> dtos = this.exerciceComptableService.initExerciceComptable(Mockito.anyLong());
		//3. on verifie la veracite des resultats
		verify(espaceOrganisationRepository, times(1)).findOne(Mockito.anyLong());
		//verify(exerciceComptableRepository, times(4)).findOneBydateDebutAndDateFinAndEspaceOrganisationId(Mockito.any(LocalDate.class),Mockito.any(LocalDate.class), Mockito.anyLong());
		//verify(espaceOrganisationRepository, times(1)).flush();

		assertThat(dtos.size()).isEqualTo(3);
		assertThat(dtos.get(0).getDateDebut()).isEqualTo(LocalDate.of(2018, 02, 2));
		assertThat(dtos.get(0).getDateFin()).isEqualTo(LocalDate.of(2019, 02, 1));
		assertThat(dtos.get(1).getDateDebut()).isEqualTo(LocalDate.of(2018, 01, 1));
		assertThat(dtos.get(1).getDateFin()).isEqualTo(LocalDate.of(2018, 02, 1));
		assertThat(dtos.get(2).getDateDebut()).isEqualTo(LocalDate.of(2017, 07, 1));
		assertThat(dtos.get(2).getDateFin()).isEqualTo(LocalDate.of(2017, 12, 31));

	}

	
	@Test
	/**
	 * Test avec date inscription au 03/09/2017 et date de cloture au 31/12 et date du jour au 15/06/2021
	 * 01/07/17 au 31/12/17
	 * 01/01/18 au 31/12/2018
	 * 01/01/2019 au 31/12/2019
	 * 01/01/2020 au 31/12/2020
	 * 01/01/2021 au 31/12/2021
	 */
	public void testCheckExercices2() {
		//1. on pose lee postulat de depart
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setFinExerciceFiscal("31-12");
		espaceOrganisationEntity.setDateActionCreation(java.util.Date.from(LocalDate.parse("03-09-2017", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		localDateNowBuilder.setClock(Clock.fixed(LocalDate.of(2021, 06, 15).atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault()));

		when(espaceOrganisationRepository.findOne(Mockito.anyLong())).thenReturn(espaceOrganisationEntity);
		//2. on appelle la methode a tester
		List<ExerciceComptableSimpleDto> dtos = this.exerciceComptableService.initExerciceComptable(Mockito.anyLong());
		//3. on verifie la veracite des resultats

		assertThat(dtos.size()).isEqualTo(5);
		
		assertThat(dtos.get(0).getDateDebut()).isEqualTo(LocalDate.of(2021, 01, 1));
		assertThat(dtos.get(0).getDateFin()).isEqualTo(LocalDate.of(2021, 12, 31));		

		assertThat(dtos.get(1).getDateDebut()).isEqualTo(LocalDate.of(2020, 01, 1));
		assertThat(dtos.get(1).getDateFin()).isEqualTo(LocalDate.of(2020, 12, 31));
		
		assertThat(dtos.get(2).getDateDebut()).isEqualTo(LocalDate.of(2019, 01, 01));
		assertThat(dtos.get(2).getDateFin()).isEqualTo(LocalDate.of(2019, 12, 31));
		
		assertThat(dtos.get(3).getDateDebut()).isEqualTo(LocalDate.of(2018, 01, 1));
		assertThat(dtos.get(3).getDateFin()).isEqualTo(LocalDate.of(2018, 12, 31));
		
		assertThat(dtos.get(4).getDateDebut()).isEqualTo(LocalDate.of(2017, 07, 1));
		assertThat(dtos.get(4).getDateFin()).isEqualTo(LocalDate.of(2017, 12, 31));

	}
	
	@Test
	/**
	 * Test avec date date du jour au 15/06/2019
		Exercice 1	Exercice 2	Exercice 3	Exercice 4
		01/07/2017	01/01/2018	01/01/2019	07/05/2019	
		31/07/2017	31/12/2018	06/05/2019	31/12/2019
	 */
	public void testfindAllBasicByEspaceOrganisationId() {
	    DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
		//1. on pose lee postulat de depart
		localDateNowBuilder.setClock(Clock.fixed(LocalDate.of(2019, 06, 15).atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault()));
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(1L);
		PublicationEntity publi = new PublicationEntity();
		publi.setCreationDate(java.util.Date.from(LocalDate.parse("01-01-2018", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		publi.setStatut(StatutPublicationEnum.PUBLIEE);
		espaceOrganisationEntity.setExercicesComptable(this.genereListeExercice1(espaceOrganisationEntity));
		
		espaceOrganisationEntity.setFinExerciceFiscal("31-12");
		when(espaceOrganisationRepository.findOne(Mockito.anyLong())).thenReturn(espaceOrganisationEntity);
        when(desinscriptionRepository.findByEspaceOrganisation(Mockito.anyObject())).thenReturn(desinscriptionEntity);
		when(publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(espaceOrganisationEntity.getId(),StatutPublicationEnum.DEPUBLIEE)).thenReturn(Optional.of(publi));
		//2. on appelle la methode a tester
		List<ExerciceComptableSimpleDto> dtos = this.exerciceComptableService.findAllBasicByEspaceOrganisationId(espaceOrganisationEntity.getId());
		dtos.sort(Comparator.comparing(ExerciceComptableSimpleDto::getDateDebut).reversed());
		dtos.remove(0);
		//3. on verifie la veracite des resultats

		assertThat(dtos.size()).isEqualTo(4);
		
		assertThat(dtos.get(0).getDateDebut()).isEqualTo(LocalDate.of(2019, 5, 07));
		assertThat(dtos.get(0).getDateFin()).isEqualTo(LocalDate.of(2019, 12, 31));
		
		assertThat(dtos.get(1).getDateDebut()).isEqualTo(LocalDate.of(2019, 1, 01));
		assertThat(dtos.get(1).getDateFin()).isEqualTo(LocalDate.of(2019,5 , 06));
		
		assertThat(dtos.get(2).getDateDebut()).isEqualTo(LocalDate.of(2018, 01, 1));
		assertThat(dtos.get(2).getDateFin()).isEqualTo(LocalDate.of(2018, 12, 31));
		
		assertThat(dtos.get(3).getDateDebut()).isEqualTo(LocalDate.of(2017, 07, 1));
		assertThat(dtos.get(3).getDateFin()).isEqualTo(LocalDate.of(2017, 12, 31));

	}

	
	@Test
	/**
	 * Test avec date date du jour au 15/01/2020
		Exercice 1	Exercice 2	Exercice 3	Exercice 4  Exercice 5
		01/07/2017	01/01/2018	01/01/2019	07/05/2019	01/01/2020
		31/07/2017	31/12/2018	06/05/2019	31/12/2019  31/12/2020


	 */
	public void testfindAllBasicByEspaceOrganisationId2() {
		//1. on pose lee postulat de depart
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
		espaceOrganisationEntity.setExercicesComptable(this.genereListeExercice1(espaceOrganisationEntity));
		localDateNowBuilder.setClock(Clock.fixed(LocalDate.of(2020, 01, 15).atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault()));
		espaceOrganisationEntity.setFinExerciceFiscal("31-12");
		espaceOrganisationEntity.setId(1L);
		when(espaceOrganisationRepository.findOne(Mockito.anyLong())).thenReturn(espaceOrganisationEntity);
        when(desinscriptionRepository.findByEspaceOrganisation(Mockito.anyObject())).thenReturn(desinscriptionEntity);
		when(publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(espaceOrganisationEntity.getId(),StatutPublicationEnum.DEPUBLIEE)).thenReturn(Optional.of(new PublicationEntity()));
		//2. on appelle la methode a tester
		List<ExerciceComptableSimpleDto> dtos = this.exerciceComptableService.findAllBasicByEspaceOrganisationId(espaceOrganisationEntity.getId());
		dtos.sort(Comparator.comparing(ExerciceComptableSimpleDto::getDateDebut).reversed());
		//3. on verifie la veracite des resultats

		assertThat(dtos.size()).isEqualTo(5);
		assertThat(dtos.get(0).getDateDebut()).isEqualTo(LocalDate.of(2020, 1, 01));
		assertThat(dtos.get(0).getDateFin()).isEqualTo(LocalDate.of(2020, 12, 31));
		
		assertThat(dtos.get(1).getDateDebut()).isEqualTo(LocalDate.of(2019, 5, 07));
		assertThat(dtos.get(1).getDateFin()).isEqualTo(LocalDate.of(2019, 12, 31));
		
		assertThat(dtos.get(2).getDateDebut()).isEqualTo(LocalDate.of(2019, 1, 01));
		assertThat(dtos.get(2).getDateFin()).isEqualTo(LocalDate.of(2019,5 , 06));
		
		assertThat(dtos.get(3).getDateDebut()).isEqualTo(LocalDate.of(2018, 01, 1));
		assertThat(dtos.get(3).getDateFin()).isEqualTo(LocalDate.of(2018, 12, 31));
		
		assertThat(dtos.get(4).getDateDebut()).isEqualTo(LocalDate.of(2017, 07, 1));
		assertThat(dtos.get(4).getDateFin()).isEqualTo(LocalDate.of(2017, 12, 31));
		
	}
	
	
	@Test
	public void testFindAllWithDeclarationByEspaceOrganisationId2019BeforeClosureDate1() {
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_ORGANISATION_ID);
		espaceOrganisationEntity.setDateActionCreation(java.util.Date.from(LocalDate.parse("01-01-2018", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		espaceOrganisationEntity.setFinExerciceFiscal("31-03");
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity(1L);
		exerciceComptableEntity.setDateDebut(LocalDate.of(2018, 4, 1));
		exerciceComptableEntity.setDateFin(LocalDate.of(2019, 3, 31));
		exerciceComptableEntity.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptableEntity.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity.setMontantDepense(new MontantDepenseEntity(1L));
		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity(1L);
		DeclarantEntity declarantEntity = new DeclarantEntity(1L);
		declarantEntity.setNom("Bla");
		declarantEntity.setPrenom("Blo");
		activiteRepresentationInteretEntity.setCreateurDeclaration(declarantEntity);
		activiteRepresentationInteretEntity.setDomainesIntervention(new HashSet<>(Arrays.asList(new DomaineInterventionEntity(1L), new DomaineInterventionEntity(3L))));
		activiteRepresentationInteretEntity.setExerciceComptable(exerciceComptableEntity);
		activiteRepresentationInteretEntity.setStatut(StatutPublicationEnum.DEPUBLIEE);
		exerciceComptableEntity.getActivitesRepresentationInteret().add(activiteRepresentationInteretEntity);
		List<ExerciceComptableEntity> exerciceComptableEntities = new ArrayList<>();
		exerciceComptableEntities.add(exerciceComptableEntity);
		espaceOrganisationEntity.setExercicesComptable(exerciceComptableEntities);

		localDateNowBuilder.setClock(Clock.fixed(LocalDate.of(2019, 03, 01).atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault()));
		when(publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(ESPACE_ORGANISATION_ID,StatutPublicationEnum.DEPUBLIEE)).thenReturn(Optional.of(new PublicationEntity()));
		when(espaceOrganisationRepository.findOne(ESPACE_ORGANISATION_ID)).thenReturn(espaceOrganisationEntity);
		
		List<ExerciceComptableEtDeclarationDto> dtos = this.exerciceComptableService.findAllWithDeclarationByEspaceOrganisationId(ESPACE_ORGANISATION_ID);

		verify(espaceOrganisationRepository, times(1)).findOne(ESPACE_ORGANISATION_ID);
		dtos.remove(1);
		assertThat(dtos.size()).isEqualTo(1);
		assertThat(dtos.get(0).getDateDebut()).isEqualTo(LocalDate.of(2018, 4, 1));
		assertThat(dtos.get(0).getDateFin()).isEqualTo(LocalDate.of(2019, 3, 31));
		assertThat(dtos.get(0).getActiviteSimpleDto().get(0).getNomCreateur()).isEqualTo(declarantEntity.getNom() + " " + declarantEntity.getPrenom());
		assertThat(dtos.get(0).getChiffreAffaire()).isEqualTo(exerciceComptableEntity.getChiffreAffaire().getId());
		assertThat(dtos.get(0).getMontantDepense()).isEqualTo(exerciceComptableEntity.getMontantDepense().getId());
		assertThat(dtos.get(0).getActiviteSimpleDto().get(0).getDomainesIntervention())
				.containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretEntity.getDomainesIntervention().stream().map(di -> {
					return di.getLibelle();
				}).collect(Collectors.toList()));

	}

	@Test
	public void testFindAllWithDeclarationByEspaceOrganisationId2019BeforeClosureDate2() {
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_ORGANISATION_ID);
		espaceOrganisationEntity.setDateActionCreation(java.util.Date.from(LocalDate.parse("01-01-2018", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		espaceOrganisationEntity.setFinExerciceFiscal("31-03");
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity(1L);
		exerciceComptableEntity.setDateDebut(LocalDate.of(2018, 4, 1));
		exerciceComptableEntity.setDateFin(LocalDate.of(2019, 3, 31));
		exerciceComptableEntity.setEspaceOrganisation(espaceOrganisationEntity);
		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity(1L);
		DeclarantEntity declarantEntity = new DeclarantEntity(1L);
		declarantEntity.setNom("Bla");
		declarantEntity.setPrenom("Blo");
		activiteRepresentationInteretEntity.setCreateurDeclaration(declarantEntity);
		activiteRepresentationInteretEntity.setStatut(StatutPublicationEnum.DEPUBLIEE);
		activiteRepresentationInteretEntity.setDomainesIntervention(new HashSet<>(Arrays.asList(new DomaineInterventionEntity(1L), new DomaineInterventionEntity(3L))));
		activiteRepresentationInteretEntity.setExerciceComptable(exerciceComptableEntity);
		exerciceComptableEntity.getActivitesRepresentationInteret().add(activiteRepresentationInteretEntity);
		List<ExerciceComptableEntity> exerciceComptableEntities = new ArrayList<>();
		exerciceComptableEntities.add(exerciceComptableEntity);
		espaceOrganisationEntity.setExercicesComptable(exerciceComptableEntities);

		localDateNowBuilder.setClock(Clock.fixed(LocalDate.of(2019, 03, 01).atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault()));
		when(publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(ESPACE_ORGANISATION_ID,StatutPublicationEnum.DEPUBLIEE)).thenReturn(Optional.of(new PublicationEntity()));
		when(espaceOrganisationRepository.findOne(ESPACE_ORGANISATION_ID)).thenReturn(espaceOrganisationEntity);
		List<ExerciceComptableEtDeclarationDto> dtos = this.exerciceComptableService.findAllWithDeclarationByEspaceOrganisationId(ESPACE_ORGANISATION_ID);

		verify(espaceOrganisationRepository, times(1)).findOne(ESPACE_ORGANISATION_ID);
        dtos.remove(1);

		assertThat(dtos.size()).isEqualTo(1);
		assertThat(dtos.get(0).getDateDebut()).isEqualTo(LocalDate.of(2018, 4, 1));
		assertThat(dtos.get(0).getDateFin()).isEqualTo(LocalDate.of(2019, 3, 31));
		assertThat(dtos.get(0).getActiviteSimpleDto().get(0).getNomCreateur()).isEqualTo(declarantEntity.getNom() + " " + declarantEntity.getPrenom());
		assertThat(dtos.get(0).getChiffreAffaire()).isNull();
		assertThat(dtos.get(0).getMontantDepense()).isNull();
		assertThat(dtos.get(0).getActiviteSimpleDto().get(0).getDomainesIntervention())
				.containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretEntity.getDomainesIntervention().stream().map(di -> {
					return di.getLibelle();
				}).collect(Collectors.toList()));

	}



	@Test
	public void testUpdateExerciceComptableWithoutExercice() {
		Long commonId = 1L;

		try {
			this.exerciceComptableService.updateExerciceComptable(commonId, new ExerciceComptableSimpleDto(), commonId);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.EXERCICE_COMPTABLE_INEXISTANT.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	@Test
	public void testUpdateExerciceComptableWithBadEspace() {
		Long commonId = 1L;
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity();
		exerciceComptableEntity.setEspaceOrganisation(new EspaceOrganisationEntity(commonId + 1L));

		when(exerciceComptableRepository.findOne(Mockito.anyLong())).thenReturn(exerciceComptableEntity);

		try {
			this.exerciceComptableService.updateExerciceComptable(commonId, new ExerciceComptableSimpleDto(), commonId);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.MAUVAIS_ESPACE.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	@Test
	public void testUpdateExerciceComptableWithoutCA() {
		Long commonId = 1L;
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity();
		exerciceComptableEntity.setEspaceOrganisation(new EspaceOrganisationEntity(commonId));

		when(exerciceComptableRepository.findOne(Mockito.anyLong())).thenReturn(exerciceComptableEntity);
		try {
			this.exerciceComptableService.updateExerciceComptable(commonId, new ExerciceComptableSimpleDto(), commonId);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.PAS_DE_RENSEIGNEMENT_CA.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	@Test
	public void testUpdateExerciceComptableSimple() {
		Long commonId = 1L;
		ExerciceComptableSimpleDto exerciceComptableSimpleDto = new ExerciceComptableSimpleDto();
		exerciceComptableSimpleDto.setHasNotChiffreAffaire(Boolean.TRUE);
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity();
		exerciceComptableEntity.setEspaceOrganisation(new EspaceOrganisationEntity(commonId));

		when(exerciceComptableRepository.findOne(commonId)).thenReturn(exerciceComptableEntity);
		when(exerciceComptableRepository.save(exerciceComptableEntity)).thenReturn(exerciceComptableEntity);

		ExerciceComptableSimpleDto dto = this.exerciceComptableService.updateExerciceComptable(commonId, exerciceComptableSimpleDto, commonId);

		verify(exerciceComptableRepository, times(1)).findOne(Mockito.anyLong());
		verify(exerciceComptableRepository, times(1)).save(Mockito.any(ExerciceComptableEntity.class));

		assertThat(dto.getHasNotChiffreAffaire()).isEqualTo(exerciceComptableSimpleDto.getHasNotChiffreAffaire());

	}

	@Test
	public void testUpdateExerciceComptableAdv() {
		Long commonId = 1L;
		ExerciceComptableSimpleDto exerciceComptableSimpleDto = new ExerciceComptableSimpleDto();
		exerciceComptableSimpleDto.setChiffreAffaire(1L);
		exerciceComptableSimpleDto.setMontantDepense(1L);
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity();
		exerciceComptableEntity.setEspaceOrganisation(new EspaceOrganisationEntity(commonId));

		when(exerciceComptableRepository.findOne(commonId)).thenReturn(exerciceComptableEntity);
		when(exerciceComptableRepository.save(exerciceComptableEntity)).thenReturn(exerciceComptableEntity);
		when(chiffreAffaireRepository.getOne(exerciceComptableSimpleDto.getChiffreAffaire())).thenReturn(new ChiffreAffaireEntity(exerciceComptableSimpleDto.getChiffreAffaire()));
		when(montantDepenseRepository.getOne(exerciceComptableSimpleDto.getMontantDepense())).thenReturn(new MontantDepenseEntity(exerciceComptableSimpleDto.getMontantDepense()));

		ExerciceComptableSimpleDto dto = this.exerciceComptableService.updateExerciceComptable(commonId, exerciceComptableSimpleDto, commonId);

		verify(exerciceComptableRepository, times(1)).findOne(Mockito.anyLong());
		verify(exerciceComptableRepository, times(1)).save(Mockito.any(ExerciceComptableEntity.class));
		verify(chiffreAffaireRepository, times(1)).getOne(Mockito.anyLong());
		verify(montantDepenseRepository, times(1)).getOne(Mockito.anyLong());

		assertThat(dto.getChiffreAffaire()).isEqualTo(exerciceComptableSimpleDto.getChiffreAffaire());
		assertThat(dto.getMontantDepense()).isEqualTo(exerciceComptableSimpleDto.getMontantDepense());

	}

	@Test
	public void testupdateDatesExerciceComptable() {
		ExerciceComptableDatesDto dto = new ExerciceComptableDatesDto();
		Long espaceOrganisationId = 1L;
		Long exerciceId = 1L;
		dto.setDateDebut(LocalDate.of(2018, 4, 1));
		dto.setDateFin(LocalDate.of(2019, 3, 31));
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity();
		exerciceComptableEntity.setEspaceOrganisation(new EspaceOrganisationEntity(espaceOrganisationId));

		when(exerciceComptableRepository.findOne(exerciceId)).thenReturn(exerciceComptableEntity);
		when(exerciceComptableRepository.save(exerciceComptableEntity)).thenReturn(exerciceComptableEntity);
		ExerciceComptableSimpleDto dtoSimple = exerciceComptableService.updateDatesExerciceComptable(exerciceId, dto, espaceOrganisationId);
		assertThat(dtoSimple.getDateDebut()).isEqualTo(dto.getDateDebut());
		assertThat(dtoSimple.getDateFin()).isEqualTo(dto.getDateFin());
	}
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#updateDatesExerciceComptable(Long exerciceId, ExerciceComptableDatesDto exerciceComptable, Long espaceOrganisationId)}
	 *  cas date début> date fin
	 */
	@Test
	public void testupdateDatesExerciceComptableEntityNull() {
		ExerciceComptableDatesDto dto = new ExerciceComptableDatesDto();
		Long espaceOrganisationId = 1L;
		Long exerciceId = 1L;
		dto.setDateDebut(LocalDate.of(2019, 4, 1));
		dto.setDateFin(LocalDate.of(2018, 3, 31));
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity();
		exerciceComptableEntity.setEspaceOrganisation(new EspaceOrganisationEntity(espaceOrganisationId));

		when(exerciceComptableRepository.findOne(exerciceId)).thenReturn(exerciceComptableEntity);
		
		try {
			ExerciceComptableSimpleDto retourDtoSimple = exerciceComptableService.updateDatesExerciceComptable(exerciceId, dto, espaceOrganisationId);
		  }catch(BusinessGlobalException aExp){
		    assertThat(ErrorMessageEnum.DATE_EXERCICE_ERROR.getMessage()).isEqualTo(aExp.getMessage());
		  }
	}
	

	@Test
	public void testBuildPdf() {
		Long exerciceId = new Random().nextLong();
		Long currentEspaceOrganisationId = new Random().nextLong();
		Long activiteId = ThreadLocalRandom.current().nextLong(0, Long.MAX_VALUE);
		Long actionId = new Random().nextLong();

		ExerciceComptableEntity exerciceComptable = new ExerciceComptableEntity(exerciceId);
		exerciceComptable.setEspaceOrganisation(new EspaceOrganisationEntity(currentEspaceOrganisationId));
		exerciceComptable.getEspaceOrganisation().setNomUsage("nom d'usage");
		exerciceComptable.setDateDebut(LocalDate.of(2017, 01, 01));
		exerciceComptable.setDateFin(LocalDate.of(2017, 12, 31));
		exerciceComptable.setChiffreAffaire(new ChiffreAffaireEntity(2L));
		exerciceComptable.setHasNotChiffreAffaire(Boolean.FALSE);
		exerciceComptable.setMontantDepense(new MontantDepenseEntity(3L));
		exerciceComptable.setNombreSalaries(25);
		ActiviteRepresentationInteretEntity activiteRepresentationInteret = new ActiviteRepresentationInteretEntity(activiteId);
		activiteRepresentationInteret.setIdFiche(new HumanReadableUniqueIdUtil("HATVP fiche", 8, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").encode(activiteId));
		activiteRepresentationInteret.setObjet("");
		activiteRepresentationInteret.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		HashSet<DomaineInterventionEntity> dis = new HashSet<>();
		DomaineInterventionEntity di = new DomaineInterventionEntity(1L);
		di.setIdCategorie(1L);
		di.setCategorie("Cat");
		di.setLibelle("Lib");
		dis.add(di);
		activiteRepresentationInteret.setDomainesIntervention(dis);
		ActionRepresentationInteretEntity actionRepresentationInteret = new ActionRepresentationInteretEntity(actionId);
		actionRepresentationInteret.setTiers(Arrays.asList(new OrganisationEntity()));
		ResponsablePublicEntity rp = new ResponsablePublicEntity(1L);
		rp.setIdCategorie(1L);
		rp.setCategorie("Cat");
		rp.setLibelle("Lib");
		actionRepresentationInteret.setReponsablesPublics(new HashSet<ResponsablePublicEntity>(Arrays.asList(rp)));
		actionRepresentationInteret.setResponsablePublicAutre("RS_autre");
		DecisionConcerneeEntity dc = new DecisionConcerneeEntity(1L);
		dc.setLibelle("Lib");
		actionRepresentationInteret.setDecisionsConcernees(new HashSet<DecisionConcerneeEntity>(Arrays.asList(dc)));
		ActionMeneeEntity am = new ActionMeneeEntity(1L);
		am.setLibelle("Lib");
		actionRepresentationInteret.setActionsMenees(new HashSet<ActionMeneeEntity>(Arrays.asList(am)));
		actionRepresentationInteret.setActionMeneeAutre("AM_autre");
		actionRepresentationInteret.setObservation("");
		activiteRepresentationInteret.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteret));
		exerciceComptable.getActivitesRepresentationInteret().add(activiteRepresentationInteret);

		Boolean withMoyens = Boolean.TRUE;
		List<StatutPublicationEnum> statuts = Arrays.asList(StatutPublicationEnum.NON_PUBLIEE);

		ArgumentCaptor<HashMap> argumentCaptor = ArgumentCaptor.forClass(HashMap.class);
		// ArgumentCaptor<String> argumentCaptor2 = ArgumentCaptor.forClass(String.class);

		when(exerciceComptableRepository.findOne(exerciceId)).thenReturn(exerciceComptable);

		// when(templateEngine.process(Mockito.anyString(), Mockito.any(Context.class))).thenAnswer(answer -> {
		// return templateEngine.process(Mockito.anyString(), argumentCaptor.getValue());
		// });

		Pair<byte[], String> pdf = exerciceComptableService.buildPdf(exerciceId, withMoyens, statuts, currentEspaceOrganisationId);

		verify(pdfGeneratorUtil, times(1)).createPdf(Mockito.anyString(), argumentCaptor.capture());

		Map<String, Object> contextData = argumentCaptor.getAllValues().get(0);
		assertThat(contextData.keySet().size()).isEqualTo(3);
		assertThat(contextData.size()).isEqualTo(3);
		assertThat(contextData.get("exercice")).isNotNull();
		assertThat(contextData.get("exercice")).isInstanceOf(RapportActivitePdfTemplateData.class);

		RapportActivitePdfTemplateData pdfData = (RapportActivitePdfTemplateData) contextData.get("exercice");
		assertThat(pdfData.getChiffreAffaire()).isEqualTo(exerciceComptable.getChiffreAffaire().getLibelle());
		assertThat(pdfData.getMontantDepense()).isEqualTo(exerciceComptable.getMontantDepense().getLibelle());
		assertThat(pdfData.getNombreSalaries()).isEqualTo(exerciceComptable.getNombreSalaries().toString());
		assertThat(pdfData.getDateDebut()).isEqualTo(exerciceComptable.getDateDebut().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
		assertThat(pdfData.getDateFin()).isEqualTo(exerciceComptable.getDateFin().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
		assertThat(pdfData.getHasNotChiffreAffaire()).isEqualTo(exerciceComptable.getHasNotChiffreAffaire());
		assertThat(pdfData.getActivites().size()).isEqualTo(exerciceComptable.getActivitesRepresentationInteret().size());
		assertThat(pdfData.getActivites().get(0).getIdFiche()).isEqualTo(exerciceComptable.getActivitesRepresentationInteret().get(0).getIdFiche());
		assertThat(new HumanReadableUniqueIdUtil("HATVP fiche", 8, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").decode(pdfData.getActivites().get(0).getIdFiche()))
				.isEqualTo(exerciceComptable.getActivitesRepresentationInteret().get(0).getId());
		assertThat(pdfData.getActivites().get(0).getObjet()).isEqualTo(exerciceComptable.getActivitesRepresentationInteret().get(0).getObjet());
		assertThat(pdfData.getActivites().get(0).getDomaines().size()).isEqualTo(exerciceComptable.getActivitesRepresentationInteret().get(0).getDomainesIntervention().size());
		assertThat(pdfData.getActivites().get(0).getStatut()).isEqualTo(exerciceComptable.getActivitesRepresentationInteret().get(0).getStatut().getLabel());
		assertThat(pdfData.getActivites().get(0).getActions().get(0).getTiers().size())
				.isEqualTo(exerciceComptable.getActivitesRepresentationInteret().get(0).getActionsRepresentationInteret().get(0).getTiers().size());
		assertThat(pdfData.getActivites().get(0).getActions().get(0).getReponsablesPublics().size())
				.isEqualTo(exerciceComptable.getActivitesRepresentationInteret().get(0).getActionsRepresentationInteret().get(0).getReponsablesPublics().size());
		assertThat(pdfData.getActivites().get(0).getActions().get(0).getResponsablePublicAutre())
				.isEqualTo(exerciceComptable.getActivitesRepresentationInteret().get(0).getActionsRepresentationInteret().get(0).getResponsablePublicAutre());
		assertThat(pdfData.getActivites().get(0).getActions().get(0).getDecisionsConcernees().size())
				.isEqualTo(exerciceComptable.getActivitesRepresentationInteret().get(0).getActionsRepresentationInteret().get(0).getDecisionsConcernees().size());
		assertThat(pdfData.getActivites().get(0).getActions().get(0).getActionsMenees().size())
				.isEqualTo(exerciceComptable.getActivitesRepresentationInteret().get(0).getActionsRepresentationInteret().get(0).getActionsMenees().size());
		assertThat(pdfData.getActivites().get(0).getActions().get(0).getActionMeneeAutre())
				.isEqualTo(exerciceComptable.getActivitesRepresentationInteret().get(0).getActionsRepresentationInteret().get(0).getActionMeneeAutre());

		assertThat(pdf.getRight()).isEqualTo("agora-export-periode-du-" + exerciceComptable.getDateDebut().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "-au-"
				+ exerciceComptable.getDateFin().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "-le-"
				+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy-HH-mm")));
	}
	
	private List<ExerciceComptableEntity> genereListeExercice1( EspaceOrganisationEntity espaceOrganisationEntity){
		List<ExerciceComptableEntity> liste = new ArrayList<ExerciceComptableEntity>();
		ExerciceComptableEntity ex1 = new ExerciceComptableEntity();
		ex1.setEspaceOrganisation(espaceOrganisationEntity);
		ex1.setDateDebut(LocalDate.of(2017, 07, 1));
		ex1.setDateFin(LocalDate.of(2017, 12, 31));
		liste.add(ex1);
		ExerciceComptableEntity ex2 = new ExerciceComptableEntity();
		ex2.setEspaceOrganisation(espaceOrganisationEntity);
		ex2.setDateDebut(LocalDate.of(2018, 01, 1));
		ex2.setDateFin(LocalDate.of(2018, 12, 31));
		liste.add(ex2);
		ExerciceComptableEntity ex3 = new ExerciceComptableEntity();
		ex3.setEspaceOrganisation(espaceOrganisationEntity);
		ex3.setDateDebut(LocalDate.of(2019, 01, 1));
		ex3.setDateFin(LocalDate.of(2019, 05, 6));
		liste.add(ex3);
		ExerciceComptableEntity ex4 = new ExerciceComptableEntity();
		ex4.setEspaceOrganisation(espaceOrganisationEntity);
		ex4.setDateDebut(LocalDate.of(2019, 05, 7));
		ex4.setDateFin(LocalDate.of(2019, 12, 31));
		liste.add(ex4);
		return liste;
	}
	
	/**
	 * test les espace_organisation qui 
	 * 		n'ont ni publié d'activité ni de moyen
	 * 		ont publié à date < date cessation
	 */
	@Test
	public void testHasPublishedAfterCessationDatePasDePublicationActivitePasPublicationMoyen() {
		
		LocalDate localDateCessation = LocalDate.of(2020, 06, 15);
		Date dateCessation = Date.from(localDateCessation.atStartOfDay(ZoneId.systemDefault()).toInstant());
		
		when(activiteRepresentationInteretRepository.findActivitePublieeApresDateCessation(dateCessation, ESPACE_ORGANISATION_ID)).thenReturn(null);
		when(exerciceComptableRepository.findFirstByEspaceOrganisationIdAndDateDebutAfterAndStatutIn(ESPACE_ORGANISATION_ID, localDateCessation, LIST_STATUT_PUBLICATION_PUBLIE)).thenReturn(null);
		
		Boolean retour = this.exerciceComptableService.hasPublishedAfterCessationDate(ESPACE_ORGANISATION_ID, localDateCessation);
		
		Assert.assertEquals(false, retour);
	}
	
	/**
	 * test les espace_organisation qui n'ont 
	 * 		pas publié d'activité
	 *		publié des moyens
	 * 		date de publication moyen > date cessation
	 */
	@Test
	public void testHasPublishedAfterCessationDatePasDePublicationActiviteAvecPublicationMoyenDateCessationAfter() {
		
		LocalDate localDateCessation = LocalDate.of(2020, 06, 15);
		Date dateCessation = Date.from(localDateCessation.atStartOfDay(ZoneId.systemDefault()).toInstant());
			
		ExerciceComptableEntity exerciceComptableEntityPublie = new ExerciceComptableEntity(1L);

		
		when(activiteRepresentationInteretRepository.findActivitePublieeApresDateCessation(dateCessation, ESPACE_ORGANISATION_ID)).thenReturn(null);
		when(exerciceComptableRepository.findFirstByEspaceOrganisationIdAndDateDebutAfterAndStatutIn(ESPACE_ORGANISATION_ID, localDateCessation, LIST_STATUT_PUBLICATION_PUBLIE)).thenReturn(exerciceComptableEntityPublie);
		
		Boolean retour = this.exerciceComptableService.hasPublishedAfterCessationDate(ESPACE_ORGANISATION_ID, localDateCessation);
		
		Assert.assertEquals(true, retour);
		
	}
	
	/**
	 * test les espace_organisation qui ont 
	 * 		publié 1 ou plusierus activité
	 *		pas publié des moyens
	 * 		date de publication activité > date cessation
	 */
	@Test
	public void testHasPublishedAfterCessationDateDateCessationBefore() {
		
		LocalDate localDateCessation = LocalDate.of(2020, 06, 15);
		Date dateCessation = Date.from(localDateCessation.atStartOfDay(ZoneId.systemDefault()).toInstant());
		
		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntityPublie = new ActiviteRepresentationInteretEntity(1L);

		
		when(activiteRepresentationInteretRepository.findActivitePublieeApresDateCessation(dateCessation, ESPACE_ORGANISATION_ID)).thenReturn(activiteRepresentationInteretEntityPublie);
		when(exerciceComptableRepository.findFirstByEspaceOrganisationIdAndDateDebutAfterAndStatutIn(ESPACE_ORGANISATION_ID, localDateCessation, LIST_STATUT_PUBLICATION_PUBLIE)).thenReturn(null);
		
		Boolean retour = this.exerciceComptableService.hasPublishedAfterCessationDate(ESPACE_ORGANISATION_ID, localDateCessation);
		
		Assert.assertEquals(true, retour);
		
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#findAllBasicByEspaceOrganisationId(final Long currentEspaceOrganisationId)}
	 *  cas espaceOrganisationEntity pas trouvée 
	 */
	@Test
	public void testfindAllBasicByEspaceOrganisationIdEntityNull() {
	    EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_ORGANISATION_ID);

		when(publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(espaceOrganisationEntity.getId(),StatutPublicationEnum.DEPUBLIEE)).thenReturn(Optional.empty());
		
		try {
			List<ExerciceComptableSimpleDto> retourDtos = this.exerciceComptableService.findAllBasicByEspaceOrganisationId(espaceOrganisationEntity.getId());
		    fail("Should throw exception no entity found");
		  }catch(BusinessGlobalException aExp){
			  assertThat(ErrorMessageEnum.ACCES_ACTIVITE_NO_PUBLICATION.getMessage()).isEqualTo(aExp.getMessage());
		  }
	}
	
	@Test
	@Ignore
	/**
	 * Test avec date date du jour au 
		Exercice 1	Exercice 2	Exercice 3	Exercice 4
		01/07/2017	01/01/2018	01/01/2019	07/05/2019	
		31/07/2017	31/12/2018	06/05/2019	31/12/2019
		avec Désinscription && dateCourante(15/03/2020)>lastExercice.DateFin && dateCourante<CessationDate(30/06/2020)
		
		parametrage date du jour ne fonctionne pas
	 */
	public void testfindAllBasicByEspaceOrganisationIdAvecDesinscriptionDateCouranteBeforeCessationDate() {
	    DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
	    desinscriptionEntity.setId(11L);
	    desinscriptionEntity.setCessationDate(LocalDate.of(2020, 06, 30));
		//1. on pose lee postulat de depart
		localDateNowBuilder.setClock(Clock.fixed(LocalDate.of(2020, 03, 15).atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault()));

		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(1L);
		
		PublicationEntity publi = new PublicationEntity();
		publi.setCreationDate(java.util.Date.from(LocalDate.parse("01-01-2018", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		publi.setStatut(StatutPublicationEnum.PUBLIEE);
		
		espaceOrganisationEntity.setFinExerciceFiscal("31-12");
		espaceOrganisationEntity.setExercicesComptable(this.genereListeExercice1(espaceOrganisationEntity));		
		
		when(espaceOrganisationRepository.findOne(Mockito.anyLong())).thenReturn(espaceOrganisationEntity);
        when(desinscriptionRepository.findByEspaceOrganisation(Mockito.anyObject())).thenReturn(desinscriptionEntity);
		when(publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(espaceOrganisationEntity.getId(),StatutPublicationEnum.DEPUBLIEE)).thenReturn(Optional.of(publi));
		//2. on appelle la methode a tester
		List<ExerciceComptableSimpleDto> dtos = this.exerciceComptableService.findAllBasicByEspaceOrganisationId(espaceOrganisationEntity.getId());
		
		dtos.sort(Comparator.comparing(ExerciceComptableSimpleDto::getDateDebut).reversed());
		dtos.remove(0);
		//3. on verifie la veracite des resultats

		assertThat(dtos.size()).isEqualTo(4);
		
		assertThat(dtos.get(0).getDateDebut()).isEqualTo(LocalDate.of(2019, 5, 07));
		assertThat(dtos.get(0).getDateFin()).isEqualTo(LocalDate.of(2019, 12, 31));
		
		assertThat(dtos.get(1).getDateDebut()).isEqualTo(LocalDate.of(2019, 1, 01));
		assertThat(dtos.get(1).getDateFin()).isEqualTo(LocalDate.of(2019,5 , 06));
		
		assertThat(dtos.get(2).getDateDebut()).isEqualTo(LocalDate.of(2018, 01, 1));
		assertThat(dtos.get(2).getDateFin()).isEqualTo(LocalDate.of(2018, 12, 31));
		
		assertThat(dtos.get(3).getDateDebut()).isEqualTo(LocalDate.of(2017, 07, 1));
		assertThat(dtos.get(3).getDateFin()).isEqualTo(LocalDate.of(2017, 12, 31));

	}
	
	
	@Test
	/**
	 * Test avec date date du jour au 15/03/2020
		Exercice 1	Exercice 2	Exercice 3	Exercice 4
		01/07/2017	01/01/2018	01/01/2019	07/05/2019	
		31/07/2017	31/12/2018	06/05/2019	31/12/2019
		avec Désinscription && dateCourante(15/03/2020)>lastExercice.DateFin && dateCourante>CessationDate(12/01/2020)
	 */
	public void testfindAllBasicByEspaceOrganisationIdAvecDesinscriptionDateCouranteAfterCessationDate() {
	    DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
	    desinscriptionEntity.setId(11L);
	    desinscriptionEntity.setCessationDate(LocalDate.of(2020, 01, 12));
		//1. on pose lee postulat de depart
		localDateNowBuilder.setClock(Clock.fixed(LocalDate.of(2020, 03, 15).atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault()));
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(1L);
		PublicationEntity publi = new PublicationEntity();
		publi.setCreationDate(java.util.Date.from(LocalDate.parse("01-01-2018", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		publi.setStatut(StatutPublicationEnum.PUBLIEE);
		espaceOrganisationEntity.setExercicesComptable(this.genereListeExercice1(espaceOrganisationEntity));
		
		espaceOrganisationEntity.setFinExerciceFiscal("31-12");
		when(espaceOrganisationRepository.findOne(Mockito.anyLong())).thenReturn(espaceOrganisationEntity);
        when(desinscriptionRepository.findByEspaceOrganisation(Mockito.anyObject())).thenReturn(desinscriptionEntity);
		when(publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(espaceOrganisationEntity.getId(),StatutPublicationEnum.DEPUBLIEE)).thenReturn(Optional.of(publi));
		//2. on appelle la methode a tester
		List<ExerciceComptableSimpleDto> dtos = this.exerciceComptableService.findAllBasicByEspaceOrganisationId(espaceOrganisationEntity.getId());
		dtos.sort(Comparator.comparing(ExerciceComptableSimpleDto::getDateDebut).reversed());
		dtos.remove(0);
		//3. on verifie la veracite des resultats

		assertThat(dtos.size()).isEqualTo(3);
		
		assertThat(dtos.get(0).getDateDebut()).isEqualTo(LocalDate.of(2019, 1, 01));
		assertThat(dtos.get(0).getDateFin()).isEqualTo(LocalDate.of(2019,5 , 06));
		
		assertThat(dtos.get(1).getDateDebut()).isEqualTo(LocalDate.of(2018, 01, 1));
		assertThat(dtos.get(1).getDateFin()).isEqualTo(LocalDate.of(2018, 12, 31));
		
		assertThat(dtos.get(2).getDateDebut()).isEqualTo(LocalDate.of(2017, 07, 1));
		assertThat(dtos.get(2).getDateFin()).isEqualTo(LocalDate.of(2017, 12, 31));
	}
	
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#findAllWithDeclarationByEspaceOrganisationId(final Long currentEspaceOrganisationId)}
	 *  cas espaceOrganisationEntity pas trouvée
	 */
	@Test
	public void testFindAllWithDeclarationByEspaceOrganisationIddEntityNull() {
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_ORGANISATION_ID);
		espaceOrganisationEntity.setDateActionCreation(java.util.Date.from(LocalDate.parse("01-01-2018", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		espaceOrganisationEntity.setFinExerciceFiscal("31-03");
		
		when(publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(espaceOrganisationEntity.getId(),StatutPublicationEnum.DEPUBLIEE)).thenReturn(Optional.empty());
		
		try {
			List<ExerciceComptableEtDeclarationDto> retourDtos = this.exerciceComptableService.findAllWithDeclarationByEspaceOrganisationId(ESPACE_ORGANISATION_ID);
		  }catch(BusinessGlobalException aExp){
		    assertThat(ErrorMessageEnum.ACCES_ACTIVITE_NO_PUBLICATION.getMessage()).isEqualTo(aExp.getMessage());
		  }
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#getExerciceWithoutDeclarationByDate(LocalDate today)}
	 * cas avec déjà publication moyen et activité => aucunePublicationActivites & aucunePublicationMoyens = false
	 * cas aucunePublicationActivites ou aucunePublicationMoyens = true
	 */
	@Test
	public void testGetExerciceWithoutDeclarationByDateAvecPublicationMoyenEtActivite() {
		LocalDate today = LocalDate.of(2020, 3, 15);
		
		List<ExerciceComptableEntity> exerciceComptableEntityList = new ArrayList<ExerciceComptableEntity>();
		
		//cas aucunePublicationActivites & aucunePublicationMoyens = false
		ExerciceComptableEntity exerciceComptableEntityAvecPubli = new ExerciceComptableEntity(1L);
		exerciceComptableEntityAvecPubli.setDateDebut(LocalDate.of(2018, 4, 1));
		exerciceComptableEntityAvecPubli.setDateFin(LocalDate.of(2019, 3, 31));
		exerciceComptableEntityAvecPubli.setEspaceOrganisation(null);
		exerciceComptableEntityAvecPubli.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntityAvecPubli.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntityAvecPubli.setStatut(StatutPublicationEnum.PUBLIEE);
		exerciceComptableEntityAvecPubli.setNoActivite(false);
		
		List<ActiviteRepresentationInteretEntity> listActiviteRepresentationInteret = new ArrayList<ActiviteRepresentationInteretEntity>();
		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity();
		activiteRepresentationInteretEntity.setStatut(StatutPublicationEnum.PUBLIEE);
		
		listActiviteRepresentationInteret.add(activiteRepresentationInteretEntity);
		
		exerciceComptableEntityAvecPubli.setActivitesRepresentationInteret(listActiviteRepresentationInteret);
		
		exerciceComptableEntityList.add(exerciceComptableEntityAvecPubli);
		
		
		//on force monthAfterCloture & dayBeforeCloture1
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "monthAfterCloture", 4L);
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayBeforeCloture1", 29L);
        
		Mockito.doReturn(exerciceComptableEntityList).when(this.exerciceComptableRepository).findLastExerciceForEachEspaceByDateFinBefore(any());
		Mockito.doReturn(exerciceComptableEntityAvecPubli).when(this.exerciceComptableRepository).save(exerciceComptableEntityAvecPubli);

		List<DeclarationReminderDto> retourDeclaReminderDto = this.exerciceComptableService.getExerciceWithoutDeclarationByDate(today);
		
		Assert.assertTrue(retourDeclaReminderDto.isEmpty());		
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#needRelance(ExerciceComptableEntity exerciceComptableEntity, LocalDate today)}
	 */
	@Test
	public void testNeedRelanceTrue() {  
		LocalDate today = LocalDate.of(2020, 3, 30);		

		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity(1L);
		exerciceComptableEntity.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity.setDateFin(LocalDate.of(2020, 1, 1));
		exerciceComptableEntity.setEspaceOrganisation(null);
		exerciceComptableEntity.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity.setNoActivite(false);		
		
		//on force monthAfterCloture & dayBeforeCloture1
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "monthAfterCloture", 3L);
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayBeforeCloture1", 15L);
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayBeforeCloture2", 0L);
		
		Boolean retour =  this.exerciceComptableService.needRelance(exerciceComptableEntity, today);
		
		Assert.assertTrue(retour);
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#needRelance(ExerciceComptableEntity exerciceComptableEntity, LocalDate today)}
	 */
	@Test
	public void testNeedRelanceFalse() {  
		LocalDate today = LocalDate.of(2020, 3, 30);		

		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity(1L);
		exerciceComptableEntity.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptableEntity.setEspaceOrganisation(null);
		exerciceComptableEntity.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity.setNoActivite(false);		
		
		//on force monthAfterCloture & dayBeforeCloture1
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "monthAfterCloture", 3L);
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayBeforeCloture1", 15L);
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayBeforeCloture2", 0L);
		
		Boolean retour =  this.exerciceComptableService.needRelance(exerciceComptableEntity, today);
		
		Assert.assertFalse(retour);
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#needRelance(ExerciceComptableEntity exerciceComptableEntity, LocalDate today)}
	 */
	@Test
	public void getMinDate() {
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity(1L);
		exerciceComptableEntity.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptableEntity.setEspaceOrganisation(null);
		exerciceComptableEntity.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity.setNoActivite(false);
		
		List<PublicationRelanceEntity> listPublicationRelanceEntity = new ArrayList<PublicationRelanceEntity>();
		//doit ressortir
		PublicationRelanceEntity publicationRelance = new PublicationRelanceEntity();
		publicationRelance.setCreationDate(LocalDateTime.of(2019, 1, 1, 0, 0));
		publicationRelance.setNiveau(PublicationRelanceNiveauEnum.R2);		
		listPublicationRelanceEntity.add(publicationRelance);
		//doit pas ressortir car la plus récente
		PublicationRelanceEntity publicationRelance2 = new PublicationRelanceEntity();
		publicationRelance2.setCreationDate(LocalDateTime.of(2019, 3, 15, 0, 0));
		publicationRelance2.setNiveau(PublicationRelanceNiveauEnum.R2);		
		listPublicationRelanceEntity.add(publicationRelance2);		
		//doit pas ressortir car R1
		PublicationRelanceEntity publicationRelance3 = new PublicationRelanceEntity();
		publicationRelance3.setCreationDate(LocalDateTime.of(2020, 3, 15, 0, 0));
		publicationRelance3.setNiveau(PublicationRelanceNiveauEnum.R1);		
		listPublicationRelanceEntity.add(publicationRelance3);	
				
		exerciceComptableEntity.setPublicationRelance(listPublicationRelanceEntity);
		
		LocalDate retour =  this.exerciceComptableService.getMinDate(exerciceComptableEntity, PublicationRelanceNiveauEnum.R2);
		
		Assert.assertEquals(LocalDate.of(2019, 1, 1), retour);		
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#checkDateRelanceNblR2(LocalDate minDate, Long dayMinus, Long dayPlus, LocalDate today )}
	 * 30 mars - 5j + 6j
	 */
	@Test
	public void checkDateRelanceNblR2() {
		LocalDate today = LocalDate.of(2020, 3, 30);
		
		Boolean retour =  this.exerciceComptableService.checkDateRelanceNblR2(LocalDate.of(2020, 3, 28), 5L, 10L, today );
		
		Assert.assertTrue(retour);
	}

	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#getEndOfMonthAdjustment(ExerciceComptableEntity exerciceComptableEntity)}
	 * pas d'ajustement
	 */
	@Test
	public void getEndOfMonthAdjustmentNone() {
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity(1L);
		exerciceComptableEntity.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptableEntity.setEspaceOrganisation(null);
		exerciceComptableEntity.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity.setNoActivite(false);
		
		//on force monthAfterCloture & dayBeforeCloture1
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "monthAfterCloture", 3L);
        
		int retour =  this.exerciceComptableService.getEndOfMonthAdjustment(exerciceComptableEntity);
		
		Assert.assertEquals(0, retour);
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#getEndOfMonthAdjustment(ExerciceComptableEntity exerciceComptableEntity)}
	 * date fin = dernier jour du mois = 30
	 * jour date fin 30(nov) < dernier jour du mois dans monthAfterCloture mois avec date de fin (nov+4=mars => 31 le dernier jour)
	 */
	@Test
	public void getEndOfMonthAdjustmentWith() {
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity(1L);
		exerciceComptableEntity.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity.setDateFin(LocalDate.of(2019, 11, 30));
		exerciceComptableEntity.setEspaceOrganisation(null);
		exerciceComptableEntity.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity.setNoActivite(false);
		
		//on force monthAfterCloture & dayBeforeCloture1
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "monthAfterCloture", 4L);
        
		int retour =  this.exerciceComptableService.getEndOfMonthAdjustment(exerciceComptableEntity);
		
		Assert.assertEquals(1, retour);
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#hasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnum(ExerciceComptableEntity exerciceComptableEntity, PublicationRelanceNiveauEnum publicationRelanceNiveauEnum)}
	 * cas TRUE
	 */
	@Test
	public void testHasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnumTrue() {
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity(1L);
		exerciceComptableEntity.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptableEntity.setEspaceOrganisation(null);
		exerciceComptableEntity.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity.setNoActivite(false);
		
		List<PublicationRelanceEntity> listPublicationRelanceEntity = new ArrayList<PublicationRelanceEntity>();
		//doit ressortir
		PublicationRelanceEntity publicationRelance = new PublicationRelanceEntity();
		publicationRelance.setCreationDate(LocalDateTime.of(2019, 1, 1, 0, 0));
		publicationRelance.setNiveau(PublicationRelanceNiveauEnum.R2);		
		listPublicationRelanceEntity.add(publicationRelance);
		//doit pas ressortir car la plus récente
		PublicationRelanceEntity publicationRelance2 = new PublicationRelanceEntity();
		publicationRelance2.setCreationDate(LocalDateTime.of(2019, 3, 15, 0, 0));
		publicationRelance2.setNiveau(PublicationRelanceNiveauEnum.R1);
		publicationRelance2.setEnvoye(true);
		listPublicationRelanceEntity.add(publicationRelance2);
				
		exerciceComptableEntity.setPublicationRelance(listPublicationRelanceEntity);
		
		Boolean retour =  this.exerciceComptableService.hasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnum(exerciceComptableEntity, PublicationRelanceNiveauEnum.R1);
	
		Assert.assertTrue(retour);
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#hasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnum(ExerciceComptableEntity exerciceComptableEntity, PublicationRelanceNiveauEnum publicationRelanceNiveauEnum)}
	 * cas FALSE
	 */
	@Test
	public void testHasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnumFalse() {
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity(1L);
		exerciceComptableEntity.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptableEntity.setEspaceOrganisation(null);
		exerciceComptableEntity.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity.setNoActivite(false);
		
		List<PublicationRelanceEntity> listPublicationRelanceEntity = new ArrayList<PublicationRelanceEntity>();
		//doit ressortir
		PublicationRelanceEntity publicationRelance = new PublicationRelanceEntity();
		publicationRelance.setCreationDate(LocalDateTime.of(2019, 1, 1, 0, 0));
		publicationRelance.setNiveau(PublicationRelanceNiveauEnum.R2);		
		listPublicationRelanceEntity.add(publicationRelance);
		//doit pas ressortir car la plus récente
		PublicationRelanceEntity publicationRelance2 = new PublicationRelanceEntity();
		publicationRelance2.setCreationDate(LocalDateTime.of(2019, 3, 15, 0, 0));
		publicationRelance2.setNiveau(PublicationRelanceNiveauEnum.R1);
		publicationRelance2.setEnvoye(false);
		listPublicationRelanceEntity.add(publicationRelance2);
				
		exerciceComptableEntity.setPublicationRelance(listPublicationRelanceEntity);
		
		Boolean retour =  this.exerciceComptableService.hasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnum(exerciceComptableEntity, PublicationRelanceNiveauEnum.R1);
	
		Assert.assertFalse(retour);
	}
	
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#getExerciceWithoutDeclarationByDate(LocalDate today)}
	 * cas sans publication activité et avec publication moyen => aucunePublicationActivites = false && aucunePublicationMoyens = true
	 */
	@Test
	public void testGetExerciceWithoutDeclarationByDateAvecPublicationMoyenEtSansPublicationActivite() {
		LocalDate today = LocalDate.of(2020, 3, 30);		
	
		List<ExerciceComptableEntity> exerciceComptableEntityList = new ArrayList<ExerciceComptableEntity>();
		
		//cas aucunePublicationActivites & aucunePublicationMoyens = false
		//needRelance = true + listPublicationRelanceEntity not enmpty => continue
		ExerciceComptableEntity exerciceComptableEntityNonPubli = new ExerciceComptableEntity(1L);
		exerciceComptableEntityNonPubli.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntityNonPubli.setDateFin(LocalDate.of(2020, 1, 1));
		exerciceComptableEntityNonPubli.setEspaceOrganisation(null);
		exerciceComptableEntityNonPubli.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntityNonPubli.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntityNonPubli.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntityNonPubli.setNoActivite(false);		
				
		List<ActiviteRepresentationInteretEntity> listActiviteRepresentationInteret = new ArrayList<ActiviteRepresentationInteretEntity>();
		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity();
		activiteRepresentationInteretEntity.setStatut(StatutPublicationEnum.PUBLIEE);
		
		listActiviteRepresentationInteret.add(activiteRepresentationInteretEntity);		
		exerciceComptableEntityNonPubli.setActivitesRepresentationInteret(listActiviteRepresentationInteret);
		
		List<PublicationRelanceEntity> listPublicationRelanceEntity = new ArrayList<PublicationRelanceEntity>();
		PublicationRelanceEntity publicationRelanceEntity = new PublicationRelanceEntity();
		publicationRelanceEntity.setNiveau(PublicationRelanceNiveauEnum.R1);
		
		listPublicationRelanceEntity.add(publicationRelanceEntity);		
		exerciceComptableEntityNonPubli.setPublicationRelance(listPublicationRelanceEntity);
		
		exerciceComptableEntityList.add(exerciceComptableEntityNonPubli);		

		//cas aucunePublicationActivites & aucunePublicationMoyens = false
		//needRelance = false
		//hasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnum R2= true
		//checkDateRelanceNblR2=true => continue
		ExerciceComptableEntity exerciceComptableEntity2 = new ExerciceComptableEntity(2L);
		exerciceComptableEntity2.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity2.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptableEntity2.setEspaceOrganisation(null);
		exerciceComptableEntity2.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity2.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity2.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity2.setNoActivite(false);		
			
		List<PublicationRelanceEntity> listPublicationRelanceEntity2 = new ArrayList<PublicationRelanceEntity>();
		//doit ressortir
		PublicationRelanceEntity publicationRelance2a = new PublicationRelanceEntity();
		publicationRelance2a.setCreationDate(LocalDateTime.of(2020, 3, 28, 0, 0));
		publicationRelance2a.setNiveau(PublicationRelanceNiveauEnum.R1);		
		listPublicationRelanceEntity2.add(publicationRelance2a);
		//doit pas ressortir car la plus récente
		PublicationRelanceEntity publicationRelance2b = new PublicationRelanceEntity();
		publicationRelance2b.setCreationDate(LocalDateTime.of(2020, 3, 28, 0, 0));
		publicationRelance2b.setNiveau(PublicationRelanceNiveauEnum.R2);
		publicationRelance2b.setEnvoye(true);
		listPublicationRelanceEntity2.add(publicationRelance2b);
				
		exerciceComptableEntity2.setPublicationRelance(listPublicationRelanceEntity2);
		
		exerciceComptableEntityList.add(exerciceComptableEntity2);
		
		//cas aucunePublicationActivites & aucunePublicationMoyens = false
//		needRelance = false
//		hasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnum R1= true
//		checkDateRelanceNblR2=true => continue
		ExerciceComptableEntity exerciceComptableEntity3 = new ExerciceComptableEntity(3L);
		exerciceComptableEntity3.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity3.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptableEntity3.setEspaceOrganisation(null);
		exerciceComptableEntity3.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity3.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity3.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity3.setNoActivite(false);
		
		List<PublicationRelanceEntity> listPublicationRelanceEntity3 = new ArrayList<PublicationRelanceEntity>();
		//doit ressortir
		PublicationRelanceEntity publicationRelance3a = new PublicationRelanceEntity();
		publicationRelance3a.setCreationDate(LocalDateTime.of(2020, 3, 28, 0, 0));
		publicationRelance3a.setNiveau(PublicationRelanceNiveauEnum.R1);
		publicationRelance3a.setEnvoye(true);
		listPublicationRelanceEntity3.add(publicationRelance3a);
		//doit pas ressortir car la plus récente
		PublicationRelanceEntity publicationRelance3b = new PublicationRelanceEntity();
		publicationRelance3b.setCreationDate(LocalDateTime.of(2020, 3, 28, 0, 0));
		publicationRelance3b.setNiveau(PublicationRelanceNiveauEnum.R2);

		listPublicationRelanceEntity3.add(publicationRelance3b);				
				
		exerciceComptableEntity3.setPublicationRelance(listPublicationRelanceEntity3);				
		
		exerciceComptableEntityList.add(exerciceComptableEntity3);		
		
		//cas aucunePublicationActivites & aucunePublicationMoyens = false
		//passe par le else
		ExerciceComptableEntity exerciceComptableEntity4 = new ExerciceComptableEntity(4L);
		exerciceComptableEntity4.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity4.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptableEntity4.setEspaceOrganisation(null);
		exerciceComptableEntity4.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity4.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity4.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity4.setNoActivite(false);
		
		List<PublicationRelanceEntity> listPublicationRelanceEntity4 = new ArrayList<PublicationRelanceEntity>();
		//doit ressortir
		PublicationRelanceEntity publicationRelance4a = new PublicationRelanceEntity();
		publicationRelance4a.setCreationDate(LocalDateTime.of(2020, 3, 28, 0, 0));
		publicationRelance4a.setNiveau(PublicationRelanceNiveauEnum.R1);
		//publicationRelance2b.setEnvoye(true);
		listPublicationRelanceEntity4.add(publicationRelance4a);
		//doit pas ressortir car la plus récente
		PublicationRelanceEntity publicationRelance4b = new PublicationRelanceEntity();
		publicationRelance4b.setCreationDate(LocalDateTime.of(2020, 3, 28, 0, 0));
		publicationRelance4b.setNiveau(PublicationRelanceNiveauEnum.R2);

		listPublicationRelanceEntity4.add(publicationRelance4b);				
				
		exerciceComptableEntity4.setPublicationRelance(listPublicationRelanceEntity4);				
		
		exerciceComptableEntityList.add(exerciceComptableEntity4);		
		
		DeclarationReminderDto declarationReminderECNonPubliDto = new DeclarationReminderDto();

		declarationReminderECNonPubliDto.setDenomination(null);
		declarationReminderECNonPubliDto.setDateDebut(exerciceComptableEntityNonPubli.getDateDebut());
		declarationReminderECNonPubliDto.setDateFin(exerciceComptableEntityNonPubli.getDateFin());

		//on force les paramtères de config
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "monthAfterCloture", 3L);
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayBeforeCloture1", 15L);
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayBeforeCloture2", 0L);
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayAfterRelance1", 1L);
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayAfterRelance2", 5L);
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayAfterBlacklist", 10L);        
        
		Mockito.doReturn(exerciceComptableEntityList).when(this.exerciceComptableRepository).findLastExerciceForEachEspaceByDateFinBefore(any());
		Mockito.doReturn(exerciceComptableEntityNonPubli).when(this.exerciceComptableRepository).save(exerciceComptableEntityNonPubli);
		Mockito.doReturn(declarationReminderECNonPubliDto).when(this.declarationReminderTransformer).modelToDto(exerciceComptableEntityNonPubli);

		List<DeclarationReminderDto> retourDeclaReminderDto = this.exerciceComptableService.getExerciceWithoutDeclarationByDate(today);
		
		Assert.assertTrue(retourDeclaReminderDto.isEmpty());		
	}
	
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#getExerciceWithoutDeclarationByDate(LocalDate today)}
	 * cas sans publication activité et avec publication moyen => aucunePublicationActivites = false && aucunePublicationMoyens = true
	 */
	@Test
	public void testGetExerciceWithoutDeclarationByDateAvecPublicationMoyenEtSansPublicationActiviteNecessitantRelance() {
		LocalDate today = LocalDate.of(2020, 3, 30);		
	
		List<ExerciceComptableEntity> exerciceComptableEntityList = new ArrayList<ExerciceComptableEntity>();
		
		//cas aucunePublicationActivites & aucunePublicationMoyens = false
		//needRelance = true + listPublicationRelanceEntity enmpty => +1 dans liste DTO
		ExerciceComptableEntity exerciceComptableEntityNonPubli = new ExerciceComptableEntity(1L);
		exerciceComptableEntityNonPubli.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntityNonPubli.setDateFin(LocalDate.of(2020, 1, 1));
		exerciceComptableEntityNonPubli.setEspaceOrganisation(null);
		exerciceComptableEntityNonPubli.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntityNonPubli.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntityNonPubli.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntityNonPubli.setNoActivite(false);		
				
		List<ActiviteRepresentationInteretEntity> listActiviteRepresentationInteret = new ArrayList<ActiviteRepresentationInteretEntity>();
		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity();
		activiteRepresentationInteretEntity.setStatut(StatutPublicationEnum.PUBLIEE);
		
		listActiviteRepresentationInteret.add(activiteRepresentationInteretEntity);		
		exerciceComptableEntityNonPubli.setActivitesRepresentationInteret(listActiviteRepresentationInteret);
		
		List<PublicationRelanceEntity> listPublicationRelanceEntity = new ArrayList<PublicationRelanceEntity>();
		
		exerciceComptableEntityNonPubli.setPublicationRelance(listPublicationRelanceEntity);		
			
		exerciceComptableEntityList.add(exerciceComptableEntityNonPubli);
		
		//hasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnum R2 true + checkDateRelanceNblR2 = false
		ExerciceComptableEntity exerciceComptableEntity2 = new ExerciceComptableEntity(2L);
		exerciceComptableEntity2.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity2.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptableEntity2.setEspaceOrganisation(null);
		exerciceComptableEntity2.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity2.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity2.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity2.setNoActivite(false);		
			
		List<PublicationRelanceEntity> listPublicationRelanceEntity2 = new ArrayList<PublicationRelanceEntity>();
		//doit ressortir
		PublicationRelanceEntity publicationRelance2a = new PublicationRelanceEntity();
		publicationRelance2a.setCreationDate(LocalDateTime.of(2020, 3, 28, 0, 0));
		publicationRelance2a.setNiveau(PublicationRelanceNiveauEnum.R1);		
		listPublicationRelanceEntity2.add(publicationRelance2a);
		//doit pas ressortir car la plus récente
		PublicationRelanceEntity publicationRelance2b = new PublicationRelanceEntity();
		publicationRelance2b.setCreationDate(LocalDateTime.of(2020, 3, 28, 0, 0));
		publicationRelance2b.setNiveau(PublicationRelanceNiveauEnum.R2);
		publicationRelance2b.setEnvoye(true);
		listPublicationRelanceEntity2.add(publicationRelance2b);
				
		exerciceComptableEntity2.setPublicationRelance(listPublicationRelanceEntity2);
		
		exerciceComptableEntityList.add(exerciceComptableEntity2);		
		
		//hasAlreadyRelanceEnvoyeDeTypePublicationRelanceNiveauEnum R1 true +  checkDateRelanceNblR2 = false
		ExerciceComptableEntity exerciceComptableEntity3 = new ExerciceComptableEntity(3L);
		exerciceComptableEntity3.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity3.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptableEntity3.setEspaceOrganisation(null);
		exerciceComptableEntity3.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity3.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity3.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity3.setNoActivite(false);
		
		List<PublicationRelanceEntity> listPublicationRelanceEntity3 = new ArrayList<PublicationRelanceEntity>();
		//doit ressortir
		PublicationRelanceEntity publicationRelance3a = new PublicationRelanceEntity();
		publicationRelance3a.setCreationDate(LocalDateTime.of(2020, 3, 28, 0, 0));
		publicationRelance3a.setNiveau(PublicationRelanceNiveauEnum.R1);
		publicationRelance3a.setEnvoye(true);
		listPublicationRelanceEntity3.add(publicationRelance3a);
		//doit pas ressortir car la plus récente
		PublicationRelanceEntity publicationRelance3b = new PublicationRelanceEntity();
		publicationRelance3b.setCreationDate(LocalDateTime.of(2020, 3, 28, 0, 0));
		publicationRelance3b.setNiveau(PublicationRelanceNiveauEnum.R2);

		listPublicationRelanceEntity3.add(publicationRelance3b);				
				
		exerciceComptableEntity3.setPublicationRelance(listPublicationRelanceEntity3);				
		
		exerciceComptableEntityList.add(exerciceComptableEntity3);				
		
		//ctrl date avec endOfMonthAdjustment = false
		ExerciceComptableEntity exerciceComptableEntity4 = new ExerciceComptableEntity(4L);
		exerciceComptableEntity4.setDateDebut(LocalDate.of(2019, 1, 1));
		exerciceComptableEntity4.setDateFin(LocalDate.of(2019, 11, 30));
		exerciceComptableEntity4.setEspaceOrganisation(null);
		exerciceComptableEntity4.setChiffreAffaire(new ChiffreAffaireEntity(1L));
		exerciceComptableEntity4.setMontantDepense(new MontantDepenseEntity(1L));
		exerciceComptableEntity4.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptableEntity4.setNoActivite(false);
		
		List<PublicationRelanceEntity> listPublicationRelanceEntity4 = new ArrayList<PublicationRelanceEntity>();
		//doit ressortir
		PublicationRelanceEntity publicationRelance4a = new PublicationRelanceEntity();
		publicationRelance4a.setCreationDate(LocalDateTime.of(2020, 3, 28, 0, 0));
		publicationRelance4a.setNiveau(PublicationRelanceNiveauEnum.R1);
		//publicationRelance2b.setEnvoye(true);
		listPublicationRelanceEntity4.add(publicationRelance4a);
		//doit pas ressortir car la plus récente
		PublicationRelanceEntity publicationRelance4b = new PublicationRelanceEntity();
		publicationRelance4b.setCreationDate(LocalDateTime.of(2020, 3, 28, 0, 0));
		publicationRelance4b.setNiveau(PublicationRelanceNiveauEnum.R2);

		listPublicationRelanceEntity4.add(publicationRelance4b);				
				
		exerciceComptableEntity4.setPublicationRelance(listPublicationRelanceEntity4);				
		
		exerciceComptableEntityList.add(exerciceComptableEntity4);			
		
		DeclarationReminderDto declarationReminderECNonPubliDto = new DeclarationReminderDto();
		declarationReminderECNonPubliDto.setDenomination(null);
		declarationReminderECNonPubliDto.setDateDebut(exerciceComptableEntityNonPubli.getDateDebut());
		declarationReminderECNonPubliDto.setDateFin(exerciceComptableEntityNonPubli.getDateFin());
		
		DeclarationReminderDto declarationReminderECNonPubliDto2 = new DeclarationReminderDto();
		declarationReminderECNonPubliDto2.setDenomination(null);
		declarationReminderECNonPubliDto2.setDateDebut(exerciceComptableEntity2.getDateDebut());
		declarationReminderECNonPubliDto2.setDateFin(exerciceComptableEntity2.getDateFin());
		
		DeclarationReminderDto declarationReminderECNonPubliDto3 = new DeclarationReminderDto();
		declarationReminderECNonPubliDto3.setDenomination(null);
		declarationReminderECNonPubliDto3.setDateDebut(exerciceComptableEntity3.getDateDebut());
		declarationReminderECNonPubliDto3.setDateFin(exerciceComptableEntity3.getDateFin());
		
		DeclarationReminderDto declarationReminderECNonPubliDto4 = new DeclarationReminderDto();
		declarationReminderECNonPubliDto4.setDenomination(null);
		declarationReminderECNonPubliDto4.setDateDebut(exerciceComptableEntity4.getDateDebut());
		declarationReminderECNonPubliDto4.setDateFin(exerciceComptableEntity4.getDateFin());
		
		//on force les paramtères de config
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "monthAfterCloture", 3L);
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayBeforeCloture1", 15L);
        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayBeforeCloture2", 0L);
//        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayAfterRelance1", 1L);
//        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayAfterRelance2", 5L);
//        org.springframework.test.util.ReflectionTestUtils.setField(exerciceComptableService, "dayAfterBlacklist", 10L);   
        
        
		Mockito.doReturn(exerciceComptableEntityList).when(this.exerciceComptableRepository).findLastExerciceForEachEspaceByDateFinBefore(any());
		Mockito.doReturn(exerciceComptableEntityNonPubli).when(this.exerciceComptableRepository).save(exerciceComptableEntityNonPubli);
		Mockito.doReturn(declarationReminderECNonPubliDto).when(this.declarationReminderTransformer).modelToDto(exerciceComptableEntityNonPubli);
		Mockito.doReturn(declarationReminderECNonPubliDto2).when(this.declarationReminderTransformer).modelToDto(exerciceComptableEntity2);
		Mockito.doReturn(declarationReminderECNonPubliDto3).when(this.declarationReminderTransformer).modelToDto(exerciceComptableEntity3);
		Mockito.doReturn(declarationReminderECNonPubliDto4).when(this.declarationReminderTransformer).modelToDto(exerciceComptableEntity4);

		List<DeclarationReminderDto> retourDeclaReminderDto = this.exerciceComptableService.getExerciceWithoutDeclarationByDate(today);
		
		Assert.assertEquals(3, retourDeclaReminderDto.size());
		Assert.assertTrue(retourDeclaReminderDto.get(0).isAucunePublicationMoyens());
	}
	
}
