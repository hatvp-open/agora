/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.publication;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationActiviteTransformer;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActiviteDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;

/**
 * Test de la classe {@link PublicationTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class PublicationActiviteTransformerTest
{

    /** Transformer des publications. */
    @Autowired
    private PublicationActiviteTransformer publicationActiviteTransformer;


    /**
     * Test de la méthode de {@link fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationActiviteTransformer#activiteToPublication(ActiviteRepresentationInteretEntity)}
     */
    @Test
    public void testActiviteToPublication() {
        final PublicationActiviteEntity entity = new PublicationActiviteEntity();
        entity.setCreationDate(RegistreUtils.generateNewDate(0));
		entity.setPublicationJson("{\"identifiantFiche\":\"9SVFD5Q3\",\"objet\":\"Promouvoir la sécurité transfusionnelle en France.\",\"domainesIntervention\":[\"Droit d'auteur\",\"Prévention\"],\"actionsRepresentationInteret\":[{\"reponsablesPublics\":[\"Titulaire d'un emploi à la décision du Gouvernement\",\"Député ; sénateur ; collaborateur parlementaire ou agents des services des assemblées parlementaires\",\"Membre du Gouvernement ou membre de cabinet ministériel - Affaires sociales et santé\"],\"decisionsConcernees\":[\"Lois, y compris constitutionnelles\",\"Autres décisions publiques\"],\"actionsMenees\":[\"Organiser des discussions informelles ou des réunions en tête-à-tête\",\"Transmettre aux décideurs publics des informations, expertises dans un objectif de conviction\",\"Organiser des débats publics, des marches, des stratégies d'influence sur internet\",\"Etablir une correspondance régulière (par courriel, par courrier…)\",\"Convenir pour un tiers d'une entrevue avec le titulaire d'une charge publique\"],\"tiers\":[\"TERUMO BCT EUROPE N.V.\"],\"observation\":\"troisDE MODIFICATION ACTIVITE\"}]}");
		
		final PublicationActiviteDto publicationActivite = this.publicationActiviteTransformer.activiteHistoriqueToPublication(entity);

        Assert.assertEquals("9SVFD5Q3",publicationActivite.getIdentifiantFiche());
        Assert.assertEquals("Promouvoir la sécurité transfusionnelle en France.", publicationActivite.getObjet());
        Assert.assertEquals("Droit d'auteur",publicationActivite.getDomainesIntervention().get(0));
        Assert.assertEquals("Prévention",publicationActivite.getDomainesIntervention().get(1));
        Assert.assertNotNull(publicationActivite.getActionsRepresentationInteret());
        Assert.assertEquals(entity.getCreationDate(), publicationActivite.getPublicationDate());
    }

    /**
     * Test de la méthode de {@link fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationActiviteTransformer#activiteToPublication(PublicationActiviteEntity)}
     */
    @Test
    public void testActiviteToPublicationSansJson() {
        final PublicationActiviteEntity entity = CommonTestClass.getPublicationActiviteEntity();
        
        try {
        	this.publicationActiviteTransformer.activiteHistoriqueToPublication(entity);
		} catch (BusinessGlobalException ex) {
			assertThat("Impossible de récupérer le json de la publication activite").isEqualTo(ex.getMessage());
		}     

    }

    /**
     * Test de la méthode de {@link fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationActiviteTransformer#activitesToPublication(List)}
     */
    @Test
    public void testActivitesToPublication() {
        final PublicationActiviteEntity entity1 = CommonTestClass.getPublicationActiviteEntity();
        final PublicationActiviteEntity entity2 = CommonTestClass.getPublicationActiviteEntity();
        final PublicationActiviteEntity entity3 = CommonTestClass.getPublicationActiviteEntity();
        entity1.getActivite().getActionsRepresentationInteret().stream().forEach(action->{
        	action.setActiviteRepresentationInteret(entity1.getActivite());
        });
        entity2.getActivite().getActionsRepresentationInteret().stream().forEach(action->{
        	action.setActiviteRepresentationInteret(entity2.getActivite());
        });
        entity3.getActivite().getActionsRepresentationInteret().stream().forEach(action->{
        	action.setActiviteRepresentationInteret(entity3.getActivite());
        });
        List<PublicationActiviteEntity> publications = new ArrayList<>();
        publications.add(entity1);
        
        publications.add(entity2);
        publications.add(entity3);

        List<PublicationActiviteDto> publicationsDto = this.publicationActiviteTransformer.activitesHistoriqueToPublication(publications);
        Assert.assertEquals(3, publicationsDto.size());
        Assert.assertNotNull(publicationsDto.get(0));
        Assert.assertNotNull(publicationsDto.get(1));
        Assert.assertNotNull(publicationsDto.get(2));
    }



}
