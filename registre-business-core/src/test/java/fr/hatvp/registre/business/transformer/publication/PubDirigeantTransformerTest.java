/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.publication;

import static fr.hatvp.registre.business.transformer.publication.CommonTestClass.getDirigeantDto;
import static fr.hatvp.registre.business.transformer.publication.CommonTestClass.getDirigeantEntity;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.publication.PubDirigeantTransformer;
import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.persistence.entity.publication.PubDirigeantEntity;


/**
 * Test de la classe {@link PubDirigeantTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestBusinessConfig.class },
		loader = AnnotationConfigContextLoader.class)
public class PubDirigeantTransformerTest {
	
	
	/** Transfoemr des dirigeants publiés. */
	@Autowired
	private PubDirigeantTransformer pubDirigeantTransformer;
	
	/**
	 * Test de la métode modelToDto.
	 */
	@Test
	public void testDtoToModel() {
		final PubDirigeantEntity entity = getDirigeantEntity();
		final DirigeantDto dto = this.pubDirigeantTransformer.modelToDto(entity);
		
		Assert.assertNotNull(dto);
		Assert.assertEquals(entity.getId(), dto.getId());
		Assert.assertEquals(entity.getVersion(), dto.getVersion());
		Assert.assertEquals(entity.getNom(), dto.getNom());
		Assert.assertEquals(entity.getPrenom(), dto.getPrenom());
		
	}
	
	/**
	 * Test de la métode modelToDto.
	 */
	@Test
	public void testModelToDto() {
		
		final DirigeantDto dto = getDirigeantDto();
		final PubDirigeantEntity entity = this.pubDirigeantTransformer.dtoToModel(dto);
		
		Assert.assertNotNull(entity);
		Assert.assertEquals(dto.getId(), entity.getId());
		Assert.assertEquals(dto.getVersion(), entity.getVersion());
		Assert.assertEquals(dto.getNom(), entity.getNom());
		Assert.assertEquals(dto.getPrenom(), entity.getPrenom());
        Assert.assertEquals(dto.getEspaceOrganisationId(), entity.getEspaceOrganisation().getId());
		
	}
	
}
