/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.impl.DeclarantContactTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.DeclarantContactTransformer;
import fr.hatvp.registre.commons.dto.DeclarantContactDto;
import fr.hatvp.registre.commons.lists.CategoryTelephoneMail;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantContactEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;

/**
 * Test de la classe {@link DeclarantContactTransformerImpl}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class DeclarantContactTransformerTest
{

    /** Transformer à tester. */
    @Autowired
    private DeclarantContactTransformer declarantContactTransformer;

    /**
     * Test de la métode modelToDto
     */
    @Test
    public void testModelToDto()
    {
        /*
         * Création d'une entité
         */
        final DeclarantContactEntity model = new DeclarantContactEntity();
        model.setCategory(CategoryTelephoneMail.PERSO);
        model.setDeclarant(new DeclarantEntity());
        model.setEmail(RandomStringUtils.randomAlphanumeric(15));
        model.setTelephone(RandomStringUtils.randomAlphanumeric(10));
        model.setId(1L);

        /*
         * Transformation
         */
        final DeclarantContactDto dto = this.declarantContactTransformer.modelToDto(model);

        /*
         * Assertions
         */
        Assert.assertNotNull(dto);
        Assert.assertEquals(model.getEmail(), dto.getEmail());
        Assert.assertEquals(model.getTelephone(), dto.getTelephone());
        Assert.assertEquals(model.getCategory(), dto.getCategorie());
        Assert.assertEquals(model.getId(), dto.getId());
        Assert.assertEquals(model.getVersion(), dto.getVersion());
    }

    /**
     * Test de la métode modelToDto
     */
    @Test
    public void testDtoToModel()
    {
        /*
         * Création d'un Dto
         */
        final DeclarantContactDto dto = new DeclarantContactDto();
        dto.setCategorie(CategoryTelephoneMail.PERSO);
        dto.setEmail(RandomStringUtils.randomAlphanumeric(15));
        dto.setTelephone(RandomStringUtils.randomAlphanumeric(10));
        dto.setId(1L);
        dto.setVersion(0);

        /*
         * Transformation
         */
        final DeclarantContactEntity model = this.declarantContactTransformer.dtoToModel(dto);

        /*
         * Assertions
         */
        Assert.assertNotNull(model);
        Assert.assertEquals(dto.getEmail(), model.getEmail());
        Assert.assertEquals(dto.getTelephone(), model.getTelephone());
        Assert.assertEquals(dto.getCategorie(), model.getCategory());
        Assert.assertEquals(dto.getId(), model.getId());
        Assert.assertEquals(dto.getVersion(), model.getVersion());
    }

}
