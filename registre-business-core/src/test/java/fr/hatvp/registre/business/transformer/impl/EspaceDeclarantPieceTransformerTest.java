/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.EspaceDeclarantPieceTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.EspaceDeclarantPieceDto;
import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.piece.EspaceDeclarantPieceEntity;

/**
 * Test de la classe {@link EspaceDeclarantPieceTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestBusinessConfig.class }, loader = AnnotationConfigContextLoader.class)
public class EspaceDeclarantPieceTransformerTest {
	/** Transformeur des pièces du déclarant. */
	@Autowired
	private EspaceDeclarantPieceTransformer declarantPieceTransformer;

	private final Date DATE_VERSEMENT = new Date();
	private final OrigineVersementEnum ORIGINE_VERSEMENT = OrigineVersementEnum.INSCRIPTION_DECLARANT;
	private final String NOM_FICHIER = "fichier.extension";
	private final byte[] CONTENT_FICHIER = { 0x1, 0x2, 0x3, 0x4, 0x5 };

	/**
	 * Test de la méthode {@link EspaceDeclarantPieceTransformer#modelToDto(AbstractCommonEntity)}
	 */
	@Test
	public void testModelToDto() {
		final EspaceDeclarantPieceEntity entity = getPiece();
		final EspaceDeclarantPieceDto dto = declarantPieceTransformer.modelToDto(entity);

		Assert.assertNotNull(dto);

		Assert.assertEquals(entity.getId(), dto.getId());
		Assert.assertEquals(entity.getVersion(), dto.getVersion());
		Assert.assertEquals(entity.getDateVersementPiece(), dto.getDateVersementPiece());
		Assert.assertEquals(entity.getOrigineVersement(), dto.getOrigineVersement());
		Assert.assertEquals(entity.getNomFichier(), dto.getNomFichier());
		Assert.assertArrayEquals(entity.getContenuFichier(), dto.getContenuFichier());
		Assert.assertEquals(entity.getMediaType(), dto.getMediaType());
		Assert.assertEquals(entity.getDeclarant().getId(), dto.getDeclarantId());

	}

	/**
	 * Test de la méthode {@link EspaceDeclarantPieceTransformer#dtoToModel(AbstractCommonDto)}
	 */
	@Test
	public void testDtoToModel() {
		final EspaceDeclarantPieceDto dto = getPieceDto();
		final EspaceDeclarantPieceEntity entity = declarantPieceTransformer.dtoToModel(dto);

		Assert.assertNotNull(entity);

		Assert.assertEquals(dto.getId(), entity.getId());
		Assert.assertEquals(dto.getVersion(), entity.getVersion());
		Assert.assertEquals(dto.getDateVersementPiece(), entity.getDateVersementPiece());
		Assert.assertEquals(dto.getOrigineVersement(), entity.getOrigineVersement());
		Assert.assertEquals(dto.getNomFichier(), entity.getNomFichier());
		Assert.assertArrayEquals(dto.getContenuFichier(), entity.getContenuFichier());
		Assert.assertEquals(dto.getMediaType(), entity.getMediaType());
	}

	/**
	 * Retourne une entité de EspaceDeclarantPiece
	 * 
	 * @return
	 */
	private EspaceDeclarantPieceEntity getPiece() {
		final EspaceDeclarantPieceEntity entity = new EspaceDeclarantPieceEntity();

		entity.setId(1L);
		entity.setVersion(0);

		entity.setDateVersementPiece(DATE_VERSEMENT);
		entity.setOrigineVersement(ORIGINE_VERSEMENT);
		entity.setNomFichier(NOM_FICHIER);
		entity.setContenuFichier(CONTENT_FICHIER);
		entity.setMediaType("image/jpeg");

		DeclarantEntity declarant = new DeclarantEntity();
		declarant.setId(1L);
		declarant.setVersion(0);

		entity.setDeclarant(declarant);

		return entity;
	}

	/**
	 * Retourne un DDTO de EspaceDeclarantPiece
	 * 
	 * @return
	 */
	private EspaceDeclarantPieceDto getPieceDto() {
		final EspaceDeclarantPieceDto piece = new EspaceDeclarantPieceDto();

		piece.setId(1L);
		piece.setVersion(0);

		piece.setDateVersementPiece(DATE_VERSEMENT);
		piece.setOrigineVersement(ORIGINE_VERSEMENT);
		piece.setNomFichier(NOM_FICHIER);
		piece.setContenuFichier(CONTENT_FICHIER);
		piece.setMediaType("image/jpeg");

		piece.setDeclarantId(1L);

		return piece;
	}
}
