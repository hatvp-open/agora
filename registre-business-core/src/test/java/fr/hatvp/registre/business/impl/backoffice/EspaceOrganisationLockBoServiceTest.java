/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice;

import com.mifmif.common.regex.Generex;
import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.backoffice.EspaceOrganisationLockBoTransformer;
import fr.hatvp.registre.commons.dto.backoffice.EspaceOrganisationLockBoDto;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.commons.utils.ValidatePatternUtils;
import fr.hatvp.registre.persistence.entity.backoffice.EspaceOrganisationLockBoEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.repository.backoffice.EspaceOrganisationLockBoRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe de test su service
 * {@link fr.hatvp.registre.business.backoffice.EspaceOrganisationLockBoService}
 *
 * @version $Revision$ $Date${0xD}
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
    loader = AnnotationConfigContextLoader.class)
public class EspaceOrganisationLockBoServiceTest {
    /** Durée Timeout lock d'entité (en secondes). */
    @Value("${app.lock.espaceorganisation.timeout}")
    private long lockTimeout;

    /** Repository des espaces organisations locks. */
    @Mock
    private EspaceOrganisationLockBoRepository espaceOrganisationLockBoRepository;

    /** Transformer de l'entité EspaceOrganisationLockBoEntity. */
    @Mock
    private EspaceOrganisationLockBoTransformer espaceOrganisationLockBoTransformer;

    /** Injections des mocks. */
    @InjectMocks
    private EspaceOrganisationLockBoServiceImpl espaceOrganisationLockBoService;

    /**
     * Initialisation des tests..
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test de la méthode
     * {@link fr.hatvp.registre.business.backoffice.EspaceOrganisationLockBoService#getLockOn(Long)}}
     * Cas d'un lock actif
     */
    @Test
    public void testGetLockOnWithResult() {
        final Long espaceOrganisationId = 2L;

        final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(espaceOrganisationId);
        espaceOrganisationEntity.setCreationDate(new Date());

        final UtilisateurBoEntity utilisateurBoEntity = getUtilisateurBoEntity();

        final EspaceOrganisationLockBoEntity espaceOrganisationLockBoEntity =
            getEspaceOrganisationLockBoEntity(espaceOrganisationEntity, utilisateurBoEntity,
                Date.from(LocalDateTime.now().minus(this.lockTimeout / 2, ChronoUnit.SECONDS)
                    .atZone(ZoneId.systemDefault()).toInstant()));

        final List<EspaceOrganisationLockBoEntity> result = new ArrayList<>();
        result.add(espaceOrganisationLockBoEntity);

        final EspaceOrganisationLockBoDto espaceOrganisationLockBoDto = new EspaceOrganisationLockBoDto();
        espaceOrganisationLockBoDto.setId(espaceOrganisationLockBoEntity.getId());
        espaceOrganisationLockBoDto
            .setUtilisateurBoId(espaceOrganisationLockBoEntity.getUtilisateurBo().getId());
        espaceOrganisationLockBoDto.setEspaceOrganisationId(
            espaceOrganisationLockBoEntity.getEspaceOrganisation().getId());
        espaceOrganisationLockBoDto
            .setLockTimeStart(espaceOrganisationLockBoEntity.getLockedTime());

        final Long remainingTime = this.lockTimeout
            - (new Date().getTime() - espaceOrganisationLockBoEntity.getLockedTime().getTime())
            / 1000;
        espaceOrganisationLockBoDto.setLockTimeRemain(remainingTime > 0 ? remainingTime : 0);

        Mockito.doReturn(espaceOrganisationLockBoDto).when(this.espaceOrganisationLockBoTransformer)
            .modelToDto(espaceOrganisationLockBoEntity);

        Mockito.doReturn(result).when(this.espaceOrganisationLockBoRepository)
            .findByEspaceOrganisationId(espaceOrganisationId);

        final EspaceOrganisationLockBoDto res = this.espaceOrganisationLockBoService
            .getLockOn(espaceOrganisationId);

        Assert.assertNotNull(res);
        Assert.assertTrue(res.getLockTimeRemain() > 0);
    }

    /**
     * Test de la méthode
     * {@link fr.hatvp.registre.business.backoffice.EspaceOrganisationLockBoService#getLockOn(Long)}}
     * Cas d'un lock inactif
     */
    @Test
    public void testGetLockOnNoResult() {
        final Long espaceOrganisationId = 2L;

        final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(espaceOrganisationId);
        espaceOrganisationEntity.setCreationDate(new Date());

        final UtilisateurBoEntity utilisateurBoEntity = getUtilisateurBoEntity();

        final EspaceOrganisationLockBoEntity espaceOrganisationLockBoEntity =
            getEspaceOrganisationLockBoEntity(espaceOrganisationEntity, utilisateurBoEntity,
                Date.from(LocalDateTime.now().minus(this.lockTimeout * 2, ChronoUnit.SECONDS)
                    .atZone(ZoneId.systemDefault()).toInstant()));

        final List<EspaceOrganisationLockBoEntity> result = new ArrayList<>();
        result.add(espaceOrganisationLockBoEntity);

        final EspaceOrganisationLockBoDto espaceOrganisationLockBoDto = new EspaceOrganisationLockBoDto();
        espaceOrganisationLockBoDto.setId(espaceOrganisationLockBoEntity.getId());
        espaceOrganisationLockBoDto
            .setUtilisateurBoId(espaceOrganisationLockBoEntity.getUtilisateurBo().getId());
        espaceOrganisationLockBoDto.setEspaceOrganisationId(
            espaceOrganisationLockBoEntity.getEspaceOrganisation().getId());
        espaceOrganisationLockBoDto
            .setLockTimeStart(espaceOrganisationLockBoEntity.getLockedTime());

        final Long remainingTime = this.lockTimeout
            - (new Date().getTime() - espaceOrganisationLockBoEntity.getLockedTime().getTime())
            / 1000;
        espaceOrganisationLockBoDto.setLockTimeRemain(remainingTime > 0 ? remainingTime : 0);

        Mockito.doReturn(espaceOrganisationLockBoDto).when(this.espaceOrganisationLockBoTransformer)
            .modelToDto(espaceOrganisationLockBoEntity);

        Mockito.doReturn(result).when(this.espaceOrganisationLockBoRepository)
            .findByEspaceOrganisationId(espaceOrganisationId);

        final EspaceOrganisationLockBoDto res = this.espaceOrganisationLockBoService
            .getLockOn(espaceOrganisationId);

        Assert.assertNull(res);
    }

    /**
     * Récupérer uen instance de {@link EspaceOrganisationLockBoEntity}
     */
    EspaceOrganisationLockBoEntity getEspaceOrganisationLockBoEntity(
        final EspaceOrganisationEntity espaceOrganisationEntity,
        final UtilisateurBoEntity utilisateurBoEntity, final Date from) {
        final EspaceOrganisationLockBoEntity espaceOrganisationLockBoEntity = new EspaceOrganisationLockBoEntity();
        espaceOrganisationLockBoEntity.setId(1L);
        espaceOrganisationLockBoEntity.setUtilisateurBo(utilisateurBoEntity);
        espaceOrganisationLockBoEntity.setEspaceOrganisation(espaceOrganisationEntity);
        espaceOrganisationLockBoEntity.setLockedTime(
            from);
        return espaceOrganisationLockBoEntity;
    }

    /**
     * Test de la méthode {@link fr.hatvp.registre.business.backoffice.EspaceOrganisationLockBoService#updateLockOnEspace(EspaceOrganisationLockBoDto,
     * Long)}.
     */
    public void testUpdateLockOnEspace() {
        //TODO: a faire !
    }

    /**
     * @return entité utilisateur bo.
     */
    private UtilisateurBoEntity getUtilisateurBoEntity() {
        final Generex generex = new Generex(ValidatePatternUtils.getPasswordGenerationPattern());
        final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
        utilisateurBoEntity.setEmail("test@test.test");
        utilisateurBoEntity.setId(3L);
        utilisateurBoEntity.setPassword(generex.random());
        utilisateurBoEntity.setNom(RegistreUtils.randomString(10));
        utilisateurBoEntity.setPrenom(RegistreUtils.randomString(10));
        return utilisateurBoEntity;
    }
}
