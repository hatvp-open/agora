/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.email.DesinscriptionMailer;
import fr.hatvp.registre.business.transformer.proxy.DesinscriptionPieceTransformer;
import fr.hatvp.registre.business.transformer.proxy.DesinscriptionTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationTransformer;
import fr.hatvp.registre.commons.dto.DesinscriptionDto;
import fr.hatvp.registre.commons.dto.DesinscriptionPieceDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.OrigineSaisieEnum;
import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.piece.DesinscriptionPieceEntity;
import fr.hatvp.registre.persistence.entity.publication.batch.relance.PublicationRelanceEntity;
import fr.hatvp.registre.persistence.repository.activite.ExerciceComptableRepository;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionPieceRepository;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.PeriodeActiviteClientRepository;

/**
 * Test du service EspaceOrganisationService {@link fr.hatvp.registre.business.DesinscriptionService}.
 */

// Permet la compatibilité des tests junit4 et junit5
@RunWith(SpringJUnit4ClassRunner.class)
// Charge un ApplicationContext permettant l'utilisation des composants qui sont annotés avec @Component, @Service, @Repository
@ContextConfiguration(classes = { TestBusinessConfig.class }, loader = AnnotationConfigContextLoader.class)
public class DesinscriptionServiceTest {

    /** Id de test de l'espace organisation. */
    private final static Long ID_ESPACE_ORGANISATION = 1L;

    /** Id de test du déclarant. */
    private final static Long ID_DECLARANT = 2L;

    /** Id de test du déclarant. */
    private final static Long ID_DESINSCRIPTION = 3L;

    /** Id de test du déclarant. */
    private final static Long ID_DESINSCRIPTION_PIECE = 4L;

    /** Injecter les mock dans le service. */
    @InjectMocks
    private DesinscriptionServiceImpl desinscriptionService;

    /**
     * Mock des repository
     */

    /** Repository pour la désinscription. */
    @Mock
    private DesinscriptionRepository desinscriptionRepository;

    /** Repository pour l'espace organisation. */
    @Mock
    private EspaceOrganisationRepository espaceOrganisationRepository;

    /** Repository pour les déclarants. */
    @Mock
    private DeclarantRepository declarantRepository;

    /** Repository pour les pièces de désinscription. */
    @Mock
    private DesinscriptionPieceRepository desinscriptionPieceRepository;
    
    @Mock
    private PeriodeActiviteClientRepository periodeActiviteClientRepository;

    /**
     * Mock des transformers
     */

    @Mock
    private DesinscriptionTransformer desinscriptionTransformer;

    @Mock
    private DesinscriptionPieceTransformer desinscriptionPieceTransformer;

    @Mock
    private EspaceOrganisationTransformer espaceOrganisationTransformer;
    
    @Mock
    ExerciceComptableRepository exerciceComptableRepository;
    
    @Mock
    private DesinscriptionMailer desinscriptionMailer;

    /**
     * Initialiser les mocks avant de lancer les test.
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        // Process mock annotations
        MockitoAnnotations.initMocks(this);

    }

    /**
     * Test de la méthode "DesinscriptionDto getDesinscription(long espaceId)"
     */
    @Test
    public void testGetDesinscription(){
        EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);

        EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
        espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);

        DesinscriptionDto desinscriptionDto = new DesinscriptionDto();
        desinscriptionDto.setId(ID_DESINSCRIPTION);
        desinscriptionDto.setEspaceOrganisation(espaceOrganisationDto);

        DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
        desinscriptionEntity.setId(ID_DESINSCRIPTION);
        desinscriptionEntity.setEspaceOrganisation(espaceOrganisationEntity);

        Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findById(ID_ESPACE_ORGANISATION);
        Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionRepository).findByEspaceOrganisation(espaceOrganisationEntity);
        Mockito.doReturn(desinscriptionDto).when(this.desinscriptionTransformer).modelToDto(desinscriptionEntity);

        DesinscriptionDto desinscriptionDtoValue = this.desinscriptionService.getDesinscription(ID_ESPACE_ORGANISATION);

        Assert.assertEquals(ID_ESPACE_ORGANISATION,desinscriptionDtoValue.getEspaceOrganisation().getId());
    }

    /**
     * Test de la méthode "void saveDesinscription (Long espaceId, DesinscriptionDto desinscriptionDto, List<DesinscriptionPieceDto> pieces, Long userId)"
     */
    @Test
    public void testSaveDesinscription(){

        // déclarant
        DeclarantEntity declarantEntity = new DeclarantEntity();
        declarantEntity.setId(ID_DECLARANT);

        // desinscriptionDto
        DesinscriptionDto desinscriptionDto = new DesinscriptionDto();
        desinscriptionDto.setId(ID_DESINSCRIPTION);
        desinscriptionDto.setCessationDate(LocalDate.of(2020,02,29));


        // espaceOrganisationEntity
        EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);

        EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
        espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
        desinscriptionDto.setEspaceOrganisation(espaceOrganisationDto);


        // desinscriptionEntity
        DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
        desinscriptionEntity.setId(ID_DESINSCRIPTION);
        desinscriptionEntity.setCessationDate(LocalDate.of(2020,02,29));
        desinscriptionEntity.setEspaceOrganisation(espaceOrganisationEntity);

        // pieceDto
        List<DesinscriptionPieceDto> pieces = new ArrayList<>();
        DesinscriptionPieceDto desinscriptionPieceDto = new DesinscriptionPieceDto();
        desinscriptionPieceDto.setContenuFichier(new byte[500]);
        desinscriptionPieceDto.setMediaType("application/pdf");
        desinscriptionPieceDto.setNomFichier("test");
        desinscriptionPieceDto.setDateVersementPiece(new Date());
        desinscriptionPieceDto.setDeclarantId(ID_DECLARANT);
        desinscriptionPieceDto.setId(ID_DESINSCRIPTION_PIECE);
        desinscriptionPieceDto.setOrigineVersement(OrigineVersementEnum.DEMANDE_DESINSCRIPTION_ESPACE);
        pieces.add(desinscriptionPieceDto);
        desinscriptionDto.setPieces(pieces);

        // pieceEntity
        DesinscriptionPieceEntity desinscriptionPieceEntity = new DesinscriptionPieceEntity();
        desinscriptionPieceEntity.setContenuFichier(new byte[500]);
        desinscriptionPieceEntity.setMediaType("application/pdf");
        desinscriptionPieceEntity.setNomFichier("test");
        desinscriptionPieceEntity.setDateVersementPiece(new Date());
        desinscriptionPieceEntity.setId(ID_DESINSCRIPTION_PIECE);
        desinscriptionPieceEntity.setOrigineVersement(OrigineVersementEnum.DEMANDE_DESINSCRIPTION_ESPACE);

        // Initialisation des informations lorsqu'un répo est appellé
        Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findById(ID_ESPACE_ORGANISATION);
        Mockito.doReturn(espaceOrganisationDto).when(this.espaceOrganisationTransformer).modelToDto(espaceOrganisationEntity);
        Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionTransformer).dtoToModel(desinscriptionDto);
        Mockito.doReturn(desinscriptionPieceEntity).when(this.desinscriptionPieceTransformer).dtoToModel(desinscriptionPieceDto);
        Mockito.doReturn(declarantEntity).when(this.declarantRepository).findById(ID_DECLARANT);
        Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionRepository).save(desinscriptionEntity);
        Mockito.doReturn(null).when(this.periodeActiviteClientRepository).findPeriodeActiveByEspaceId(ID_ESPACE_ORGANISATION);
        

        // apelle de la méthode
        DesinscriptionDto desinscriptionDtoTest = this.desinscriptionService.saveDesinscription(desinscriptionDto, pieces, ID_DECLARANT, ID_ESPACE_ORGANISATION);

        // assertion
        Assert.assertEquals(desinscriptionDto.getCessationDate(), desinscriptionDtoTest.getCessationDate());
        Assert.assertEquals(desinscriptionDto.getId(), desinscriptionDtoTest.getId());
        Assert.assertEquals(espaceOrganisationEntity.getId(), desinscriptionDtoTest.getEspaceOrganisation().getId());
        Assert.assertEquals(desinscriptionDto.getPieces().get(0), desinscriptionDtoTest.getPieces().get(0));
    }
    /**
     * TODO
     */
    @Ignore
    @Test
    public void testSaveDesinscriptionAvecClientActif(){
    	
    }

    /** Test de la méthode List<DesinscriptionPieceDto> getPiecesDesinscription(long espaceId) */
    @Test
    public void testGetPiecesDesinscription(){
        DesinscriptionPieceEntity desinscriptionPieceEntity = new DesinscriptionPieceEntity();
        desinscriptionPieceEntity.setId(ID_DESINSCRIPTION_PIECE);
        DesinscriptionPieceDto desinscriptionPieceDto = new DesinscriptionPieceDto();
        desinscriptionPieceDto.setId(ID_DESINSCRIPTION_PIECE);
        List<DesinscriptionPieceEntity> desinscriptionPieceEntitiesList = new ArrayList<>();
        desinscriptionPieceEntitiesList.add(desinscriptionPieceEntity);
        Stream<DesinscriptionPieceEntity> desinscriptionPieceEntities = desinscriptionPieceEntitiesList.stream();

        Mockito.doReturn(desinscriptionPieceEntities).when(this.desinscriptionPieceRepository).findByDesinscription_EspaceOrganisation_IdOrderByDateVersementPieceDesc(ID_ESPACE_ORGANISATION);
        Mockito.doReturn(desinscriptionPieceDto).when(this.desinscriptionPieceTransformer).modelToDto(desinscriptionPieceEntity);

        List<DesinscriptionPieceDto> desinscriptionPieceDtos = this.desinscriptionService.getPiecesDesinscription(ID_ESPACE_ORGANISATION);

        Assert.assertEquals(desinscriptionPieceDto.getId(), desinscriptionPieceDtos.get(0).getId());
    }
    
    /**
     * List<ExerciceComptableEntity> miseAjourExerciceComptable(List<ExerciceComptableEntity> exerciceComptableEntities, LocalDate dateCessation)
     * 1- Si date début > date cessation on supprime l'exercice 
     */
    @Test
    public void miseAjourExerciceComptableDateCessationBeforeDateDebutSansRelance() {
	
    	LocalDate dateCessation = LocalDate.of(2018,12,29);

    	List<ExerciceComptableEntity> listExerciceComptableEntities = new ArrayList<ExerciceComptableEntity>();
    	
    	ExerciceComptableEntity exerciceComptableEntity2017 = new ExerciceComptableEntity();
    	exerciceComptableEntity2017.setDateDebut(LocalDate.of(2017,07,01));
    	exerciceComptableEntity2017.setDateFin(LocalDate.of(2017,12,31));
    	exerciceComptableEntity2017.setPublicationRelance(null);
    	listExerciceComptableEntities.add(exerciceComptableEntity2017);
    	
    	ExerciceComptableEntity exerciceComptableEntity2018 = new ExerciceComptableEntity();
    	exerciceComptableEntity2018.setDateDebut(LocalDate.of(2018,01,01));
    	exerciceComptableEntity2018.setDateFin(LocalDate.of(2018,12,31));
    	exerciceComptableEntity2017.setPublicationRelance(null);
    	listExerciceComptableEntities.add(exerciceComptableEntity2018);
    	
    	ExerciceComptableEntity exerciceComptableEntity2019 = new ExerciceComptableEntity();
    	exerciceComptableEntity2019.setDateDebut(LocalDate.of(2019,01,01));
    	exerciceComptableEntity2019.setDateFin(LocalDate.of(2019,12,31));
    	exerciceComptableEntity2017.setPublicationRelance(null);
    	listExerciceComptableEntities.add(exerciceComptableEntity2019);
    	
    	Mockito.doNothing().when(this.exerciceComptableRepository).delete(exerciceComptableEntity2019);    	
    	
    	List<ExerciceComptableEntity> listExerciceComptableEntitiesRetour= this.desinscriptionService.miseAjourExerciceComptable(listExerciceComptableEntities, dateCessation);
    	
    	Assert.assertEquals(2, listExerciceComptableEntitiesRetour.size());
    	Assert.assertEquals(LocalDate.of(2018,12,29), listExerciceComptableEntitiesRetour.get(1).getDateFin());
    }
    /**
     * List<ExerciceComptableEntity> miseAjourExerciceComptable(List<ExerciceComptableEntity> exerciceComptableEntities, LocalDate dateCessation)
     * 1- Si date début > date cessation on supprime l'exercice et sa relance
     */
    @Test
    public void miseAjourExerciceComptableDateCessationBeforeDateDebutAvecRelance() {
	
    	LocalDate dateCessation = LocalDate.of(2018,12,29);

    	List<ExerciceComptableEntity> listExerciceComptableEntities = new ArrayList<ExerciceComptableEntity>();
    	
    	ExerciceComptableEntity exerciceComptableEntity2017 = new ExerciceComptableEntity();
    	exerciceComptableEntity2017.setDateDebut(LocalDate.of(2017,07,01));
    	exerciceComptableEntity2017.setDateFin(LocalDate.of(2017,12,31));
    	exerciceComptableEntity2017.setPublicationRelance(null);
    	listExerciceComptableEntities.add(exerciceComptableEntity2017);
    	
    	PublicationRelanceEntity publicationRelanceEntity = new PublicationRelanceEntity();
    	publicationRelanceEntity .setId(1L);
    	List<PublicationRelanceEntity> listRelance = new ArrayList<>();
    	listRelance.add(publicationRelanceEntity);
    	
    	ExerciceComptableEntity exerciceComptableEntity2018 = new ExerciceComptableEntity();
    	exerciceComptableEntity2018.setDateDebut(LocalDate.of(2018,01,01));
    	exerciceComptableEntity2018.setDateFin(LocalDate.of(2018,12,31));
    	exerciceComptableEntity2017.setPublicationRelance(listRelance);
    	listExerciceComptableEntities.add(exerciceComptableEntity2018);
    	
    	ExerciceComptableEntity exerciceComptableEntity2019 = new ExerciceComptableEntity();
    	exerciceComptableEntity2019.setDateDebut(LocalDate.of(2019,01,01));
    	exerciceComptableEntity2019.setDateFin(LocalDate.of(2019,12,31));
    	exerciceComptableEntity2017.setPublicationRelance(null);
    	listExerciceComptableEntities.add(exerciceComptableEntity2019);
    	
    	Mockito.doNothing().when(this.exerciceComptableRepository).delete(exerciceComptableEntity2019);    	
    	
    	List<ExerciceComptableEntity> listExerciceComptableEntitiesRetour= this.desinscriptionService.miseAjourExerciceComptable(listExerciceComptableEntities, dateCessation);
    	
    	Assert.assertEquals(2, listExerciceComptableEntitiesRetour.size());
    	Assert.assertEquals(LocalDate.of(2018,12,29), listExerciceComptableEntitiesRetour.get(1).getDateFin());
    }

    
    /**
     * List<ExerciceComptableEntity> miseAjourExerciceComptable(List<ExerciceComptableEntity> exerciceComptableEntities, LocalDate dateCessation)
     * 
     * 3- Date début < date cessation et date fin < date cessation => on ne doit pas modifier la date de fin 
     */
    @Test
    public void miseAjourExerciceComptableDateCessationAfterDateDebutDateFinBefore() {
    	
    	LocalDate dateCessation = LocalDate.of(2019,12,31);

    	List<ExerciceComptableEntity> listExerciceComptableEntities = new ArrayList<ExerciceComptableEntity>();
    	
    	ExerciceComptableEntity exerciceComptableEntity2017 = new ExerciceComptableEntity();
    	exerciceComptableEntity2017.setDateDebut(LocalDate.of(2017,07,01));
    	exerciceComptableEntity2017.setDateFin(LocalDate.of(2017,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2017);
    	
    	ExerciceComptableEntity exerciceComptableEntity2018 = new ExerciceComptableEntity();
    	exerciceComptableEntity2018.setDateDebut(LocalDate.of(2018,01,01));
    	exerciceComptableEntity2018.setDateFin(LocalDate.of(2018,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2018);
    	
    	ExerciceComptableEntity exerciceComptableEntity2019 = new ExerciceComptableEntity();
    	exerciceComptableEntity2019.setDateDebut(LocalDate.of(2019,01,01));
    	exerciceComptableEntity2019.setDateFin(LocalDate.of(2019,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2019);  	
    	
    	List<ExerciceComptableEntity> listExerciceComptableEntitiesRetour= this.desinscriptionService.miseAjourExerciceComptable(listExerciceComptableEntities, dateCessation);
    	
    	Assert.assertEquals(3, listExerciceComptableEntitiesRetour.size());
    	Assert.assertEquals(LocalDate.of(2019,12,31), listExerciceComptableEntitiesRetour.get(2).getDateFin()); 	
    }
    
    /**
     * Test de la méthode "void saveDesinscription (Long espaceId, DesinscriptionDto desinscriptionDto, List<DesinscriptionPieceDto> pieces, Long userId)"
     * lors de la saisie par une agent et avec mise à jour des exrecice comptable
     * 
     */
    @Test
    public void testSaveDesinscriptionSaisieParAgentAvecMiseAjourExerciceComptableCessationDansPasse(){
        // desinscriptionDto
        DesinscriptionDto desinscriptionDto = new DesinscriptionDto();
        desinscriptionDto.setId(null);
        desinscriptionDto.setCessationDate(LocalDate.of(2020,02,29));

        List<ExerciceComptableEntity> listExerciceComptableEntities = new ArrayList<ExerciceComptableEntity>();
    	
    	ExerciceComptableEntity exerciceComptableEntity2017 = new ExerciceComptableEntity();
    	exerciceComptableEntity2017.setDateDebut(LocalDate.of(2017,07,01));
    	exerciceComptableEntity2017.setDateFin(LocalDate.of(2017,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2017);
    	
    	ExerciceComptableEntity exerciceComptableEntity2018 = new ExerciceComptableEntity();
    	exerciceComptableEntity2018.setDateDebut(LocalDate.of(2018,01,01));
    	exerciceComptableEntity2018.setDateFin(LocalDate.of(2018,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2018);
    	
    	ExerciceComptableEntity exerciceComptableEntity2019 = new ExerciceComptableEntity();
    	exerciceComptableEntity2019.setDateDebut(LocalDate.of(2019,01,01));
    	exerciceComptableEntity2019.setDateFin(LocalDate.of(2019,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2019);
    	
    	ExerciceComptableEntity exerciceComptableEntity2020 = new ExerciceComptableEntity();
    	exerciceComptableEntity2020.setDateDebut(LocalDate.of(2020,01,01));
    	exerciceComptableEntity2020.setDateFin(LocalDate.of(2020,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2020);
    	
        // espaceOrganisationEntity
        EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
        espaceOrganisationEntity.setExercicesComptable(listExerciceComptableEntities);        

        EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
        espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
        desinscriptionDto.setEspaceOrganisation(espaceOrganisationDto);

        // desinscriptionEntity
        DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
        desinscriptionEntity.setId(null);
        desinscriptionEntity.setCessationDate(LocalDate.of(2020,02,29));
        desinscriptionEntity.setEspaceOrganisation(espaceOrganisationEntity);

        // pieceDto
        List<DesinscriptionPieceDto> pieces = new ArrayList<>();
        desinscriptionDto.setPieces(pieces);

        // Initialisation des informations lorsqu'un répo est appellé
        Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findById(ID_ESPACE_ORGANISATION);
        Mockito.doReturn(espaceOrganisationDto).when(this.espaceOrganisationTransformer).modelToDto(espaceOrganisationEntity);
        Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionTransformer).dtoToModel(desinscriptionDto);
        
        Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionRepository).save(desinscriptionEntity);

        // apelle de la méthode
        this.desinscriptionService.saveDesinscription(desinscriptionDto, pieces, ID_DECLARANT, ID_ESPACE_ORGANISATION);

        // assertion
        Assert.assertEquals(OrigineSaisieEnum.AGENT, desinscriptionEntity.getOrigineSaisie());
        Assert.assertEquals(StatutEspaceEnum.DESINSCRIT, espaceOrganisationEntity.getStatut());
        Assert.assertEquals(desinscriptionDto.getCessationDate(), espaceOrganisationEntity.getExercicesComptable().get(3).getDateFin()); 
    }
    
    @Test
    public void testSaveDesinscriptionSaisieParAgentAvecMiseAjourExerciceComptableCessationDansFutur(){
        // desinscriptionDto
        DesinscriptionDto desinscriptionDto = new DesinscriptionDto();
        desinscriptionDto.setId(null);
        desinscriptionDto.setCessationDate(LocalDate.of(2060,02,29));

        List<ExerciceComptableEntity> listExerciceComptableEntities = new ArrayList<ExerciceComptableEntity>();
    	
    	ExerciceComptableEntity exerciceComptableEntity2017 = new ExerciceComptableEntity();
    	exerciceComptableEntity2017.setDateDebut(LocalDate.of(2017,07,01));
    	exerciceComptableEntity2017.setDateFin(LocalDate.of(2017,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2017);
    	
    	ExerciceComptableEntity exerciceComptableEntity2018 = new ExerciceComptableEntity();
    	exerciceComptableEntity2018.setDateDebut(LocalDate.of(2018,01,01));
    	exerciceComptableEntity2018.setDateFin(LocalDate.of(2018,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2018);
    	
    	ExerciceComptableEntity exerciceComptableEntity2019 = new ExerciceComptableEntity();
    	exerciceComptableEntity2019.setDateDebut(LocalDate.of(2019,01,01));
    	exerciceComptableEntity2019.setDateFin(LocalDate.of(2019,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2019);
    	
    	ExerciceComptableEntity exerciceComptableEntity2060 = new ExerciceComptableEntity();
    	exerciceComptableEntity2060.setDateDebut(LocalDate.of(2060,01,01));
    	exerciceComptableEntity2060.setDateFin(LocalDate.of(2060,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2060);
    	
        // espaceOrganisationEntity
        EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
        espaceOrganisationEntity.setExercicesComptable(listExerciceComptableEntities);        

        EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
        espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
        desinscriptionDto.setEspaceOrganisation(espaceOrganisationDto);

        // desinscriptionEntity
        DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
        desinscriptionEntity.setId(null);
        desinscriptionEntity.setCessationDate(LocalDate.of(2060,02,29));
        desinscriptionEntity.setEspaceOrganisation(espaceOrganisationEntity);

        // pieceDto
        List<DesinscriptionPieceDto> pieces = new ArrayList<>();
        desinscriptionDto.setPieces(pieces);

        // Initialisation des informations lorsqu'un répo est appellé
        Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findById(ID_ESPACE_ORGANISATION);
        Mockito.doReturn(espaceOrganisationDto).when(this.espaceOrganisationTransformer).modelToDto(espaceOrganisationEntity);
        Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionTransformer).dtoToModel(desinscriptionDto);
        
        Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionRepository).save(desinscriptionEntity);

        // apelle de la méthode
        this.desinscriptionService.saveDesinscription(desinscriptionDto, pieces, ID_DECLARANT, ID_ESPACE_ORGANISATION);

        // assertion
        Assert.assertEquals(OrigineSaisieEnum.AGENT, desinscriptionEntity.getOrigineSaisie());
        Assert.assertEquals(StatutEspaceEnum.DESINSCRIPTION_VALIDEE_HATVP, espaceOrganisationEntity.getStatut());
        Assert.assertEquals(desinscriptionDto.getCessationDate(), espaceOrganisationEntity.getExercicesComptable().get(3).getDateFin()); 
    }

    /**
     * Test validation par un agent d'une désinscription saisie par un CO avec une mise à jour des exercices comptable
     */
    @Test
    public void testSaveDesinscriptionValidationParAgentSansMiseAJOurExerciecComptableCessationDansPasse(){
    	// desinscriptionDto
        DesinscriptionDto desinscriptionDto = new DesinscriptionDto();
        desinscriptionDto.setId(1L);
        desinscriptionDto.setCessationDate(LocalDate.of(2020,02,29));

        List<ExerciceComptableEntity> listExerciceComptableEntities = new ArrayList<ExerciceComptableEntity>();
    	
    	ExerciceComptableEntity exerciceComptableEntity2017 = new ExerciceComptableEntity();
    	exerciceComptableEntity2017.setDateDebut(LocalDate.of(2017,07,01));
    	exerciceComptableEntity2017.setDateFin(LocalDate.of(2017,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2017);
    	
    	ExerciceComptableEntity exerciceComptableEntity2018 = new ExerciceComptableEntity();
    	exerciceComptableEntity2018.setDateDebut(LocalDate.of(2018,01,01));
    	exerciceComptableEntity2018.setDateFin(LocalDate.of(2018,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2018);
    	
    	ExerciceComptableEntity exerciceComptableEntity2019 = new ExerciceComptableEntity();
    	exerciceComptableEntity2019.setDateDebut(LocalDate.of(2019,01,01));
    	exerciceComptableEntity2019.setDateFin(LocalDate.of(2019,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2019);
    	
    	ExerciceComptableEntity exerciceComptableEntity2020 = new ExerciceComptableEntity();
    	exerciceComptableEntity2020.setDateDebut(LocalDate.of(2020,01,01));
    	exerciceComptableEntity2020.setDateFin(LocalDate.of(2020,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2020);
    	
        // espaceOrganisationEntity
        EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
        espaceOrganisationEntity.setExercicesComptable(listExerciceComptableEntities);        

        EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
        espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
        desinscriptionDto.setEspaceOrganisation(espaceOrganisationDto);

        // desinscriptionEntity
        DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
        desinscriptionEntity.setId(1L);
        desinscriptionEntity.setCessationDate(LocalDate.of(2020,02,29));
        desinscriptionEntity.setOrigineSaisie(OrigineSaisieEnum.DECLARANT);
        desinscriptionEntity.setEspaceOrganisation(espaceOrganisationEntity);

        // pieceDto
        List<DesinscriptionPieceDto> pieces = new ArrayList<>();
        desinscriptionDto.setPieces(pieces);

        // Initialisation des informations lorsqu'un répo est appellé
        Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findById(ID_ESPACE_ORGANISATION);
        Mockito.doReturn(espaceOrganisationDto).when(this.espaceOrganisationTransformer).modelToDto(espaceOrganisationEntity);
        Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionTransformer).dtoToModel(desinscriptionDto);
        
        Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionRepository).save(desinscriptionEntity);
        
        Mockito.doNothing().when(this.desinscriptionMailer).sendDesinscriptionValidationMailDRI(anyObject(), anyObject());
        
        // apelle de la méthode
        this.desinscriptionService.saveDesinscription(desinscriptionDto, pieces, ID_DECLARANT, ID_ESPACE_ORGANISATION);

        // assertion
        Assert.assertEquals(desinscriptionDto.getCessationDate(), espaceOrganisationEntity.getExercicesComptable().get(3).getDateFin());
        Assert.assertEquals(StatutEspaceEnum.DESINSCRIT, espaceOrganisationEntity.getStatut());    	
    }
    
    /**
     * Test validation par un agent d'une désinscription saisie par un CO avec une mise à jour des exercices comptable
     */
    @Test
    public void testSaveDesinscriptionValidationParAgentSansMiseAJOurExerciecComptableCessationDansFutur(){
    	// desinscriptionDto
        DesinscriptionDto desinscriptionDto = new DesinscriptionDto();
        desinscriptionDto.setId(1L);
        desinscriptionDto.setCessationDate(LocalDate.of(2060,02,29));

        List<ExerciceComptableEntity> listExerciceComptableEntities = new ArrayList<ExerciceComptableEntity>();
    	
    	ExerciceComptableEntity exerciceComptableEntity2017 = new ExerciceComptableEntity();
    	exerciceComptableEntity2017.setDateDebut(LocalDate.of(2017,07,01));
    	exerciceComptableEntity2017.setDateFin(LocalDate.of(2017,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2017);
    	
    	ExerciceComptableEntity exerciceComptableEntity2018 = new ExerciceComptableEntity();
    	exerciceComptableEntity2018.setDateDebut(LocalDate.of(2018,01,01));
    	exerciceComptableEntity2018.setDateFin(LocalDate.of(2018,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2018);
    	
    	ExerciceComptableEntity exerciceComptableEntity2019 = new ExerciceComptableEntity();
    	exerciceComptableEntity2019.setDateDebut(LocalDate.of(2019,01,01));
    	exerciceComptableEntity2019.setDateFin(LocalDate.of(2019,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2019);
    	
    	ExerciceComptableEntity exerciceComptableEntity2060 = new ExerciceComptableEntity();
    	exerciceComptableEntity2060.setDateDebut(LocalDate.of(2060,01,01));
    	exerciceComptableEntity2060.setDateFin(LocalDate.of(2060,12,31));
    	listExerciceComptableEntities.add(exerciceComptableEntity2060);
    	
        // espaceOrganisationEntity
        EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
        espaceOrganisationEntity.setExercicesComptable(listExerciceComptableEntities);        

        EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
        espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
        desinscriptionDto.setEspaceOrganisation(espaceOrganisationDto);

        // desinscriptionEntity
        DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
        desinscriptionEntity.setId(1L);
        desinscriptionEntity.setCessationDate(LocalDate.of(2060,02,29));
        desinscriptionEntity.setOrigineSaisie(OrigineSaisieEnum.DECLARANT);
        desinscriptionEntity.setEspaceOrganisation(espaceOrganisationEntity);

        // pieceDto
        List<DesinscriptionPieceDto> pieces = new ArrayList<>();
        desinscriptionDto.setPieces(pieces);

        // Initialisation des informations lorsqu'un répo est appellé
        Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findById(ID_ESPACE_ORGANISATION);
        Mockito.doReturn(espaceOrganisationDto).when(this.espaceOrganisationTransformer).modelToDto(espaceOrganisationEntity);
        Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionTransformer).dtoToModel(desinscriptionDto);
        
        Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionRepository).save(desinscriptionEntity);

        // apelle de la méthode
        this.desinscriptionService.saveDesinscription(desinscriptionDto, pieces, ID_DECLARANT, ID_ESPACE_ORGANISATION);

        // assertion
        Assert.assertEquals(desinscriptionDto.getCessationDate(), espaceOrganisationEntity.getExercicesComptable().get(3).getDateFin());
        Assert.assertEquals(StatutEspaceEnum.DESINSCRIPTION_VALIDEE_HATVP, espaceOrganisationEntity.getStatut());    	
    }
    
    /**
     * public void rejectDemande(long desinscriptionId) 
     */
    @Test
    public void rejectDemande() {
    	
    	EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
    	espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
    	espaceOrganisationEntity.setStatut(StatutEspaceEnum.DESINSCRIPTION_DEMANDEE);
    	
    	DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
    	desinscriptionEntity.setId(ID_DESINSCRIPTION);
    	desinscriptionEntity.setEspaceOrganisation(espaceOrganisationEntity);
    	
    	List<DesinscriptionPieceEntity> desinscriptionPieceEntity = new ArrayList<DesinscriptionPieceEntity>();
    	
    	Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionRepository).findById(ID_DESINSCRIPTION);
    	Mockito.doReturn(desinscriptionPieceEntity).when(this.desinscriptionPieceRepository).findByDesinscription(desinscriptionEntity);
    	Mockito.doNothing().when(this.desinscriptionPieceRepository).delete(desinscriptionPieceEntity);
    	Mockito.doNothing().when(this.desinscriptionRepository).delete(desinscriptionEntity);
    	Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).save(espaceOrganisationEntity);    	
   	
    	this.desinscriptionService.rejectDemande(ID_DESINSCRIPTION);
    	
    	Assert.assertEquals(StatutEspaceEnum.ACCEPTEE, espaceOrganisationEntity.getStatut());  
    }
    
    /**
     * public void verserPiecesComplementairesDesinscription(Long idEspaceOrganisation, Long idDeclarant, List<DesinscriptionPieceDto> pieces) 
     * Cas où l'espace n'est pas COMPLEMENTS_DEMANDE_DESINSCRIPTION => exception
     */
    @Test
    public void verserPiecesComplementairesDesinscriptionFailStatutDifferentDeCOMPLEMENTS_DEMANDE_DESINSCRIPTION() {
    	// espaceOrganisationEntity             
        EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
        espaceOrganisationEntity.setStatut(StatutEspaceEnum.DESINSCRIPTION_DEMANDEE);
        
        DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
    	desinscriptionEntity.setId(ID_DESINSCRIPTION);
    	desinscriptionEntity.setEspaceOrganisation(espaceOrganisationEntity);
    	
    	List<DesinscriptionPieceDto> pieces = new ArrayList<>();
        DesinscriptionPieceDto desinscriptionPieceDto = new DesinscriptionPieceDto();
        desinscriptionPieceDto.setContenuFichier(new byte[500]);
        desinscriptionPieceDto.setMediaType("application/pdf");
        desinscriptionPieceDto.setNomFichier("test");
        desinscriptionPieceDto.setDateVersementPiece(new Date());
        desinscriptionPieceDto.setDeclarantId(ID_DECLARANT);
        desinscriptionPieceDto.setId(ID_DESINSCRIPTION_PIECE);
        desinscriptionPieceDto.setOrigineVersement(OrigineVersementEnum.DEMANDE_DESINSCRIPTION_ESPACE);
        pieces.add(desinscriptionPieceDto);
    	
        Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
    	Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionRepository).findByEspaceOrganisation(espaceOrganisationEntity);
 	
    	try {
    		this.desinscriptionService.verserPiecesComplementairesDesinscription(ID_ESPACE_ORGANISATION, ID_DECLARANT, pieces);
          }catch(BusinessGlobalException aExp){
        	assertThat(ErrorMessageEnum.AJOUT_COMPLEMENT_INTERDIT.getMessage()).isEqualTo(aExp.getMessage());
          }	
    }
    
    /**
     * public void verserPiecesComplementairesDesinscription(Long idEspaceOrganisation, Long idDeclarant, List<DesinscriptionPieceDto> pieces) 
     * Cas où COMPLEMENTS_DEMANDE_DESINSCRIPTION
     */
    @Test
    public void verserPiecesComplementairesDesinscriptionStatutDifferentDeCOMPLEMENTS_DEMANDE_DESINSCRIPTION() {
    	// espaceOrganisationEntity
             
        EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
        espaceOrganisationEntity.setStatut(StatutEspaceEnum.COMPLEMENTS_DEMANDE_DESINSCRIPTION);
        
        DesinscriptionEntity desinscriptionEntity = new DesinscriptionEntity();
    	desinscriptionEntity.setId(ID_DESINSCRIPTION);
    	desinscriptionEntity.setEspaceOrganisation(espaceOrganisationEntity);
    	
    	List<DesinscriptionPieceDto> pieces = new ArrayList<>();
        DesinscriptionPieceDto desinscriptionPieceDto = new DesinscriptionPieceDto();
        desinscriptionPieceDto.setContenuFichier(new byte[500]);
        desinscriptionPieceDto.setMediaType("application/pdf");
        desinscriptionPieceDto.setNomFichier("test");
        desinscriptionPieceDto.setDateVersementPiece(null);
        desinscriptionPieceDto.setDeclarantId(ID_DECLARANT);
        desinscriptionPieceDto.setId(ID_DESINSCRIPTION_PIECE);
        desinscriptionPieceDto.setOrigineVersement(null);
        pieces.add(desinscriptionPieceDto);
        
        DesinscriptionPieceEntity desinscriptionPieceEntity = new DesinscriptionPieceEntity();       
    	
        Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
    	Mockito.doReturn(desinscriptionEntity).when(this.desinscriptionRepository).findByEspaceOrganisation(espaceOrganisationEntity);
    	Mockito.doReturn(desinscriptionPieceEntity).when(this.desinscriptionPieceTransformer).dtoToModel(pieces.get(0));	
    	
    	this.desinscriptionService.verserPiecesComplementairesDesinscription(ID_ESPACE_ORGANISATION, ID_DECLARANT, pieces);
    	
    	Assert.assertEquals(StatutEspaceEnum.COMPLEMENTS_DEMANDE_DESINSCRIPTION_RECU, espaceOrganisationEntity.getStatut());
    	Assert.assertEquals(OrigineVersementEnum.DEMANDE_DESINSCRIPTION_ESPACE, desinscriptionEntity.getPieces().get(0).getOrigineVersement()); 
    }

}
