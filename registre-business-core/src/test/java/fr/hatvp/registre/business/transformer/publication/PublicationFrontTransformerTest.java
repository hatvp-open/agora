/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.publication;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationFrontTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.publication.PublicationFrontDto;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * Test de la classe {@link PublicationFrontTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class PublicationFrontTransformerTest {

    /** Transformer des publications. */
    @Autowired
    private PublicationFrontTransformer publicationTransformer;

    /**
     * Test de la métode modelToDto de {@link PublicationTransformer#dtoToModel(AbstractCommonDto)}
     */
    @Test
    public void testModelToDto() {
        final PublicationEntity entity = CommonTestClass.getPublicationEntity();

        final PublicationFrontDto dto = this.publicationTransformer.modelToDto(entity);

        Assert.assertNotNull(dto);

        Assert.assertNull(dto.getId());
        Assert.assertNull(dto.getVersion());
        Assert.assertNotEquals(entity.getId(), dto.getId());
        Assert.assertNotEquals(entity.getVersion(), dto.getVersion());

        Assert.assertEquals(entity.getAdresse(), dto.getAdresse());
        Assert.assertEquals(entity.getCodePostal(), dto.getCodePostal());
        Assert.assertEquals(entity.getVille(), dto.getVille());
        Assert.assertEquals(entity.getPays(), dto.getPays());

        Assert.assertEquals(entity.getTelephoneDeContact(), dto.getTelephoneDeContact());
        Assert.assertEquals(entity.getEmailDeContact(), dto.getEmailDeContact());
        Assert.assertEquals(entity.getLienSiteWeb(), dto.getLienSiteWeb());
        Assert.assertEquals(entity.getLienPageTwitter(), dto.getLienPageTwitter());
        Assert.assertEquals(entity.getLienPageLinkedin(), dto.getLienPageLinkedin());
        Assert.assertEquals(entity.getLienListeTiers(), dto.getLienListeTiers());
        Assert.assertEquals(entity.getLienPageFacebook(), dto.getLienPageFacebook());

        Assert.assertEquals(entity.getLogo(), dto.getLogo());
        Assert.assertEquals(entity.getLogoType(), dto.getLogoType());

        Assert.assertEquals(2, dto.getDirigeants().size());
        Assert.assertEquals(2, dto.getCollaborateurs().size());
        Assert.assertEquals(2, dto.getClients().size());
        Assert.assertEquals(2, dto.getAffiliations().size());
    }

    /**
     * Test de la métode modelToDto de {@link PublicationTransformer#modelToDto(AbstractCommonEntity)}
     */
    @Test
    public void testDtoToModel() {

        final PublicationFrontDto dto = CommonTestClass.getFrontPublicationDto();

        final PublicationEntity entity = this.publicationTransformer.dtoToModel(dto);

        Assert.assertNotNull(entity);

        Assert.assertEquals(dto.getId(), entity.getId());
        Assert.assertNull(entity.getVersion());

        Assert.assertEquals(dto.getLogo(), entity.getLogo());
        Assert.assertEquals(dto.getLogoType(), entity.getLogoType());

    }
}
