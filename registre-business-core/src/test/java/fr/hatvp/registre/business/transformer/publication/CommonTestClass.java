/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.publication;

import static fr.hatvp.registre.commons.utils.RegistreUtils.randomString;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.commons.dto.publication.PubActiviteDto;
import fr.hatvp.registre.commons.dto.publication.PubCollaborateurDto;
import fr.hatvp.registre.commons.dto.publication.PubDirigeantDto;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.commons.dto.publication.PublicationFrontDto;
import fr.hatvp.registre.commons.dto.publication.PublicationRechercheDto;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ActionMeneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ChiffreAffaireEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DecisionConcerneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.MontantDepenseEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ResponsablePublicEntity;
import fr.hatvp.registre.persistence.entity.publication.PubAssociationEntity;
import fr.hatvp.registre.persistence.entity.publication.PubClientEntity;
import fr.hatvp.registre.persistence.entity.publication.PubCollaborateurEntity;
import fr.hatvp.registre.persistence.entity.publication.PubDirigeantEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity;

/**
 * Classe contenat les méthodes communes pour récupérer des instances.
 *
 * @version $Revision$ $Date${0xD}
 */
public class CommonTestClass {

	/** Id de l'espace organisation. */
	private final static Long ESPACE_TEST_ID = 1L;

	/** Id de l'organisation. */
	private final static Long ORGA_TEST_ID = 1L;

	/** Nom d'usage utilisé pour les tests */
	private final static String NOMUSAGE_TEST = "Organisation de test";

	/**
	 * Récupérer une instance de {@link AssoClientDto}
	 */
	static AssoClientDto getClientDto() {
		final AssoClientDto dto = new AssoClientDto();
		dto.setId(RegistreUtils.randomLong(1));
		dto.setVersion(RegistreUtils.randomLong(2).intValue());
		dto.setOrganisationId(ORGA_TEST_ID);
		dto.setEspaceOrganisationId(ESPACE_TEST_ID);
		return dto;
	}

	/**
	 * Récupérer une instance de {@link EspaceOrganisationEntity}
	 */
	private static EspaceOrganisationEntity getEspaceEntity() {
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_TEST_ID);
		espaceOrganisationEntity.setVersion(RegistreUtils.randomLong(2).intValue());
		espaceOrganisationEntity.setAdresse(RegistreUtils.randomString(20));
		espaceOrganisationEntity.setVille(RegistreUtils.randomString(30));
		espaceOrganisationEntity.setCodePostal(RegistreUtils.randomString(4));
		espaceOrganisationEntity.setNomUsageHatvp("hatvp");
		espaceOrganisationEntity.setAncienNomHatvp("hatvp");
		espaceOrganisationEntity.setSigleHatvp("hatvp");

		espaceOrganisationEntity.setOrganisation(getOrganisationEntity());

		return espaceOrganisationEntity;
	}

	/**
	 * Récupérer une instance de {@link OrganisationEntity}
	 */
	private static OrganisationEntity getOrganisationEntity() {

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ORGA_TEST_ID);
		organisationEntity.setDenomination("TEST");
		organisationEntity.setSiren(RegistreUtils.randomString(10));
		organisationEntity.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
		return organisationEntity;
	}

	/**
	 * Récupérer une instance de {@link PubClientEntity}
	 */
	static PublicationEntity getPublicationEntity() {
		final PublicationEntity publicationEntity = new PublicationEntity();
		publicationEntity.setId(RegistreUtils.randomLong(1));
		publicationEntity.setVersion(RegistreUtils.randomLong(2).intValue());

		publicationEntity.setSecteursActivites(SecteurActiviteEnum.AGRI.name());
		publicationEntity.setNiveauIntervention(NiveauInterventionEnum.MONDIAL.name());

		publicationEntity.setAdresse(RegistreUtils.randomString(20));
		publicationEntity.setCodePostal(RegistreUtils.randomString(4));
		publicationEntity.setVille(RegistreUtils.randomString(4));
		publicationEntity.setPays(RegistreUtils.randomString(4));
		publicationEntity.setEspaceOrganisation(getEspaceEntity());

		publicationEntity.setTelephoneDeContact(RegistreUtils.randomString(10));
		publicationEntity.setEmailDeContact(RegistreUtils.randomString(10));
		publicationEntity.setLienSiteWeb(RegistreUtils.randomString(10));
		publicationEntity.setLienPageTwitter(RegistreUtils.randomString(10));
		publicationEntity.setLienPageLinkedin(RegistreUtils.randomString(10));
		publicationEntity.setLienListeTiers(RegistreUtils.randomString(10));
		publicationEntity.setLienPageFacebook(RegistreUtils.randomString(10));
		publicationEntity.setPublicateur(getPublicateur());
		publicationEntity.setNomUsage(NOMUSAGE_TEST);

		final List<PubClientEntity> clientEntities = new ArrayList<>();
		clientEntities.add(getPubClientEntity());
		clientEntities.add(getPubClientEntity());
		publicationEntity.setClients(clientEntities);

		final List<PubAssociationEntity> associationEntities = new ArrayList<>();
		associationEntities.add(getPubAsociationEntity());
		associationEntities.add(getPubAsociationEntity());
		publicationEntity.setAssociations(associationEntities);

		final List<PubDirigeantEntity> dirigeantEntities = new ArrayList<>();
		dirigeantEntities.add(getDirigeantEntity());
		dirigeantEntities.add(getDirigeantEntity());
		publicationEntity.setDirigeants(dirigeantEntities);

		final List<PubCollaborateurEntity> collaborateurEntities = new ArrayList<>();
		collaborateurEntities.add(getCollaborateur());
		collaborateurEntities.add(getCollaborateur());
		publicationEntity.setCollaborateurs(collaborateurEntities);

		publicationEntity.setLogo(RegistreUtils.randomByteArray(20));
		publicationEntity.setLogoType(RegistreUtils.randomString(30));

		publicationEntity.setCategorieOrganisation(TypeOrganisationEnum.ASSOC);

		return publicationEntity;
	}

	/**
	 * Récuéprer uen instance de {@link fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity}
	 */
	private static DeclarantEntity getPublicateur() {
		final DeclarantEntity entity = new DeclarantEntity();
		entity.setNom(RegistreUtils.randomString(10));
		entity.setPrenom(RegistreUtils.randomString(10));
		entity.setEmail(RegistreUtils.randomString(20));
		return entity;
	}

	/**
	 * Récupérer une instance de {@link PubClientEntity}
	 */
	static PubClientEntity getPubClientEntity() {
		final PubClientEntity pubClientEntity = new PubClientEntity();
		pubClientEntity.setOrganisation(getOrganisationEntity());
		pubClientEntity.setEspaceOrganisation(getEspaceEntity());
		pubClientEntity.setId(RegistreUtils.randomLong(1));
		pubClientEntity.setVersion(RegistreUtils.randomLong(2).intValue());
		return pubClientEntity;
	}

	/**
	 * Récupérer une instance de {@link PubAssociationEntity}
	 */
	static PubAssociationEntity getPubAsociationEntity() {
		final PubAssociationEntity entity = new PubAssociationEntity();
		entity.setOrganisation(getOrganisationEntity());
		entity.setEspaceOrganisation(getEspaceEntity());
		entity.setId(RegistreUtils.randomLong(1));
		entity.setVersion(RegistreUtils.randomLong(2).intValue());
		return entity;
	}

	/**
	 * Récupérer une instance de {@link PubDirigeantEntity}
	 */
	static PubDirigeantEntity getDirigeantEntity() {
		final PubDirigeantEntity entity = new PubDirigeantEntity();
		entity.setId(RegistreUtils.randomLong(1));
		entity.setVersion(RegistreUtils.randomLong(2).intValue());
		entity.setNom(randomString(20));
		entity.setPrenom(randomString(20));

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(RegistreUtils.randomLong(1));
		entity.setEspaceOrganisation(espaceOrganisationEntity);
		return entity;
	}

	/**
	 * Récupérer une instance de {@link DirigeantDto}
	 */
	static DirigeantDto getDirigeantDto() {
		final DirigeantDto dto = new DirigeantDto();
		dto.setId(RegistreUtils.randomLong(1));
		dto.setVersion(RegistreUtils.randomLong(2).intValue());
		dto.setNom(randomString(20));
		dto.setPrenom(randomString(20));
		return dto;
	}

	/**
	 * Récupérer une instance de {@link PubCollaborateurEntity}
	 */
	static PubCollaborateurEntity getCollaborateur() {
		final PubCollaborateurEntity collaborateurEntity = new PubCollaborateurEntity();
		collaborateurEntity.setActif(true);
		collaborateurEntity.setCivility(CiviliteEnum.M);
		collaborateurEntity.setEmail(RegistreUtils.randomString(30));
		collaborateurEntity.setFonction(RegistreUtils.randomString(20));
		collaborateurEntity.setNom(RegistreUtils.randomString(20));
		collaborateurEntity.setPrenom(RegistreUtils.randomString(20));
		collaborateurEntity.setId(RegistreUtils.randomLong(2));
		collaborateurEntity.setVersion(RegistreUtils.randomLong(2).intValue());
		collaborateurEntity.setEspaceOrganisation(getEspaceEntity());
		return collaborateurEntity;
	}

	/**
	 * Récupérer une instance de {@link PublicationDto}
	 */
	static PublicationDto getPublicationDto() {
		final PublicationDto dto = new PublicationDto();

		dto.setId(RegistreUtils.randomLong(2));
		dto.setVersion(RegistreUtils.randomLong(2).intValue());

		final PubActiviteDto pubActiviteDto = new PubActiviteDto();
		pubActiviteDto.getListSecteursActivites().add(SecteurActiviteEnum.AGRI);
		pubActiviteDto.getListNiveauIntervention().add(NiveauInterventionEnum.EUROPEEN);
		dto.setActivites(pubActiviteDto);

		dto.setCategorieOrganisation(TypeOrganisationEnum.ASSOC);

		dto.setAdresse(RegistreUtils.randomString(20));
		dto.setPublierMonAdressePhysique(true);
		dto.setCodePostal(RegistreUtils.randomString(5));
		dto.setVille(RegistreUtils.randomString(20));
		dto.setPays(RegistreUtils.randomString(40));

		dto.setCreationDate(new Date());
		dto.setDenomination(RegistreUtils.randomString(60));
		dto.setNationalId(RegistreUtils.randomString(9));
		dto.setTypeIdentifiantNational(TypeIdentifiantNationalEnum.SIREN);

		dto.setEmailDeContact(RegistreUtils.randomString(30));
		dto.setPublierMonAdresseEmail(true);
		dto.setTelephoneDeContact(RegistreUtils.randomString(40));
		dto.setPublierMonTelephoneDeContact(true);

		dto.setDeclarationOrgaAppartenance(false);
		dto.setDeclarationTiers(false);

		dto.setNomUsage(NOMUSAGE_TEST);

		final PubDirigeantDto dirigeantDto = new PubDirigeantDto();
		dirigeantDto.setCivilite(CiviliteEnum.M);
		dirigeantDto.setFonction(RegistreUtils.randomString(20));
		dirigeantDto.setNom(RegistreUtils.randomString(20));
		dirigeantDto.setPrenom(RegistreUtils.randomString(20));
		final List<PubDirigeantDto> dirigeantDtos = new ArrayList<>();
		dirigeantDtos.add(dirigeantDto);
		dto.setDirigeants(dirigeantDtos);

		final PubCollaborateurDto collaborateurDto = new PubCollaborateurDto();
		collaborateurDto.setFonction(RegistreUtils.randomString(20));
		collaborateurDto.setNom(RegistreUtils.randomString(20));
		collaborateurDto.setPrenom(RegistreUtils.randomString(20));
		final List<PubCollaborateurDto> collaborateurDtos = new ArrayList<>();
		collaborateurDtos.add(collaborateurDto);
		dto.setCollaborateurs(collaborateurDtos);

		dto.setPublisher(new DeclarantDto());

		return dto;
	}

	/**
	 * Récupérer une instance de {@link PublicationFrontDto}
	 */
	static PublicationFrontDto getFrontPublicationDto() {
		final PublicationFrontDto frontDto = new PublicationFrontDto();

		frontDto.setLogo(RegistreUtils.randomByteArray(30));
		frontDto.setLogoType(RegistreUtils.randomString(5));

		frontDto.setPublisher(new DeclarantDto());

		return frontDto;
	}

	/**
	 * Récupérer une instance de {@link PublicationRechercheDto}
	 */
	static PublicationRechercheDto getPubRechercheDto() {
		final PublicationRechercheDto dto = new PublicationRechercheDto();

		final PubActiviteDto pubActiviteDto = new PubActiviteDto();
		pubActiviteDto.getListNiveauIntervention().add(NiveauInterventionEnum.EUROPEEN);
		pubActiviteDto.getListSecteursActivites().add(SecteurActiviteEnum.AMENAGEMENT);

		dto.setDenomination(RegistreUtils.randomString(30));
		dto.setCategorieOrganisation(TypeOrganisationEnum.ASSOC);
		dto.setNationalId(RegistreUtils.randomString(10));
		dto.setCreationDate(new Date());

		return dto;
	}

	/**
	 * Récupérer une instance de {@link fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity}
	 */
	static PublicationExerciceEntity getPublicationExerciceEntity() {
		final PublicationExerciceEntity publicationExerciceEntity = new PublicationExerciceEntity();
		publicationExerciceEntity.setId(RegistreUtils.randomLong(1));
		publicationExerciceEntity.setVersion(RegistreUtils.randomLong(2).intValue());

		/* data */
		publicationExerciceEntity.setNonDeclarationMoyens(false);
		ExerciceComptableEntity ex = getExerciceComptableEntity();
		publicationExerciceEntity.setExerciceComptable(ex);
		publicationExerciceEntity.setVersionExercice(1);
		publicationExerciceEntity.setPublicationJson("{\"dateDebut\":\"01-01-2019\",\"dateFin\":\"31-12-2019\",\"chiffreAffaire\":\"> = 100 000 euros et < 500 000 euros\",\"hasNotChiffreAffaire\":false,\"montantDepense\":\"> = 300 000 euros et < 400 000 euros\",\"nombreSalaries\":9999,\"exerciceId\":4884,\"noActivite\":false,\"nombreActivite\":10,\"commentaire\":\"test modification trois\",\"defautDeclaration\":false}");

		/* meta data */
		publicationExerciceEntity.setCreationDate(RegistreUtils.generateNewDate(RegistreUtils.randomLong(2).intValue()));
		publicationExerciceEntity.setDeleted(false);
		publicationExerciceEntity.setDepublicateur(null);
		publicationExerciceEntity.setDepublicationDate(null);
		publicationExerciceEntity.setPublicateur(new DeclarantEntity(RegistreUtils.randomLong(1)));
		publicationExerciceEntity.setRepublicateur(null);
		publicationExerciceEntity.setRepublicationDate(null);
		publicationExerciceEntity.setStatut(StatutPublicationEnum.PUBLIEE);

		return publicationExerciceEntity;
	}

	/**
	 * Récupérer une instance de {@link fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity}
	 */
	static PublicationActiviteEntity getPublicationActiviteEntity() {
		final PublicationActiviteEntity publicationActiviteEntity = new PublicationActiviteEntity();
		publicationActiviteEntity.setId(RegistreUtils.randomLong(1));
		publicationActiviteEntity.setVersion(RegistreUtils.randomLong(2).intValue());

		/* data */
		publicationActiviteEntity.setActivite(getActiviteRepresentationInteretEntity());
		publicationActiviteEntity.setVersionActivite(1);
		publicationActiviteEntity.setPublicationJson(
				"{\"identifiantFiche\":\"SQV7REVY\",\"objet\":\"une fiche pour vérifier que les RP sont bien affichés avec leur parent\",\"domainesIntervention\":[\"Industrie aéronautique\"],\"actionsRepresentationInteret\":[{\"reponsablesPublics\":[\"MEMBRES DU GOUVERNEMENT OU MEMBRES DE CABINET MINISTÉRIEL - Premier ministre, Education nationale, enseignement supérieur et recherche\",\"RESPONSABLES DES AUTORITÉS ADMINISTRATIVES INDÉPENDANTES ET AUTORITÉS ADMINISTRATIVES INDÉPENDANTES - Haute Autorité pour la transparence de la vie publique, Autorité de sûreté nucléaire, Haute Autorité pour la diffusion des œuvres et la protection des droits sur internet\",\"Titulaire d'un emploi à la décision du Gouvernement, Député ; sénateur ; collaborateur parlementaire ou agents des services des assemblées parlementaires\"],\"decisionsConcernees\":[\"Lois, y compris constitutionnelles\",\"Actes réglementaires\"],\"actionsMenees\":[\"Organiser des discussions informelles ou des réunions en tête-à-tête\",\"Convenir pour un tiers d'une entrevue avec le titulaire d'une charge publique\"],\"tiers\":[\"HARIBO RICQLES ZAN\"]}]}");

		/* meta data */
		publicationActiviteEntity.setCreationDate(RegistreUtils.generateNewDate(RegistreUtils.randomLong(2).intValue()));
		publicationActiviteEntity.setDeleted(false);
		publicationActiviteEntity.setDepublicateur(null);
		publicationActiviteEntity.setDepublicationDate(null);
		publicationActiviteEntity.setPublicateur(new DeclarantEntity(RegistreUtils.randomLong(1)));
		publicationActiviteEntity.setRepublicateur(null);
		publicationActiviteEntity.setRepublicationDate(null);
		publicationActiviteEntity.setStatut(StatutPublicationEnum.PUBLIEE);

		return publicationActiviteEntity;
	}

	/**
	 * Récupérer une instance de {@link ExerciceComptableEntity}
	 */
	static ExerciceComptableEntity getExerciceComptableEntity() {
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity(1L);
		exerciceComptableEntity.setDateDebut(LocalDate.of(2018, 4, 1));
		exerciceComptableEntity.setDateFin(LocalDate.of(2019, 3, 31));
		exerciceComptableEntity.setEspaceOrganisation(getEspaceEntity());
		ChiffreAffaireEntity chiffreAffaire = new ChiffreAffaireEntity(1L);
		chiffreAffaire.setLibelle("testChiffreAffaire");
		exerciceComptableEntity.setChiffreAffaire(chiffreAffaire);
		MontantDepenseEntity montant = new MontantDepenseEntity(1L);
		montant.setLibelle("testMontant");
		exerciceComptableEntity.setMontantDepense(montant);
		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity(1L);
		DeclarantEntity declarantEntity = new DeclarantEntity(1L);
		declarantEntity.setNom("Bla");
		declarantEntity.setPrenom("Blo");
		activiteRepresentationInteretEntity.setCreateurDeclaration(declarantEntity);
		activiteRepresentationInteretEntity.setDomainesIntervention(new HashSet<>(Arrays.asList(new DomaineInterventionEntity(1L), new DomaineInterventionEntity(3L))));
		activiteRepresentationInteretEntity.setExerciceComptable(exerciceComptableEntity);
		exerciceComptableEntity.getActivitesRepresentationInteret().add(activiteRepresentationInteretEntity);
		exerciceComptableEntity.setHasNotChiffreAffaire(false);
		return exerciceComptableEntity;

	}

	/**
	 * Récupérer une instance de {@link ActiviteRepresentationInteretEntity}
	 */
	static ActiviteRepresentationInteretEntity getActiviteRepresentationInteretEntity() {
		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity(RegistreUtils.randomLong(1));

		activiteRepresentationInteretEntity.setId(RegistreUtils.randomLong(1));
		activiteRepresentationInteretEntity.setVersion(1);

		activiteRepresentationInteretEntity.setCreateurDeclaration(getPublicateur());
		activiteRepresentationInteretEntity.setCreationDate(RegistreUtils.generateNewDate(0));
		activiteRepresentationInteretEntity.setExerciceComptable(getExerciceComptableEntity());

		ActionRepresentationInteretEntity action = getActionRepresentationInteretEntity();
		action.setActiviteRepresentationInteret(activiteRepresentationInteretEntity);
		ActionRepresentationInteretEntity action2 = getActionRepresentationInteretEntity();
		action2.setActiviteRepresentationInteret(activiteRepresentationInteretEntity);
		List<ActionRepresentationInteretEntity> actions = new ArrayList<>(Arrays.asList(action, action2));
		activiteRepresentationInteretEntity.setActionsRepresentationInteret(actions);

		DomaineInterventionEntity domaine = new DomaineInterventionEntity(RegistreUtils.randomLong(1));
		domaine.setVersion(1);
		domaine.setLibelle("domaineLibelle" + RegistreUtils.randomString(8));
		DomaineInterventionEntity domaine2 = new DomaineInterventionEntity(RegistreUtils.randomLong(1));
		domaine2.setVersion(1);
		domaine2.setLibelle("domaineLibelle" + RegistreUtils.randomString(8));
		Set<DomaineInterventionEntity> domaines = new HashSet<>(Arrays.asList(domaine, domaine2));
		activiteRepresentationInteretEntity.setDomainesIntervention(domaines);

		activiteRepresentationInteretEntity.setObjet("testObjet");
		activiteRepresentationInteretEntity.setStatut(StatutPublicationEnum.PUBLIEE);

		return activiteRepresentationInteretEntity;

	}

	/**
	 * Récupérer une instance de {@link fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity}
	 */
	static ActionRepresentationInteretEntity getActionRepresentationInteretEntity() {
		ActionRepresentationInteretEntity actionRepresentationInteretEntity = new ActionRepresentationInteretEntity(RegistreUtils.randomLong(1));
		actionRepresentationInteretEntity.setVersion(1);

		ActionMeneeEntity actionMeneeEntity = new ActionMeneeEntity(RegistreUtils.randomLong(1));
		actionMeneeEntity.setVersion(1);
		actionMeneeEntity.setLibelle("libelleActionMenee" + RegistreUtils.randomString(8));
		ActionMeneeEntity actionMeneeEntity2 = new ActionMeneeEntity(RegistreUtils.randomLong(1));
		actionMeneeEntity2.setVersion(1);
		actionMeneeEntity2.setLibelle("libelleActionMenee" + RegistreUtils.randomString(8));
		Set<ActionMeneeEntity> actionsMenee = new HashSet<>(Arrays.asList(actionMeneeEntity, actionMeneeEntity2));
		actionRepresentationInteretEntity.setActionsMenees(actionsMenee);
		actionRepresentationInteretEntity.setActionMeneeAutre("autreAction");

		DecisionConcerneeEntity decision = new DecisionConcerneeEntity(RegistreUtils.randomLong(1));
		decision.setVersion(1);
		decision.setLibelle("decisionLibelle" + RegistreUtils.randomString(8));
		DecisionConcerneeEntity decision2 = new DecisionConcerneeEntity(RegistreUtils.randomLong(1));
		decision2.setVersion(1);
		decision2.setLibelle("decisionLibelle" + RegistreUtils.randomString(8));
		Set<DecisionConcerneeEntity> decisions = new HashSet<>(Arrays.asList(decision, decision2));
		actionRepresentationInteretEntity.setDecisionsConcernees(decisions);

		OrganisationEntity organisation = getOrganisationEntity();
		OrganisationEntity organisation2 = getOrganisationEntity();
		List<OrganisationEntity> organisations = new ArrayList<>(Arrays.asList(organisation, organisation2));
		actionRepresentationInteretEntity.setTiers(organisations);

		ResponsablePublicEntity responsable = new ResponsablePublicEntity(RegistreUtils.randomLong(1));
		responsable.setVersion(1);
		responsable.setCategorie("idCategorie" + RegistreUtils.randomLong(8));
		responsable.setIdCategorie(RegistreUtils.randomLong(4));
		responsable.setLibelle("libelleResponsable" + RegistreUtils.randomString(8));
		ResponsablePublicEntity responsable2 = new ResponsablePublicEntity(RegistreUtils.randomLong(1));
		responsable2.setVersion(1);
		responsable2.setCategorie("idCategorie" + RegistreUtils.randomLong(8));
		responsable2.setIdCategorie(RegistreUtils.randomLong(4));
		responsable2.setLibelle("libelleResponsable" + RegistreUtils.randomString(8));
		Set<ResponsablePublicEntity> responsables = new HashSet<>(Arrays.asList(responsable, responsable2));
		actionRepresentationInteretEntity.setReponsablesPublics(responsables);
		actionRepresentationInteretEntity.setResponsablePublicAutre("autreResponsable");

		actionRepresentationInteretEntity.setActiviteRepresentationInteret(new ActiviteRepresentationInteretEntity(RegistreUtils.randomLong(1)));
		actionRepresentationInteretEntity.setObservation("obs");

		return actionRepresentationInteretEntity;
	}

}
