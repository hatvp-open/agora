/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static fr.hatvp.registre.business.utils.SiretSirenRnaHatvpValidation.HATVP;
import static fr.hatvp.registre.business.utils.SiretSirenRnaHatvpValidation.NOTVALID;
import static fr.hatvp.registre.business.utils.SiretSirenRnaHatvpValidation.RNA;
import static fr.hatvp.registre.business.utils.SiretSirenRnaHatvpValidation.SIREN;
import static fr.hatvp.registre.business.utils.SiretSirenRnaHatvpValidation.SIRET;
import static fr.hatvp.registre.business.utils.SiretSirenRnaHatvpValidation.WRONGNUMBER;

import org.junit.Assert;
import org.junit.Test;

import fr.hatvp.registre.business.utils.SiretSirenRnaHatvpValidation;

/**
 * Test de l'utilitaire de validation des SIREt/SIREN
 *
 * @version $Revision$ $Date${0xD}
 */

public class SirenSiretValidationTest {

    /**
     * Tester l'utilitaire de validation des numéros siret/siren.
     */
    @Test
    public void testSiretSirenValidation() {

        final SiretSirenRnaHatvpValidation sirenValidation = new SiretSirenRnaHatvpValidation("732829320");
        Assert.assertEquals(SIREN, sirenValidation.getType());

        final SiretSirenRnaHatvpValidation siretValidation = new SiretSirenRnaHatvpValidation();
        siretValidation.setNationalId("35152822900088");
        Assert.assertEquals(SIRET, siretValidation.getType());

        final SiretSirenRnaHatvpValidation rnaValidation = new SiretSirenRnaHatvpValidation("w123456789");
        Assert.assertEquals(RNA, rnaValidation.getType());
        

        final SiretSirenRnaHatvpValidation hatvpValidation = new SiretSirenRnaHatvpValidation("H123456789");
        Assert.assertEquals(HATVP, hatvpValidation.getType());

    }
    
    /**
     * Tester l'utilitaire de validation des numéros siret/siren.
     */
    @Test
    public void testSiretSirenValidationKO() {

        final SiretSirenRnaHatvpValidation sirenValidation = new SiretSirenRnaHatvpValidation("7328293200");
        Assert.assertEquals(WRONGNUMBER, sirenValidation.getType());

        final SiretSirenRnaHatvpValidation sirenValidation2 = new SiretSirenRnaHatvpValidation("732829321");
        Assert.assertEquals(WRONGNUMBER, sirenValidation2.getType());
        
        final SiretSirenRnaHatvpValidation siretValidation = new SiretSirenRnaHatvpValidation();
        siretValidation.setNationalId("35152822900089");
        Assert.assertEquals(WRONGNUMBER, siretValidation.getType());

        final SiretSirenRnaHatvpValidation rnaValidation = new SiretSirenRnaHatvpValidation("J123456789");
        Assert.assertEquals(NOTVALID, rnaValidation.getType());
    }   
}
