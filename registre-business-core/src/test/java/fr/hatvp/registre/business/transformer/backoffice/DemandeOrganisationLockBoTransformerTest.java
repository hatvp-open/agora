/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.commons.dto.backoffice.DemandeOrganisationLockBoDto;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.backoffice.DemandeOrganisationLockBoEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;

/**
 * Test de la classe {@link DemandeOrganisationLockBoTransformer}.
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class DemandeOrganisationLockBoTransformerTest
{

    /** Transformer à tester. */
    @Autowired
    private DemandeOrganisationLockBoTransformer demandeOrganisationLockBoTransformer;

    /**
     * Tester la méthode DtoToModel du transformeur
     * {@link fr.hatvp.registre.business.transformer.backoffice.impl.DemandeOrganisationLockBoTransformerImpl}.
     */
    @Test
    public void testDtoToModel()
    {
        final DemandeOrganisationLockBoDto dto = new DemandeOrganisationLockBoDto();
        dto.setId(1L);
        dto.setVersion(0);
        dto.setUtilisateurBoId(2L);
        dto.setLockTimeStart(new Date());
        dto.setDemandeOrganisationId(3L);

        final DemandeOrganisationLockBoEntity entity = this.demandeOrganisationLockBoTransformer
                .dtoToModel(dto);

        // Assert NOT NULL.
        Assert.assertNotNull(entity.getId());
        Assert.assertNotNull(entity.getVersion());

        // Assert EQUALS.
        Assert.assertEquals(dto.getId(), entity.getId());
        Assert.assertEquals(dto.getVersion(), entity.getVersion());
        Assert.assertEquals(dto.getDemandeOrganisationId(),
                entity.getDemandeOrganisation().getId());
        Assert.assertEquals(dto.getUtilisateurBoId(), entity.getUtilisateurBo().getId());
        Assert.assertEquals(dto.getLockTimeStart(), entity.getLockedTime());
    }

    /**
     * Tester la méthode ModelToDto du transformeur
     * {@link fr.hatvp.registre.business.transformer.backoffice.impl.DemandeOrganisationLockBoTransformerImpl}.
     */
    @Test
    public void testModelToDto()
    {
        final DemandeOrganisationLockBoEntity entity = new DemandeOrganisationLockBoEntity();
        entity.setId(1L);
        entity.setVersion(0);
        entity.setLockedTime(new Date());

        final DemandeOrganisationEntity espaceOrganisationEntity = new DemandeOrganisationEntity();
        espaceOrganisationEntity.setId(2L);
        entity.setDemandeOrganisation(espaceOrganisationEntity);

        final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
        utilisateurBoEntity.setId(3L);
        utilisateurBoEntity.setNom(RegistreUtils.randomString(20));
        utilisateurBoEntity.setPrenom(RegistreUtils.randomString(10));
        entity.setUtilisateurBo(utilisateurBoEntity);

        final DemandeOrganisationLockBoDto dto = this.demandeOrganisationLockBoTransformer
                .modelToDto(entity);

        // Assert NOT NULL.
        Assert.assertNotNull(dto.getId());
        Assert.assertNotNull(dto.getVersion());

        // Assert EQUALS.
        Assert.assertEquals(entity.getId(), dto.getId());
        Assert.assertEquals(entity.getVersion(), dto.getVersion());
        Assert.assertEquals(entity.getDemandeOrganisation().getId(),
                dto.getDemandeOrganisationId());
        Assert.assertEquals(entity.getUtilisateurBo().getId(), dto.getUtilisateurBoId());
        Assert.assertEquals(
                entity.getUtilisateurBo().getNom() + " " + entity.getUtilisateurBo().getPrenom(),
                dto.getUtilisateurBoName());
    }
}
