/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.activite;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import fr.hatvp.registre.business.transformer.impl.activite.ActiviteRepresentationInteretSimpleTransformerImpl;
import fr.hatvp.registre.business.transformer.impl.activite.ExerciceComptableSimpleTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.activite.ActiviteRepresentationInteretSimpleTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ExerciceComptableEtDeclarationTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ExerciceComptableSimpleTransformer;
import fr.hatvp.registre.business.utils.PdfGeneratorUtil;
import fr.hatvp.registre.commons.dto.activite.ActiviteRepresentationInteretSimpleDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableEtDeclarationDto;
import fr.hatvp.registre.commons.utilities.LocalDateNowBuilder;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.repository.activite.ActiviteRepresentationInteretRepository;
import fr.hatvp.registre.persistence.repository.activite.ExerciceComptableRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationActiviteRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationExerciceRepository;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.ChiffreAffaireRepository;
import fr.hatvp.registre.persistence.repository.nomenclature.MontantDepenseRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;


public class ExerciceComptableServiceTestSansSpy {

	@Mock
	private EspaceOrganisationRepository espaceOrganisationRepository;

    @Mock
    private DesinscriptionRepository desinscriptionRepository;
	@Mock
	private PublicationRepository publicationRepository;
	@Mock
	private ExerciceComptableRepository exerciceComptableRepository;
	
	@Mock
	private ActiviteRepresentationInteretRepository activiteRepresentationInteretRepository;

	@Mock
	private ChiffreAffaireRepository chiffreAffaireRepository;

	@Mock
	private MontantDepenseRepository montantDepenseRepository;

	@Spy
	private ExerciceComptableSimpleTransformer exerciceComptableSimpleTransformer = new ExerciceComptableSimpleTransformerImpl();

	@Mock
	private ExerciceComptableEtDeclarationTransformer exerciceComptableEtDeclarationTransformer;

	@Spy
	private ActiviteRepresentationInteretSimpleTransformer activiteRepresentationInteretSimpleTransformer = new ActiviteRepresentationInteretSimpleTransformerImpl();

	@Spy
	private LocalDateNowBuilder localDateNowBuilder;

	@Mock
	private PdfGeneratorUtil pdfGeneratorUtil;
	
	@Mock
	private PublicationExerciceRepository publicationExerciceRepository;
	
	@Mock
	private PublicationActiviteRepository publicationActiviteRepository;

	@InjectMocks
	private ExerciceComptableServiceImpl exerciceComptableService;

	private static final Long ESPACE_ORGANISATION_ID = 1L;	

	private static final Long EC_ET_DECLARATION_ID = 10L;
	
	private static final Long ACTIVITE_RI_ID = 100L;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	private List<ExerciceComptableEntity> genereListeExercice( EspaceOrganisationEntity espaceOrganisationEntity){
		List<ExerciceComptableEntity> liste = new ArrayList<ExerciceComptableEntity>();
		ExerciceComptableEntity ex1 = new ExerciceComptableEntity();
		ex1.setEspaceOrganisation(espaceOrganisationEntity);
		ex1.setDateDebut(LocalDate.of(2017, 07, 1));
		ex1.setDateFin(LocalDate.of(2017, 12, 31));
		liste.add(ex1);
		ExerciceComptableEntity ex2 = new ExerciceComptableEntity();
		ex2.setEspaceOrganisation(espaceOrganisationEntity);
		ex2.setDateDebut(LocalDate.of(2018, 01, 1));
		ex2.setDateFin(LocalDate.of(2018, 12, 31));
		liste.add(ex2);
		ExerciceComptableEntity ex3 = new ExerciceComptableEntity();
		ex3.setEspaceOrganisation(espaceOrganisationEntity);
		ex3.setDateDebut(LocalDate.of(2019, 01, 1));
		ex3.setDateFin(LocalDate.of(2019, 05, 6));
		liste.add(ex3);
		ExerciceComptableEntity ex4 = new ExerciceComptableEntity();
		ex4.setEspaceOrganisation(espaceOrganisationEntity);
		ex4.setDateDebut(LocalDate.of(2019, 05, 7));
		ex4.setDateFin(LocalDate.of(2019, 12, 31));
		liste.add(ex4);
		return liste;
	}

	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.ExerciceComptableServiceImpl#findAllWithDeclarationByEspaceOrganisationIdBack(final Long currentEspaceOrganisationId)}
	 * 
	 */
	@Test
	public void testFindAllWithDeclarationByEspaceOrganisationIdBack() {		
		
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_ORGANISATION_ID);
		
		List<ExerciceComptableEntity> liste = new ArrayList<ExerciceComptableEntity>();
		ExerciceComptableEntity ex1 = new ExerciceComptableEntity();
		ex1.setId(3L);
		ex1.setDateDebut(LocalDate.of(2017, 07, 1));
		ex1.setDateFin(LocalDate.of(2017, 12, 31));
		liste.add(ex1);
		
		espaceOrganisationEntity.setExercicesComptable(liste);
		
		List<ExerciceComptableEtDeclarationDto> listECetDeclarationsDto = new ArrayList<ExerciceComptableEtDeclarationDto>();
		ExerciceComptableEtDeclarationDto exerciceComptableEtDeclarationDto = new ExerciceComptableEtDeclarationDto();
		exerciceComptableEtDeclarationDto.setId(EC_ET_DECLARATION_ID);
		exerciceComptableEtDeclarationDto.setEspaceOrganisationId(ESPACE_ORGANISATION_ID);
		exerciceComptableEtDeclarationDto.setDateFirstCommMoyens("20/01/2016");
		exerciceComptableEtDeclarationDto.setDateLastChangeMoyens("16/01/2019");
		
		List<ActiviteRepresentationInteretSimpleDto> lisActiviteRepresentationInteretSimpleDto = new ArrayList<ActiviteRepresentationInteretSimpleDto>();
		ActiviteRepresentationInteretSimpleDto activiteRepresentationInteretSimpleDto = new ActiviteRepresentationInteretSimpleDto();
		activiteRepresentationInteretSimpleDto.setId(ACTIVITE_RI_ID);
		activiteRepresentationInteretSimpleDto.setExerciceComptable(ex1.getId());
		activiteRepresentationInteretSimpleDto.setDateFirstComm("18/02/2016");
		activiteRepresentationInteretSimpleDto.setDateLastChange("19/03/2019");
		
		lisActiviteRepresentationInteretSimpleDto.add(activiteRepresentationInteretSimpleDto);
		
		exerciceComptableEtDeclarationDto.setActiviteSimpleDto(lisActiviteRepresentationInteretSimpleDto);
		
		listECetDeclarationsDto.add(exerciceComptableEtDeclarationDto);		
		
		String dateFirstCommMoyens = "20/01/2017";
		String dateLastChangeMoyens = "16/01/2020";
		String latestPublicationDate = "19/03/2020";
		String firstPublicationDate = "18/02/2017";		
		
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ESPACE_ORGANISATION_ID);
		Mockito.doReturn(listECetDeclarationsDto).when(this.exerciceComptableEtDeclarationTransformer).modelToDto(espaceOrganisationEntity.getExercicesComptable());
		Mockito.doReturn(dateFirstCommMoyens).when(this.publicationExerciceRepository).findDateFirstCommMoyens(exerciceComptableEtDeclarationDto.getId());
		Mockito.doReturn(dateLastChangeMoyens).when(this.publicationExerciceRepository).findDateLastChangeMoyens(exerciceComptableEtDeclarationDto.getId());
		Mockito.doReturn(latestPublicationDate).when(this.publicationActiviteRepository).getLatestPublicationDate(activiteRepresentationInteretSimpleDto.getId());
		Mockito.doReturn(firstPublicationDate).when(this.publicationActiviteRepository).getFirstPublicationDate(activiteRepresentationInteretSimpleDto.getId());

		List<ExerciceComptableEtDeclarationDto> retourDtos = this.exerciceComptableService.findAllWithDeclarationByEspaceOrganisationIdBack(ESPACE_ORGANISATION_ID);
		
		Assert.assertEquals(dateFirstCommMoyens, retourDtos.get(0).getDateFirstCommMoyens());
		Assert.assertEquals(dateLastChangeMoyens, retourDtos.get(0).getDateLastChangeMoyens());
		Assert.assertEquals(latestPublicationDate, retourDtos.get(0).getActiviteSimpleDto().get(0).getDateLastChange());
		Assert.assertEquals(firstPublicationDate, retourDtos.get(0).getActiviteSimpleDto().get(0).getDateFirstComm());
	}
}
	