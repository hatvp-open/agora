/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import fr.hatvp.registre.business.ClientAndAssociationsService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.transformer.impl.referentiel.OrganisationTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.AssociationProTransformer;
import fr.hatvp.registre.business.transformer.proxy.ClientTransformer;
import fr.hatvp.registre.business.transformer.proxy.referentiel.OrganisationTransformer;
import fr.hatvp.registre.commons.dto.AssoClientBatchDto;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.client.ClientSimpleDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.persistence.entity.espace.AssociationEntity;
import fr.hatvp.registre.persistence.entity.espace.ClientEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.PeriodeActiviteClientEntity;
import fr.hatvp.registre.persistence.repository.espace.AssoAppartenanceRepository;
import fr.hatvp.registre.persistence.repository.espace.ClientsRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.PeriodeActiviteClientRepository;

/**
 * Classe de test su service {@link ClientAndAssociationsService}
 *
 * @version $Revision$ $Date${0xD}
 */

public class ClientAndAssociationsServiceTest {
	/**
	 * Id de test.
	 */
	public static final Long TEST_ID = 1L;

	public static final Long TEST_ID_PERIODE_ACTIVITE = 2L;
	/**
	 * Ce test contient un jeu de données mocké intéressant à réutiliser, notamment dans le cadre des chargement par lot
	 */
	@InjectMocks
	OrganisationServiceTest dependencyTestWithMockDataset;

	/**
	 * Répository de l'espace organisation.
	 */
	@Mock
	private EspaceOrganisationRepository espaceOrganisationRepository;

	/**
	 * Repository des clients.
	 */
	@Mock
	private ClientsRepository clientsRepository;

	/**
	 * Repository des associations.
	 */
	@Mock
	private AssoAppartenanceRepository associationsRepository;

	/**
	 * Répository des organisations.
	 */
	@Mock
	private OrganisationRepository organisationRepository;

	@Mock
	private EspaceOrganisationService espaceService;

	/**
	 * Transformeur de l'entitée client.
	 */
	@Mock
	private ClientTransformer clientTransformer;

	/**
	 * Transformeur de du dto organisation.
	 */
	@Spy
	@InjectMocks
	private OrganisationTransformer organisationTransformer = new OrganisationTransformerImpl();

	/**
	 * Injections des mocks..
	 */
	@InjectMocks
	private ClientAndAssociationsServiceImpl clientService;
	
	@Mock
	private AssociationProTransformer associationProTransformer;
	
	@Mock
	private PeriodeActiviteClientRepository periodeActiviteClientRepository;

	/**
	 * Initialisation des tests..
	 */
	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
		MockitoAnnotations.initMocks(dependencyTestWithMockDataset);

	}

	/**
	 * Test d'ajout sans erreur.
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#addClientOrAssociation(Long, OrganisationDto, Integer)}
	 */
	@Test
	public void testAddClientWithoutErrors() {

		final OrganisationDto organisationDto = new OrganisationDto();
		organisationDto.setId(TEST_ID);
		organisationDto.setOrganisationExist(false);

		final OrganisationEntity organisation = new OrganisationEntity();
		organisation.setId(TEST_ID);

		final EspaceOrganisationEntity espaceOrganisation = new EspaceOrganisationEntity();
		espaceOrganisation.setOrganisation(organisation);
		espaceOrganisation.setId(TEST_ID);

		final ClientEntity clientEntity = new ClientEntity();
		clientEntity.setEspaceOrganisation(espaceOrganisation);
		clientEntity.setOrganisation(organisation);
		
		final PeriodeActiviteClientEntity periodeActiviteClientEntity = new PeriodeActiviteClientEntity();
		periodeActiviteClientEntity.setId(TEST_ID_PERIODE_ACTIVITE);
		periodeActiviteClientEntity.setClient(clientEntity);
		periodeActiviteClientEntity.setDateAjout(LocalDateTime.now());
		
		final AssoClientDto clientDto = new AssoClientDto();
		clientDto.setOrganisationId(organisation.getId());
		clientDto.setEspaceOrganisationId(espaceOrganisation.getId());
		clientDto.setId(clientEntity.getId());

		when(this.organisationRepository.findOne(TEST_ID)).thenReturn(organisation);
		when(this.espaceOrganisationRepository.findOne(TEST_ID)).thenReturn(espaceOrganisation);
		when(this.clientsRepository.save(clientEntity)).thenReturn(clientEntity);
		when(this.organisationRepository.save(Mockito.any(OrganisationEntity.class))).thenReturn(organisation);
		when(this.clientTransformer.modelToDto(clientEntity)).thenReturn(clientDto);
		when(this.periodeActiviteClientRepository.save(Mockito.any(PeriodeActiviteClientEntity.class))).thenReturn(periodeActiviteClientEntity);	

		final AssoClientDto savedClient = this.clientService.addClient(TEST_ID, organisationDto);

		Assert.assertNotNull(organisation);
		Assert.assertNotNull(espaceOrganisation);
		Assert.assertNotNull(clientEntity);
		Assert.assertNotNull(savedClient);
		Assert.assertEquals(clientEntity.getId(), savedClient.getId());
		Assert.assertEquals(organisation.getId(), savedClient.getOrganisationId());
		Assert.assertEquals(espaceOrganisation.getId(), savedClient.getEspaceOrganisationId());
	}

	/**
	 * exception dans findClientIfExist
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#addClient(final Long espaceOrganisationId, final OrganisationDto organisationDto)}
	 */
	@Test
	public void testAddClientWithExistingClient() {

		final OrganisationDto organisationDto = new OrganisationDto();
		organisationDto.setId(TEST_ID);
		organisationDto.setOrganisationExist(true);
		
		final OrganisationEntity organisation = new OrganisationEntity();
		organisation.setId(TEST_ID);

		final EspaceOrganisationEntity espaceOrganisation = new EspaceOrganisationEntity();
		espaceOrganisation.setId(1L);
		espaceOrganisation.setOrganisation(organisation);
		
		ClientEntity ClientEntity = new ClientEntity();

		Mockito.doReturn(espaceOrganisation).when(this.espaceOrganisationRepository).findOne(espaceOrganisation.getId());
		Mockito.doReturn(organisation).when(this.organisationRepository).findOne(organisationDto.getId());
		Mockito.doReturn(ClientEntity).when(this.clientsRepository).findByEspaceOrganisationIdAndOrganisationId(espaceOrganisation.getId(),organisationDto.getId() );
		
		try {
			this.clientService.addClient(espaceOrganisation.getId(), organisationDto);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.CE_CLIENT_EXISTE_DEJA.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Cas organisation non existante
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#addClient(final Long espaceOrganisationId, final OrganisationDto organisationDto)}
	 */
	@Test
	public void testAddClientWithNonExistingOrganisation() {

		final OrganisationDto organisationDto = new OrganisationDto();
		organisationDto.setId(TEST_ID);
		organisationDto.setOrganisationExist(true);
		
		final OrganisationEntity organisation = new OrganisationEntity();
		organisation.setId(TEST_ID);

		final EspaceOrganisationEntity espaceOrganisation = new EspaceOrganisationEntity();
		espaceOrganisation.setId(1L);
		espaceOrganisation.setOrganisation(organisation);

		Mockito.doReturn(espaceOrganisation).when(this.espaceOrganisationRepository).findOne(espaceOrganisation.getId());
		Mockito.doReturn(null).when(this.organisationRepository).findOne(organisationDto.getId());
		Mockito.doReturn(null).when(this.clientsRepository).findByEspaceOrganisationIdAndOrganisationId(espaceOrganisation.getId(),organisationDto.getId() );
		
		try {
			this.clientService.addClient(espaceOrganisation.getId(), organisationDto);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	
	/**
	 * Cas du client qui s'ajoute
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#addClient(final Long espaceOrganisationId, final OrganisationDto organisationDto)}
	 */
	@Test
	public void testAddClientWithClientAddingHimself() {

		final OrganisationDto organisationDto = new OrganisationDto();
		organisationDto.setId(TEST_ID);
		organisationDto.setOrganisationExist(true);
		
		final OrganisationEntity organisation = new OrganisationEntity();
		organisation.setId(TEST_ID);

		final EspaceOrganisationEntity espaceOrganisation = new EspaceOrganisationEntity();
		espaceOrganisation.setId(1L);
		espaceOrganisation.setOrganisation(organisation);

		Mockito.doReturn(espaceOrganisation).when(this.espaceOrganisationRepository).findOne(espaceOrganisation.getId());
		Mockito.doReturn(organisation).when(this.organisationRepository).findOne(organisationDto.getId());
		Mockito.doReturn(null).when(this.clientsRepository).findByEspaceOrganisationIdAndOrganisationId(espaceOrganisation.getId(),organisationDto.getId() );
		
		try {
			this.clientService.addClient(espaceOrganisation.getId(), organisationDto);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.CLIENT_SOI_MEME.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Test de suppression d'un client avec un id client correct.
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#deleteClient(Long, Long)}
	 */
	@Test
	public void testDeleteClientWithCorrectClientId() {

		final Long testId = 1L;

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(1L);

		final ClientEntity espaceOrganisationClient = new ClientEntity();
		espaceOrganisationClient.setEspaceOrganisation(espaceOrganisationEntity);
		espaceOrganisationClient.setId(testId);

		when(this.clientsRepository.findByIdAndEspaceOrganisationId(testId, 1L)).thenReturn(espaceOrganisationClient);

		this.clientService.deleteClient(testId, 1L);
	}

	/**
	 * Test de suppression d'un client avec un id client incorrect.
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#deleteClient(Long, Long)}
	 */
	@Test
	public void testDeleteClientWithUnknownClientId() {

		final Long testId = 1L;

		when(this.clientsRepository.findByIdAndEspaceOrganisationId(testId, 1L)).thenReturn(null);
		
		try {
			this.clientService.deleteClient(testId, 1L);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.CLIENT_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Test de suppression d'un client avec un id espace incorrect.
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#deleteClient(Long, Long)}
	 */
	@Test
	public void testDeleteClientWithUnknownEspaceId() {
		final Long testId = 1L;

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(1L);

		final ClientEntity espaceOrganisationClient = new ClientEntity();
		espaceOrganisationClient.setEspaceOrganisation(espaceOrganisationEntity);
		espaceOrganisationClient.setId(testId);

		when(this.clientsRepository.findByIdAndEspaceOrganisationId(testId, 1L)).thenReturn(espaceOrganisationClient);
		
		try {
			this.clientService.deleteClient(testId, 2L);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.CLIENT_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Test de suppression d'une association avec un id correct.
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#deleteAssociation(Long, Long)}
	 */
	@Test
	public void testDeleteAssociationWithCorrectAssociationId() {

		final Long testId = 1L;

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(1L);

		final AssociationEntity associationEntity = new AssociationEntity();
		associationEntity.setEspaceOrganisation(espaceOrganisationEntity);
		associationEntity.setId(testId);

		when(this.associationsRepository.findByIdAndEspaceOrganisationId(testId, 1L)).thenReturn(associationEntity);

		this.clientService.deleteAssociation(testId, 1L);
	}

	/**
	 * Test de suppression d'une association avec un id association incorrect.
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#deleteAssociation(Long, Long)}
	 */
	@Test
	public void testDeleteClientWithUnknownAssociationId() {

		final Long testId = 1L;

		when(this.associationsRepository.findByIdAndEspaceOrganisationId(testId, 1L)).thenReturn(null);
		
		try {
			this.clientService.deleteAssociation(testId, 1L);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Test de suppression d'une association avec un id association incorrect.
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#deleteAssociation(Long, Long)}
	 */
	@Test
	public void testDeleteAssociationWithUnknownEspaceId() {
		final Long testId = 1L;

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(1L);

		final AssociationEntity associationEntity = new AssociationEntity();
		associationEntity.setEspaceOrganisation(espaceOrganisationEntity);
		associationEntity.setId(testId);

		when(this.associationsRepository.findByIdAndEspaceOrganisationId(testId, 1L)).thenReturn(associationEntity);
		
		try {
			this.clientService.deleteAssociation(testId, 2L);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}


	

	/**
	 * Test de récupération de clients paginés.
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#getPaginatedClientsByEspaceOrganisation(Integer, Integer, Long)}
	 */
	@Test
	public void testGetPaginatedClients() {
		final Long currentEspaceOrganisationId = 1L;
		int resultPerPage = 10;

		final List<ClientEntity> clientsList = new ArrayList<>();
		for (int i = 0; i < resultPerPage; i++) {
			ClientEntity clientEntity = new ClientEntity();
			clientEntity.setId(Long.valueOf(RandomStringUtils.randomNumeric(3)));
			OrganisationEntity organisationEntity = new OrganisationEntity();
			organisationEntity.setId(1L);
			organisationEntity.setDenomination("blabla");
			organisationEntity.setNomUsageSiren("blou");
			organisationEntity.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
			organisationEntity.setSiren("4444");
			clientEntity.setOrganisation(organisationEntity);

			List<PeriodeActiviteClientEntity> listPeriodeActivite = new ArrayList<PeriodeActiviteClientEntity>();
			//pas retourné pour les paires
			if (i % 2 == 0) {
				PeriodeActiviteClientEntity periodeUn = new PeriodeActiviteClientEntity();
				periodeUn.setDateAjout(LocalDateTime.of(2018, 6, 20, 12, 55));
				periodeUn.setDateDesactivation(LocalDateTime.now());
				periodeUn.setCommentaire("Commentaire balalla");
				periodeUn.setDateFinContrat(null);
				listPeriodeActivite.add(periodeUn);
			}
			
			PeriodeActiviteClientEntity periodeDeux = new PeriodeActiviteClientEntity();
			periodeDeux.setDateAjout(LocalDateTime.now());
			periodeDeux.setDateDesactivation(null);
			periodeDeux.setCommentaire(null);
			periodeDeux.setDateFinContrat(null);
			listPeriodeActivite.add(periodeDeux);

			
			clientEntity.setPeriodeActiviteClientList(listPeriodeActivite);
			
			clientsList.add(clientEntity);
		}

		when(clientsRepository.findClientsActifsByEspaceId(currentEspaceOrganisationId, resultPerPage, 0)).thenReturn(clientsList);
		when(clientsRepository.countClientsActifsByEspaceId(currentEspaceOrganisationId)).thenReturn(25L);


		final PaginatedDto<ClientSimpleDto> paginatedDto = clientService.getPaginatedClientsByEspaceOrganisation("actif", 1, resultPerPage, currentEspaceOrganisationId);

		Assert.assertEquals(25L, paginatedDto.getResultTotalNumber().longValue());
		Assert.assertNull(paginatedDto.getDtos().get(0).getDateDesactivation());
		Assert.assertNull(paginatedDto.getDtos().get(0).getCommentaire());
		Assert.assertNull(paginatedDto.getDtos().get(1).getDateDesactivation());
		Assert.assertNull(paginatedDto.getDtos().get(1).getCommentaire());

	}

	/**
	 * Test de récupération d'associations paginées.
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#getPaginatedAssociations(Integer, Integer, Long)}
	 */
	@Test
	public void testGetPaginatedAssociations() {

		final Long currentEspaceOrganisationId = 1L;

		final Pageable pageable = new PageRequest(10, 25, Sort.Direction.ASC, "id");

		final List<AssociationEntity> associationsList = new ArrayList<>();
		for (int i = 0; i < pageable.getPageSize(); i++) {
			AssociationEntity associationEntity = new AssociationEntity();
			associationEntity.setId(Long.valueOf(RandomStringUtils.randomNumeric(3)));
			OrganisationEntity organisationEntity = new OrganisationEntity();
			organisationEntity.setId(1L);
			organisationEntity.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
			associationEntity.setOrganisation(organisationEntity);
			associationsList.add(associationEntity);
		}

		final Page<AssociationEntity> associationsPage = new PageImpl<>(associationsList, pageable, associationsList.size() * pageable.getPageNumber() * 2);

		when(associationsRepository.findByEspaceOrganisationId(currentEspaceOrganisationId, pageable)).thenReturn(associationsPage);
		when(espaceOrganisationRepository.findEspaceOrganisationByOrganisationIdAndStatutNotRefusee(Mockito.any())).thenReturn(new EspaceOrganisationEntity());

		final PaginatedDto<OrganisationDto> paginatedDto = clientService.getPaginatedAssociations(pageable.getPageNumber() + 1, pageable.getPageSize(), currentEspaceOrganisationId);

		Assert.assertEquals(paginatedDto.getDtos().size(), pageable.getPageSize());
		Assert.assertEquals(paginatedDto.getResultTotalNumber().longValue(), associationsPage.getTotalElements());

	}
	/**
	 * Test 
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#verifyAssociationBatch(AssoClientBatchDto batchReport, final Long espaceOrganisationId) }
	 */
	@Test
	public void verifyAssociationBatchInMap() {
		final Long espaceOrganisationId = 1L;
		List<Long> orgaIdList = new ArrayList<Long>();
		
		AssoClientBatchDto batchReport = new AssoClientBatchDto();
		
		List<OrganisationDto> listOrganisationDto = new ArrayList<OrganisationDto>();
		
		OrganisationDto organisationDto1 = new OrganisationDto();
		organisationDto1.setId(1L);
		organisationDto1.setOrganisationExist(true);
		
		listOrganisationDto.add(organisationDto1);
		orgaIdList.add(organisationDto1.getId());
		
		batchReport.setOrganisationList(listOrganisationDto);
		
		List<OrganisationEntity> organisationEntityList = new ArrayList<OrganisationEntity>();
		
		OrganisationEntity organisationEntity = new OrganisationEntity(organisationDto1.getId());
		organisationEntityList.add(organisationEntity);		
		
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity(espaceOrganisationId);
		OrganisationEntity organisation = new OrganisationEntity(999L);		
		espaceOrganisationEntity.setOrganisation(organisation);
		
		AssoClientDto assoClientDto = new AssoClientDto();
		assoClientDto.setEspaceOrganisationId(espaceOrganisationEntity.getId());
		assoClientDto.setOrganisationId(organisation.getId());		
		
		AssociationEntity associationEntity = new AssociationEntity();
		associationEntity.setEspaceOrganisation(espaceOrganisationEntity);
		associationEntity.setOrganisation(organisation);		
			
		when(this.organisationRepository.findByIdIn(orgaIdList)).thenReturn(organisationEntityList);
		when(this.espaceOrganisationRepository.findOne(espaceOrganisationId)).thenReturn(espaceOrganisationEntity);
		when(this.associationProTransformer.modelToDto(associationEntity)).thenReturn(assoClientDto);
		
		AssoClientBatchDto retourAssoClientBatchDto = clientService.verifyAssociationBatch(batchReport, espaceOrganisationId);
		
		Assert.assertEquals(1, retourAssoClientBatchDto.getValidEntities().size());
	}
	
	/**
	 * Test 
	 * <p>
	 * Test de la méthode: {@link ClientAndAssociationsService#verifyAssociationBatch(AssoClientBatchDto batchReport, final Long espaceOrganisationId) }
	 */
	@Test
	public void verifyAssociationBatchNotInMap() {
		final Long espaceOrganisationId = 1L;
		List<Long> orgaIdList = new ArrayList<Long>();
		
		AssoClientBatchDto batchReport = new AssoClientBatchDto();
		
		List<OrganisationDto> listOrganisationDto = new ArrayList<OrganisationDto>();
		
		//OrganisationDto pas dans la MAP
		OrganisationDto organisationDto2 = new OrganisationDto();
		organisationDto2.setId(2L);
		organisationDto2.setOrganisationExist(true);
		
		listOrganisationDto.add(organisationDto2);
		orgaIdList.add(organisationDto2.getId());
		
		batchReport.setOrganisationList(listOrganisationDto);
		
		List<OrganisationEntity> organisationEntityList = new ArrayList<OrganisationEntity>();
		
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity(espaceOrganisationId);
		OrganisationEntity organisation = new OrganisationEntity(999L);		
		espaceOrganisationEntity.setOrganisation(organisation);

		OrganisationEntity organisation2 = new OrganisationEntity(2L);		
		espaceOrganisationEntity.setOrganisation(organisation2);
		
		AssoClientDto assoClientDto2 = new AssoClientDto();
		assoClientDto2.setEspaceOrganisationId(espaceOrganisationEntity.getId());
		assoClientDto2.setOrganisationId(organisation2.getId());	
		
		AssociationEntity associationEntity2 = new AssociationEntity();
		associationEntity2.setEspaceOrganisation(espaceOrganisationEntity);
		associationEntity2.setOrganisation(organisation);
		
		when(this.organisationRepository.findByIdIn(orgaIdList)).thenReturn(organisationEntityList);
		when(this.espaceOrganisationRepository.findOne(espaceOrganisationId)).thenReturn(espaceOrganisationEntity);
		when(this.associationProTransformer.modelToDto(associationEntity2)).thenReturn(assoClientDto2);	
		
		AssoClientBatchDto retourAssoClientBatchDto = clientService.verifyAssociationBatch(batchReport, espaceOrganisationId);
		
		Assert.assertEquals(1, retourAssoClientBatchDto.getValidEntities().size());
	}
}
