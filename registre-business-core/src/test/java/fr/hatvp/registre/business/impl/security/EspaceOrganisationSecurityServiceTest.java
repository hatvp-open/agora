
/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.security;

import fr.hatvp.registre.business.InscriptionEspaceService;
import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Test du service EspaceOrganisationSecurityService
 * {@link fr.hatvp.registre.business.security.EspaceOrganisationSecurityService}.
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
    loader = AnnotationConfigContextLoader.class)
public class EspaceOrganisationSecurityServiceTest
{
    /** Id de test de l'espace organisation. */
    private final static Long ID_ESPACE_ORGANISATION = 1L;
    /** Id de test de l'utilisateur. */
    private final static Long ID_UTILISATEUR = 2L;

    /** Service des inscriptions à l'espace; */
    @Mock
    private InscriptionEspaceService inscriptionEspaceService;

    /** Injecter les mock dans le service. */
    @InjectMocks
    private EspaceOrganisationSecurityServiceImpl espaceOrganisationSecurityService;

    /**
     * Initialiser les mocks avant de lancer les test.
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        // Process mock annotations
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test de vérification des accès aux infos d'un espace corporate, echec pour un utilisateur dont l'accès est vérouillé.
     * Method: checkUserAuthorization(final Long currentEspaceOrganisationId, final Long currentUserId
     */
    @Test(expected= BusinessGlobalException.class)
    public void testCheckUserAuthorizationForUserVerouille() throws Exception
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        final Date dateDeValidation = sdf.parse("18/06/1994");

        final InscriptionEspaceDto inscriptionEspaceDto = new InscriptionEspaceDto();
        inscriptionEspaceDto.setVerrouille(true);
        inscriptionEspaceDto.setDateValidation(dateDeValidation);

        Mockito.doReturn(inscriptionEspaceDto).when(this.inscriptionEspaceService)
            .getRegistrationByEspaceOrganisationIdAndDeclarantId(ID_ESPACE_ORGANISATION, ID_UTILISATEUR);

        this.espaceOrganisationSecurityService.checkUserAuthorization(ID_ESPACE_ORGANISATION, ID_UTILISATEUR);

    }

    /**
     * Test de vérification des accès aux infos d'un espace corporate, echec pour un utilisateur dont l'inscription n'est pas encore validé.
     * Method: checkUserAuthorization(final Long currentEspaceOrganisationId, final Long currentUserId
     */
    @Test(expected= BusinessGlobalException.class)
    public void testCheckUserAuthorizationForUserNotInscriptionValidated() throws Exception
    {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        final Date dateDeValidation = sdf.parse("18/06/1994");

        final InscriptionEspaceDto inscriptionEspaceDto = new InscriptionEspaceDto();
        inscriptionEspaceDto.setVerrouille(false);
        inscriptionEspaceDto.setDateValidation(null);

        Mockito.doReturn(inscriptionEspaceDto).when(this.inscriptionEspaceService)
            .getRegistrationByEspaceOrganisationIdAndDeclarantId(ID_ESPACE_ORGANISATION, ID_UTILISATEUR);

        this.espaceOrganisationSecurityService.checkUserAuthorization(ID_ESPACE_ORGANISATION, ID_UTILISATEUR);

    }
}
