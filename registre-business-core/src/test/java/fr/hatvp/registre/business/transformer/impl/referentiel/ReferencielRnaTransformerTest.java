/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.referentiel;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.referentiel.RnaReferencielTransformer;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.referentiel.ReferentielRnaEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * Test de la classe {@link RnaReferencielTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
    loader = AnnotationConfigContextLoader.class)
public class ReferencielRnaTransformerTest {

    /**
     * Transformer des référenciels RNA.
     */
    @Autowired
    private RnaReferencielTransformer rnaReferencielTransformer;

    /**
     * Test de la métode modelToDto.
     */
    @Test
    public void testModelToDto() {
        final ReferentielRnaEntity referentielRnaEntity = new ReferentielRnaEntity();
        referentielRnaEntity.setSiren(RegistreUtils.randomString(9));
        referentielRnaEntity.setRna(RegistreUtils.randomString(10));
        referentielRnaEntity.setDenomination(RegistreUtils.randomString(23));
        referentielRnaEntity.setCodePostal(RegistreUtils.randomString(5));
        referentielRnaEntity.setVille(RegistreUtils.randomString(12));
        referentielRnaEntity.setAdresse(RegistreUtils.randomString(15));

        final OrganisationDto organisationDto = this.rnaReferencielTransformer.modelToDto(referentielRnaEntity);

        Assert.assertNotNull(organisationDto);
        Assert.assertEquals(referentielRnaEntity.getSiren(), organisationDto.getSiren());
        Assert.assertEquals(referentielRnaEntity.getRna(), organisationDto.getRna());
        Assert.assertEquals(referentielRnaEntity.getDenomination(), organisationDto.getDenomination());
        Assert.assertEquals(referentielRnaEntity.getCodePostal(), organisationDto.getCodePostal());
        Assert.assertEquals(referentielRnaEntity.getVille(), organisationDto.getVille());
        Assert.assertEquals(referentielRnaEntity.getAdresse(), organisationDto.getAdresse());
    }
}
