/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment;

import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.backoffice.comment.impl.CommentDemandeBoTransformerImpl;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.commons.lists.backoffice.StatutDemandeOrganisationEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.commentaire.CommentDemandeEntity;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;
import fr.hatvp.registre.persistence.repository.espace.DemandeOrganisationRepository;

/**
 * Test de la classe {@link CommentDemandeBoTransformer}.
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class CommentDemandeTransformerTest
    extends AbstractCommentTest<CommentDemandeEntity>
{

    /**
     * Préciser la classe de commentaire.
     */
    public CommentDemandeTransformerTest()
    {
        this.entityClass = CommentDemandeEntity.class;
    }

    /**
     * Repository des demandes.
     */
    @Mock
    private DemandeOrganisationRepository demandeOrganisationRepository;

    /**
     * Transformer à tester.
     */
    @InjectMocks
    private CommentDemandeBoTransformerImpl commentDemandeBoTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    @Test
    public void testModelToDto() throws InstantiationException, IllegalAccessException
    {
        super.testModelToDto();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    CommentBoDto getModelToDtoTransformerResult(final CommentDemandeEntity commentDemandeEntity)
    {
        commentDemandeEntity.setDemandeOrganisationEntity(this.getDemande());
        return this.commentDemandeBoTransformer.commentModelToDto(commentDemandeEntity);
    }

    /**
     * Tester la méthode dtotoModel du transformeur {@link CommentDeclarantBoTransformer}
     */
    @Override
    @Test
    public void testDtoToModel()
    {
        super.testDtoToModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    CommentDemandeEntity getDtoToModelTransformerResult(final CommentBoDto dto)
    {
        when(this.demandeOrganisationRepository.findOne(ID)).thenReturn(this.getDemande());
        return this.commentDemandeBoTransformer.dtoToModel(dto);
    }

    /**
     * @return une instance de {@link DemandeOrganisationEntity}
     */
    private DemandeOrganisationEntity getDemande()
    {
        final DemandeOrganisationEntity demandeOrganisationEntity = new DemandeOrganisationEntity();
        demandeOrganisationEntity.setId(ID);
        demandeOrganisationEntity.setVersion(VERSION);
        demandeOrganisationEntity.setDateCreation(new Date());
        demandeOrganisationEntity.setAdresse(RegistreUtils.randomString(100));
        demandeOrganisationEntity.setCodePostal(RegistreUtils.randomString(10));
        demandeOrganisationEntity.setVille(RegistreUtils.randomString(20));
        demandeOrganisationEntity.setPays(RegistreUtils.randomString(20));
        demandeOrganisationEntity.setStatut(StatutDemandeOrganisationEnum.A_TRAITER);
        return demandeOrganisationEntity;
    }
}
