/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.publication;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationBucketActiviteTransformer;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketActiviteDto;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;

/**
 * Test de la classe {@link fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationActiviteTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class PublicationBucketActiviteTransformerTest
{

    /** Transformeur de production de livrable. */
    @Autowired
    private PublicationBucketActiviteTransformer publicationBucketActiviteTransformer;

    /**
     * Test de la méthode {@link PublicationBucketActiviteTransformer#listPublicationToPublicationBucketActivite(List)}
     */
    @Test
    public void testPublicationListToPublicationBucket() {

        final List<PublicationActiviteEntity> entities = new ArrayList<>();
        entities.add(CommonTestClass.getPublicationActiviteEntity());
        entities.add(CommonTestClass.getPublicationActiviteEntity());
        entities.add(CommonTestClass.getPublicationActiviteEntity());

        final PublicationBucketActiviteDto bucketDto =
            this.publicationBucketActiviteTransformer.listPublicationToPublicationBucketActivite(entities, false);

        Assert.assertNotNull(bucketDto);
        Assert.assertNotNull(bucketDto.getPublicationCourante());

        Assert.assertNull(bucketDto.getPublicationCourante().getId());
        Assert.assertNull(bucketDto.getPublicationCourante().getVersion());

        Assert.assertEquals(3, entities.size());
//        Assert.assertEquals(0, bucketDto.getHistorique().size());

    }
}
