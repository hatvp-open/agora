/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.publication;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationExerciceTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationExerciceDto;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity;

/**
 * Test de la classe {@link PublicationTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class PublicationExerciceTransformerTest
{

    /** Transformer des publications. */
    @Autowired
    private PublicationExerciceTransformer publicationTransformer;

    /**
     * Test de la métode modelToDto de {@link PublicationTransformer#dtoToModel(AbstractCommonDto)}
     * la méthode fait rien => fail à suppr?
     */
    @Test
    @Ignore
    public void testModelToDto() {
    	final PublicationExerciceEntity entity = CommonTestClass.getPublicationExerciceEntity();
    	final PublicationExerciceDto publicationExerciceDto = this.publicationTransformer.modelToDto(entity);
    	
    	Assert.assertEquals(entity.getExerciceComptable().getDateDebut(), publicationExerciceDto.getDateDebut());
    }

    /**
     * Test de la métode modelToDto de {@link PublicationTransformer#modelToDto(AbstractCommonEntity)}
     */
    //TODO
    @Test
    public void testDtoToModel() {
    	PublicationExerciceDto publicationExerciceDto = new PublicationExerciceDto();
    	final PublicationExerciceEntity entity = this.publicationTransformer.dtoToModel(publicationExerciceDto);
    	
    	Assert.assertEquals(null, entity.getExerciceComptable());

    }

    /**
     * Test de la méthode de {@link fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationExerciceTransformer#exerciceToPublication(PublicationExerciceEntity)}
     */
    @Test
    public void testPubExerciceToPublication() {
        final PublicationExerciceEntity entity = CommonTestClass.getPublicationExerciceEntity();
        final ExerciceComptableEntity exercice = entity.getExerciceComptable();
        
        final PublicationExerciceDto publicationExercice = this.publicationTransformer.exerciceToPublication(entity);
        Assert.assertEquals(publicationExercice.getDateDebut(), exercice.getDateDebut());
        Assert.assertEquals(publicationExercice.getDateFin(), exercice.getDateFin());
        Assert.assertEquals(publicationExercice.getChiffreAffaire(), exercice.getChiffreAffaire().getLibelle());
        Assert.assertEquals(publicationExercice.getHasNotChiffreAffaire(), exercice.getHasNotChiffreAffaire());
        Assert.assertEquals(publicationExercice.getMontantDepense(), exercice.getMontantDepense().getLibelle());
        Assert.assertEquals(publicationExercice.getNombreSalaries(), exercice.getNombreSalaries());
        Assert.assertEquals(publicationExercice.getExerciceId(), exercice.getId());
        Assert.assertEquals(publicationExercice.getPublicationDate(), entity.getCreationDate());

    }

    /**
     * Test de la méthode de {@link fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationExerciceTransformer#exerciceToPublication(ExerciceComptableEntity)}
     */
    @Test
    public void testExerciceToPublication() {
        final PublicationExerciceEntity entity = new PublicationExerciceEntity();
        final ExerciceComptableEntity exercice =CommonTestClass.getExerciceComptableEntity();
        entity.setCreationDate(RegistreUtils.generateNewDate(0));
        entity.setExerciceComptable(exercice);
        
        final PublicationExerciceDto publicationExercice = this.publicationTransformer.exerciceToPublication(entity);
        Assert.assertEquals(publicationExercice.getDateDebut(), exercice.getDateDebut());
        Assert.assertEquals(publicationExercice.getDateFin(), exercice.getDateFin());
        Assert.assertEquals(publicationExercice.getChiffreAffaire(), exercice.getChiffreAffaire().getLibelle());
        Assert.assertEquals(publicationExercice.getHasNotChiffreAffaire(), exercice.getHasNotChiffreAffaire());
        Assert.assertEquals(publicationExercice.getMontantDepense(), exercice.getMontantDepense().getLibelle());
        Assert.assertEquals(publicationExercice.getNombreSalaries(), exercice.getNombreSalaries());
        Assert.assertEquals(publicationExercice.getExerciceId(), exercice.getId());
        Assert.assertEquals(publicationExercice.getPublicationDate(), entity.getCreationDate());
    }

    /**
     * Test de la méthode de {@link fr.hatvp.registre.business.transformer.proxy.publication.activite.PublicationExerciceTransformer#exercicesToPublication(List)}
     */
    @Test
    public void testExercicesToPublication() {
        final PublicationExerciceEntity entity1 = CommonTestClass.getPublicationExerciceEntity();
        final PublicationExerciceEntity entity2 = CommonTestClass.getPublicationExerciceEntity();
        final PublicationExerciceEntity entity3 = CommonTestClass.getPublicationExerciceEntity();

        List<PublicationExerciceEntity> publications = new ArrayList<>();
        publications.add(entity1);
        publications.add(entity2);
        publications.add(entity3);

        List<PublicationExerciceDto> publicationsDto = this.publicationTransformer.exercicesToPublication(publications);
        Assert.assertEquals( 3, publicationsDto.size());
        Assert.assertNotNull(publicationsDto.get(0));
        Assert.assertNotNull(publicationsDto.get(1));
        Assert.assertNotNull(publicationsDto.get(2));
    }

}
