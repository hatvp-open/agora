/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl.referentiel;

import java.util.Calendar;

import fr.hatvp.registre.commons.dto.OrganisationDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.referentiel.OrganisationTransformer;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;

/**
 * Classe de test du transformer pour les organisations. 
 * 
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
    loader = AnnotationConfigContextLoader.class)
public class OrganisationTransformerTest {

    /**
     * Transformer du référenciel des organisations.
     */
    @Autowired
    private OrganisationTransformer organisationTransformer;

    /** Nom d'usage utilisé pour les tests */
    private final String NOMUSAGE_TEST = "Organisation de test";
    
    /**
     * Test de la métode modelToDto.
     */
    @Test
    public void testModelToDto() {
        final OrganisationEntity entity = getOrganisationSiren();
        final OrganisationDto organisation = this.organisationTransformer.modelToDto(entity);

        Assert.assertNotNull(organisation);
        Assert.assertEquals(organisation.getId(), entity.getId());
        Assert.assertEquals(organisation.getVersion(), entity.getVersion());
        Assert.assertEquals(organisation.getOriginNationalId(), entity.getOriginNationalId());
        Assert.assertEquals(organisation.getAdresse(), entity.getAdresse());
        Assert.assertEquals(organisation.getCodePostal(), entity.getCodePostal());
        Assert.assertEquals(organisation.getCreationDate(), entity.getCreationDate());
        Assert.assertEquals(organisation.getDenomination(), entity.getDenomination());
        Assert.assertEquals(organisation.getDunsNumber(), entity.getDunsNumber());
        Assert.assertEquals(organisation.getHatvpNumber(), entity.getHatvpNumber());
        Assert.assertEquals(organisation.getPays(), entity.getPays());
        Assert.assertEquals(organisation.getRna(), entity.getRna());
        Assert.assertEquals(organisation.getSiren(), entity.getSiren());
        Assert.assertEquals(organisation.getSiteWeb(), entity.getSiteWeb());
        Assert.assertEquals(organisation.getVille(), entity.getVille());
        Assert.assertEquals(organisation.getNomUsageSiren(), entity.getNomUsageSiren());
    }

    /**
     * Test de la métode DtoToModel.
     */
    @Test
    public void testDtoToModel() {
        final OrganisationDto organisation = getOrganisationSirenDto();
        final OrganisationEntity entity = this.organisationTransformer.dtoToModel(organisation);
        Assert.assertNotNull(entity);
        Assert.assertEquals(organisation.getId(), entity.getId());
        Assert.assertEquals(organisation.getVersion(), entity.getVersion());
        Assert.assertEquals(organisation.getOriginNationalId(), entity.getOriginNationalId());
        Assert.assertEquals(organisation.getAdresse(), entity.getAdresse());
        Assert.assertEquals(organisation.getCodePostal(), entity.getCodePostal());
        Assert.assertEquals(organisation.getCreationDate(), entity.getCreationDate());
        Assert.assertEquals(organisation.getDenomination(), entity.getDenomination());
        Assert.assertEquals(organisation.getDunsNumber(), entity.getDunsNumber());
        Assert.assertEquals(organisation.getHatvpNumber(), entity.getHatvpNumber());
        Assert.assertEquals(organisation.getPays(), entity.getPays());
        Assert.assertEquals(organisation.getRna(), entity.getRna());
        Assert.assertEquals(organisation.getSiren(), entity.getSiren());
        Assert.assertEquals(organisation.getSiteWeb(), entity.getSiteWeb());
        Assert.assertEquals(organisation.getVille(), entity.getVille());
        Assert.assertEquals(organisation.getNomUsageSiren(), entity.getNomUsageSiren());
    }
    
    /**
     * Test la méthode abstraite qui détermine l'identifiant national d'une organisation selon son origine.
     * Cas du numéro SIREN.
     */
    @Test
    public void testDetermineIdentifiantNationalSiren() {
        String identifiantNational = organisationTransformer.determineIdentifiantNational(this.getOrganisationSiren());
        Assert.assertEquals("123456789", identifiantNational);
    }
    
    /**
     * Test la méthode abstraite qui détermine l'identifiant national d'une organisation selon son origine.
     * Cas du numéro RNA.
     */
    @Test
    public void testDetermineIdentifiantNationalRna() {
        String identifiantNational = organisationTransformer.determineIdentifiantNational(this.getOrganisationRna());
        Assert.assertEquals("W123456789", identifiantNational);
    }
    
    /**
     * Test la méthode abstraite qui détermine l'identifiant national d'une organisation selon son origine.
     * Cas du numéro HATVP.
     */
    @Test
    public void testDetermineIdentifiantNationalHatvp() {
        String identifiantNational = organisationTransformer.determineIdentifiantNational(this.getOrganisationHatvp());
        Assert.assertEquals("H123456789", identifiantNational);
    }

    /**
     * Test la méthode abstraite qui détermine l'identifiant national d'une organisation selon son origine.
     * Cas du numéro DUNS : non supporté actuellement par l'application.
     */
    @Test
    public void testDetermineIdentifiantNationalDuns() {
        String identifiantNational = organisationTransformer.determineIdentifiantNational(this.getOrganisationDuns());
        Assert.assertEquals( "", identifiantNational);
    }
    
    /**
     * Permet d'obtenir une entité d'une organisation de type SIREN
     * @return organisationEntity
     */
    private OrganisationEntity getOrganisationSiren() {
        OrganisationEntity organisationEntity = new OrganisationEntity();
        organisationEntity.setId(1L);
        organisationEntity.setVersion(0);
        organisationEntity.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
        organisationEntity.setAdresse("adresse");
        organisationEntity.setCodePostal("12345");
        organisationEntity.setCreationDate(Calendar.getInstance().getTime());
        organisationEntity.setDenomination("ORGANISATION TEST");
        organisationEntity.setDunsNumber(null);
        organisationEntity.setHatvpNumber(null);
        organisationEntity.setPays("FRANCE");
        organisationEntity.setRna(null);
        organisationEntity.setSiren("123456789");
        organisationEntity.setSiteWeb("http://www.site.com/");
        organisationEntity.setVille("PARIS");
        organisationEntity.setNomUsageSiren(NOMUSAGE_TEST);

        return organisationEntity;
    }
    
    /**
     * Permet d'obtenir une entité d'une organisation de type RNA
     * @return organisationEntity
     */
    private OrganisationEntity getOrganisationRna() {
        OrganisationEntity organisationEntity = new OrganisationEntity();
        organisationEntity.setId(2L);
        organisationEntity.setVersion(0);
        organisationEntity.setOriginNationalId(TypeIdentifiantNationalEnum.RNA);
        organisationEntity.setAdresse("adresse");
        organisationEntity.setCodePostal("12345");
        organisationEntity.setCreationDate(Calendar.getInstance().getTime());
        organisationEntity.setDenomination("ORGANISATION TEST");
        organisationEntity.setDunsNumber(null);
        organisationEntity.setHatvpNumber(null);
        organisationEntity.setPays("FRANCE");
        organisationEntity.setRna("W123456789");
        organisationEntity.setSiren(null);
        organisationEntity.setSiteWeb("http://www.site.com/");
        organisationEntity.setVille("PARIS");
        organisationEntity.setNomUsageSiren(NOMUSAGE_TEST);

        return organisationEntity;
    }
    
    /**
     * Permet d'obtenir une entité d'une organisation de type HATVP
     * @return organisationEntity
     */
    private OrganisationEntity getOrganisationHatvp() {
        OrganisationEntity organisationEntity = new OrganisationEntity();
        organisationEntity.setId(3L);
        organisationEntity.setVersion(0);
        organisationEntity.setOriginNationalId(TypeIdentifiantNationalEnum.HATVP);
        organisationEntity.setAdresse("adresse");
        organisationEntity.setCodePostal("12345");
        organisationEntity.setCreationDate(Calendar.getInstance().getTime());
        organisationEntity.setDenomination("ORGANISATION TEST");
        organisationEntity.setDunsNumber(null);
        organisationEntity.setHatvpNumber("H123456789");
        organisationEntity.setPays("FRANCE");
        organisationEntity.setRna(null);
        organisationEntity.setSiren(null);
        organisationEntity.setSiteWeb("http://www.site.com/");
        organisationEntity.setVille("PARIS");
        organisationEntity.setNomUsageSiren(NOMUSAGE_TEST);

        return organisationEntity;
    }
    
    /**
     * Permet d'obtenir une entité d'une organisation de type DUNS
     * @return organisationEntity
     */
    private OrganisationEntity getOrganisationDuns() {
        OrganisationEntity organisationEntity = new OrganisationEntity();
        organisationEntity.setId(3L);
        organisationEntity.setVersion(0);
        organisationEntity.setOriginNationalId(TypeIdentifiantNationalEnum.DUNS);
        organisationEntity.setAdresse("adresse");
        organisationEntity.setCodePostal("12345");
        organisationEntity.setCreationDate(Calendar.getInstance().getTime());
        organisationEntity.setDenomination("ORGANISATION TEST");
        organisationEntity.setDunsNumber("987654321");
        organisationEntity.setHatvpNumber(null);
        organisationEntity.setPays("FRANCE");
        organisationEntity.setRna(null);
        organisationEntity.setSiren(null);
        organisationEntity.setSiteWeb("http://www.site.com/");
        organisationEntity.setVille("PARIS");
        organisationEntity.setNomUsageSiren(NOMUSAGE_TEST);

        return organisationEntity;
    }

    /**
     * Permet d'obtenir un Dto d'une organisation de type SIREN
     * @return organisationDto
     */
    private OrganisationDto getOrganisationSirenDto() {
        OrganisationDto organisationDto = new OrganisationDto();
        organisationDto.setId(1L);
        organisationDto.setVersion(0);
        organisationDto.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
        organisationDto.setAdresse("adresse");
        organisationDto.setCodePostal("12345");
        organisationDto.setCreationDate(Calendar.getInstance().getTime());
        organisationDto.setDenomination("ORGANISATION TEST");
        organisationDto.setDunsNumber(null);
        organisationDto.setHatvpNumber(null);
        organisationDto.setPays("FRANCE");
        organisationDto.setRna(null);
        organisationDto.setSiren("123456789");
        organisationDto.setSiteWeb("http://www.site.com/");
        organisationDto.setVille("PARIS");
        organisationDto.setNomUsageSiren(NOMUSAGE_TEST);

        return organisationDto;
    }
}
