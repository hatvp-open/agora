/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.commentaire.AbstractCommentEntity;
import fr.hatvp.registre.persistence.repository.backoffice.UtilisateurBoRepository;

abstract class AbstractCommentTest<E extends AbstractCommentEntity> {

	Class<E> entityClass;

	/**
	 * ID des test.
	 */
	static final Long ID = 1L;
	/**
	 * Version des test.
	 */
	static final Integer VERSION = 0;
	/**
	 * Commentaire de test.
	 */
	static final String MESSAGE_TEST = "message des tests";
	/**
	 * Email des test.
	 */
	static final String EMAIL_TEST = "test@test.fr";

	/**
	 * Repository des utilisateurs bo.
	 */
	@Mock
	private UtilisateurBoRepository utilisateurBoRepository;

	/**
	 * Initialisation de la classe de test.
	 *
	 * @throws Exception
	 *             exception d'initialisation.
	 */

	@Before
	public void setUp() throws Exception {
		ArrayList<? extends GrantedAuthority> authorities = new ArrayList<>();
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
		// add principal object to SecurityContextHolder
		User user = new User(EMAIL_TEST, "blabla", true, false, false, false, authorities);
		/* fill user object */

		Authentication auth = new UsernamePasswordAuthenticationToken(user, null);

		SecurityContextHolder.getContext().setAuthentication(auth);
	}

	/**
	 * récupérer une instance d'utilsateur backoffice.
	 *
	 * @return UtilisateurBoEntity de test.
	 */
	UtilisateurBoEntity getEditeur() {
		final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
		utilisateurBoEntity.setId(ID);
		utilisateurBoEntity.setVersion(VERSION);
		utilisateurBoEntity.setEmail(EMAIL_TEST);
		utilisateurBoEntity.setPrenom("test_p");
		utilisateurBoEntity.setNom("test_n");
		return utilisateurBoEntity;
	}

	/**
	 * Test de la transformation vers de DTO.
	 */
	public void testModelToDto() throws IllegalAccessException, InstantiationException {

		final E entity = this.entityClass.newInstance();
		entity.setId(ID);
		entity.setVersion(VERSION);
		entity.setMessage(MESSAGE_TEST);
		entity.setDateCreation(new Date());
		entity.setEditeur(this.getEditeur());

		final CommentBoDto commentBoDto = this.getModelToDtoTransformerResult(entity);

		Assert.assertNotNull(commentBoDto.getId());
		Assert.assertNotNull(commentBoDto.getVersion());
		Assert.assertNotNull(commentBoDto.getMessage());
		Assert.assertNotNull(commentBoDto.getDateCreation());
		Assert.assertNotNull(commentBoDto.getAuteur());
		Assert.assertNotNull(commentBoDto.getContextId());
		// Assertion d'égalités
		Assert.assertEquals(entity.getId(), commentBoDto.getId());
		Assert.assertEquals(entity.getVersion(), commentBoDto.getVersion());
		Assert.assertEquals(entity.getMessage(), commentBoDto.getMessage());
		Assert.assertEquals(entity.getDateCreation(), commentBoDto.getDateCreation());
		Assert.assertEquals(entity.getEditeur().getNom() + " " + entity.getEditeur().getPrenom(), commentBoDto.getAuteur());

		this.additionalModelToDtoDeclarantCommentAssertions(entity, commentBoDto);
		this.additionalModelToDtoEspaceOrganisationAssertions(entity, commentBoDto);
		this.additionalModelToDtoValidationEspaceAssertions(entity, commentBoDto);
	}

	/**
	 * Plus d'assertion sur la partie commentaire déclarant.
	 */
	void additionalModelToDtoDeclarantCommentAssertions(final E entity, final CommentBoDto dto) {
	}

	/**
	 * Plus d'assertion sur la partie commentaire des espaces organisations.
	 */
	void additionalModelToDtoEspaceOrganisationAssertions(final E entity, final CommentBoDto dto) {
	}

	/**
	 * Plus de commentaire sur la partie commentaire des validations des inscriptions.
	 */
	void additionalModelToDtoValidationEspaceAssertions(final E entity, final CommentBoDto dto) {
	}

	/**
	 * @param entity
	 *            entité à transformer.
	 * @return commentaire dto de l'entité transformé.
	 */
	abstract CommentBoDto getModelToDtoTransformerResult(E entity);

	/**
	 * test de la transformation vers l'entité.
	 */
	public void testDtoToModel() {

		final CommentBoDto dto = new CommentBoDto();
		dto.setId(ID);
		dto.setVersion(VERSION);
		dto.setMessage(MESSAGE_TEST);
		dto.setContextId(ID);

		Mockito.doReturn(Optional.ofNullable(this.getEditeur())).when(this.utilisateurBoRepository).findByEmail(EMAIL_TEST);

		final E commentBoEntity = this.getDtoToModelTransformerResult(dto);

		Assert.assertNotNull(commentBoEntity.getId());
		Assert.assertNotNull(commentBoEntity.getVersion());
		Assert.assertNotNull(commentBoEntity.getMessage());

		this.additionalDtoToModelDeclarantCommentAssertions(dto, commentBoEntity);
		this.additionalDtoToModelEspaceOrganisationAssertions(dto, commentBoEntity);
		this.additionalDtoToModelValidationEspaceAssertions(dto, commentBoEntity);
	}

	/**
	 * @param dto
	 *            dto à transformer.
	 * @return commentaire entity du dto.
	 */
	abstract E getDtoToModelTransformerResult(CommentBoDto dto);

	/**
	 * Plus d'assertion sur la partie commentaire déclarant.
	 */
	void additionalDtoToModelDeclarantCommentAssertions(final CommentBoDto dto, final E entity) {
	}

	/**
	 * Plus d'assertion sur la partie commentaire des espaces organisations.
	 */
	void additionalDtoToModelEspaceOrganisationAssertions(final CommentBoDto dto, final E entity) {
	}

	/**
	 * Plus de commentaire sur la partie commentaire des validations des inscriptions.
	 */
	void additionalDtoToModelValidationEspaceAssertions(final CommentBoDto dto, final E entity) {
	}
}
