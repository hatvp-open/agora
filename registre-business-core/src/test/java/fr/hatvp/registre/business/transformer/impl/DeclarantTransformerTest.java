/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.impl.DeclarantTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.commons.dto.DeclarantContactDto;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.lists.CategoryTelephoneMail;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantContactEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;

/**
 * Test de la classe {@link DeclarantTransformerImpl}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class DeclarantTransformerTest
{

    /** Transformer à tester. */
    @Autowired
    private DeclarantTransformer declarantTransformer;

    /** Valeur de la civilité pour les tests. */
    private static final CiviliteEnum CIVILITY_TEST = CiviliteEnum.M;

    /**
     * Test de la métode modelToDto
     */
    @Test
    public void testModelToDto()
    {
        /*
         * Création d'une entité
         */
        final DeclarantEntity model = new DeclarantEntity();
        model.setId(1L);
        model.setVersion(0);
        model.setPrenom(RegistreUtils.randomString(10));
        model.setNom(RegistreUtils.randomString(10));
        model.setBirthDate(new Date());
        model.setEmail(RegistreUtils.randomString(10));
        model.setEmailTemp(RegistreUtils.randomString(10));
        model.setTelephone(RegistreUtils.randomString(10));
        model.setCivility(CIVILITY_TEST);
        model.setPassword(RegistreUtils.randomString(33));
        model.setActivated(true);
        model.setRecoverConfirmationCode(RegistreUtils.randomString(40));
        model.setContacts(new HashSet<>());
        model.getContacts().add(this.createDeclarantContact(true));
        model.getContacts().add(this.createDeclarantContact(true));
        model.getContacts().add(this.createDeclarantContact(true));
        model.getContacts().add(this.createDeclarantContact(false));
        model.getContacts().add(this.createDeclarantContact(false));
        model.setInscriptionEspaces(new HashSet<>());
        model.getInscriptionEspaces().add(this.createInscription(true));

        /*
         * Transformation
         */
        final DeclarantDto dto = this.declarantTransformer.modelToDto(model);

        /*
         * Assertions
         */
        Assert.assertNotNull(dto);
        Assert.assertEquals(model.getId(), dto.getId());
        Assert.assertEquals(model.getVersion(), dto.getVersion());
        Assert.assertEquals(model.getPrenom(), dto.getPrenom());
        Assert.assertEquals(model.getNom(), dto.getNom());
        Assert.assertEquals(model.getBirthDate(), dto.getBirthDate());
        Assert.assertEquals(model.getEmail(), dto.getEmail());
        Assert.assertEquals(model.getEmailTemp(), dto.getEmailTemp());
        Assert.assertEquals(model.getTelephone(), dto.getTelephone());
        Assert.assertEquals(model.getCivility(), dto.getCivility());
        Assert.assertEquals(model.isActivated(), dto.isActivated());
        Assert.assertEquals(model.getRecoverConfirmationCode(),
                dto.getRecuperationPasswordKeyCode());
        Assert.assertNotNull(dto.getEmailComplement());
        Assert.assertEquals(3, dto.getEmailComplement().size());
        Assert.assertNotNull(dto.getPhone());
        Assert.assertEquals(2, dto.getPhone().size());
    }

    /**
     * Test de la métode modelToDto
     */
    @Test
    public void testDtoToModel()
    {
        /*
         * Création d'un Dto
         */
        final DeclarantDto dto = new DeclarantDto();
        dto.setId(RegistreUtils.randomLong(3));
        dto.setVersion(0);
        dto.setPrenom(RegistreUtils.randomString(10));
        dto.setNom(RegistreUtils.randomString(10));
        dto.setBirthDate(new Date());
        dto.setEmail(RegistreUtils.randomString(10));
        dto.setEmailTemp(RegistreUtils.randomString(10));
        dto.setTelephone(RegistreUtils.randomString(10));
        dto.setCivility(CIVILITY_TEST);
        dto.setPassword(RegistreUtils.randomString(33));
        dto.setActivated(true);
        dto.setActivationEmailKeyCode(RegistreUtils.randomString(40));
        dto.setEmailComplement(new ArrayList<>());
        dto.getEmailComplement().add(this.createDeclarantContactDto(true));
        dto.getEmailComplement().add(this.createDeclarantContactDto(true));
        dto.getEmailComplement().add(this.createDeclarantContactDto(true));
        dto.setPhone(new ArrayList<>());
        dto.getPhone().add(this.createDeclarantContactDto(false));
        dto.getPhone().add(this.createDeclarantContactDto(false));
        /*
         * Transformation
         */
        final DeclarantEntity result = this.declarantTransformer.dtoToModel(dto);

        /*
         * Assertions
         */
        Assert.assertNotNull(result);
        Assert.assertEquals(dto.getId(), result.getId());
        Assert.assertEquals(dto.getVersion(), result.getVersion());
        Assert.assertEquals(dto.getPrenom(), result.getPrenom());
        Assert.assertEquals(dto.getNom(), result.getNom());
        Assert.assertEquals(dto.getBirthDate(), result.getBirthDate());
        Assert.assertEquals(dto.getEmail(), result.getEmail());
        Assert.assertEquals(dto.getEmailTemp(), result.getEmailTemp());
        Assert.assertEquals(dto.getTelephone(), result.getTelephone());
        Assert.assertEquals(dto.getCivility(), result.getCivility());
        Assert.assertEquals(dto.getPassword(), result.getPassword());
        Assert.assertEquals(dto.isActivated(), result.isActivated());
        Assert.assertEquals(dto.getActivationEmailKeyCode(), result.getEmailConfirmationCode());
        Assert.assertNotNull(result.getContacts());
        Assert.assertEquals(5, result.getContacts().size());
    }

    /**
     * Créer une inscription.
     *
     * @return une instance de inscriptionespace.
     */
    private InscriptionEspaceEntity createInscription(final boolean actif)
    {
        final InscriptionEspaceEntity inscriptionEspaceEntity = new InscriptionEspaceEntity();
        inscriptionEspaceEntity.setActif(actif);

        return inscriptionEspaceEntity;
    }

    /**
     * Création d'un contact de déclarant avec un email ou un numéro de téléphone.
     *
     * @param isMail
     *         marqueur contact mail
     * @return si le marqueur est à true, on crée un contact avec un email, sinon on en crée un avec un téléphone
     */
    private DeclarantContactEntity createDeclarantContact(final boolean isMail)
    {
        final DeclarantContactEntity model = new DeclarantContactEntity();
        model.setCategory(CategoryTelephoneMail.PERSO);
        model.setDeclarant(new DeclarantEntity());
        if (isMail) {
            model.setEmail(RegistreUtils.randomString(15));
        }
        else {
            model.setTelephone(RegistreUtils.randomString(10));
        }
        model.setId(RegistreUtils.randomLong(10));
        return model;
    }

    /**
     * Création d'un contact de déclarant Dto avec un email ou un numéro de téléphone.
     *
     * @param isMail
     *         marqueur contact mail
     * @return si le marqueur est à true, on crée un contact avec un email, sinon on en crée un avec un téléphone
     */
    private DeclarantContactDto createDeclarantContactDto(final boolean isMail)
    {
        final DeclarantContactDto result = new DeclarantContactDto();
        result.setCategorie(CategoryTelephoneMail.PERSO);
        if (isMail) {
            result.setEmail(RegistreUtils.randomString(15));
        }
        else {
            result.setTelephone(RegistreUtils.randomString(10));
        }
        result.setId(RegistreUtils.randomLong(10));
        return result;
    }

}
