/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;
import fr.hatvp.registre.commons.lists.backoffice.RoleEnumBackOffice;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;

/**
 * Test de la classe
 * {@link fr.hatvp.registre.business.transformer.backoffice.impl.UtilisateurBoTransformerImpl}.
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestBusinessConfig.class }, loader = AnnotationConfigContextLoader.class)
public class UtilisateurBoTransformerTest {

	/** Transformer à tester. */
	@Autowired
	private UtilisateurBoTransformer utilisateurBoTransformer;

	/**
	 * Tester la méthode ModelToDto du transformeur
	 * {@link fr.hatvp.registre.business.transformer.backoffice.impl.UtilisateurBoTransformerImpl}.
	 */
	@Test
	public void testModelToDto() {

		final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
		utilisateurBoEntity.setId(1L);
		utilisateurBoEntity.setVersion(0);
		utilisateurBoEntity.setPrenom(RegistreUtils.randomString(10));
		utilisateurBoEntity.setNom(RegistreUtils.randomString(10));
		utilisateurBoEntity.setEmail(RegistreUtils.randomString(10));
		utilisateurBoEntity.setPassword(RegistreUtils.randomString(33));
		utilisateurBoEntity.setCompteSupprime(true);

		final Set<RoleEnumBackOffice> roleEnumBackOffices = new HashSet<>();
		roleEnumBackOffices.add(RoleEnumBackOffice.RESTRICTION_ACCES);
		utilisateurBoEntity.setRoleEnumBackOffices(roleEnumBackOffices);

		final UtilisateurBoDto utilisateurBoDto = this.utilisateurBoTransformer.modelToDto(utilisateurBoEntity);

		// Assert NOT NULL.
		Assert.assertNotNull(utilisateurBoDto.getId());
		Assert.assertNotNull(utilisateurBoDto.getVersion());
		Assert.assertNotNull(utilisateurBoDto.getPrenom());
		Assert.assertNotNull(utilisateurBoDto.getNom());
		Assert.assertNotNull(utilisateurBoDto.getEmail());
		Assert.assertNotNull(utilisateurBoDto.getPassword());
		Assert.assertNotNull(utilisateurBoDto.getCompteSupprime());
		Assert.assertNotNull(utilisateurBoDto.getRoles());

		// Assert EQUALS.
		Assert.assertEquals(utilisateurBoEntity.getId(), utilisateurBoDto.getId());
		Assert.assertEquals(utilisateurBoEntity.getVersion(), utilisateurBoDto.getVersion());
		Assert.assertEquals(utilisateurBoEntity.getPrenom(), utilisateurBoDto.getPrenom());
		Assert.assertEquals(utilisateurBoEntity.getNom(), utilisateurBoDto.getNom());
		Assert.assertEquals(utilisateurBoEntity.getEmail(), utilisateurBoDto.getEmail());
		Assert.assertEquals(utilisateurBoEntity.getPassword(), utilisateurBoDto.getPassword());
		Assert.assertEquals(utilisateurBoEntity.getCompteSupprime(), utilisateurBoDto.getCompteSupprime());

		final List<RoleEnumBackOffice> roleEnumBackOffices1 = new ArrayList<>();
		roleEnumBackOffices1.addAll(utilisateurBoEntity.getRoleEnumBackOffices());
		Assert.assertEquals(roleEnumBackOffices1, utilisateurBoDto.getRoles());
	}

	/**
	 * Tester la méthode DtoToModel du transformeur
	 * {@link fr.hatvp.registre.business.transformer.backoffice.impl.UtilisateurBoTransformerImpl}.
	 */
	@Test
	public void testDtoToModel() {
		final UtilisateurBoDto utilisateurBoDto = new UtilisateurBoDto();
		utilisateurBoDto.setId(1L);
		utilisateurBoDto.setVersion(0);
		utilisateurBoDto.setPrenom(RegistreUtils.randomString(10));
		utilisateurBoDto.setNom(RegistreUtils.randomString(10));
		utilisateurBoDto.setEmail(RegistreUtils.randomString(10));
		utilisateurBoDto.setPassword(RegistreUtils.randomString(33));
		utilisateurBoDto.setCompteSupprime(true);

		final List<RoleEnumBackOffice> roleEnumBackOffices = new ArrayList<>();
		roleEnumBackOffices.add(RoleEnumBackOffice.RESTRICTION_ACCES);
		utilisateurBoDto.setRoles(roleEnumBackOffices);

		final UtilisateurBoEntity utilisateurBoEntity = this.utilisateurBoTransformer.dtoToModel(utilisateurBoDto);

		// Assert NOT NULL.
		Assert.assertNotNull(utilisateurBoEntity.getId());
		Assert.assertNotNull(utilisateurBoEntity.getVersion());
		Assert.assertNotNull(utilisateurBoEntity.getPrenom());
		Assert.assertNotNull(utilisateurBoEntity.getNom());
		Assert.assertNotNull(utilisateurBoEntity.getEmail());
		Assert.assertNotNull(utilisateurBoEntity.getPassword());
		Assert.assertNotNull(utilisateurBoEntity.getCompteSupprime());
		Assert.assertNotNull(utilisateurBoEntity.getRoleEnumBackOffices());

		// Assert EQUALS.
		Assert.assertEquals(utilisateurBoDto.getId(), utilisateurBoEntity.getId());
		Assert.assertEquals(utilisateurBoDto.getVersion(), utilisateurBoEntity.getVersion());
		Assert.assertEquals(utilisateurBoDto.getPrenom(), utilisateurBoEntity.getPrenom());
		Assert.assertEquals(utilisateurBoDto.getNom(), utilisateurBoEntity.getNom());
		Assert.assertEquals(utilisateurBoDto.getEmail(), utilisateurBoEntity.getEmail());
		Assert.assertEquals(utilisateurBoDto.getPassword(), utilisateurBoEntity.getPassword());
		Assert.assertEquals(utilisateurBoDto.getCompteSupprime(), utilisateurBoEntity.getCompteSupprime());

		final List<RoleEnumBackOffice> roleEnumBackOffices1 = new ArrayList<>();
		roleEnumBackOffices1.addAll(utilisateurBoDto.getRoles());

		Assert.assertEquals(utilisateurBoDto.getRoles(), roleEnumBackOffices1);
	}
}
