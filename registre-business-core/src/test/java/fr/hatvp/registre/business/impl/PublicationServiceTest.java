/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.hatvp.registre.business.PublicationService;
import fr.hatvp.registre.business.impl.activite.PublicationExerciceServiceImpl;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationBucketTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationFrontTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationRechercheTransformer;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationTransformer;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.publication.PublicationBucketDto;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.commons.dto.publication.PublicationFrontDto;
import fr.hatvp.registre.commons.dto.publication.PublicationRechercheBlacklistDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketExerciceDto;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ActionMeneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DecisionConcerneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ResponsablePublicEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;
import fr.hatvp.registre.persistence.repository.activite.ExerciceComptableRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationActiviteRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;

/**
 * Test du service {@link fr.hatvp.registre.business.PublicationService}
 *
 * @version $Revision$ $Date${0xD}
 */

public class PublicationServiceTest {

	/** Transformeur de l'entitée organisation. */
	@Mock
	private OrganisationRepository organisationRepository;
	
	/** Transformeur de l'entitée organisation. */
	@Mock
	private ExerciceComptableRepository exerciceComptableRepository;

	/** Répository des publications. */
	@Mock
	private PublicationRepository publicationRepository;

	/** Repository des espaces organisations. */
	@Mock
	private EspaceOrganisationRepository espaceOrganisationRepository;

	/** Transformeur des publications. */
	@Mock
	private PublicationFrontTransformer publicationFrontTransformer;

	/** Transformeur des déclarants. */
	@Mock
	private DeclarantTransformer declarantTransformer;

	/** Transoformeur des publications. */
	@Mock
	private PublicationTransformer publicationTransformer;
	
	
	/** Transoformeur des publications. */
	@Mock
	private PublicationRechercheTransformer publicationRechercheTransformer;

	/** Injection des mock. */
	@InjectMocks
	private PublicationServiceImpl publicationService;
	
	@Mock
	private PublicationExerciceServiceImpl publicationExerciceService;
	
	@Mock
	private PublicationTransformer publicationBaseTransformer;
	
	@Mock
	PublicationActiviteRepository publicationActiviteRepository;
	
	@Mock
	PublicationBucketTransformer publicationBucketTransformer;

	/** ID de test de l'espace organisation. */
	private static final Long ESPACE_ID = 1L;

	/**
	 * Initialisation des tests.
	 *
	 * @throws Exception
	 */
	@Before
	public void before() throws Exception {
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test de la méthode {@link PublicationService#savePublication(EspaceOrganisationDto, Long)}
	 */
	@Test
	public void testSavePublication() {
		// TODO: implémenter

	}

	/**
	 * Test de la méthode {@link PublicationService#getDernierePublication(Long)} Cas 1: Aucune publication
	 */
	@Test
	public void testGetDernierePublicationCase1() {
		Mockito.doReturn(Optional.empty()).when(this.publicationRepository).findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(ESPACE_ID, StatutPublicationEnum.DEPUBLIEE);

		final PublicationFrontDto dto = this.publicationService.getDernierePublication(ESPACE_ID);

		Assert.assertNotNull(dto);
		Assert.assertNull(dto.getId());
		Assert.assertNull(dto.getVersion());
	}

	/**
	 * Test de la méthode {@link PublicationService#getDernierePublication(Long)} Cas 2: Au moins une publication.
	 */
	@Test
	public void testGetDernierePublicationCase2() {

		final PublicationEntity entity = getPublicationEntity();

		Mockito.doReturn(Optional.of(entity)).when(this.publicationRepository).findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(ESPACE_ID, StatutPublicationEnum.DEPUBLIEE);

		final PublicationFrontDto publicationFrontDto = new PublicationFrontDto();
		publicationFrontDto.setId(entity.getId());
		publicationFrontDto.setVersion(entity.getVersion());

		Mockito.doReturn(publicationFrontDto).when(this.publicationFrontTransformer).modelToDto(entity);

		final PublicationFrontDto dto = this.publicationService.getDernierePublication(ESPACE_ID);

		Assert.assertNotNull(dto);
		Assert.assertNotNull(dto.getId());
		Assert.assertNotNull(dto.getVersion());
	}

	/**
	 * Test de la méthode {@link PublicationServiceImpl#updateReferencielOraganisation(PublicationEntity, EspaceOrganisationEntity)}
	 */
	@Test
	public void updateReferencielOraganisationTest() {
		final OrganisationEntity organisationEntity = new OrganisationEntity();

		final PublicationEntity publicationEntity = getPublicationEntity();

		final EspaceOrganisationEntity espaceEntity = new EspaceOrganisationEntity();
		espaceEntity.setOrganisation(organisationEntity);

		Mockito.doReturn(organisationEntity).when(this.organisationRepository).save(organisationEntity);

		final OrganisationEntity entity = this.publicationService.updateReferencielOraganisation(publicationEntity, espaceEntity);

		Assert.assertNotNull(entity);
		Assert.assertEquals(publicationEntity.getAdresse(), entity.getAdresse());
		Assert.assertEquals(publicationEntity.getCodePostal(), entity.getCodePostal());
		Assert.assertEquals(publicationEntity.getVille(), entity.getVille());
		Assert.assertEquals(publicationEntity.getPays(), entity.getPays());
	}

	/**
	 * test de la méthode {@link PublicationServiceImpl#updateFlagDePublicationTrue(Long)}
	 */
	@Test
	public void updateFlagDePublicationTest() {
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setNewPublication(true);

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ESPACE_ID);
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).save(espaceOrganisationEntity);

		final EspaceOrganisationEntity entity = this.publicationService.updateFlagDePublicationTrue(ESPACE_ID, null);

		Assert.assertNotNull(entity);
		Assert.assertTrue(entity.getNewPublication());
	}

	/**
	 * test de la méthode {@link PublicationServiceImpl#getPublicationLivrable(Long ) }
	 */
	@Test
	public void getPublicationLivrableTest() {
		PublicationBucketDto dto = this.publicationService.getPublicationLivrable(ESPACE_ID);
		assertThat(dto).isNull();
	}

	private PublicationEntity getPublicationEntity() {
		final PublicationEntity publicationEntity = new PublicationEntity();
		publicationEntity.setId(RegistreUtils.randomLong(3));
		publicationEntity.setVersion(RegistreUtils.randomLong(2).intValue());
		publicationEntity.setAdresse(RegistreUtils.randomString(20));
		publicationEntity.setCodePostal(RegistreUtils.randomString(20));
		publicationEntity.setVille(RegistreUtils.randomString(20));
		publicationEntity.setPays(RegistreUtils.randomString(20));
		return publicationEntity;
	}
	@Test
	/**
	 * Test avec 4 exercices comptables et une date inscription au 13/03/19 
	 * 01/07/17 au 31/12/17 -> 1 activite
	 * 01/01/18 au 31/12/18 -> pas de publi
	 * 01/01/19 au 31/12/19 -> pas de publi
	 * 01/01/20 au 31/12/20 -> pas de publi
	 */
	public void getPublicationExerciceTest() {
		//en entrée : 1 espace avec 4 exercice : deux avant sa date d'inscription (dont un avec une fiche d'activité) et deux autres apres
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setDateActionCreation(java.util.Date.from(LocalDate.parse("13-03-2019", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		List<ExerciceComptableEntity> listeExercices = new ArrayList<ExerciceComptableEntity>();
		PublicationBucketExerciceDto bucketDto = new PublicationBucketExerciceDto();
		ExerciceComptableEntity exerciceComptable1 = new ExerciceComptableEntity(1L);
		exerciceComptable1.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable1.setDateDebut(LocalDate.of(2017, 01, 01));
		exerciceComptable1.setDateFin(LocalDate.of(2017, 12, 31));
		exerciceComptable1.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable1.setPublicationEnabled(true);
		exerciceComptable1.getActivitesRepresentationInteret().add(this.getActivite());
		exerciceComptable1.setNoActivite(false);
		exerciceComptable1.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable2 = new ExerciceComptableEntity(2L);
		exerciceComptable2.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable2.setDateDebut(LocalDate.of(2018, 01, 01));
		exerciceComptable2.setDateFin(LocalDate.of(2018, 12, 31));
		exerciceComptable2.setPublicationEnabled(true);
		exerciceComptable2.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable2.setNoActivite(false);
		exerciceComptable2.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable3 = new ExerciceComptableEntity(3L);
		exerciceComptable3.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable3.setDateDebut(LocalDate.of(2019, 01, 01));
		exerciceComptable3.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptable3.setPublicationEnabled(true);
		exerciceComptable3.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable3.setNoActivite(false);
		exerciceComptable3.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable4 = new ExerciceComptableEntity(4L);
		exerciceComptable4.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable4.setDateDebut(LocalDate.of(2020, 01, 01));
		exerciceComptable4.setDateFin(LocalDate.of(2020, 12, 31));
		exerciceComptable4.setPublicationEnabled(true);
		exerciceComptable4.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable4.setNoActivite(false);
		exerciceComptable4.setBlacklisted(false);
		
		listeExercices.add(exerciceComptable1);
		listeExercices.add(exerciceComptable2);
		listeExercices.add(exerciceComptable3);
		listeExercices.add(exerciceComptable4);
		espaceOrganisationEntity.setExercicesComptable(listeExercices);
		when(exerciceComptableRepository.findOne(exerciceComptable1.getId())).thenReturn(exerciceComptable1);
		when(exerciceComptableRepository.findOne(exerciceComptable2.getId())).thenReturn(exerciceComptable2);
		when(exerciceComptableRepository.findOne(exerciceComptable3.getId())).thenReturn(exerciceComptable3);
		when(exerciceComptableRepository.findOne(exerciceComptable4.getId())).thenReturn(exerciceComptable4);
		List<PublicationBucketExerciceDto> listToTest = this.publicationService.getPublicationExercice(espaceOrganisationEntity,false);
		
		when(publicationExerciceService.getPublicationLivrableExercice(Mockito.anyLong(),Mockito.anyBoolean())).thenReturn(bucketDto);
		verify(publicationExerciceService, times(3)).getPublicationLivrableExercice(Mockito.anyLong(),Mockito.anyBoolean());
		
	}
	
	
	@Test
	/**
	 * Test avec 4 exercices comptables et une date inscription au 13/03/19 
	 * 01/07/17 au 31/12/17 -> pas de publi
	 * 01/01/18 au 31/12/18 -> pas de publi
	 * 01/01/19 au 31/12/19 -> pas de publi
	 * 01/01/20 au 31/12/20 -> pas de publi
	 */
	public void getPublicationExerciceTest2() {
		//en entrée : 1 espace avec 3 exercices : deux avant sa date d'inscription (dont un avec moyen publiees) et deux autres apres
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setDateActionCreation(java.util.Date.from(LocalDate.parse("13-03-2019", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		List<ExerciceComptableEntity> listeExercices = new ArrayList<ExerciceComptableEntity>();
		PublicationBucketExerciceDto bucketDto = new PublicationBucketExerciceDto();
		ExerciceComptableEntity exerciceComptable1 = new ExerciceComptableEntity(1L);
		exerciceComptable1.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable1.setDateDebut(LocalDate.of(2017, 01, 01));
		exerciceComptable1.setDateFin(LocalDate.of(2017, 12, 31));
		exerciceComptable1.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable1.setPublicationEnabled(true);
		exerciceComptable1.setNoActivite(false);
		exerciceComptable1.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable2 = new ExerciceComptableEntity(2L);
		exerciceComptable2.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable2.setDateDebut(LocalDate.of(2018, 01, 01));
		exerciceComptable2.setDateFin(LocalDate.of(2018, 12, 31));
		exerciceComptable2.setPublicationEnabled(true);
		exerciceComptable2.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable2.setNoActivite(false);
		exerciceComptable2.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable3 = new ExerciceComptableEntity(3L);
		exerciceComptable3.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable3.setDateDebut(LocalDate.of(2019, 01, 01));
		exerciceComptable3.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptable3.setPublicationEnabled(true);
		exerciceComptable3.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable3.setNoActivite(false);
		exerciceComptable3.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable4 = new ExerciceComptableEntity(4L);
		exerciceComptable4.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable4.setDateDebut(LocalDate.of(2020, 01, 01));
		exerciceComptable4.setDateFin(LocalDate.of(2020, 12, 31));
		exerciceComptable4.setPublicationEnabled(true);
		exerciceComptable4.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable4.setNoActivite(false);
		exerciceComptable4.setBlacklisted(false);
		
		listeExercices.add(exerciceComptable1);
		listeExercices.add(exerciceComptable2);
		listeExercices.add(exerciceComptable3);
		listeExercices.add(exerciceComptable4);
		espaceOrganisationEntity.setExercicesComptable(listeExercices);
		when(exerciceComptableRepository.findOne(exerciceComptable1.getId())).thenReturn(exerciceComptable1);
		when(exerciceComptableRepository.findOne(exerciceComptable2.getId())).thenReturn(exerciceComptable2);
		when(exerciceComptableRepository.findOne(exerciceComptable3.getId())).thenReturn(exerciceComptable3);
		when(exerciceComptableRepository.findOne(exerciceComptable4.getId())).thenReturn(exerciceComptable4);
		List<PublicationBucketExerciceDto> listToTest = this.publicationService.getPublicationExercice(espaceOrganisationEntity,false);
		
		when(publicationExerciceService.getPublicationLivrableExercice(Mockito.anyLong(),Mockito.anyBoolean())).thenReturn(bucketDto);
		verify(publicationExerciceService, times(2)).getPublicationLivrableExercice(Mockito.anyLong(),Mockito.anyBoolean());
		
	}
	
	@Test
	/**
	 * Test avec 4 exercices comptables et une date inscription au 13/03/19 
	 * 01/07/17 au 31/12/17 -> pas de publi
	 * 01/01/18 au 31/12/18 -> no activité déclaré
	 * 01/01/19 au 31/12/19 -> pas de publi
	 * 01/01/20 au 31/12/20 -> pas de publi
	 */
	public void getPublicationExerciceTest3() {
		//en entrée : 1 espace avec 3 exercices : deux avant sa date d'inscription (dont un avec moyen publiees) et deux autres apres
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setDateActionCreation(java.util.Date.from(LocalDate.parse("13-03-2019", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		List<ExerciceComptableEntity> listeExercices = new ArrayList<ExerciceComptableEntity>();
		PublicationBucketExerciceDto bucketDto = new PublicationBucketExerciceDto();
		ExerciceComptableEntity exerciceComptable1 = new ExerciceComptableEntity(1L);
		exerciceComptable1.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable1.setDateDebut(LocalDate.of(2017, 01, 01));
		exerciceComptable1.setDateFin(LocalDate.of(2017, 12, 31));
		exerciceComptable1.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable1.setPublicationEnabled(true);
		exerciceComptable1.setNoActivite(false);
		exerciceComptable1.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable2 = new ExerciceComptableEntity(2L);
		exerciceComptable2.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable2.setDateDebut(LocalDate.of(2018, 01, 01));
		exerciceComptable2.setDateFin(LocalDate.of(2018, 12, 31));
		exerciceComptable2.setPublicationEnabled(true);
		exerciceComptable2.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable2.setNoActivite(true);
		exerciceComptable2.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable3 = new ExerciceComptableEntity(3L);
		exerciceComptable3.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable3.setDateDebut(LocalDate.of(2019, 01, 01));
		exerciceComptable3.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptable3.setPublicationEnabled(true);
		exerciceComptable3.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable3.setNoActivite(false);
		exerciceComptable3.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable4 = new ExerciceComptableEntity(4L);
		exerciceComptable4.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable4.setDateDebut(LocalDate.of(2020, 01, 01));
		exerciceComptable4.setDateFin(LocalDate.of(2020, 12, 31));
		exerciceComptable4.setPublicationEnabled(true);
		exerciceComptable4.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable4.setNoActivite(false);
		exerciceComptable4.setBlacklisted(false);
		
		listeExercices.add(exerciceComptable1);
		listeExercices.add(exerciceComptable2);
		listeExercices.add(exerciceComptable3);
		listeExercices.add(exerciceComptable4);
		espaceOrganisationEntity.setExercicesComptable(listeExercices);
		when(exerciceComptableRepository.findOne(exerciceComptable1.getId())).thenReturn(exerciceComptable1);
		when(exerciceComptableRepository.findOne(exerciceComptable2.getId())).thenReturn(exerciceComptable2);
		when(exerciceComptableRepository.findOne(exerciceComptable3.getId())).thenReturn(exerciceComptable3);
		when(exerciceComptableRepository.findOne(exerciceComptable4.getId())).thenReturn(exerciceComptable4);
		List<PublicationBucketExerciceDto> listToTest = this.publicationService.getPublicationExercice(espaceOrganisationEntity,false);
		
		when(publicationExerciceService.getPublicationLivrableExercice(Mockito.anyLong(),Mockito.anyBoolean())).thenReturn(bucketDto);
		verify(publicationExerciceService, times(3)).getPublicationLivrableExercice(Mockito.anyLong(),Mockito.anyBoolean());
		
	}
	
	@Test
	/**
	 * Test avec 4 exercices comptables et une date inscription au 13/03/21
	 * 01/07/17 au 31/12/17 -> pas de publi
	 * 01/01/18 au 31/12/18 -> pas de publi
	 * 01/01/19 au 31/12/19 -> pas de publi
	 * 01/01/20 au 31/12/20 -> pas de publi
	 */
	public void getPublicationExerciceTest4() {
		//en entrée : 1 espace avec 3 exercices : deux avant sa date d'inscription (dont un avec moyen publiees) et deux autres apres
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setDateActionCreation(java.util.Date.from(LocalDate.parse("13-03-2021", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		List<ExerciceComptableEntity> listeExercices = new ArrayList<ExerciceComptableEntity>();
		PublicationBucketExerciceDto bucketDto = new PublicationBucketExerciceDto();
		ExerciceComptableEntity exerciceComptable1 = new ExerciceComptableEntity(1L);
		exerciceComptable1.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable1.setDateDebut(LocalDate.of(2017, 01, 01));
		exerciceComptable1.setDateFin(LocalDate.of(2017, 12, 31));
		exerciceComptable1.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable1.setPublicationEnabled(true);
		exerciceComptable1.setNoActivite(false);
		exerciceComptable1.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable2 = new ExerciceComptableEntity(2L);
		exerciceComptable2.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable2.setDateDebut(LocalDate.of(2018, 01, 01));
		exerciceComptable2.setDateFin(LocalDate.of(2018, 12, 31));
		exerciceComptable2.setPublicationEnabled(true);
		exerciceComptable2.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable2.setNoActivite(false);
		exerciceComptable2.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable3 = new ExerciceComptableEntity(3L);
		exerciceComptable3.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable3.setDateDebut(LocalDate.of(2019, 01, 01));
		exerciceComptable3.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptable3.setPublicationEnabled(true);
		exerciceComptable3.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable3.setNoActivite(false);
		exerciceComptable3.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable4 = new ExerciceComptableEntity(4L);
		exerciceComptable4.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable4.setDateDebut(LocalDate.of(2020, 01, 01));
		exerciceComptable4.setDateFin(LocalDate.of(2020, 12, 31));
		exerciceComptable4.setPublicationEnabled(true);
		exerciceComptable4.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable4.setNoActivite(false);
		exerciceComptable4.setBlacklisted(false);
		
		listeExercices.add(exerciceComptable1);
		listeExercices.add(exerciceComptable2);
		listeExercices.add(exerciceComptable3);
		listeExercices.add(exerciceComptable4);
		espaceOrganisationEntity.setExercicesComptable(listeExercices);
		when(exerciceComptableRepository.findOne(exerciceComptable1.getId())).thenReturn(exerciceComptable1);
		when(exerciceComptableRepository.findOne(exerciceComptable2.getId())).thenReturn(exerciceComptable2);
		when(exerciceComptableRepository.findOne(exerciceComptable3.getId())).thenReturn(exerciceComptable3);
		when(exerciceComptableRepository.findOne(exerciceComptable4.getId())).thenReturn(exerciceComptable4);
		List<PublicationBucketExerciceDto> listToTest = this.publicationService.getPublicationExercice(espaceOrganisationEntity,false);
		
		when(publicationExerciceService.getPublicationLivrableExercice(Mockito.anyLong(),Mockito.anyBoolean())).thenReturn(bucketDto);
		verify(publicationExerciceService, times(0)).getPublicationLivrableExercice(Mockito.anyLong(),Mockito.anyBoolean());
		
	}
	private ActiviteRepresentationInteretEntity getActivite() {
		ActiviteRepresentationInteretEntity activiteRepresentationInteret = new ActiviteRepresentationInteretEntity(new Random().nextLong());
		activiteRepresentationInteret.setObjet("");
		activiteRepresentationInteret.setStatut(StatutPublicationEnum.PUBLIEE);
		HashSet<DomaineInterventionEntity> dis = new HashSet<>();
		DomaineInterventionEntity di = new DomaineInterventionEntity(1L);
		di.setIdCategorie(1L);
		di.setCategorie("Cat");
		di.setLibelle("Lib");
		dis.add(di);
		activiteRepresentationInteret.setDomainesIntervention(dis);
		ActionRepresentationInteretEntity actionRepresentationInteret = new ActionRepresentationInteretEntity(new Random().nextLong());
		actionRepresentationInteret.setTiers(Arrays.asList(new OrganisationEntity()));
		ResponsablePublicEntity rp = new ResponsablePublicEntity(1L);
		rp.setIdCategorie(1L);
		rp.setCategorie("Cat");
		rp.setLibelle("Lib");
		actionRepresentationInteret.setReponsablesPublics(new HashSet<ResponsablePublicEntity>(Arrays.asList(rp)));
		actionRepresentationInteret.setResponsablePublicAutre("RS_autre");
		DecisionConcerneeEntity dc = new DecisionConcerneeEntity(1L);
		dc.setLibelle("Lib");
		actionRepresentationInteret.setDecisionsConcernees(new HashSet<DecisionConcerneeEntity>(Arrays.asList(dc)));
		ActionMeneeEntity am = new ActionMeneeEntity(1L);
		am.setLibelle("Lib");
		actionRepresentationInteret.setActionsMenees(new HashSet<ActionMeneeEntity>(Arrays.asList(am)));
		actionRepresentationInteret.setActionMeneeAutre("AM_autre");
		actionRepresentationInteret.setObservation("");
		activiteRepresentationInteret.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteret));
		return activiteRepresentationInteret;
	}
	
	@Test
	@Ignore
	/**
	 * Test avec 4 exercices blacklistes et deux non blackliste 
	 * 01/07/17 au 31/12/17 -> blackliste
	 * 01/07/17 au 31/12/17 -> pas blackliste
	 * 01/01/19 au 31/12/19 -> blackliste
	 * 01/01/20 au 31/12/20 -> blackliste
	 * 01/01/20 au 31/12/20 -> blackliste
	 */
	public void getEspacesBlacklistesTest() {
		//en entrée : 1 espace avec 3 exercices : deux avant sa date d'inscription (dont un avec moyen publiees) et deux autres apres
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setDateActionCreation(java.util.Date.from(LocalDate.parse("13-03-2021", DateTimeFormatter.ofPattern("dd-MM-yyyy")).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		List<ExerciceComptableEntity> listeExercices = new ArrayList<ExerciceComptableEntity>();
		ExerciceComptableEntity exerciceComptable1 = new ExerciceComptableEntity(1L);
		exerciceComptable1.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable1.setDateDebut(LocalDate.of(2017, 01, 01));
		exerciceComptable1.setDateFin(LocalDate.of(2017, 12, 31));
		exerciceComptable1.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable1.setPublicationEnabled(true);
		exerciceComptable1.setNoActivite(false);
		exerciceComptable1.setBlacklisted(true);
		
		ExerciceComptableEntity exerciceComptable2 = new ExerciceComptableEntity(2L);
		exerciceComptable2.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable2.setDateDebut(LocalDate.of(2017, 01, 01));
		exerciceComptable2.setDateFin(LocalDate.of(2017, 12, 31));
		exerciceComptable2.setPublicationEnabled(true);
		exerciceComptable2.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable2.setNoActivite(false);
		exerciceComptable2.setBlacklisted(false);
		
		ExerciceComptableEntity exerciceComptable3 = new ExerciceComptableEntity(3L);
		exerciceComptable3.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable3.setDateDebut(LocalDate.of(2019, 01, 01));
		exerciceComptable3.setDateFin(LocalDate.of(2019, 12, 31));
		exerciceComptable3.setPublicationEnabled(true);
		exerciceComptable3.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable3.setNoActivite(false);
		exerciceComptable3.setBlacklisted(true);
		
		ExerciceComptableEntity exerciceComptable4 = new ExerciceComptableEntity(4L);
		exerciceComptable4.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable4.setDateDebut(LocalDate.of(2020, 01, 01));
		exerciceComptable4.setDateFin(LocalDate.of(2020, 12, 31));
		exerciceComptable4.setPublicationEnabled(true);
		exerciceComptable4.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable4.setNoActivite(false);
		exerciceComptable4.setBlacklisted(true);
		
		ExerciceComptableEntity exerciceComptable5 = new ExerciceComptableEntity(4L);
		exerciceComptable5.setEspaceOrganisation(espaceOrganisationEntity);
		exerciceComptable5.setDateDebut(LocalDate.of(2020, 01, 01));
		exerciceComptable5.setDateFin(LocalDate.of(2020, 12, 31));
		exerciceComptable5.setPublicationEnabled(true);
		exerciceComptable5.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		exerciceComptable5.setNoActivite(false);
		exerciceComptable5.setBlacklisted(true);
		
		listeExercices.add(exerciceComptable1);
		listeExercices.add(exerciceComptable2);
		listeExercices.add(exerciceComptable3);
		listeExercices.add(exerciceComptable4);
		listeExercices.add(exerciceComptable5);
		espaceOrganisationEntity.setExercicesComptable(listeExercices);
		when(exerciceComptableRepository.findAllByBlacklisted(Mockito.anyBoolean())).thenReturn(listeExercices);

		List<PublicationRechercheBlacklistDto> listToTest = this.publicationService.getEspacesBlacklistes();
		

		assertThat(listToTest.size()).isEqualTo(3);
	}
	/**
	 * public PublicationDto getDernierContenuPourPublication(final Long espaceId)
	 * cas sans publication autre que dépublié
	 */
	@Test
	public void getDernierContenuPourPublicationSansPublicationAutreQueDepublie() {
		Optional<PublicationEntity> publicationEntity = Optional.empty();
		
		Mockito.doReturn(publicationEntity).when(this.publicationRepository).findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(ESPACE_ID, StatutPublicationEnum.DEPUBLIEE);
		
		PublicationDto retour = this.publicationService.getDernierContenuPourPublication(ESPACE_ID);
		
		Assert.assertNull(retour.getId());		
	}
	
	/**
	 * public PublicationDto getDernierContenuPourPublication(final Long espaceId)
	 * cas avec publication autre que dépublié et sans PublicationActiviteEntity
	 * @throws ParseException 
	 */
	@Test
	public void getDernierContenuPourPublicationAvecPublicationAutreQueDepublieSansPublicationActivite() throws ParseException {
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_ID);
		
		PublicationEntity entity = new PublicationEntity();
		entity.setEspaceOrganisation(espaceOrganisationEntity);
		
		Optional<PublicationEntity> dernierePublicationEntity = Optional.of(entity);
		
		PublicationDto publicationDto = new PublicationDto();
		
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		PublicationEntity premierePublicationEspace = new PublicationEntity();
		premierePublicationEspace.setCreationDate(format.parse("26/10/2017"));
		
		Mockito.doReturn(dernierePublicationEntity).when(this.publicationRepository).findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(ESPACE_ID, StatutPublicationEnum.DEPUBLIEE);
		Mockito.doReturn(premierePublicationEspace).when(this.publicationRepository).findFirstByEspaceOrganisationIdAndStatutNotOrderByIdAsc(ESPACE_ID, StatutPublicationEnum.DEPUBLIEE);
		Mockito.doReturn(publicationDto).when(this.publicationBaseTransformer).modelToDto(dernierePublicationEntity.get());
		Mockito.doReturn(null).when(this.publicationActiviteRepository).getLatestPublicationActivite(dernierePublicationEntity.get().getEspaceOrganisation().getId());
		
		PublicationDto retour = this.publicationService.getDernierContenuPourPublication(ESPACE_ID);
		
		Assert.assertEquals(false, retour.getIsActivitesPubliees());
		Assert.assertEquals(premierePublicationEspace.getCreationDate(), retour.getPremierePublicationDate());		
	}
	/**
	 * public PublicationDto getDernierContenuPourPublication(final Long espaceId)
	 * cas avec publication autre que dépublié et avec PublicationActiviteEntity
	 * @throws ParseException 
	 */
	@Test
	public void getDernierContenuPourPublicationAvecPublicationAutreQueDepublieAvecPublicationActivite() throws ParseException {
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_ID);
		
		PublicationEntity entity = new PublicationEntity();
		entity.setEspaceOrganisation(espaceOrganisationEntity);
		
		Optional<PublicationEntity> dernierePublicationEntity = Optional.of(entity);
		
		PublicationDto publicationDto = new PublicationDto();
		
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		PublicationEntity premierePublicationEspace = new PublicationEntity();
		premierePublicationEspace.setCreationDate(format.parse("26/10/2017"));
		
		PublicationActiviteEntity publicationActiviteEntity = new PublicationActiviteEntity();
		publicationActiviteEntity.setCreationDate(format.parse("02/10/2018"));
		
		Mockito.doReturn(dernierePublicationEntity).when(this.publicationRepository).findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(ESPACE_ID, StatutPublicationEnum.DEPUBLIEE);
		Mockito.doReturn(premierePublicationEspace).when(this.publicationRepository).findFirstByEspaceOrganisationIdAndStatutNotOrderByIdAsc(ESPACE_ID, StatutPublicationEnum.DEPUBLIEE);
		Mockito.doReturn(publicationDto).when(this.publicationBaseTransformer).modelToDto(dernierePublicationEntity.get());
		Mockito.doReturn(publicationActiviteEntity).when(this.publicationActiviteRepository).getLatestPublicationActivite(dernierePublicationEntity.get().getEspaceOrganisation().getId());
		
		PublicationDto retour = this.publicationService.getDernierContenuPourPublication(ESPACE_ID);
		
		Assert.assertEquals(publicationActiviteEntity.getCreationDate(), retour.getDateDernierePublicationActivite());
		Assert.assertEquals(true, retour.getIsActivitesPubliees());		
		Assert.assertEquals(premierePublicationEspace.getCreationDate(), retour.getPremierePublicationDate());		
	}
}
