/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.publication;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationBucketTransformer;
import fr.hatvp.registre.commons.dto.publication.PublicationBucketDto;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Test de la classe {@link PublicationBucketTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class PublicationBucketTransformerTest {

    /** Transformeur de production de livrable. */
    @Autowired
    private PublicationBucketTransformer publicationBucketTransformer;

    /**
     * Test de la méthode {@link PublicationBucketTransformer#listPublicationToPublicationBucket(List)}
     */
    @Test
    public void testPublicationListToPublicationBucket() {

        final List<PublicationEntity> entities = new ArrayList<>();
        entities.add(CommonTestClass.getPublicationEntity());
        entities.add(CommonTestClass.getPublicationEntity());
        entities.add(CommonTestClass.getPublicationEntity());

        final PublicationBucketDto bucketDto =
                this.publicationBucketTransformer.listPublicationToPublicationBucket(entities);

        Assert.assertNotNull(bucketDto);
//        Assert.assertNotNull(bucketDto.getHistorique());
        Assert.assertNotNull(bucketDto.getLogo());
        Assert.assertNotNull(bucketDto.getLogoType());
        Assert.assertNotNull(bucketDto.getPublicationCourante());

        Assert.assertNull(bucketDto.getPublicationCourante().getId());
        Assert.assertNull(bucketDto.getPublicationCourante().getVersion());

        Assert.assertEquals(3, entities.size());
//        Assert.assertEquals(3, bucketDto.getHistorique().size());
    }
}
