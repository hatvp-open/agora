/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.transformer.proxy.DirigeantTransformer;
import fr.hatvp.registre.commons.dto.CollaborateurDto;
import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.espace.DirigeantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.repository.espace.DirigeantRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

/**
 * Test du service Dirigeant {@link fr.hatvp.registre.business.DirigeantService}.
 *
 * @version $Revision$ $Date${0xD}
 */

public class DirigeantServiceTest {

	private final static Long ESPACE_ORGANISATION_ID = 3L;
	private final static Long UNALLOWED_ESPACE_ORGANISATION_ID = 4L;

	/** Repository Dirigeants. */
	@Mock
	private DirigeantRepository dirigeantRepository;

	/** Repository Espace Organisation. */
	@Mock
	private EspaceOrganisationRepository espaceOrganisationRepository;

	@Mock
	private EspaceOrganisationService espaceService;

	/** Transformer Dirigeants. */
	@Mock
	private DirigeantTransformer dirigeantTransformer;

	/** Service Dirigeants. */
	@InjectMocks
	private DirigeantServiceImpl dirigeantService;

	@Before
	public void before() throws Exception {
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method: getAllDirigeants(final Long espaceOrganisationId)
	 */
	@Test
	public void testGetAllDirigeants() throws Exception {
		/*
		 * Data
		 */
		final Long espaceOrganisationId = 1L;

		final List<DirigeantEntity> dirigeantEntities = new ArrayList<>();
		final DirigeantEntity dirigeantEntity = new DirigeantEntity();
		dirigeantEntities.add(dirigeantEntity);

		final CollaborateurDto collaborateurDto = new CollaborateurDto();

		/*
		 * Mock
		 */
		Mockito.doReturn(dirigeantEntities).when(this.dirigeantRepository).findByEspaceOrganisationId(espaceOrganisationId);
		Mockito.doReturn(collaborateurDto).when(this.dirigeantTransformer).modelToDto(dirigeantEntity);

		/*
		 * Test
		 */
		final List<DirigeantDto> result = this.dirigeantService.getAllDirigeants(espaceOrganisationId);
		Assert.assertNotNull(result);
	}

	/**
	 * Ajouter un Dirigeant à un espace avec d'autres Dirigeant Method: addDirigeant(final DirigeantDto dirigeant)
	 */
	@Test
	public void testAddDirigeant() throws Exception {
		/*
		 * Data
		 */
		final DirigeantDto dto = getDirigeantDto();
		final DirigeantEntity entity = getDirigeantEntity(dto);

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_ORGANISATION_ID);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setDenomination(RegistreUtils.randomString(20));
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ESPACE_ORGANISATION_ID);
		Mockito.doReturn(entity).when(this.dirigeantTransformer).dtoToModel(dto);
		Mockito.doReturn(dto).when(this.dirigeantTransformer).modelToDto(entity);
		Mockito.doReturn(entity).when(this.dirigeantRepository).saveAndFlush(entity);

		final DirigeantDto dirigeantDto = this.dirigeantService.addDirigeant(dto, ESPACE_ORGANISATION_ID);

		Assert.assertNotNull(dirigeantDto);
		Assert.assertEquals(dirigeantDto.getCivilite(), dto.getCivilite());
		Assert.assertEquals(dirigeantDto.getNom(), dto.getNom());
		Assert.assertEquals(dirigeantDto.getPrenom(), dto.getPrenom());
		Assert.assertEquals(dirigeantDto.getFonction(), dto.getFonction());
		Assert.assertEquals(dirigeantDto.getEspaceOrganisationId(), dto.getEspaceOrganisationId());
	}

	private DirigeantDto getDirigeantDto() {
		final DirigeantDto dto = new DirigeantDto();
		dto.setId(RegistreUtils.randomLong(2));
		dto.setVersion(0);
		dto.setPrenom(RegistreUtils.randomString(10));
		dto.setNom(RegistreUtils.randomString(10));
		dto.setCivilite(CiviliteEnum.M);
		dto.setEspaceOrganisationId(ESPACE_ORGANISATION_ID);
		dto.setFonction(RegistreUtils.randomString(50));

		return dto;
	}

	/**
	 * Method: deleteDirigeant(final Long id)
	 */
	@Test
	public void testDeleteDirigeant() throws Exception {
		/*
		 * Data
		 */
		final DirigeantEntity model = getDirigeantEntity(getDirigeantDto());

		/*
		 * Mock
		 */
		Mockito.doReturn(model).when(this.dirigeantRepository).findByIdAndEspaceOrganisationId(model.getId(), ESPACE_ORGANISATION_ID);

		this.dirigeantService.deleteDirigeant(model.getId(), ESPACE_ORGANISATION_ID);
		
		verify(dirigeantRepository, times(1)).delete(model);

	}

	/**
	 * Test la suppression d'un dirigeant pour un espace différent.
	 */
	@Test
	public void testDeleteDirigeantNotAllowed() {
		/*
		 * Data
		 */
		final DirigeantEntity model = getDirigeantEntity(getDirigeantDto());

		/*
		 * Mock
		 */
		Mockito.doReturn(model).when(this.dirigeantRepository).findByIdAndEspaceOrganisationId(model.getId(), ESPACE_ORGANISATION_ID);
		
		try {
			this.dirigeantService.deleteDirigeant(model.getId(), UNALLOWED_ESPACE_ORGANISATION_ID);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.MSG_404.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Test de la méthode edit dirigeant
	 */
	@Test
	public void testEditDirigeant() {
		final DirigeantDto dto = new DirigeantDto();
		dto.setFonction(RegistreUtils.randomString(30));
		dto.setPrenom(RegistreUtils.randomString(20));
		dto.setNom(RegistreUtils.randomString(41));
		dto.setCivilite(CiviliteEnum.M);

		final DirigeantEntity entity = getDirigeantEntity(dto);
		Mockito.doReturn(entity).when(this.dirigeantTransformer).dtoToModel(dto);
		Mockito.doReturn(dto).when(this.dirigeantTransformer).modelToDto(entity);
		Mockito.doReturn(entity).when(this.dirigeantRepository).save(entity);

		final DirigeantDto savedDto = this.dirigeantService.editDirigeant(dto, ESPACE_ORGANISATION_ID);

		Assert.assertNotNull(savedDto);
		Assert.assertEquals(dto.getFonction(), savedDto.getFonction());
		Assert.assertEquals(dto.getNom(), savedDto.getNom());
		Assert.assertEquals(dto.getPrenom(), savedDto.getPrenom());
		Assert.assertEquals(dto.getCivilite(), savedDto.getCivilite());
	}

	/**
	 * générer une entité DirigeantEntity pour tests
	 *
	 * @param dto
	 *            dto dirigeant
	 * @return objet entité de test
	 */
	private DirigeantEntity getDirigeantEntity(final DirigeantDto dto) {
		final DirigeantEntity model = new DirigeantEntity();
		model.setId(dto.getId());
		model.setVersion(dto.getVersion());

		model.setNom(dto.getNom());
		model.setPrenom(dto.getPrenom());
		model.setFonction(dto.getFonction());
		model.setCivilite(dto.getCivilite());

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_ORGANISATION_ID);
		model.setEspaceOrganisation(espaceOrganisationEntity);
		return model;
	}

}
