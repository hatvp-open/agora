/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.publication;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.publication.PublicationRechercheTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.publication.PublicationRechercheDto;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

/**
 * Test de la classe {@link PublicationRechercheTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
    loader = AnnotationConfigContextLoader.class)
public class PublicationRechercheTransformerTest {

    /** Transformer des publications. */
    @Autowired
    private PublicationRechercheTransformer publicationTransformer;

    /**
     * Test de la métode modelToDto de {@link PublicationRechercheTransformer#dtoToModel(AbstractCommonDto)}
     */
    @Test
    public void testModelToDto() {
        final PublicationEntity entity = CommonTestClass.getPublicationEntity();

        final PublicationRechercheDto dto = this.publicationTransformer.modelToDto(entity);

        Assert.assertNotNull(dto);

        Assert.assertNull(dto.getId());
        Assert.assertNull(dto.getVersion());

        Assert.assertEquals(entity.getDenomination(), dto.getDenomination());
        Assert.assertEquals(entity.getNationalId(), dto.getNationalId());
       Assert.assertEquals(entity.getCategorieOrganisation().name(), dto.getCategorieOrganisation().name());
       Assert.assertEquals(entity.getCategorieOrganisation().getLabel(), dto.getCategorieOrganisation().getLabel());
        Assert.assertEquals(entity.getCategorieOrganisation().getGroup(), dto.getCategorieOrganisation().getGroup());
        Assert.assertEquals(entity.getCreationDate(), entity.getCreationDate());
    }

    /**
     * Test de la métode modelToDto de {@link PublicationRechercheTransformer#modelToDto(AbstractCommonEntity)}
     */
    @Test
    public void dtoToModel() {
        final PublicationRechercheDto dto = CommonTestClass.getPubRechercheDto();

        final PublicationEntity entity = this.publicationTransformer.dtoToModel(dto);

        Assert.assertNotNull(entity);

        Assert.assertEquals(entity.getDenomination(), dto.getDenomination());
        Assert.assertEquals(entity.getNationalId(), dto.getNationalId());

    }
}
