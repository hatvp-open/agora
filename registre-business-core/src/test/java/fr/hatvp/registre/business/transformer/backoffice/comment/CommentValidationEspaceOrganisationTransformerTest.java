/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment;

import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.backoffice.comment.impl.CommentEspaceOrganisationBoTransformerImpl;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentEspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

/**
 * Test de la classe {@link CommentDeclarantBoTransformer}.
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class CommentValidationEspaceOrganisationTransformerTest
    extends AbstractCommentTest<CommentEspaceOrganisationEntity>
{

    /**
     * Préciser la classe de commentaire.
     */
    public CommentValidationEspaceOrganisationTransformerTest()
    {
        this.entityClass = CommentEspaceOrganisationEntity.class;
    }

    /**
     * Repository des espaces organisations.
     */
    @Mock
    private EspaceOrganisationRepository espaceOrganisationRepository;

    /**
     * Transformer à tester.
     */
    @InjectMocks
    private CommentEspaceOrganisationBoTransformerImpl commentEspaceOrganisationBoTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    @Test
    public void testModelToDto() throws InstantiationException, IllegalAccessException
    {
        super.testModelToDto();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void additionalModelToDtoEspaceOrganisationAssertions(
            final CommentEspaceOrganisationEntity entity, final CommentBoDto dto)
    {
        super.additionalModelToDtoEspaceOrganisationAssertions(entity, dto);
        Assert.assertEquals(entity.getEspaceOrganisationEntity().getId(), dto.getContextId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    CommentBoDto getModelToDtoTransformerResult(final CommentEspaceOrganisationEntity entity)
    {
        entity.setEspaceOrganisationEntity(this.getEspaceOraganisation());
        return this.commentEspaceOrganisationBoTransformer.commentModelToDto(entity);
    }

    @Override
    @Test
    public void testDtoToModel()
    {
        super.testDtoToModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void additionalDtoToModelEspaceOrganisationAssertions(final CommentBoDto dto,
            final CommentEspaceOrganisationEntity entity)
    {
        super.additionalDtoToModelEspaceOrganisationAssertions(dto, entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    CommentEspaceOrganisationEntity getDtoToModelTransformerResult(final CommentBoDto dto)
    {
        when(this.espaceOrganisationRepository.findOne(ID))
                .thenReturn(this.getEspaceOraganisation());
        return this.commentEspaceOrganisationBoTransformer.dtoToModel(dto);
    }

    /**
     * @return une instance de {@link EspaceOrganisationEntity}
     */
    private EspaceOrganisationEntity getEspaceOraganisation()
    {
        final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(ID);
        espaceOrganisationEntity.setVersion(VERSION);
        espaceOrganisationEntity.setCreationDate(new Date());
        return espaceOrganisationEntity;
    }
}
