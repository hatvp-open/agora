/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment;

import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.backoffice.comment.impl.CommentValidationInscriptionBoTransformerImpl;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentValidationEspaceEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;

/**
 * Test de la classe {@link CommentDeclarantBoTransformer}.
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class CommentValidationTransformerTest
    extends AbstractCommentTest<CommentValidationEspaceEntity>
{

    /**
     * Préciser la classe de commentaire.
     */
    public CommentValidationTransformerTest()
    {
        this.entityClass = CommentValidationEspaceEntity.class;
    }

    /**
     * Repository des espaces organisations.
     */
    @Mock
    private InscriptionEspaceRepository inscriptionEspaceRepository;

    /**
     * Transformer à tester.
     */
    @InjectMocks
    private CommentValidationInscriptionBoTransformerImpl commentValidationInscriptionBoTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    @Test
    public void testModelToDto() throws InstantiationException, IllegalAccessException
    {
        super.testModelToDto();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void additionalModelToDtoEspaceOrganisationAssertions(
            final CommentValidationEspaceEntity entity, final CommentBoDto dto)
    {
        super.additionalModelToDtoEspaceOrganisationAssertions(entity, dto);
        Assert.assertEquals(entity.getInscriptionEspaceEntity().getId(), dto.getContextId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    CommentBoDto getModelToDtoTransformerResult(final CommentValidationEspaceEntity entity)
    {
        entity.setInscriptionEspaceEntity(this.getInscriptionEspaceOraganisation());
        return this.commentValidationInscriptionBoTransformer.commentModelToDto(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Test
    public void testDtoToModel()
    {
        super.testDtoToModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void additionalDtoToModelEspaceOrganisationAssertions(final CommentBoDto dto,
            final CommentValidationEspaceEntity entity)
    {
        super.additionalDtoToModelEspaceOrganisationAssertions(dto, entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    CommentValidationEspaceEntity getDtoToModelTransformerResult(final CommentBoDto dto)
    {
        when(this.inscriptionEspaceRepository.findOne(ID))
                .thenReturn(this.getInscriptionEspaceOraganisation());
        return this.commentValidationInscriptionBoTransformer.dtoToModel(dto);
    }

    /**
     * @return une instance de {@link InscriptionEspaceEntity}
     */
    private InscriptionEspaceEntity getInscriptionEspaceOraganisation()
    {
        final InscriptionEspaceEntity inscriptionEspaceEntity = new InscriptionEspaceEntity();
        inscriptionEspaceEntity.setId(ID);
        inscriptionEspaceEntity.setVersion(VERSION);
        inscriptionEspaceEntity.setActif(true);
        inscriptionEspaceEntity.setDateDemande(new Date());
        inscriptionEspaceEntity.setFavori(false);
        return inscriptionEspaceEntity;
    }
}
