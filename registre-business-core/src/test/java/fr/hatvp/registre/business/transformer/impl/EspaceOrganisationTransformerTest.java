/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.espace.AssociationEntity;
import fr.hatvp.registre.persistence.entity.espace.DirigeantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;

/**
 * Test de la classe {@link EspaceOrganisationTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
    loader = AnnotationConfigContextLoader.class)
public class EspaceOrganisationTransformerTest {

    /** Transformeur de l'espace organisation. */
    @Autowired
    private EspaceOrganisationTransformer espaceOrganisationTransformer;

    /** Nom d'usage utilisé pour les tests */
    private final String NOMUSAGE_TEST = "Organisation de test";

    /**
     * Test de la méthode {@link EspaceOrganisationTransformer#modelToDto(AbstractCommonEntity)}
     */
    @Test
    public void tesModelToDto() {
        final EspaceOrganisationEntity entity = this.getEspaceOrganisation();
        final EspaceOrganisationDto espaceOrganisationDto = this.espaceOrganisationTransformer
            .modelToDto(entity);

        Assert.assertNotNull(espaceOrganisationDto);
        Assert.assertEquals(espaceOrganisationDto.getId(), entity.getId());
        Assert.assertEquals(espaceOrganisationDto.getVersion(), entity.getVersion());
        Assert.assertEquals(espaceOrganisationDto.getChiffreAffaire(),
            entity.getChiffreAffaire());

        Assert.assertEquals(entity.getSecteursActivites().split(",").length,
            espaceOrganisationDto.getListSecteursActivites().size());
        Assert.assertEquals(entity.getSecteursActivites().split(",").length,
            espaceOrganisationDto.getListSecteursActivites().size());

        Assert.assertEquals(espaceOrganisationDto.getAnneeChiffreAffaire(),
            entity.getAnneeChiffreAffaire());

        Assert.assertEquals(espaceOrganisationDto.getAdresse(), entity.getAdresse());
        Assert.assertEquals(espaceOrganisationDto.getVille(), entity.getVille());
        Assert.assertEquals(espaceOrganisationDto.getCodePostal(), entity.getCodePostal());
        Assert.assertEquals(espaceOrganisationDto.getPays(), entity.getPays());

        Assert.assertEquals(1, espaceOrganisationDto.getDirigeants().size());

        Assert.assertEquals(espaceOrganisationDto.getNomUsage(), entity.getNomUsage());
    }

    /**
     * Test de la méthode {@link EspaceOrganisationTransformer#dtoToModel(AbstractCommonDto)}
     */
    @Test
    public void testDtoToModel() {
        final EspaceOrganisationDto dto = this.getEspaceOrganisationDto();
        final EspaceOrganisationEntity entity = this.espaceOrganisationTransformer.dtoToModel(dto);

        Assert.assertNotNull(entity);
        Assert.assertEquals(entity.getId(), dto.getId());
        Assert.assertEquals(entity.getVersion(), dto.getVersion());
        Assert.assertEquals(entity.getChiffreAffaire(), dto.getChiffreAffaire());

        Assert.assertEquals(dto.getListSecteursActivites().size(), entity.getSecteursActivites().split(",").length);
        Assert.assertEquals(dto.getListSecteursActivites().size(), entity.getSecteursActivites().split(",").length);

        Assert.assertEquals(entity.getAnneeChiffreAffaire(), dto.getAnneeChiffreAffaire());

        Assert.assertEquals(entity.getAdresse(), dto.getAdresse());
        Assert.assertEquals(entity.getVille(), dto.getVille());
        Assert.assertEquals(entity.getCodePostal(), dto.getCodePostal());
        Assert.assertEquals(entity.getPays(), dto.getPays());

        Assert.assertEquals(entity.getNomUsage(), dto.getNomUsage());
    }

    /**
     * Création d'un nouvel Espace Organisation.
     *
     * @return entité espace organisation
     */
    private EspaceOrganisationEntity getEspaceOrganisation() {
        final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();

        espaceOrganisationEntity.setId(RegistreUtils.randomLong(1));
        espaceOrganisationEntity.setVersion(1);
        espaceOrganisationEntity.setCreationDate(new Date());
        espaceOrganisationEntity.setNonDeclarationTiers(true);
        espaceOrganisationEntity.setEspaceActif(true);

        espaceOrganisationEntity.setChiffreAffaire(RegistreUtils.randomLong(20));

        espaceOrganisationEntity
            .setSecteursActivites(SecteurActiviteEnum.AGRI.name());
        espaceOrganisationEntity
            .setNiveauIntervention(NiveauInterventionEnum.LOCAL.name());

        espaceOrganisationEntity.setAnneeChiffreAffaire(RegistreUtils.randomLong(4).intValue());
        espaceOrganisationEntity.setEmailDeContact(RegistreUtils.randomString(30));
        espaceOrganisationEntity.setTelephoneDeContact(RegistreUtils.randomString(15));
        espaceOrganisationEntity.setNonPublierMonAdresseEmail(false);
        espaceOrganisationEntity.setNonPublierMonTelephoneDeContact(true);
        espaceOrganisationEntity.setNomUsage(NOMUSAGE_TEST);

        final AssociationEntity asso = new AssociationEntity();
        asso.setEspaceOrganisation(espaceOrganisationEntity);
        asso.setId(RegistreUtils.randomLong(1));
        asso.setVersion(1);

        final DirigeantEntity dirigeantEntity = new DirigeantEntity();
        dirigeantEntity.setEspaceOrganisation(espaceOrganisationEntity);
        dirigeantEntity.setVersion(1);
        dirigeantEntity.setId(RegistreUtils.randomLong(1));
        dirigeantEntity.setNom(RegistreUtils.randomString(20));
        dirigeantEntity.setPrenom(RegistreUtils.randomString(20));
        espaceOrganisationEntity.getDirigeants().add(dirigeantEntity);

        final OrganisationEntity o = new OrganisationEntity();
        o.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
        o.setDenomination(RegistreUtils.randomString(50));
        o.setSiren(RegistreUtils.randomString(30));
        espaceOrganisationEntity.setOrganisation(o);

        espaceOrganisationEntity.setAdresse(RegistreUtils.randomString(20));
        espaceOrganisationEntity.setCodePostal(RegistreUtils.randomString(5));
        espaceOrganisationEntity.setPays(RegistreUtils.randomString(5));
        espaceOrganisationEntity.setVille(RegistreUtils.randomString(5));

        return espaceOrganisationEntity;
    }

    /**
     * Créer un {@link EspaceOrganisationDto}
     *
     * @return EspaceOrganisationDto
     */
    private EspaceOrganisationDto getEspaceOrganisationDto() {
        final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
        espaceOrganisationDto.setId(RegistreUtils.randomLong(1));
        espaceOrganisationDto.setVersion(1);
        espaceOrganisationDto.setCreationDate(new Date());
        espaceOrganisationDto.setEspaceActif(true);

        espaceOrganisationDto.setChiffreAffaire(RegistreUtils.randomLong(20));

        espaceOrganisationDto.getListSecteursActivites().add(SecteurActiviteEnum.AGRI);
        espaceOrganisationDto.getListNiveauIntervention().add(NiveauInterventionEnum.EUROPEEN);

        espaceOrganisationDto.setAnneeChiffreAffaire(RegistreUtils.randomLong(4).intValue());
        espaceOrganisationDto.setEmailDeContact(RegistreUtils.randomString(30));
        espaceOrganisationDto.setTelephoneDeContact(RegistreUtils.randomString(15));
        espaceOrganisationDto.setNonPublierMonAdresseEmail(false);
        espaceOrganisationDto.setNonPublierMonTelephoneDeContact(true);

        espaceOrganisationDto.setAdresse(RegistreUtils.randomString(20));
        espaceOrganisationDto.setCodePostal(RegistreUtils.randomString(5));
        espaceOrganisationDto.setPays(RegistreUtils.randomString(5));
        espaceOrganisationDto.setVille(RegistreUtils.randomString(5));
        espaceOrganisationDto.setNomUsage(NOMUSAGE_TEST);

        return espaceOrganisationDto;
    }
}
