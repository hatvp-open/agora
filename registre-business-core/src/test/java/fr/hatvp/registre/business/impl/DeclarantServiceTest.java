/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static fr.hatvp.registre.commons.utils.RegistreUtils.codeTo64;
import static fr.hatvp.registre.commons.utils.RegistreUtils.decode64ToString;
import static fr.hatvp.registre.commons.utils.RegistreUtils.generateNewDate;
import static fr.hatvp.registre.commons.utils.RegistreUtils.generateOldDate;
import static fr.hatvp.registre.commons.utils.RegistreUtils.randomString;
import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityExistsException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import fr.hatvp.registre.business.email.AccountMailer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.commons.dto.DeclarantContactDto;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.CategoryTelephoneMail;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;

/**
 * Test du service declarant {@link fr.hatvp.registre.business.DeclarantService}.
 *
 * @version $Revision$ $Date${0xD}
 */

public class DeclarantServiceTest {

	/** Le repository du déclarant. */
	@Mock
	private DeclarantRepository declarantRepository;

	/** Le transformeur de l'entitée déclarant. */
	@Mock
	private DeclarantTransformer declarantTransformer;

	/** Le crypteur des mots de passes. */
	@Mock
	private PasswordEncoder passwordEncoder;

	/** Account mailer. */
	@Mock
	private AccountMailer accountMailer;

	/** Le service à tester. */
	@InjectMocks
	private DeclarantServiceImpl declarantService;

	/**
	 * Initialisation de la classe de test.
	 *
	 * @throws Exception
	 *             exception d'initialisation.
	 */
	@Before
	public void setUp() throws Exception {
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Mail utilisé pour les tests.
	 */
	private final String DECLARANT_EMAIL_TEST = "test@hatvp.fr";
	/**
	 * Id du déclarant utilisé pour les tests.
	 */
	private final Long DECLARANT_ID_TEST = 1L;

	/**
	 * Test de la méthode loadDeclarantById {@link DeclarantServiceImpl#loadDeclarantByEmail(String, boolean)}
	 */
	@Test
	public void testLoadDeclarantByEmail() {
		// init un model déclarant
		final DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(this.DECLARANT_ID_TEST);
		declarantEntity.setEmail(this.DECLARANT_EMAIL_TEST);

		// init un DTO declarant
		final DeclarantDto declarantDto = new DeclarantDto();
		declarantDto.setId(this.DECLARANT_ID_TEST);
		declarantDto.setEmail(this.DECLARANT_EMAIL_TEST);

		// Mock de la récupération du déclarant de la base de données
		Mockito.doReturn(declarantEntity).when(this.declarantRepository).findByEmailIgnoreCase(this.DECLARANT_EMAIL_TEST);

		// Mock de la transformation du model en DTO
		Mockito.doReturn(declarantDto).when(this.declarantTransformer).modelToDto(declarantEntity);

		// Appel du service
		final DeclarantDto declarantDtoRetour = this.declarantService.loadDeclarantByEmail(this.DECLARANT_EMAIL_TEST, false);

		// Assertations
		Assert.assertNotNull(declarantEntity);
		Assert.assertNotNull(declarantDto);
		Assert.assertNotNull(declarantDtoRetour);
		Assert.assertEquals(declarantDto.getId(), declarantEntity.getId());
		Assert.assertEquals(declarantDtoRetour.getId(), declarantEntity.getId());
		Assert.assertEquals(declarantDtoRetour.getEmail(), declarantEntity.getEmail());
	}

	/**
	 * Test de la méthode loadDeclarantById {@link DeclarantServiceImpl#loadDeclarantById(Long)}.
	 */
	@Test
	public void testLoadDeclarantById() {
		final Long declarantId = 1L;

		final DeclarantDto declarantDto = this.getUser();
		final DeclarantEntity declarantEntity = this.getDeclarant(declarantDto);

		Mockito.doReturn(declarantEntity).when(this.declarantRepository).findOne(declarantId);

		Mockito.doReturn(declarantDto).when(this.declarantTransformer).modelToDto(declarantEntity);

		final DeclarantDto declarantDtoSaved = this.declarantService.loadDeclarantById(declarantId);

		Assert.assertNotNull(declarantDto);
		Assert.assertEquals(declarantDto.getEmail(), declarantDtoSaved.getEmail());
	}

	/**
	 * Test de la méthode saveDeclarant avec une adresse email qui n'existe pas dans la bdd {@link DeclarantServiceImpl#saveDeclarant(DeclarantDto)}.
	 */
	@Test
	public void testSaveDeclarantAvecAdresseEmailNonExistanteDansLaBdd() {

		final DeclarantDto declarantToSave = this.getUser();

		final DeclarantEntity declarant = new DeclarantEntity();
		declarant.setEmail(declarantToSave.getEmail());

		Mockito.doReturn(randomString(10)).when(this.passwordEncoder).encode(declarantToSave.getPassword());

		Mockito.doReturn(declarant).when(this.declarantTransformer).dtoToModel(declarantToSave);

		Mockito.doReturn(null).when(this.declarantRepository).findByEmailIgnoreCase(declarantToSave.getEmail());

		Mockito.doReturn(new DeclarantEntity()).when(this.declarantRepository).save(declarant);

		Mockito.doReturn(declarantToSave).when(this.declarantTransformer).modelToDto(declarant);

		// Appel du service.
		final DeclarantDto declarantDto = this.declarantService.saveDeclarant(declarantToSave);

		Assert.assertNotNull(declarantDto);
	}

	/**
	 * Test de la méthode verifierSiLeCompteExiste avec un email qui n'existe pas {@link DeclarantServiceImpl#saveDeclarant(DeclarantDto)} .
	 */
	@Test
	public void testSaveDeclarantAvecUneAdressEmailNonExistante() {

		final DeclarantDto declarantToSave = this.getUser();

		Mockito.doReturn(null).when(this.declarantRepository).findByEmailIgnoreCaseOrEmailTempIgnoreCase(declarantToSave.getEmail(), declarantToSave.getEmail());

		final boolean ok = this.declarantService.verifierSiLeCompteExiste(declarantToSave);

		Assert.assertNotNull(ok);
		Assert.assertEquals(true, ok);
	}

	/**
	 * Test de la méthode verifierSiLeCompteExiste avec un email qui existe et le compte active {@link DeclarantServiceImpl#verifierSiLeCompteExiste(DeclarantDto)} .
	 */
	@Test(expected = EntityExistsException.class)
	public void testSaveDeclarantAvecUneAdressEmailExistanteEtCompteActive() {

		final DeclarantDto declarantToSave = this.getUser();
		declarantToSave.setActivated(true);

		Mockito.doReturn(this.getDeclarant(declarantToSave)).when(this.declarantRepository).findByEmailIgnoreCaseOrEmailTempIgnoreCase(declarantToSave.getEmail(),
				declarantToSave.getEmail());

		this.declarantService.verifierSiLeCompteExiste(declarantToSave);

	}

	/**
	 * Test de la méthode verifierSiLeCompteExiste avec un email qui existe, non activé et a expiré {@link DeclarantServiceImpl#verifierSiLeCompteExiste(DeclarantDto)}.
	 * @throws ParseException 
	 */
	@Test
	public void testSaveDeclarantAvecUneAdressEmailExistanteMaisExpire() throws ParseException {
	
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dateInString = "15/03/2000";
		Date date = sdf.parse(dateInString);

		DeclarantEntity oldDeclarant = new DeclarantEntity(10L);
		oldDeclarant.setEmail("email@old.fr");
		oldDeclarant.setEmailTemp("emailTemp@old.fr");
		oldDeclarant.setActivated(false);
		oldDeclarant.setEmailCodeExpiryDate(date);
		
		DeclarantDto  declarantDto = new DeclarantDto();		
		declarantDto.setId(1L);
		declarantDto.setEmail("email@old.fr");

		Mockito.doReturn(oldDeclarant).when(this.declarantRepository).findByEmailIgnoreCaseOrEmailTempIgnoreCase(oldDeclarant.getEmail(),
				oldDeclarant.getEmail());

		// lancer le servie.
		final boolean ok = this.declarantService.verifierSiLeCompteExiste(declarantDto);

		Assert.assertNotNull(ok);
		Assert.assertEquals(true, ok);
	}

	/**
	 * Test de la méthode verifierSiLeCompteExiste avec un email qui existe, non activé et non expiré.
	 * <p>
	 * {@link DeclarantServiceImpl#verifierSiLeCompteExiste(DeclarantDto)}.
	 */
	@Test(expected = EntityExistsException.class)
	public void testSaveDeclarantAvecUneAdressEmailExistanteMaisNonExpire() {

		final DeclarantDto declarantToSave = this.getUser();
		final DeclarantEntity declarantEntity = this.getDeclarant(declarantToSave);

		// ajouter une date supérieur.
		declarantEntity.setEmailCodeExpiryDate(generateNewDate(2));

		Mockito.doReturn(declarantEntity).when(this.declarantRepository).findByEmailIgnoreCaseOrEmailTempIgnoreCase(declarantToSave.getEmail(), declarantToSave.getEmail());

		// lancer le servie.
		this.declarantService.verifierSiLeCompteExiste(declarantToSave);

	}

	/**
	 * Test de la méthode isDateExpired avec une date expiré.
	 * <p>
	 * {@link RegistreUtils#isDateExpire(Date)}.
	 */
	@Test
	public void testIsDateExpiredAvecDateExpire() {

		final boolean isExpired = RegistreUtils.isDateExpire(generateNewDate(1));

		Assert.assertNotNull(isExpired);
		Assert.assertEquals(false, isExpired);
	}

	/**
	 * Test de la méthode isDateExpired avec une date non expiré. {@link fr.hatvp.registre.commons.utils.RegistreUtils#isDateExpire(Date)}.
	 */
	@Test
	public void testIsDateExpiredAvecUneDateNonExpire() {

		final boolean isExpired = RegistreUtils.isDateExpire(generateOldDate(1));

		Assert.assertNotNull(isExpired);
		Assert.assertEquals(true, isExpired);
	}

	/**
	 * Dans ceette méthode on test le fonctionnement correct des conditions dans la méthode, On vérifie que c le dernier appel non conditionné de la méthode qui est effectué.
	 * <p>
	 * On accède à la méthode de modification des données de contacts, et on vérifie que les résultats sont correct.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#updateDeclarant(DeclarantDto)}
	 */
	@Test
	public void testUpdateDeclarant() {
		final DeclarantDto declarantDto = getUser();

		final DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(declarantDto.getId());

		Mockito.doReturn(declarantEntity).when(this.declarantRepository).findOne(declarantDto.getId());
		Mockito.doReturn(declarantEntity).when(this.declarantRepository).save(declarantEntity);
		Mockito.doReturn(declarantDto).when(this.declarantTransformer).modelToDto(declarantEntity);

		final DeclarantDto declarantDto1 = this.declarantService.updateDeclarant(declarantDto);

		Assert.assertNotNull(declarantDto1);
		Assert.assertEquals(declarantDto.getId(), declarantDto1.getId());
	}

	/**
	 * Update email avec une ancienne adresse 
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#updateEmailPrincipalDeclarant(DeclarantDto, DeclarantEntity)}
	 */
	@Test
	public void testUpdateEmailPrincipalAvecUneAncienneAdresse() {
		final DeclarantDto declarantDto = getUser();
		final DeclarantEntity declarant = getDeclarant(declarantDto);

		declarant.setEmail(declarantDto.getEmail());
		declarant.setRecoverCodeExpiryDate(generateOldDate(1));
		
		try {
			final DeclarantDto updatedDeclarant = this.declarantService.updateEmailPrincipalDeclarant(declarantDto, declarant);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.ADRESSE_EMAIL_SAISI_EST_CELLE_QUE_VOUS_UTILISEZ_DEJA.getMessage()).isEqualTo(ex.getMessage());
		}		
	}
	
	/**
	 * Update email avec une adrresse déjà utilisé.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#updateEmailPrincipalDeclarant(DeclarantDto, DeclarantEntity)}
	 */
	@Test
	public void testUpdateEmailPrincipalAvecUneAncienneEnBase() {
		final DeclarantDto declarantDto = getUser();
		final DeclarantEntity declarant = getDeclarant(declarantDto);

		declarant.setEmail("nouvelEmail");
		declarant.setRecoverCodeExpiryDate(generateOldDate(1));

		Mockito.doReturn(declarant).when(this.declarantRepository).findByEmailIgnoreCase(declarantDto.getEmail());
		
		try {
			final DeclarantDto updatedDeclarant = this.declarantService.updateEmailPrincipalDeclarant(declarantDto, declarant);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.ADRESSE_EMAIL_EXISTE_DEJA.getMessage()).isEqualTo(ex.getMessage());
		}		
	}
	
	/**
	 * Update email avec une adrresse en attente de validation
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#updateEmailPrincipalDeclarant(DeclarantDto, DeclarantEntity)}
	 * @throws ParseException 
	 */
	@Test
	public void testUpdateEmailPrincipalAvecAdresseEnAttenteValidation() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dateInString = "15/03/2050";
		Date date = sdf.parse(dateInString);
		
		final DeclarantDto declarantDto = getUser();
		final DeclarantEntity declarant = getDeclarant(declarantDto);

		declarant.setEmail("nouvelEmail");
		declarant.setRecoverCodeExpiryDate(generateOldDate(1));		
		//déclarant avec même email 
		final DeclarantEntity declarantEntityTemp = getDeclarant(declarantDto);
		declarantEntityTemp.setId(2L);
		declarantEntityTemp.setEmailCodeExpiryDate(date);

		Mockito.doReturn(null).when(this.declarantRepository).findByEmailIgnoreCase(declarantDto.getEmail());
		Mockito.doReturn(declarantEntityTemp).when(this.declarantRepository).findByEmailTemp(declarantDto.getEmail());
		
		try {
			final DeclarantDto updatedDeclarant = this.declarantService.updateEmailPrincipalDeclarant(declarantDto, declarant);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.EMAIL_EN_ATTENTE_DE_VALIDATION_PAR_UN_AUTRE_UTILISATEUR.getMessage()).isEqualTo(ex.getMessage());
		}		
	}

	/**
	 * Update email avec une nouvelle adrresse email.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#updateEmailPrincipalDeclarant(DeclarantDto, DeclarantEntity)}
	 */
	@Test
	public void testUpdateEmailPrincipalAvecUneNouvelleAdresse() {
		final DeclarantDto newDeclarantData = getUser();
		final Long connectedDeclarantId = 1L;

		final DeclarantEntity declarant = getDeclarant(newDeclarantData);
		declarant.setEmail(randomString(20));
		declarant.setRecoverCodeExpiryDate(generateOldDate(1));

		Mockito.doReturn(declarant).when(this.declarantRepository).findOne(connectedDeclarantId);
		Mockito.doReturn(declarant).when(this.declarantRepository).save(declarant);
		Mockito.doReturn(newDeclarantData).when(this.declarantTransformer).modelToDto(declarant);

		final DeclarantDto updatedDeclarant = this.declarantService.updateEmailPrincipalDeclarant(newDeclarantData, declarant);

		Assert.assertNotNull(newDeclarantData);
		Assert.assertNotNull(updatedDeclarant);
		Assert.assertNotNull(declarant);
		Assert.assertEquals(newDeclarantData.getId(), declarant.getId());
		Assert.assertNotEquals(newDeclarantData.getEmail(), declarant.getEmail());
	}

	/**
	 * Tentative de mise à jour du mot de passe avec une clé de récupération non valide.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#updateMotDePasseRecuperationDeCompte(DeclarantDto)}
	 */
	@Test(expected = BusinessGlobalException.class)
	public void testUpdateMotDePasseRecuperationDeCompteAvecUneCleExpire() {
		final DeclarantDto newDeclarantData = getUser();
		newDeclarantData.setRecuperationPasswordKeyCode(codeTo64(randomString(50)));
		newDeclarantData.setPassword(randomString(63));

		Mockito.doReturn(null).when(this.declarantRepository).findByRecoverConfirmationCode(decode64ToString(newDeclarantData.getRecuperationPasswordKeyCode()));

		this.declarantService.updateMotDePasseRecuperationDeCompte(newDeclarantData);
	}

	/**
	 * Tentative de mise à jour du mot de passe avec une clé de récupération valide.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#updateMotDePasseRecuperationDeCompte(DeclarantDto)}
	 */
	@Test
	public void testUpdateMotDePasseRecuperationDeCompteAvecUneCleNonExpire() {
		final DeclarantDto newDeclarantData = getUser();
		newDeclarantData.setRecuperationPasswordKeyCode(codeTo64(randomString(50)));
		newDeclarantData.setPassword(randomString(63));

		final DeclarantEntity declarant = getDeclarant(newDeclarantData);
		declarant.setRecoverCodeExpiryDate(generateNewDate(1));

		Mockito.doReturn(declarant).when(this.declarantRepository).findByRecoverConfirmationCode(decode64ToString(newDeclarantData.getRecuperationPasswordKeyCode()));
		Mockito.doReturn(randomString(25)).when(this.passwordEncoder).encode(newDeclarantData.getPassword());
		Mockito.doReturn(declarant).when(this.declarantRepository).save(declarant);
		Mockito.doReturn(newDeclarantData).when(this.declarantTransformer).modelToDto(declarant);

		final DeclarantDto updatedDeclarant = this.declarantService.updateMotDePasseRecuperationDeCompte(newDeclarantData);

		Assert.assertNotNull(newDeclarantData);
		Assert.assertNotNull(declarant);
		Assert.assertNotNull(updatedDeclarant);
		Assert.assertNull(declarant.getRecoverConfirmationCode());
		Assert.assertEquals(updatedDeclarant.getPassword(), newDeclarantData.getPassword());
	}

	/**
	 * Tentative de mise à jour de mot de passe avec ancien mot de passe incorrect.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#updateMotDePasseCompteDeclarant(DeclarantEntity, DeclarantDto)}
	 */
	@Test(expected = BusinessGlobalException.class)
	public void testUpdateMotDePasseCompteDeclarantAvecAncienPassNonValide() {
		final DeclarantDto newDeclarantData = getUser();
		newDeclarantData.setPreviousPassword(randomString(50));
		newDeclarantData.setPassword(randomString(63));

		final DeclarantEntity declarant = getDeclarant(newDeclarantData);

		Mockito.doReturn(randomString(25)).when(this.passwordEncoder).encode(newDeclarantData.getPassword());
		this.declarantService.updateMotDePasseCompteDeclarant(declarant, newDeclarantData);

	}

	/**
	 * Tentative de mise à jour de mot de passe avec ancien mot de passe correct, et nv mot de passe identique à l'ancien.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#updateMotDePasseCompteDeclarant(DeclarantEntity, DeclarantDto)}
	 */
	@Test
	public void testUpdateMotDePasseCompteDeclarantAvecAncienPassValideEtNvPassIdemAuNv() {
		final DeclarantDto declarantDto = getUser();
		declarantDto.setPassword("password");
		declarantDto.setPreviousPassword("password");

		final DeclarantEntity declarantEntity = getDeclarant(declarantDto);
		declarantEntity.setPassword("password");
		
		Mockito.doReturn(true).when(this.passwordEncoder).matches(declarantDto.getPreviousPassword(), declarantEntity.getPassword());		

		try {
			final DeclarantDto retourDeclarant = this.declarantService.updateMotDePasseCompteDeclarant(declarantEntity, declarantDto);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.MOT_DE_PASSE_INVALIDE.getMessage()).isEqualTo(ex.getMessage());
		}

	}

	/**
	 * Tentative de mise à jour de mot de passe avec ancien mot de passe correct, et nv mot de passe différent de l'ancien.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#updateMotDePasseCompteDeclarant(DeclarantEntity, DeclarantDto)}
	 */
	@Test(expected = BusinessGlobalException.class)
	public void testUpdateMotDePasseCompteDeclarantAvecAncienPassValideEtNvPassDiffDuNv() {
		final DeclarantDto newDeclarantData = getUser();
		newDeclarantData.setPreviousPassword(randomString(50));
		newDeclarantData.setPassword(randomString(25));

		final DeclarantEntity declarant = getDeclarant(newDeclarantData);
		declarant.setPassword(randomString(25));

		Mockito.doReturn(randomString(25)).when(this.passwordEncoder).encode(newDeclarantData.getPassword());
		Mockito.doReturn(declarant).when(this.declarantRepository).save(declarant);
		Mockito.doReturn(newDeclarantData).when(this.declarantTransformer).modelToDto(declarant);

		final DeclarantDto updatedDeclarantDto = this.declarantService.updateMotDePasseCompteDeclarant(declarant, newDeclarantData);

		Assert.assertNotNull(newDeclarantData);
		Assert.assertNotNull(declarant);
		Assert.assertNotNull(updatedDeclarantDto);
		Assert.assertNotEquals(newDeclarantData.getPassword(), declarant.getPassword());
		Assert.assertEquals(updatedDeclarantDto.getPassword(), declarant.getPassword());
	}

	/**
	 * Tentative de validation de compte, avec un token non expiré, et un bon mot de passe.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#activationDeCompte(String, String)}
	 */
	@Test
	public void testActivationDeCompte() {

		final DeclarantDto declarantToActivate = getUser();
		declarantToActivate.setActivationEmailKeyCode(randomString(5));

		final DeclarantEntity declarantToActivateModel = getDeclarant(declarantToActivate);
		// Token non expiré.
		declarantToActivateModel.setEmailCodeExpiryDate(generateNewDate(1));

		Mockito.doReturn(declarantToActivateModel).when(this.declarantRepository).findByEmailConfirmationCode(codeTo64(declarantToActivate.getActivationEmailKeyCode()));

		Mockito.doReturn(declarantToActivateModel).when(this.declarantRepository).findByEmailConfirmationCode(declarantToActivate.getActivationEmailKeyCode());

		Mockito.doReturn(true).when(this.passwordEncoder).matches(declarantToActivate.getPassword(), declarantToActivateModel.getPassword());

		Mockito.doReturn(declarantToActivateModel).when(this.declarantRepository).save(declarantToActivateModel);
		Mockito.doReturn(declarantToActivate).when(this.declarantTransformer).modelToDto(declarantToActivateModel);

		final DeclarantDto declarantDtoSaved = this.declarantService.activationDeCompte(codeTo64(declarantToActivate.getActivationEmailKeyCode()),
				declarantToActivate.getPassword());

		Assert.assertNotNull(declarantToActivate);
		Assert.assertNotNull(declarantToActivateModel);
		Assert.assertNotNull(declarantDtoSaved);
		Assert.assertEquals(true, declarantToActivateModel.isActivated());
	}

	/**
	 * Tentative de validation de compte, avec un token expiré.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#activationDeCompte(String, String)}
	 */
	@Test
	public void testActivationDeCompteAvecTokenExpire() {
		final DeclarantDto declarantToUpdate = getUser();
		declarantToUpdate.setActivationEmailKeyCode(randomString(5));

		final DeclarantEntity declarantToActivateModel = getDeclarant(declarantToUpdate);
		// Token non expiré.
		declarantToActivateModel.setEmailCodeExpiryDate(generateNewDate(1));

		Mockito.doReturn(declarantToActivateModel).when(this.declarantRepository).findByEmailConfirmationCode(codeTo64(declarantToUpdate.getActivationEmailKeyCode()));

		Mockito.doReturn(declarantToActivateModel).when(this.declarantRepository).findByEmailConfirmationCode(declarantToUpdate.getActivationEmailKeyCode());

		Mockito.doReturn(true).when(this.passwordEncoder).matches(declarantToUpdate.getPassword(), declarantToActivateModel.getPassword());

		Mockito.doReturn(declarantToActivateModel).when(this.declarantRepository).save(declarantToActivateModel);
		Mockito.doReturn(declarantToUpdate).when(this.declarantTransformer).modelToDto(declarantToActivateModel);

		final DeclarantDto declarantDtoSaved = this.declarantService.activationDeLaNouvelleAdresseEmail(codeTo64(declarantToUpdate.getActivationEmailKeyCode()),
				declarantToUpdate.getPassword());

		Assert.assertNotNull(declarantToUpdate);
		Assert.assertNotNull(declarantToActivateModel);
		Assert.assertNotNull(declarantDtoSaved);
		Assert.assertNull(declarantToActivateModel.getEmailTemp());
		Assert.assertEquals(declarantDtoSaved.getEmail(), declarantToUpdate.getEmail());
	}

	/**
	 * Activation de la nouvelle adresse email.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#activationDeLaNouvelleAdresseEmail(String, String)}
	 */
	@Test(expected = BusinessGlobalException.class)
	public void testActivationDeLaNouvelleAdresseEmail() {
		final DeclarantDto declarantToActivate = getUser();
		declarantToActivate.setActivationEmailKeyCode(randomString(5));

		final DeclarantEntity declarantToActivateModel = getDeclarant(declarantToActivate);
		// Token non expiré.
		declarantToActivateModel.setEmailCodeExpiryDate(generateOldDate(1));

		Mockito.doReturn(true).when(this.passwordEncoder).matches(declarantToActivate.getPassword(), declarantToActivateModel.getPassword());
		Mockito.doReturn(declarantToActivateModel).when(this.declarantRepository).findByEmailConfirmationCode(declarantToActivate.getActivationEmailKeyCode());

		// Appel du service.
		this.declarantService.activationDeCompte(codeTo64(declarantToActivate.getActivationEmailKeyCode()), declarantToActivate.getPassword());

	}

	/**
	 * Test avec une adresse email valide et un compte validé.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#recuperationDeCompte(String)}
	 */
	@Test
	public void testEnvoiDeMailDeRecuperationDeCompteAvecCompteValide() {
		// init un DTO declarant.
		final DeclarantDto declarantDto = getUser();
		declarantDto.setEmail(this.DECLARANT_EMAIL_TEST);
		// init un model déclarant.
		final DeclarantEntity declarantEntity = getDeclarant(declarantDto);
		declarantEntity.setEmail(this.DECLARANT_EMAIL_TEST);
		declarantEntity.setActivated(true);

		// Mock de la récupération du déclarant de la base de données.
		Mockito.doReturn(declarantEntity).when(this.declarantRepository).findByEmailIgnoreCase(this.DECLARANT_EMAIL_TEST);

		// Mock de l'enregistrement des données.
		Mockito.doReturn(declarantEntity).when(this.declarantRepository).save(declarantEntity);

		// Mock du transformeur.
		Mockito.doReturn(declarantDto).when(this.declarantTransformer).modelToDto(declarantEntity);

		// appel du service.
		final DeclarantDto savedDeclarant = this.declarantService.recuperationDeCompte(this.DECLARANT_EMAIL_TEST);

		Assert.assertNotNull(savedDeclarant);
		Assert.assertNotNull(declarantEntity);
		Assert.assertNotNull(declarantDto);
		Assert.assertNotNull(declarantEntity.getRecoverConfirmationCode());
		Assert.assertNotNull(declarantEntity.getRecoverCodeExpiryDate());
	}

	/**
	 * Test avec une adresse email valide et un compte non validé.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#recuperationDeCompte(String)}
	 */
	@Test
	public void testEnvoiDeMailDeRecuperationDeCompteAvecCompteNonValide() {
		final DeclarantDto declarantDto = getUser();
		declarantDto.setEmail(this.DECLARANT_EMAIL_TEST);

		// Mock de la récupération du déclarant de la base de données.
		Mockito.doReturn(null).when(this.declarantRepository).findByEmailIgnoreCase(this.DECLARANT_EMAIL_TEST);

		final DeclarantDto savedDeclarant = this.declarantService.recuperationDeCompte(this.DECLARANT_EMAIL_TEST);
		Assert.assertNull(savedDeclarant);
	}

	/**
	 * @return Renvoi un déclarant DTO.
	 */
	private DeclarantDto getUser() {
		final DeclarantDto declarantToSave = new DeclarantDto();
		declarantToSave.setId(1L);
		declarantToSave.setPrenom(randomString(10));
		declarantToSave.setNom(randomString(10));
		declarantToSave.setEmail(randomString(20));
		declarantToSave.setTelephone(randomString(10));
		declarantToSave.setPassword(randomString(30));
		declarantToSave.setBirthDate(new Date());
		declarantToSave.setCivility(CiviliteEnum.MME);
		declarantToSave.setActivated(false);

		final List<DeclarantContactDto> emails = new ArrayList<>();
		emails.add(getContact(randomString(15), null, CategoryTelephoneMail.PERSO));
		final List<DeclarantContactDto> phones = new ArrayList<>();
		phones.add(getContact(null, randomString(10), CategoryTelephoneMail.PERSO));

		declarantToSave.setEmailComplement(emails);
		declarantToSave.setPhone(phones);

		return declarantToSave;
	}

	/**
	 * @param declarantDto
	 *            dto à transformer.
	 * @return Renvoi une entitée déclarant avec les informations du DTO.
	 */
	private DeclarantEntity getDeclarant(final DeclarantDto declarantDto) {

		final DeclarantEntity declarant = new DeclarantEntity();
		declarant.setId(declarantDto.getId());
		declarant.setEmailConfirmationCode(declarantDto.getActivationEmailKeyCode());
		declarant.setEmail(declarantDto.getEmail());
		declarant.setTelephone(declarantDto.getTelephone());
		declarant.setPassword(declarantDto.getPassword());
		declarant.setCivility(declarantDto.getCivility());
		declarant.setNom(declarantDto.getPrenom());
		declarant.setPrenom(declarantDto.getNom());
		declarant.setBirthDate(declarantDto.getBirthDate());
		declarant.setActivated(declarantDto.isActivated());

		return declarant;
	}

	/**
	 * Créer une nstance de {@link DeclarantContactDto}
	 */
	private DeclarantContactDto getContact(final String telephone, final String email, final CategoryTelephoneMail type) {
		final DeclarantContactDto declarantContactDto = new DeclarantContactDto();
		declarantContactDto.setTelephone(telephone);
		declarantContactDto.setEmail(email);
		declarantContactDto.setCategorie(type);

		return declarantContactDto;
	}
	
	/**
	 * Test de la méthode loadDeclarantById {@link DeclarantServiceImpl#loadDeclarantById(Long)}.
	 */
	@Test
	public void testLoadDeclarantByIdException() {
		final Long declarantId = 1L;

		Mockito.doReturn(null).when(this.declarantRepository).findOne(declarantId);
		
		try {
			final DeclarantDto declarantDtoSaved = this.declarantService.loadDeclarantById(declarantId);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.UTILISATEUR_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	/**
	 * Test de la méthode {@link DeclarantServiceImpl#updateDeclarant(DeclarantDto)}
	 */
	@Test
	public void testUpdateDeclarantException() {
		final DeclarantDto declarantDto = getUser();

		Mockito.doReturn(null).when(this.declarantRepository).findOne(declarantDto.getId());
		
		try {
			final DeclarantDto declarantDto1 = this.declarantService.updateDeclarant(declarantDto);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.DECLARANT_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	
	/**
	 * Test de la méthode verifierSiLeCompteExiste avec un email qui n'existe pas {@link DeclarantServiceImpl#saveDeclarant(DeclarantDto)} .
	 * @throws ParseException 
	 */
	@Test
	public void testSaveDeclarantAvecUneAdressEmailEnAttenteValidation() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dateInString = "15/03/2050";
		Date date = sdf.parse(dateInString);

		DeclarantEntity oldDeclarant = new DeclarantEntity(10L);
		oldDeclarant.setEmail("email@old.fr");
		oldDeclarant.setEmailTemp("emailTemp@old.fr");
		oldDeclarant.setEmailCodeExpiryDate(date);

		DeclarantDto  declarantDto = new DeclarantDto();		
		declarantDto.setId(1L);
		declarantDto.setEmail("emailTemp@old.fr");

		Mockito.doReturn(oldDeclarant).when(this.declarantRepository).findByEmailIgnoreCaseOrEmailTempIgnoreCase(declarantDto.getEmail(), declarantDto.getEmail());

		try {
			final boolean retour = this.declarantService.verifierSiLeCompteExiste(declarantDto);
		} catch (EntityExistsException ex) {
			assertThat(ErrorMessageEnum.ADRESSE_EMAIL_En_ATTENTE_DE_VALIDATION.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	
	/**
	 * Activation de la nouvelle adresse email.
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#activationDeLaNouvelleAdresseEmail(String, String)}
	 */
	@Test
	public void testActivationDeCompteCompteActive() {
		final DeclarantDto declarantToActivate = getUser();
		declarantToActivate.setActivationEmailKeyCode(randomString(5));
		declarantToActivate.setActivated(true);

		final DeclarantEntity declarantToActivateModel = getDeclarant(declarantToActivate);
		// Token non expiré.
		declarantToActivateModel.setEmailCodeExpiryDate(generateNewDate(1));

		Mockito.doReturn(true).when(this.passwordEncoder).matches(declarantToActivate.getPassword(), declarantToActivateModel.getPassword());
		Mockito.doReturn(declarantToActivateModel).when(this.declarantRepository).findByEmailConfirmationCode(declarantToActivate.getActivationEmailKeyCode());

		// Appel du service.
		
		try {
			this.declarantService.activationDeCompte(codeTo64(declarantToActivate.getActivationEmailKeyCode()), declarantToActivate.getPassword());
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.COMPTE_DEJA_ACTIVE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	
	/**
	 * Tentative de validation de compte, avec un code email expiré
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#activationDeCompte(String, String)}
	 */
	@Test
	public void testActivationDeCompteAvecCodeEmailExpire() {
		final DeclarantDto declarantToUpdate = getUser();
		declarantToUpdate.setActivationEmailKeyCode(randomString(5));

		final DeclarantEntity declarantToActivateModel = getDeclarant(declarantToUpdate);
		// Token non expiré.
		declarantToActivateModel.setEmailCodeExpiryDate(generateNewDate(1));
		declarantToActivateModel.setEmailTemp("");
		
		DeclarantEntity declarantEntity = new DeclarantEntity(10L);
		declarantEntity.setActivated(true);

		Mockito.doReturn(declarantToActivateModel).when(this.declarantRepository).findByEmailConfirmationCode(codeTo64(declarantToUpdate.getActivationEmailKeyCode()));
		Mockito.doReturn(declarantEntity).when(this.declarantRepository).findByEmailIgnoreCase(declarantToActivateModel.getEmailTemp());		
		Mockito.doReturn(declarantToActivateModel).when(this.declarantRepository).findByEmailConfirmationCode(declarantToUpdate.getActivationEmailKeyCode());
		Mockito.doReturn(true).when(this.passwordEncoder).matches(declarantToUpdate.getPassword(), declarantToActivateModel.getPassword());

		try {
			final DeclarantDto declarantDtoSaved = this.declarantService.activationDeLaNouvelleAdresseEmail(codeTo64(declarantToUpdate.getActivationEmailKeyCode()),
					declarantToUpdate.getPassword());
		} catch (EntityExistsException ex) {
			assertThat(ErrorMessageEnum.ADRESSE_EMAIL_EST_DEJA_ACTIVE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	
	/**
	 * Tentative de mise à jour du mot de passe avec RecoverCodeExpiryDate expiré
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#updateMotDePasseRecuperationDeCompte(DeclarantDto)}
	 */
	@Test
	public void testUpdateMotDePasseRecuperationDeCompteCodeExpire() {
		final DeclarantDto newDeclarantData = getUser();
		newDeclarantData.setRecuperationPasswordKeyCode(codeTo64(randomString(50)));
		newDeclarantData.setPassword(randomString(63));

		final DeclarantEntity declarant = getDeclarant(newDeclarantData);
		declarant.setRecoverCodeExpiryDate(generateOldDate(1));

		Mockito.doReturn(declarant).when(this.declarantRepository).findByRecoverConfirmationCode(decode64ToString(newDeclarantData.getRecuperationPasswordKeyCode()));
		Mockito.doReturn(randomString(25)).when(this.passwordEncoder).encode(newDeclarantData.getPassword());
		Mockito.doReturn(declarant).when(this.declarantRepository).save(declarant);
		Mockito.doReturn(newDeclarantData).when(this.declarantTransformer).modelToDto(declarant);		

		try {
			final DeclarantDto updatedDeclarant = this.declarantService.updateMotDePasseRecuperationDeCompte(newDeclarantData);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.LE_TOKEN_DE_RECUPERATION_A_EXPIRE.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Tentative de validation de compte, avec verifierLeTokenEtLeMotDePasse qui retourne null
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#activationDeCompte(String, String)}
	 */
	@Test
	public void testActivationDeCompteVerifierLeTokenEtLeMotDePasseNull() {

		final DeclarantDto declarantToActivate = getUser();
		declarantToActivate.setActivationEmailKeyCode(randomString(5));

		final DeclarantEntity declarantToActivateModel = getDeclarant(declarantToActivate);
		// Token non expiré.
		declarantToActivateModel.setEmailCodeExpiryDate(generateNewDate(1));

		Mockito.doReturn(null).when(this.declarantRepository).findByEmailConfirmationCode(codeTo64(declarantToActivate.getActivationEmailKeyCode()));

		try {
			final DeclarantDto declarantDtoSaved = this.declarantService.activationDeCompte(codeTo64(declarantToActivate.getActivationEmailKeyCode()),
					declarantToActivate.getPassword());
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.COMPTE_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	
	/**
	 * Tentative de validation de compte, avec Le mot de passe saisi ne correspond pas à l'ancien mot de passe
	 * <p>
	 * Test de la méthode {@link DeclarantServiceImpl#activationDeCompte(String, String)}
	 */
	@Test
	public void testActivationDeCompteMauvaisMotDePasse() {
		final DeclarantDto declarantToActivate = getUser();
		declarantToActivate.setActivationEmailKeyCode(randomString(5));

		final DeclarantEntity declarantToActivateModel = getDeclarant(declarantToActivate);

		Mockito.doReturn(declarantToActivateModel).when(this.declarantRepository).findByEmailConfirmationCode( decode64ToString(codeTo64(declarantToActivate.getActivationEmailKeyCode())));
		Mockito.doReturn(false).when(this.passwordEncoder).matches(declarantToActivate.getPassword(), declarantToActivateModel.getPassword());
		
		try {
			final DeclarantDto declarantDtoSaved = this.declarantService.activationDeCompte(codeTo64(declarantToActivate.getActivationEmailKeyCode()),
					declarantToActivate.getPassword());
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.MOT_DE_PASSE_INVALIDE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
}
