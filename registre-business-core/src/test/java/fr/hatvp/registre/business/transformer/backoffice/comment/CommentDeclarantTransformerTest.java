/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.backoffice.comment;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.backoffice.comment.impl.CommentDeclarantBoTransformerImpl;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.persistence.entity.commentaire.CommentDeclarantEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.mockito.Mockito.when;

/**
 * Test de la classe {@link CommentDeclarantBoTransformer}.
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class CommentDeclarantTransformerTest
    extends AbstractCommentTest<CommentDeclarantEntity>
{

    /**
     * Préciser la classe de commentaire.
     */
    public CommentDeclarantTransformerTest()
    {
        this.entityClass = CommentDeclarantEntity.class;
    }

    /**
     * Repository des declarants.
     */
    @Mock
    private DeclarantRepository declarantRepository;

    /**
     * Transformer à tester.
     */
    @InjectMocks
    private CommentDeclarantBoTransformerImpl commentDeclarantBoTransformer;

    /**
     * {@inheritDoc}
     */
    @Override
    @Test
    public void testModelToDto() throws InstantiationException, IllegalAccessException
    {
        super.testModelToDto();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    CommentBoDto getModelToDtoTransformerResult(final CommentDeclarantEntity commentDeclarantEntity)
    {
        commentDeclarantEntity.setDeclarantEntity(this.getDeclarant());
        return this.commentDeclarantBoTransformer.commentModelToDto(commentDeclarantEntity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void additionalModelToDtoDeclarantCommentAssertions(final CommentDeclarantEntity entity,
            final CommentBoDto dto)
    {
        super.additionalModelToDtoDeclarantCommentAssertions(entity, dto);
        Assert.assertEquals(entity.getDeclarantEntity().getId(), dto.getContextId());
    }

    /**
     * Tester la méthode dtotoModel du transformeur {@link CommentDeclarantBoTransformer}
     */
    @Override
    @Test
    public void testDtoToModel()
    {
        super.testDtoToModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    CommentDeclarantEntity getDtoToModelTransformerResult(final CommentBoDto dto)
    {
        when(this.declarantRepository.findOne(ID)).thenReturn(this.getDeclarant());
        return this.commentDeclarantBoTransformer.dtoToModel(dto);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void additionalDtoToModelDeclarantCommentAssertions(final CommentBoDto dto,
            final CommentDeclarantEntity entity)
    {
        super.additionalDtoToModelDeclarantCommentAssertions(dto, entity);
        Assert.assertEquals(dto.getContextId(), entity.getDeclarantEntity().getId());
    }

    /**
     * @return une instance de {@link DeclarantEntity}
     */
    private DeclarantEntity getDeclarant()
    {
        final DeclarantEntity declarantEntity = new DeclarantEntity();
        declarantEntity.setId(ID);
        declarantEntity.setVersion(VERSION);
        declarantEntity.setEmail(EMAIL_TEST);
        declarantEntity.setPrenom("test_p");
        declarantEntity.setNom("test_n");
        return declarantEntity;
    }
}
