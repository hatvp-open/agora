/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.commons.dto.backoffice.SurveillanceDetailDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.SurveillanceRepository;

/**
 * Test du service {@link fr.hatvp.registre.business.SurveillanceService}
 *
 * @version $Revision$ $Date${0xD}
 */
//Permet la compatibilité des tests junit4 et junit5
@RunWith(SpringJUnit4ClassRunner.class)
//Charge un ApplicationContext permettant l'utilisation des composants qui sont annotés avec @Component, @Service, @Repository
@ContextConfiguration(classes = { TestBusinessConfig.class }, loader = AnnotationConfigContextLoader.class)
public class SurveillanceServiceTest {
	
	@InjectMocks
	SurveillanceServiceImpl surveillanceServiceImpl;
	
	@Mock
	SurveillanceRepository surveillanceRepository;
	
	@Mock
	OrganisationRepository organisationRepository;

	
	/**
	 * Initialisation des tests.
	 *
	 * @throws Exception
	 */
	@Before
	public void before() throws Exception {
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test de la méthode avec une organisation existante qui n'a pas de surveillance
	 * <p>
	 * Test de la méthode: {@link fr.hatvp.registre.business.SurveillanceService#verifyListOrganisation(List<String> nationalIds)} 
	 */
	@Test
	public void testVerifyListOrganisationExistingOrganisationEntity() {
		String nationalId = "817769499";
		List<String> nationalIds = new ArrayList<String>();
		nationalIds.add(nationalId);
		
		List<OrganisationEntity> listOrgaEntity = new ArrayList<OrganisationEntity>();
		OrganisationEntity organisationEntity = new OrganisationEntity(1L);
		organisationEntity.setDenomination("TEST");
		organisationEntity.setSiren(nationalId);
		organisationEntity.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
		
		listOrgaEntity.add(organisationEntity);
		
		Mockito.doReturn(listOrgaEntity).when(this.organisationRepository).findBySirenOrRnaOrHatvpNumber(nationalId, nationalId, nationalId);
		Mockito.doReturn(organisationEntity).when(this.organisationRepository).findOne(organisationEntity.getId());
		Mockito.doReturn(null).when(this.surveillanceRepository).findByOrganisationEntity(organisationEntity);
		
		List<SurveillanceDetailDto> listRetour = this.surveillanceServiceImpl.verifyListOrganisation(nationalIds);
		
		Assert.assertEquals(1, listRetour.size());
		Assert.assertNull(listRetour.get(0).getId());
		Assert.assertEquals(organisationEntity.getDenomination(), listRetour.get(0).getDenomination());		
	}	
	
	/**
	 * 
	 * <p>
	 * Test de la méthode: {@link fr.hatvp.registre.business.SurveillanceService#verifyListOrganisation(List<String> nationalIds)} 
	 */
	@Test
	public void testVerifyListOrganisationWrongSirenFormat() {
		String nationalId = "8491547299";
		List<String> nationalIds = new ArrayList<String>();
		nationalIds.add(nationalId);
		
		List<OrganisationEntity> listOrgaEntity = new ArrayList<OrganisationEntity>();

		Mockito.doReturn(listOrgaEntity).when(this.organisationRepository).findBySirenOrRnaOrHatvpNumber(nationalId, nationalId, nationalId);

		try {
			this.surveillanceServiceImpl.verifyListOrganisation(nationalIds);
		} catch (BusinessGlobalException ex) {
			assertThat("Identifiant SIREN non valide").isEqualTo(ex.getMessage());
		}		
	}
	
	/**
	 * 
	 * <p>
	 * Test de la méthode: {@link fr.hatvp.registre.business.SurveillanceService#verifyListOrganisation(List<String> nationalIds)} 
	 */
	@Test
	public void testVerifyListOrganisationWrongRNAFormat() {
		String nationalId = "H849154729";
		List<String> nationalIds = new ArrayList<String>();
		nationalIds.add(nationalId);
		
		List<OrganisationEntity> listOrgaEntity = new ArrayList<OrganisationEntity>();

		Mockito.doReturn(listOrgaEntity).when(this.organisationRepository).findBySirenOrRnaOrHatvpNumber(nationalId, nationalId, nationalId);

		try {
			this.surveillanceServiceImpl.verifyListOrganisation(nationalIds);
		} catch (BusinessGlobalException ex) {
			assertThat("Identifiant HATVP non valide").isEqualTo(ex.getMessage());
		}		
	}
	
	/**
	 * 
	 * <p>
	 * Test de la méthode: {@link fr.hatvp.registre.business.SurveillanceService#verifyListOrganisation(List<String> nationalIds)} 
	 */
	@Test
	public void testVerifyListOrganisationWrongHatvpFormat() {
		String nationalId = "W8491547299";
		List<String> nationalIds = new ArrayList<String>();
		nationalIds.add(nationalId);
		
		List<OrganisationEntity> listOrgaEntity = new ArrayList<OrganisationEntity>();

		Mockito.doReturn(listOrgaEntity).when(this.organisationRepository).findBySirenOrRnaOrHatvpNumber(nationalId, nationalId, nationalId);

		try {
			this.surveillanceServiceImpl.verifyListOrganisation(nationalIds);
		} catch (BusinessGlobalException ex) {
			assertEquals("Identifiant RNA non valide", ex.getMessage());
		}		
	}
	
}
