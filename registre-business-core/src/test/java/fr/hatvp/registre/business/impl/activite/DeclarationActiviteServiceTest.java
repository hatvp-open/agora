/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.activite;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.transformer.impl.activite.ActionsRepresentationInteretTransformerImpl;
import fr.hatvp.registre.business.transformer.impl.activite.ActiviteRepresentationInteretTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.activite.ActionsRepresentationInteretTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ActiviteRepresentationInteretTransformer;
import fr.hatvp.registre.business.utils.HumanReadableUniqueIdUtil;
import fr.hatvp.registre.commons.dto.activite.ActionRepresentationInteretDto;
import fr.hatvp.registre.commons.dto.activite.ActiviteRepresentationInteretDto;
import fr.hatvp.registre.commons.dto.activite.TiersDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.repository.activite.ActiviteRepresentationInteretRepository;
import fr.hatvp.registre.persistence.repository.activite.ExerciceComptableRepository;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.DesinscriptionRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;

public class DeclarationActiviteServiceTest {

	@Mock
	private ActiviteRepresentationInteretRepository activiteRepresentationInteretRepository;

	@Mock
	private ExerciceComptableRepository exerciceComptableRepository;

	@Mock
	private DeclarantRepository declarantRepository;
	
	@Mock
	private EspaceOrganisationRepository espaceOrganisationRepository;
	
	@Mock
	private DesinscriptionRepository desinscriptionRepository;

	@Spy
	@InjectMocks
	private ActiviteRepresentationInteretTransformer activiteRepresentationInteretTransformer = new ActiviteRepresentationInteretTransformerImpl();

	@Spy
	private ActionsRepresentationInteretTransformer actionsRepresentationInteretTransformer = new ActionsRepresentationInteretTransformerImpl();

	@Mock
	private EspaceOrganisationService espaceService;

	@InjectMocks
	private DeclarationActiviteServiceImpl declarationActiviteService;
	


	private HumanReadableUniqueIdUtil idUtil = new HumanReadableUniqueIdUtil("HATVP fiche", 8, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetDeclarationActivite() {

		Long ficheId = new Random().nextLong();

		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity(ficheId);
		ActionRepresentationInteretEntity actionRepresentationInteretEntity = new ActionRepresentationInteretEntity(new Random().nextLong());
		activiteRepresentationInteretEntity.getActionsRepresentationInteret().add(actionRepresentationInteretEntity);
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity(new Random().nextLong());
		activiteRepresentationInteretEntity.setExerciceComptable(exerciceComptableEntity);

		when(activiteRepresentationInteretRepository.findOne(ficheId)).thenReturn(activiteRepresentationInteretEntity);

		ActiviteRepresentationInteretDto activiteRepresentationInteretDto = this.declarationActiviteService.getDeclarationActivite(ficheId);

		verify(activiteRepresentationInteretRepository, times(1)).findOne(ficheId);

		assertThat(activiteRepresentationInteretDto.getId()).isEqualTo(activiteRepresentationInteretEntity.getId());
		assertThat(activiteRepresentationInteretDto.getActionsRepresentationInteret().size()).isEqualTo(activiteRepresentationInteretEntity.getActionsRepresentationInteret().size());
		assertThat(activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().findFirst().get().getId())
				.isEqualTo(activiteRepresentationInteretEntity.getActionsRepresentationInteret().stream().findFirst().get().getId());
		assertThat(activiteRepresentationInteretDto.getExerciceComptable()).isEqualTo(activiteRepresentationInteretEntity.getExerciceComptable().getId());

	}

	@Test
	public void testDeleteDeclarationActiviteNonPubliee() {

		Long ficheId = new Random().nextLong();
		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity(ficheId);
		activiteRepresentationInteretEntity.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		when(activiteRepresentationInteretRepository.findOne(ficheId)).thenReturn(activiteRepresentationInteretEntity);
		this.declarationActiviteService.deleteDeclarationActivite(ficheId);

		verify(activiteRepresentationInteretRepository, times(1)).delete(activiteRepresentationInteretEntity);
	}

	@Test
	public void testDeleteDeclarationActivitePubliee() {

		Long ficheId = new Random().nextLong();
		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity(ficheId);
		activiteRepresentationInteretEntity.setStatut(StatutPublicationEnum.PUBLIEE);
		when(activiteRepresentationInteretRepository.findOne(ficheId)).thenReturn(activiteRepresentationInteretEntity);

		try {
			this.declarationActiviteService.deleteDeclarationActivite(ficheId);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.SUPPRESSION_ACTIVITE_IMPOSSIBLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	@Test
	public void testCreateDeclarationActiviteWithBadSpaceId() {
		Long otherEspaceOrganisationId = 2L;
		Long currentEspaceOrganisationId = 1L;
		Long exerciceComptableId = 3L;

		ActiviteRepresentationInteretDto activiteRepresentationInteretDto = new ActiviteRepresentationInteretDto();
		activiteRepresentationInteretDto.setDomainesIntervention(new HashSet<>(Arrays.asList(3L, 4L, 5L)));
		activiteRepresentationInteretDto.setExerciceComptable(exerciceComptableId);

		ExerciceComptableEntity exerciceComptable = new ExerciceComptableEntity(exerciceComptableId);
		exerciceComptable.setEspaceOrganisation(new EspaceOrganisationEntity(otherEspaceOrganisationId));

		when(exerciceComptableRepository.getOne(exerciceComptableId)).thenReturn(exerciceComptable);

		try {
			this.declarationActiviteService.createDeclarationActivite(activiteRepresentationInteretDto, 1L, currentEspaceOrganisationId);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.MAUVAIS_ESPACE.getMessage()).isEqualTo(ex.getMessage());
		}

	}

	@Test
	public void testCreateDeclarationActivite() {

		ArgumentCaptor<ActiviteRepresentationInteretEntity> argumentCaptor = ArgumentCaptor.forClass(ActiviteRepresentationInteretEntity.class);

		Long currentUserId = new Random().nextLong();
		Long currentEspaceOrganisationId = new Random().nextLong();
		Long exerciceComptableId = new Random().nextLong();

		Long databaseGeneratedId = ThreadLocalRandom.current().nextLong(0L, Long.MAX_VALUE);

		ActiviteRepresentationInteretDto activiteRepresentationInteretDto = new ActiviteRepresentationInteretDto();
		activiteRepresentationInteretDto.setDomainesIntervention(new HashSet<>(Arrays.asList(3L, 4L, 5L)));
		activiteRepresentationInteretDto.setExerciceComptable(exerciceComptableId);
		ActionRepresentationInteretDto actionRepresentationInteretDto = new ActionRepresentationInteretDto();
		actionRepresentationInteretDto.setReponsablesPublics(new HashSet<>(Arrays.asList(7L, 8L)));
		actionRepresentationInteretDto.setDecisionsConcernees(new HashSet<>(Arrays.asList(5L, 6L)));
		actionRepresentationInteretDto.setActionsMenees(new HashSet<>(Arrays.asList(3L, 4L)));
		actionRepresentationInteretDto.setTiers(Arrays.asList(new TiersDto(1L, "bla"), new TiersDto(2L, "blu")));
		activiteRepresentationInteretDto.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteretDto));

		ExerciceComptableEntity exerciceComptable = new ExerciceComptableEntity(exerciceComptableId);
		EspaceOrganisationEntity espace = new EspaceOrganisationEntity(currentEspaceOrganisationId);
		espace.setOrganisation(new OrganisationEntity(new Random().nextLong()));
		exerciceComptable.setEspaceOrganisation(espace);

		when(exerciceComptableRepository.getOne(exerciceComptableId)).thenReturn(exerciceComptable);

		when(declarantRepository.getOne(currentUserId)).thenReturn(new DeclarantEntity(currentUserId));
		
		
		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findByOrganisationId(anyLong());    	
//		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findByOrganisationId(actionRepresentationInteretDto.getTiers().get(1).getId());
		
	
		when(activiteRepresentationInteretRepository.save(Mockito.any(ActiviteRepresentationInteretEntity.class))).thenAnswer(repo -> {
			ActiviteRepresentationInteretEntity x = (ActiviteRepresentationInteretEntity) repo.getArguments()[0];
			x.setId(databaseGeneratedId);
			return repo.getArguments()[0];
		});

		ActiviteRepresentationInteretDto dto = declarationActiviteService.createDeclarationActivite(activiteRepresentationInteretDto, currentUserId, currentEspaceOrganisationId);

		verify(declarantRepository, times(1)).getOne(currentUserId);
		verify(exerciceComptableRepository, times(1)).getOne(exerciceComptableId);
		verify(activiteRepresentationInteretRepository, times(1)).save(argumentCaptor.capture());

		assertThat(databaseGeneratedId).isEqualTo(idUtil.decode(dto.getIdFiche()));

		assertThat(dto.getExerciceComptable()).isEqualTo(activiteRepresentationInteretDto.getExerciceComptable());
		assertThat(dto.getDomainesIntervention()).containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretDto.getDomainesIntervention());
		assertThat(dto.getActionsRepresentationInteret().size()).isEqualTo(activiteRepresentationInteretDto.getActionsRepresentationInteret().size());
		dto.getActionsRepresentationInteret().forEach(ar -> {
			assertThat(ar.getReponsablesPublics())
					.containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().findFirst().get().getReponsablesPublics());
			assertThat(ar.getDecisionsConcernees())
					.containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().findFirst().get().getDecisionsConcernees());
			assertThat(ar.getActionsMenees())
					.containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().findFirst().get().getActionsMenees());
			assertThat(ar.getTiers()).containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().findFirst().get().getTiers());
		});

		ActiviteRepresentationInteretEntity entity = argumentCaptor.getValue();

		assertThat(entity.getExerciceComptable().getId()).isEqualTo(activiteRepresentationInteretDto.getExerciceComptable());
		assertThat(entity.getDomainesIntervention().stream().map(di -> {
			return di.getId();
		}).collect(Collectors.toList())).containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretDto.getDomainesIntervention());
		assertThat(entity.getActionsRepresentationInteret().size()).isEqualTo(activiteRepresentationInteretDto.getActionsRepresentationInteret().size());
		entity.getActionsRepresentationInteret().forEach(ar -> {
			assertThat(ar.getReponsablesPublics().stream().map(rs -> {
				return rs.getId();
			}).collect(Collectors.toList()))
					.containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().findFirst().get().getReponsablesPublics());
			assertThat(ar.getDecisionsConcernees().stream().map(dc -> {
				return dc.getId();
			}).collect(Collectors.toList()))
					.containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().findFirst().get().getDecisionsConcernees());
			assertThat(ar.getActionsMenees().stream().map(am -> {
				return am.getId();
			}).collect(Collectors.toList()))
					.containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().findFirst().get().getActionsMenees());
			assertThat(ar.getTiers().stream().map(tier -> {
				return tier.getId();
			}).collect(Collectors.toList()))
					.containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().findFirst().get().getTiers().stream().map(tier -> {
						return tier.getOrganisationId();
					}).collect(Collectors.toList()));
		});

	}

	@Test
	public void testUpdateDeclarationActiviteWithBadSpaceId() {
		Long currentEspaceOrganisationId = new Random().nextLong();

		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity();
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity();
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity(currentEspaceOrganisationId + 1);
		exerciceComptableEntity.setEspaceOrganisation(espaceOrganisationEntity);
		activiteRepresentationInteretEntity.setExerciceComptable(exerciceComptableEntity);

		when(activiteRepresentationInteretRepository.findOne(Mockito.anyLong())).thenReturn(activiteRepresentationInteretEntity);

		try {
			this.declarationActiviteService.updateDeclarationActivite(new ActiviteRepresentationInteretDto(), currentEspaceOrganisationId);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.MAUVAIS_ESPACE.getMessage()).isEqualTo(ex.getMessage());
		}

	}

	@Test
	public void testUpdateDeclarationActiviteWithDuplicateTiers() {
		ActiviteRepresentationInteretDto activiteRepresentationInteretDto = new ActiviteRepresentationInteretDto();
		ActionRepresentationInteretDto actionRepresentationInteretDto = new ActionRepresentationInteretDto();
		actionRepresentationInteretDto.setTiers(Arrays.asList(new TiersDto(1L, "bla"), new TiersDto(2L, "blu")));
		activiteRepresentationInteretDto.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteretDto));
		ActionRepresentationInteretDto actionRepresentationInteretDtoWithId = new ActionRepresentationInteretDto();
		actionRepresentationInteretDtoWithId.setTiers(Arrays.asList(new TiersDto(1L, "bla"), new TiersDto(2L, "blu")));
		activiteRepresentationInteretDto.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteretDto, actionRepresentationInteretDtoWithId));

		try {
			this.declarationActiviteService.updateDeclarationActivite(activiteRepresentationInteretDto, new Random().nextLong());
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.DUPLICATE_TIERS.getMessage()).isEqualTo(ex.getMessage());

		}

	}

	@Test
	public void testUpdateDeclarationActiviteWithActionAutreError1() {
		ActiviteRepresentationInteretDto activiteRepresentationInteretDto = new ActiviteRepresentationInteretDto();
		ActionRepresentationInteretDto actionRepresentationInteretDto = new ActionRepresentationInteretDto();
		actionRepresentationInteretDto.setActionsMenees(new HashSet<>(Arrays.asList(10L)));
		activiteRepresentationInteretDto.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteretDto));

		try {
			this.declarationActiviteService.updateDeclarationActivite(activiteRepresentationInteretDto, new Random().nextLong());
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.ACTION_AUTRE_NON_RENSEIGNEE.getMessage()).isEqualTo(ex.getMessage());
		}

	}

	@Test
	public void testUpdateDeclarationActiviteWithActionAutreError2() {
		ActiviteRepresentationInteretDto activiteRepresentationInteretDto = new ActiviteRepresentationInteretDto();
		ActionRepresentationInteretDto actionRepresentationInteretDto = new ActionRepresentationInteretDto();
		actionRepresentationInteretDto.setActionsMenees(new HashSet<>(Arrays.asList(10L)));
		actionRepresentationInteretDto.setActionMeneeAutre("");
		activiteRepresentationInteretDto.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteretDto));

		try {
			this.declarationActiviteService.updateDeclarationActivite(activiteRepresentationInteretDto, new Random().nextLong());
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.ACTION_AUTRE_NON_RENSEIGNEE.getMessage()).isEqualTo(ex.getMessage());
		}

	}

	@Test
	public void testUpdateDeclarationActiviteWithReponsablePublicAutreError1() {
		ActiviteRepresentationInteretDto activiteRepresentationInteretDto = new ActiviteRepresentationInteretDto();
		ActionRepresentationInteretDto actionRepresentationInteretDto = new ActionRepresentationInteretDto();
		actionRepresentationInteretDto.setActionsMenees(new HashSet<>());
		actionRepresentationInteretDto.setReponsablesPublics(new HashSet<>(Arrays.asList(19L)));
		activiteRepresentationInteretDto.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteretDto));

		try {
			this.declarationActiviteService.updateDeclarationActivite(activiteRepresentationInteretDto, new Random().nextLong());
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.RESPONSABLE_PUBLIC_AUTRE_NON_RENSEIGNEE.getMessage()).isEqualTo(ex.getMessage());
		}

	}

	@Test
	public void testUpdateDeclarationActiviteWithReponsablePublicAutreError2() {
		ActiviteRepresentationInteretDto activiteRepresentationInteretDto = new ActiviteRepresentationInteretDto();
		ActionRepresentationInteretDto actionRepresentationInteretDto = new ActionRepresentationInteretDto();
		actionRepresentationInteretDto.setActionsMenees(new HashSet<>());
		actionRepresentationInteretDto.setReponsablesPublics(new HashSet<>(Arrays.asList(19L)));
		actionRepresentationInteretDto.setResponsablePublicAutre("");
		activiteRepresentationInteretDto.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteretDto));

		try {
			this.declarationActiviteService.updateDeclarationActivite(activiteRepresentationInteretDto, new Random().nextLong());
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.RESPONSABLE_PUBLIC_AUTRE_NON_RENSEIGNEE.getMessage()).isEqualTo(ex.getMessage());
		}

	}

	@Test
	public void testUpdateDeclarationActivite() {
		Long currentEspaceOrganisationId = new Random().nextLong();
		Long activiteRepresentationInteretId = new Random().nextLong();
		Long actionRepresentationInteretToUpdateId = new Random().nextLong();
		Long actionRepresentationInteretToDeleteId = new Random().nextLong();

		ActiviteRepresentationInteretDto activiteRepresentationInteretDto = new ActiviteRepresentationInteretDto();
		activiteRepresentationInteretDto.setId(activiteRepresentationInteretId);
		activiteRepresentationInteretDto.setDomainesIntervention(new HashSet<>(Arrays.asList(7L, 8L, 9L)));
		ActionRepresentationInteretDto actionRepresentationInteretDto = new ActionRepresentationInteretDto();
		actionRepresentationInteretDto.setReponsablesPublics(new HashSet<>(Arrays.asList(1L, 2L)));
		actionRepresentationInteretDto.setDecisionsConcernees(new HashSet<>(Arrays.asList(1L, 2L)));
		actionRepresentationInteretDto.setActionsMenees(new HashSet<>(Arrays.asList(1L, 2L, 10L)));
		actionRepresentationInteretDto.setActionMeneeAutre("mon action");
		actionRepresentationInteretDto.setTiers(Arrays.asList(new TiersDto(1L, "bla"), new TiersDto(2L, "blu")));
		activiteRepresentationInteretDto.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteretDto));
		ActionRepresentationInteretDto actionRepresentationInteretDtoWithId = new ActionRepresentationInteretDto();
		actionRepresentationInteretDtoWithId.setReponsablesPublics(new HashSet<>(Arrays.asList(1L, 2L, 19L)));
		actionRepresentationInteretDtoWithId.setResponsablePublicAutre("mon responsable");
		actionRepresentationInteretDtoWithId.setDecisionsConcernees(new HashSet<>(Arrays.asList(1L, 2L)));
		actionRepresentationInteretDtoWithId.setActionsMenees(new HashSet<>(Arrays.asList(1L, 2L)));
		actionRepresentationInteretDtoWithId.setTiers(Arrays.asList(new TiersDto(3L, "blo"), new TiersDto(4L, "bli")));
		actionRepresentationInteretDtoWithId.setId(actionRepresentationInteretToUpdateId);
		activiteRepresentationInteretDto.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteretDto, actionRepresentationInteretDtoWithId));

		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity(activiteRepresentationInteretId);
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity();
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity(currentEspaceOrganisationId);
		exerciceComptableEntity.setEspaceOrganisation(espaceOrganisationEntity);
		activiteRepresentationInteretEntity.setExerciceComptable(exerciceComptableEntity);
		ActionRepresentationInteretEntity actionRepresentationInteretEntityToUpdate = new ActionRepresentationInteretEntity(actionRepresentationInteretToUpdateId);
		ActionRepresentationInteretEntity actionRepresentationInteretEntityToDelete = new ActionRepresentationInteretEntity(actionRepresentationInteretToDeleteId);
		activiteRepresentationInteretEntity.getActionsRepresentationInteret().add(actionRepresentationInteretEntityToUpdate);
		activiteRepresentationInteretEntity.getActionsRepresentationInteret().add(actionRepresentationInteretEntityToDelete);

		when(activiteRepresentationInteretRepository.findOne(activiteRepresentationInteretId)).thenReturn(activiteRepresentationInteretEntity);

		when(activiteRepresentationInteretRepository.save(Mockito.any(ActiviteRepresentationInteretEntity.class))).thenAnswer(repo -> repo.getArguments()[0]);

		ActiviteRepresentationInteretDto dto = this.declarationActiviteService.updateDeclarationActivite(activiteRepresentationInteretDto, currentEspaceOrganisationId);

		verify(activiteRepresentationInteretRepository, times(1)).findOne(activiteRepresentationInteretId);
		verify(activiteRepresentationInteretRepository, times(1)).save(Mockito.any(ActiviteRepresentationInteretEntity.class));

		assertThat(dto.getId()).isEqualTo(activiteRepresentationInteretDto.getId());
		assertThat(dto.getDomainesIntervention()).containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretDto.getDomainesIntervention());
		assertThat(dto.getActionsRepresentationInteret().size()).isEqualTo(activiteRepresentationInteretDto.getActionsRepresentationInteret().size());
		assertThat(dto.getActionsRepresentationInteret().stream().peek(t -> t.getId()).map(lamb -> {
			return lamb.getId();
		}).collect(Collectors.toList())).contains(actionRepresentationInteretToUpdateId);
		dto.getActionsRepresentationInteret().stream().forEach(response -> {
			assertThat(actionRepresentationInteretToDeleteId).isNotEqualTo(response.getId());
			if (actionRepresentationInteretToUpdateId.equals(response.getId())) {
				activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().filter(filtered -> actionRepresentationInteretToUpdateId.equals(filtered.getId()))
						.forEach(request -> {
							assertThat(response.getReponsablesPublics()).containsExactlyInAnyOrderElementsOf(request.getReponsablesPublics());
							assertThat(response.getResponsablePublicAutre()).isEqualTo(request.getResponsablePublicAutre());
							assertThat(response.getDecisionsConcernees()).containsExactlyInAnyOrderElementsOf(request.getDecisionsConcernees());
							assertThat(response.getActionsMenees()).containsExactlyInAnyOrderElementsOf(request.getActionsMenees());
							assertThat(response.getTiers()).containsExactlyInAnyOrderElementsOf(request.getTiers());
						});
			} else {
				assertThat(response.getId()).isNull();
				activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().filter(filtered -> filtered.getId() == null).forEach(request -> {
					assertThat(response.getReponsablesPublics()).containsExactlyInAnyOrderElementsOf(request.getReponsablesPublics());
					assertThat(response.getDecisionsConcernees()).containsExactlyInAnyOrderElementsOf(request.getDecisionsConcernees());
					assertThat(response.getActionsMenees()).containsExactlyInAnyOrderElementsOf(request.getActionsMenees());
					assertThat(response.getActionMeneeAutre()).isEqualTo(request.getActionMeneeAutre());
					assertThat(response.getTiers()).containsExactlyInAnyOrderElementsOf(request.getTiers());
				});
			}
		});
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.DeclarationActiviteServiceImpl#getDeclarationActivite(final Long ficheId) }
	 * 
	 */
	@Test
	public void testGetDeclarationActiviteAvecException() {
	
		Mockito.doReturn(null).when(this.activiteRepresentationInteretRepository).findOne(anyLong());
		
		try {
			ActiviteRepresentationInteretDto retourActiviteRepresentationInteretDto = this.declarationActiviteService.getDeclarationActivite(anyLong());
		  }catch(BusinessGlobalException aExp){
			  assertThat(ErrorMessageEnum.DECLARATION_INTROUVABLE.getMessage()).isEqualTo(aExp.getMessage());
		  }		
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.DeclarationActiviteServiceImpl#updateDeclarationActivite(final ActiviteRepresentationInteretDto activiteRepresentationInteret, final Long currentEspaceOrganisationId) }
	 * cas StatutPublicationEnum.NON_PUBLIEE
	 */
	@Test
	public void testUpdateDeclarationActiviteNonPublie() {
		Long currentEspaceOrganisationId = new Random().nextLong();
		Long activiteRepresentationInteretId = new Random().nextLong();
		Long actionRepresentationInteretToUpdateId = new Random().nextLong();
		Long actionRepresentationInteretToDeleteId = new Random().nextLong();
		Long exerciceComptableId = new Random().nextLong();

		ActiviteRepresentationInteretDto activiteRepresentationInteretDto = new ActiviteRepresentationInteretDto();
		activiteRepresentationInteretDto.setId(activiteRepresentationInteretId);
		activiteRepresentationInteretDto.setDomainesIntervention(new HashSet<>(Arrays.asList(7L, 8L, 9L)));
		ActionRepresentationInteretDto actionRepresentationInteretDto = new ActionRepresentationInteretDto();
		actionRepresentationInteretDto.setReponsablesPublics(new HashSet<>(Arrays.asList(1L, 2L)));
		actionRepresentationInteretDto.setDecisionsConcernees(new HashSet<>(Arrays.asList(1L, 2L)));
		actionRepresentationInteretDto.setActionsMenees(new HashSet<>(Arrays.asList(1L, 2L, 10L)));
		actionRepresentationInteretDto.setActionMeneeAutre("mon action");
		actionRepresentationInteretDto.setTiers(Arrays.asList(new TiersDto(1L, "bla"), new TiersDto(2L, "blu")));
		activiteRepresentationInteretDto.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteretDto));
		ActionRepresentationInteretDto actionRepresentationInteretDtoWithId = new ActionRepresentationInteretDto();
		actionRepresentationInteretDtoWithId.setReponsablesPublics(new HashSet<>(Arrays.asList(1L, 2L, 19L)));
		actionRepresentationInteretDtoWithId.setResponsablePublicAutre("mon responsable");
		actionRepresentationInteretDtoWithId.setDecisionsConcernees(new HashSet<>(Arrays.asList(1L, 2L)));
		actionRepresentationInteretDtoWithId.setActionsMenees(new HashSet<>(Arrays.asList(1L, 2L)));
		actionRepresentationInteretDtoWithId.setTiers(Arrays.asList(new TiersDto(3L, "blo"), new TiersDto(4L, "bli")));
		actionRepresentationInteretDtoWithId.setId(actionRepresentationInteretToUpdateId);
		activiteRepresentationInteretDto.setActionsRepresentationInteret(Arrays.asList(actionRepresentationInteretDto, actionRepresentationInteretDtoWithId));
		
		activiteRepresentationInteretDto.setExerciceComptable(exerciceComptableId); 

		ActiviteRepresentationInteretEntity activiteRepresentationInteretEntity = new ActiviteRepresentationInteretEntity(activiteRepresentationInteretId);
		activiteRepresentationInteretEntity.setStatut(StatutPublicationEnum.NON_PUBLIEE);
		ExerciceComptableEntity exerciceComptableEntity = new ExerciceComptableEntity();
		exerciceComptableEntity.setId(new Random().nextLong());
		EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity(currentEspaceOrganisationId);
		exerciceComptableEntity.setEspaceOrganisation(espaceOrganisationEntity);
		activiteRepresentationInteretEntity.setExerciceComptable(exerciceComptableEntity);
		ActionRepresentationInteretEntity actionRepresentationInteretEntityToUpdate = new ActionRepresentationInteretEntity(actionRepresentationInteretToUpdateId);
		ActionRepresentationInteretEntity actionRepresentationInteretEntityToDelete = new ActionRepresentationInteretEntity(actionRepresentationInteretToDeleteId);
		activiteRepresentationInteretEntity.getActionsRepresentationInteret().add(actionRepresentationInteretEntityToUpdate);
		activiteRepresentationInteretEntity.getActionsRepresentationInteret().add(actionRepresentationInteretEntityToDelete);
		
		
		ExerciceComptableEntity exerciceComptableEntityRepo = new ExerciceComptableEntity();
		exerciceComptableEntityRepo.setId(exerciceComptableId);

		when(activiteRepresentationInteretRepository.findOne(activiteRepresentationInteretId)).thenReturn(activiteRepresentationInteretEntity);
		when(exerciceComptableRepository.getOne(activiteRepresentationInteretDto.getExerciceComptable())).thenReturn(exerciceComptableEntityRepo);
		when(activiteRepresentationInteretRepository.save(Mockito.any(ActiviteRepresentationInteretEntity.class))).thenAnswer(repo -> repo.getArguments()[0]);

		ActiviteRepresentationInteretDto dto = this.declarationActiviteService.updateDeclarationActivite(activiteRepresentationInteretDto, currentEspaceOrganisationId);

		verify(activiteRepresentationInteretRepository, times(1)).findOne(activiteRepresentationInteretId);
		verify(activiteRepresentationInteretRepository, times(1)).save(Mockito.any(ActiviteRepresentationInteretEntity.class));

		assertThat(dto.getId()).isEqualTo(activiteRepresentationInteretDto.getId());
		assertThat(dto.getExerciceComptable()).isEqualTo(exerciceComptableEntityRepo.getId());
		assertThat(dto.getDomainesIntervention()).containsExactlyInAnyOrderElementsOf(activiteRepresentationInteretDto.getDomainesIntervention());
		assertThat(dto.getActionsRepresentationInteret().size()).isEqualTo(activiteRepresentationInteretDto.getActionsRepresentationInteret().size());
		assertThat(dto.getActionsRepresentationInteret().stream().peek(t -> t.getId()).map(lamb -> {
			return lamb.getId();
		}).collect(Collectors.toList())).contains(actionRepresentationInteretToUpdateId);
		dto.getActionsRepresentationInteret().stream().forEach(response -> {
			assertThat(actionRepresentationInteretToDeleteId).isNotEqualTo(response.getId());
			if (actionRepresentationInteretToUpdateId.equals(response.getId())) {
				activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().filter(filtered -> actionRepresentationInteretToUpdateId.equals(filtered.getId()))
						.forEach(request -> {
							assertThat(response.getReponsablesPublics()).containsExactlyInAnyOrderElementsOf(request.getReponsablesPublics());
							assertThat(response.getResponsablePublicAutre()).isEqualTo(request.getResponsablePublicAutre());
							assertThat(response.getDecisionsConcernees()).containsExactlyInAnyOrderElementsOf(request.getDecisionsConcernees());
							assertThat(response.getActionsMenees()).containsExactlyInAnyOrderElementsOf(request.getActionsMenees());
							assertThat(response.getTiers()).containsExactlyInAnyOrderElementsOf(request.getTiers());
						});
			} else {
				assertThat(response.getId()).isNull();
				activiteRepresentationInteretDto.getActionsRepresentationInteret().stream().filter(filtered -> filtered.getId() == null).forEach(request -> {
					assertThat(response.getReponsablesPublics()).containsExactlyInAnyOrderElementsOf(request.getReponsablesPublics());
					assertThat(response.getDecisionsConcernees()).containsExactlyInAnyOrderElementsOf(request.getDecisionsConcernees());
					assertThat(response.getActionsMenees()).containsExactlyInAnyOrderElementsOf(request.getActionsMenees());
					assertThat(response.getActionMeneeAutre()).isEqualTo(request.getActionMeneeAutre());
					assertThat(response.getTiers()).containsExactlyInAnyOrderElementsOf(request.getTiers());
				});
			}
		});
	}
}
