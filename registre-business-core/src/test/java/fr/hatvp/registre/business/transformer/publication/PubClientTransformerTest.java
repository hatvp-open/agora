/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.publication;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.publication.PubClientTransformer;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.publication.PubClientEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static fr.hatvp.registre.business.transformer.publication.CommonTestClass.getClientDto;
import static fr.hatvp.registre.business.transformer.publication.CommonTestClass.getPubClientEntity;

/**
 * Test de la classe {@link PubClientTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class PubClientTransformerTest {

    /** Transformer des clients publiés. */
    @Autowired
    private PubClientTransformer pubClientTransformer;

    /**
     * Test de la métode modelToDto de {@link PubClientTransformer#dtoToModel(AbstractCommonDto)}
     */
    @Test
    public void testDtoToModel() {
        final AssoClientDto dto = getClientDto();

        final PubClientEntity entity = this.pubClientTransformer.dtoToModel(dto);

        Assert.assertNotNull(entity);
        Assert.assertEquals(dto.getId(), entity.getId());
        Assert.assertEquals(dto.getVersion(), entity.getVersion());
    }

    /**
     * Test de la métode modelToDto de {@link PubClientTransformer#modelToDto(AbstractCommonEntity)}
     */
    @Test
    public void testModelToDto() {
        final PubClientEntity entity = getPubClientEntity();

        final AssoClientDto dto = this.pubClientTransformer.modelToDto(entity);

        Assert.assertNotNull(dto);
        Assert.assertEquals(entity.getId(), dto.getId());
        Assert.assertEquals(entity.getVersion(), dto.getVersion());
        Assert.assertEquals(entity.getEspaceOrganisation().getId(), dto.getEspaceOrganisationId());
        Assert.assertEquals(entity.getOrganisation().getId(), dto.getOrganisationId());
        Assert.assertEquals(entity.getOrganisation().getDenomination(), dto.getDenomination());
        Assert.assertEquals(entity.getOrganisation().getSiren(), dto.getNationalId());
    }
}
