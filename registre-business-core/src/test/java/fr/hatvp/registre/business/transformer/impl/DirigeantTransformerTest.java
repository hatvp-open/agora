/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.DirigeantTransformer;
import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.espace.DirigeantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * Test de la classe {@link DirigeantTransformer}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class DirigeantTransformerTest
{

    /** Transfoemr des dirigeants. */
    @Autowired
    private DirigeantTransformer dirigeantTransformer;

    /**
     * Test de la métode modelToDto.
     */
    @Test
    public void testDtoToModel()
    {
        final DirigeantEntity entity = this.getDirigeantEntity();
        final DirigeantDto dto = this.dirigeantTransformer.modelToDto(entity);

        Assert.assertNotNull(dto);
        Assert.assertEquals(entity.getId(), dto.getId());
        Assert.assertEquals(entity.getVersion(), dto.getVersion());
        Assert.assertEquals(entity.getNom(), dto.getNom());
        Assert.assertEquals(entity.getPrenom(), dto.getPrenom());

    }

    /**
     * Test de la métode modelToDto.
     */
    @Test
    public void testModelToDto()
    {

        final DirigeantDto dto = this.getDirigeantDto();
        final DirigeantEntity entity = this.dirigeantTransformer.dtoToModel(dto);

        Assert.assertNotNull(entity);
        Assert.assertEquals(dto.getId(), entity.getId());
        Assert.assertEquals(dto.getVersion(), entity.getVersion());
        Assert.assertEquals(dto.getNom(), entity.getNom());
        Assert.assertEquals(dto.getPrenom(), entity.getPrenom());
        Assert.assertEquals(dto.getEspaceOrganisationId(), entity.getEspaceOrganisation().getId());

    }

    /**
     * Crréer une entité dirigeant.
     */
    private DirigeantEntity getDirigeantEntity()
    {
        final DirigeantEntity dirigeantEntity = new DirigeantEntity();
        dirigeantEntity.setId(RegistreUtils.randomLong(1));
        dirigeantEntity.setVersion(RegistreUtils.randomLong(2).intValue());
        dirigeantEntity.setNom(RegistreUtils.randomString(20));
        dirigeantEntity.setPrenom(RegistreUtils.randomString(20));

        final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(RegistreUtils.randomLong(1));
        dirigeantEntity.setEspaceOrganisation(espaceOrganisationEntity);
        return dirigeantEntity;
    }

    /**
     * Crréer un dto dirigeant.
     */
    private DirigeantDto getDirigeantDto()
    {
        final DirigeantDto dto = new DirigeantDto();
        dto.setId(RegistreUtils.randomLong(1));
        dto.setVersion(RegistreUtils.randomLong(2).intValue());
        dto.setNom(RegistreUtils.randomString(20));
        dto.setPrenom(RegistreUtils.randomString(20));
        return dto;
    }
}
