/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.mifmif.common.regex.Generex;

import fr.hatvp.registre.business.transformer.backoffice.UtilisateurBoTransformer;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoGestionDto;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.commons.utils.ValidatePatternUtils;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.repository.backoffice.UtilisateurBoRepository;

/**
 * Classe de test su service {@link fr.hatvp.registre.business.backoffice.UtilisateurBoService}
 *
 * @version $Revision$ $Date${0xD}
 */

public class UtilisateurBoServiceTest {

    /** Répository des utilisateurs bo. */
    @Mock
    private UtilisateurBoRepository utilisateurBoRepository;

    /** Transformeur de l'entitée utilisateurBo. */
    @Mock
    private UtilisateurBoTransformer utilisateurBoTransformer;

    /** Générateur de mot de passe de spring security. */
    @Mock
    private PasswordEncoder passwordEncoder;

    /** Injections des mocks. */
    @InjectMocks
    private UtilisateurBoServiceImpl utilisateurBoService;

    /** Initialisation des tests. */
    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

    }

    /**
     * EMAIL_TEST utilisé pour les tests.
     */
    private final String EMAIL_UTILISATEUR_TEST = "test@test.test";

    /**
     * ID utilisé pour les tests.
     */
    private static final Long ID_UTILISATEUR_TEST = 1L;

    /**
     * PASSWORD utilisé pour les tests.
     */
    private static final String PASSWORD_UTILISATEUR_TEST = "1azsZ84Z";

    /**
     * Test de la méthode
     * {@link fr.hatvp.registre.business.backoffice.UtilisateurBoService#findUtilisateurByEmail(String)}
     */
    @Test
    public void testFindUtilisateurByEmail() {

        final UtilisateurBoDto utilisateurBoDto = new UtilisateurBoDto();
        utilisateurBoDto.setEmail(this.EMAIL_UTILISATEUR_TEST);

        final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
        utilisateurBoEntity.setEmail(utilisateurBoDto.getEmail());

        Mockito.doReturn(Optional.of(utilisateurBoEntity)).when(this.utilisateurBoRepository)
            .findByEmail(this.EMAIL_UTILISATEUR_TEST);
        Mockito.doReturn(utilisateurBoDto).when(this.utilisateurBoTransformer)
            .modelToDto(utilisateurBoEntity);

        final UtilisateurBoDto foundUtilisateurDto = this.utilisateurBoService
            .findUtilisateurByEmail(this.EMAIL_UTILISATEUR_TEST);

        Assert.assertNotNull(foundUtilisateurDto);
        Assert.assertNotNull(foundUtilisateurDto.getEmail());
        Assert.assertEquals(utilisateurBoEntity.getEmail(), foundUtilisateurDto.getEmail());
    }

    /**
     * Test de la méthode
     * {@link fr.hatvp.registre.business.backoffice.UtilisateurBoService#ajouterUtilisateurBo(UtilisateurBoDto)}
     */
    @Test
    public void testSaveUtilisateurBo() {
        final UtilisateurBoDto utilisateurBoDto = new UtilisateurBoDto();
        utilisateurBoDto.setNom(RegistreUtils.randomString(20));
        utilisateurBoDto.setPrenom(RegistreUtils.randomString(20));
        utilisateurBoDto.setEmail(RegistreUtils.randomString(30));
        utilisateurBoDto.setPassword(RegistreUtils.randomString(30));

        final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
        utilisateurBoDto.setNom(utilisateurBoDto.getNom());
        utilisateurBoDto.setPrenom(utilisateurBoDto.getPrenom());
        utilisateurBoDto.setEmail(utilisateurBoDto.getEmail());

        Mockito.doReturn(Optional.empty()).when(this.utilisateurBoRepository)
            .findByEmail(utilisateurBoDto.getEmail());
        Mockito.doReturn(utilisateurBoEntity).when(this.utilisateurBoTransformer)
            .dtoToModel(utilisateurBoDto);
        Mockito.doReturn(utilisateurBoEntity).when(this.utilisateurBoRepository)
            .save(utilisateurBoEntity);
        Mockito.doReturn(utilisateurBoDto).when(this.utilisateurBoTransformer)
            .modelToDto(utilisateurBoEntity);
        when(this.passwordEncoder.encode(utilisateurBoDto.getPassword())).thenReturn(utilisateurBoDto.getPassword());

        final UtilisateurBoGestionDto savedUtilisateur = this.utilisateurBoService
            .ajouterUtilisateurBo(utilisateurBoDto);

        Assert.assertNotNull(savedUtilisateur);
        Assert.assertEquals(utilisateurBoDto.getNom(), savedUtilisateur.getNom());
        Assert.assertEquals(utilisateurBoDto.getPrenom(), savedUtilisateur.getPrenom());
        Assert.assertEquals(utilisateurBoDto.getEmail(), savedUtilisateur.getEmail());
        Assert.assertEquals(utilisateurBoDto.getPassword(), savedUtilisateur.getPassword());
    }

    /**
     * Test de la méthode
     * {@link fr.hatvp.registre.business.backoffice.UtilisateurBoService#updatePassword(Long)}
     */
    @Test
    public void testUpdatePassword() {

        final UtilisateurBoEntity utilisateurBoEntity = this.getUtilisateurBoEntity();

        final UtilisateurBoDto utilisateurBoDto = this.getUtilisateurBoDto(utilisateurBoEntity);

        Mockito.doReturn(utilisateurBoEntity).when(this.utilisateurBoRepository)
            .findOne(ID_UTILISATEUR_TEST);
        Mockito.doReturn(utilisateurBoDto).when(this.utilisateurBoTransformer)
            .modelToDto(utilisateurBoEntity);

        Mockito.doReturn(utilisateurBoEntity).when(this.utilisateurBoRepository)
            .save(utilisateurBoEntity);

        final UtilisateurBoGestionDto updatedUtilisateurDto = this.utilisateurBoService
            .updatePassword(ID_UTILISATEUR_TEST);

        Assert.assertNotNull(updatedUtilisateurDto);
        Assert.assertEquals(8, updatedUtilisateurDto.getPassword().length());
    }

    /**
     * Test de la méthode
     * {@link fr.hatvp.registre.business.backoffice.UtilisateurBoService#supprimerUtilisateur(Long)}
     */
    @Test
    public void testSupprimerUtilisateur() {

        final UtilisateurBoEntity utilisateurBoEntity = this.getUtilisateurBoEntity();

        Mockito.doReturn(utilisateurBoEntity).when(this.utilisateurBoRepository)
            .findOne(ID_UTILISATEUR_TEST);

        this.utilisateurBoService.supprimerUtilisateur(ID_UTILISATEUR_TEST);
    }

    /**
     * Test de la méthode
     * {@link fr.hatvp.registre.business.backoffice.UtilisateurBoService#supprimerUtilisateur(Long)}
     * avec un utilisateur qui n'existe pas.
     */
    @Test(expected = EntityNotFoundException.class)
    public void testSupprimerUtilisateurAvecIdNonExistant() {

        Mockito.doReturn(null).when(this.utilisateurBoRepository).findOne(ID_UTILISATEUR_TEST);

        this.utilisateurBoService.supprimerUtilisateur(ID_UTILISATEUR_TEST);
    }

    /**
     * Test de la méthode
     * {@link fr.hatvp.registre.business.backoffice.UtilisateurBoService#majUtilisateurBo(UtilisateurBoDto)}
     */
    @Test
    public void testMmjUtilisateurBo() {

        final UtilisateurBoEntity utilisateurBoEntity = this.getUtilisateurBoEntity();

        final UtilisateurBoDto utilisateurBoDto = this.getUtilisateurBoDto(utilisateurBoEntity);

        Mockito.doReturn(utilisateurBoEntity).when(this.utilisateurBoTransformer)
            .dtoToModel(utilisateurBoDto);
        Mockito.doReturn(utilisateurBoDto).when(this.utilisateurBoTransformer)
            .modelToDto(utilisateurBoEntity);
        Mockito.doReturn(utilisateurBoEntity).when(this.utilisateurBoRepository)
            .findOne(ID_UTILISATEUR_TEST);
        Mockito.doReturn(utilisateurBoEntity).when(this.utilisateurBoRepository)
            .save(utilisateurBoEntity);

        final UtilisateurBoDto savedDto = this.utilisateurBoService
            .majUtilisateurBo(utilisateurBoDto);

        Assert.assertNotNull(savedDto);
        Assert.assertEquals(utilisateurBoDto.getEmail(), savedDto.getEmail());
        Assert.assertEquals(utilisateurBoDto.getNom(), savedDto.getNom());
        Assert.assertEquals(utilisateurBoDto.getPrenom(), savedDto.getPrenom());
    }

    /**
     * Test de la méthode
     * {@link fr.hatvp.registre.business.backoffice.UtilisateurBoService#switchActivationCompte(UtilisateurBoDto)}
     */
    @Test
    public void testSwitchActivationCompteCompteActive() {

        final UtilisateurBoEntity utilisateurBoEntity = this.getUtilisateurBoEntity();
        utilisateurBoEntity.setCompteActive(true);

        final UtilisateurBoDto utilisateurBoDto = this.getUtilisateurBoDto(utilisateurBoEntity);
        utilisateurBoDto.setCompteActive(utilisateurBoEntity.getCompteActive());

        Mockito.doReturn(utilisateurBoEntity).when(this.utilisateurBoTransformer)
            .dtoToModel(utilisateurBoDto);
        Mockito.doReturn(utilisateurBoEntity).when(this.utilisateurBoRepository)
            .save(utilisateurBoEntity);
        Mockito.doReturn(utilisateurBoDto).when(this.utilisateurBoTransformer)
            .modelToDto(utilisateurBoEntity);

        final UtilisateurBoDto updatedUser = this.utilisateurBoService
            .switchActivationCompte(utilisateurBoDto);

        Assert.assertNotNull(updatedUser);
        Assert.assertTrue(updatedUser.getCompteActive());
    }

    /**
     * Test du générateur de mot de passe à partir d''une expression régulière.
     */
    @Test
    public void testPasswordRegexGenerator() {
        // initialiser le générateur de mot de passe.
        final Generex generex = new Generex(ValidatePatternUtils.getPasswordGenerationPattern());
        // générer un mot de passe.
        final String result = generex.random();

        Assert.assertEquals(8, result.length());
        assert result.matches(ValidatePatternUtils.getPasswordGenerationPattern());
    }

    /**
     * @param utilisateurBoEntity entité utilisateur bo.
     * @return dto utilisateur bo.
     */
    private UtilisateurBoDto getUtilisateurBoDto(final UtilisateurBoEntity utilisateurBoEntity) {
        final UtilisateurBoDto utilisateurBoDto = new UtilisateurBoDto();
        utilisateurBoDto.setId(utilisateurBoEntity.getId());
        utilisateurBoDto.setEmail(utilisateurBoEntity.getEmail());
        utilisateurBoDto.setPassword(utilisateurBoEntity.getPassword());
        utilisateurBoDto.setNom(utilisateurBoEntity.getNom());
        utilisateurBoDto.setPrenom(utilisateurBoEntity.getPrenom());
        return utilisateurBoDto;
    }

    /**
     * @return entité utilisateur bo.
     */
    private UtilisateurBoEntity getUtilisateurBoEntity() {
        final Generex generex = new Generex(ValidatePatternUtils.getPasswordGenerationPattern());
        final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
        utilisateurBoEntity.setEmail(this.EMAIL_UTILISATEUR_TEST);
        utilisateurBoEntity.setId(ID_UTILISATEUR_TEST);
        utilisateurBoEntity.setPassword(generex.random());
        utilisateurBoEntity.setNom(RegistreUtils.randomString(10));
        utilisateurBoEntity.setPrenom(RegistreUtils.randomString(10));
        return utilisateurBoEntity;
    }
	/**
	 * Test de la méthode
	 * {@link fr.hatvp.registre.business.backoffice.UtilisateurBoService#ajouterUtilisateurBo(UtilisateurBoDto)}
	 */
	@Test
	public void testSaveUtilisateurBoException() {
	    final UtilisateurBoDto utilisateurBoDto = new UtilisateurBoDto();
	    utilisateurBoDto.setNom(RegistreUtils.randomString(20));
	    utilisateurBoDto.setPrenom(RegistreUtils.randomString(20));
	    utilisateurBoDto.setEmail(RegistreUtils.randomString(30));
	    utilisateurBoDto.setPassword(RegistreUtils.randomString(30));
	
	    final UtilisateurBoEntity utilisateurBoEntity = new UtilisateurBoEntity();
	    utilisateurBoDto.setNom(utilisateurBoDto.getNom());
	    utilisateurBoDto.setPrenom(utilisateurBoDto.getPrenom());
	    utilisateurBoDto.setEmail(utilisateurBoDto.getEmail());
	
	    Mockito.doReturn(utilisateurBoEntity).when(this.utilisateurBoRepository)
	        .findByEmailIgnoreCase(utilisateurBoDto.getEmail());
	    when(this.passwordEncoder.encode(utilisateurBoDto.getPassword())).thenReturn(utilisateurBoDto.getPassword());
  
	
	    try {
	    	final UtilisateurBoGestionDto savedUtilisateur = this.utilisateurBoService
	    	        .ajouterUtilisateurBo(utilisateurBoDto);
		} catch (EntityExistsException ex) {
			assertThat(ErrorMessageEnum.ADRESSE_EMAIL_EXISTE_DEJA.getMessage()).isEqualTo(ex.getMessage());
		}
	}	
}
