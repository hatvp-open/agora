
/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.CollaborateurTransformer;
import fr.hatvp.registre.commons.dto.CollaborateurDto;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.CollaborateurEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * Test de la classe
 * {@link fr.hatvp.registre.business.transformer.impl.CollaborateurTransformerImpl}.
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class CollaborateurTransformerTest
{

    /** Transformer à tester. */
    @Autowired
    private CollaborateurTransformer collaborateurTransformer;

    /**
     * Test de la métode modelToDto pour collaborateur additionnel.
     */
    @Test
    public void testModelToDtoAdditionnel()
    {
        /*
         * Création d'une entité
         */
        final CollaborateurEntity model = new CollaborateurEntity();
        model.setId(1L);
        model.setVersion(0);
        model.setActif(true);
        model.setNom(RegistreUtils.randomString(15));
        model.setPrenom(RegistreUtils.randomString(10));
        model.setEmail(RegistreUtils.randomString(20));
        model.setCivility(CiviliteEnum.M);

        final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(2L);
        model.setEspaceOrganisation(espaceOrganisationEntity);

        /*
         * Transformation
         */
        final CollaborateurDto dto = this.collaborateurTransformer.modelToDto(model);

        /*
         * Assertions
         */
        Assert.assertNotNull(dto);
        Assert.assertEquals(model.getId(), dto.getId());
        Assert.assertEquals(model.getVersion(), dto.getVersion());
        Assert.assertEquals(model.getCivility(), dto.getCivilite());
        Assert.assertEquals(model.getNom(), dto.getNom());
        Assert.assertEquals(model.getPrenom(), dto.getPrenom());
        Assert.assertEquals(model.getEmail(), dto.getEmail());
        Assert.assertNull(dto.getDeclarantId());
        Assert.assertEquals(model.getActif(), dto.getActif());
        Assert.assertFalse(dto.getInscrit());
        Assert.assertEquals(model.getEspaceOrganisation().getId(), dto.getEspaceOrganisationId());
    }

    /**
     * Test de la métode modelToDto pour collaborateur inscrit.
     */
    @Test
    public void testModelToDtoInscrit()
    {
        /*
         * Création d'une entité
         */
        final CollaborateurEntity model = new CollaborateurEntity();
        model.setId(1L);
        model.setVersion(0);
        model.setActif(true);

        final DeclarantEntity declarantEntity = new DeclarantEntity();
        declarantEntity.setId(2L);
        declarantEntity.setNom(RegistreUtils.randomString(15));
        declarantEntity.setPrenom(RegistreUtils.randomString(10));
        declarantEntity.setEmail(RegistreUtils.randomString(20));
        declarantEntity.setCivility(CiviliteEnum.M);
        model.setDeclarant(declarantEntity);

        final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
        espaceOrganisationEntity.setId(3L);
        model.setEspaceOrganisation(espaceOrganisationEntity);

        /*
         * Transformation
         */
        final CollaborateurDto dto = this.collaborateurTransformer.modelToDto(model);

        /*
         * Assertions
         */
        Assert.assertNotNull(dto);
        Assert.assertEquals(model.getId(), dto.getId());
        Assert.assertEquals(model.getVersion(), dto.getVersion());
        Assert.assertEquals(model.getDeclarant().getId(), dto.getDeclarantId());
        Assert.assertEquals(model.getDeclarant().getCivility(), dto.getCivilite());
        Assert.assertEquals(model.getDeclarant().getNom(), dto.getNom());
        Assert.assertEquals(model.getDeclarant().getPrenom(), dto.getPrenom());
        Assert.assertEquals(model.getDeclarant().getEmail(), dto.getEmail());
        Assert.assertEquals(model.getActif(), dto.getActif());
        Assert.assertTrue(dto.getInscrit());
        Assert.assertEquals(model.getEspaceOrganisation().getId(), dto.getEspaceOrganisationId());
    }

    /**
     * Test de la métode modelToDto pour Collaborateur additionnel.
     */
    @Test
    public void testDtoToModelAdditionnel()
    {
        /*
         * Création d'un Dto
         */
        final CollaborateurDto dto = new CollaborateurDto();
        dto.setId(1L);
        dto.setVersion(0);
        dto.setEspaceOrganisationId(2L);
        dto.setCivilite(CiviliteEnum.M);
        dto.setNom(RegistreUtils.randomString(15));
        dto.setPrenom(RegistreUtils.randomString(10));
        dto.setEmail(RegistreUtils.randomString(20));
        dto.setActif(true);
        dto.setInscrit(false);

        /*
         * Transformation
         */
        final CollaborateurEntity model = this.collaborateurTransformer.dtoToModel(dto);

        /*
         * Assertions
         */
        Assert.assertNotNull(model);
        Assert.assertEquals(dto.getId(), model.getId());
        Assert.assertEquals(dto.getVersion(), model.getVersion());
        Assert.assertNull(model.getDeclarant());
        Assert.assertNotNull(model.getEspaceOrganisation());
        Assert.assertEquals(dto.getEspaceOrganisationId(), model.getEspaceOrganisation().getId());
    }

    /**
     * Test de la métode modelToDto pour Collaborateur inscrit.
     */
    @Test
    public void testDtoToModelInscrit()
    {
        /*
         * Création d'un Dto
         */
        final CollaborateurDto dto = new CollaborateurDto();
        dto.setId(1L);
        dto.setVersion(0);
        dto.setEspaceOrganisationId(2L);
        dto.setDeclarantId(3L);
        dto.setCivilite(CiviliteEnum.M);
        dto.setNom(RegistreUtils.randomString(15));
        dto.setPrenom(RegistreUtils.randomString(10));
        dto.setEmail(RegistreUtils.randomString(20));
        dto.setActif(true);
        dto.setInscrit(true);

        /*
         * Transformation
         */
        final CollaborateurEntity model = this.collaborateurTransformer.dtoToModel(dto);

        /*
         * Assertions
         */
        Assert.assertNotNull(model);
        Assert.assertEquals(dto.getId(), model.getId());
        Assert.assertEquals(dto.getVersion(), model.getVersion());
        Assert.assertNotNull(model.getDeclarant());
        Assert.assertEquals(dto.getDeclarantId(), model.getDeclarant().getId());
        Assert.assertNotNull(model.getEspaceOrganisation());
        Assert.assertEquals(dto.getEspaceOrganisationId(), model.getEspaceOrganisation().getId());
    }

}
