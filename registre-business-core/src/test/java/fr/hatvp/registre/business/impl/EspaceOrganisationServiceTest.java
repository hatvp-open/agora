

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.email.EspaceOrganisationBoMailer;
import fr.hatvp.registre.business.transformer.backoffice.ConsultationEspaceOrganisationSimpleTransformer;
import fr.hatvp.registre.business.transformer.backoffice.ValidationEspaceOrganisationDetailBoTransformer;
import fr.hatvp.registre.business.transformer.backoffice.ValidationEspaceOrganisationSimpleTransformer;
import fr.hatvp.registre.business.transformer.backoffice.ValidationEspaceOrganisationTransformer;
import fr.hatvp.registre.business.transformer.impl.DirigeantTransformerImpl;
import fr.hatvp.registre.business.transformer.impl.publication.PublicationEspaceTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.AssociationProTransformer;
import fr.hatvp.registre.business.transformer.proxy.ClientTransformer;
import fr.hatvp.registre.business.transformer.proxy.CollaborateurTransformer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.business.transformer.proxy.DirigeantTransformer;
import fr.hatvp.registre.business.transformer.proxy.EspaceOrganisationTransformer;
import fr.hatvp.registre.business.transformer.proxy.InscriptionEspaceTransformer;
import fr.hatvp.registre.business.transformer.proxy.activite.ExerciceComptableBlacklistedTransformer;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableBlacklistedDto;
import fr.hatvp.registre.commons.dto.backoffice.ConsultationEspaceBoSimpleDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceBoDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceBoSimpleDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceDetailBoDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.PublicationRelanceNiveauEnum;
import fr.hatvp.registre.commons.lists.PublicationRelanceTypeEnum;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;
import fr.hatvp.registre.commons.lists.backoffice.MotifComplementEspaceEnum;
import fr.hatvp.registre.commons.lists.backoffice.MotifRefusEspaceEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.commentaire.CommentValidationEspaceEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.DirigeantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity;
import fr.hatvp.registre.persistence.entity.publication.batch.relance.PublicationRelanceEntity;
import fr.hatvp.registre.persistence.repository.activite.PublicationActiviteRepository;
import fr.hatvp.registre.persistence.repository.activite.PublicationExerciceRepository;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentEspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentValidationEspaceRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;
import fr.hatvp.registre.persistence.repository.publication.PublicationRepository;

/**
 * Test du service EspaceOrganisationService {@link fr.hatvp.registre.business.EspaceOrganisationService}.
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestBusinessConfig.class }, loader = AnnotationConfigContextLoader.class)
public class EspaceOrganisationServiceTest {
	/** Id de test de l'espace organisation. */
	private final static Long ID_ESPACE_ORGANISATION = 1L;
	/** Id de test de l'utilisateur. */
	private final static Long ID_UTILISATEUR = 2L;
	/** Id de test de l'admin de l'espace orga. */
	private final static Long ID_ADMINISTRATEUR_ESPACE = 3L;
	/** Id de test de l'inscription. */
	private final static Long ID_INSCRIPTION_ESPACE = 4L;
	/** Id de test de l'organisation. */
	private final static Long ID_ORGANISATION = 5L;

	/** Id national utilisé pour les test. */
	private final static String ID_NATIONAL_TEST = "145236987";

	/** Nom d'usage utilisé pour les test. */
	private final static String NOM_USAGE_TEST = "NOM USAGE ORGA";

	/** fin d'exercice fiscal utilisé pour les test. */
	private final static String DATE_DE_CLOTURE = "01-01";
	/** Repository pour l'Espace Organisation. */
	@Mock
	private EspaceOrganisationRepository espaceOrganisationRepository;
	/** Repository pour l'inscription à l'Espace Organisation. */
	@Mock
	private InscriptionEspaceRepository inscriptionEspaceRepository;
	/** Service d'envoi de mails. */
	@Mock
	private EspaceOrganisationBoMailer espaceOrganisationBoMailer;
	/**
	 * Transformeur des {@link fr.hatvp.registre.commons.dto.EspaceOrganisationDto}.
	 */
	@Mock
	private ValidationEspaceOrganisationTransformer validationEspaceOrganisationTransformer;
	
	@Mock
	private ValidationEspaceOrganisationDetailBoTransformer validationEspaceOrganisationDetailBoTransformer;

	@Mock
	private ConsultationEspaceOrganisationSimpleTransformer consultationEspaceOrganisationSimpleTransformer;

	@Mock
	private ValidationEspaceOrganisationSimpleTransformer validationEspaceOrganisationSimpleTransformer;

	/** Transformeur des {@link InscriptionEspaceEntity}. */
	@Mock
	private InscriptionEspaceTransformer inscriptionEspaceTransformer;

	/** Transformeur de l'espace organisation. */
	@Mock
	private EspaceOrganisationTransformer espaceOrganisationTransformer;

	@Spy
	private DirigeantTransformer dirigeantTransformer = new DirigeantTransformerImpl();

	/** Transformeur des déclarants; */
	@Mock
	private DeclarantTransformer declarantTransformer;

	@Mock
	private PublicationRepository publicationRepository;

	/** Injecter les mock dans le service. */
	@InjectMocks
	private EspaceOrganisationServiceImpl espaceOrganisationService;

	/**
	 * Transformer des espaces vers publications.
	 */
	@Spy
	private PublicationEspaceTransformerImpl publicationEspaceTransformer = new PublicationEspaceTransformerImpl();

	@Mock
	private CommentValidationEspaceRepository commentValidationEspaceRepository;

	@Mock
	private CommentEspaceOrganisationRepository commentEspaceOrganisationRepository;
	
	@Mock
	private ClientTransformer clientTransformer;
	@Mock
	private AssociationProTransformer associationProTransformer;
	@Mock
	private CollaborateurTransformer collaborateurTransformer;
	
	@Mock
	private PublicationExerciceRepository publicationExerciceRepository;
	
	@Mock
	private PublicationActiviteRepository publicationActiviteRepository;
	
	@Mock
	private ExerciceComptableBlacklistedTransformer exerciceBlacklistedTransformer;

	/**
	 * Initialiser les mocks avant de lancer les test.
	 *
	 * @throws Exception
	 */
	@Before
	public void before() throws Exception {
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test upload Logo Method: uploadLogo(final Long id, final byte[] logo_file, final String logo_type)
	 */
	@Test
	public void testUploadLogo() {
		final String logoType = "image/png";
		final byte[] logo = new byte[500];

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setNonDeclarationTiers(false);
		espaceOrganisationEntity.setOrganisation(new OrganisationEntity());
		espaceOrganisationEntity.setCreationDate(new Date());

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);

		this.espaceOrganisationService.uploadLogo(ID_ESPACE_ORGANISATION, logo, logoType);

		Assert.assertEquals(logo, espaceOrganisationEntity.getLogo());
		Assert.assertEquals(logoType, espaceOrganisationEntity.getLogoType());
	}

	/**
	 * Test delete Logo Method: deleteLogo(final Long id)
	 */
	@Test
	public void testDeleteLogo() {
		final String logoType = "image/png";
		final byte[] logo = new byte[500];

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setNonDeclarationTiers(false);
		espaceOrganisationEntity.setOrganisation(new OrganisationEntity());
		espaceOrganisationEntity.setCreationDate(new Date());
		espaceOrganisationEntity.setLogo(logo);
		espaceOrganisationEntity.setLogoType(logoType);

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);

		this.espaceOrganisationService.deleteLogo(ID_ESPACE_ORGANISATION);

		Assert.assertNull(espaceOrganisationEntity.getLogo());
		Assert.assertNull(espaceOrganisationEntity.getLogoType());
	}

	/**
	 * Test de récupération des espaces collaboratifs à valider.
	 * <p>
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#getEspacesOrganisationEnAttente(Integer, Integer)}
	 */
	@Test
	public void testPaginatedGetEspacesOrganisationEnAttente() {

		final Pageable pageable = new PageRequest(10, 25, Sort.Direction.ASC, "id");

		final List<EspaceOrganisationEntity> espaceList = new ArrayList<>();
		for (int i = 0; i < pageable.getPageSize(); i++) {
			EspaceOrganisationEntity espaceEntity = new EspaceOrganisationEntity();
			espaceEntity.setId(Long.valueOf(RandomStringUtils.randomNumeric(3)));
			OrganisationEntity organisationEntity = new OrganisationEntity();
			organisationEntity.setId(1L);
			organisationEntity.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
			espaceEntity.setOrganisation(organisationEntity);
			espaceList.add(espaceEntity);
		}

		final Page<EspaceOrganisationEntity> espacePage = new PageImpl<>(espaceList, pageable, espaceList.size() * pageable.getPageNumber() * 2);

		when(espaceOrganisationRepository.findByStatutNotAndStatutNotOrderByIdDesc(StatutEspaceEnum.ACCEPTEE, StatutEspaceEnum.REFUSEE, pageable)).thenReturn(espacePage);
		when(espaceOrganisationRepository.findEspaceOrganisationByOrganisationIdAndStatutNotRefusee(Mockito.any())).thenReturn(new EspaceOrganisationEntity());

		final PaginatedDto<ValidationEspaceBoSimpleDto> paginatedDto = espaceOrganisationService.getEspacesOrganisationEnAttente(pageable.getPageNumber() + 1,
				pageable.getPageSize());

		Assert.assertEquals(paginatedDto.getDtos().size(), pageable.getPageSize());
		Assert.assertEquals(paginatedDto.getResultTotalNumber().longValue(), espacePage.getTotalElements());

	}

	/**
	 * Test validation d'un espace corporate
	 */
	@Test
	public void testValidateCreationRequest() {
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setStatut(StatutEspaceEnum.EN_TRAITEMENT);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);
		organisationEntity.setDenomination(RegistreUtils.randomString(20));
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		final DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(ID_ADMINISTRATEUR_ESPACE);
		declarantEntity.setCivility(CiviliteEnum.M);
		espaceOrganisationEntity.setCreateurEspaceOrganisation(declarantEntity);

		final InscriptionEspaceEntity inscriptionEspaceEntity = new InscriptionEspaceEntity();
		inscriptionEspaceEntity.setId(ID_INSCRIPTION_ESPACE);
		inscriptionEspaceEntity.setDateDemande(new Date());
		inscriptionEspaceEntity.setDeclarant(declarantEntity);
		inscriptionEspaceEntity.setEspaceOrganisation(espaceOrganisationEntity);

		final Set<RoleEnumFrontOffice> roleSet = new TreeSet<>();
		roleSet.add(RoleEnumFrontOffice.CONTRIBUTEUR);
		inscriptionEspaceEntity.setRolesFront(roleSet);

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		Mockito.doReturn(Optional.of(inscriptionEspaceEntity)).when(this.inscriptionEspaceRepository)
				.findByDateSuppressionIsNullAndDeclarantIdAndEspaceOrganisationId(ID_ADMINISTRATEUR_ESPACE, ID_ESPACE_ORGANISATION);
		Mockito.doReturn(new DeclarantDto()).when(this.declarantTransformer).modelToDto(espaceOrganisationEntity.getCreateurEspaceOrganisation());
		when(this.commentValidationEspaceRepository.findByInscriptionEspaceEntityId(Mockito.anyLong())).thenReturn(new ArrayList<CommentValidationEspaceEntity>());

		this.espaceOrganisationService.validateCreationRequest(ID_ESPACE_ORGANISATION, ID_UTILISATEUR);

		verify(commentEspaceOrganisationRepository, times(1)).save(Mockito.anyList());
		Assert.assertEquals(StatutEspaceEnum.ACCEPTEE, espaceOrganisationEntity.getStatut());
		Assert.assertEquals(ID_UTILISATEUR, espaceOrganisationEntity.getUtilisateurActionCreation().getId());
		Assert.assertTrue(inscriptionEspaceEntity.getRolesFront().contains(RoleEnumFrontOffice.ADMINISTRATEUR));
		Assert.assertTrue(inscriptionEspaceEntity.getRolesFront().contains(RoleEnumFrontOffice.PUBLICATEUR));
	}

	/**
	 * Test rejet d'un espace corporate
	 */
	@Test
	public void testRejectCreationRequest() {
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setStatut(StatutEspaceEnum.EN_TRAITEMENT);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);
		organisationEntity.setDenomination(RegistreUtils.randomString(20));
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		final DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(ID_ADMINISTRATEUR_ESPACE);
		declarantEntity.setEmail(RegistreUtils.randomString(100));
		espaceOrganisationEntity.setCreateurEspaceOrganisation(declarantEntity);

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);

		this.espaceOrganisationService.rejectCreationRequest(ID_ESPACE_ORGANISATION, ID_UTILISATEUR, RegistreUtils.randomString(500));

		Assert.assertEquals(StatutEspaceEnum.REFUSEE, espaceOrganisationEntity.getStatut());
		Assert.assertEquals(ID_UTILISATEUR, espaceOrganisationEntity.getUtilisateurActionCreation().getId());
	}

	/**
	 * Test génération d'un rejet.
	 */
	@Test
	public void testGenerateRejectMessage() {
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setStatut(StatutEspaceEnum.EN_TRAITEMENT);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);
		organisationEntity.setDenomination(RegistreUtils.randomString(20));
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		final DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(ID_ADMINISTRATEUR_ESPACE);
		declarantEntity.setEmail(RegistreUtils.randomString(100));
		espaceOrganisationEntity.setCreateurEspaceOrganisation(declarantEntity);
        org.springframework.test.util.ReflectionTestUtils.setField(espaceOrganisationService, "espaceRejetMessage5", "test");
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);

		String message = this.espaceOrganisationService.generateRejectMessage(ID_ESPACE_ORGANISATION, MotifRefusEspaceEnum.AUTRE);
		Assert.assertEquals("test",message);
	}

	/**
	 * Test de la méthode {@link EspaceOrganisationServiceImpl#getEspacesOrganisationsConfirmes()}
	 */
	@Test
	public void testGetEspacesOrganisationsConfirmes() {
		final List<EspaceOrganisationEntity> list = new ArrayList<>();
		list.add(new EspaceOrganisationEntity());

		final List<ValidationEspaceBoDto> listDto = new ArrayList<>();
		listDto.add(new ValidationEspaceBoDto());

		when(this.espaceOrganisationRepository.findByStatutOrderByIdAsc(StatutEspaceEnum.ACCEPTEE)).thenReturn(list.stream());
		when(this.validationEspaceOrganisationTransformer.modelToDto(list)).thenReturn(listDto);

		final List<ValidationEspaceBoDto> transformed = this.espaceOrganisationService.getEspacesOrganisationsConfirmes();

		Assert.assertNotNull(transformed);
		Assert.assertEquals(list.size(), listDto.size());
	}

	/**
	 * Test de récupération des espaces collaboratifs en gestion.
	 * <p>
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#getEspacesOrganisationsConfirmes(Integer, Integer)}
	 */
	@Test
	public void testPaginatedGetEspacesOrganisationConfirmed() {

		final Pageable pageable = new PageRequest(10, 25, Sort.Direction.ASC, "id");

		final List<EspaceOrganisationEntity> espaceList = new ArrayList<>();
		for (int i = 0; i < pageable.getPageSize(); i++) {
			EspaceOrganisationEntity espaceEntity = new EspaceOrganisationEntity();
			espaceEntity.setId(Long.valueOf(RandomStringUtils.randomNumeric(3)));
			OrganisationEntity organisationEntity = new OrganisationEntity();
			organisationEntity.setId(1L);
			organisationEntity.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
			espaceEntity.setOrganisation(organisationEntity);
			espaceList.add(espaceEntity);
		}

		final Page<EspaceOrganisationEntity> espacePage = new PageImpl<>(espaceList, pageable, espaceList.size() * pageable.getPageNumber() * 2);

		when(espaceOrganisationRepository.findByStatutOrderByIdAsc(StatutEspaceEnum.ACCEPTEE, pageable)).thenReturn(espacePage);
		when(espaceOrganisationRepository.findEspaceOrganisationByOrganisationIdAndStatutNotRefusee(Mockito.any())).thenReturn(new EspaceOrganisationEntity());

		final PaginatedDto<ConsultationEspaceBoSimpleDto> paginatedDto = espaceOrganisationService.getEspacesOrganisationsConfirmes(pageable.getPageNumber() + 1,
				pageable.getPageSize());

		Assert.assertEquals(paginatedDto.getDtos().size(), pageable.getPageSize());
		Assert.assertEquals(paginatedDto.getResultTotalNumber().longValue(), espacePage.getTotalElements());

	}

	/**
	 * Test de la méthode {@link EspaceOrganisationServiceImpl#switchActivationStatus(Long)}
	 */
	@Test
	public void testSwitchActivationStatus() {
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setEspaceActif(false);

		when(this.espaceOrganisationRepository.findOne(ID_ESPACE_ORGANISATION)).thenReturn(espaceOrganisationEntity);
		when(this.espaceOrganisationRepository.save(espaceOrganisationEntity)).thenReturn(espaceOrganisationEntity);

		this.espaceOrganisationService.switchActivationStatus(ID_ESPACE_ORGANISATION);

		Assert.assertNotNull(espaceOrganisationEntity);
		Assert.assertEquals(true, espaceOrganisationEntity.isEspaceActif());
	}

	/**
	 * Test de la méthode {@link EspaceOrganisationServiceImpl#getEspaceOrganisationDeclarant(Long)}
	 */
	@Test
	public void testGetEspaceOrganisationDeclarant() {
		final List<InscriptionEspaceEntity> espaceOrganisationEntities = new ArrayList<>();
		espaceOrganisationEntities.add(new InscriptionEspaceEntity());

		final List<InscriptionEspaceDto> espaceDtos = new ArrayList<>();
		espaceDtos.add(new InscriptionEspaceDto());

		when(this.inscriptionEspaceRepository.findAllByEspaceOrganisationId(ID_ESPACE_ORGANISATION)).thenReturn(espaceOrganisationEntities);

		when(this.inscriptionEspaceTransformer.modelToDto(espaceOrganisationEntities)).thenReturn(espaceDtos);

		final List<InscriptionEspaceDto> returnedList = this.espaceOrganisationService.getEspaceOrganisationDeclarant(ID_ESPACE_ORGANISATION);

		Assert.assertNotNull(espaceOrganisationEntities);
		Assert.assertNotNull(espaceDtos);
		Assert.assertNotNull(returnedList);

		Assert.assertEquals(espaceOrganisationEntities.size(), returnedList.size());
	}

	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateDirigeants(EspaceOrganisationDto)}
	 */
	@Ignore
	@Test
	public void testUpdateDirigeants() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		
		final List<DirigeantDto> dirigeantDtos = new ArrayList<>();
		final DirigeantDto dirigeantDto = new DirigeantDto();
		dirigeantDto.setId(2L);
		dirigeantDto.setNom(RegistreUtils.randomString(10));
		dirigeantDto.setPrenom(RegistreUtils.randomString(20));
		dirigeantDtos.add(dirigeantDto);
		espaceOrganisationDto.setDirigeants(dirigeantDtos);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).save(espaceOrganisationEntity);		
		Mockito.doReturn(espaceOrganisationDto).when(this.espaceOrganisationTransformer).modelToDto(espaceOrganisationEntity);

		EspaceOrganisationDto retourDto = this.espaceOrganisationService.updateDirigeants(espaceOrganisationDto);
		
		Assert.assertEquals(espaceOrganisationDto.getId(), retourDto.getId());
		Assert.assertEquals(espaceOrganisationDto.getDirigeants(), retourDto.getDirigeants());
	}

	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateDirigeants(EspaceOrganisationDto)}
	 *  cas espaceOrganisationEntity pas trouvée
	 */
	@Test
	public void testUpdateDirigeantsEntityNull() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);

		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		
		try {
			this.espaceOrganisationService.updateDirigeants(espaceOrganisationDto);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateDonnees(EspaceOrganisationDto)}
	 */
	@Ignore
	@Test
	public void testUpdateDonnees() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		espaceOrganisationEntity.setChiffreAffaire(RegistreUtils.randomLong(12));
		espaceOrganisationEntity.setAnneeChiffreAffaire(2017);
		espaceOrganisationEntity.setnPersonnesEmployees(999);

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);		
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).save(espaceOrganisationEntity);		
		Mockito.doReturn(espaceOrganisationDto).when(this.espaceOrganisationTransformer).modelToDto(espaceOrganisationEntity);

		EspaceOrganisationDto retourDto = this.espaceOrganisationService.updateDonnees(espaceOrganisationDto);
		
		Assert.assertEquals(espaceOrganisationDto.getId(), retourDto.getId());
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateDonnees(EspaceOrganisationDto)}
	 * cas espaceOrganisationEntity pas trouvée
	 */
	@Test
	public void testUpdateDonneesEntityNull() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);

		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		
		try {
			 this.espaceOrganisationService.updateDonnees(espaceOrganisationDto);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateLocalisation(EspaceOrganisationDto)}
	 */
	@Ignore
	@Test
	public void testUpdateLocalisation() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationDto.setAdresse("Adresse espace orga");
		espaceOrganisationDto.setCodePostal("code postal");
		espaceOrganisationDto.setVille("ville");
		espaceOrganisationDto.setPays("France");
		espaceOrganisationDto.setNonPublierMonAdressePhysique(true);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		espaceOrganisationEntity.setAdresse(RegistreUtils.randomString(500));
		espaceOrganisationEntity.setCodePostal(RegistreUtils.randomString(10));
		espaceOrganisationEntity.setVille(RegistreUtils.randomString(20));
		espaceOrganisationEntity.setPays(RegistreUtils.randomString(20));

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).save(espaceOrganisationEntity);
		Mockito.doReturn(espaceOrganisationDto).when(this.espaceOrganisationTransformer).modelToDto(espaceOrganisationEntity);

		EspaceOrganisationDto rertourDto = this.espaceOrganisationService.updateLocalisation(espaceOrganisationDto);
		
		Assert.assertEquals(espaceOrganisationDto.getId(), rertourDto.getId());
		Assert.assertEquals(espaceOrganisationDto.getAdresse(), rertourDto.getAdresse());
		Assert.assertEquals(espaceOrganisationDto.getCodePostal(), rertourDto.getCodePostal());
		Assert.assertEquals(espaceOrganisationDto.getVille(), rertourDto.getVille());
		Assert.assertEquals(espaceOrganisationDto.getPays(), rertourDto.getPays());
		Assert.assertEquals(espaceOrganisationDto.getNonPublierMonAdressePhysique(), rertourDto.getNonPublierMonAdressePhysique());
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateLocalisation(EspaceOrganisationDto)}
	 * cas espaceOrganisationEntity pas trouvée
	 */
	@Test
	public void testUpdateLocalisationEntityNull() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationDto.setAdresse("Adresse espace orga");
		espaceOrganisationDto.setCodePostal("code postal");
		espaceOrganisationDto.setVille("ville");
		espaceOrganisationDto.setPays("France");
		espaceOrganisationDto.setNonPublierMonAdressePhysique(true);

		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);

		try {
			this.espaceOrganisationService.updateLocalisation(espaceOrganisationDto);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateInfosContact(EspaceOrganisationDto)}
	 */
	@Ignore
	@Test
	public void testUpdateInfosContact() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationDto.setTelephoneDeContact("0130405060");
		espaceOrganisationDto.setNonPublierMonTelephoneDeContact(false);
		espaceOrganisationDto.setEmailDeContact("nouveau.mail@hatvp.fr");
		espaceOrganisationDto.setNonPublierMonAdresseEmail(false);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		espaceOrganisationEntity.setTelephoneDeContact(RegistreUtils.randomString(15));
		espaceOrganisationEntity.setNonPublierMonTelephoneDeContact(true);
		espaceOrganisationEntity.setEmailDeContact(RegistreUtils.randomString(50));
		espaceOrganisationEntity.setNonPublierMonAdresseEmail(true);

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).save(espaceOrganisationEntity);
		Mockito.doReturn(espaceOrganisationDto).when(this.espaceOrganisationTransformer).modelToDto(espaceOrganisationEntity);

		EspaceOrganisationDto rertourDto = this.espaceOrganisationService.updateInfosContact(espaceOrganisationDto);
		
		Assert.assertEquals(espaceOrganisationDto.getId(), rertourDto.getId());
		Assert.assertEquals(espaceOrganisationDto.getEmailDeContact(), rertourDto.getEmailDeContact());
		Assert.assertEquals(espaceOrganisationDto.getTelephoneDeContact(), rertourDto.getTelephoneDeContact());
		Assert.assertEquals(espaceOrganisationDto.getNonPublierMonAdresseEmail(), rertourDto.getNonPublierMonAdresseEmail());
		Assert.assertEquals(espaceOrganisationDto.getNonPublierMonTelephoneDeContact(), rertourDto.getNonPublierMonTelephoneDeContact());
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateInfosContact(EspaceOrganisationDto)}
	 * cas espaceOrganisationEntity pas trouvée
	 */
	@Test
	public void testUpdateInfosContactEntityNull() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationDto.setTelephoneDeContact("0130405060");
		espaceOrganisationDto.setNonPublierMonTelephoneDeContact(false);
		espaceOrganisationDto.setEmailDeContact("nouveau.mail@hatvp.fr");
		espaceOrganisationDto.setNonPublierMonAdresseEmail(false);

		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		
		try {
			this.espaceOrganisationService.updateInfosContact(espaceOrganisationDto);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateWeb(EspaceOrganisationDto)}
	 * cas espaceOrganisationEntity pas trouvée
	 */
	@Test
	public void testUpdateWebEntityNull() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationDto.setLienSiteWeb("URL");
		espaceOrganisationDto.setLienListeTiers("lien");
		espaceOrganisationDto.setLienPageFacebook("facebook");
		espaceOrganisationDto.setLienPageLinkedin("linkedin");
		espaceOrganisationDto.setLienPageTwitter("twitter");
		
		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);

		try {
			 this.espaceOrganisationService.updateWeb(espaceOrganisationDto);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateWeb(EspaceOrganisationDto)}
	 * cas espaceOrganisationEntity pas trouvée
	 */
	@Ignore
	@Test
	public void testUpdateWeb() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationDto.setLienSiteWeb("URL");
		espaceOrganisationDto.setLienListeTiers("lien");
		espaceOrganisationDto.setLienPageFacebook("facebook");
		espaceOrganisationDto.setLienPageLinkedin("linkedin");
		espaceOrganisationDto.setLienPageTwitter("twitter");

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		espaceOrganisationEntity.setLienSiteWeb(RegistreUtils.randomString(100));
		espaceOrganisationEntity.setLienPageTwitter(RegistreUtils.randomString(100));
		espaceOrganisationEntity.setLienPageFacebook(RegistreUtils.randomString(100));
		espaceOrganisationEntity.setLienPageLinkedin(RegistreUtils.randomString(100));

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).save(espaceOrganisationEntity);
		Mockito.doReturn(espaceOrganisationDto).when(this.espaceOrganisationTransformer).modelToDto(espaceOrganisationEntity);

		EspaceOrganisationDto rertourDto = this.espaceOrganisationService.updateWeb(espaceOrganisationDto);
		
		Assert.assertEquals(espaceOrganisationDto.getId(), rertourDto.getId());
		Assert.assertEquals(espaceOrganisationDto.getLienSiteWeb(), rertourDto.getLienSiteWeb());
		Assert.assertEquals(espaceOrganisationDto.getLienListeTiers(), rertourDto.getLienListeTiers());
		Assert.assertEquals(espaceOrganisationDto.getLienPageFacebook(), rertourDto.getLienPageFacebook());
		Assert.assertEquals(espaceOrganisationDto.getLienPageLinkedin(), rertourDto.getLienPageLinkedin());
		Assert.assertEquals(espaceOrganisationDto.getLienPageTwitter(), rertourDto.getLienPageTwitter());
	}

	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateProfileOrga(EspaceOrganisationDto)}
	 * cas nom usage null
	 */
	@Ignore
	@Test
	public void testUpdateProfileOrgaNomUsageNull() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationDto.setCategorieOrganisation(TypeOrganisationEnum.AVIND);
		espaceOrganisationDto.setNomUsage(null);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setOrganisation(organisationEntity);
		espaceOrganisationEntity.setCategorieOrganisation(TypeOrganisationEnum.ASSOC);		

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).save(espaceOrganisationEntity);
		Mockito.doReturn(espaceOrganisationDto).when(this.espaceOrganisationTransformer).modelToDto(espaceOrganisationEntity);

		EspaceOrganisationDto rertourDto = this.espaceOrganisationService.updateProfileOrga(espaceOrganisationDto, false);
		
		Assert.assertEquals(espaceOrganisationDto.getId(), rertourDto.getId());
		Assert.assertEquals(espaceOrganisationDto.getCategorieOrganisation(), rertourDto.getCategorieOrganisation());
		Assert.assertNull(rertourDto.getNomUsage());
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateProfileOrga(EspaceOrganisationDto)}
	 * cas nom usage minuscule avec espace
	 */
	@Ignore
	@Test
	public void testUpdateProfileOrgaNomUsageNotNull() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationDto.setCategorieOrganisation(TypeOrganisationEnum.AVIND);
		espaceOrganisationDto.setNomUsage("nom usage");

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setOrganisation(organisationEntity);
		espaceOrganisationEntity.setCategorieOrganisation(TypeOrganisationEnum.ASSOC);
		espaceOrganisationEntity.setNomUsage("");

		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).save(espaceOrganisationEntity);
		
		Mockito.doReturn(null).when(this.dirigeantTransformer).modelToDto(espaceOrganisationEntity.getDirigeants());
		Mockito.doReturn(null).when(this.clientTransformer).modelToDto(espaceOrganisationEntity.getClients());
		Mockito.doReturn(null).when(this.associationProTransformer).modelToDto(espaceOrganisationEntity.getOrganisationProfessionnelles());
		Mockito.doReturn(null).when(this.collaborateurTransformer).modelToDto(espaceOrganisationEntity.getListeDesCollaborateurs());


		EspaceOrganisationDto rertourDto = this.espaceOrganisationService.updateProfileOrga(espaceOrganisationDto, false);
		

	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateProfileOrga(EspaceOrganisationDto)}
	 * cas espaceOrganisationEntity pas trouvée
	 */
	@Test
	public void testUpdateProfileOrgaEntityNull() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationDto.setCategorieOrganisation(TypeOrganisationEnum.AVIND);
		espaceOrganisationDto.setNomUsage("nom usage");

		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);

		try {
			this.espaceOrganisationService.updateProfileOrga(espaceOrganisationDto, false);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateProfileOrgaBack(EspaceOrganisationDto)} (EspaceOrganisationDto)}
	 */
	@Ignore
	@Test
	public void testUpdateProfileOrgaBack() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		espaceOrganisationEntity.setNomUsage(NOM_USAGE_TEST);
		espaceOrganisationEntity.setFinExerciceFiscal(DATE_DE_CLOTURE);
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).save(espaceOrganisationEntity);
		Mockito.doReturn(espaceOrganisationDto).when(this.espaceOrganisationTransformer).modelToDto(espaceOrganisationEntity);

		this.espaceOrganisationService.updateProfileOrga(espaceOrganisationDto, true);
	}
	
	/**
	 * Test de la méthode: {@link fr.hatvp.registre.business.EspaceOrganisationService#updateProfileOrgaBack(EspaceOrganisationDto)} (EspaceOrganisationDto)}
	 * cas espaceOrganisationEntity pas trouvée
	 */
	@Test
	public void testUpdateProfileOrgaBackEntityNull() throws Exception {
		final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);

		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);

		try {
			 this.espaceOrganisationService.updateProfileOrga(espaceOrganisationDto, true);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}	
	}

	/**
	 * Test Succès de la méthode: {@link fr.hatvp.registre.business .EspaceOrganisationService#updateEspaceOrganisationEnCoursDeValidation(EspaceOrganisationDto, Long)}
	 */
	@Ignore
	@Test
	public void testSetOrganisationData() throws Exception {
		final ValidationEspaceDetailBoDto espaceOrganisationDto = new ValidationEspaceDetailBoDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationDto.setNationalId(ID_NATIONAL_TEST);
		espaceOrganisationDto.setAdresse(RegistreUtils.randomString(500));
		espaceOrganisationDto.setDenomination(RegistreUtils.randomString(30));

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);
		organisationEntity.setId(espaceOrganisationDto.getId());
		organisationEntity.setDenomination(espaceOrganisationDto.getDenomination());

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		Mockito.doReturn(organisationEntity).when(this.validationEspaceOrganisationDetailBoTransformer).dtoToModel(espaceOrganisationDto);
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		Mockito.doReturn(espaceOrganisationEntity).when(this.validationEspaceOrganisationDetailBoTransformer).dtoToModel(espaceOrganisationDto);

		ValidationEspaceDetailBoDto rertourDto = this.espaceOrganisationService.updateEspaceOrganisationEnCoursDeValidation(espaceOrganisationDto, null);
	}
	
	/**
	 * Test Succès de la méthode: {@link fr.hatvp.registre.business .EspaceOrganisationService#updateEspaceOrganisationEnCoursDeValidation(EspaceOrganisationDto, Long)}
	 * cas espaceOrganisationEntity pas trouvée
	 */
	@Test
	public void testSetOrganisationDataEntityNull() throws Exception {
		final ValidationEspaceDetailBoDto espaceOrganisationDto = new ValidationEspaceDetailBoDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationDto.setNationalId(ID_NATIONAL_TEST);
		espaceOrganisationDto.setAdresse(RegistreUtils.randomString(500));
		espaceOrganisationDto.setDenomination(RegistreUtils.randomString(30));

		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);

		try {
			this.espaceOrganisationService.updateEspaceOrganisationEnCoursDeValidation(espaceOrganisationDto, null);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}

	/**
	 * Test de la méthode {@link EspaceOrganisationServiceImpl#getAllEspaceOrganisationInformations(Long)}
	 */
	@Test
	public void testGetAllEspaceOrganisationInformations() {

		final EspaceOrganisationEntity entity = this.getEspaceOrganisation();
		final EspaceOrganisationDto dto = this.getEspaceOrganisationDto(entity);

		when(this.publicationRepository.findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(entity.getId(), StatutPublicationEnum.DEPUBLIEE)).thenReturn(Optional.empty());
		when(this.espaceOrganisationRepository.findOne(ID_ESPACE_ORGANISATION)).thenReturn(entity);
		when(this.espaceOrganisationTransformer.modelToDto(entity)).thenReturn(dto);
		// when(this.publicationEspaceTransformer.espaceToPublication(espaceOrganisationDto))
		final EspaceOrganisationDto returnedDto = this.espaceOrganisationService.getAllEspaceOrganisationInformations(ID_ESPACE_ORGANISATION);

		Assert.assertNotNull(returnedDto);
		Assert.assertEquals(entity.getId(), returnedDto.getId());
		Assert.assertEquals(entity.getVersion(), returnedDto.getVersion());
		Assert.assertEquals(entity.getChiffreAffaire(), returnedDto.getChiffreAffaire());

		Assert.assertEquals(entity.getSecteursActivites().split(",").length, returnedDto.getListSecteursActivites().size());
		Assert.assertEquals(entity.getNiveauIntervention().split(",").length, returnedDto.getListNiveauIntervention().size());

		Assert.assertEquals(entity.getAnneeChiffreAffaire(), returnedDto.getAnneeChiffreAffaire());

		Assert.assertEquals(entity.getAdresse(), returnedDto.getAdresse());
		Assert.assertEquals(entity.getVille(), returnedDto.getVille());
		Assert.assertEquals(entity.getCodePostal(), returnedDto.getCodePostal());
		Assert.assertEquals(entity.getPays(), returnedDto.getPays());
	}

	/**
	 * Création d'un nouvel Espace Organisation.
	 *
	 * @return entité espace organisation
	 */
	private EspaceOrganisationEntity getEspaceOrganisation() {
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();

		espaceOrganisationEntity.setId(RegistreUtils.randomLong(1));
		espaceOrganisationEntity.setVersion(1);
		espaceOrganisationEntity.setCreationDate(new Date());
		espaceOrganisationEntity.setNonDeclarationTiers(true);
		espaceOrganisationEntity.setNonDeclarationOrgaAppartenance(false);
		espaceOrganisationEntity.setEspaceActif(true);

		espaceOrganisationEntity.setChiffreAffaire(RegistreUtils.randomLong(20));

		espaceOrganisationEntity.setSecteursActivites(RegistreUtils.randomString(20));
		espaceOrganisationEntity.setNiveauIntervention(RegistreUtils.randomString(20));

		espaceOrganisationEntity.setAnneeChiffreAffaire(RegistreUtils.randomLong(4).intValue());
		espaceOrganisationEntity.setEmailDeContact(RegistreUtils.randomString(30));
		espaceOrganisationEntity.setTelephoneDeContact(RegistreUtils.randomString(15));
		espaceOrganisationEntity.setNonPublierMonAdresseEmail(false);
		espaceOrganisationEntity.setNonPublierMonTelephoneDeContact(true);
		espaceOrganisationEntity.setNonPublierMonAdressePhysique(true);
		final DirigeantEntity dirigeantEntity = new DirigeantEntity();
		dirigeantEntity.setEspaceOrganisation(espaceOrganisationEntity);
		dirigeantEntity.setVersion(1);
		dirigeantEntity.setId(RegistreUtils.randomLong(1));
		dirigeantEntity.setNom(RegistreUtils.randomString(20));
		dirigeantEntity.setPrenom(RegistreUtils.randomString(20));
		espaceOrganisationEntity.getDirigeants().add(dirigeantEntity);

		espaceOrganisationEntity.setAdresse(RegistreUtils.randomString(20));
		espaceOrganisationEntity.setCodePostal(RegistreUtils.randomString(5));
		espaceOrganisationEntity.setPays(RegistreUtils.randomString(5));
		espaceOrganisationEntity.setVille(RegistreUtils.randomString(5));
		espaceOrganisationEntity.setNonDeclarationOrgaAppartenance(false);

		return espaceOrganisationEntity;
	}
	

	/**
	 * Créer un {@link EspaceOrganisationDto}
	 *
	 * @return EspaceOrganisationDto
	 */
	private EspaceOrganisationDto getEspaceOrganisationDto(final EspaceOrganisationEntity entity) {
		final EspaceOrganisationDto dto = new EspaceOrganisationDto();
		dto.setId(entity.getId());
		dto.setVersion(entity.getVersion());
		dto.setCreationDate(entity.getCreationDate());
		dto.setEspaceActif(entity.isEspaceActif());

		dto.setChiffreAffaire(entity.getChiffreAffaire());

		dto.getListSecteursActivites().add(SecteurActiviteEnum.AGRI);
		dto.getListNiveauIntervention().add(NiveauInterventionEnum.EUROPEEN);

		dto.setAnneeChiffreAffaire(entity.getAnneeChiffreAffaire());
		dto.setEmailDeContact(entity.getEmailDeContact());
		dto.setTelephoneDeContact(entity.getTelephoneDeContact());
		dto.setNonPublierMonAdresseEmail(entity.getNonPublierMonAdresseEmail());
		dto.setNonPublierMonTelephoneDeContact(entity.getNonPublierMonTelephoneDeContact());
		dto.setNonPublierMonAdressePhysique(entity.getNonPublierMonAdressePhysique());
		dto.setAdresse(entity.getAdresse());
		dto.setCodePostal(entity.getCodePostal());
		dto.setPays(entity.getPays());
		dto.setVille(entity.getVille());
		dto.setNonDeclarationOrgaAppartenance(entity.isNonDeclarationOrgaAppartenance());
		dto.setNonDeclarationTiers(entity.isNonDeclarationTiers());

		return dto;
	}
	
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#getEspaceOrganisationEnCoursDeValidation(Long id)}
	 * cas pas d'entité trouvée
	 * public ValidationEspaceDetailBoDto getEspaceOrganisationEnCoursDeValidation(Long id) {
	 */
	@Test
	public void getEspaceOrganisationEnCoursDeValidationEntityNotFoundException() {
		
		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);		
		try {
			this.espaceOrganisationService.getEspaceOrganisationEnCoursDeValidation(ID_ESPACE_ORGANISATION);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#getEspaceOrganisationEnCoursDeValidation(Long id)}
	 * cas entité StatutEspaceEnum.ACCEPTEE
	 * public ValidationEspaceDetailBoDto getEspaceOrganisationEnCoursDeValidation(Long id) {
	 */
	@Test
	public void getEspaceOrganisationEnCoursDeValidationEntityAcceptee() {
		EspaceOrganisationEntity espace = new EspaceOrganisationEntity();
		espace.setId(ID_ESPACE_ORGANISATION);
		espace.setStatut(StatutEspaceEnum.ACCEPTEE);
		
		Mockito.doReturn(espace).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);		
		try {
			this.espaceOrganisationService.getEspaceOrganisationEnCoursDeValidation(ID_ESPACE_ORGANISATION);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.ESPACE_ORGANISATION_DEJA_VALIDE_OU_REFUSE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#getEspaceOrganisationEnCoursDeValidation(Long id)}
	 * cas entité OK
	 * public ValidationEspaceDetailBoDto getEspaceOrganisationEnCoursDeValidation(Long id) {
	 */
	@Test
	public void getEspaceOrganisationEnCoursDeValidationEntityOK() {
		EspaceOrganisationEntity espace = new EspaceOrganisationEntity();
		espace.setId(ID_ESPACE_ORGANISATION);
		espace.setStatut(StatutEspaceEnum.NOUVEAU);
		
		Mockito.doReturn(espace).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		ValidationEspaceDetailBoDto retour = this.espaceOrganisationService.getEspaceOrganisationEnCoursDeValidation(ID_ESPACE_ORGANISATION);
		
		Assert.assertNull(retour);
		
	}
	
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#generateDemandeComplementMessage(final Long id, final MotifComplementEspaceEnum motif)}
	 */
	@Test
	public void testGenerateDemandeComplementMessage() {
		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ID_ESPACE_ORGANISATION);
		espaceOrganisationEntity.setStatut(StatutEspaceEnum.EN_TRAITEMENT);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setId(ID_ORGANISATION);
		organisationEntity.setDenomination(RegistreUtils.randomString(20));
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		final DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(ID_ADMINISTRATEUR_ESPACE);
		declarantEntity.setEmail(RegistreUtils.randomString(100));
		espaceOrganisationEntity.setCreateurEspaceOrganisation(declarantEntity);
        org.springframework.test.util.ReflectionTestUtils.setField(espaceOrganisationService, "espaceComplementMessageMandat", "test");
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);

		String message = this.espaceOrganisationService.generateDemandeComplementMessage(ID_ESPACE_ORGANISATION, MotifComplementEspaceEnum.ID_CONFORME_MANDAT_NON_CONFORME);
		Assert.assertEquals("test",message);
	}
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#updateActivites(final EspaceOrganisationDto dto)}
	 */
	@Test
	public void testUpdateActivitesExceptionEspace() {
		
		EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
		espaceOrganisationDto.setId(ID_ESPACE_ORGANISATION);
		
		Mockito.doReturn(null).when(this.espaceOrganisationRepository).findOne(ID_ESPACE_ORGANISATION);
		
		try {
			this.espaceOrganisationService.updateActivites(espaceOrganisationDto);
		} catch (EntityNotFoundException ex) {
			assertThat(ErrorMessageEnum.ESPACE_ORGANISATION_INTROUVABLE.getMessage()).isEqualTo(ex.getMessage());
		}
	}
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#parametrageJourBlacklistEnFonctiontRaisonBlacklistage(ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto, Long exerciceId, PublicationRelanceEntity relance) }
	 * publicationExercice et publicationActivite not null
	 * @throws ParseException 
	 */
	@Test
	 public void testParametrageJourBlacklistEnFonctiontRaisonBlacklistageCasFAAvecPublicationExerciceEtPublicationActivite() throws ParseException {
		Long exerciceId = 1L;
		
		PublicationRelanceEntity relance = new PublicationRelanceEntity();
		relance.setCreationDate(LocalDateTime.of(2020,3, 29, 19, 30, 40));
		relance.setType(PublicationRelanceTypeEnum.FA);
		
		ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto = new ExerciceComptableBlacklistedDto();
		exerciceComptableBlacklistedDto.setNbJoursBlacklistFA(0);
		exerciceComptableBlacklistedDto.setNbJoursBlacklistMA(0);
		exerciceComptableBlacklistedDto.setRaisonBlacklistage(null);
		
		PublicationExerciceEntity publicationExercice = new PublicationExerciceEntity();
		publicationExercice.setCreationDate(new SimpleDateFormat("dd/MM/yyyy").parse("15/04/2020"));
		
		PublicationActiviteEntity publicationActivite = new PublicationActiviteEntity();
		publicationActivite.setCreationDate(new SimpleDateFormat("dd/MM/yyyy").parse("1/04/2020"));
		
		Mockito.doReturn(publicationExercice).when(this.publicationExerciceRepository).findFirstByExerciceComptableIdAndStatutNotOrderByIdDesc(exerciceId,StatutPublicationEnum.NON_PUBLIEE);
		Mockito.doReturn(publicationActivite).when(this.publicationActiviteRepository).getFirstPublicationActivite(exerciceId);
		
		ExerciceComptableBlacklistedDto retourExComptableBLDto =  this.espaceOrganisationService.parametrageJourBlacklistEnFonctiontRaisonBlacklistage(exerciceComptableBlacklistedDto, exerciceId, relance); 
		 
		Assert.assertEquals(relance.getType().getLabel(),retourExComptableBLDto.getRaisonBlacklistage());
		assertThat(retourExComptableBLDto.getNbJoursBlacklistFA()).isEqualTo(3);
		assertThat(retourExComptableBLDto.getNbJoursBlacklistMA()).isEqualTo(0);
	 }
	
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#parametrageJourBlacklistEnFonctiontRaisonBlacklistage(ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto, Long exerciceId, PublicationRelanceEntity relance) }
	 * publicationActivite null
	 * @throws ParseException 
	 */
	@Test
	 public void testParametrageJourBlacklistEnFonctiontRaisonBlacklistageCasFAAvecPublicationExerciceEtSansPublicationActivite() throws ParseException {
		Long exerciceId = 1L;		
		
		PublicationRelanceEntity relance = new PublicationRelanceEntity();
		relance.setCreationDate(LocalDateTime.of(2020,3, 29, 19, 30, 40));
		relance.setType(PublicationRelanceTypeEnum.FA);
		
		int nbJourEntreRelanceEtDateDuJOur = (int)ChronoUnit.DAYS.between(relance.getCreationDate().toLocalDate(), LocalDateTime.now().toLocalDate());
		
		ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto = new ExerciceComptableBlacklistedDto();
		exerciceComptableBlacklistedDto.setNbJoursBlacklistFA(0);
		exerciceComptableBlacklistedDto.setNbJoursBlacklistMA(0);
		exerciceComptableBlacklistedDto.setRaisonBlacklistage(null);
		
		PublicationExerciceEntity publicationExercice = new PublicationExerciceEntity();
		publicationExercice.setCreationDate(new SimpleDateFormat("dd/MM/yyyy").parse("15/04/2020"));
		
		Mockito.doReturn(publicationExercice).when(this.publicationExerciceRepository).findFirstByExerciceComptableIdAndStatutNotOrderByIdDesc(exerciceId,StatutPublicationEnum.NON_PUBLIEE);
		Mockito.doReturn(null).when(this.publicationActiviteRepository).getFirstPublicationActivite(exerciceId);
		
		ExerciceComptableBlacklistedDto retourExComptableBLDto =  this.espaceOrganisationService.parametrageJourBlacklistEnFonctiontRaisonBlacklistage(exerciceComptableBlacklistedDto, exerciceId, relance); 
		 
		Assert.assertEquals(relance.getType().getLabel(),retourExComptableBLDto.getRaisonBlacklistage());
		assertThat(retourExComptableBLDto.getNbJoursBlacklistFA()).isEqualTo(nbJourEntreRelanceEtDateDuJOur);
		assertThat(retourExComptableBLDto.getNbJoursBlacklistMA()).isEqualTo(0);
	 }
	
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#parametrageJourBlacklistEnFonctiontRaisonBlacklistage(ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto, Long exerciceId, PublicationRelanceEntity relance) }
	 * publicationExercice et publicationActivite not null
	 * @throws ParseException 
	 */
	@Test
	 public void testParametrageJourBlacklistEnFonctiontRaisonBlacklistageCasMAAvecPublicationExerciceEtPublicationActivite() throws ParseException {
		Long exerciceId = 1L;
		
		PublicationRelanceEntity relance = new PublicationRelanceEntity();
		relance.setCreationDate(LocalDateTime.of(2020,3, 29, 19, 30, 40));
		relance.setType(PublicationRelanceTypeEnum.MA);
		
		ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto = new ExerciceComptableBlacklistedDto();
		exerciceComptableBlacklistedDto.setNbJoursBlacklistFA(0);
		exerciceComptableBlacklistedDto.setNbJoursBlacklistMA(0);
		exerciceComptableBlacklistedDto.setRaisonBlacklistage(null);
		
		PublicationExerciceEntity publicationExercice = new PublicationExerciceEntity();
		publicationExercice.setCreationDate(new SimpleDateFormat("dd/MM/yyyy").parse("15/04/2020"));
		
		PublicationActiviteEntity publicationActivite = new PublicationActiviteEntity();
		publicationActivite.setCreationDate(new SimpleDateFormat("dd/MM/yyyy").parse("1/04/2020"));
		
		Mockito.doReturn(publicationExercice).when(this.publicationExerciceRepository).findFirstByExerciceComptableIdAndStatutNotOrderByIdDesc(exerciceId,StatutPublicationEnum.NON_PUBLIEE);
		Mockito.doReturn(publicationActivite).when(this.publicationActiviteRepository).getFirstPublicationActivite(exerciceId);
		
		ExerciceComptableBlacklistedDto retourExComptableBLDto =  this.espaceOrganisationService.parametrageJourBlacklistEnFonctiontRaisonBlacklistage(exerciceComptableBlacklistedDto, exerciceId, relance); 
		 
		Assert.assertEquals(relance.getType().getLabel(),retourExComptableBLDto.getRaisonBlacklistage());
		assertThat(retourExComptableBLDto.getNbJoursBlacklistFA()).isEqualTo(0);
		assertThat(retourExComptableBLDto.getNbJoursBlacklistMA()).isEqualTo(17);
	 }
	
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#parametrageJourBlacklistEnFonctiontRaisonBlacklistage(ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto, Long exerciceId, PublicationRelanceEntity relance) }
	 * publicationExercice et publicationActivite null
	 * @throws ParseException 
	 */
	@Test
	 public void testParametrageJourBlacklistEnFonctiontRaisonBlacklistageCasMASansPublicationExerciceEtSansPublicationActivite() throws ParseException {
		Long exerciceId = 1L;		
		
		PublicationRelanceEntity relance = new PublicationRelanceEntity();
		relance.setCreationDate(LocalDateTime.of(2020,3, 29, 19, 30, 40));
		relance.setType(PublicationRelanceTypeEnum.MA);
		
		int nbJourEntreRelanceEtDateDuJOur = (int)ChronoUnit.DAYS.between(relance.getCreationDate().toLocalDate(), LocalDateTime.now().toLocalDate());
		
		ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto = new ExerciceComptableBlacklistedDto();
		exerciceComptableBlacklistedDto.setNbJoursBlacklistFA(0);
		exerciceComptableBlacklistedDto.setNbJoursBlacklistMA(0);
		exerciceComptableBlacklistedDto.setRaisonBlacklistage(null);
		
		Mockito.doReturn(null).when(this.publicationExerciceRepository).findFirstByExerciceComptableIdAndStatutNotOrderByIdDesc(exerciceId,StatutPublicationEnum.NON_PUBLIEE);
		Mockito.doReturn(null).when(this.publicationActiviteRepository).getFirstPublicationActivite(exerciceId);
		
		ExerciceComptableBlacklistedDto retourExComptableBLDto =  this.espaceOrganisationService.parametrageJourBlacklistEnFonctiontRaisonBlacklistage(exerciceComptableBlacklistedDto, exerciceId, relance); 
		 
		Assert.assertEquals(relance.getType().getLabel(),retourExComptableBLDto.getRaisonBlacklistage());
		assertThat(retourExComptableBLDto.getNbJoursBlacklistFA()).isEqualTo(0);
		assertThat(retourExComptableBLDto.getNbJoursBlacklistMA()).isEqualTo(nbJourEntreRelanceEtDateDuJOur);
	 }
	
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#parametrageJourBlacklistEnFonctiontRaisonBlacklistage(ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto, Long exerciceId, PublicationRelanceEntity relance) }
	 * publicationExercice et publicationActivite not null
	 * publication exercice after publication activité
	 * @throws ParseException 
	 */
	@Test
	 public void testParametrageJourBlacklistEnFonctiontRaisonBlacklistageCasFAMAAvecPublicationExerciceEtPublicationActiviteBefore() throws ParseException {
		Long exerciceId = 1L;
		
		PublicationRelanceEntity relance = new PublicationRelanceEntity();
		relance.setCreationDate(LocalDateTime.of(2020,3, 29, 19, 30, 40));
		relance.setType(PublicationRelanceTypeEnum.FAMA);
		
		ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto = new ExerciceComptableBlacklistedDto();
		exerciceComptableBlacklistedDto.setNbJoursBlacklistFA(0);
		exerciceComptableBlacklistedDto.setNbJoursBlacklistMA(0);
		exerciceComptableBlacklistedDto.setRaisonBlacklistage(null);
		
		PublicationExerciceEntity publicationExercice = new PublicationExerciceEntity();
		publicationExercice.setCreationDate(new SimpleDateFormat("dd/MM/yyyy").parse("15/04/2020"));
		
		PublicationActiviteEntity publicationActivite = new PublicationActiviteEntity();
		publicationActivite.setCreationDate(new SimpleDateFormat("dd/MM/yyyy").parse("1/04/2020"));
		
		Mockito.doReturn(publicationExercice).when(this.publicationExerciceRepository).findFirstByExerciceComptableIdAndStatutNotOrderByIdDesc(exerciceId,StatutPublicationEnum.NON_PUBLIEE);
		Mockito.doReturn(publicationActivite).when(this.publicationActiviteRepository).getFirstPublicationActivite(exerciceId);
		
		ExerciceComptableBlacklistedDto retourExComptableBLDto =  this.espaceOrganisationService.parametrageJourBlacklistEnFonctiontRaisonBlacklistage(exerciceComptableBlacklistedDto, exerciceId, relance); 
		 
		Assert.assertEquals(relance.getType().getLabel(),retourExComptableBLDto.getRaisonBlacklistage());
		assertThat(retourExComptableBLDto.getNbJoursBlacklistFA()).isEqualTo(17);
		assertThat(retourExComptableBLDto.getNbJoursBlacklistMA()).isEqualTo(17);
	 }
	
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#parametrageJourBlacklistEnFonctiontRaisonBlacklistage(ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto, Long exerciceId, PublicationRelanceEntity relance) }
	 * publicationExercice et publicationActivite not null
	 * publication exercice before publication activité
	 * @throws ParseException 
	 */
	@Test
	 public void testParametrageJourBlacklistEnFonctiontRaisonBlacklistageCasFAMAAvecPublicationExerciceEtPublicationActiviteAfter() throws ParseException {
		Long exerciceId = 1L;
		
		PublicationRelanceEntity relance = new PublicationRelanceEntity();
		relance.setCreationDate(LocalDateTime.of(2020,3, 29, 19, 30, 40));
		relance.setType(PublicationRelanceTypeEnum.FAMA);
		
		ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto = new ExerciceComptableBlacklistedDto();
		exerciceComptableBlacklistedDto.setNbJoursBlacklistFA(0);
		exerciceComptableBlacklistedDto.setNbJoursBlacklistMA(0);
		exerciceComptableBlacklistedDto.setRaisonBlacklistage(null);
		
		PublicationExerciceEntity publicationExercice = new PublicationExerciceEntity();
		publicationExercice.setCreationDate(new SimpleDateFormat("dd/MM/yyyy").parse("01/04/2020"));
		
		PublicationActiviteEntity publicationActivite = new PublicationActiviteEntity();
		publicationActivite.setCreationDate(new SimpleDateFormat("dd/MM/yyyy").parse("15/04/2020"));
		
		Mockito.doReturn(publicationExercice).when(this.publicationExerciceRepository).findFirstByExerciceComptableIdAndStatutNotOrderByIdDesc(exerciceId,StatutPublicationEnum.NON_PUBLIEE);
		Mockito.doReturn(publicationActivite).when(this.publicationActiviteRepository).getFirstPublicationActivite(exerciceId);
		
		ExerciceComptableBlacklistedDto retourExComptableBLDto =  this.espaceOrganisationService.parametrageJourBlacklistEnFonctiontRaisonBlacklistage(exerciceComptableBlacklistedDto, exerciceId, relance); 
		 
		Assert.assertEquals(relance.getType().getLabel(),retourExComptableBLDto.getRaisonBlacklistage());
		assertThat(retourExComptableBLDto.getNbJoursBlacklistFA()).isEqualTo(17);
		assertThat(retourExComptableBLDto.getNbJoursBlacklistMA()).isEqualTo(17);
	 }
	
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#parametrageJourBlacklistEnFonctiontRaisonBlacklistage(ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto, Long exerciceId, PublicationRelanceEntity relance) }
	 * publicationExercice et publicationActivite null
	 * @throws ParseException 
	 */
	@Test
	 public void testParametrageJourBlacklistEnFonctiontRaisonBlacklistageCasFAMASansPublicationExerciceEtSansPublicationActivite() throws ParseException {
		Long exerciceId = 1L;		
		
		PublicationRelanceEntity relance = new PublicationRelanceEntity();
		relance.setCreationDate(LocalDateTime.of(2020,3, 29, 19, 30, 40));
		relance.setType(PublicationRelanceTypeEnum.FAMA);
		
		int nbJourEntreRelanceEtDateDuJOur = (int)ChronoUnit.DAYS.between(relance.getCreationDate().toLocalDate(), LocalDateTime.now().toLocalDate());
		
		ExerciceComptableBlacklistedDto exerciceComptableBlacklistedDto = new ExerciceComptableBlacklistedDto();
		exerciceComptableBlacklistedDto.setNbJoursBlacklistFA(0);
		exerciceComptableBlacklistedDto.setNbJoursBlacklistMA(0);
		exerciceComptableBlacklistedDto.setRaisonBlacklistage(null);
		
		Mockito.doReturn(null).when(this.publicationExerciceRepository).findFirstByExerciceComptableIdAndStatutNotOrderByIdDesc(exerciceId,StatutPublicationEnum.NON_PUBLIEE);
		Mockito.doReturn(null).when(this.publicationActiviteRepository).getFirstPublicationActivite(exerciceId);		
		 
		ExerciceComptableBlacklistedDto retourExComptableBLDto =  this.espaceOrganisationService.parametrageJourBlacklistEnFonctiontRaisonBlacklistage(exerciceComptableBlacklistedDto, exerciceId, relance); 
		 
		Assert.assertEquals(relance.getType().getLabel(),retourExComptableBLDto.getRaisonBlacklistage());
		assertThat(retourExComptableBLDto.getNbJoursBlacklistFA()).isEqualTo(nbJourEntreRelanceEtDateDuJOur);
		assertThat(retourExComptableBLDto.getNbJoursBlacklistMA()).isEqualTo(nbJourEntreRelanceEtDateDuJOur);
	 }
	
	/**
	 * Test de la méthode: {@link EspaceOrganisationServiceImpl#getExerciceComptableBlacklistedDto(ExerciceComptableEntity exercice)}
	 */
	@Test
	public void testGetExerciceComptableBlacklistedDtoSansRelanceNBL() {
		ExerciceComptableEntity exerciceEntity = new ExerciceComptableEntity(1L);
		
		List<PublicationRelanceEntity> lisPublication = new ArrayList<PublicationRelanceEntity>();
		PublicationRelanceEntity publicationRelanceEntity = new PublicationRelanceEntity();
		publicationRelanceEntity.setNiveau(PublicationRelanceNiveauEnum.P);
		exerciceEntity.setPublicationRelance(lisPublication);		
		
		ExerciceComptableBlacklistedDto dto = new ExerciceComptableBlacklistedDto();
		dto.setId(exerciceEntity.getId());
		
		Mockito.doReturn(dto).when(this.exerciceBlacklistedTransformer).modelToDto(exerciceEntity);	
		
		ExerciceComptableBlacklistedDto retourExComptableBLDto =  this.espaceOrganisationService.getExerciceComptableBlacklistedDto(exerciceEntity);
		
		assertThat(retourExComptableBLDto.getNbJoursBlacklistFA()).isEqualTo(0);
		assertThat(retourExComptableBLDto.getNbJoursBlacklistMA()).isEqualTo(0);
	}
}
