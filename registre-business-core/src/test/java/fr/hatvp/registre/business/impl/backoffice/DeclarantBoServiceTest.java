/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.backoffice;

import static fr.hatvp.registre.commons.utils.RegistreUtils.generateNewDate;
import static fr.hatvp.registre.commons.utils.RegistreUtils.generateOldDate;
import static fr.hatvp.registre.commons.utils.RegistreUtils.randomString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import fr.hatvp.registre.business.email.AccountMailer;
import fr.hatvp.registre.business.transformer.impl.DeclarantContactTransformerImpl;
import fr.hatvp.registre.business.transformer.impl.DeclarantTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.DeclarantContactTransformer;
import fr.hatvp.registre.business.transformer.proxy.DeclarantTransformer;
import fr.hatvp.registre.commons.dto.DeclarantContactDto;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.CategoryTelephoneMail;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository;
import fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository;


public class DeclarantBoServiceTest {

	/** Le repository du déclarant. */
	@Mock
	private DeclarantRepository declarantRepository;

	/**
	 * Transformer des déclarants contacts.
	 */
	@Spy
	private DeclarantContactTransformer declarantContactTransformer = new DeclarantContactTransformerImpl();

	/** Le transformeur de l'entitée déclarant. */
	@Spy
	@InjectMocks
	private DeclarantTransformer declarantTransformer = new DeclarantTransformerImpl();

	/** Mailer des comptes utilisateurs. */
	@Mock
	private AccountMailer accountMailer;

	/** Repo des inscriptions */
	@Mock
	private InscriptionEspaceRepository inscriptionEspaceRepository;

	/** Le service à tester. */
	@InjectMocks
	private DeclarantBoServiceImpl declarantService;

	/**
	 * Initialisation de la classe de test.
	 *
	 * @throws Exception
	 *             exception d'initialisation.
	 */
	@Before
	public void setUp() throws Exception {
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Update email avec une adrresse déjà utilisé.
	 * <p>
	 * Test de la méthode
	 * {@link DeclarantBoServiceImpl#updateEmailDeclarant(DeclarantDto)}
	 */
	@Test(expected = BusinessGlobalException.class)
	public void testUpdateEmailPrincipalAvecUneAncienneAdresse() {
		final DeclarantDto newDeclarantData = this.getUser();
		final DeclarantEntity declarant = this.getDeclarant(newDeclarantData);

		declarant.setEmail(newDeclarantData.getEmail());
		declarant.setRecoverCodeExpiryDate(generateOldDate(1));

		Mockito.doReturn(declarant).when(this.declarantRepository).findOne(newDeclarantData.getId());

		this.declarantService.updateEmailDeclarant(newDeclarantData);
	}

	/**
	 * Update email avec une nouvelle adrresse email.
	 * <p>
	 * Test de la méthode
	 * {@link DeclarantBoServiceImpl#updateEmailDeclarant(DeclarantDto)}
	 */
	@Test
	public void testUpdateEmailPrincipalAvecUneNouvelleAdresse() {
		final DeclarantDto newDeclarantData = this.getUser();

		final DeclarantEntity declarant = this.getDeclarant(newDeclarantData);
		declarant.setEmail(randomString(20));
		declarant.setRecoverCodeExpiryDate(generateOldDate(1));
		declarant.setId(newDeclarantData.getId());

		Mockito.doReturn(declarant).when(this.declarantRepository).findOne(newDeclarantData.getId());
		Mockito.doReturn(declarant).when(this.declarantRepository).save(declarant);
		Mockito.doReturn(newDeclarantData).when(this.declarantTransformer).modelToDto(declarant);

		final DeclarantDto updatedDeclarant = this.declarantService.updateEmailDeclarant(newDeclarantData);

		Assert.assertNotNull(newDeclarantData);
		Assert.assertNotNull(updatedDeclarant);
		Assert.assertNotNull(declarant);
		Assert.assertEquals(newDeclarantData.getId(), declarant.getId());
		Assert.assertNotEquals(newDeclarantData.getEmail(), declarant.getEmail());
		Assert.assertNotEquals(newDeclarantData.getEmailTemp(), declarant.getEmailTemp());
	}

	/**
	 * Validation BO d'Update d'email demandé par le déclarant
	 * <p>
	 * Test de la méthode
	 * {@link DeclarantBoServiceImpl#validateDeclarantEmailUpdate(DeclarantDto)}
	 */
	@Test
	public void testValidationBOeMailAddressUpdate() {
		final DeclarantDto newDeclarantData = this.getUser();

		final DeclarantEntity declarant = this.getDeclarant(newDeclarantData);
		declarant.setEmail(randomString(20));
		declarant.setEmailTemp(randomString(20));
		declarant.setRecoverCodeExpiryDate(generateOldDate(1));
		declarant.setId(newDeclarantData.getId());

		Mockito.doReturn(declarant).when(this.declarantRepository).findOne(newDeclarantData.getId());
		Mockito.doReturn(declarant).when(this.declarantRepository).save(declarant);
		Mockito.doReturn(newDeclarantData).when(this.declarantTransformer).modelToDto(declarant);

		final DeclarantDto updatedDeclarant = this.declarantService.validateDeclarantEmailUpdate(newDeclarantData);

		Assert.assertNotNull(newDeclarantData);
		Assert.assertNotNull(updatedDeclarant);
		Assert.assertNotNull(declarant);
		Assert.assertEquals(updatedDeclarant.getId(), declarant.getId());
		Assert.assertEquals(newDeclarantData.getEmailTemp(), declarant.getEmail());
		Assert.assertNull(declarant.getEmailTemp());
	}

	/**
	 * Method: getPaginatedDemandesOrganisation(final Integer pageNumber, final
	 * Integer resultPerPage, final boolean all)
	 */
	@Test
	public void testFindPaginatedDeclarant() {

		final Pageable pageable = new PageRequest(10, 25, Sort.Direction.ASC, "id");

		final List<DeclarantEntity> declarantEntityList = new ArrayList<>();
		for (int i = 0; i < pageable.getPageSize(); i++) {
			DeclarantEntity declarantEntity = new DeclarantEntity();
			declarantEntity.setId(Long.valueOf(RandomStringUtils.randomNumeric(3)));
			declarantEntityList.add(declarantEntity);
		}

		final Page<DeclarantEntity> declarantEntityPage = new PageImpl<>(declarantEntityList, pageable,
				declarantEntityList.size() * pageable.getPageNumber() * 2);

		when(inscriptionEspaceRepository.findAllByDeclarantId(Mockito.anyLong())).thenReturn(new ArrayList<>());
		when(declarantRepository.findAll(pageable)).thenReturn(declarantEntityPage);

		PaginatedDto<DeclarantDto> paginatedDto = declarantService.findPaginatedDeclarant(pageable.getPageNumber() + 1,
				pageable.getPageSize());

		Assert.assertEquals(paginatedDto.getDtos().size(), pageable.getPageSize());
		Assert.assertEquals(paginatedDto.getResultTotalNumber().longValue(), declarantEntityPage.getTotalElements());

	}

	/**
	 * @return Renvoi un déclarant DTO.
	 */
	private DeclarantDto getUser() {
		final DeclarantDto declarantToSave = new DeclarantDto();
		declarantToSave.setId(1L);
		declarantToSave.setPrenom(randomString(10));
		declarantToSave.setNom(randomString(10));
		declarantToSave.setEmail(randomString(20));
		declarantToSave.setEmailTemp(randomString(20));
		declarantToSave.setPassword(randomString(30));
		declarantToSave.setBirthDate(new Date());
		declarantToSave.setCivility(CiviliteEnum.MME);
		declarantToSave.setActivated(false);

		final List<DeclarantContactDto> emails = new ArrayList<>();
		emails.add(getContact(randomString(15), null, CategoryTelephoneMail.PERSO));
		final List<DeclarantContactDto> phones = new ArrayList<>();
		phones.add(getContact(null, randomString(10), CategoryTelephoneMail.PERSO));

		declarantToSave.setEmailComplement(emails);
		declarantToSave.setPhone(phones);

		return declarantToSave;
	}

	/**
	 * @param declarantDto
	 *            dto à transformer.
	 * @return Renvoi une entitée déclarant avec les informations du DTO.
	 */
	private DeclarantEntity getDeclarant(final DeclarantDto declarantDto) {

		final DeclarantEntity declarant = new DeclarantEntity();
		declarant.setEmailConfirmationCode(declarantDto.getActivationEmailKeyCode());
		declarant.setEmail(declarantDto.getEmail());
		declarant.setEmailTemp(declarantDto.getEmailTemp());
		declarant.setPassword(declarantDto.getPassword());
		declarant.setCivility(declarantDto.getCivility());
		declarant.setNom(declarantDto.getPrenom());
		declarant.setPrenom(declarantDto.getNom());
		declarant.setBirthDate(declarantDto.getBirthDate());
		declarant.setActivated(declarantDto.isActivated());

		return declarant;
	}

	/**
	 * Créer une nstance de {@link DeclarantContactDto}
	 */
	private DeclarantContactDto getContact(final String telephone, final String email,
			final CategoryTelephoneMail type) {
		final DeclarantContactDto declarantContactDto = new DeclarantContactDto();
		declarantContactDto.setTelephone(telephone);
		declarantContactDto.setEmail(email);
		declarantContactDto.setCategorie(type);

		return declarantContactDto;
	}
	
	/**
	 * Update email avec une adrresse email déjà existente en base
	 * <p>
	 * Test de la méthode
	 * {@link DeclarantBoServiceImpl#updateEmailDeclarant(DeclarantDto)}
	 */
	@Test
	public void testUpdateEmailPrincipalEmailDejaEnBase() {
		final DeclarantDto newDeclarantData = this.getUser();

		final DeclarantEntity declarant = this.getDeclarant(newDeclarantData);
		declarant.setEmail(randomString(20));
		declarant.setRecoverCodeExpiryDate(generateOldDate(1));
		declarant.setId(newDeclarantData.getId());

		Mockito.doReturn(declarant).when(this.declarantRepository).findOne(newDeclarantData.getId());
		Mockito.doReturn(declarant).when(this.declarantRepository).findByEmailIgnoreCase(newDeclarantData.getEmail());
		
		Mockito.doReturn(declarant).when(this.declarantRepository).save(declarant);
		Mockito.doReturn(newDeclarantData).when(this.declarantTransformer).modelToDto(declarant);		

		try {
			final DeclarantDto updatedDeclarant = this.declarantService.updateEmailDeclarant(newDeclarantData);
		  }catch(EntityExistsException aExp){
			  assertThat(ErrorMessageEnum.ADRESSE_EMAIL_EXISTE_DEJA.getMessage()).isEqualTo(aExp.getMessage());
		  }	
	}
	
	/**
	 * Cas Rejet le nouveau email s'il est en attente de validation par un autre utilisateur avec date non expirée
	 * <p>
	 * Test de la méthode
	 * {@link DeclarantBoServiceImpl#updateEmailDeclarant(DeclarantDto)}
	 */
	@Test
	public void testUpdateEmailPrincipalEmailDejaEnAttenteValidation() {
		final DeclarantDto newDeclarantData = this.getUser();

		final DeclarantEntity declarant = this.getDeclarant(newDeclarantData);
		declarant.setEmail(randomString(20));
		declarant.setRecoverCodeExpiryDate(generateOldDate(1));
		declarant.setId(newDeclarantData.getId());
		
		final DeclarantEntity declarant2 = new DeclarantEntity(2L);
		declarant2.setEmail(randomString(20));
		declarant2.setEmailCodeExpiryDate(generateNewDate(10));
		

		Mockito.doReturn(declarant).when(this.declarantRepository).findOne(newDeclarantData.getId());		
		Mockito.doReturn(declarant2).when(this.declarantRepository).findByEmailTemp(newDeclarantData.getEmail());	
		Mockito.doReturn(declarant).when(this.declarantRepository).save(declarant);
		Mockito.doReturn(newDeclarantData).when(this.declarantTransformer).modelToDto(declarant);		

		try {
			final DeclarantDto updatedDeclarant = this.declarantService.updateEmailDeclarant(newDeclarantData);
		}catch(EntityExistsException aExp){
		  assertThat(ErrorMessageEnum.EMAIL_EN_ATTENTE_DE_VALIDATION_PAR_UN_AUTRE_UTILISATEUR.getMessage()).isEqualTo(aExp.getMessage());
		}	
	}
	
	
	/**
	 * Cas Rejet le nouveau email s'il est en attente de validation par un autre utilisateur avec expirée
	 * <p>
	 * Test de la méthode {@link DeclarantBoServiceImpl#updateEmailDeclarant(DeclarantDto)}
	 */
	@Test
	public void testUpdateEmailPrincipalEmailDejaEnAttenteValidationDateExpiree() {
		final DeclarantDto newDeclarantData = this.getUser();

		final DeclarantEntity declarant = this.getDeclarant(newDeclarantData);
		declarant.setEmail(randomString(20));
		declarant.setRecoverCodeExpiryDate(generateOldDate(1));
		declarant.setId(newDeclarantData.getId());
		
		final DeclarantEntity declarant2 = new DeclarantEntity(2L);
		declarant2.setEmail(randomString(20));
		declarant2.setEmailCodeExpiryDate(generateOldDate(10));
		

		Mockito.doReturn(declarant).when(this.declarantRepository).findOne(newDeclarantData.getId());		
		Mockito.doReturn(declarant2).when(this.declarantRepository).findByEmailTemp(newDeclarantData.getEmail());	
		Mockito.doReturn(declarant).when(this.declarantRepository).save(declarant);
		Mockito.doReturn(newDeclarantData).when(this.declarantTransformer).modelToDto(declarant);		

		try {
			final DeclarantDto updatedDeclarant = this.declarantService.updateEmailDeclarant(newDeclarantData);
		  }catch(EntityExistsException aExp){
			  assertThat(ErrorMessageEnum.EMAIL_EN_ATTENTE_DE_VALIDATION_PAR_UN_AUTRE_UTILISATEUR.getMessage()).isEqualTo(aExp.getMessage());
		  }	
	}
	/**
	 * Test de la méthode {@link DeclarantBoServiceImpl#updateDeclarant(final DeclarantDto declarantDto) }
	 * 
	 */
	@Test
	public void testUpdateDeclarantEntityNotFound() {
		final DeclarantDto declarantDto = new DeclarantDto();
		declarantDto.setId(1L);
		
		Mockito.doReturn(null).when(this.declarantRepository).findOne(declarantDto.getId());	
		
		try {
			DeclarantDto retourDeclarantDto = this.declarantService.updateDeclarant(declarantDto);
		}catch(EntityNotFoundException aExp){
		  assertThat("Déclarant introuvable").isEqualTo(aExp.getMessage());
		}	
	}	
	
}
