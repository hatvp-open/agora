/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import fr.hatvp.registre.business.transformer.proxy.DemandeOrganisationTransformer;
import fr.hatvp.registre.commons.dto.DemandeOrganisationDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ContextDemandeOrganisationEnum;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.backoffice.StatutDemandeOrganisationEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;
import fr.hatvp.registre.persistence.repository.espace.DemandeOrganisationRepository;

/**
 * DemandeOrganisationServiceImpl Tester.
 *
 */

public class DemandeOrganisationServiceTest {
	@Mock
	private DemandeOrganisationRepository demandeOrganisationRepository;

	@Mock
	private DemandeOrganisationTransformer demandeOrganisationTransformer;

	@InjectMocks
	private DemandeOrganisationServiceImpl demandeOrganisationService;

	/** Identifiant. */
	private final Long DEMANDE_ID = 1L;

	@Before
	public void setup() throws Exception {
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method: cloturerDemande(final Long id, final StatutDemandeOrganisationEnum statut)
	 */
	@Test
	public void testCloturerDemandeRefusee() throws Exception {
		final DemandeOrganisationEntity demande = getDemande();

		Mockito.doReturn(demande).when(this.demandeOrganisationRepository).findOne(this.DEMANDE_ID);

		this.demandeOrganisationService.cloturerDemande(this.DEMANDE_ID, StatutDemandeOrganisationEnum.TRAITEE_ORGANISATION_REFUSEE);
		Assert.assertEquals(StatutDemandeOrganisationEnum.TRAITEE_ORGANISATION_REFUSEE, demande.getStatut());
	}

	/**
	 * Method: cloturerDemande(final Long id, final StatutDemandeOrganisationEnum statut)
	 */
	@Test
	public void testCloturerDemandeTrouvee() throws Exception {
		final DemandeOrganisationEntity demande = getDemande();

		Mockito.doReturn(demande).when(this.demandeOrganisationRepository).findOne(this.DEMANDE_ID);

		this.demandeOrganisationService.cloturerDemande(this.DEMANDE_ID, StatutDemandeOrganisationEnum.TRAITEE_ORGANISATION_TROUVEE);
		Assert.assertEquals(StatutDemandeOrganisationEnum.TRAITEE_ORGANISATION_TROUVEE, demande.getStatut());
	}

	/**
	 * Method: getPaginatedDemandesOrganisation(final Integer pageNumber, final Integer resultPerPage, final boolean all)
	 */
	@Test
	public void testGetPaginatedDemandesOrganisation() {

		final PageRequest pageable = new PageRequest(10, 25, Sort.Direction.ASC, "id");

		final List<DemandeOrganisationEntity> demandeOrganisationList = new ArrayList<>();
		for (int i = 0; i < pageable.getPageSize(); i++) {
			DemandeOrganisationEntity demandeOrganisationEntity = new DemandeOrganisationEntity();
			demandeOrganisationEntity.setId(Long.valueOf(RandomStringUtils.randomNumeric(3)));
			demandeOrganisationList.add(demandeOrganisationEntity);
		}

		final Page<DemandeOrganisationEntity> demandeOrganisationPage = new PageImpl<>(demandeOrganisationList, pageable,
				demandeOrganisationList.size() * pageable.getPageNumber() * 2);

		when(demandeOrganisationRepository.findByStatut(Mockito.any(StatutDemandeOrganisationEnum.class), Mockito.any(PageRequest.class))).thenReturn(demandeOrganisationPage);
		when(demandeOrganisationRepository.findAll(Mockito.any(PageRequest.class))).thenReturn(demandeOrganisationPage);

		PaginatedDto<DemandeOrganisationDto> paginatedDto = demandeOrganisationService.getPaginatedDemandesOrganisation(pageable.getPageNumber() + 1, pageable.getPageSize());

		Assert.assertEquals(paginatedDto.getDtos().size(), pageable.getPageSize());
		Assert.assertEquals(paginatedDto.getResultTotalNumber().longValue(), demandeOrganisationPage.getTotalElements());

		paginatedDto = demandeOrganisationService.getPaginatedDemandesOrganisationByStatus(StatutDemandeOrganisationEnum.A_TRAITER, pageable.getPageNumber() + 1,
				pageable.getPageSize());

		Assert.assertEquals(paginatedDto.getDtos().size(), pageable.getPageSize());
		Assert.assertEquals(paginatedDto.getResultTotalNumber().longValue(), demandeOrganisationPage.getTotalElements());
	}

	/**
	 * Demande entity getter
	 *
	 * @return demande entity
	 */
	private DemandeOrganisationEntity getDemande() {
		final DemandeOrganisationEntity demande = new DemandeOrganisationEntity();

		demande.setId(this.DEMANDE_ID);
		demande.setVersion(0);
		demande.setContext(ContextDemandeOrganisationEnum.Ajout_Espace);
		demande.setAdresse(RegistreUtils.randomString(100));
		demande.setStatut(StatutDemandeOrganisationEnum.A_TRAITER);

		return demande;
	}
	/**
	 * Test de la méthode {@link DemandeOrganisationServiceImpl#demandeOrganisationService(final Long id))}
	 */
	@Test
	public void testGetDemandeOrganisationException() {
		Long id = 1L;
		Mockito.doReturn(null).when(this.demandeOrganisationRepository).findOne(id);
		
		try {
			demandeOrganisationService.getDemandeOrganisation(id);
		} catch (BusinessGlobalException ex) {
			assertThat(ErrorMessageEnum.MSG_404.getMessage()).isEqualTo(ex.getMessage());
		}	
	}
}
