/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

    import java.time.LocalDate;
    import fr.hatvp.registre.business.transformer.proxy.activite.QualificationObjetTransformer;
    import fr.hatvp.registre.commons.dto.activite.QualificationObjetDto;
    import fr.hatvp.registre.persistence.entity.activite.QualificationObjetEntity;
    import org.junit.Assert;
    import org.junit.Test;
    import org.junit.runner.RunWith;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.test.context.ContextConfiguration;
    import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
    import org.springframework.test.context.support.AnnotationConfigContextLoader;
    import fr.hatvp.registre.business.config.TestBusinessConfig;
    import fr.hatvp.registre.commons.dto.AbstractCommonDto;
    import fr.hatvp.registre.commons.utils.RegistreUtils;
    import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

 /**
 * Test de la classe {@link QualificationObjetTransformerTest}
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBusinessConfig.class},
    loader = AnnotationConfigContextLoader.class)
public class QualificationObjetTransformerTest
 {

    /** Transformeur de l'espace organisation. */
    @Autowired
    private QualificationObjetTransformer qualificationObjetTransformer;

    /** Denomination utilisé pour les tests */
    private final String DENOMINATION = "TEST DENOMINATION ORGANISATION";

    /** Nom d'usage utilisé pour les tests */
    private final String NOMUSAGE = "TEST DENOMINATION ORGANISATION";

    /**
     * Test de la méthode {@link QualificationObjetTransformer#modelToDto(AbstractCommonEntity)}
     */
    @Test
    public void testModelToDto() {
        final QualificationObjetEntity entity = this.getQualificationObjet();
        final QualificationObjetDto qualificationObjetDto = this.qualificationObjetTransformer
            .modelToDto(entity);

        Assert.assertNotNull(qualificationObjetDto);
        Assert.assertEquals(qualificationObjetDto.getId(), entity.getId());
        Assert.assertEquals(qualificationObjetDto.getCoefficiantConfiance(), entity.getCoefficiantConfiance());
        Assert.assertEquals(qualificationObjetDto.getDateEnregistrement(), entity.getDateEnregistrement());
        Assert.assertEquals(qualificationObjetDto.getDenomination(), entity.getDenomination());
        Assert.assertEquals(qualificationObjetDto.getIdentifiantNational(), entity.getIdentifiantNational());
        Assert.assertEquals(qualificationObjetDto.getIdFiche(), entity.getIdFiche());
        Assert.assertEquals(qualificationObjetDto.getNomUsage(), entity.getNomUsage());
        Assert.assertEquals(qualificationObjetDto.getObjet(), entity.getObjet());
    }

    /**
     * Test de la méthode {@link QualificationObjetTransformer#dtoToModel(AbstractCommonDto)}
     */
    @Test
    public void testDtoToModel() {

        final QualificationObjetDto dto = this.getQualifObjetDto();
        final QualificationObjetEntity entity = this.qualificationObjetTransformer
            .dtoToModel(dto);

        Assert.assertNotNull(entity);
        Assert.assertEquals(entity.getId(), dto.getId());
        Assert.assertEquals(entity.getCoefficiantConfiance(), dto.getCoefficiantConfiance());
        Assert.assertEquals(entity.getDateEnregistrement(), dto.getDateEnregistrement());
        Assert.assertEquals(entity.getDenomination(), dto.getDenomination());
        Assert.assertEquals(entity.getIdentifiantNational(), dto.getIdentifiantNational());
        Assert.assertEquals(entity.getIdFiche(), dto.getIdFiche());
        Assert.assertEquals(entity.getNomUsage(), dto.getNomUsage());
        Assert.assertEquals(entity.getObjet(), dto.getObjet());
    }

    /**
     * Création d'un nouvel Espace Organisation.
     *
     * @return entité espace organisation
     */
    private QualificationObjetEntity getQualificationObjet() {
        final QualificationObjetEntity qualificationObjetEntity = new QualificationObjetEntity();

        qualificationObjetEntity.setId(RegistreUtils.randomLong(1));
        qualificationObjetEntity.setVersion(1);
        qualificationObjetEntity.setObjet(RegistreUtils.randomString(50));
        qualificationObjetEntity.setDenomination(DENOMINATION);
        qualificationObjetEntity.setNomUsage(NOMUSAGE);
        qualificationObjetEntity.setCoefficiantConfiance(RegistreUtils.randomFloat(1));
        qualificationObjetEntity.setDateEnregistrement(LocalDate.now());
        qualificationObjetEntity.setIdentifiantNational(RegistreUtils.randomString(10));
        qualificationObjetEntity.setIdFiche(RegistreUtils.randomString(5));
        qualificationObjetEntity.setNote(RegistreUtils.randomBoolean());

        return qualificationObjetEntity;
    }

    /**
     * Créer un {@link QualificationObjetDto}
     *
     * @return QualificationObjetDto
     */
    private QualificationObjetDto getQualifObjetDto() {
        final QualificationObjetDto qualificationObjetDto = new QualificationObjetDto();
        qualificationObjetDto.setId(RegistreUtils.randomLong(1));
        qualificationObjetDto.setVersion(1);
        qualificationObjetDto.setObjet(RegistreUtils.randomString(50));
        qualificationObjetDto.setDenomination(DENOMINATION);
        qualificationObjetDto.setNomUsage(NOMUSAGE);
        qualificationObjetDto.setCoefficiantConfiance(RegistreUtils.randomFloat(1));
        qualificationObjetDto.setDateEnregistrement(LocalDate.now());
        qualificationObjetDto.setIdentifiantNational(RegistreUtils.randomString(10));
        qualificationObjetDto.setIdFiche(RegistreUtils.randomString(5));
        qualificationObjetDto.setNote(RegistreUtils.randomBoolean());
        return qualificationObjetDto;
    }
}
