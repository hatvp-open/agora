/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.hatvp.registre.business.CollaborateurService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.email.CollaboratorMailer;
import fr.hatvp.registre.business.transformer.impl.CollaborateurTransformerImpl;
import fr.hatvp.registre.business.transformer.proxy.CollaborateurTransformer;
import fr.hatvp.registre.commons.dto.CollaborateurDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.CollaborateurEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.publication.PubCollaborateurEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;
import fr.hatvp.registre.persistence.repository.espace.CollaborateurRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.publication.PubCollaborateurRepository;

/**
 * Test du service Collaborateur {@link fr.hatvp.registre.business.CollaborateurService}.
 *
 * @version $Revision$ $Date${0xD}
 */

public class CollaborateurServiceTest {

	private final static Long ESPACE_ORGANISATION_ID = 3L;

	/** Repository Collaborateurs. */
	@Mock
	private CollaborateurRepository collaborateurRepository;

	/** Repository Espace Organisation. */
	@Mock
	private EspaceOrganisationRepository espaceOrganisationRepository;
	
	@Mock
	private PubCollaborateurRepository pubCollaborateurRepository;

	/** Transformer collaborateurs. */
	@Mock
	private CollaborateurTransformer collaborateurTransformer;

	/** service espace */
	@Mock
	private EspaceOrganisationService espaceService;

	/** Mailer pour service collaborateurs. */
	@Mock
	private CollaboratorMailer collaboratorMailer;
	/** Service collaborateurs. */
	@InjectMocks
	private CollaborateurServiceImpl collaborateurService;
	

	@Before
	public void before() throws Exception {
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method: getAllCollaborateurs(final Long espaceOrganisationId)
	 */
	@Test
	public void testGetAllCollaborateurs() throws Exception {
		/*
		 * Data
		 */
		final Long espaceOrganisationId = 1L;

		final List<CollaborateurEntity> collaborateurEntities = new ArrayList<>();
		final CollaborateurEntity collaborateurEntity = new CollaborateurEntity();
		collaborateurEntities.add(collaborateurEntity);

		final CollaborateurDto collaborateurDto = new CollaborateurDto();

		/*
		 * Mock
		 */
		Mockito.doReturn(collaborateurEntities).when(this.collaborateurRepository).findByEspaceOrganisationId(espaceOrganisationId);
		Mockito.doReturn(collaborateurDto).when(this.collaborateurTransformer).modelToDto(collaborateurEntity);

		/*
		 * Test
		 */
		final List<CollaborateurDto> result = this.collaborateurService.getAllCollaborateurs(espaceOrganisationId);
		Assert.assertNotNull(result);
	}

	/**
	 * Ajouter un collaborateur à un espace sans collaborateurs Method: addCollaborateur(final CollaborateurDto collaborateur)
	 */
	@Test
	public void testAddCollaborateurPremier() throws Exception {
		/*
		 * Data
		 */
		final Long id = 1L;
		final CollaborateurDto dto = new CollaborateurDto();

		dto.setId(id);
		dto.setVersion(0);
		dto.setPrenom(RegistreUtils.randomString(10));
		dto.setNom(RegistreUtils.randomString(10));
		dto.setEmail(RegistreUtils.randomString(50));
		dto.setCivilite(CiviliteEnum.M);
		dto.setEspaceOrganisationId(ESPACE_ORGANISATION_ID);
		dto.setFonction(RegistreUtils.randomString(50));
		dto.setActif(true);

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_ORGANISATION_ID);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setDenomination(RegistreUtils.randomString(20));
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		/*
		 * Mock
		 */
		Mockito.doReturn(new ArrayList<CollaborateurEntity>()).when(this.collaborateurRepository).findByEspaceOrganisationId(ESPACE_ORGANISATION_ID);
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ESPACE_ORGANISATION_ID);

		/*
		 * Tests
		 */
		this.collaborateurService.addCollaborateurAdditionel(dto, ESPACE_ORGANISATION_ID);
		Assert.assertTrue(dto.getActif());
		Assert.assertEquals(0, dto.getSortOrder().intValue());
	}

	/**
	 * Ajouter un collaborateur à un espace avec d'autres collaborateurs Method: addCollaborateur(final CollaborateurDto collaborateur)
	 */
	@Test
	public void testAddCollaborateurEspaceRemplis() throws Exception {
		/*
		 * Data
		 */
		final Long id = 1L;
		final CollaborateurDto dto = new CollaborateurDto();

		dto.setId(id);
		dto.setVersion(0);
		dto.setPrenom(RegistreUtils.randomString(10));
		dto.setNom(RegistreUtils.randomString(10));
		dto.setEmail(RegistreUtils.randomString(50));
		dto.setCivilite(CiviliteEnum.M);
		dto.setEspaceOrganisationId(ESPACE_ORGANISATION_ID);
		dto.setFonction(RegistreUtils.randomString(50));
		dto.setActif(true);

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_ORGANISATION_ID);

		final OrganisationEntity organisationEntity = new OrganisationEntity();
		organisationEntity.setDenomination(RegistreUtils.randomString(20));
		espaceOrganisationEntity.setOrganisation(organisationEntity);

		final List<CollaborateurEntity> collabs = new ArrayList<>();
		collabs.add(getCollaborateurEntity(2L, true));
		collabs.add(getCollaborateurEntity(3L, true));
		collabs.add(getCollaborateurEntity(4L, true));

		/*
		 * Mock
		 */
		Mockito.doReturn(collabs).when(this.collaborateurRepository).findByEspaceOrganisationId(ESPACE_ORGANISATION_ID);
		Mockito.doReturn(espaceOrganisationEntity).when(this.espaceOrganisationRepository).findOne(ESPACE_ORGANISATION_ID);

		/*
		 * Tests
		 */
		this.collaborateurService.addCollaborateurAdditionel(dto, ESPACE_ORGANISATION_ID);
		Assert.assertTrue(dto.getActif());
		Assert.assertEquals(3, dto.getSortOrder().intValue());
	}

	/**
	 * Activer collaborateur Method: setCollaborateurActif(final Long id, final boolean actif)
	 */
	@Test
	public void testSetCollaborateurActif() throws Exception {
		/*
		 * Data
		 */
		final Long id = 1L;
		final CollaborateurEntity model = getCollaborateurEntity(id, true);

		/*
		 * Mock
		 */
		Mockito.doReturn(model).when(this.collaborateurRepository).findByIdAndEspaceOrganisationId(id, ESPACE_ORGANISATION_ID);

		/*
		 * Tests
		 */
		this.collaborateurService.setCollaborateurActif(id, ESPACE_ORGANISATION_ID, true);
		Assert.assertTrue(model.getActif());
	}

	/**
	 * Désactiver collaborateur Method: setCollaborateurActif(final Long id, final boolean actif)
	 */
	@Test
	public void testSetCollaborateurInactifctif() throws Exception {
		/*
		 * Data
		 */
		final Long id = 1L;
		final CollaborateurEntity model = getCollaborateurEntity(id, true);

		/*
		 * Mock
		 */
		Mockito.doReturn(model).when(this.collaborateurRepository).findByIdAndEspaceOrganisationId(id, ESPACE_ORGANISATION_ID);

		/*
		 * Tests
		 */
		this.collaborateurService.setCollaborateurActif(id, ESPACE_ORGANISATION_ID, false);
		Assert.assertFalse(model.getActif());
	}

	/**
	 * Activer collaborateur additionnel Method: setCollaborateurActif(final Long id, final boolean actif)
	 */
	@Test(expected = BusinessGlobalException.class)
	public void testSetCollaborateurAdditionnelActif() throws Exception {
		/*
		 * Data
		 */
		final Long id = 1L;
		final CollaborateurEntity model = getCollaborateurEntity(id, false);

		/*
		 * Mock
		 */
		Mockito.doReturn(model).when(this.collaborateurRepository).findByIdAndEspaceOrganisationId(id, ESPACE_ORGANISATION_ID);

		/*
		 * Tests
		 */
		this.collaborateurService.setCollaborateurActif(id, ESPACE_ORGANISATION_ID, true);
	}

	/**
	 * Method: remonterCollaborateur(final Long id, final Long espaceOrganisationId)
	 */
	@Test
	public void testRemonterCollaborateur() {
		/*
		 * Data
		 */
		final Long id1 = 1L;
		final CollaborateurEntity c1 = getCollaborateurEntity(id1, false);
		c1.setSortOrder(0);

		final Long id2 = 2L;
		final CollaborateurEntity c2 = getCollaborateurEntity(id2, false);
		c2.setSortOrder(1);

		final List<CollaborateurEntity> collabs = new ArrayList<>();
		collabs.add(c1);
		collabs.add(c2);

		/*
		 * Mock
		 */
		Mockito.doReturn(c1).when(this.collaborateurRepository).findOne(id1);
		Mockito.doReturn(c2).when(this.collaborateurRepository).findOne(id2);
		Mockito.doReturn(collabs).when(this.collaborateurRepository).findByEspaceOrganisationId(ESPACE_ORGANISATION_ID);

		/*
		 * Tests
		 */
		this.collaborateurService.remonterCollaborateur(id2, ESPACE_ORGANISATION_ID);
		Assert.assertTrue(c2.getSortOrder() == 0);
		Assert.assertTrue(c1.getSortOrder() == 1);
	}

	/**
	 * Method: descendreCollaborateur(final Long id, final Long espaceOrganisationId)
	 */
	@Test
	public void testDescendreCollaborateur() {
		/*
		 * Data
		 */
		final Long id1 = 1L;
		final CollaborateurEntity c1 = getCollaborateurEntity(id1, false);
		c1.setSortOrder(0);

		final Long id2 = 2L;
		final CollaborateurEntity c2 = getCollaborateurEntity(id2, false);
		c2.setSortOrder(1);

		final List<CollaborateurEntity> collabs = new ArrayList<>();
		collabs.add(c1);
		collabs.add(c2);

		/*
		 * Mock
		 */
		Mockito.doReturn(c1).when(this.collaborateurRepository).findOne(id1);
		Mockito.doReturn(c2).when(this.collaborateurRepository).findOne(id2);
		Mockito.doReturn(collabs).when(this.collaborateurRepository).findByEspaceOrganisationId(ESPACE_ORGANISATION_ID);

		/*
		 * Tests
		 */
		this.collaborateurService.descendreCollaborateur(id1, ESPACE_ORGANISATION_ID);
		Assert.assertTrue(c2.getSortOrder() == 0);
		Assert.assertTrue(c1.getSortOrder() == 1);
	}

	/**
	 * Method: deleteCollaborateur(final Long id)
	 */
	@Test
	public void testDeleteCollaborateur() throws Exception {
		/*
		 * Data
		 */
		final Long id = 1L;
		final CollaborateurEntity c = getCollaborateurEntity(id, false);
		c.setSortOrder(0);

		final Long id1 = 1L;
		final CollaborateurEntity c1 = getCollaborateurEntity(id1, false);
		c1.setSortOrder(1);

		final Long id2 = 2L;
		final CollaborateurEntity c2 = getCollaborateurEntity(id2, false);
		c2.setSortOrder(2);

		final List<CollaborateurEntity> collabs = new ArrayList<>();
		collabs.add(c);
		collabs.add(c1);
		collabs.add(c2);

		/*
		 * Mock
		 */
		Mockito.doReturn(c).when(this.collaborateurRepository).findByIdAndEspaceOrganisationId(id, ESPACE_ORGANISATION_ID);
		Mockito.doReturn(collabs).when(this.collaborateurRepository).findByEspaceOrganisationId(ESPACE_ORGANISATION_ID);

		this.collaborateurService.deleteCollaborateur(id, ESPACE_ORGANISATION_ID);

		Assert.assertTrue(c1.getSortOrder() == 0);
		Assert.assertTrue(c2.getSortOrder() == 1);
	}

	/**
	 * Method: deleteCollaborateur(final Long id)
	 */
	@Test(expected = BusinessGlobalException.class)
	public void testDeleteCollaborateurError() throws Exception {
		/*
		 * Data
		 */
		final Long id = 1L;
		final CollaborateurEntity model = getCollaborateurEntity(id, true);

		/*
		 * Mock
		 */
		Mockito.doReturn(model).when(this.collaborateurRepository).findByIdAndEspaceOrganisationId(id, ESPACE_ORGANISATION_ID);
		this.collaborateurService.deleteCollaborateur(id, ESPACE_ORGANISATION_ID);
	}

	/**
	 * Test de la méthode {@link CollaborateurService#editCollaborateur(CollaborateurDto)} avec collaborateur introuvable
	 */
	@Test(expected = EntityNotFoundException.class)
	public void testEditCollaboratorAvecCollaborateurIntrouvable() throws Exception {
		Mockito.doReturn(null).when(this.collaborateurRepository).findByIdAndEspaceOrganisationId(1L, ESPACE_ORGANISATION_ID + 1);
		this.collaborateurService.editFonctionCollaborateur(new CollaborateurDto(), ESPACE_ORGANISATION_ID);
	}

	/**
	 * Test de la méthode {@link CollaborateurService#editCollaborateur(CollaborateurDto)}
	 */
	@Test
	@Ignore
	public void testEditCollaborator() throws Exception {
		final CollaborateurEntity entity = getCollaborateurEntity(1L, true);

		// Ces données de l'entity ne devraient pas changer
		String nomOrig = entity.getDeclarant().getNom();
		String prenomOrig = entity.getDeclarant().getPrenom();
		String emailOrig = entity.getDeclarant().getEmail();
		Long declarantIdOrig = entity.getDeclarant().getId();
		CiviliteEnum civOrig = entity.getDeclarant().getCivility();

		final CollaborateurDto dto = new CollaborateurDto();
		dto.setId(1L);
		dto.setNom("nom test");
		dto.setPrenom("prenom test");
		dto.setEmail("email test");
		dto.setDeclarantId(25L);
		dto.setCivilite(CiviliteEnum.MME);
		dto.setFonction(RegistreUtils.randomString(50));
		
		PubCollaborateurEntity premierePubCollaborateur = new PubCollaborateurEntity();
		premierePubCollaborateur.setId(1L);
		premierePubCollaborateur.setNom(entity.getNom());
		premierePubCollaborateur.setPrenom(entity.getPrenom());
		premierePubCollaborateur.setEmail(entity.getEmail());
		premierePubCollaborateur.setDeclarant(entity.getDeclarant());
		premierePubCollaborateur.setCivility(entity.getCivility());
		
		PublicationEntity pub = new PublicationEntity();
		pub.setCreationDate(new Date());
		
		premierePubCollaborateur.setPublication(pub);
		
		Mockito.doReturn(null).when(this.pubCollaborateurRepository).findFirstByDeclarantAndEspaceOrganisationOrderByPublicationIdAsc(entity.getDeclarant(), entity.getEspaceOrganisation()); 
		Mockito.doReturn(entity).when(this.collaborateurRepository).findByIdAndEspaceOrganisationId(dto.getId(), ESPACE_ORGANISATION_ID);
		CollaborateurTransformerImpl collaborateurTransformerImpl = new CollaborateurTransformerImpl();
		
		
		
		Mockito.doReturn(collaborateurTransformerImpl.modelToDto(entity)).when(this.collaborateurTransformer).modelToDto(entity);
		

		Mockito.doReturn(entity).when(this.collaborateurRepository).save(entity);

		final CollaborateurDto resultDto = this.collaborateurService.editFonctionCollaborateur(dto, ESPACE_ORGANISATION_ID);

		Assert.assertNotNull(resultDto);
		Assert.assertEquals(resultDto.getFonction(), resultDto.getFonction());
		Assert.assertEquals(resultDto.getNom(), nomOrig);
		Assert.assertEquals(resultDto.getPrenom(), prenomOrig);
		Assert.assertEquals(resultDto.getCivilite(), civOrig);
		Assert.assertEquals(resultDto.getEmail(), emailOrig);
		Assert.assertEquals(resultDto.getDeclarantId(), declarantIdOrig);
	}

	/**
	 * générer une entité CollaborateurEntity pour tests
	 *
	 * @param id
	 *            identifiant de l'entité
	 * @return objet entité de test
	 */
	private CollaborateurEntity getCollaborateurEntity(final Long id, final boolean inscrit) {
		final CollaborateurEntity model = new CollaborateurEntity();
		model.setId(id);
		model.setVersion(0);
		model.setActif(true);

		if (inscrit) {
			final DeclarantEntity declarantEntity = new DeclarantEntity();
			declarantEntity.setId(2L);
			declarantEntity.setNom(RegistreUtils.randomString(15));
			declarantEntity.setPrenom(RegistreUtils.randomString(10));
			declarantEntity.setEmail(RegistreUtils.randomString(20));
			declarantEntity.setCivility(CiviliteEnum.M);
			model.setDeclarant(declarantEntity);
		} else {
			model.setNom(RegistreUtils.randomString(15));
			model.setPrenom(RegistreUtils.randomString(10));
			model.setEmail(RegistreUtils.randomString(20));
			model.setCivility(CiviliteEnum.M);
		}

		final EspaceOrganisationEntity espaceOrganisationEntity = new EspaceOrganisationEntity();
		espaceOrganisationEntity.setId(ESPACE_ORGANISATION_ID);
		model.setEspaceOrganisation(espaceOrganisationEntity);
		return model;
	}

}
