/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.impl.nomenclature;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.business.NomenclatureService;
import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.commons.dto.nomenclature.NomenclatureDto;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestBusinessConfig.class }, loader = AnnotationConfigContextLoader.class)
public class NomenclatureServiceTest {

	@Autowired
	private NomenclatureService nomenclatureService;

	@Test
	public void testGetNomenclatures() {

		NomenclatureDto nomenclature = nomenclatureService.getNomenclatures();

		assertThat(nomenclature.getNomenclature().size()).isEqualTo(6);
	}
}
