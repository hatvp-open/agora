

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.transformer.impl;

import static fr.hatvp.registre.commons.utils.RegistreUtils.randomLong;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.hatvp.registre.business.config.TestBusinessConfig;
import fr.hatvp.registre.business.transformer.proxy.DemandeChangementMandantTransformer;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.DemandeChangementMandantDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.lists.ChangementContactEnum;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.DemandeChangementMandantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.entity.piece.EspaceOrganisationPieceEntity;

/**
 * Test de la classe {@link DemandeOrganisationTransformerImpl}.
 *
 * @version $Revision$ $Date${0xD}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestBusinessConfig.class }, loader = AnnotationConfigContextLoader.class)
public class DemandeChangementMandantTransformerTest {

	/** Transformer à tester. */
	@Autowired
	private DemandeChangementMandantTransformer demandeChangementMandantTransformer;

	/**
	 * Test de la métode dtoToModel.
	 */
	@Test
	public void testDtoToModel() {
		/*
		 * Création du Dto
		 */
		final DemandeChangementMandantDto dto = new DemandeChangementMandantDto();
		dto.setId(1L);
		dto.setVersion(0);
		dto.setTypeDemande(ChangementContactEnum.REMPLACEMENT_OPERATIONNEL);
		dto.setRepresentantLegal(true);
		dto.setDateDemande(new Date(randomLong(9)));
		dto.setDateValidation(new Date(randomLong(9)));
		dto.setDateRejet(new Date(randomLong(9)));
		dto.setDeclarantDechuInscriptionEspaceId(1L);
		dto.setDeclarantPromuInscriptionEspaceId(2L);
		dto.setEspaceOrganisationId(1L);
		DeclarantDto declarantDto = new DeclarantDto();
		declarantDto.setId(3L);
		dto.setDeclarantDemande(declarantDto);
		EspaceOrganisationPieceDto dtoPiece1 = new EspaceOrganisationPieceDto();
		EspaceOrganisationPieceDto dtoPiece2 = new EspaceOrganisationPieceDto();
		dtoPiece1.setId(1L);
		dtoPiece2.setId(2L);
		dto.setIdentitePieceRepresentantLegal(dtoPiece1);
		dto.setMandatPieceRepresentantLegal(dtoPiece2);

		/*
		 * Transformation
		 */
		final DemandeChangementMandantEntity model = this.demandeChangementMandantTransformer.dtoToModel(dto);

		/*
		 * Assertions
		 */
		Assert.assertNotNull(model);
		Assert.assertEquals(dto.getId(), model.getId());
		Assert.assertEquals(dto.getVersion(), model.getVersion());
		Assert.assertEquals(dto.getTypeDemande(), model.getTypeDemande());
		Assert.assertEquals(dto.getRepresentantLegal(), model.getRepresentantLegal());

		Assert.assertNotNull(model.getPieceIdentiteRepresentantLegal());
		Assert.assertNotNull(model.getMandatRepresentantLegal());
		Assert.assertNotNull(model.getDeclarant());
		Assert.assertNotNull(model.getDeclarantPromu());
		Assert.assertNotNull(model.getDeclarantDechu());
		Assert.assertEquals(dto.getDeclarantDechuInscriptionEspaceId(), model.getDeclarantDechu().getId());
		Assert.assertEquals(dto.getDeclarantPromuInscriptionEspaceId(), model.getDeclarantPromu().getId());
		Assert.assertEquals(dto.getIdentitePieceRepresentantLegal().getId(), model.getPieceIdentiteRepresentantLegal().getId());
		Assert.assertEquals(dto.getMandatPieceRepresentantLegal().getId(), model.getMandatRepresentantLegal().getId());
		Assert.assertEquals(dto.getDeclarantDemande().getId(), model.getDeclarant().getId());

		Assert.assertEquals(dto.getDateRejet(), model.getDateRejet());
		Assert.assertEquals(dto.getDateDemande(), model.getDateDemande());
		Assert.assertEquals(dto.getDateValidation(), model.getDateValidation());
	}

	/**
	 * Test de la métode modelToDto.
	 */
	@Test
	public void testModelToDto() {
		/*
		 * Création du Dto
		 */
		final DemandeChangementMandantEntity entity = new DemandeChangementMandantEntity();
		entity.setId(1L);
		entity.setVersion(0);
		entity.setTypeDemande(ChangementContactEnum.REMPLACEMENT_OPERATIONNEL);
		entity.setRepresentantLegal(true);
		entity.setDateDemande(new Date(randomLong(9)));
		entity.setDateValidation(new Date(randomLong(9)));
		entity.setDateRejet(new Date(randomLong(9)));
		entity.setDeclarantDechu(new InscriptionEspaceEntity(1L));
		entity.setDeclarantPromu(new InscriptionEspaceEntity(2L));
		entity.setEspaceOrganisation(new EspaceOrganisationEntity(1L));
		DeclarantEntity declarantEntity = new DeclarantEntity();
		declarantEntity.setId(3L);
		entity.setDeclarant(declarantEntity);
		EspaceOrganisationPieceEntity entityPiece1 = new EspaceOrganisationPieceEntity();
		EspaceOrganisationPieceEntity entityPiece2 = new EspaceOrganisationPieceEntity();
		entityPiece1.setId(1L);
		entityPiece1.setEspaceOrganisation(new EspaceOrganisationEntity(1L));
		entityPiece2.setId(2L);
		entityPiece2.setEspaceOrganisation(new EspaceOrganisationEntity(1L));
		entity.setPieceIdentiteRepresentantLegal(entityPiece1);
		entity.setMandatRepresentantLegal(entityPiece2);

		/*
		 * Transformation
		 */
		final DemandeChangementMandantDto dto = this.demandeChangementMandantTransformer.modelToDto(entity);

		/*
		 * Assertions
		 */
		Assert.assertNotNull(dto);
		Assert.assertEquals(entity.getId(), dto.getId());
		Assert.assertEquals(entity.getVersion(), dto.getVersion());
		Assert.assertEquals(entity.getTypeDemande(), dto.getTypeDemande());
		Assert.assertEquals(entity.getRepresentantLegal(), dto.getRepresentantLegal());

		Assert.assertNotNull(dto.getIdentitePieceRepresentantLegal());
		Assert.assertNotNull(dto.getMandatPieceRepresentantLegal());
		Assert.assertNotNull(dto.getDeclarantDemande());

		Assert.assertEquals(entity.getDeclarantDechu().getId(), dto.getDeclarantDechuInscriptionEspaceId());
		Assert.assertEquals(entity.getDeclarantPromu().getId(), dto.getDeclarantPromuInscriptionEspaceId());
		Assert.assertEquals(entity.getPieceIdentiteRepresentantLegal().getId(), dto.getIdentitePieceRepresentantLegal().getId());
		Assert.assertEquals(entity.getMandatRepresentantLegal().getId(), dto.getMandatPieceRepresentantLegal().getId());
		Assert.assertEquals(entity.getDeclarant().getId(), dto.getDeclarantDemande().getId());

		Assert.assertEquals(entity.getDateRejet(), dto.getDateRejet());
		Assert.assertEquals(entity.getDateDemande(), dto.getDateDemande());
		Assert.assertEquals(entity.getDateValidation(), dto.getDateValidation());
	}
}
