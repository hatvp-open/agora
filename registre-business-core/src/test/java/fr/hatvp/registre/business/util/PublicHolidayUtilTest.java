/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.assertj.core.util.Arrays;
import org.junit.Test;

import fr.hatvp.registre.business.utils.PublicHolidayUtil;

public class PublicHolidayUtilTest {

	private Map<Integer, List<LocalDate>> movingPublicHolidaysByYear = new HashMap<>();

	@Test
	public void testDate() {

		for (int i = 2018; i < 2021; i++) {
			PublicHolidayUtil ph = new PublicHolidayUtil(i);
			assertThat(ph.getPublicHolidays().containsAll(movingPublicHolidaysByYear.get(i))).isTrue();
		}
	}

	{
		movingPublicHolidaysByYear.put(2018, new ArrayList(Arrays.asList(new LocalDate[] { LocalDate.of(2018, 4, 2), LocalDate.of(2018, 5, 10), LocalDate.of(2018, 5, 21) })));
		movingPublicHolidaysByYear.put(2019, new ArrayList(Arrays.asList(new LocalDate[] { LocalDate.of(2019, 4, 22), LocalDate.of(2019, 5, 30), LocalDate.of(2019, 6, 10) })));
		movingPublicHolidaysByYear.put(2020, new ArrayList(Arrays.asList(new LocalDate[] { LocalDate.of(2020, 4, 13), LocalDate.of(2020, 5, 21), LocalDate.of(2020, 6, 1) })));
	}
}
