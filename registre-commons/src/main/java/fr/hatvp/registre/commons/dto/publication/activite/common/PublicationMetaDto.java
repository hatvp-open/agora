/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication.activite.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;

import java.util.Date;

/**
 * DTO de l'entité Publication, Meta données pour les activités et les exercices
 * @version $Revision$ $Date${0xD}
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PublicationMetaDto
    extends AbstractCommonDto
{
    /** Date de création. */
    @JsonFormat(pattern = "dd/MM/yyyy à HH:mm:ss", timezone = "Europe/Paris")
    private Date publicationDate;

    /** référence au publisher. */
    private String publicateurNom;

    /** Date de dépublication. */
    @JsonFormat(pattern = "dd/MM/yyyy à HH:mm:ss", timezone = "Europe/Paris")
    private Date depublicationDate;

    /** nom complet dépublicateur. */
    private String depublicateurNom;

    /** Date de republication. */
    @JsonFormat(pattern = "dd/MM/yyyy à HH:mm:ss", timezone = "Europe/Paris")
    private Date republicationDate;

    /** nom complet republicateur. */
    private String republicateurNom;

    /** Statut de la publication. */
    @JsonIgnore
    private StatutPublicationEnum statut;

    public Date getPublicationDate()
    {
        return publicationDate;
    }
    public void setPublicationDate(Date publicationDate)
    {
        this.publicationDate = publicationDate;
    }
    public String getPublicateurNom()
    {
        return publicateurNom;
    }
    public void setPublicateurNom(String publicateurNom)
    {
        this.publicateurNom = publicateurNom;
    }
    public Date getDepublicationDate()
    {
        return depublicationDate;
    }
    public void setDepublicationDate(Date depublicationDate)
    {
        this.depublicationDate = depublicationDate;
    }
    public String getDepublicateurNom()
    {
        return depublicateurNom;
    }
    public void setDepublicateurNom(String depublicateurNom)
    {
        this.depublicateurNom = depublicateurNom;
    }
    public Date getRepublicationDate()
    {
        return republicationDate;
    }
    public void setRepublicationDate(Date republicationDate)
    {
        this.republicationDate = republicationDate;
    }
    public String getRepublicateurNom()
    {
        return republicateurNom;
    }
    public void setRepublicateurNom(String republicateurNom)
    {
        this.republicateurNom = republicateurNom;
    }
    public StatutPublicationEnum getStatut()
    {
        return statut;
    }
    public void setStatut(StatutPublicationEnum statut)
    {
        this.statut = statut;
    }
}
