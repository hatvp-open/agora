/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import fr.hatvp.registre.commons.lists.ChangementContactEnum;
import fr.hatvp.registre.commons.lists.StatutDemandeEnum;

/**
 * DTO de l'entité DemandeChangementMandantEntity.
 *
 * @version $Revision$ $Date${0xD}
 */
public class DemandeChangementMandantDto extends AbstractCommonDto {

	/** Date de la demande. */
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date dateDemande = new Date();

	/** Date de validation de la demande. */
	private Date dateValidation;

	/** Date de rejet. */
	private Date dateRejet;

	/** Si representant Legal. */
	private Boolean representantLegal = false;

	/** Référence vers le déclarant auteur de la demande */
	private DeclarantDto declarantDemande;

	/** Référence vers le déclarant auteur de la demande */
	private DeclarantDto declarantPromotion;

	/** Type de demande **/
	private ChangementContactEnum typeDemande;

	/** Statut de la demande **/
	private StatutDemandeEnum statut;

	/** id Référence vers la piece d'identite du representant legal */
	private EspaceOrganisationPieceDto identitePieceRepresentantLegal;

	/** id Référence vers le mandat du representant legal */
	private EspaceOrganisationPieceDto mandatPieceRepresentantLegal;

	/** Référence vers les pieces complémentaires */
	private List<EspaceOrganisationPieceDto> complementPieceRepresentantLegal;

	/** Espace Organisation. */
	private Long espaceOrganisationId;

	/** Référence vers l'inscription du déclarant à promouvoir. */
	private Long declarantDechuInscriptionEspaceId;

	/** Référence vers l'inscription du déclarant à promouvoir. */
	private Long declarantPromuInscriptionEspaceId;

	/** Motif */
	private String motifDemande;

	/** Raison sociale de l'organisation de l'espace */
	private String raisonSociale;

	/**
	 * @return the dateDemande
	 */
	public Date getDateDemande() {
		return dateDemande;
	}

	/**
	 * @param dateDemande
	 *            the dateDemande to set
	 */
	public void setDateDemande(Date dateDemande) {
		this.dateDemande = dateDemande;
	}

	/**
	 * @return the dateValidation
	 */
	public Date getDateValidation() {
		return dateValidation;
	}

	/**
	 * @param dateValidation
	 *            the dateValidation to set
	 */
	public void setDateValidation(Date dateValidation) {
		this.dateValidation = dateValidation;
	}

	/**
	 * @return the dateRejet
	 */
	public Date getDateRejet() {
		return dateRejet;
	}

	/**
	 * @param dateRejet
	 *            the dateRejet to set
	 */
	public void setDateRejet(Date dateRejet) {
		this.dateRejet = dateRejet;
	}

	/**
	 * @return the representantLegal
	 */
	public Boolean getRepresentantLegal() {
		return representantLegal;
	}

	/**
	 * @param representantLegal
	 *            the representantLegal to set
	 */
	public void setRepresentantLegal(Boolean representantLegal) {
		this.representantLegal = representantLegal;
	}

	/**
	 * @return the declarantDemande
	 */
	public DeclarantDto getDeclarantDemande() {
		return declarantDemande;
	}

	/**
	 * @param declarantDemande
	 *            the declarantDemande to set
	 */
	public void setDeclarantDemande(DeclarantDto declarantDemande) {
		this.declarantDemande = declarantDemande;
	}

	/**
	 * @return the declarantPromotion
	 */
	public DeclarantDto getDeclarantPromotion() {
		return declarantPromotion;
	}

	/**
	 * @param declarantPromotion
	 *            the declarantPromotion to set
	 */
	public void setDeclarantPromotion(DeclarantDto declarantPromotion) {
		this.declarantPromotion = declarantPromotion;
	}

	/**
	 * @return the typeDemande
	 */
	public ChangementContactEnum getTypeDemande() {
		return typeDemande;
	}

	/**
	 * @param typeDemande
	 *            the typeDemande to set
	 */
	public void setTypeDemande(ChangementContactEnum typeDemande) {
		this.typeDemande = typeDemande;
	}

	/**
	 * @return the statut
	 */
	public StatutDemandeEnum getStatut() {
		return statut;
	}

	/**
	 * @param statut
	 *            the statut to set
	 */
	public void setStatut(StatutDemandeEnum statut) {
		this.statut = statut;
	}

	/**
	 * @return the identitePieceRepresentantLegal
	 */
	public EspaceOrganisationPieceDto getIdentitePieceRepresentantLegal() {
		return identitePieceRepresentantLegal;
	}

	/**
	 * @param identitePieceRepresentantLegal
	 *            the identitePieceRepresentantLegal to set
	 */
	public void setIdentitePieceRepresentantLegal(EspaceOrganisationPieceDto identitePieceRepresentantLegal) {
		this.identitePieceRepresentantLegal = identitePieceRepresentantLegal;
	}

	/**
	 * @return the mandatPieceRepresentantLegal
	 */
	public EspaceOrganisationPieceDto getMandatPieceRepresentantLegal() {
		return mandatPieceRepresentantLegal;
	}

	/**
	 * @param mandatPieceRepresentantLegal
	 *            the mandatPieceRepresentantLegal to set
	 */
	public void setMandatPieceRepresentantLegal(EspaceOrganisationPieceDto mandatPieceRepresentantLegal) {
		this.mandatPieceRepresentantLegal = mandatPieceRepresentantLegal;
	}

	/**
	 * @return the complementPieceRepresentantLegal
	 */
	public List<EspaceOrganisationPieceDto> getComplementPieceRepresentantLegal() {
		return complementPieceRepresentantLegal;
	}

	/**
	 * @param complementPieceRepresentantLegal
	 *            the complementPieceRepresentantLegal to set
	 */
	public void setComplementPieceRepresentantLegal(List<EspaceOrganisationPieceDto> complementPieceRepresentantLegal) {
		this.complementPieceRepresentantLegal = complementPieceRepresentantLegal;
	}

	/**
	 * @return the espaceOrganisationId
	 */
	public Long getEspaceOrganisationId() {
		return espaceOrganisationId;
	}

	/**
	 * @param espaceOrganisationId
	 *            the espaceOrganisationId to set
	 */
	public void setEspaceOrganisationId(Long espaceOrganisationId) {
		this.espaceOrganisationId = espaceOrganisationId;
	}

	/**
	 * @return the declarantDechuInscriptionEspaceId
	 */
	public Long getDeclarantDechuInscriptionEspaceId() {
		return declarantDechuInscriptionEspaceId;
	}

	/**
	 * @param declarantDechuInscriptionEspaceId
	 *            the declarantDechuInscriptionEspaceId to set
	 */
	public void setDeclarantDechuInscriptionEspaceId(Long declarantDechuInscriptionEspaceId) {
		this.declarantDechuInscriptionEspaceId = declarantDechuInscriptionEspaceId;
	}

	/**
	 * @return the declarantPromuInscriptionEspaceId
	 */
	public Long getDeclarantPromuInscriptionEspaceId() {
		return declarantPromuInscriptionEspaceId;
	}

	/**
	 * @param declarantPromuInscriptionEspaceId
	 *            the declarantPromuInscriptionEspaceId to set
	 */
	public void setDeclarantPromuInscriptionEspaceId(Long declarantPromuInscriptionEspaceId) {
		this.declarantPromuInscriptionEspaceId = declarantPromuInscriptionEspaceId;
	}

	/**
	 * @return the motifDemande
	 */
	public String getMotifDemande() {
		return motifDemande;
	}

	/**
	 * @param motifDemande
	 *            the motifDemande to set
	 */
	public void setMotifDemande(String motifDemande) {
		this.motifDemande = motifDemande;
	}

	/**
	 * @return the raisonSociale
	 */
	public String getRaisonSociale() {
		return raisonSociale;
	}

	/**
	 * @param raisonSociale
	 *            the raisonSociale to set
	 */
	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

}
