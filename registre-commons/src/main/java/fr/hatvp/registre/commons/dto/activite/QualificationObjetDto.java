/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.utils.LocalDateDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateSerializer;

public class QualificationObjetDto
    extends AbstractCommonDto {

    /**
     * Identifiant national de l'organisation
     */
    @JsonProperty("sentence")
    private String objet;

    /**
     * denomination national de l'organisation
     */
    private String denomination;

    /**
     * nom d'usage de l'organisation
     */
    private String nomUsage;

    /**
     * Date où la qualification a été réalisée
     */
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateEnregistrement;

    /**
     * Identifiant de la fiche d'activité
     */
    private String idFiche;

    /**
     * Identifiant national de l'organisation
     */
    private String identifiantNational;

    /**
     * Note attribué à l'objet 0 = Mauvais ; 1 = Bon
     */
    @JsonProperty("valid")
    private boolean note;

    /**
     * Coefficient de confiance lié à la note
     */
    @JsonProperty("confidence")
    private Float coefficiantConfiance;

    public boolean reprise;

    public LocalDate getDateEnregistrement()
    {
        return dateEnregistrement;
    }
    public void setDateEnregistrement(LocalDate dateEnregistrement)
    {
        this.dateEnregistrement = dateEnregistrement;
    }
    public String getIdFiche()
    {
        return idFiche;
    }
    public void setIdFiche(String idFiche)
    {
        this.idFiche = idFiche;
    }
    public String getIdentifiantNational()
    {
        return identifiantNational;
    }
    public void setIdentifiantNational(String identifiantNational)
    {
        this.identifiantNational = identifiantNational;
    }
    public boolean isNote()
    {
        return note;
    }
    public void setNote(boolean note)
    {
        this.note = note;
    }
    public Float getCoefficiantConfiance()
    {
        return coefficiantConfiance;
    }
    public void setCoefficiantConfiance(Float coefficiantConfiance)
    {
        this.coefficiantConfiance = coefficiantConfiance;
    }

    public String getObjet()
    {
        return objet;
    }
    public void setObjet(String objet)
    {
        this.objet = objet;
    }

    public String getDenomination()
    {
        return denomination;
    }
    public void setDenomination(String denomination)
    {
        this.denomination = denomination;
    }
    public String getNomUsage()
    {
        return nomUsage;
    }
    public void setNomUsage(String nomUsage)
    {
        this.nomUsage = nomUsage;
    }

    public boolean isReprise()
    {
        return reprise;
    }
    public void setReprise(boolean reprise)
    {
        this.reprise = reprise;
    }
}
