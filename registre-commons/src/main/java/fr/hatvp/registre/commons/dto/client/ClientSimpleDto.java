/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.client;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.utils.LocalDateDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateSerializer;
import fr.hatvp.registre.commons.utils.LocalDateTimeDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateTimeSerializer;
@JsonInclude(JsonInclude.Include.ALWAYS)
public class ClientSimpleDto extends AbstractCommonDto {

	private String denomination;
	private String nomUsage;
	private String nationalId;
	private boolean isAncienClient;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime dateDesactivation;
	
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateFinContrat;
	
	private String commentaire;

	public ClientSimpleDto(Long id, String denomination, String nomUsage, String nationalId, boolean isAncienClient,
							LocalDateTime dateDesactivation,LocalDate dateFinContrat, String commentaire) {
		this.id = id;
		this.denomination = denomination;
		this.nomUsage = nomUsage;
		this.nationalId = nationalId;
		this.isAncienClient = isAncienClient;
		this.dateDesactivation = dateDesactivation;
		this.dateFinContrat = dateFinContrat;
		this.commentaire = commentaire;
	}

	public ClientSimpleDto(Long id, String denomination, String nomUsage) {
		this.id = id;
		this.denomination = denomination;
		this.nomUsage = nomUsage;
	}

	public ClientSimpleDto(Long id, String denomination) {
		this.id = id;
		this.denomination = denomination;
	}

	/**
	 * 
	 */
	public ClientSimpleDto() {
		super();
	}

	/**
	 * @return the denomination
	 */
	public String getDenomination() {
		return denomination;
	}

	/**
	 * @param denomination
	 *            the denomination to set
	 */
	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	/**
	 * @return the nomUsage
	 */
	public String getNomUsage() {
		return nomUsage;
	}

	/**
	 * @param nomUsage
	 *            the nomUsage to set
	 */
	public void setNomUsage(String nomUsage) {
		this.nomUsage = nomUsage;
	}

	/**
	 * @return the nationalId
	 */
	public String getNationalId() {
		return nationalId;
	}

	/**
	 * @param nationalId
	 *            the nationalId to set
	 */
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	/**
	 * @return the isAncienClient
	 */
	public boolean isAncienClient() {
		return isAncienClient;
	}

	/**
	 * @param isAncienClient
	 *            the isAncienClient to set
	 */
	public void setAncienClient(boolean isAncienClient) {
		this.isAncienClient = isAncienClient;
	}

	public LocalDateTime getDateDesactivation() {
		return dateDesactivation;
	}

	public void setDateDesactivation(LocalDateTime dateDesactivation) {
		this.dateDesactivation = dateDesactivation;
	}

	public LocalDate getDateFinContrat() {
		return dateFinContrat;
	}

	public void setDateFinContrat(LocalDate dateFinContrat) {
		this.dateFinContrat = dateFinContrat;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	

}
