/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists.backoffice;

import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;

/**
 * Classe d'énumération des motifs de refus de création des espaces organisations.
 *
 * @version $Revision$ $Date${0xD}
 */
public enum MotifRefusEspaceEnum
{

    DEMANDE_NON_SERIEUSE(1, "Demande non sérieuse"),
    ID_CONFORME_MANDAT_NON_CONFORME(2, "ID représentant conforme / Mandat non conforme"),
    PIECE_ID_NON_CONFORME(
            3,
            "ID représentant conforme / Mandat conforme / Pièce d'identité du représentant non conforme"),
    ID_NON_CONFORME(4, "ID Représentant non conforme"),
    AUTRE(0, "Autre");

    private final int value;
    private final String label;

    /**
     * Constructeur.
     *
     * @param value
     *         identifiant motif
     */
    MotifRefusEspaceEnum(final int value, final String label)
    {
        this.value = value;
        this.label = label;
    }

    /**
     * Accesseur en lecture du champ <code>value</code>.
     *
     * @return le champ <code>value</code>.
     */
    public int getValue()
    {
        return this.value;
    }

    /**
     * Récupérer le motif du rejet à partir de son id.
     *
     * @param id
     *         identifiant du motif.
     * @return Enum du motif.
     */
    public static MotifRefusEspaceEnum getMotifRefusEspaceEnumById(final int id)
    {
        switch (id) {
            case 1 :
                return DEMANDE_NON_SERIEUSE;
            case 2 :
                return ID_CONFORME_MANDAT_NON_CONFORME;
            case 3 :
                return PIECE_ID_NON_CONFORME;
            case 4 :
                return ID_NON_CONFORME;
            case 0: return AUTRE;
            default: throw new BusinessGlobalException(ErrorMessageEnum.MOTIF_REJET_INTROUVABLE.getMessage());
        }
    }

    @Override
    public String toString()
    {
        return this.label;
    }
}
