/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

/**
 * DTO des types d'organisations.
 *
 * @version $Revision$ $Date${0xD}
 */
public class TypeOrganisationDto {

    private String code;
    private String label;
    private String group;
    private Boolean notifSansChiffreAffaire;

    public String getCode() {
        return this.code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public String getGroup() {
        return this.group;
    }

    public void setGroup(final String group) {
        this.group = group;
    }

	public Boolean getNotifSansChiffreAffaire() {
		return notifSansChiffreAffaire;
	}    
}
