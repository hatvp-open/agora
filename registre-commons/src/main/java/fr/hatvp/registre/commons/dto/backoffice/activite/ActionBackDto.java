/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice.activite;

import java.util.List;
import java.util.Map;

public class ActionBackDto {

	private List<String> tiers;

	private Map<String, List<String>> reponsablesPublics;

	private List<String> decisionsConcernees;

	private List<String> actionsMenees;

	private String responsablePublicAutre;

	private String actionMeneeAutre;

	private String observation;

	/**
	 * @return the tiers
	 */
	public List<String> getTiers() {
		return tiers;
	}

	/**
	 * @param tiers
	 *            the tiers to set
	 */
	public void setTiers(List<String> tiers) {
		this.tiers = tiers;
	}

	/**
	 * @return the reponsablesPublics
	 */
	public Map<String, List<String>> getReponsablesPublics() {
		return reponsablesPublics;
	}

	/**
	 * @param reponsablesPublics
	 *            the reponsablesPublics to set
	 */
	public void setReponsablesPublics(Map<String, List<String>> reponsablesPublics) {
		this.reponsablesPublics = reponsablesPublics;
	}

	/**
	 * @return the decisionsConcernees
	 */
	public List<String> getDecisionsConcernees() {
		return decisionsConcernees;
	}

	/**
	 * @param decisionsConcernees
	 *            the decisionsConcernees to set
	 */
	public void setDecisionsConcernees(List<String> decisionsConcernees) {
		this.decisionsConcernees = decisionsConcernees;
	}

	/**
	 * @return the actionsMenees
	 */
	public List<String> getActionsMenees() {
		return actionsMenees;
	}

	/**
	 * @param actionsMenees
	 *            the actionsMenees to set
	 */
	public void setActionsMenees(List<String> actionsMenees) {
		this.actionsMenees = actionsMenees;
	}

	/**
	 * @return the responsablePublicAutre
	 */
	public String getResponsablePublicAutre() {
		return responsablePublicAutre;
	}

	/**
	 * @param responsablePublicAutre
	 *            the responsablePublicAutre to set
	 */
	public void setResponsablePublicAutre(String responsablePublicAutre) {
		this.responsablePublicAutre = responsablePublicAutre;
	}

	/**
	 * @return the actionMeneeAutre
	 */
	public String getActionMeneeAutre() {
		return actionMeneeAutre;
	}

	/**
	 * @param actionMeneeAutre
	 *            the actionMeneeAutre to set
	 */
	public void setActionMeneeAutre(String actionMeneeAutre) {
		this.actionMeneeAutre = actionMeneeAutre;
	}

	/**
	 * @return the observation
	 */
	public String getObservation() {
		return observation;
	}

	/**
	 * @param observation
	 *            the observation to set
	 */
	public void setObservation(String observation) {
		this.observation = observation;
	}

}
