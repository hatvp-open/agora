

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.hatvp.registre.commons.dto.DirigeantDto;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.validators.civility.Civility;

/**
 * DTO de l'entité DirigeantEntity utilisée pour la publication.
 * Permet de nommer / ignorer les champs pour le Json de la publication. 
 * 
 * @version $Revision$ $Date${0xD}
 */
public class PubDirigeantDto
    extends DirigeantDto
{

    @JsonIgnore
    private Long id;
    
    @JsonIgnore
    private Integer version;
    
    /** Civilité. */
    @Civility(value = {CiviliteEnum.MME, CiviliteEnum.M})
    private CiviliteEnum civilite;

    /** Nom dirigeant */
    @JsonProperty("nom")
    private String nom;

    /** Prénom Dirigeant */
    @JsonProperty("prenom")
    private String prenom;

    /** Fonctions exercées. */
    @JsonProperty("fonction")
    private String fonction;

    /** Id espace organisation */
    @JsonIgnore
    private Long espaceOrganisationId;

}
