/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

public class ActionRepresentationInteretDto extends AbstractCommonDto {

	@NotNull
	@Size(min = 1)
	private Set<Long> reponsablesPublics;

	@NotNull
	@Size(min = 1)
	private Set<Long> decisionsConcernees;

	@NotNull
	@Size(min = 1)
	private Set<Long> actionsMenees;

	private String actionMeneeAutre;
	private String responsablePublicAutre;

	@Valid
	@NotNull
	@Size(min = 1)
	private List<TiersDto> tiers;

	@Size(max = 700)
	private String observation;

	private Long activiteRepresentationInteret;

	/**
	 * @return the reponsablesPublics
	 */
	public Set<Long> getReponsablesPublics() {
		return reponsablesPublics;
	}

	/**
	 * @param reponsablesPublics
	 *            the reponsablesPublics to set
	 */
	public void setReponsablesPublics(Set<Long> reponsablesPublics) {
		this.reponsablesPublics = reponsablesPublics;
	}

	/**
	 * @return the decisionsConcernees
	 */
	public Set<Long> getDecisionsConcernees() {
		return decisionsConcernees;
	}

	/**
	 * @param decisionsConcernees
	 *            the decisionsConcernees to set
	 */
	public void setDecisionsConcernees(Set<Long> decisionsConcernees) {
		this.decisionsConcernees = decisionsConcernees;
	}

	/**
	 * @return the actionsMenees
	 */
	public Set<Long> getActionsMenees() {
		return actionsMenees;
	}

	/**
	 * @param actionsMenees
	 *            the actionsMenees to set
	 */
	public void setActionsMenees(Set<Long> actionsMenees) {
		this.actionsMenees = actionsMenees;
	}

	/**
	 * @return the responsablePublicAutre
	 */
	public String getResponsablePublicAutre() {
		return responsablePublicAutre;
	}

	/**
	 * @param responsablePublicAutre
	 *            the responsablePublicAutre to set
	 */
	public void setResponsablePublicAutre(String responsablePublicAutre) {
		this.responsablePublicAutre = responsablePublicAutre;
	}

	/**
	 * @return the actionMeneeAutre
	 */
	public String getActionMeneeAutre() {
		return actionMeneeAutre;
	}

	/**
	 * @param actionMeneeAutre
	 *            the actionMeneeAutre to set
	 */
	public void setActionMeneeAutre(String actionMeneeAutre) {
		this.actionMeneeAutre = actionMeneeAutre;
	}

	/**
	 * @return the tiers
	 */
	public List<TiersDto> getTiers() {
		return tiers;
	}

	/**
	 * @param tiers
	 *            the tiers to set
	 */
	public void setTiers(List<TiersDto> tiers) {
		this.tiers = tiers;
	}

	/**
	 * @return the observation
	 */
	public String getObservation() {
		return observation;
	}

	/**
	 * @param observation
	 *            the observation to set
	 */
	public void setObservation(String observation) {
		this.observation = observation;
	}

	/**
	 * @return the activiteRepresentationInteret
	 */
	public Long getActiviteRepresentationInteret() {
		return activiteRepresentationInteret;
	}

	/**
	 * @param activiteRepresentationInteret
	 *            the activiteRepresentationInteret to set
	 */
	public void setActiviteRepresentationInteret(Long activiteRepresentationInteret) {
		this.activiteRepresentationInteret = activiteRepresentationInteret;
	}

}
