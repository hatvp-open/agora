/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Classe abstraite des DTO.
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractCommonDto {
    /** Identifiant technique de l'objet. */
    protected Long id;

    /**
     * Champ version.
     */
    protected Integer version;

    /**
     * Accesseur en lecture du champ <code>id</code>.
     *
     * @return le champ <code>id</code>.
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Accesseur en écriture du champ <code>id</code>.
     *
     * @param id la valeur à écrire dans <code>id</code>.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Accesseur en lecture du champ <code>version</code>.
     *
     * @return le champ <code>version</code>.
     */
    public Integer getVersion() {
        return this.version;
    }

    /**
     * Accesseur en écriture du champ <code>version</code>.
     *
     * @param version la valeur à écrire dans <code>version</code>.
     */
    public void setVersion(final Integer version) {
        this.version = version;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public String toString() {
        return this.getClass().getName() + ": id: " + this.id + " version: " + this.version;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.id == null) ? 0 : this.id.hashCode());
        result = (prime * result) + ((this.version == null) ? 0 : this.version.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof AbstractCommonDto)) {
            return false;
        }
        final AbstractCommonDto other = (AbstractCommonDto) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        if (this.version == null) {
            if (other.version != null) {
                return false;
            }
        } else if (!this.version.equals(other.version)) {
            return false;
        }
        return true;
    }

}
