/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.time.LocalDateTime;
import java.util.Date;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.utils.LocalDateTimeDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateTimeSerializer;
import fr.hatvp.registre.commons.validators.civility.Civility;

/**
 * DTO de l'entité CollaborateurEntity.
 *
 * @version $Revision$ $Date${0xD}
 */
public class CollaborateurDto
    extends AbstractCommonDto {

    /** Ordre hierarchique. */
    protected Integer sortOrder;

    /** Civilité. */
    @Civility(value = {CiviliteEnum.MME, CiviliteEnum.M})
    protected CiviliteEnum civilite;

    /** Nom collaborateur. */
    @NotBlank
    @Size(max = 150)
    protected String nom;

    /** Prénom collaborateur. */
    @NotBlank
    @Size(max = 150)
    protected String prenom;

    /** Email. */
    @NotBlank
    @Size(max = 150)
    protected String email;

    /** Fonctions exercées. */
    @Size(max = 150)
    protected String fonction;

    /** Si Collaborateur Inscrit (non additionnel). */
    protected Boolean inscrit;

    /** Si Collaborateur actif. */
    protected Boolean actif;

    /** Identifiant du déclarant si inscrit. */
    protected Long declarantId;

    /** Identifiant de l'espace corporate. */
    protected Long espaceOrganisationId;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime enregistrementDate;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dateDesactivation;
    

    private Date datePublication;


    /**
     * Accesseur en lecture du champ <code>nom</code>.
     *
     * @return le champ <code>nom</code>.
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Accesseur en écriture du champ <code>nom</code>.
     *
     * @param nom la valeur à écrire dans <code>nom</code>.
     */
    public void setNom(final String nom) {
        this.nom = nom;
    }

    /**
     * Accesseur en lecture du champ <code>prenom</code>.
     *
     * @return le champ <code>prenom</code>.
     */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * Accesseur en écriture du champ <code>prenom</code>.
     *
     * @param prenom la valeur à écrire dans <code>prenom</code>.
     */
    public void setPrenom(final String prenom) {
        this.prenom = prenom;
    }

    /**
     * Accesseur en lecture du champ <code>civilite</code>.
     *
     * @return le champ <code>civilite</code>.
     */
    public CiviliteEnum getCivilite() {
        return this.civilite;
    }

    /**
     * Accesseur en écriture du champ <code>civilite</code>.
     *
     * @param civilite la valeur à écrire dans <code>civilite</code>.
     */
    public void setCivilite(final CiviliteEnum civilite) {
        this.civilite = civilite;
    }

    /**
     * Accesseur en lecture du champ <code>inscrit</code>.
     *
     * @return le champ <code>inscrit</code>.
     */
    public Boolean getInscrit() {
        return this.inscrit;
    }

    /**
     * Accesseur en écriture du champ <code>inscrit</code>.
     *
     * @param inscrit la valeur à écrire dans <code>inscrit</code>.
     */
    public void setInscrit(final Boolean inscrit) {
        this.inscrit = inscrit;
    }

    /**
     * Accesseur en lecture du champ <code>actif</code>.
     *
     * @return le champ <code>actif</code>.
     */
    public Boolean getActif() {
        return this.actif;
    }

    /**
     * Accesseur en écriture du champ <code>actif</code>.
     *
     * @param actif la valeur à écrire dans <code>actif</code>.
     */
    public void setActif(final Boolean actif) {
        this.actif = actif;
    }

    /**
     * Accesseur en lecture du champ <code>email</code>.
     *
     * @return le champ <code>email</code>.
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Accesseur en écriture du champ <code>email</code>.
     *
     * @param email la valeur à écrire dans <code>email</code>.
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Gets declarant id.
     *
     * @return the declarant id
     */
    public Long getDeclarantId() {
        return this.declarantId;
    }

    /**
     * Sets declarant id.
     *
     * @param declarantId the declarant id
     */
    public void setDeclarantId(final Long declarantId) {
        this.declarantId = declarantId;
    }

    /**
     * Gets espace corporate id.
     *
     * @return the espace corporate id
     */
    public Long getEspaceOrganisationId() {
        return this.espaceOrganisationId;
    }

    /**
     * Sets espace corporate id.
     *
     * @param espaceOrganisationId the espace corporate id
     */
    public void setEspaceOrganisationId(final Long espaceOrganisationId) {
        this.espaceOrganisationId = espaceOrganisationId;
    }

    /**
     * Gets fonction.
     *
     * @return the fonction
     */
    public String getFonction() {
        return this.fonction;
    }

    /**
     * Sets fonction.
     *
     * @param fonction the fonction
     */
    public void setFonction(final String fonction) {
        this.fonction = fonction;
    }

    /**
     * Gets order.
     *
     * @return the sort order
     */
    public Integer getSortOrder() {
        return this.sortOrder;
    }

    /**
     * Sets order.
     *
     * @param sortOrder the sort order
     */
    public void setSortOrder(final Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public LocalDateTime getEnregistrementDate() {
        return enregistrementDate;
    }

    public void setEnregistrementDate(LocalDateTime enregistrementDate) {
        this.enregistrementDate = enregistrementDate;
    }

    public LocalDateTime getDateDesactivation() {
        return dateDesactivation;
    }

    public void setDateDesactivation(LocalDateTime dateDesactivation) {
        this.dateDesactivation = dateDesactivation;
    }

	public Date getDatePublication() {
		return datePublication;
	}

	public void setDatePublication(Date datePublication) {
		this.datePublication = datePublication;
	}    
}
