/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;

public class EspaceCollaboratifDto extends AbstractCommonDto {

	private Long inscriptionId;

	private Long organisationId;

	private String denomination;

	private String nomUsage;

	private String nomUsageHatvp;

	private String ancienNomHatvp;

	private String sigleHatvp;

	private String identifiant;

	private List<ContactOperationnelSimpleDto> contacts;

	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
	private Date creationDate;

	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
	private Date validationDate;

	private StatutEspaceEnum statut;

	private String finExerciceFiscal;

	private Boolean isPublication;

	private Boolean actif;

	private boolean relancesBloquees;

	private EspaceOrganisationLockBoDto lock;

    private Boolean desinscriptionAgent;
    
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
	private Date dateDesinscription;


	public Long getInscriptionId() {
		return inscriptionId;
	}

	public void setInscriptionId(Long inscriptionId) {
		this.inscriptionId = inscriptionId;
	}

	public Long getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public String getNomUsage() {
		return nomUsage;
	}

	public void setNomUsage(String nomUsage) {
		this.nomUsage = nomUsage;
	}

	public String getNomUsageHatvp() {
		return nomUsageHatvp;
	}

	public void setNomUsageHatvp(String nomUsageHatvp) {
		this.nomUsageHatvp = nomUsageHatvp;
	}

	public String getAncienNomHatvp() {
		return ancienNomHatvp;
	}

	public void setAncienNomHatvp(String ancienNomHatvp) {
		this.ancienNomHatvp = ancienNomHatvp;
	}

	public String getSigleHatvp() {
		return sigleHatvp;
	}

	public void setSigleHatvp(String sigleHatvp) {
		this.sigleHatvp = sigleHatvp;
	}

	public String getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	public List<ContactOperationnelSimpleDto> getContacts() {
		return contacts;
	}

	public void setContacts(List<ContactOperationnelSimpleDto> contacts) {
		this.contacts = contacts;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getValidationDate() {
		return validationDate;
	}

	public void setValidationDate(Date validationDate) {
		this.validationDate = validationDate;
	}

	public StatutEspaceEnum getStatut() {
		return statut;
	}

	public void setStatut(StatutEspaceEnum statut) {
		this.statut = statut;
	}

	public String getFinExerciceFiscal() {
		return finExerciceFiscal;
	}

	public void setFinExerciceFiscal(String finExerciceFiscal) {
		this.finExerciceFiscal = finExerciceFiscal;
	}

	public Boolean getIsPublication() {
		return isPublication;
	}

	public void setIsPublication(Boolean isPublication) {
		this.isPublication = isPublication;
	}

	public Boolean getActif() {
		return actif;
	}

	public void setActif(Boolean actif) {
		this.actif = actif;
	}

	public boolean isRelancesBloquees() {
		return relancesBloquees;
	}

	public void setRelancesBloquees(boolean relancesBloquees) {
		this.relancesBloquees = relancesBloquees;
	}

	public EspaceOrganisationLockBoDto getLock() {
		return lock;
	}

	public void setLock(EspaceOrganisationLockBoDto lock) {
		this.lock = lock;
	}

    public Boolean getDesinscriptionAgent() {
        return desinscriptionAgent;
    }

    public void setDesinscriptionAgent(Boolean desinscriptionAgent) {
        this.desinscriptionAgent = desinscriptionAgent;
    }

	public Date getDateDesinscription() {
		return dateDesinscription;
	}

	public void setDateDesinscription(Date dateDesinscription) {
		this.dateDesinscription = dateDesinscription;
	}  
    
}
