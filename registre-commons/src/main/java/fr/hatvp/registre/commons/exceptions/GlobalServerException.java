/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.exceptions;

import fr.hatvp.registre.commons.dto.erreur.FieldErrorResourceDto;

/**
 * Classe des exceptions serveur.
 *
 * @version $Revision$ $Date${0xD}
 */
public class GlobalServerException
        extends RuntimeException
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = 6620762239742144909L;

    private FieldErrorResourceDto fieldErrorResourceList;
    private int codeError;

    /**
     * Constructeur.
     *
     * @param message
     *         message d'erreur
     * @param cause
     *         cause de l'erreur
     */
    public GlobalServerException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public GlobalServerException(final String message, final FieldErrorResourceDto fieldErrorResource,
                                 final int codeError)
    {
        super(message);
        this.fieldErrorResourceList = fieldErrorResource;
        this.codeError = codeError;

    }

    public FieldErrorResourceDto getFieldErrorResourceList()
    {
        return this.fieldErrorResourceList;
    }

    public int getCodeError()
    {
        return this.codeError;
    }
}
