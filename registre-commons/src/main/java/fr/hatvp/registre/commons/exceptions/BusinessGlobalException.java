/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.exceptions;

import fr.hatvp.registre.commons.dto.erreur.FieldErrorResourceDto;

import java.util.List;

/**
 * Classe des business exceptions.
 *
 * @version $Revision$ $Date${0xD}
 */
public class BusinessGlobalException
        extends RuntimeException
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = 4088221351690246996L;

    private FieldErrorResourceDto fieldErrorResourceList;
    private List<FieldErrorResourceDto> fieldErrorResources;

    private int codeError;

    public BusinessGlobalException(final String message)
    {
        super(message);
    }

    public BusinessGlobalException(final String message, final int codeError)
    {
        super(message);
        this.codeError = codeError;
    }

    public BusinessGlobalException(final String message,
                                   final FieldErrorResourceDto fieldErrorResource, final int codeError)
    {
        super(message);
        this.codeError = codeError;
        this.fieldErrorResourceList = fieldErrorResource;

    }

    public BusinessGlobalException(final String message,
                                   final List<FieldErrorResourceDto> fieldErrorResources, final int codeError)
    {
        super(message);
        this.codeError = codeError;
        this.fieldErrorResources = fieldErrorResources;

    }

    public int getCodeError()
    {
        return this.codeError;
    }

    public FieldErrorResourceDto getFieldErrorResourceList()
    {
        return this.fieldErrorResourceList;
    }

    public List<FieldErrorResourceDto> getFieldErrorResources()
    {
        return this.fieldErrorResources;
    }
}