/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;

/**
 * DTO de l'entité EspaceOrganisationEntity.
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonInclude(JsonInclude.Include.ALWAYS)
public class EspaceOrganisationDto extends AbstractCommonDto {

	/** Date demande de création de l'espace. */
	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
	private Date creationDate;

	/** Statut Validation Espace. */
	private StatutEspaceEnum statut;

	/** Représentant légal. */
	private boolean representantLegal = false;

	/** Déclaration pour un tiers. */

	private boolean nonDeclarationTiers;

	/** association avec un tiers. */
	private boolean nonDeclarationOrgaAppartenance;

	/** Publication active sur espace. */
	private boolean publication = true;

	/** Publication active sur espace. */
	private boolean elementsAPublier;

	/** Id créateur espace organisation. */
	private Long createurId;

	/** Nom complet créateur espace organisation */
	private String createurNomComplet;

	/** Email créateur espace organisation. */
	private String createurEmail;

	/** Etat d'activation de l'espace organisation. */
	private boolean espaceActif;

	/** Logo de l'espace organisation. */
	private byte[] logo;

	/** Type MIME du logo de l'espace organisation. */
	private String logoType;

	/** Liste des contacts opérationnels de l'espace Organisation. */
	private List<DeclarantDto> administrateurs;

	/** Nombre des declarants de l'espace organisation. */
	private Integer nbrDeclarant;

	/** Champ secteurs des activités. */
	private List<SecteurActiviteEnum> listSecteursActivites = new ArrayList<>();

	/** Champ niveaux des interventions. */
	private List<NiveauInterventionEnum> listNiveauIntervention = new ArrayList<>();

	/** Adresse organisation. */
	@Size(max = 500)
	private String adresse;

	/** Code Postal organisation. */
	@Size(max = 10)
	private String codePostal;

	/** Ville organisation. */
	@Size(max = 150)
	private String ville;

	/** Pays organisation. */
	@Size(max = 150)
	private String pays;

	/** Publier mon adresse physique. */
	private Boolean nonPublierMonAdressePhysique;

	/** Email de contact. */
	@Size(max = 150)
	private String emailDeContact;

	/** Publier mon adresse email. */
	private Boolean nonPublierMonAdresseEmail;

	@Size(max = 15)
	/** Téléphone de contact. */
	private String telephoneDeContact;

	/** Publier mon numéro de téléphone de contact. */
	private Boolean nonPublierMonTelephoneDeContact;

	/** Identifiant national de l'organisation. */
	@Size(max = 150)
	private String nationalId;

	/** Type identifiant national (SIREN/RNA). */
	private TypeIdentifiantNationalEnum typeIdentifiantNational;

	/** Denomination de l'organisation. */
	@Size(max = 150)
	private String denomination;

	/** Nom d'usage de l'organisation. */
	@Size(max = 150)
	private String nomUsage;
	/** Nom d'usage Interne de l'organisation. */
	@Size(max = 150)
	private String nomUsageHatvp;
	@Size(max = 150)
	private String ancienNomHatvp;
	/** Sigle de l'organisation. */
	@Size(max = 150)
	private String sigleHatvp;
	/** URL Site Web. */
	@Size(max = 150)
	private String lienSiteWeb;

	/** URL liste des tiers */
	@Size(max = 150)
	private String lienListeTiers;

	/** URL Page Twitter. */
	@Size(max = 150)
	private String lienPageTwitter;

	/** URL Page Facebook. */
	@Size(max = 150)
	private String lienPageFacebook;

	/** URL Page LinkedIn. */
	@Size(max = 150)
	private String lienPageLinkedin;

	/**
	 * Données financières.
	 */
	/** Catégorie de l'organisation. */
	private TypeOrganisationEnum categorieOrganisation;

	@Range(min = 0, max = 999999999999L)
	/** CA Organisation. */
	private Long chiffreAffaire;

	@Range(min = 1900, max = 3000)
	/** Année Chiffre d'affaire. */
	private Integer anneeChiffreAffaire;

	@Range(min = 0, max = 99999999L)
	/** Montant des dépenses liées aux actions de lobbying pour l'année précédente. */
	private Long depenses;

	@Range(min = 1900, max = 3000)
	/** Années des dépenses. */
	private Integer anneeDepenses;

	@Range(min = 0, max = 999999999999L)
	/** Chiffre d'Affaire. */
	private Long chiffreAffaireRepresentationInteret;

	@Range(min = 1900, max = 3000)
	/** Année Chiffre d'Affaire. */
	private Integer anneeChiffreAffaireInteret;

	@Range(min = 0, max = 999999999999L)
	/** Budget total. */
	private Long budgetTotal;

	@Range(min = 1900, max = 3000)
	/** Année du budget. */
	private Integer anneeBudget;

	@Range(min = 0, max = 999)
	/** Nombre de personnes employées. */
	private Integer nPersonnesEmployees;

	/** Existance d'un exercice comptable */
	private Boolean nonExerciceComptable;

	/** si une publication aa déjà été faite */
	private Boolean hasPublication;

	/** date de la dernière publication */
	private String dateDernierePublication;
	
	private String datePremierePublication;

	/**
	 * Organisations professionnelles ou syndicales et associations d'appartenance en lien avec la représentation d'intérêts.
	 */
	private List<AssoClientDto> assosAppartenance = new ArrayList<>();

	/** Nom dirigeat Organisation */
	private List<DirigeantDto> dirigeants = new ArrayList<>();

	/** Collaborateurs de l'espace organisation. */
	private List<CollaborateurDto> collaborateurs = new ArrayList<>();

	/** Liste des clients. */
	private List<AssoClientDto> clients = new ArrayList<>();
	
	/** Liste des anciens clients. */
	private List<AssoClientDto> clientsAnciens = new ArrayList<>();

	/** date de cloture de l'exercice comptable */
	private String finExerciceFiscal;

	private DesinscriptionDto desinscription;

	/**
	 * Accesseur en lecture du champ <code>creationDate</code>.
	 *
	 * @return le champ <code>creationDate</code>.
	 */
	public Date getCreationDate() {
		return this.creationDate;
	}

	/**
	 * Accesseur en écriture du champ <code>creationDate</code>.
	 *
	 * @param creationDate
	 *            la valeur à écrire dans <code>creationDate</code>.
	 */
	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Accesseur en lecture du champ <code>statut</code>.
	 *
	 * @return le champ <code>statut</code>.
	 */
	public StatutEspaceEnum getStatut() {
		return this.statut;
	}

	/**
	 * Accesseur en écriture du champ <code>statut</code>.
	 *
	 * @param statut
	 *            la valeur à écrire dans <code>statut</code>.
	 */
	public void setStatut(final StatutEspaceEnum statut) {
		this.statut = statut;
	}

	/**
	 * Accesseur en lecture du champ <code>nonDeclarationTiers</code>.
	 *
	 * @return le champ <code>nonDeclarationTiers</code>.
	 */
	public boolean isNonDeclarationTiers() {
		return this.nonDeclarationTiers;
	}

	/**
	 * Accesseur en écriture du champ <code>nonDeclarationTiers</code>.
	 *
	 * @param nonDeclarationTiers
	 *            la valeur à écrire dans <code>nonDeclarationTiers</code>.
	 */
	public void setNonDeclarationTiers(final boolean nonDeclarationTiers) {
		this.nonDeclarationTiers = nonDeclarationTiers;
	}

	/**
	 * Accesseur en lecture du champ <code>createurId</code>.
	 *
	 * @return le champ <code>createurId</code>.
	 */
	public Long getCreateurId() {
		return this.createurId;
	}

	/**
	 * Accesseur en écriture du champ <code>createurId</code>.
	 *
	 * @param createurId
	 *            la valeur à écrire dans <code>createurId</code>.
	 */
	public void setCreateurId(final Long createurId) {
		this.createurId = createurId;
	}

	/**
	 * Accesseur en lecture du champ <code>createurNomComplet</code>.
	 *
	 * @return le champ <code>createurNomComplet</code>.
	 */
	public String getCreateurNomComplet() {
		return this.createurNomComplet;
	}

	/**
	 * Accesseur en écriture du champ <code>createurNomComplet</code>.
	 *
	 * @param createurNomComplet
	 *            la valeur à écrire dans <code>createurNomComplet</code>.
	 */
	public void setCreateurNomComplet(final String createurNomComplet) {
		this.createurNomComplet = createurNomComplet;
	}

	/**
	 * Accesseur en lecture du champ <code>createurEmail</code>.
	 *
	 * @return le champ <code>createurEmail</code>.
	 */
	public String getCreateurEmail() {
		return this.createurEmail;
	}

	/**
	 * Accesseur en écriture du champ <code>createurEmail</code>.
	 *
	 * @param createurEmail
	 *            la valeur à écrire dans <code>createurEmail</code>.
	 */
	public void setCreateurEmail(final String createurEmail) {
		this.createurEmail = createurEmail;
	}

	/**
	 * Accesseur en lecture du champ <code>espaceActif</code>.
	 *
	 * @return le champ <code>espaceActif</code>.
	 */
	public boolean isEspaceActif() {
		return this.espaceActif;
	}

	/**
	 * Accesseur en écriture du champ <code>espaceActif</code>.
	 *
	 * @param espaceActif
	 *            la valeur à écrire dans <code>espaceActif</code>.
	 */
	public void setEspaceActif(final boolean espaceActif) {
		this.espaceActif = espaceActif;
	}

	/**
	 * Accesseur en lecture du champ <code>logo</code>.
	 *
	 * @return le champ <code>logo</code>.
	 */
	public byte[] getLogo() {
		return this.logo;
	}

	/**
	 * Accesseur en écriture du champ <code>logo</code>.
	 *
	 * @param logo
	 *            la valeur à écrire dans <code>logo</code>.
	 */
	public void setLogo(final byte[] logo) {
		this.logo = logo;
	}

	/**
	 * Accesseur en lecture du champ <code>logoType</code>.
	 *
	 * @return le champ <code>logoType</code>.
	 */
	public String getLogoType() {
		return this.logoType;
	}

	/**
	 * Accesseur en écriture du champ <code>logoType</code>.
	 *
	 * @param logoType
	 *            la valeur à écrire dans <code>logoType</code>.
	 */
	public void setLogoType(final String logoType) {
		this.logoType = logoType;
	}

	/**
	 * Accesseur en lecture du champ <code>administrateurs</code>.
	 *
	 * @return le champ <code>administrateurs</code>.
	 */
	public List<DeclarantDto> getAdministrateurs() {
		return this.administrateurs;
	}

	/**
	 * Accesseur en écriture du champ <code>administrateurs</code>.
	 *
	 * @param administrateurs
	 *            la valeur à écrire dans <code>administrateurs</code>.
	 */
	public void setAdministrateurs(final List<DeclarantDto> administrateurs) {
		this.administrateurs = administrateurs;
	}

	/**
	 * Accesseur en lecture du champ <code>nbrDeclarant</code>.
	 *
	 * @return le champ <code>nbrDeclarant</code>.
	 */
	public Integer getNbrDeclarant() {
		return this.nbrDeclarant;
	}

	/**
	 * Accesseur en écriture du champ <code>nbrDeclarant</code>.
	 *
	 * @param nbrDeclarant
	 *            la valeur à écrire dans <code>nbrDeclarant</code>.
	 */
	public void setNbrDeclarant(final Integer nbrDeclarant) {
		this.nbrDeclarant = nbrDeclarant;
	}

	/**
	 * Accesseur en lecture du champ <code>adresse</code>.
	 *
	 * @return le champ <code>adresse</code>.
	 */
	public String getAdresse() {
		return this.adresse;
	}

	/**
	 * Accesseur en écriture du champ <code>adresse</code>.
	 *
	 * @param adresse
	 *            la valeur à écrire dans <code>adresse</code>.
	 */
	public void setAdresse(final String adresse) {
		this.adresse = adresse;
	}

	/**
	 * Accesseur en lecture du champ <code>codePostal</code>.
	 *
	 * @return le champ <code>codePostal</code>.
	 */
	public String getCodePostal() {
		return this.codePostal;
	}

	/**
	 * Accesseur en écriture du champ <code>codePostal</code>.
	 *
	 * @param codePostal
	 *            la valeur à écrire dans <code>codePostal</code>.
	 */
	public void setCodePostal(final String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * Accesseur en lecture du champ <code>ville</code>.
	 *
	 * @return le champ <code>ville</code>.
	 */
	public String getVille() {
		return this.ville;
	}

	/**
	 * Accesseur en écriture du champ <code>ville</code>.
	 *
	 * @param ville
	 *            la valeur à écrire dans <code>ville</code>.
	 */
	public void setVille(final String ville) {
		this.ville = ville;
	}

	/**
	 * Accesseur en lecture du champ <code>pays</code>.
	 *
	 * @return le champ <code>pays</code>.
	 */
	public String getPays() {
		return this.pays;
	}

	/**
	 * Accesseur en écriture du champ <code>pays</code>.
	 *
	 * @param pays
	 *            la valeur à écrire dans <code>pays</code>.
	 */
	public void setPays(final String pays) {
		this.pays = pays;
	}

	/**
	 * Accesseur en lecture du champ <code>nonPublierMonAdressePhysique</code>.
	 *
	 * @return le champ <code>nonPublierMonAdressePhysique</code>.
	 */
	public Boolean getNonPublierMonAdressePhysique() {
		return this.nonPublierMonAdressePhysique;
	}

	/**
	 * Accesseur en écriture du champ <code>nonPublierMonAdressePhysique</code>.
	 *
	 * @param nonPublierMonAdressePhysique
	 *            la valeur à écrire dans <code>nonPublierMonAdressePhysique</code>.
	 */
	public void setNonPublierMonAdressePhysique(final Boolean nonPublierMonAdressePhysique) {
		this.nonPublierMonAdressePhysique = nonPublierMonAdressePhysique;
	}

	/**
	 * Accesseur en lecture du champ <code>emailDeContact</code>.
	 *
	 * @return le champ <code>emailDeContact</code>.
	 */
	public String getEmailDeContact() {
		return this.emailDeContact;
	}

	/**
	 * Accesseur en écriture du champ <code>emailDeContact</code>.
	 *
	 * @param emailDeContact
	 *            la valeur à écrire dans <code>emailDeContact</code>.
	 */
	public void setEmailDeContact(final String emailDeContact) {
		this.emailDeContact = emailDeContact;
	}

	/**
	 * Accesseur en lecture du champ <code>nonPublierMonAdresseEmail</code>.
	 *
	 * @return le champ <code>nonPublierMonAdresseEmail</code>.
	 */
	public Boolean getNonPublierMonAdresseEmail() {
		return this.nonPublierMonAdresseEmail;
	}

	/**
	 * Accesseur en écriture du champ <code>nonPublierMonAdresseEmail</code>.
	 *
	 * @param nonPublierMonAdresseEmail
	 *            la valeur à écrire dans <code>nonPublierMonAdresseEmail</code>.
	 */
	public void setNonPublierMonAdresseEmail(final Boolean nonPublierMonAdresseEmail) {
		this.nonPublierMonAdresseEmail = nonPublierMonAdresseEmail;
	}

	/**
	 * Accesseur en lecture du champ <code>telephoneDeContact</code>.
	 *
	 * @return le champ <code>telephoneDeContact</code>.
	 */
	public String getTelephoneDeContact() {
		return this.telephoneDeContact;
	}

	/**
	 * Accesseur en écriture du champ <code>telephoneDeContact</code>.
	 *
	 * @param telephoneDeContact
	 *            la valeur à écrire dans <code>telephoneDeContact</code>.
	 */
	public void setTelephoneDeContact(final String telephoneDeContact) {
		this.telephoneDeContact = telephoneDeContact;
	}

	/**
	 * Accesseur en lecture du champ <code>nonPublierMonTelephoneDeContact</code>.
	 *
	 * @return le champ <code>nonPublierMonTelephoneDeContact</code>.
	 */
	public Boolean getNonPublierMonTelephoneDeContact() {
		return this.nonPublierMonTelephoneDeContact;
	}

	/**
	 * Accesseur en écriture du champ <code>nonPublierMonTelephoneDeContact</code>.
	 *
	 * @param nonPublierMonTelephoneDeContact
	 *            la valeur à écrire dans <code>nonPublierMonTelephoneDeContact</code>.
	 */
	public void setNonPublierMonTelephoneDeContact(final Boolean nonPublierMonTelephoneDeContact) {
		this.nonPublierMonTelephoneDeContact = nonPublierMonTelephoneDeContact;
	}

	/**
	 * Accesseur en lecture du champ <code>nationalId</code>.
	 *
	 * @return le champ <code>nationalId</code>.
	 */
	public String getNationalId() {
		return this.nationalId;
	}

	/**
	 * Accesseur en écriture du champ <code>nationalId</code>.
	 *
	 * @param nationalId
	 *            la valeur à écrire dans <code>nationalId</code>.
	 */
	public void setNationalId(final String nationalId) {
		this.nationalId = nationalId;
	}

	/**
	 * Accesseur en lecture du champ <code>typeIdentifiantNational</code>.
	 *
	 * @return le champ <code>typeIdentifiantNational</code>.
	 */
	public TypeIdentifiantNationalEnum getTypeIdentifiantNational() {
		return this.typeIdentifiantNational;
	}

	/**
	 * Accesseur en écriture du champ <code>typeIdentifiantNational</code>.
	 *
	 * @param typeIdentifiantNational
	 *            la valeur à écrire dans <code>typeIdentifiantNational</code>.
	 */
	public void setTypeIdentifiantNational(final TypeIdentifiantNationalEnum typeIdentifiantNational) {
		this.typeIdentifiantNational = typeIdentifiantNational;
	}

	/**
	 * Accesseur en lecture du champ <code>denomination</code>.
	 *
	 * @return le champ <code>denomination</code>.
	 */
	public String getDenomination() {
		return this.denomination;
	}

	/**
	 * Accesseur en écriture du champ <code>denomination</code>.
	 *
	 * @param denomination
	 *            la valeur à écrire dans <code>denomination</code>.
	 */
	public void setDenomination(final String denomination) {
		this.denomination = denomination;
	}

	/**
	 * Accesseur en lecture du champ <code>nomUsage</code>.
	 *
	 * @return le champ <code>nomUsage</code>.
	 */
	public String getNomUsage() {
		return this.nomUsage;
	}

	/**
	 * Accesseur en écriture du champ <code>nomUsage</code>.
	 *
	 * @param nomUsage
	 *            la valeur à écrire dans <code>nomUsage</code>.
	 */
	public void setNomUsage(final String nomUsage) {
		this.nomUsage = nomUsage;
	}

	
	/**
	 * Accesseur en lecture du champ <code>nomUsageHatvp</code>.
	 *
	 * @return le champ <code>nomUsageHatvp</code>.
	 */
	public String getNomUsageHatvp() {
		return this.nomUsageHatvp;
	}

	/**
	 * Accesseur en écriture du champ <code>nomUsageHatvp</code>.
	 *
	 * @param nomUsageHatvp
	 *            la valeur à écrire dans <code>nomUsageHatvp</code>.
	 */
	public void setNomUsageHatvp(final String nomUsageHatvp) {
		this.nomUsageHatvp = nomUsageHatvp;
	}
	
	/**
	 * Accesseur en lecture du champ <code>sigleHatvp</code>.
	 *
	 * @return le champ <code>sigleHatvp</code>.
	 */
	public String getSigleHatvp() {
		return this.sigleHatvp;
	}

	/**
	 * Accesseur en écriture du champ <code>sigleHatvp</code>.
	 *
	 * @param sigleHatvp
	 *            la valeur à écrire dans <code>sigleHatvp</code>.
	 */
	public void setSigleHatvp(final String sigleHatvp) {
		this.sigleHatvp = sigleHatvp;
	}
	public String getAncienNomHatvp() {
		return this.ancienNomHatvp;
	}
	public void setAncienNomHatvp(final String ancienNomHatvp) {
		this.ancienNomHatvp = ancienNomHatvp;
	}
	/**
	 * Accesseur en lecture du champ <code>lienSiteWeb</code>.
	 *
	 * @return le champ <code>lienSiteWeb</code>.
	 */
	public String getLienSiteWeb() {
		return this.lienSiteWeb;
	}

	/**
	 * Accesseur en écriture du champ <code>lienSiteWeb</code>.
	 *
	 * @param lienSiteWeb
	 *            la valeur à écrire dans <code>lienSiteWeb</code>.
	 */
	public void setLienSiteWeb(final String lienSiteWeb) {
		this.lienSiteWeb = lienSiteWeb;
	}

	/**
	 * @return the lienListeTiers
	 */
	public String getLienListeTiers() {
		return lienListeTiers;
	}

	/**
	 * @param lienListeTiers
	 *            the lienListeTiers to set
	 */
	public void setLienListeTiers(String lienListeTiers) {
		this.lienListeTiers = lienListeTiers;
	}

	/**
	 * Accesseur en lecture du champ <code>lienPageTwitter</code>.
	 *
	 * @return le champ <code>lienPageTwitter</code>.
	 */
	public String getLienPageTwitter() {
		return this.lienPageTwitter;
	}

	/**
	 * Accesseur en écriture du champ <code>lienPageTwitter</code>.
	 *
	 * @param lienPageTwitter
	 *            la valeur à écrire dans <code>lienPageTwitter</code>.
	 */
	public void setLienPageTwitter(final String lienPageTwitter) {
		this.lienPageTwitter = lienPageTwitter;
	}

	/**
	 * Accesseur en lecture du champ <code>lienPageFacebook</code>.
	 *
	 * @return le champ <code>lienPageFacebook</code>.
	 */
	public String getLienPageFacebook() {
		return this.lienPageFacebook;
	}

	/**
	 * Accesseur en écriture du champ <code>lienPageFacebook</code>.
	 *
	 * @param lienPageFacebook
	 *            la valeur à écrire dans <code>lienPageFacebook</code>.
	 */
	public void setLienPageFacebook(final String lienPageFacebook) {
		this.lienPageFacebook = lienPageFacebook;
	}

	/**
	 * Accesseur en lecture du champ <code>lienPageLinkedin</code>.
	 *
	 * @return le champ <code>lienPageLinkedin</code>.
	 */
	public String getLienPageLinkedin() {
		return this.lienPageLinkedin;
	}

	/**
	 * Accesseur en écriture du champ <code>lienPageLinkedin</code>.
	 *
	 * @param lienPageLinkedin
	 *            la valeur à écrire dans <code>lienPageLinkedin</code>.
	 */
	public void setLienPageLinkedin(final String lienPageLinkedin) {
		this.lienPageLinkedin = lienPageLinkedin;
	}

	/**
	 * Accesseur en lecture du champ <code>categorieOrganisation</code>.
	 *
	 * @return le champ <code>categorieOrganisation</code>.
	 */
	public TypeOrganisationEnum getCategorieOrganisation() {
		return this.categorieOrganisation;
	}

	/**
	 * Accesseur en écriture du champ <code>categorieOrganisation</code>.
	 *
	 * @param categorieOrganisation
	 *            la valeur à écrire dans <code>categorieOrganisation</code>.
	 */
	public void setCategorieOrganisation(final TypeOrganisationEnum categorieOrganisation) {
		this.categorieOrganisation = categorieOrganisation;
	}

	/**
	 * Accesseur en lecture du champ <code>chiffreAffaire</code>.
	 *
	 * @return le champ <code>chiffreAffaire</code>.
	 */
	public Long getChiffreAffaire() {
		return this.chiffreAffaire;
	}

	/**
	 * Accesseur en écriture du champ <code>chiffreAffaire</code>.
	 *
	 * @param chiffreAffaire
	 *            la valeur à écrire dans <code>chiffreAffaire</code>.
	 */
	public void setChiffreAffaire(final Long chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	/**
	 * Accesseur en lecture du champ <code>anneeChiffreAffaire</code>.
	 *
	 * @return le champ <code>anneeChiffreAffaire</code>.
	 */
	public Integer getAnneeChiffreAffaire() {
		return this.anneeChiffreAffaire;
	}

	/**
	 * Accesseur en écriture du champ <code>anneeChiffreAffaire</code>.
	 *
	 * @param anneeChiffreAffaire
	 *            la valeur à écrire dans <code>anneeChiffreAffaire</code>.
	 */
	public void setAnneeChiffreAffaire(final Integer anneeChiffreAffaire) {
		this.anneeChiffreAffaire = anneeChiffreAffaire;
	}

	/**
	 * Accesseur en lecture du champ <code>depenses</code>.
	 *
	 * @return le champ <code>depenses</code>.
	 */
	public Long getDepenses() {
		return this.depenses;
	}

	/**
	 * Accesseur en écriture du champ <code>depenses</code>.
	 *
	 * @param depenses
	 *            la valeur à écrire dans <code>depenses</code>.
	 */
	public void setDepenses(final Long depenses) {
		this.depenses = depenses;
	}

	/**
	 * Accesseur en lecture du champ <code>anneeDepenses</code>.
	 *
	 * @return le champ <code>anneeDepenses</code>.
	 */
	public Integer getAnneeDepenses() {
		return this.anneeDepenses;
	}

	/**
	 * Accesseur en écriture du champ <code>anneeDepenses</code>.
	 *
	 * @param anneeDepenses
	 *            la valeur à écrire dans <code>anneeDepenses</code>.
	 */
	public void setAnneeDepenses(final Integer anneeDepenses) {
		this.anneeDepenses = anneeDepenses;
	}

	/**
	 * Accesseur en lecture du champ <code>chiffreAffaireRepresentationInteret</code>.
	 *
	 * @return le champ <code>chiffreAffaireRepresentationInteret</code>.
	 */
	public Long getChiffreAffaireRepresentationInteret() {
		return this.chiffreAffaireRepresentationInteret;
	}

	/**
	 * Accesseur en écriture du champ <code>chiffreAffaireRepresentationInteret</code>.
	 *
	 * @param chiffreAffaireRepresentationInteret
	 *            la valeur à écrire dans <code>chiffreAffaireRepresentationInteret</code>.
	 */
	public void setChiffreAffaireRepresentationInteret(final Long chiffreAffaireRepresentationInteret) {
		this.chiffreAffaireRepresentationInteret = chiffreAffaireRepresentationInteret;
	}

	/**
	 * Accesseur en lecture du champ <code>anneeChiffreAffaireInteret</code>.
	 *
	 * @return le champ <code>anneeChiffreAffaireInteret</code>.
	 */
	public Integer getAnneeChiffreAffaireInteret() {
		return this.anneeChiffreAffaireInteret;
	}

	/**
	 * Accesseur en écriture du champ <code>anneeChiffreAffaireInteret</code>.
	 *
	 * @param anneeChiffreAffaireInteret
	 *            la valeur à écrire dans <code>anneeChiffreAffaireInteret</code>.
	 */
	public void setAnneeChiffreAffaireInteret(final Integer anneeChiffreAffaireInteret) {
		this.anneeChiffreAffaireInteret = anneeChiffreAffaireInteret;
	}

	/**
	 * Accesseur en lecture du champ <code>budgetTotal</code>.
	 *
	 * @return le champ <code>budgetTotal</code>.
	 */
	public Long getBudgetTotal() {
		return this.budgetTotal;
	}

	/**
	 * Accesseur en écriture du champ <code>budgetTotal</code>.
	 *
	 * @param budgetTotal
	 *            la valeur à écrire dans <code>budgetTotal</code>.
	 */
	public void setBudgetTotal(final Long budgetTotal) {
		this.budgetTotal = budgetTotal;
	}

	/**
	 * Accesseur en lecture du champ <code>anneeBudget</code>.
	 *
	 * @return le champ <code>anneeBudget</code>.
	 */
	public Integer getAnneeBudget() {
		return this.anneeBudget;
	}

	/**
	 * Accesseur en écriture du champ <code>anneeBudget</code>.
	 *
	 * @param anneeBudget
	 *            la valeur à écrire dans <code>anneeBudget</code>.
	 */
	public void setAnneeBudget(final Integer anneeBudget) {
		this.anneeBudget = anneeBudget;
	}

	/**
	 * Accesseur en lecture du champ <code>nPersonnesEmployees</code>.
	 *
	 * @return le champ <code>nPersonnesEmployees</code>.
	 */
	public Integer getnPersonnesEmployees() {
		return this.nPersonnesEmployees;
	}

	/**
	 * Accesseur en écriture du champ <code>nPersonnesEmployees</code>.
	 *
	 * @param nPersonnesEmployees
	 *            la valeur à écrire dans <code>nPersonnesEmployees</code>.
	 */
	public void setnPersonnesEmployees(final Integer nPersonnesEmployees) {
		this.nPersonnesEmployees = nPersonnesEmployees;
	}

	/**
	 * Accesseur en lecture du champ <code>assosAppartenance</code>.
	 *
	 * @return le champ <code>assosAppartenance</code>.
	 */
	public List<AssoClientDto> getAssosAppartenance() {
		return this.assosAppartenance;
	}

	/**
	 * Accesseur en écriture du champ <code>assosAppartenance</code>.
	 *
	 * @param assosAppartenance
	 *            la valeur à écrire dans <code>assosAppartenance</code>.
	 */
	public void setAssosAppartenance(final List<AssoClientDto> assosAppartenance) {
		this.assosAppartenance = assosAppartenance;
	}

	/**
	 * Accesseur en lecture du champ <code>dirigeants</code>.
	 *
	 * @return le champ <code>dirigeants</code>.
	 */
	public List<DirigeantDto> getDirigeants() {
		return this.dirigeants;
	}

	/**
	 * Accesseur en écriture du champ <code>dirigeants</code>.
	 *
	 * @param dirigeants
	 *            la valeur à écrire dans <code>dirigeants</code>.
	 */
	public void setDirigeants(final List<DirigeantDto> dirigeants) {
		this.dirigeants = dirigeants;
	}

	/**
	 * Accesseur en lecture du champ <code>collaborateurs</code>.
	 *
	 * @return le champ <code>collaborateurs</code>.
	 */
	public List<CollaborateurDto> getCollaborateurs() {
		return this.collaborateurs;
	}

	/**
	 * Accesseur en écriture du champ <code>collaborateurs</code>.
	 *
	 * @param collaborateurs
	 *            la valeur à écrire dans <code>collaborateurs</code>.
	 */
	public void setCollaborateurs(final List<CollaborateurDto> collaborateurs) {
		this.collaborateurs = collaborateurs;
	}

	/**
	 * Accesseur en lecture du champ <code>clients</code>.
	 *
	 * @return le champ <code>clients</code>.
	 */
	public List<AssoClientDto> getClients() {
		return this.clients;
	}

	/**
	 * Accesseur en écriture du champ <code>clients</code>.
	 *
	 * @param clients
	 *            la valeur à écrire dans <code>clients</code>.
	 */
	public void setClients(final List<AssoClientDto> clients) {
		this.clients = clients;
	}

	/**
	 * Accesseur en lecture du champ <code>nonDeclarationOrgaAppartenance</code>.
	 *
	 * @return le champ <code>nonDeclarationOrgaAppartenance</code>.
	 */
	public boolean isNonDeclarationOrgaAppartenance() {
		return this.nonDeclarationOrgaAppartenance;
	}

	/**
	 * Accesseur en écriture du champ <code>nonDeclarationOrgaAppartenance</code>.
	 *
	 * @param nonDeclarationOrgaAppartenance
	 *            la valeur à écrire dans <code>nonDeclarationOrgaAppartenance</code>.
	 */
	public void setNonDeclarationOrgaAppartenance(final boolean nonDeclarationOrgaAppartenance) {
		this.nonDeclarationOrgaAppartenance = nonDeclarationOrgaAppartenance;
	}

	/**
	 * Is publication boolean.
	 *
	 * @return the boolean
	 */
	public boolean isPublication() {
		return this.publication;
	}

	/**
	 * Sets publication.
	 *
	 * @param publication
	 *            the publication
	 */
	public void setPublication(final boolean publication) {
		this.publication = publication;
	}

	/**
	 * Accesseur en lecture du champ <code>listSecteursActivites</code>.
	 *
	 * @return le champ <code>listSecteursActivites</code>.
	 */
	public List<SecteurActiviteEnum> getListSecteursActivites() {
		return this.listSecteursActivites;
	}

	/**
	 * Accesseur en écriture du champ <code>listSecteursActivites</code>.
	 *
	 * @param listSecteursActivites
	 *            la valeur à écrire dans <code>listSecteursActivites</code>.
	 */
	public void setListSecteursActivites(final List<SecteurActiviteEnum> listSecteursActivites) {
		this.listSecteursActivites = listSecteursActivites;
	}

	/**
	 * Accesseur en lecture du champ <code>listNiveauIntervention</code>.
	 *
	 * @return le champ <code>listNiveauIntervention</code>.
	 */
	public List<NiveauInterventionEnum> getListNiveauIntervention() {
		return this.listNiveauIntervention;
	}

	/**
	 * Accesseur en écriture du champ <code>listNiveauIntervention</code>.
	 *
	 * @param listNiveauIntervention
	 *            la valeur à écrire dans <code>listNiveauIntervention</code>.
	 */
	public void setListNiveauIntervention(final List<NiveauInterventionEnum> listNiveauIntervention) {
		this.listNiveauIntervention = listNiveauIntervention;
	}

	/**
	 * Is representant legal boolean.
	 *
	 * @return the boolean
	 */
	public boolean isRepresentantLegal() {
		return this.representantLegal;
	}

	/**
	 * Sets representant legal.
	 *
	 * @param representantLegal
	 *            the representant legal
	 */
	public void setRepresentantLegal(final boolean representantLegal) {
		this.representantLegal = representantLegal;
	}

	/**
	 * @return the finExerciceFiscal
	 */
	public String getFinExerciceFiscal() {
		return finExerciceFiscal;
	}

	/**
	 * @param finExerciceFiscal
	 *            the finExerciceFiscal to set
	 */
	public void setFinExerciceFiscal(String finExerciceFiscal) {
		this.finExerciceFiscal = finExerciceFiscal;
	}

	/**
	 *
	 * @return nonExerciceComptable
	 */
	public Boolean getNonExerciceComptable() {
		return this.nonExerciceComptable;
	}

	/**
	 *
	 * @param nonExerciceComptable
	 */
	public void setNonExerciceComptable(final Boolean nonExerciceComptable) {
		this.nonExerciceComptable = nonExerciceComptable;
	}

	/**
	 *
	 * @return elementsAPublier
	 */
	public boolean getElementsAPublier() {
		return elementsAPublier;
	}

	/**
	 *
	 * @param elementsAPublier
	 */
	public void setElementsAPublier(boolean elementsAPublier) {
		this.elementsAPublier = elementsAPublier;
	}

	public Boolean getHasPublication() {
		return hasPublication;
	}

	public void setHasPublication(Boolean hasPublication) {
		this.hasPublication = hasPublication;
	}

	/**
	 * @return the dateDernierePublication
	 */
	public String getDateDernierePublication() {
		return dateDernierePublication;
	}

	/**
	 * @param dateDernierePublication
	 *            the dateDernierePublication to set
	 */
	public void setDateDernierePublication(String dateDernierePublication) {
		this.dateDernierePublication = dateDernierePublication;
	}

	public DesinscriptionDto getDesinscription() {
		return desinscription;
	}

	public void setDesinscription(DesinscriptionDto desinscription) {
		this.desinscription = desinscription;
	}

	public String getDatePremierePublication() {
		return datePremierePublication;
	}

	public void setDatePremierePublication(String datePremierePublication) {
		this.datePremierePublication = datePremierePublication;
	}

	public List<AssoClientDto> getClientsAnciens() {
		return clientsAnciens;
	}

	public void setClientsAnciens(List<AssoClientDto> clientsAnciens) {
		this.clientsAnciens = clientsAnciens;
	}

}
