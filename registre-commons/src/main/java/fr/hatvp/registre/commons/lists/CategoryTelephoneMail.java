/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

/**
 * 
 * Classe d'énumération des catégories des téléphones et emails.
 * 
 * @version $Revision$ $Date${0xD}
 */
public enum CategoryTelephoneMail {

    PRO("PRO"), PERSO("PERSO");

    private String type;

    CategoryTelephoneMail(final String type)
    {
        this.type = type;
    }

    /**
     * Accesseur en lecture du champ <code>type</code>.
     * @return le champ <code>type</code>.
     */
    public String getType()
    {
        return this.type;
    }

}
