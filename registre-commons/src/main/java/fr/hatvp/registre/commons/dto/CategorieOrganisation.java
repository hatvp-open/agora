/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * DTO de la sérialisation de l'enum des categorie des organisation.
 * Ce DTO permet de respecter le format json de la catégorie comme décrit ds le manifest.
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategorieOrganisation {

    /** Code. */
    private String code;
    /** Label. */
    private String label;
    /** Categorie. */
    private String categorie;
    
    private Boolean notifSansChiffreAffaire;

    /** Constructeur. */
    public CategorieOrganisation(final String code, final String value, final String categorie, final Boolean notifSansChiffreAffaire) {
        this.code = code;
        this.label = value;
        this.categorie = categorie;
        this.notifSansChiffreAffaire = notifSansChiffreAffaire;
    }

    /**
     * Accesseur en lecture du champ <code>code</code>.
     *
     * @return le champ <code>code</code>.
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Accesseur en écriture du champ <code>code</code>.
     *
     * @param code la valeur à écrire dans <code>code</code>.
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Accesseur en lecture du champ <code>label</code>.
     *
     * @return le champ <code>label</code>.
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Accesseur en écriture du champ <code>label</code>.
     *
     * @param label la valeur à écrire dans <code>label</code>.
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Accesseur en lecture du champ <code>categorie</code>.
     *
     * @return le champ <code>categorie</code>.
     */
    public String getCategorie() {
        return this.categorie;
    }

    /**
     * Accesseur en écriture du champ <code>categorie</code>.
     *
     * @param categorie la valeur à écrire dans <code>categorie</code>.
     */
    public void setCategorie(final String categorie) {
        this.categorie = categorie;
    }

	public Boolean getNotifSansChiffreAffaire() {
		return notifSansChiffreAffaire;
	}

	public void setNotifSansChiffreAffaire(Boolean notifSansChiffreAffaire) {
		this.notifSansChiffreAffaire = notifSansChiffreAffaire;
	}   
    
}
