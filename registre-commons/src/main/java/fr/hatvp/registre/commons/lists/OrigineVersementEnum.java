/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

/**
 * Permet d'enregistrer la provenance d'une pièce attachée à un espace.
 *
 * @version $Revision$ $Date${0xD}
 */
public enum OrigineVersementEnum {

	DEMANDE_CREATION_ESPACE("Demande de création d'un nouvel espace"),
    DEMANDE_DESINSCRIPTION_ESPACE("Demande de désinscription d'un espace"),//quand saisie par déclarant
    DESINSCRIPTION_ESPACE("Désinscription d'un espace"), //quand saisie par agent
	DEMANDE_CHANGEMENT_MANDANT("Demande de changement de mandant"),
	DEMANDE_COMPLEMENT_INSCRIPTION("Demande de complément de la Haute Autorité"),
	DEMANDE_COMPLEMENT_CHANGEMENT_MANDANT("Demande de complément de changement de mandant"),
	INSCRIPTION_DECLARANT("Inscription d'un nouveau déclarant"),
	BO_DECLARANT("Ajout par le back office"),
	FO_DECLARANT("Ajout par le front office");

	private final String label;

	OrigineVersementEnum(final String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}

}
