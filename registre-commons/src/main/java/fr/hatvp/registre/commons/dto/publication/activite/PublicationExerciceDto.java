/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication.activite;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.hatvp.registre.commons.dto.publication.activite.common.PublicationMetaDto;
import fr.hatvp.registre.commons.utils.LocalDateDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateSerializer;

/**
    * DTO de publication de l'entité ExerciceComptable
    *
    * @version $Revision$ $Date${0xD}
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PublicationExerciceDto extends PublicationMetaDto
{
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateDebut;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateFin;

    private String chiffreAffaire;

    private Boolean hasNotChiffreAffaire;

    private String montantDepense;

    private Integer nombreSalaries;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<PublicationBucketActiviteDto> activites = new ArrayList<>();

    @JsonIgnore
    private boolean nonDeclarationMoyens;

    @JsonProperty("defautDeclaration")
	private boolean blacklisted;


	private Boolean declarationDone;
    
    private Long exerciceId;
    
	private boolean noActivite = false;
	
	private int nombreActivite;
	
	private String commentaire;

    public LocalDate getDateDebut()
    {
        return dateDebut;
    }
    public void setDateDebut(LocalDate dateDebut)
    {
        this.dateDebut = dateDebut;
    }
    public LocalDate getDateFin()
    {
        return dateFin;
    }
    public void setDateFin(LocalDate dateFin)
    {
        this.dateFin = dateFin;
    }
    public String getChiffreAffaire()
    {
        return chiffreAffaire;
    }
    public void setChiffreAffaire(String chiffreAffaire)
    {
        this.chiffreAffaire = chiffreAffaire;
    }
    public Boolean getHasNotChiffreAffaire()
    {
        return hasNotChiffreAffaire;
    }
    public void setHasNotChiffreAffaire(Boolean hasNotChiffreAffaire)
    {
        this.hasNotChiffreAffaire = hasNotChiffreAffaire;
    }

    public String getMontantDepense()
    {
        return montantDepense;
    }
    public void setMontantDepense(String montantDepense)
    {
        this.montantDepense = montantDepense;
    }
    public Integer getNombreSalaries()
    {
        return nombreSalaries;
    }
    public void setNombreSalaries(Integer nombreSalaries)
    {
        this.nombreSalaries = nombreSalaries;
    }
    public List<PublicationBucketActiviteDto> getActivites()
    {
        return activites;
    }
    public void setActivites(List<PublicationBucketActiviteDto> activites)
    {
        this.activites = activites;
    }

    public boolean isNonDeclarationMoyens()
    {
        return nonDeclarationMoyens;
    }
    public void setNonDeclarationMoyens(boolean nonDeclarationMoyens)
    {
        this.nonDeclarationMoyens = nonDeclarationMoyens;
    }

    public Long getExerciceId()
    {
        return exerciceId;
    }
    public void setExerciceId(Long exerciceId)
    {
        this.exerciceId = exerciceId;
    }

	/**
	 * @return the noActivite
	 */
	public boolean isNoActivite() {
		return noActivite;
	}

	/**
	 * @param noActivite
	 *            the noActivite to set
	 */
	public void setNoActivite(boolean noActivite) {
		this.noActivite = noActivite;
	}
	
	public int getNombreActivite() {
		return nombreActivite;
	}
	public void setNombreActivite(int nombreActivite) {
		this.nombreActivite = nombreActivite;
	}

	/**
	 * @return the blacklisted
	 */
	public boolean isBlacklisted() {
		return blacklisted;
	}

	/**
	 * @param blacklisted
	 *            the blacklisted to set
	 */
	public void setBlacklisted(boolean blacklisted) {
		this.blacklisted = blacklisted;
	}

	/**
	 * @return the declarationDone
	 */
	public Boolean getDeclarationDone() {
		return declarationDone;
	}

	/**
	 * @param declarationDone
	 *            the declarationDone to set
	 */
	public void setDeclarationDone(Boolean declarationDone) {
		this.declarationDone = declarationDone;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	
}
