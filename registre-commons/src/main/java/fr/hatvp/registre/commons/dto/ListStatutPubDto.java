/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import fr.hatvp.registre.commons.lists.StatutPublicationEnum;

@JsonFormat(shape = Shape.OBJECT)
public class ListStatutPubDto {

	private List<StatutPublicationEnum> statutActiviteEnumList = new ArrayList<>();

	public List<StatutPublicationEnum> getStatutActiviteEnumList() {
		return statutActiviteEnumList;
	}

	public void setStatutActiviteEnumList(List<StatutPublicationEnum> statutActiviteEnumList) {
		this.statutActiviteEnumList = statutActiviteEnumList;
	}

}
