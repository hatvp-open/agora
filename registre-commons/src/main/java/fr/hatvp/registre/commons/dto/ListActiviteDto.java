/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * Dto contenant les deux listes servant à la déclaration de représentation d'intéret.
 *
 * @version $Revision$ $Date${0xD}
 */
public class ListActiviteDto {

    private List<SecteurActiviteEnum> secteurActiviteEnumList = new ArrayList<>();
    private List<NiveauInterventionEnum> niveauInterventionEnums = new ArrayList<>();

    public List<SecteurActiviteEnum> getSecteurActiviteEnumList() {
        return this.secteurActiviteEnumList;
    }

    public void setSecteurActiviteEnumList(
        final List<SecteurActiviteEnum> secteurActiviteEnumList) {
        this.secteurActiviteEnumList = secteurActiviteEnumList;
    }

    public List<NiveauInterventionEnum> getNiveauInterventionEnums() {
        return this.niveauInterventionEnums;
    }

    public void setNiveauInterventionEnums(
        final List<NiveauInterventionEnum> niveauInterventionEnums) {
        this.niveauInterventionEnums = niveauInterventionEnums;
    }
}
