/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.commons.utils.LocalDateDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateSerializer;


public class SurveillanceDetailDto extends AbstractCommonDto {
	private Long organisationId;

	private String denomination;

	private String nomUsage;

	private StatutEspaceEnum statut;

	private Set<SurveillanceOrganisationEnum> typeSurveillance;

	private List<UtilisateurBoDto> surveillants;
	
	private List<GroupBoDto> surveillantsGroupes;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateSurveillance;

    private String motif;

    public SurveillanceDetailDto(Long organisationId, String denomination, String nomUsage) {
        this.organisationId = organisationId;
        this.denomination = denomination;
        this.nomUsage = nomUsage;
    }

    public SurveillanceDetailDto() {
    }

	public Long getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public String getNomUsage() {
		return nomUsage;
	}

	public void setNomUsage(String nomUsage) {
		this.nomUsage = nomUsage;
	}

	public Set<SurveillanceOrganisationEnum> getTypeSurveillance() {
		return typeSurveillance;
	}

	public void setTypeSurveillance(Set<SurveillanceOrganisationEnum> typeSurveillance) {
		this.typeSurveillance = typeSurveillance;
	}

	public List<UtilisateurBoDto> getSurveillants() {
		return surveillants;
	}

	public void setSurveillants(List<UtilisateurBoDto> surveillants) {
		this.surveillants = surveillants;
	}

	public LocalDate getDateSurveillance() {
		return dateSurveillance;
	}

	public void setDateSurveillance(LocalDate dateSurveillance) {
		this.dateSurveillance = dateSurveillance;
	}

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

	/**
	 * @return the surveillantGroups
	 */
	public List<GroupBoDto> getSurveillantsGroupes() {
		return surveillantsGroupes;
	}

	/**
	 * @param surveillantsGroupes the surveillantGroups to set
	 */
	public void setSurveillantsGroupes(List<GroupBoDto> surveillantsGroupes) {
		this.surveillantsGroupes = surveillantsGroupes;
	}

    public StatutEspaceEnum getStatut() {
        return statut;
    }

    public void setStatut(StatutEspaceEnum statut) {
        this.statut = statut;
    }
}
