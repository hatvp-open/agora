

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import java.util.List;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;

/**
 * 
 * DTO de l'entité EspaceOrganisationEntity et EspaceOrganisationLockBoEntity.
 * 
 * @version $Revision$ $Date${0xD}
 */
public class ValidationEspaceBoDto
    extends AbstractCommonDto
{
    /** Espace Commenté */
    private Boolean commente;

    /** Espace Organisation créé */
    private EspaceOrganisationDto espaceOrganisation;

    /** Objet Verrou sur l'objet */
    private EspaceOrganisationLockBoDto lock;

    /** Organisation */
    private OrganisationDto organisationData;

    /** Id de l'inscription. */
    private Long validationInscriptionId;

	private boolean relancesBloquees;
    
	private List<PublicationDto> publicationIdentiteList;
    /**
     * Accesseur en lecture du champ <code>commente</code>.
     * @return le champ <code>commente</code>.
     */
    public Boolean getCommente()
    {
        return this.commente;
    }

    /**
     * Accesseur en écriture du champ <code>commente</code>.
     * @param commente la valeur à écrire dans <code>commente</code>.
     */
    public void setCommente(final Boolean commente)
    {
        this.commente = commente;
    }

    /**
     * Accesseur en lecture du champ <code>espaceOrganisation</code>.
     * @return le champ <code>espaceOrganisation</code>.
     */
    public EspaceOrganisationDto getEspaceOrganisation()
    {
        return this.espaceOrganisation;
    }

    /**
     * Accesseur en écriture du champ <code>espaceOrganisation</code>.
     * @param espaceOrganisation la valeur à écrire dans <code>espaceOrganisation</code>.
     */
    public void setEspaceOrganisation(final EspaceOrganisationDto espaceOrganisation)
    {
        this.espaceOrganisation = espaceOrganisation;
    }

    /**
     * Accesseur en lecture du champ <code>lock</code>.
     * @return le champ <code>lock</code>.
     */
    public EspaceOrganisationLockBoDto getLock()
    {
        return this.lock;
    }

    /**
     * Accesseur en écriture du champ <code>lock</code>.
     * @param lock la valeur à écrire dans <code>lock</code>.
     */
    public void setLock(final EspaceOrganisationLockBoDto lock)
    {
        this.lock = lock;
    }

    /**
     * Accesseur en lecture du champ <code>organisationData</code>.
     * @return le champ <code>organisationData</code>.
     */
    public OrganisationDto getOrganisationData()
    {
        return this.organisationData;
    }

    /**
     * Accesseur en écriture du champ <code>organisationData</code>.
     * @param organisationData la valeur à écrire dans <code>organisationData</code>.
     */
    public void setOrganisationData(final OrganisationDto organisationData)
    {
        this.organisationData = organisationData;
    }

    /**
     * Accesseur en lecture du champ <code>validationInscriptionId</code>.
     * @return le champ <code>validationInscriptionId</code>.
     */
    public Long getValidationInscriptionId()
    {
        return this.validationInscriptionId;
    }

    /**
     * Accesseur en écriture du champ <code>validationInscriptionId</code>.
     * @param validationInscriptionId la valeur à écrire dans <code>validationInscriptionId</code>.
     */
    public void setValidationInscriptionId(final Long validationInscriptionId)
    {
        this.validationInscriptionId = validationInscriptionId;
    }

	public boolean isRelancesBloquees() {
		return relancesBloquees;
	}

	public void setRelancesBloquees(boolean relancesBloquees) {
		this.relancesBloquees = relancesBloquees;
	}

	public List<PublicationDto> getPublicationIdentiteList() {
		return publicationIdentiteList;
	}

	public void setPublicationIdentiteList(List<PublicationDto> publicationIdentiteList) {
		this.publicationIdentiteList = publicationIdentiteList;
	}
    
    

}
