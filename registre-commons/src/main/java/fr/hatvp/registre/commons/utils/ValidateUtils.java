/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.utils;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import fr.hatvp.registre.commons.dto.DeclarantContactDto;
import fr.hatvp.registre.commons.lists.CategoryTelephoneMail;
/**
 * Class permettant de faire différents types de validation.
 *
 * @version $Revision$ $Date${0xD}
 */
public class ValidateUtils
{
    /**
     * Password pattern.
     */
    private static final String PASSWORD_PATTERN = "^(?=.*?[a-zA-Z])(?=.*?[0-9]).{8,}$";

    /**
     * Email pattern.
     */
    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&’*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

    /**
     * Valider le pattern du mot de passe.
     *
     * @param password mot de passe en entrée.
     * @return un boolean de validation.
     */
    public static boolean validatePassword(final String password)
    {
        return !StringUtils.isEmpty(password) && password.matches(PASSWORD_PATTERN);

    }

    /**
     * Valider le pattern de l'email.
     *
     * @param email email en entrée.
     * @return un boolean de validation.
     */
    public static boolean validateEmail(final String email)
    {
        return !StringUtils.isEmpty(email) && email.matches(EMAIL_PATTERN);

    }

    /**
     * Valider le type des catégories de contacts saisi.
     *
     * @param declarantContactDtos liste de contacts.
     * @return un boolean de validation.
     */
    public static boolean isContactListCategoryValid(
            final List<DeclarantContactDto> declarantContactDtos)
    {

        for (final DeclarantContactDto declarantContactDto : declarantContactDtos) {
            boolean isFound = false;
            for (final CategoryTelephoneMail type : CategoryTelephoneMail.values()) {

                if (type.equals(declarantContactDto.getCategorie())) {
                    isFound = true;
                }

            }
            if (!isFound) {
                return false;
            }
        }
        return false;
    }

}
