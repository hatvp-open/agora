/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

/**
 * Dto des commentaires.
 *
 * @version $Revision$ $Date${0xD}
 */
public class CommentBoDto extends AbstractCommonDto {

	/** Nom et prénom de l'auteur du commentaire. */
	@NotEmpty
	private String auteur;

	/** Nom et prénom de dernier modificateur du commentaire. */
	private String modificateur;

	/** Contenu du commentaire posté. */
	@NotEmpty
	private String message;

	/** Date de création du commentaire. */
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
	private Date dateCreation;

	/** Date de création du commentaire. */
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
	private Date dateModification;

	/** Id du context d'ou vient le commentaire, Declarant, Validataion ... */
	@NotNull
	private Long contextId;

	/**
	 * @return the auteur
	 */
	public String getAuteur() {
		return auteur;
	}

	/**
	 * @param auteur
	 *            the auteur to set
	 */
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	/**
	 * @return the modificateur
	 */
	public String getModificateur() {
		return modificateur;
	}

	/**
	 * @param modificateur
	 *            the modificateur to set
	 */
	public void setModificateur(String modificateur) {
		this.modificateur = modificateur;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the dateCreation
	 */
	public Date getDateCreation() {
		return dateCreation;
	}

	/**
	 * @param dateCreation
	 *            the dateCreation to set
	 */
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	/**
	 * @return the dateModification
	 */
	public Date getDateModification() {
		return dateModification;
	}

	/**
	 * @param dateModification
	 *            the dateModification to set
	 */
	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	/**
	 * @return the contextId
	 */
	public Long getContextId() {
		return contextId;
	}

	/**
	 * @param contextId
	 *            the contextId to set
	 */
	public void setContextId(Long contextId) {
		this.contextId = contextId;
	}

}
