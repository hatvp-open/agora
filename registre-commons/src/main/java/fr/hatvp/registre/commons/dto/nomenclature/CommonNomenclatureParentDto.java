/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.nomenclature;

import java.util.List;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

public abstract class CommonNomenclatureParentDto<T extends CommonNomenclatureDto> extends AbstractCommonDto {

	private String categorie;

	private List<T> nomenclature;

	/**
	 * @return the categorie
	 */
	public String getCategorie() {
		return categorie;
	}

	/**
	 * @param categorie
	 *            the categorie to set
	 */
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	/**
	 * @return the nomenclature
	 */
	public List<T> getNomenclature() {
		return nomenclature;
	}

	/**
	 * @param nomenclature
	 *            the nomenclature to set
	 */
	public void setNomenclature(List<T> nomenclature) {
		this.nomenclature = nomenclature;
	}

}
