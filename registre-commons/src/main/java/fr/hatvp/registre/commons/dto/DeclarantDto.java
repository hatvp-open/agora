/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import static fr.hatvp.registre.commons.validators.ValidationMessagesConstantes.*;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.commons.validators.category.CategoryMailPhone;
import fr.hatvp.registre.commons.validators.civility.Civility;
import fr.hatvp.registre.commons.validators.password.PasswordPattern;

/**
 * DTO de l'entité DeclarantEntity.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class DeclarantDto
    extends AbstractCommonDto
{

    @NotBlank(message = NOM_OBLIGATOIRE)
    @Size(max = 150)
    private String prenom;

    @JsonProperty("nom")
    @NotBlank(message = PRENOM_OBLIGATOIRE)
    @Size(max = 150)
    private String nom;

    @JsonProperty("date_naissance")
    @NotNull
    @Past(message = DATE_NAISSANCE_INVALIDE)
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    private Date birthDate;

    @JsonProperty("email")
    @NotBlank(message = EMAIL_OBLIGATOIRE)
    @Email(message = EMAIL_INCORRECTE)
    @Size(max = 150)
    private String email;

    @JsonProperty("emailTemp")
    @Size(max = 150)
    private String emailTemp;

    @JsonProperty("telephone")
    @NotBlank(message = TELEPHONE_OBLIGATOIRE)
    @Size(max = 15)
    private String telephone;

    @JsonProperty("emails_complement")
    @CategoryMailPhone(message = CATEGORIE_INCORRECTE)
    private List<DeclarantContactDto> emailComplement;

    @JsonProperty("telephones")
    @CategoryMailPhone(message = CATEGORIE_INCORRECTE)
    private List<DeclarantContactDto> phone;

    @JsonProperty("civilite")
    @Civility(message = CIVILITE_INCORRECTE,
            value = {CiviliteEnum.MME, CiviliteEnum.M})
    private CiviliteEnum civility;

    @NotNull(message = MOT_DE_PASSE_OBLIGATOIRE)
    @NotBlank(message = MOT_DE_PASSE_OBLIGATOIRE)
    @PasswordPattern(
            message = MOT_DE_PASSE_INCORRECTE, value = "^(?=.*?[a-zA-Z])(?=.*?[0-9]).{8,}$")
    @Size(max = 150)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @JsonProperty("prev_password")
    @Size(max = 150)
    private String previousPassword;

    private boolean activated;
    private boolean firstConn;
    @JsonIgnore
    private String activationEmailKeyCode;
    @JsonIgnore
    private String recuperationPasswordKeyCode;
    @JsonIgnore
    private Collection<RoleEnumFrontOffice> inscriptionEspaceFavRoles;

    private Collection<String> listeOrganisationClient;

    /**
     * Date de dernière connexion.
     */
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
    private Date lastConnexionDate;

    /**
     * Accesseur en lecture du champ <code>prenom</code>.
     *
     * @return le champ <code>prenom</code>.
     */
    public String getPrenom()
    {
        return this.prenom;
    }

    /**
     * Accesseur en écriture du champ <code>prenom</code>.
     *
     * @param prenom la valeur à écrire dans <code>prenom</code>.
     */
    public void setPrenom(final String prenom)
    {
        this.prenom = prenom;
    }

    /**
     * Accesseur en lecture du champ <code>nom</code>.
     *
     * @return le champ <code>nom</code>.
     */
    public String getNom()
    {
        return this.nom;
    }

    /**
     * Accesseur en écriture du champ <code>nom</code>.
     *
     * @param nom la valeur à écrire dans <code>nom</code>.
     */
    public void setNom(final String nom)
    {
        this.nom = nom;
    }

    /**
     * Accesseur en lecture du champ <code>birthDate</code>.
     *
     * @return le champ <code>birthDate</code>.
     */
    public Date getBirthDate()
    {
        return this.birthDate;
    }

    /**
     * Accesseur en écriture du champ <code>birthDate</code>.
     *
     * @param birthDate la valeur à écrire dans <code>birthDate</code>.
     */
    public void setBirthDate(final Date birthDate)
    {
        this.birthDate = birthDate;
    }

    /**
     * Accesseur en lecture du champ <code>email</code>.
     *
     * @return le champ <code>email</code>.
     */
    public String getEmail()
    {
        return this.email;
    }

    /**
     * Accesseur en écriture du champ <code>email</code>.
     *
     * @param email la valeur à écrire dans <code>email</code>.
     */
    public void setEmail(final String email)
    {
        this.email = email;
    }

    /**
     * Accesseur en lecture du champ <code>emailTemp</code>.
     *
     * @return le champ <code>emailTemp</code>.
     */
    public String getEmailTemp()
    {
        return this.emailTemp;
    }

    /**
     * Accesseur en écriture du champ <code>emailTemp</code>.
     *
     * @param emailTemp la valeur à écrire dans <code>emailTemp</code>.
     */
    public void setEmailTemp(final String emailTemp)
    {
        this.emailTemp = emailTemp;
    }

    /**
     * Accesseur en lecture du champ <code>emailComplement</code>.
     *
     * @return le champ <code>emailComplement</code>.
     */
    public List<DeclarantContactDto> getEmailComplement()
    {
        return this.emailComplement;
    }

    /**
     * Accesseur en écriture du champ <code>emailComplement</code>.
     *
     * @param emailComplement la valeur à écrire dans <code>emailComplement</code>.
     */
    public void setEmailComplement(final List<DeclarantContactDto> emailComplement)
    {
        this.emailComplement = emailComplement;
    }

    /**
     * Accesseur en lecture du champ <code>phone</code>.
     *
     * @return le champ <code>phone</code>.
     */
    public List<DeclarantContactDto> getPhone()
    {
        return this.phone;
    }

    /**
     * Accesseur en écriture du champ <code>phone</code>.
     *
     * @param phone la valeur à écrire dans <code>phone</code>.
     */
    public void setPhone(final List<DeclarantContactDto> phone)
    {
        this.phone = phone;
    }

    /**
     * Accesseur en lecture du champ <code>civility</code>.
     *
     * @return le champ <code>civility</code>.
     */
    public CiviliteEnum getCivility()
    {
        return this.civility;
    }

    /**
     * Accesseur en écriture du champ <code>civility</code>.
     *
     * @param civility la valeur à écrire dans <code>civility</code>.
     */
    public void setCivility(final CiviliteEnum civility)
    {
        this.civility = civility;
    }

    /**
     * Accesseur en lecture du champ <code>password</code>.
     *
     * @return le champ <code>password</code>.
     */
    public String getPassword()
    {
        return this.password;
    }

    /**
     * Accesseur en écriture du champ <code>password</code>.
     *
     * @param password la valeur à écrire dans <code>password</code>.
     */
    public void setPassword(final String password)
    {
        this.password = password;
    }

    /**
     * Accesseur en lecture du champ <code>previousPassword</code>.
     *
     * @return le champ <code>previousPassword</code>.
     */
    public String getPreviousPassword()
    {
        return this.previousPassword;
    }

    /**
     * Accesseur en écriture du champ <code>previousPassword</code>.
     *
     * @param previousPassword la valeur à écrire dans <code>previousPassword</code>.
     */
    public void setPreviousPassword(final String previousPassword)
    {
        this.previousPassword = previousPassword;
    }

    /**
     * Accesseur en lecture du champ <code>activated</code>.
     *
     * @return le champ <code>activated</code>.
     */
    public boolean isActivated()
    {
        return this.activated;
    }

    /**
     * Accesseur en écriture du champ <code>activated</code>.
     *
     * @param activated la valeur à écrire dans <code>activated</code>.
     */
    public void setActivated(final boolean activated)
    {
        this.activated = activated;
    }

    /**
     * Accesseur en lecture du champ <code>activationEmailKeyCode</code>.
     *
     * @return le champ <code>activationEmailKeyCode</code>.
     */
    public String getActivationEmailKeyCode()
    {
        return this.activationEmailKeyCode;
    }

    /**
     * Accesseur en écriture du champ <code>activationEmailKeyCode</code>.
     *
     * @param activationEmailKeyCode la valeur à écrire dans <code>activationEmailKeyCode</code>.
     */
    public void setActivationEmailKeyCode(final String activationEmailKeyCode)
    {
        this.activationEmailKeyCode = activationEmailKeyCode;
    }

    /**
     * Accesseur en lecture du champ <code>recuperationPasswordKeyCode</code>.
     *
     * @return le champ <code>recuperationPasswordKeyCode</code>.
     */
    public String getRecuperationPasswordKeyCode()
    {
        return this.recuperationPasswordKeyCode;
    }

    /**
     * Accesseur en écriture du champ <code>recuperationPasswordKeyCode</code>.
     *
     * @param recuperationPasswordKeyCode la valeur à écrire dans
     * <code>recuperationPasswordKeyCode</code>.
     */
    public void setRecuperationPasswordKeyCode(final String recuperationPasswordKeyCode)
    {
        this.recuperationPasswordKeyCode = recuperationPasswordKeyCode;
    }

    /**
     * Accesseur en lecture du champ <code>inscriptionEspaceFavRoles</code>.
     *
     * @return le champ <code>inscriptionEspaceFavRoles</code>.
     */
    public Collection<RoleEnumFrontOffice> getInscriptionEspaceFavRoles()
    {
        return this.inscriptionEspaceFavRoles;
    }

    /**
     * Accesseur en écriture du champ <code>inscriptionEspaceFavRoles</code>.
     *
     * @param inscriptionEspaceFavRoles la valeur à écrire dans
     * <code>inscriptionEspaceFavRoles</code>.
     */
    public void setInscriptionEspaceFavRoles(
            final Collection<RoleEnumFrontOffice> inscriptionEspaceFavRoles)
    {
        this.inscriptionEspaceFavRoles = inscriptionEspaceFavRoles;
    }

    /**
     * Accesseur en lecture du champ <code>listeOrganisationClient</code>.
     *
     * @return le champ <code>listeOrganisationClient</code>.
     */
    public final Collection<String> getListeOrganisationClient()
    {
        return this.listeOrganisationClient;
    }

    /**
     * Accesseur en écriture du champ <code>listeOrganisationClient</code>.
     *
     * @param listeOrganisationClient la valeur à écrire dans <code>listeOrganisationClient</code>.
     */
    public final void setListeOrganisationClient(final Collection<String> listeOrganisationClient)
    {
        this.listeOrganisationClient = listeOrganisationClient;
    }

    /**
     * Accesseur en lecture du champ <code>lastConnexionDate</code>.
     *
     * @return le champ <code>lastConnexionDate</code>.
     */
    public Date getLastConnexionDate() {
        return this.lastConnexionDate;
    }

    /**
     * Accesseur en écriture du champ <code>lastConnexionDate</code>.
     *
     * @param lastConnexionDate la valeur à écrire dans <code>lastConnexionDate</code>.
     */
    public void setLastConnexionDate(final Date lastConnexionDate)
    {
        this.lastConnexionDate = lastConnexionDate;
    }

    /**
     * Gets telephone.
     *
     * @return the telephone
     */
    public String getTelephone()
    {
        return this.telephone;
    }

    /**
     * Sets telephone.
     *
     * @param telephone the telephone
     */
    public void setTelephone(final String telephone)
    {
        this.telephone = telephone;
    }

    public boolean getFirstConn() {
        return firstConn;
    }

    public void setFirstConn(boolean firstConn) {
        this.firstConn = firstConn;
    }
}