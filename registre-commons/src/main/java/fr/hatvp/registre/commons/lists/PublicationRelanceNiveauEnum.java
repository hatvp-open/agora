/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

public enum PublicationRelanceNiveauEnum {

	P("relance préalable"), R1("relance niveau 1"), R2("relance niveau 2"), NBL("notification blacklist");

	private String label;

	private PublicationRelanceNiveauEnum(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
