/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

/**
 * 
 * Classe d'énumération des roles Front-Office;
 * @version $Revision$ $Date${0xD}
 */
public enum RoleEnumFrontOffice {

    // Roles front-office.
    ADMINISTRATEUR("Contact opérationnel"),
    CONTRIBUTEUR("Contributeur"),
    PUBLICATEUR("Publicateur");

    private final String label;

    RoleEnumFrontOffice(final String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return this.label;
    }
}
