/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.util.ArrayList;
import java.util.List;

import fr.hatvp.registre.commons.lists.StatutEspaceEnum;

public class ListStatutEspaceDto {

	private List<StatutEspaceEnum> statutEspaceEnumList = new ArrayList<>();

	public List<StatutEspaceEnum> getStatutEspaceEnumList() {
		return statutEspaceEnumList;
	}

	public void setStatutEspaceEnumList(List<StatutEspaceEnum> statutEspaceEnumList) {
		this.statutEspaceEnumList = statutEspaceEnumList;
	}

	{
		for (final StatutEspaceEnum statutActiviteEnum : StatutEspaceEnum.values()) {
			this.statutEspaceEnumList.add(statutActiviteEnum);
		}
	}
}
