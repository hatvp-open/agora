/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.hatvp.registre.commons.dto.CategorieOrganisation;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;

/**
 * Catégorie Organisation.
 * @version $Revision$ $Date${0xD}
 */
@JsonFormat(shape = Shape.OBJECT)
public enum TypeOrganisationEnum {

    SOCOM("SOCIETE", "Société commerciale", false),
    SOCIV("SOCIETE", "Société civile (autre que cabinet d’avocats)", false),

    AVCAB("AVO_CONS", "Cabinet d’avocats", false),
    AVIND("AVO_CONS", "Avocat indépendant", false),
    COCAB("AVO_CONS", "Cabinet de conseils", false),
    COIND("AVO_CONS", "Consultant indépendant", false),

    ORPRO("ORGA_REP", "Organisation professionnelle", true),
    SYNDI("ORGA_REP", "Syndicat", true),
    CHAMCO("ORGA_REP", "Chambre consulaire", true),

    ASSOC("ORGA_NON_GOUV", "Association", true),
    FONDA("ORGA_NON_GOUV", "Fondation", true),
    ORGRR("ORGA_NON_GOUV", "Organisme de recherche ou de réflexion", true),
    AUONG("ORGA_NON_GOUV", "Autres organisations non gouvernementales", true),

    EPAIC("ORGA_PUB", "Etablissement public exerçant une activité industrielle et commerciale", true),
    GIPIC("ORGA_PUB", "Groupement d’intérêt public exerçant une activité industrielle et commerciale", false),

    AUORG("AUTRE", "Autres organisations", false);

    private final String label;
    private final String group;
    private final Boolean notifSansChiffreAffaire;

    TypeOrganisationEnum(final String group, final String label, Boolean notifSansChiffreAffaire) {
        this.group = group;
        this.label = label;
        this.notifSansChiffreAffaire = notifSansChiffreAffaire;
    }

    public String getLabel() {
        return this.label;
    }

    public String getGroup() {
        return this.group;
    }   

    public Boolean getNotifSansChiffreAffaire() {
		return notifSansChiffreAffaire;
	}

	@JsonCreator
    public static TypeOrganisationEnum fromNode(final JsonNode node) {
        if (!node.has("code")) {
            return null;
        }

        final String name = node.get("code").asText();

        return TypeOrganisationEnum.valueOf(name);
    }
    
    /**
     * Récupérer un jSON contenant les types d'organisations actuellement utilisées.
     *
     * @return json des organisation.
     */
    public static String typesOrganisationsAsJson() {
        final List<CategorieOrganisation> categorieOrganisations = new ArrayList<>();

        for (final TypeOrganisationEnum t : TypeOrganisationEnum.values()) {
            categorieOrganisations.add(new CategorieOrganisation(t.name(), t.getLabel(), t.getGroup(), t.notifSansChiffreAffaire));
        }

        final ObjectMapper mapper = new ObjectMapper();
        final String jsonInString;
        try {
            jsonInString = mapper.writeValueAsString(categorieOrganisations);
        } catch (final JsonProcessingException e) {
            throw new BusinessGlobalException(ErrorMessageEnum.TYPES_CATEGORIES_AS_JSON_IMPOSSIBLE.getMessage());
        }

        return jsonInString;
    }

    /**
     * Récupérer une instance de la catégorie telle décrite dans le format json de la publication.
     */
    @JsonValue
    public CategorieOrganisation getCategori() {
        return new CategorieOrganisation(this.name(), this.label, this.getGroup(), this.notifSansChiffreAffaire);
    }
}
