/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

/**
 * Classe d'énumération des messages d'erreurs.
 *
 * @version $Revision$ $Date${0xD}
 */
public enum ErrorMessageEnum {

	NOMBRE_MAX_UTILISATEURS("En raison d'un trop grand nombre de connexions simultanées, nous vous invitons à vous reconnecter ultérieurement."),
	ADRESSE_EMAIL_EST_DEJA_ACTIVE("Un compte associé à cette adresse courriel a déjà été activé. Veuillez utiliser une autre adresse " + "courriel."),
	ADRESSE_EMAIL_EXISTE_DEJA("Un compte associé à cette adresse courriel a déjà été activé. Veuillez utiliser une autre adresse " + "courriel."),
	ADRESSE_EMAIL_En_ATTENTE_DE_VALIDATION("Votre demande de modification d'adresse courriel principale est en attente de validation. Merci de "
			+ "cliquer sur le lien reçu dans le courriel qui vous a été adressé."),
	UTILISATEUR_INTROUVABLE("Le comtpe utilisateur recherché est introuvable."),
	ADRESSE_EMAIL_SAISI_EST_CELLE_QUE_VOUS_UTILISEZ_DEJA(
			"La nouvelle adresse courriel saisie est identique à celle que vous utilisez déjà. Veuillez saisir une " + "autre adresse courriel."),
	ANCIEN_MOT_DE_PASSE_SAISI_INCORRRECT("Veuillez resaisir votre mot de passe actuel."),
	COMPTE_DEJA_ACTIVE("Votre compte a déjà été activé, veuillez vous connecter pour accéder au téléservice."),
	MOT_DE_PASSE_INVALIDE("Le mot de passe saisi est invalide."),
	DECLARANT_INTROUVABLE("Il n'existe aucun compte associé à cette adresse courriel de connexion."),
	LE_TOKEN_DE_RECUPERATION_A_EXPIRE("Le lien de récupération du compté a expiré. Veuillez recommencer la procédure de modification du " + "changement de mot de passe."),
	LE_TOKEN_ACTIVATION_DE_COMPTE_EXPIRE("Le lien de validation de votre compte est obsolète."),
	COMPTE_INTROUVABLE("Le compte utilisateur recherché est introuvable."),
	VOTRE_COMPTE_N_EST_PAS_ENCORE_ACTIVE(
			"Le compte associé à l'adresse courriel saisie n'est pas encore activé. Merci de cliquer sur le lien reçu" + " dans le courriel qui vous a été adressé."),
	EMAIL_EN_ATTENTE_DE_VALIDATION_PAR_UN_AUTRE_UTILISATEUR("L'adresse courriel saisie est déjà associée à un compte."),
	ORGANISATION_INTROUVABLE("L'organisation recherchée est introuvable."),
	ESPACE_ORGANISATION_INTROUVABLE("L'espace collaboratif recherché est introuvable."),
	CLIENT_INTROUVABLE("Le client recherché est introuvable."),
	CE_CLIENT_EXISTE_DEJA("Le client que vous souhaitez ajouter se trouve déjà dans votre liste."),
	SUPPRESSION_COLLABORATEUR_INSCRIT(
			"Un collaborateur membre de votre espace collaboratif ne peut être supprimé. Vous pouvez en revanche " + "signaler qu'il est inactif en modifiant ses attributs."),
	STATUT_ACTIF_COLLABORATEUR_ADDITIONNEL("La statut d'activation d'un collaborateur ne peut être changé que pour un membre de votre espace collaboratif."),
	MSG_403("Vous n'avez pas les droits requis pour accéder à cette fonctionnalité. Merci de contacter votre " + "contact opérationnel pour obtenir ce droit."),
	MSG_404("L'entité recherchée est introuvable."),
	DEMANDE_EN_ATTENTE_DE_VALIDATION(
			"Vous avez déjà fait la demande de rejoindre cet espace collaboratif. Votre demande est en attente de " + "validation par le contact opérationnel de l'espace."),
	VOUS_ETES_DEJA_INSCRIS_A_CET_ESPACE_ORGANISATION("Vous êtes déjà inscrit à cet espace collaboratif. Veuillez vous connecter pour y accéder."),
	ACCES_VEROUILLE("Votre accès a cet espace collaboratif a été verouillé par le contact opérationnel de votre espace " + "collaboratif."),
	INSCRIPTION_INTROUVABLE("Votre inscription à cet espace collaboratif est introuvable."),
	MSG_500("Le serveur a rencontré une erreur imprévue."),
	TELEPHONE_INTROUVABLE("Vous n'avez pas saisi de numéro de téléphone. Veuillez recommencer."),
	INVALID_DECLARANT_DATA("Les données transmises au serveur sont invalides."),
	DONNEES_INCORRECTES("Les données transmises au serveur sont invalides."),
	VEUILLEZ_SELECTIONNER_UN_FICHIER_A_CHARGER("Merci de charger un fichier."),
	VEUILLEZ_FAIRE_UN_CHOIX_DE_DECLARATION_POUR_UN_TIERS("Veuillez préciser si vous déclarez ou non pour un tiers."),
	LE_FICHIER_DEPASSE_LA_TAILLE_AUTORISEE("Le fichier que vous avez chargé dépasse la taille maximale autorisée."),
	EXTENSION_INCORRECTE("Les extensions autorisées sont : PDF, PNG, JPEG ou JPG. Merci de charger un fichier respectant un de ces" + " formats."),
	ENREGISTREMENT_FICHIER_ERREUR("Une erreur est survenue lors de l'enregistrement du fichier."),
	IMPOSSIBLE_DE_RECONNAITRE_LORGANISATION("Organisation inconnue."),
	AJOUT_COMPLEMENT_INTERDIT("Vous n'avez pas le droit de verser des pièces pour cet espace collaboratif."),
	L_ESPACE_ORGANISATION_EST_OBLIGATOIRE_POUR_AJOUTER_UN_CLIENT("Merci de préciser l'espace collaboratif pour ajouter un client."),
	AUCUN_CLIENT_TROUVE_DANS_LA_REQUETE("Merci de préciser le client à ajouter."),
	CE_ORGANISATION_EXISTE_DEJA("Cette organisation existe déjà dans votre liste."),
	ORIGIN_IDENTIFIANT_INCORRECT("L'origine de l'identifiant national est incorrect."),
	LE_CONTEXT_DOIT_ETRE_CLIENT_OU_ASSOCIATION("Le context d'ajout est incorrect."),
	LOGO_INTROUVABLE("Le chemin spécifié pour le logo n'est pas valide."),
	ERREUR_D_ENVOI_DE_MAIL("L'envoi du courriel a échoué."),
	ORGANISATION_PARAM_INCORRECT("Le paramètre organisation n'est pas valide."),
	DEMANDE_CHANGEMENT_MANDANT_PARAM_INCORRECT("Le paramètre \"demande de changement de mandant\" n'est pas valide."),
	CONTEXT_AJOUT_ORGANISATION_INCORRECT("Le context d'ajout est incorrect."),
	MOTIF_REJET_INTROUVABLE("Le motif sollicité est introuvable."),
	FORMAT_IDENTIFIANT_INCORRECT("Le format de l'identifiant que vous avez saisi est incorrect."),
	COLLABORATEUR_INTROUVABLE("Le collaborateur que vous recherchez est introuvable."),
	MODIFICATION_COLLABORATEUR_IMPOSSIBLE("Impossible de modifier le collaborateur."),
	CODE_NIVEAU_INTERVENTION_NOT_FOUND("Le code envoyé ne correspond à aucun niveau d'intervention."),
	CODE_SECTEUR_ACTIVITE_NOT_FOUND("Le code envoyé ne correspond à aucun secteur d'activité."),
	ERREUR_AUTHENTIFICATION("Données d'authentification incorrectes."),
	COMPTE_INACTIF("Compte inactif."),
	SESSION_INTROUVABLE("Session introuvable."),
	TYPES_CATEGORIES_AS_JSON_IMPOSSIBLE("Impossible de créer le json des types d'organisation."),
	MODIFICATION_DIRIGEANT_IMPOSSIBLE("Impossible de modifier le dirigeant."),
	ESPACE_ORGANISATION_EXISTE_DEJA("L'espace collaboratif de DENOMINATION a déjà été créé. Merci de vous adresser à son contact opérationnel."),
	ECHEC_CREATION_ESPACE_ORGA("Une erreur est survenue lors de la création de l'organisation. Veuillez recommencer."),
	ECHEC_MODIF_ROLE_DECLARANT_NOT_ACTIVATED("Impossible de modifier les rôles et accès d'un déclarant dont l'inscription n'a pas été validée."),
	LIMITE_CREATION_ESPACE_DEPASSEE(
			"Vous avez déjà déposé plusieurs demandes de création d'espaces collaboratifs. Vous pourrez de nouveau demander la création d'un espace collaboratif après instruction de vos demandes en cours."),
	INTERDIT_ACCESS_INFOS_ESPACE("L'accès aux informations de cet espace ne vous est pas accordé."),
	CONTENU_PUBLICATION_INTROUVABLE("Le document à publier n'a pas été trouvé. La publication n'a pu être terminée."),
	ERREUR_PUBLICATION("Une erreur imprévue est survenue lors de la publication des données de votre espace."),
	ERREUR_JSON("Une erreur imprévue est survenue lors de la récupération du json pour l'historique"),
	ERREUR_FICHIER("Une erreur imprévue est survenue lors de l'analyse du fichier."),
	FORMAT_ID_NATIONAL_INCORRECT("Le format de l'identifiant National est incorrect (SIREN, RNA ou HATVP)"),
	DATE_FIN_EXERCICE_NON_RENSEIGNEE("Vous devez renseigner la date de clôture d’exercice comptable de votre organisation, ou le cas échéant une date de référence, afin d’accéder à cette page. "),
	MAUVAIS_ESPACE("Mauvais espace organisation"),
	DUPLICATE_TIERS("Vous avez saisi sélectionné un même bénéficiaire plusieurs fois dans différentes activités saisies."),
	PAS_DE_RENSEIGNEMENT_CA("Vous n'avez pas renseigné d'informations relatives à votre chiffre d'affaire"),
	ACTION_AUTRE_NON_RENSEIGNEE("Vous n'avez pas renseigné le type d'action menée: autre"),
	SUPPRESSION_ACTIVITE_IMPOSSIBLE("Vous ne pouvez pas supprimer cette fiche car elle a été publiée"),
	RESPONSABLE_PUBLIC_AUTRE_NON_RENSEIGNEE("Vous n'avez pas précisé le type de responsable public"),
	EXERCICE_COMPTABLE_INEXISTANT("Exercice comptable introuvable"),
	DECLARATION_INTROUVABLE("Fiche d'activités introuvable"),
	ECHEC_GENERATION_PDF("Echec de la génération du pdf"),
	ECHEC_GENERATION_CSV("Echec de la génération du csv"),
	ACCES_ACTIVITE_NO_PUBLICATION("L'onglet \"Activités\" d'AGORA ne vous est pas accessible car vous n'avez pas encore communiqué à la Haute Autorité les informations relatives à l'identité de votre organisation. Ces informations peuvent être renseignées puis communiquées depuis l'onglet \"Identité\" de votre espace, où vous vous trouvez actuellement."),
	PUBLICATION_ACTIVITE_NO_PUBLICATION("Vous ne pouvez communiquer à la Haute Autorité de fiche d'activité tant que vous n'aurez pas communiqué vos informations d'identité. Vous pouvez nous communiquer ces informations à partir de l'onglet \"Identité\", disponible en haut et à droite de votre page."),
	PUBLICATION_EXERCICE_NO_PUBLICATION("Vous ne pouvez communiquer à la Haute Autorité vos moyens alloués à la représentation d'intérêts tant que vous n'aurez pas communiqué vos informations d'identité. Vous pouvez nous communiquer ces informations à partir de l'onglet \"Identité\", disponible en haut et à droite de votre page. "),
	NO_ACTIVITE("Aucune fiche d'activités n'a été renseignée pour cette période"),
	NO_STATUT_PUB("Aucun statut n'a été sélectionné pour les fiches d'activités à inclure"),
	NO_ACTIVITE_WITH_SELECTED_STATUT("Aucune fiche d'activités correspondante au(x) statut(s) sélectionné(s) à importer"),
	DATE_EXERCICE_ERROR("La date de début d'exercice ne peut pas être postérieure à la date de fin d'exercice"),
	CLIENT_SOI_MEME("Il n'est pas possible d'être son propre mandant ou client"),
	AFFILIE_SOI_MEME("Il n'est pas possible d'être son propre affilié"),
	ESPACE_ORGANISATION_DEJA_VALIDE_OU_REFUSE("Cet espace collaboratif a déjà été validé ou refusé"),
	EXERCICE_PAS_DE_DECLARATION_IMPOSSIBLE("Il n'est pas possible de faire une déclaration nulle pour cet exercice"),
	DUPLICATE_CO("ajout impossible : ce déclarant est déjà contact opérationnel de cet espace"),
    SURVEILLANCE_INTROUVABLE("Cette surveillance est introuvable."),
    ESPACE_ORGANISATION_DEJA_DESINSCRIT("Cet espace collaboratif a déjà fait l'objet d'une demande de désinscription"),
    ECHEC_ENREGISTREMENT_DESINSCRIPTION("Echec lors de l'enregistrement de la désinscription. Veuillez contacter nos services."),
    DESINSCRIPTION_INTROUVABLE("Echec lors de la récupération de la désinscription"),
    DESINSCRIPTION_INTERDITE("Désinscription interdite: des activités/moyens ont été publiés après la date de cessation"),
    DESINSCRIPTION_INTERDITE_FO("Vous ne pouvez pas renseigner de date de cessation d’activités puisque vous avez déclaré des activités pour une période postérieure à la demande, merci de contacter les services de la Haute Autorité."),
    ERREUR_FORMAT_DATE("Impossible de parser la date"),
    ERREUR_EXTRACTION_JSON("Impossible d'extraire les données du pubjson"),
    ;

	private final String message;

	ErrorMessageEnum(final String message) {
		this.message = message;
	}

	/**
	 * Renvoie le message d'érreur.
	 *
	 * @return message d'erreur.
	 */
	public String getMessage() {
		return this.message;
	}

}
