/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import java.util.List;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;

public class SurveillanceBatchDto extends AbstractCommonDto {

	private String idNational;

	private String denomination;

	private List<SurveillanceOrganisationEnum> typeSurveillance;

	private List<Long> surveillantUsers;
	private List<Long> surveillantGroups;

	private String error;

	public SurveillanceBatchDto() {
	}

	public SurveillanceBatchDto(Long id, String idNational, String denomination, List<SurveillanceOrganisationEnum> typeSurveillance, List<Long> surveillantUsers, List<Long> surveillantGroups) {
		this.id = id;
		this.idNational = idNational;
		this.denomination = denomination;
		this.typeSurveillance = typeSurveillance;
		this.setSurveillantUsers(surveillantUsers);
		this.setSurveillantGroups(surveillantGroups);
	}

	public SurveillanceBatchDto(String idNational, String error) {
		this.idNational = idNational;
		this.error = error;
	}

	public String getIdNational() {
		return idNational;
	}

	public void setIdNational(String idNational) {
		this.idNational = idNational;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public List<SurveillanceOrganisationEnum> getTypeSurveillance() {
		return typeSurveillance;
	}

	public void setTypeSurveillance(List<SurveillanceOrganisationEnum> typeSurveillance) {
		this.typeSurveillance = typeSurveillance;
	}



	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return the surveillantGroups
	 */
	public List<Long> getSurveillantGroups() {
		return surveillantGroups;
	}

	/**
	 * @param surveillantGroups the surveillantGroups to set
	 */
	public void setSurveillantGroups(List<Long> surveillantGroups) {
		this.surveillantGroups = surveillantGroups;
	}

	/**
	 * @return the surveillantUsers
	 */
	public List<Long> getSurveillantUsers() {
		return surveillantUsers;
	}

	/**
	 * @param surveillantUsers the surveillantUsers to set
	 */
	public void setSurveillantUsers(List<Long> surveillantUsers) {
		this.surveillantUsers = surveillantUsers;
	}

}
