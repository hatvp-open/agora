/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JsonNode;

import fr.hatvp.registre.commons.dto.EnumAsDto;

/**
 *
 * Classe d'énumération des statuts de publications.
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonFormat(shape = Shape.OBJECT)
public enum StatutPublicationEnum {

	NON_PUBLIEE("Non publiée",5),
	PUBLIEE("Publiée",2),
	DEPUBLIEE("Dépubliée",4),
	REPUBLIEE("Republiée", 1), 
	MAJ_NON_PUBLIEE("Mise à jour non publiée",6), 
	MAJ_PUBLIEE("Mise à jour publiée",3),
	SUPPRIMEE("Supprimée",7);

	private final String label;
	private final Integer ordre;

	StatutPublicationEnum(final String label,final Integer ordre) {
		this.label = label;
		this.ordre = ordre;
	}

	public String getLabel() {
		return label;
	}
	

	public int getOrdre() {
		return ordre;
	}

	@JsonCreator
	public static StatutPublicationEnum fromNode(final JsonNode node) {
		if (!node.has("code")) {
			return null;
		}

		final String name = node.get("code").asText();

		return StatutPublicationEnum.valueOf(name);
	}

	@JsonValue
	public EnumAsDto getStatutPublicationDto() {
		return new EnumAsDto(this.name(), this.label, this.ordre);
	}

}
