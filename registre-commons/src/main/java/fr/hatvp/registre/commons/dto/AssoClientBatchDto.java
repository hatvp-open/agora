/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * DTO Util pour le retour d'appel à l'ajout en masse d'Association/Clients.
 *
 * @version $Revision$ $Date${0xD}
 */
public class AssoClientBatchDto
		extends AbstractCommonDto {
	
	/** id de l'espace organisation. */
	protected Long espaceOrganisationId;

    /** Map des identifiants erronés */
    private Map<String, String> errorMessages = new HashMap<>();

    /** Map des organisations validées */
    private Map<String, AssoClientDto> validEntities = new HashMap<>();

    /**
     * Accesseur en lecture du champ <code>organisationList</code>.
     *
     * @return
     */
    public List<OrganisationDto> getOrganisationList() {
        return organisationList;
    }

    /**
     * Accesseur en écriture du champ <code>organisationList</code>.
     *
     * @param organisationList
     */
    public void setOrganisationList(List<OrganisationDto> organisationList) {
        this.organisationList = organisationList;
    }

    /** Liste des Organisations valorisées */
    @JsonIgnore
    private List<OrganisationDto> organisationList = new ArrayList<>();

    /**
	 * Accesseur en lecture du champ <code>espaceOrganisationId</code>.
	 *
	 * @return le champ <code>espaceOrganisationId</code>.
	 */
	public Long getEspaceOrganisationId() {
		return this.espaceOrganisationId;
	}

	/**
	 * Accesseur en écriture du champ <code>espaceOrganisationId</code>.
	 *
	 * @param espaceOrganisationId
	 * 		la valeur à écrire dans <code>espaceOrganisationId</code>.
	 */
	public void setEspaceOrganisationId(final Long espaceOrganisationId) {
		this.espaceOrganisationId = espaceOrganisationId;
	}

    /**
     * Accesseur en lecture du champ <code>errorMessages</code>.
     *
     * @return le champ <code>errorMessages</code>.
     */
    public Map<String, String> getErrorMessages() {
        return errorMessages;
    }

    /**
     * Accesseur en écriture du champ <code>errorMessages</code>.
     *
     * @param errorMessages
     * 		la valeur à écrire dans <code>errorMessages</code>.
     */
    public void setErrorMessages(Map<String, String> errorMessages) {
        this.errorMessages = errorMessages;
    }

    /**
     *
     * Ajoute un message d'erreur à la map des messages d'erreurs
     * @param nationalId
     * @param errorMessage
     */
    public void addErrorMessage(String nationalId, String errorMessage) {
        this.errorMessages.put(nationalId, errorMessage);
    }

    /**
     * Accesseur en lecture du champ <code>validEntities</code>.
     *
     * @return le champ <code>validEntities</code>.
     */
    public Map<String, AssoClientDto> getValidEntities() {
        return validEntities;
    }

    /**
     * Accesseur en écriture du champ <code>validEntities</code>.
     *
     * @param validEntities
     * 		la valeur à écrire dans <code>validEntities</code>.
     */
    public void setValidEntities(Map<String, AssoClientDto> validEntities) {
        this.validEntities = validEntities;
    }

    /**
     * Ajoute une entité valide à la Map <code>validEntities</code>.
     *
     * @param validEntity
     * 		la valeur à écrire dans <code>validEntities</code>.
     */
    public void addValidEntitiy(AssoClientDto validEntity) {
        this.validEntities.put(validEntity.getNationalId(),validEntity);
    }

}
