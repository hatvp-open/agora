/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

public enum NomenclatureEnum {

	DOMAINE_INTERVENTION, RESPONSABLE_PUBLIC, DECISION_CONCERNEE, ACTION_MENEE, CHIFFRE_AFFAIRE, MONTANT_DEPENSE
}
