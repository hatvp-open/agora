

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonFormat;

import fr.hatvp.registre.commons.lists.StatutEspaceAffichageEnum;

/**
 * DTO de l'entité InscriptionEspaceEntity.
 *
 * @version $Revision$ $Date${0xD}
 */
public class InscriptionEspaceDto extends AbstractCommonDto {

	/** Espace Favori */
	private Boolean favori;

	/** Espace Actif */
	private Boolean actif;

	/** Espace Verrouillé */
	private Boolean verrouille;

	/** Date demande d'inscription à l'espace */
	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
	private Date dateDemande;

	/** Date de validation de la demande par le contact opérationnel */
	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
	private Date dateValidation;

	/** Date de rejet ou suppression de l'inscription du déclarant à l'espace */
	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
	private Date dateSuppression;

	/** Id du déclarant */
	private Long declarantId;

	/** Nom complet du déclarant */
	private String declarantNomComplet;

	/** Email du declarant */
	private String declarantEmail;

	/** Numéro de téléphone du declarant */
	private String declarantTelephone;

	/** Identifiant de l'organisation de l'espace */
	private Long espaceOrganisationId;

	/** Identifiant nationale de l'organisation */
	private String organisationIdentifiantNational;

	/** État de l'espace organisation */
	private Boolean espaceOrganisationActif;

	/** Mandat de l'espace organisation */
	private String espaceOrganisationMandat;

	/** Date de création de l'espace organisation */
	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
	private Date espaceOrganisationDateCreation;

	/** Liste des contacts opérationnels de l'espace Organisation */
	private List<DeclarantDto> administrateurs;

	/** Dénomination de l'organisation */
	private String organisationDenomination;

	/** Déclaration pour des tiers. */
	private Boolean declareForTiers;

	/** Id de l'organisation. */
	private Long organizationId;

	/** Représentant légal. */
	private boolean representantLegal;

	/** Nom d'usage */
	private String nomUsage;

	/** Liste des rôles du déclarant par rapport à l'espace organisation */
	private Set<String> roles;

	/** date de cloture de l'exercice comptable */
	private String finExerciceFiscal;
	
	private String satutDesinscription; //NULL si aucune, DEMANDEE, VALIDEE
	
	private StatutEspaceAffichageEnum statutEspaceAffichage;
	
	private Date datePublicationAffichage;
	
	private boolean identiteIsValide;
	
	private boolean dirigeantIsValide;
	
	private boolean equipeIsValide;
	
	private boolean clientIsValide;
	
	private boolean affiliationIsValide;
	
	private boolean champActiviteIsValide;
	
	private boolean informationManquante;
	
	/**
	 * Accesseur en lecture du champ <code>favori</code>.
	 *
	 * @return le champ <code>favori</code>.
	 */
	public Boolean getFavori() {
		return this.favori;
	}

	/**
	 * Accesseur en écriture du champ <code>favori</code>.
	 *
	 * @param favori
	 *            la valeur à écrire dans <code>favori</code>.
	 */
	public void setFavori(final Boolean favori) {
		this.favori = favori;
	}

	/**
	 * Accesseur en lecture du champ <code>actif</code>.
	 *
	 * @return le champ <code>actif</code>.
	 */
	public Boolean getActif() {
		return this.actif;
	}

	/**
	 * Accesseur en écriture du champ <code>actif</code>.
	 *
	 * @param actif
	 *            la valeur à écrire dans <code>actif</code>.
	 */
	public void setActif(final Boolean actif) {
		this.actif = actif;
	}

	/**
	 * Accesseur en lecture du champ <code>espaceVerrouille</code>.
	 *
	 * @return le champ <code>espaceVerrouille</code>.
	 */
	public Boolean getVerrouille() {
		return this.verrouille;
	}

	/**
	 * Accesseur en écriture du champ <code>verrouille</code>.
	 *
	 * @param verrouille
	 *            la valeur à écrire dans <code>verrouille</code>.
	 */
	public void setVerrouille(final Boolean verrouille) {
		this.verrouille = verrouille;
	}

	/**
	 * Accesseur en lecture du champ <code>dateDemande</code>.
	 *
	 * @return le champ <code>dateDemande</code>.
	 */
	public Date getDateDemande() {
		return this.dateDemande;
	}

	/**
	 * Accesseur en écriture du champ <code>dateDemande</code>.
	 *
	 * @param dateDemande
	 *            la valeur à écrire dans <code>dateDemande</code>.
	 */
	public void setDateDemande(final Date dateDemande) {
		this.dateDemande = dateDemande;
	}

	/**
	 * Accesseur en lecture du champ <code>dateValidation</code>.
	 *
	 * @return le champ <code>dateValidation</code>.
	 */
	public Date getDateValidation() {
		return this.dateValidation;
	}

	/**
	 * Accesseur en écriture du champ <code>dateValidation</code>.
	 *
	 * @param dateValidation
	 *            la valeur à écrire dans <code>dateValidation</code>.
	 */
	public void setDateValidation(final Date dateValidation) {
		this.dateValidation = dateValidation;
	}

	/**
	 * Accesseur en lecture du champ <code>dateSuppression</code>.
	 *
	 * @return le champ <code>dateSuppression</code>.
	 */
	public Date getDateSuppression() {
		return this.dateSuppression;
	}

	/**
	 * Accesseur en écriture du champ <code>dateSuppression</code>.
	 *
	 * @param dateSuppression
	 *            la valeur à écrire dans <code>dateSuppression</code>.
	 */
	public void setDateSuppression(final Date dateSuppression) {
		this.dateSuppression = dateSuppression;
	}

	/**
	 * Accesseur en lecture du champ <code>declarantNomComplet</code>.
	 *
	 * @return le champ <code>declarantNomComplet</code>.
	 */
	public String getDeclarantNomComplet() {
		return this.declarantNomComplet;
	}

	/**
	 * Accesseur en écriture du champ <code>declarantNomComplet</code>.
	 *
	 * @param declarantNomComplet
	 *            la valeur à écrire dans <code>declarantNomComplet</code>.
	 */
	public void setDeclarantNomComplet(final String declarantNomComplet) {
		this.declarantNomComplet = declarantNomComplet;
	}

	/**
	 * Accesseur en lecture du champ <code>declarantEmail</code>.
	 *
	 * @return le champ <code>declarantEmail</code>.
	 */
	public String getDeclarantEmail() {
		return this.declarantEmail;
	}

	/**
	 * Accesseur en écriture du champ <code>declarantEmail</code>.
	 *
	 * @param declarantEmail
	 *            la valeur à écrire dans <code>declarantEmail</code>.
	 */
	public void setDeclarantEmail(final String declarantEmail) {
		this.declarantEmail = declarantEmail;
	}

	/**
	 * Accesseur en lecture du champ <code>espaceOrganisationId</code>.
	 *
	 * @return le champ <code>espaceOrganisationId</code>.
	 */
	public Long getEspaceOrganisationId() {
		return this.espaceOrganisationId;
	}

	/**
	 * Accesseur en écriture du champ <code>espaceOrganisationId</code>.
	 *
	 * @param espaceOrganisationId
	 *            la valeur à écrire dans <code>espaceOrganisationId</code>.
	 */
	public void setEspaceOrganisationId(final Long espaceOrganisationId) {
		this.espaceOrganisationId = espaceOrganisationId;
	}

	/**
	 * Accesseur en lecture du champ <code>organisationIdentifiantNational</code>.
	 *
	 * @return le champ <code>organisationIdentifiantNational</code>.
	 */
	public String getOrganisationIdentifiantNational() {
		return this.organisationIdentifiantNational;
	}

	/**
	 * Accesseur en écriture du champ <code>organisationIdentifiantNational</code>.
	 *
	 * @param organisationIdentifiantNational
	 *            la valeur à écrire dans <code>organisationIdentifiantNational</code>.
	 */
	public void setOrganisationIdentifiantNational(final String organisationIdentifiantNational) {
		this.organisationIdentifiantNational = organisationIdentifiantNational;
	}

	/**
	 * Accesseur en lecture du champ <code>organisationDenomination</code>.
	 *
	 * @return le champ <code>organisationDenomination</code>.
	 */
	public String getOrganisationDenomination() {
		return this.organisationDenomination;
	}

	/**
	 * Accesseur en écriture du champ <code>organisationDenomination</code>.
	 *
	 * @param organisationDenomination
	 *            la valeur à écrire dans <code>organisationDenomination</code>.
	 */
	public void setOrganisationDenomination(final String organisationDenomination) {
		this.organisationDenomination = organisationDenomination;
	}

	/**
	 * Accesseur en lecture du champ <code>declareForTiers</code>.
	 *
	 * @return le champ <code>declareForTiers</code>.
	 */
	public Boolean getDeclareForTiers() {
		return this.declareForTiers;
	}

	/**
	 * Accesseur en écriture du champ <code>declareForTiers</code>.
	 *
	 * @param declareForTiers
	 *            la valeur à écrire dans <code>declareForTiers</code>.
	 */
	public void setDeclareForTiers(final Boolean declareForTiers) {
		this.declareForTiers = declareForTiers;
	}

	/**
	 * Accesseur en lecture du champ <code>organizationId</code>.
	 *
	 * @return le champ <code>organizationId</code>.
	 */
	public Long getOrganizationId() {
		return this.organizationId;
	}

	/**
	 * Accesseur en écriture du champ <code>organizationId</code>.
	 *
	 * @param organizationId
	 *            la valeur à écrire dans <code>organizationId</code>.
	 */
	public void setOrganizationId(final Long organizationId) {
		this.organizationId = organizationId;
	}

	/**
	 * Accesseur en lecture du champ <code>roles</code>.
	 *
	 * @return le champ <code>roles</code>.
	 */
	public Set<String> getRoles() {
		return this.roles;
	}

	/**
	 * Accesseur en écriture du champ <code>roles</code>.
	 *
	 * @param roles
	 *            la valeur à écrire dans <code>roles</code>.
	 */
	public void setRoles(final Set<String> roles) {
		this.roles = roles;
	}

	/**
	 * Accesseur en lecture du champ <code>declarantId</code>.
	 *
	 * @return le champ <code>declarantId</code>.
	 */
	public Long getDeclarantId() {
		return this.declarantId;
	}

	/**
	 * Accesseur en écriture du champ <code>declarantId</code>.
	 *
	 * @param declarantId
	 *            la valeur à écrire dans <code>espaceOrganisationAdministrateur</code>.
	 */
	public void setDeclarantId(final Long declarantId) {
		this.declarantId = declarantId;
	}

	/**
	 * Accesseur en lecture du champ <code>espaceOrganisationActif</code>.
	 *
	 * @return le champ <code>espaceOrganisationActif</code>.
	 */
	public Boolean isEspaceOrganisationActif() {
		return this.espaceOrganisationActif;
	}

	/**
	 * Accesseur en écriture du champ <code>espaceOrganisationActif</code>.
	 *
	 * @param espaceOrganisationActif
	 *            la valeur à écrire dans <code>espaceOrganisationActif</code>.
	 */
	public void setEspaceOrganisationActif(final Boolean espaceOrganisationActif) {
		this.espaceOrganisationActif = espaceOrganisationActif;
	}

	/**
	 * Accesseur en lecture du champ <code>administrateurs</code>.
	 *
	 * @return le champ <code>administrateurs</code>.
	 */
	public List<DeclarantDto> getAdministrateurs() {
		return this.administrateurs;
	}

	/**
	 * Accesseur en écriture du champ <code>administrateurs</code>.
	 *
	 * @param administrateurs
	 *            la valeur à écrire dans <code>administrateurs</code>.
	 */
	public void setAdministrateurs(final List<DeclarantDto> administrateurs) {
		this.administrateurs = administrateurs;
	}

	/**
	 * Accesseur en lecture du champ <code>espaceOrganisationDateCreation</code>.
	 *
	 * @return le champ <code>espaceOrganisationDateCreation</code>.
	 */
	public Date getEspaceOrganisationDateCreation() {
		return this.espaceOrganisationDateCreation;
	}

	/**
	 * Accesseur en écriture du champ <code>espaceOrganisationDateCreation</code>.
	 *
	 * @param espaceOrganisationDateCreation
	 *            la valeur à écrire dans <code>espaceOrganisationDateCreation</code>.
	 */
	public void setEspaceOrganisationDateCreation(final Date espaceOrganisationDateCreation) {
		this.espaceOrganisationDateCreation = espaceOrganisationDateCreation;
	}

	/**
	 * Accesseur en lecture du champ <code>espaceOrganisationMandat</code>.
	 *
	 * @return le champ <code>espaceOrganisationMandat</code>.
	 */
	public String getEspaceOrganisationMandat() {
		return this.espaceOrganisationMandat;
	}

	/**
	 * Accesseur en écriture du champ <code>espaceOrganisationMandat</code>.
	 *
	 * @param espaceOrganisationMandat
	 *            la valeur à écrire dans <code>espaceOrganisationMandat</code>.
	 */
	public void setEspaceOrganisationMandat(final String espaceOrganisationMandat) {
		this.espaceOrganisationMandat = espaceOrganisationMandat;
	}

	/**
	 * Gets declarant telephone.
	 *
	 * @return the declarant telephone
	 */
	public String getDeclarantTelephone() {
		return this.declarantTelephone;
	}

	/**
	 * Sets declarant telephone.
	 *
	 * @param declarantTelephone
	 *            the declarant telephone
	 */
	public void setDeclarantTelephone(final String declarantTelephone) {
		this.declarantTelephone = declarantTelephone;
	}

	/**
	 * Is representant legal boolean.
	 *
	 * @return the boolean
	 */
	public boolean isRepresentantLegal() {
		return this.representantLegal;
	}

	/**
	 * Sets representant legal.
	 *
	 * @param representantLegal
	 *            the representant legal
	 */
	public void setRepresentantLegal(final boolean representantLegal) {
		this.representantLegal = representantLegal;
	}

	/**
	 * Gets nomUsage
	 *
	 * @return the nomUsage
	 */
	public String getNomUsage() {
		return nomUsage;
	}

	/**
	 * Sets nomUsage.
	 *
	 * @param nomUsage
	 *            the nomUsage
	 */
	public void setNomUsage(String nomUsage) {
		this.nomUsage = nomUsage;
	}

	/**
	 * @return the finExerciceFiscal
	 */
	public String getFinExerciceFiscal() {
		return finExerciceFiscal;
	}

	/**
	 * @param finExerciceFiscal
	 *            the finExerciceFiscal to set
	 */
	public void setFinExerciceFiscal(String finExerciceFiscal) {
		this.finExerciceFiscal = finExerciceFiscal;
	}

	/**
	 * @return the espaceOrganisationActif
	 */
	public Boolean getEspaceOrganisationActif() {
		return espaceOrganisationActif;
	}
	
	public String getSatutDesinscription() {
		return satutDesinscription;
	}

	public void setSatutDesinscription(String satutDesinscription) {
		this.satutDesinscription = satutDesinscription;
	}

	public boolean isIdentiteIsValide() {
		return identiteIsValide;
	}

	public void setIdentiteIsValide(boolean identiteIsValide) {
		this.identiteIsValide = identiteIsValide;
	}

	public boolean isDirigeantIsValide() {
		return dirigeantIsValide;
	}

	public void setDirigeantIsValide(boolean dirigeantIsValide) {
		this.dirigeantIsValide = dirigeantIsValide;
	}

	public boolean isEquipeIsValide() {
		return equipeIsValide;
	}

	public void setEquipeIsValide(boolean equipeIsValide) {
		this.equipeIsValide = equipeIsValide;
	}

	public boolean isClientIsValide() {
		return clientIsValide;
	}

	public void setClientIsValide(boolean clientIsValide) {
		this.clientIsValide = clientIsValide;
	}

	public boolean isAffiliationIsValide() {
		return affiliationIsValide;
	}

	public void setAffiliationIsValide(boolean affiliationIsValide) {
		this.affiliationIsValide = affiliationIsValide;
	}

	public boolean isChampActiviteIsValide() {
		return champActiviteIsValide;
	}

	public void setChampActiviteIsValide(boolean champActiviteIsValide) {
		this.champActiviteIsValide = champActiviteIsValide;
	}

	public boolean isInformationManquante() {
		return informationManquante;
	}

	public void setInformationManquante(boolean informationManquante) {
		this.informationManquante = informationManquante;
	}

	public StatutEspaceAffichageEnum getStatutEspaceAffichage() {
		return statutEspaceAffichage;
	}

	public void setStatutEspaceAffichage(StatutEspaceAffichageEnum statutEspaceAffichage) {
		this.statutEspaceAffichage = statutEspaceAffichage;
	}

	public Date getDatePublicationAffichage() {
		return datePublicationAffichage;
	}

	public void setDatePublicationAffichage(Date datePublicationAffichage) {
		this.datePublicationAffichage = datePublicationAffichage;
	}

}
