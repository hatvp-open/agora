/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

public class ConsultationEspaceBoSimpleDto extends AbstractCommonDto {

	private String denomination;

	private String nomUsage;

	private String nationalId;

	private List<ContactOperationnelSimpleDto> administrateurs;

	private Integer nombreDeclarant;

	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
	private Date creationDate;

	private Boolean isPublication;

	/** date de cloture de l'exercice comptable */
	private String finExerciceFiscal;

	private Boolean actif;
	private boolean relancesBloquees;
	private String sigleHatvp;
	private String ancienNomHatvp;
	private String nomUsageHatvp;

	private Long organisationId;

	private Boolean desinscriptionAgent;

	/**
	 * @return the denomination
	 */
	public String getDenomination() {
		return denomination;
	}

	/**
	 * @param denomination
	 *            the denomination to set
	 */
	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	/**
	 * @return the nomUsage
	 */
	public String getNomUsage() {
		return nomUsage;
	}

	/**
	 * @param nomUsage
	 *            the nomUsage to set
	 */
	public void setNomUsage(String nomUsage) {
		this.nomUsage = nomUsage;
	}

	/**
	 * @return the nationalId
	 */
	public String getNationalId() {
		return nationalId;
	}

	/**
	 * @param nationalId
	 *            the nationalId to set
	 */
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	/**
	 * @return the administrateurs
	 */
	public List<ContactOperationnelSimpleDto> getAdministrateurs() {
		return administrateurs;
	}

	/**
	 * @param administrateurs
	 *            the administrateurs to set
	 */
	public void setAdministrateurs(List<ContactOperationnelSimpleDto> administrateurs) {
		this.administrateurs = administrateurs;
	}

	/**
	 * @return the nombreDeclarant
	 */
	public Integer getNombreDeclarant() {
		return nombreDeclarant;
	}

	/**
	 * @param nombreDeclarant
	 *            the nombreDeclarant to set
	 */
	public void setNombreDeclarant(Integer nombreDeclarant) {
		this.nombreDeclarant = nombreDeclarant;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the isPublication
	 */
	public Boolean getIsPublication() {
		return isPublication;
	}

	/**
	 * @param isPublication
	 *            the isPublication to set
	 */
	public void setIsPublication(Boolean isPublication) {
		this.isPublication = isPublication;
	}

	/**
	 * @return the finExerciceFiscal
	 */
	public String getFinExerciceFiscal() {
		return finExerciceFiscal;
	}

	/**
	 * @param finExerciceFiscal
	 *            the finExerciceFiscal to set
	 */
	public void setFinExerciceFiscal(String finExerciceFiscal) {
		this.finExerciceFiscal = finExerciceFiscal;
	}

	/**
	 * @return the actif
	 */
	public Boolean getActif() {
		return actif;
	}

	public boolean isRelancesBloquees() {
		return relancesBloquees;
	}

	public void setRelancesBloquees(boolean relancesBloquees) {
		this.relancesBloquees = relancesBloquees;
	}

	/**
	 * @param actif
	 *            the actif to set
	 */
	public void setActif(Boolean actif) {
		this.actif = actif;
	}

	/**
	 * @return the sigleHatvp
	 */
	public String getSigleHatvp() {
		return sigleHatvp;
	}

	/**
	 * @param sigleHatvp
	 *            the sigleHatvp to set
	 */
	public void setSigleHatvp(String sigleHatvp) {
		this.sigleHatvp = sigleHatvp;
	}

	/**
	 * @return the ancienNomHatvp
	 */
	public String getAncienNomHatvp() {
		return ancienNomHatvp;
	}

	/**
	 * @param ancienNomHatvp
	 *            the ancienNomHatvp to set
	 */
	public void setAncienNomHatvp(String ancienNomHatvp) {
		this.ancienNomHatvp = ancienNomHatvp;
	}

	/**
	 * @return the nomUsageHatvp
	 */
	public String getNomUsageHatvp() {
		return nomUsageHatvp;
	}

	/**
	 * @param nomUsageHatvp
	 *            the nomUsageHatvp to set
	 */
	public void setNomUsageHatvp(String nomUsageHatvp) {
		this.nomUsageHatvp = nomUsageHatvp;
	}

	/**
	 * @return the organisationId
	 */
	public Long getOrganisationId() {
		return organisationId;
	}

	/**
	 * @param organisationId
	 *            the organisationId to set
	 */
	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}

    public Boolean getDesinscriptionAgent() {
        return desinscriptionAgent;
    }

    public void setDesinscriptionAgent(Boolean desinscriptionAgent) {
        this.desinscriptionAgent = desinscriptionAgent;
    }
}
