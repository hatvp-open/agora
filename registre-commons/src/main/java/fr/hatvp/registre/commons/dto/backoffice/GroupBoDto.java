/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

import java.util.List;

public class GroupBoDto extends AbstractCommonDto {

    private String libelle;

    private List<UtilisateurBoDto> utilisateurBoDto;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public List<UtilisateurBoDto> getUtilisateurBoDto() {
        return utilisateurBoDto;
    }

    public void setUtilisateurBoDto(List<UtilisateurBoDto> utilisateurBoDto) {
        this.utilisateurBoDto = utilisateurBoDto;
    }
}
