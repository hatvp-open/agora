/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

public enum ContextDemandeOrganisationEnum {
    Ajout_Client, Ajout_Espace, Ajout_Asso_Appartenance
}
