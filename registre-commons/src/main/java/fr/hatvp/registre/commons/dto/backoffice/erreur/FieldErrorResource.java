/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice.erreur;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FieldErrorResource {
    private String resource;
    private String field;
    private String code;
    private String message;
	/**
	 * Accesseur en lecture du champ <code>resource</code>.
	 * @return le champ <code>resource</code>.
	 */
	public String getResource() {
        return this.resource;
	}
	/**
	 * Accesseur en écriture du champ <code>resource</code>.
	 * @param resource la valeur à écrire dans <code>resource</code>.
	 */
    public void setResource(final String resource)
    {
		this.resource = resource;
	}
	/**
	 * Accesseur en lecture du champ <code>field</code>.
	 * @return le champ <code>field</code>.
	 */
	public String getField() {
        return this.field;
	}
	/**
	 * Accesseur en écriture du champ <code>field</code>.
	 * @param field la valeur à écrire dans <code>field</code>.
	 */
    public void setField(final String field)
    {
		this.field = field;
	}
	/**
	 * Accesseur en lecture du champ <code>code</code>.
	 * @return le champ <code>code</code>.
	 */
	public String getCode() {
        return this.code;
	}
	/**
	 * Accesseur en écriture du champ <code>code</code>.
	 * @param code la valeur à écrire dans <code>code</code>.
	 */
    public void setCode(final String code)
    {
		this.code = code;
	}
	/**
	 * Accesseur en lecture du champ <code>message</code>.
	 * @return le champ <code>message</code>.
	 */
	public String getMessage() {
        return this.message;
	}
	/**
	 * Accesseur en écriture du champ <code>message</code>.
	 * @param message la valeur à écrire dans <code>message</code>.
	 */
    public void setMessage(final String message)
    {
		this.message = message;
	}

    
    
}
