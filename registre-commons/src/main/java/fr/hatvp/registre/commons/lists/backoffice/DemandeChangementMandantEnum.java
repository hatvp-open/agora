/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists.backoffice;

public enum DemandeChangementMandantEnum {
	NOUVEAU, VALIDEE, REFUSEE, TOUTES_LES_DEMANDES
}
