/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.utils.LocalDateDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateSerializer;

public class ExerciceComptableSimpleDto extends AbstractCommonDto {

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateDebut;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateFin;

	private Long chiffreAffaire;

	private Boolean hasNotChiffreAffaire;

	@NotNull
	private Long montantDepense;

	@NotNull
	private Integer nombreSalaries;

	private Long espaceOrganisationId;
	
	private StatutPublicationEnum statut;
	private Boolean dataToPublish;
	private String commentaire;

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public ExerciceComptableSimpleDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExerciceComptableSimpleDto(LocalDate dateDebut, LocalDate dateFin,Long espaceOrganisationId, String commentaire) {
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.espaceOrganisationId = espaceOrganisationId;
		this.commentaire = commentaire;
	}

	/**
	 * @return the dateDebut
	 */
	public LocalDate getDateDebut() {
		return dateDebut;
	}

	/**
	 * @param dateDebut
	 *            the dateDebut to set
	 */
	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	/**
	 * @return the dateFin
	 */
	public LocalDate getDateFin() {
		return dateFin;
	}

	/**
	 * @param dateFin
	 *            the dateFin to set
	 */
	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	/**
	 * @return the chiffreAffaire
	 */
	public Long getChiffreAffaire() {
		return chiffreAffaire;
	}

	/**
	 * @param chiffreAffaire
	 *            the chiffreAffaire to set
	 */
	public void setChiffreAffaire(Long chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	/**
	 * @return the hasNotChiffreAffaire
	 */
	public Boolean getHasNotChiffreAffaire() {
		return hasNotChiffreAffaire;
	}

	/**
	 * @param hasNotChiffreAffaire
	 *            the hasNotChiffreAffaire to set
	 */
	public void setHasNotChiffreAffaire(Boolean hasNotChiffreAffaire) {
		this.hasNotChiffreAffaire = hasNotChiffreAffaire;
	}

	/**
	 * @return the montantDepense
	 */
	public Long getMontantDepense() {
		return montantDepense;
	}

	/**
	 * @param montantDepense
	 *            the montantDepense to set
	 */
	public void setMontantDepense(Long montantDepense) {
		this.montantDepense = montantDepense;
	}

	/**
	 * @return the nombreSalaries
	 */
	public Integer getNombreSalaries() {
		return nombreSalaries;
	}

	/**
	 * @param nombreSalaries
	 *            the nombreSalaries to set
	 */
	public void setNombreSalaries(Integer nombreSalaries) {
		this.nombreSalaries = nombreSalaries;
	}

	/**
	 * @return the espaceOrganisationId
	 */
	public Long getEspaceOrganisationId() {
		return espaceOrganisationId;
	}

	/**
	 * @param espaceOrganisationId
	 *            the espaceOrganisationId to set
	 */
	public void setEspaceOrganisationId(Long espaceOrganisationId) {
		this.espaceOrganisationId = espaceOrganisationId;
	}
	
	/**
	 *
	 * @return statut
	 */
	public StatutPublicationEnum getStatut() {
		return statut;
	}

	/**
	 *
	 * @param statut
	 */
	public void setStatut(StatutPublicationEnum statut) {
		this.statut = statut;
	}

	/**
	 * @return the dataToPublish
	 */
	public Boolean getDataToPublish() {
		return dataToPublish;
	}

	/**
	 * @param dataToPublish
	 *            the dataToPublish to set
	 */
	public void setDataToPublish(Boolean dataToPublish) {
		this.dataToPublish = dataToPublish;
	}

}
