/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.util.ArrayList;
import java.util.List;

import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;

public class ListTypeSurveillanceDto {

	private List<SurveillanceOrganisationEnum> surveillanceOrganisationEnumList = new ArrayList<>();

	public List<SurveillanceOrganisationEnum> getSurveillanceOrganisationEnumList() {
		return surveillanceOrganisationEnumList;
	}

	public void setSurveillanceOrganisationEnumList(List<SurveillanceOrganisationEnum> surveillanceOrganisationEnumList) {
		this.surveillanceOrganisationEnumList = surveillanceOrganisationEnumList;
	}
}
