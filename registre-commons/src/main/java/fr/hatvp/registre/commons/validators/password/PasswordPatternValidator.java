/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.validators.password;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 
 * Classe de validation du mot de passe.
 * 
 * @version $Revision$ $Date${0xD}
 */
public class PasswordPatternValidator
    implements ConstraintValidator<PasswordPattern, String>
{

    /** Mot de passe saisi. */
    private String PASSWORD_PATTERN;

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(final PasswordPattern constraintAnnotation)
    {
        this.PASSWORD_PATTERN = constraintAnnotation.value();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context)
    {

        return (value == null) || value.matches(this.PASSWORD_PATTERN);

    }
}
