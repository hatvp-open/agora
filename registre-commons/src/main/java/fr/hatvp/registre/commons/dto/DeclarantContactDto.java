/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import fr.hatvp.registre.commons.lists.CategoryTelephoneMail;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * DTO de l'entité DeclarantContact.
 *
 * @version $Revision$ $Date${0xD}
 */
public class DeclarantContactDto
        extends AbstractCommonDto
{
    /** Email. */
    @Size(max = 150)
    private String email;

    /** Numéro de téléphone. */
    @Size(max = 15)
    private String telephone;

    /** Catégorie du contact. */
    @NotNull
    private CategoryTelephoneMail categorie;

    /**
     * Accesseur en lecture du champ <code>email</code>.
     *
     * @return le champ <code>email</code>.
     */
    public String getEmail()
    {
        return this.email;
    }

    /**
     * Accesseur en écriture du champ <code>email</code>.
     *
     * @param email
     *         la valeur à écrire dans <code>email</code>.
     */
    public void setEmail(final String email)
    {
        this.email = email;
    }

    /**
     * Accesseur en lecture du champ <code>telephone</code>.
     *
     * @return le champ <code>telephone</code>.
     */
    public String getTelephone()
    {
        return this.telephone;
    }

    /**
     * Accesseur en écriture du champ <code>telephone</code>.
     *
     * @param telephone
     *         la valeur à écrire dans <code>telephone</code>.
     */
    public void setTelephone(final String telephone)
    {
        this.telephone = telephone;
    }

    /**
     * Accesseur en lecture du champ <code>categorie</code>.
     *
     * @return le champ <code>categorie</code>.
     */
    public CategoryTelephoneMail getCategorie()
    {
        return this.categorie;
    }

    /**
     * Accesseur en écriture du champ <code>categorie</code>.
     *
     * @param categorie
     *         la valeur à écrire dans <code>categorie</code>.
     */
    public void setCategorie(final CategoryTelephoneMail categorie)
    {
        this.categorie = categorie;
    }

}
