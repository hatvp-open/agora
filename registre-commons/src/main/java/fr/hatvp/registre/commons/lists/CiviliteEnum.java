/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

/**
 * 
 * Classe d'énumération des civilités.
 * 
 * @version $Revision$ $Date${0xD}
 */
public enum CiviliteEnum {

    M("M.", "Monsieur"), MME("Mme", "Madame");

    private String libelleCourt;
    private String libelleLong;

    CiviliteEnum(final String libelleCourt, final String libelleLong)
    {
        this.libelleCourt = libelleCourt;
        this.libelleLong = libelleLong;
    }

    /**
     * Accesseur en lecture du champ <code>libelleCourt</code>.
     * @return le champ <code>libelleCourt</code>.
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * Accesseur en lecture du champ <code>libelleLong</code>.
     * @return le champ <code>libelleLong</code>.
     */
    public String getLibelleLong() {
        return libelleLong;
    }

}
