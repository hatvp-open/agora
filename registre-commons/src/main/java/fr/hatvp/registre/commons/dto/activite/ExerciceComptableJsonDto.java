/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite;

import java.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.utils.LocalDateDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateSerializer;

/**
 * DTO pour la récupération des données d'exercices comptables
 * publiées contenues dans le pub_json
 *
 */
public class ExerciceComptableJsonDto extends AbstractCommonDto {

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateDebut;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateFin;

	private String chiffreAffaire;

	private Boolean hasNotChiffreAffaire;
	
	private String commentaire;

	private String montantDepense;

	private Integer nombreSalaries;

	private Long exerciceId;
	
	private Boolean noActivite;
	
	private int nombreActivite;
	
	private Boolean defautDeclaration;

	private boolean blacklisted;	


	public ExerciceComptableJsonDto() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the dateDebut
	 */
	public LocalDate getDateDebut() {
		return dateDebut;
	}

	/**
	 * @param dateDebut
	 *            the dateDebut to set
	 */
	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	/**
	 * @return the dateFin
	 */
	public LocalDate getDateFin() {
		return dateFin;
	}

	/**
	 * @param dateFin
	 *            the dateFin to set
	 */
	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	/**
	 * @return the chiffreAffaire
	 */
	public String getChiffreAffaire() {
		return chiffreAffaire;
	}

	/**
	 * @param chiffreAffaire
	 *            the chiffreAffaire to set
	 */
	public void setChiffreAffaire(String chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	/**
	 * @return the hasNotChiffreAffaire
	 */
	public Boolean getHasNotChiffreAffaire() {
		return hasNotChiffreAffaire;
	}

	/**
	 * @param hasNotChiffreAffaire
	 *            the hasNotChiffreAffaire to set
	 */
	public void setHasNotChiffreAffaire(Boolean hasNotChiffreAffaire) {
		this.hasNotChiffreAffaire = hasNotChiffreAffaire;
	}

	/**
	 * @return the montantDepense
	 */
	public String getMontantDepense() {
		return montantDepense;
	}

	/**
	 * @param montantDepense
	 *            the montantDepense to set
	 */
	public void setMontantDepense(String montantDepense) {
		this.montantDepense = montantDepense;
	}

	/**
	 * @return the nombreSalaries
	 */
	public Integer getNombreSalaries() {
		return nombreSalaries;
	}

	/**
	 * @param nombreSalaries
	 *            the nombreSalaries to set
	 */
	public void setNombreSalaries(Integer nombreSalaries) {
		this.nombreSalaries = nombreSalaries;
	}


	public Long getExerciceId() {
		return exerciceId;
	}


	public void setExerciceId(Long exerciceId) {
		this.exerciceId = exerciceId;
	}


	public Boolean getNoActivite() {
		return noActivite;
	}


	public void setNoActivite(Boolean noActivite) {
		this.noActivite = noActivite;
	}


	public int getNombreActivite() {
		return nombreActivite;
	}


	public void setNombreActivite(int nombreActivite) {
		this.nombreActivite = nombreActivite;
	}


	public Boolean getDefautDeclaration() {
		return defautDeclaration;
	}


	public void setDefautDeclaration(Boolean defautDeclaration) {
		this.defautDeclaration = defautDeclaration;
	}


	public String getCommentaire() {
		return commentaire;
	}


	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}


	public boolean isBlacklisted() {
		return blacklisted;
	}


	public void setBlacklisted(boolean blacklisted) {
		this.blacklisted = blacklisted;
	}

	
}
