/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.beanutils.PropertyUtilsBean;

public class CompareBeans {
	
	public static boolean compareModelObjects( Object oldObject, Object newObject, String[] fieldsToIgnore, String[] fieldsOfModelsType )
	         throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
	   {
	      if ( !oldObject.getClass().getSimpleName().equals( newObject.getClass().getSimpleName() ) )
	      { 
	         return false;
	      }
	      BeanMap beanMap = new BeanMap( oldObject );
	      PropertyUtilsBean propertyUtils = new PropertyUtilsBean();
	      boolean result = true; 
	      for ( Object propertyNameObject : beanMap.keySet() )
	      {
	         String propertyName = ( String ) propertyNameObject;
	         if ( Arrays.asList( fieldsToIgnore ).contains( propertyName ) )
	         { 
	            continue;
	         }
	         Object oldObjPropertyValue = propertyUtils.getProperty( oldObject, propertyName ); 
	         Object newObjPropertyValue = propertyUtils.getProperty( newObject, propertyName ); 
	         if ( oldObjPropertyValue == null && newObjPropertyValue == null )
	         { 
	            continue;
	         }
	         else if ( ( oldObjPropertyValue != null && newObjPropertyValue == null ) || ( oldObjPropertyValue == null && newObjPropertyValue != null ) )
	         { 
	            return false;
	         }
	         
	         if ( Arrays.asList( fieldsOfModelsType ).contains( propertyName ) )
	         {
	        	 
	            result = compareModelObjects( oldObjPropertyValue, newObjPropertyValue, fieldsToIgnore, fieldsOfModelsType );
	         }
	         else if ( newObjPropertyValue instanceof ArrayList< ? > )
	         {
	            if ( !( oldObjPropertyValue instanceof ArrayList< ? > ) )
	            {
	               return false;
	            }
	            //les deux objets sont des listes à comparer
	            if ( ( ( ArrayList< ? > ) newObjPropertyValue ).size() != ( ( ArrayList< ? > ) oldObjPropertyValue ).size() )
	            {
	               return false;
	            }
	            for ( Object nouveau : ( ArrayList< ? > ) newObjPropertyValue )
	            {
	               for ( Object ancien : ( ArrayList< ? > ) oldObjPropertyValue )
	               {

	                  result = compareModelObjects( ancien,nouveau, fieldsToIgnore, fieldsOfModelsType );

	                  if ( result ) 
	                  {
	                     break;
	                  }
	               }
	               if (!result) return result;
	            }

	         }
	         else 
	         { 
	        	 result = oldObjPropertyValue.equals(newObjPropertyValue) ;
	         }
	         if ( !result ) 
	         {
	            return result;
	         }
	      }
	      return result; 
	   }

}
