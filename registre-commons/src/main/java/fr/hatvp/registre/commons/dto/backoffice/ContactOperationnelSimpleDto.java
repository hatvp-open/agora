/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

public class ContactOperationnelSimpleDto extends AbstractCommonDto {

	private String identite;
	private Boolean actif;
	
	public ContactOperationnelSimpleDto() {}

	
	
	public ContactOperationnelSimpleDto(Long id, String identite,Boolean actif) {
		this.id = id;
		this.identite = identite;
		this.actif = actif;
	}

	/**
	 * @return the identite
	 */
	public String getIdentite() {
		return identite;
	}

	/**
	 * @param identite
	 *            the identite to set
	 */
	public void setIdentite(String identite) {
		this.identite = identite;
	}

	public Boolean getActif() {
		return actif;
	}

	public void setActif(Boolean actif) {
		this.actif = actif;
	}

	
}
