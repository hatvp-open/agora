/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.util.List;

/**
 *
 * @param <T>
 */
public class PaginatedDto<T extends AbstractCommonDto> extends AbstractCommonDto {

	/**
	 *
	 */
    private List<T> dtos;

	/**
	 *
	 */
	private Long resultTotalNumber;

	/**
	 *
	 * @param espaceDtos
	 * @param resultTotalNumber
	 */
	public PaginatedDto(List<T> espaceDtos, Long resultTotalNumber) {
		super();
		this.dtos = espaceDtos;
		this.resultTotalNumber = resultTotalNumber;
	}

	/**
	 *
	 * @return
	 */
	public List<T> getDtos() {
		return dtos;
	}

	/**
	 *
	 * @param dtos
	 */
	public void setDtos(List<T> dtos) {
		this.dtos = dtos;
	}

	/**
	 *
	 * @return
	 */
	public Long getResultTotalNumber() {
		return resultTotalNumber;
	}

	/**
	 *
	 * @param resultTotalNumber
	 */
	public void setResultTotalNumber(Long resultTotalNumber) {
		this.resultTotalNumber = resultTotalNumber;
	}

}
