/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketExerciceDto;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;

/**
 * DTO de l'entité Publication.
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PublicationDto
    extends AbstractCommonDto {

    /** Identifiant national de l'organisation. */
    @JsonProperty("identifiantNational")
    private String nationalId;

    /** Type identifiant national (SIREN/RNA). */
    private TypeIdentifiantNationalEnum typeIdentifiantNational;

    /** Denomination de l'organisation. */
    private String denomination;

    /** Nom d'usage de l'organisation. */
    private String nomUsage;

    /** Catégorie de l'espace organisation */
    private TypeOrganisationEnum categorieOrganisation;

    /** Adresse. */
    private String adresse;

    /** Publier mon adresse physique. */
    private Boolean publierMonAdressePhysique;

    /** Code Postal. */
    private String codePostal;

    /** Ville. */
    private String ville;

    /** Pays. */
    private String pays;

    /** Publier mon adresse email. */
    private Boolean publierMonAdresseEmail;

    /** Email de contact. */
    private String emailDeContact;

    /** Publier mon numéro de téléphone de contact. */
    private Boolean publierMonTelephoneDeContact;

    /** téléphone de contact. */
    private String telephoneDeContact;

    /** Site internet. */
    private String lienSiteWeb;

    /** page twiter. */
    private String lienPageTwitter;

    /** page facebook. */
    private String lienPageFacebook;

    /** page linkedin. */
    private String lienListeTiers;


    /** page linkedin. */
    private String lienPageLinkedin;
    
    /** Liste des dirigeants à publier. */
    private List<PubDirigeantDto> dirigeants = new ArrayList<>();

    /** Liste des collaborateurs à publier. */
    private List<PubCollaborateurDto> collaborateurs = new ArrayList<>();

    /** Déclaration pour un tiers. */
    private Boolean declarationTiers;

    /** Liste des clienst à publier. */
    private List<PubAssoClientDto> clients = new ArrayList<>();

    /** Déclaration pour un tiers. */
    private Boolean declarationOrgaAppartenance;

    /** Liste des associations à publier. */
    private List<PubAssoClientDto> affiliations = new ArrayList<>();

    /** Champ des activités. */
    private PubActiviteDto activites;

    /** Liste des exercices publiés **/
    private List<PublicationBucketExerciceDto> exercices;

	/** Nom d'usage Interne de l'organisation. */
	private String nomUsageHatvp;
	private String ancienNomHatvp;
	/** Sigle de l'organisation. */
	private String sigleHatvp;
    /** Date de création. */
	
    /** La date de la 1ere publication. */
    @JsonProperty("datePremierePublication")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
    private Date premierePublicationDate;
    
    @JsonProperty("dateCreation")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
    private Date creationDate;

    /** référence au publisher. */
    @JsonIgnore
    private DeclarantDto publisher;
    /** La date de la 1ere publication. */
    @JsonProperty("dateDernierePublicationActivite")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
    private Date dateDernierePublicationActivite;
	private Boolean isActivitesPubliees;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
    private Date dateCessation;

    /** Motif de la désinscription */
    private String motifDesinscription;

    /** Commentaire Agent de la désinscription*/
    private String motivationDesinscription;

	
    @Override
    @JsonIgnore
    public Long getId() {
        return super.id;
    }

    @Override
    @JsonIgnore
    public Integer getVersion() {
        return this.version;
    }

    /** Id de l'espace orgaisation */
    @JsonIgnore
    private Long espaceOrganisationId;

    /**
     * Accesseur en lecture du champ <code>publisher</code>.
     *
     * @return le champ <code>publisher</code>.
     */
    public DeclarantDto getPublisher() {
        return this.publisher;
    }

    /**
     * Accesseur en écriture du champ <code>publisher</code>.
     *
     * @param publisher la valeur à écrire dans <code>publisher</code>.
     */
    public void setPublisher(final DeclarantDto publisher) {
        this.publisher = publisher;
    }

    /**
     * Accesseur en lecture du champ <code>emailDeContact</code>.
     *
     * @return le champ <code>emailDeContact</code>.
     */
    public String getEmailDeContact() {
        return this.emailDeContact;
    }

    /**
     * Accesseur en écriture du champ <code>emailDeContact</code>.
     *
     * @param emailDeContact la valeur à écrire dans <code>emailDeContact</code>.
     */
    public void setEmailDeContact(final String emailDeContact) {
        this.emailDeContact = emailDeContact;
    }

    /**
     * Accesseur en lecture du champ <code>telephoneDeContact</code>.
     *
     * @return le champ <code>telephoneDeContact</code>.
     */
    public String getTelephoneDeContact() {
        return this.telephoneDeContact;
    }

    /**
     * Accesseur en écriture du champ <code>telephoneDeContact</code>.
     *
     * @param telephoneDeContact la valeur à écrire dans <code>telephoneDeContact</code>.
     */
    public void setTelephoneDeContact(final String telephoneDeContact) {
        this.telephoneDeContact = telephoneDeContact;
    }

    /**
     * Accesseur en lecture du champ <code>lienSiteWeb</code>.
     *
     * @return le champ <code>lienSiteWeb</code>.
     */
    public String getLienSiteWeb() {
        return this.lienSiteWeb;
    }

    /**
     * Accesseur en écriture du champ <code>lienSiteWeb</code>.
     *
     * @param lienSiteWeb la valeur à écrire dans <code>lienSiteWeb</code>.
     */
    public void setLienSiteWeb(final String lienSiteWeb) {
        this.lienSiteWeb = lienSiteWeb;
    }

    /**
     * Accesseur en lecture du champ <code>lienPageTwitter</code>.
     *
     * @return le champ <code>lienPageTwitter</code>.
     */
    public String getLienPageTwitter() {
        return this.lienPageTwitter;
    }

    /**
     * Accesseur en écriture du champ <code>lienPageTwitter</code>.
     *
     * @param lienPageTwitter la valeur à écrire dans <code>lienPageTwitter</code>.
     */
    public void setLienPageTwitter(final String lienPageTwitter) {
        this.lienPageTwitter = lienPageTwitter;
    }

    /**
     * Accesseur en lecture du champ <code>lienPageFacebook</code>.
     *
     * @return le champ <code>lienPageFacebook</code>.
     */
    public String getLienPageFacebook() {
        return this.lienPageFacebook;
    }

    /**
     * Accesseur en écriture du champ <code>lienPageFacebook</code>.
     *
     * @param lienPageFacebook la valeur à écrire dans <code>lienPageFacebook</code>.
     */
    public void setLienPageFacebook(final String lienPageFacebook) {
        this.lienPageFacebook = lienPageFacebook;
    }
    /**
     * Accesseur en lecture du champ <code>lienListeTiers</code>.
     *
     * @return le champ <code>lienListeTiers</code>.
     */
    public String getLienListeTiers() {
        return this.lienListeTiers;
    }

    /**
     * Accesseur en écriture du champ <code>lienListeTiers</code>.
     *
     * @param lienListeTiers la valeur à écrire dans <code>lienListeTiers</code>.
     */
    public void setLienListeTiers(final String lienListeTiers) {
        this.lienListeTiers = lienListeTiers;
    }
    
    /**
     * Accesseur en lecture du champ <code>lienPageLinkedin</code>.
     *
     * @return le champ <code>lienPageLinkedin</code>.
     */
    public String getLienPageLinkedin() {
        return this.lienPageLinkedin;
    }

    /**
     * Accesseur en écriture du champ <code>lienPageLinkedin</code>.
     *
     * @param lienPageLinkedin la valeur à écrire dans <code>lienPageLinkedin</code>.
     */
    public void setLienPageLinkedin(final String lienPageLinkedin) {
        this.lienPageLinkedin = lienPageLinkedin;
    }

    /**
     * Accesseur en lecture du champ <code>adresse</code>.
     *
     * @return le champ <code>adresse</code>.
     */
    public String getAdresse() {
        return this.adresse;
    }

    /**
     * Accesseur en écriture du champ <code>adresse</code>.
     *
     * @param adresse la valeur à écrire dans <code>adresse</code>.
     */
    public void setAdresse(final String adresse) {
        this.adresse = adresse;
    }

    /**
     * Accesseur en lecture du champ <code>codePostal</code>.
     *
     * @return le champ <code>codePostal</code>.
     */
    public String getCodePostal() {
        return this.codePostal;
    }

    /**
     * Accesseur en écriture du champ <code>codePostal</code>.
     *
     * @param codePostal la valeur à écrire dans <code>codePostal</code>.
     */
    public void setCodePostal(final String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * Accesseur en lecture du champ <code>ville</code>.
     *
     * @return le champ <code>ville</code>.
     */
    public String getVille() {
        return this.ville;
    }

    /**
     * Accesseur en écriture du champ <code>ville</code>.
     *
     * @param ville la valeur à écrire dans <code>ville</code>.
     */
    public void setVille(final String ville) {
        this.ville = ville;
    }

    /**
     * Accesseur en lecture du champ <code>pays</code>.
     *
     * @return le champ <code>pays</code>.
     */
    public String getPays() {
        return this.pays;
    }

    /**
     * Accesseur en écriture du champ <code>pays</code>.
     *
     * @param pays la valeur à écrire dans <code>pays</code>.
     */
    public void setPays(final String pays) {
        this.pays = pays;
    }

    /**
     * Accesseur en lecture du champ <code>categorieOrganisation</code>.
     *
     * @return le champ <code>categorieOrganisation</code>.
     */
    public TypeOrganisationEnum getCategorieOrganisation() {
        return this.categorieOrganisation;
    }

    /**
     * Accesseur en écriture du champ <code>categorieOrganisation</code>.
     *
     * @param categorieOrganisation la valeur à écrire dans <code>categorieOrganisation</code>.
     */
    public void setCategorieOrganisation(final TypeOrganisationEnum categorieOrganisation) {
        this.categorieOrganisation = categorieOrganisation;
    }

    /**
     * Accesseur en lecture du champ <code>dirigeants</code>.
     *
     * @return le champ <code>dirigeants</code>.
     */
    public List<PubDirigeantDto> getDirigeants() {
        return this.dirigeants;
    }

    /**
     * Accesseur en écriture du champ <code>dirigeants</code>.
     *
     * @param dirigeants la valeur à écrire dans <code>dirigeants</code>.
     */
    public void setDirigeants(final List<PubDirigeantDto> dirigeants) {
        this.dirigeants = dirigeants;
    }

    /**
     * Accesseur en lecture du champ <code>collaborateurs</code>.
     *
     * @return le champ <code>collaborateurs</code>.
     */
    public List<PubCollaborateurDto> getCollaborateurs() {
        return this.collaborateurs;
    }

    /**
     * Accesseur en écriture du champ <code>collaborateurs</code>.
     *
     * @param collaborateurs la valeur à écrire dans <code>collaborateurs</code>.
     */
    public void setCollaborateurs(final List<PubCollaborateurDto> collaborateurs) {
        this.collaborateurs = collaborateurs;
    }

    /**
     * Accesseur en lecture du champ <code>clients</code>.
     *
     * @return le champ <code>clients</code>.
     */
    public List<PubAssoClientDto> getClients() {
        return this.clients;
    }

    /**
     * Accesseur en écriture du champ <code>clients</code>.
     *
     * @param clients la valeur à écrire dans <code>clients</code>.
     */
    public void setClients(final List<PubAssoClientDto> clients) {
        this.clients = clients;
    }

    /**
     * Accesseur en lecture du champ <code>associations</code>.
     *
     * @return le champ <code>associations</code>.
     */
    public List<PubAssoClientDto> getAffiliations() {
        return this.affiliations;
    }

    /**
     * Accesseur en écriture du champ <code>associations</code>.
     *
     * @param associations la valeur à écrire dans <code>associations</code>.
     */
    public void setAffiliations(final List<PubAssoClientDto> associations) {
        this.affiliations = associations;
    }

    /**
     * Accesseur en lecture du champ <code>espaceOrganisationId</code>.
     *
     * @return le champ <code>espaceOrganisationId</code>.
     */
    public Long getEspaceOrganisationId() {
        return this.espaceOrganisationId;
    }

    /**
     * Accesseur en écriture du champ <code>espaceOrganisationId</code>.
     *
     * @param espaceOrganisationId la valeur à écrire dans <code>espaceOrganisationId</code>.
     */
    public void setEspaceOrganisationId(final Long espaceOrganisationId) {
        this.espaceOrganisationId = espaceOrganisationId;
    }

    /**
     * Accesseur en lecture du champ <code>nationalId</code>.
     *
     * @return le champ <code>nationalId</code>.
     */
    public String getNationalId() {
        return this.nationalId;
    }

    /**
     * Accesseur en écriture du champ <code>nationalId</code>.
     *
     * @param nationalId la valeur à écrire dans <code>nationalId</code>.
     */
    public void setNationalId(final String nationalId) {
        this.nationalId = nationalId;
    }

    /**
     * Accesseur en lecture du champ <code>typeIdentifiantNational</code>.
     *
     * @return le champ <code>typeIdentifiantNational</code>.
     */
    public TypeIdentifiantNationalEnum getTypeIdentifiantNational() {
        return this.typeIdentifiantNational;
    }

    /**
     * Accesseur en écriture du champ <code>typeIdentifiantNational</code>.
     *
     * @param typeIdentifiantNational la valeur à écrire dans <code>typeIdentifiantNational</code>.
     */
    public void setTypeIdentifiantNational(
        final TypeIdentifiantNationalEnum typeIdentifiantNational) {
        this.typeIdentifiantNational = typeIdentifiantNational;
    }

    /**
     * Accesseur en lecture du champ <code>denomination</code>.
     *
     * @return le champ <code>denomination</code>.
     */
    public String getDenomination() {
        return this.denomination;
    }

    /**
     * Accesseur en écriture du champ <code>denomination</code>.
     *
     * @param denomination la valeur à écrire dans <code>denomination</code>.
     */
    public void setDenomination(final String denomination) {
        this.denomination = denomination;
    }

 

    /**
     * Accesseur en lecture du champ <code>declarationTiers</code>.
     *
     * @return le champ <code>declarationTiers</code>.
     */
    public Boolean isDeclarationTiers() {
        return this.declarationTiers;
    }

    /**
     * Accesseur en écriture du champ <code>declarationTiers</code>.
     *
     * @param declarationTiers la valeur à écrire dans <code>declarationTiers</code>.
     */
    public void setDeclarationTiers(final Boolean declarationTiers) {
        this.declarationTiers = declarationTiers;
    }
    /**
     * Accesseur en lecture du champ <code>creationDate</code>.
     *
     * @return le champ <code>creationDate</code>.
     */
    public Date getPremierePublicationDate() {
        return this.premierePublicationDate;
    }

    /**
     * Accesseur en écriture du champ <code>creationDate</code>.
     *
     * @param creationDate la valeur à écrire dans <code>creationDate</code>.
     */
    public void setPremierePublicationDate(final Date premierePublicationDate) {
        this.premierePublicationDate = premierePublicationDate;
    }
    /**
     * Accesseur en lecture du champ <code>creationDate</code>.
     *
     * @return le champ <code>creationDate</code>.
     */
    public Date getCreationDate() {
        return this.creationDate;
    }

    /**
     * Accesseur en écriture du champ <code>creationDate</code>.
     *
     * @param creationDate la valeur à écrire dans <code>creationDate</code>.
     */
    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Accesseur en lecture du champ <code>publierMonAdressePhysique</code>.
     *
     * @return le champ <code>publierMonAdressePhysique</code>.
     */
    public Boolean getPublierMonAdressePhysique() {
        return this.publierMonAdressePhysique;
    }

    /**
     * Accesseur en écriture du champ <code>publierMonAdressePhysique</code>.
     *
     * @param publierMonAdressePhysique la valeur à écrire dans <code>publierMonAdressePhysique</code>.
     */
    public void setPublierMonAdressePhysique(final Boolean publierMonAdressePhysique) {
        this.publierMonAdressePhysique = publierMonAdressePhysique;
    }

    /**
     * Accesseur en lecture du champ <code>publierMonTelephoneDeContact</code>.
     *
     * @return le champ <code>publierMonTelephoneDeContact</code>.
     */
    public Boolean getPublierMonTelephoneDeContact() {
        return this.publierMonTelephoneDeContact;
    }

    /**
     * Accesseur en écriture du champ <code>publierMonTelephoneDeContact</code>.
     *
     * @param publierMonTelephoneDeContact la valeur à écrire dans <code>publierMonTelephoneDeContact</code>.
     */
    public void setPublierMonTelephoneDeContact(final Boolean publierMonTelephoneDeContact) {
        this.publierMonTelephoneDeContact = publierMonTelephoneDeContact;
    }

    /**
     * Accesseur en lecture du champ <code>publierMonAdresseEmail</code>.
     *
     * @return le champ <code>publierMonAdresseEmail</code>.
     */
    public Boolean getPublierMonAdresseEmail() {
        return this.publierMonAdresseEmail;
    }

    /**
     * Accesseur en écriture du champ <code>publierMonAdresseEmail</code>.
     *
     * @param publierMonAdresseEmail la valeur à écrire dans <code>publierMonAdresseEmail</code>.
     */
    public void setPublierMonAdresseEmail(final Boolean publierMonAdresseEmail) {
        this.publierMonAdresseEmail = publierMonAdresseEmail;
    }

    /**
     * Accesseur en lecture du champ <code>declarationOrgaAppartenance</code>.
     *
     * @return le champ <code>declarationOrgaAppartenance</code>.
     */
    public Boolean isDeclarationOrgaAppartenance() {
        return this.declarationOrgaAppartenance;
    }

    /**
     * Accesseur en écriture du champ <code>declarationOrgaAppartenance</code>.
     *
     * @param declarationOrgaAppartenance la valeur à écrire dans <code>declarationOrgaAppartenance</code>.
     */
    public void setDeclarationOrgaAppartenance(final Boolean declarationOrgaAppartenance) {
        this.declarationOrgaAppartenance = declarationOrgaAppartenance;
    }

    public Boolean getDeclarationTiers()
    {
        return declarationTiers;
    }
    public Boolean getDeclarationOrgaAppartenance()
    {
        return declarationOrgaAppartenance;
    }
    public List<PublicationBucketExerciceDto> getExercices()
    {
        return exercices;
    }
    public void setExercices(List<PublicationBucketExerciceDto> exercices)
    {
        this.exercices = exercices;
    }

    public PubActiviteDto getActivites()
    {
        return activites;
    }
    public void setActivites(PubActiviteDto activites)
    {
        this.activites = activites;
    }
    /**
	 * @return the nomUsage
	 */
	public String getNomUsage() {
		return nomUsage;
	}

	/**
	 * @param nomUsage the nomUsage to set
	 */
	public void setNomUsage(String nomUsage) {
		this.nomUsage = nomUsage;
	}
	/**
	 * Accesseur en lecture du champ <code>nomUsageHatvp</code>.
	 *
	 * @return le champ <code>nomUsageHatvp</code>.
	 */
	public String getNomUsageHatvp() {
		return this.nomUsageHatvp;
	}

	/**
	 * Accesseur en écriture du champ <code>nomUsageHatvp</code>.
	 *
	 * @param nomUsageHatvp
	 *            la valeur à écrire dans <code>nomUsageHatvp</code>.
	 */
	public void setNomUsageHatvp(final String nomUsageHatvp) {
		this.nomUsageHatvp = nomUsageHatvp;
	}
	
	/**
	 * Accesseur en lecture du champ <code>sigleHatvp</code>.
	 *
	 * @return le champ <code>sigleHatvp</code>.
	 */
	public String getSigleHatvp() {
		return this.sigleHatvp;
	}

	/**
	 * Accesseur en écriture du champ <code>sigleHatvp</code>.
	 *
	 * @param sigleHatvp
	 *            la valeur à écrire dans <code>sigleHatvp</code>.
	 */
	public void setSigleHatvp(final String sigleHatvp) {
		this.sigleHatvp = sigleHatvp;
	}
	
	public String getAncienNomHatvp() {
		return this.ancienNomHatvp;
	}
	public void setAncienNomHatvp(final String ancienNomHatvp) {
		this.ancienNomHatvp = ancienNomHatvp;
	}
	public void setIsActivitesPubliees(Boolean b) {
		this.isActivitesPubliees= b;
		
	}

	public Boolean getIsActivitesPubliees() {
		return isActivitesPubliees;
	}
    public Date getDateDernierePublicationActivite() {
		return dateDernierePublicationActivite;
	}

	public void setDateDernierePublicationActivite(Date dateDernierePublicationActivite) {
		this.dateDernierePublicationActivite = dateDernierePublicationActivite;
	}

	public Date getDateCessation() {
		return dateCessation;
	}

	public void setDateCessation(Date dateCessation) {
		this.dateCessation = dateCessation;
	}


    public String getMotifDesinscription() {
        return motifDesinscription;
    }

    public void setMotifDesinscription(String motifDesinscription) {
        this.motifDesinscription = motifDesinscription;
    }

    public String getMotivationDesinscription() {
		return motivationDesinscription;
	}

	public void setMotivationDesinscription(String motivationDesinscription) {
		this.motivationDesinscription = motivationDesinscription;
	}

}
