/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;

public class ActiviteRepresentationInteretDto extends AbstractCommonDto {

	@NotNull
	@Size(min = 1)
	private String objet;

	private StatutPublicationEnum statut;

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private String idFiche;

	@NotNull
	@Size(min = 1, max = 5)
	private Set<Long> domainesIntervention;

	@Valid
	@NotNull
	@Size(min = 1, max = 5)
	private List<ActionRepresentationInteretDto> actionsRepresentationInteret = new ArrayList<>();

	@NotNull
	private Long exerciceComptable;

	private Boolean dataToPublish;

	/**
	 * @return the objet
	 */
	public String getObjet() {
		return objet;
	}

	/**
	 * @param objet
	 *            the objet to set
	 */
	public void setObjet(String objet) {
		this.objet = objet;
	}

	/**
	 * @return the domainesIntervention
	 */
	public Set<Long> getDomainesIntervention() {
		return domainesIntervention;
	}

	/**
	 * @param domainesIntervention
	 *            the domainesIntervention to set
	 */
	public void setDomainesIntervention(Set<Long> domainesIntervention) {
		this.domainesIntervention = domainesIntervention;
	}

	/**
	 * @return the actionsRepresentationInteret
	 */
	public List<ActionRepresentationInteretDto> getActionsRepresentationInteret() {
		return actionsRepresentationInteret;
	}

	/**
	 * @param actionsRepresentationInteret
	 *            the actionsRepresentationInteret to set
	 */
	public void setActionsRepresentationInteret(List<ActionRepresentationInteretDto> actionsRepresentationInteret) {
		this.actionsRepresentationInteret = actionsRepresentationInteret;
	}

	/**
	 * @return the exerciceComptable
	 */
	public Long getExerciceComptable() {
		return exerciceComptable;
	}

	/**
	 * @param exerciceComptable
	 *            the exerciceComptable to set
	 */
	public void setExerciceComptable(Long exerciceComptable) {
		this.exerciceComptable = exerciceComptable;
	}

	/**
	 *
	 * @return statut
	 */
	public StatutPublicationEnum getStatut() {
		return statut;
	}

	/**
	 *
	 * @param statut
	 */
	public void setStatut(StatutPublicationEnum statut) {
		this.statut = statut;
	}

	/**
	 * @return the idFiche
	 */
	public String getIdFiche() {
		return idFiche;
	}

	/**
	 * @param idFiche
	 *            the idFiche to set
	 */
	public void setIdFiche(String idFiche) {
		this.idFiche = idFiche;
	}

	/**
	 * @return the dataToPublish
	 */
	public Boolean getDataToPublish() {
		return dataToPublish;
	}

	/**
	 * @param dataToPublish
	 *            the dataToPublish to set
	 */
	public void setDataToPublish(Boolean dataToPublish) {
		this.dataToPublish = dataToPublish;
	}

}
