/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JsonNode;

import fr.hatvp.registre.commons.dto.EnumAsDto;

/**
 *
 * Classe d'énumération pour affiche du statut d'un espace coté front
 *
 * @version $Revision$ $Date${0xD}
 */
public enum StatutEspaceAffichageEnum {
	
//	Publication d’identité 
	ACCEPTEE_NON_PUBLIEE("Espace validé, non publié"),
	ACCEPTEE_PUBLIEE("Espace publié le"),
//	Publication des mises à jour d’identité 
	MAJ_NON_PUBLIEE("Mises à jour non publiées"),
	MAJ_PUBLIEE("Publié le");


	private final String label;

	StatutEspaceAffichageEnum(final String label) {
		this.label = label;
	}

	public String getValue() {
		return this.label;
	}

	@JsonCreator
	public static StatutEspaceAffichageEnum fromNode(final JsonNode node) {
		if (!node.has("code")) {
			return null;
		}
		final String name = node.get("code").asText();
		return StatutEspaceAffichageEnum.valueOf(name);
	}

	@JsonValue
	public EnumAsDto getStatutEspaceEnumDto() {
		return new EnumAsDto(this.name(), this.label);
	}

}
