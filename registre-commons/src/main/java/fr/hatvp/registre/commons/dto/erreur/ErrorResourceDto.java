/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.erreur;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResourceDto {
	private String code;
	private String message;
	private List<FieldErrorResourceDto> fieldErrors;

	public ErrorResourceDto() {
	}

	public ErrorResourceDto(final String code, final String message) {
		this.code = code;
		this.message = message;
	}

	public ErrorResourceDto(String code, String message, List<FieldErrorResourceDto> fieldErrors) {
		this.code = code;
		this.message = message;
		this.fieldErrors = fieldErrors;
	}

	/**
	 * Accesseur en lecture du champ <code>code</code>.
	 *
	 * @return le champ <code>code</code>.
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * Accesseur en écriture du champ <code>code</code>.
	 *
	 * @param code
	 *            la valeur à écrire dans <code>code</code>.
	 */
	public void setCode(final String code) {
		this.code = code;
	}

	/**
	 * Accesseur en lecture du champ <code>message</code>.
	 *
	 * @return le champ <code>message</code>.
	 */
	public String getMessage() {
		return this.message;
	}

	/**
	 * Accesseur en écriture du champ <code>message</code>.
	 *
	 * @param message
	 *            la valeur à écrire dans <code>message</code>.
	 */
	public void setMessage(final String message) {
		this.message = message;
	}

	/**
	 * Accesseur en lecture du champ <code>fieldErrors</code>.
	 *
	 * @return le champ <code>fieldErrors</code>.
	 */
	public List<FieldErrorResourceDto> getFieldErrors() {
		return this.fieldErrors;
	}

	/**
	 * Accesseur en écriture du champ <code>fieldErrors</code>.
	 *
	 * @param fieldErrors
	 *            la valeur à écrire dans <code>fieldErrors</code>.
	 */
	public void setFieldErrors(final List<FieldErrorResourceDto> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

}