/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication.activite;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

/**
 * DTO de publication de l'entité ActionRepresentationInteret
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PublicationActionDto
    extends AbstractCommonDto {

	@NotNull
	@Size(min = 1)
	private List<String> reponsablesPublics;

	@NotNull
	@Size(min = 1)
	private List<String> decisionsConcernees;

	@NotNull
	@Size(min = 1)
	private List<String> actionsMenees;

	private String actionMeneeAutre;
	private String responsablePublicAutre;

	@Valid
	@NotNull
	@Size(min = 1)
	private List<String> tiers;

	@Size(max = 700)
	private String observation;

    public List<String> getReponsablesPublics()
    {
        return reponsablesPublics;
    }
    public void setReponsablesPublics(List<String> reponsablesPublics)
    {
        this.reponsablesPublics = reponsablesPublics;
    }
    public List<String> getDecisionsConcernees()
    {
        return decisionsConcernees;
    }
    public void setDecisionsConcernees(List<String> decisionsConcernees)
    {
        this.decisionsConcernees = decisionsConcernees;
    }
    public List<String> getActionsMenees()
    {
        return actionsMenees;
    }
    public void setActionsMenees(List<String> actionsMenees)
    {
        this.actionsMenees = actionsMenees;
    }
    public String getActionMeneeAutre()
    {
        return actionMeneeAutre;
    }
    public void setActionMeneeAutre(String actionMeneeAutre)
    {
        this.actionMeneeAutre = actionMeneeAutre;
    }
    public String getResponsablePublicAutre()
    {
        return responsablePublicAutre;
    }
    public void setResponsablePublicAutre(String responsablePublicAutre)
    {
        this.responsablePublicAutre = responsablePublicAutre;
    }
    public List<String> getTiers()
    {
        return tiers;
    }
    public void setTiers(List<String> tiers)
    {
        this.tiers = tiers;
    }
    public String getObservation()
    {
        return observation;
    }
    public void setObservation(String observation)
    {
        this.observation = observation;
    }
}
