/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Class permettant de faire différents types de validation.
 *
 * @version $Revision$ $Date${0xD}
 */
public class ValidatePatternUtils
{

    /**
     * Pattern utilisé pour la génération automatique des mots de passe.
     */
    private static final String PASSWORD_GENERATION_PATTERN = "[A-Z]{2}[a-z]{3}\\d{3}";

    /**
     * Patttern utilisé pour la vérification du mot de passe.
     */
    private static final String PASSWORD_VALIDATION_PATTERN = "^(?=.*?[a-z])(?=.*?[0-9]).{10,}$";

    /**
     * Email pattern.
     */
    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&’*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

    /**
     * Valider le pattern du mot de passe.
     *
     * @param password mot de passe en entrée.
     * @return un boolean de validation.
     */
    public static boolean validatePassword(final String password)
    {
        return !StringUtils.isEmpty(password) && password.matches(PASSWORD_VALIDATION_PATTERN);

    }

    /**
     * Valider le pattern de l'email.
     *
     * @param email email en entrée.
     * @return un boolean de validation.
     */
    public static boolean validateEmail(final String email)
    {
        return !StringUtils.isEmpty(email) && email.matches(EMAIL_PATTERN);

    }

    /**
     * Accesseur en lecture du champ <code>passwordPattern</code>.
     * @return le champ <code>passwordPattern</code>.
     */
    public static String getPasswordValidationPattern()
    {
        return PASSWORD_VALIDATION_PATTERN;
    }

    /**
     * Accesseur en lecture du champ <code>emailPattern</code>.
     * @return le champ <code>emailPattern</code>.
     */
    public static String getEmailPattern()
    {
        return EMAIL_PATTERN;
    }

    /**
     * Accesseur en lecture du champ <code>passwordGenerationPattern</code>.
     * @return le champ <code>passwordGenerationPattern</code>.
     */
    public static String getPasswordGenerationPattern()
    {
        return PASSWORD_GENERATION_PATTERN;
    }

}
