/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;

/**
 * DTO de l'entité ClientEntity utilisée pour la publication.
 * Permet de nommer / ignorer les champs pour le Json de la publication. 
 * 
 * @version $Revision$ $Date${0xD}
 */
public class PubAssoClientDto
		extends AssoClientDto {
	
    @JsonIgnore
    private Long id;
    
    @JsonIgnore
    private Integer version;
    
    /** On surcharge pour nommer différement la propriété dans le document JSON produit. */
    @JsonProperty("identifiantNational")
    private String nationalId;
    
    /** On surcharge pour nommer différement la propriété dans le document JSON produit. */
    @JsonProperty("typeIdentifiantNational")
    private TypeIdentifiantNationalEnum originNationalId;
    
    @JsonIgnore
    private Long espaceOrganisationId;
    
    @JsonIgnore
    private Long organisationId;
}
