/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

public class ActiviteRepresentationInteretJsonDto extends AbstractCommonDto {

	private String objet;

	private String identifiantFiche;

	private Set<String> domainesIntervention;


	private List<ActionRepresentationInteretJsonDto> actionsRepresentationInteret = new ArrayList<>();

	
	/**
	 * @return the objet
	 */
	public String getObjet() {
		return objet;
	}

	/**
	 * @param objet
	 *            the objet to set
	 */
	public void setObjet(String objet) {
		this.objet = objet;
	}

	/**
	 * @return the domainesIntervention
	 */
	public Set<String> getDomainesIntervention() {
		return domainesIntervention;
	}

	/**
	 * @param domainesIntervention
	 *            the domainesIntervention to set
	 */
	public void setDomainesIntervention(Set<String> domainesIntervention) {
		this.domainesIntervention = domainesIntervention;
	}

	/**
	 * @return the actionsRepresentationInteret
	 */
	public List<ActionRepresentationInteretJsonDto> getActionsRepresentationInteret() {
		return actionsRepresentationInteret;
	}

	/**
	 * @param actionsRepresentationInteret
	 *            the actionsRepresentationInteret to set
	 */
	public void setActionsRepresentationInteret(List<ActionRepresentationInteretJsonDto> actionsRepresentationInteret) {
		this.actionsRepresentationInteret = actionsRepresentationInteret;
	}

	/**
	 * @return the idFiche
	 */
	public String getIdentifiantFiche() {
		return identifiantFiche;
	}

	/**
	 * @param idFiche
	 *            the idFiche to set
	 */
	public void setIdentifiantFiche(String identifiantFiche) {
		this.identifiantFiche = identifiantFiche;
	}	

}
