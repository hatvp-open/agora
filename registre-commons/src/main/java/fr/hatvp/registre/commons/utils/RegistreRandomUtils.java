/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.utils;

import org.springframework.stereotype.Component;

import java.util.Random;
/**
 * Classe regroupant les fonctions génératrices d'éléments aléatoires
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class RegistreRandomUtils
{
    /**
     * Création d'un nombre aléatoire avec un nombre de chiffre précis.
     *
     * @param le nombre de chiffre dans le nombre généré.
     * @return un nombre aléatoire de la taille donnée.
     */
    public Integer generateRandomNumber(Integer length)
    {
        final Random random = new Random();
        final Integer max = 9 * (int) Math.pow(10, length - 1);
        final Integer min = (int) Math.pow(10, length - 1);
        return random.nextInt(max) + min;
    }
}
