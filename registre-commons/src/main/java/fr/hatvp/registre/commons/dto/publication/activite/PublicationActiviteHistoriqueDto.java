/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication.activite;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
/**
 * 
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PublicationActiviteHistoriqueDto extends AbstractCommonDto
{
//    private String identifiantFiche;
	@JsonProperty("dateCreation")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
    private Date creationDate;
	
	private Long publicationActiviteId;


    @NotNull
    @Size(min = 1)
    private String objet;

//    @NotNull
//    @Size(min = 1, max = 5)
//    private List<String> domainesIntervention;

//    @Valid
//    @NotNull
//    @Size(min = 1, max = 5)
//    private List<PublicationActionDto> actionsRepresentationInteret = new ArrayList<>();

//    public String getIdentifiantFiche()
//    {
//        return identifiantFiche;
//    }
//    public void setIdentifiantFiche(String identifiantFiche)
//    {
//        this.identifiantFiche = identifiantFiche;
//    }

    public String getObjet()
    {
        return objet;
    }
    public Long getPublicationActiviteId() {
		return publicationActiviteId;
	}
	public void setPublicationActiviteId(Long publicationActiviteId) {
		this.publicationActiviteId = publicationActiviteId;
	}
	public void setObjet(String objet)
    {
        this.objet = objet;
    }
//    public List<String> getDomainesIntervention()
//    {
//        return domainesIntervention;
//    }
//    public void setDomainesIntervention(List<String> domainesIntervention)
//    {
//        this.domainesIntervention = domainesIntervention;
//    }
//    public List<PublicationActionDto> getActionsRepresentationInteret()
//    {
//        return actionsRepresentationInteret;
//    }
//    public void setActionsRepresentationInteret(
//        List<PublicationActionDto> actionsRepresentationInteret)
//    {
//        this.actionsRepresentationInteret = actionsRepresentationInteret;
//    }
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
    
}
