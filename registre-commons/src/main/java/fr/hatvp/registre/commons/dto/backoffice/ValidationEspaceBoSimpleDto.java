/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;

public class ValidationEspaceBoSimpleDto extends AbstractCommonDto {

	/** Id de l'inscription. */
	private Long validationInscriptionId;

	/** Denomination de l'organisation */
	private String denomination;

	/** Nom complet créateur espace organisation */
	private String createurNomComplet;

	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
	private Date creationDate;

	/** Statut Validation Espace. */
	private StatutEspaceEnum statut;

	/** Objet Verrou sur l'objet */
	private EspaceOrganisationLockBoDto lock;

	/** Espace Commenté */
	private Boolean commente;

	/**
	 * @return the validationInscriptionId
	 */
	public Long getValidationInscriptionId() {
		return validationInscriptionId;
	}

	/**
	 * @param validationInscriptionId
	 *            the validationInscriptionId to set
	 */
	public void setValidationInscriptionId(Long validationInscriptionId) {
		this.validationInscriptionId = validationInscriptionId;
	}

	/**
	 * @return the denomination
	 */
	public String getDenomination() {
		return denomination;
	}

	/**
	 * @param denomination
	 *            the denomination to set
	 */
	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	/**
	 * @return the createurNomComplet
	 */
	public String getCreateurNomComplet() {
		return createurNomComplet;
	}

	/**
	 * @param createurNomComplet
	 *            the createurNomComplet to set
	 */
	public void setCreateurNomComplet(String createurNomComplet) {
		this.createurNomComplet = createurNomComplet;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the statut
	 */
	public StatutEspaceEnum getStatut() {
		return statut;
	}

	/**
	 * @param statut
	 *            the statut to set
	 */
	public void setStatut(StatutEspaceEnum statut) {
		this.statut = statut;
	}

	/**
	 * @return the lock
	 */
	public EspaceOrganisationLockBoDto getLock() {
		return lock;
	}

	/**
	 * @param lock
	 *            the lock to set
	 */
	public void setLock(EspaceOrganisationLockBoDto lock) {
		this.lock = lock;
	}

	/**
	 * @return the commente
	 */
	public Boolean getCommente() {
		return commente;
	}

	/**
	 * @param commente
	 *            the commente to set
	 */
	public void setCommente(Boolean commente) {
		this.commente = commente;
	}

}
