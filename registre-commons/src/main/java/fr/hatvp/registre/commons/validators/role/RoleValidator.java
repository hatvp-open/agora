/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.validators.role;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import fr.hatvp.registre.commons.lists.backoffice.RoleEnumBackOffice;

/**
 * 
 * Classe de validation des roles backoffice.
 * 
 * @version $Revision$ $Date${0xD}
 */
public class RoleValidator
    implements ConstraintValidator<BackOfficeRole, List<RoleEnumBackOffice>>
{

    /** Liste des roles authorisés. */
    private RoleEnumBackOffice[] authorizedRoles;

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(final BackOfficeRole constraintAnnotation)
    {
        this.authorizedRoles = constraintAnnotation.value();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid(final List<RoleEnumBackOffice> value,
            final ConstraintValidatorContext context)
    {

        if (value == null) {
            return true;
        }

        boolean correctRole = false;
        for (final RoleEnumBackOffice enumValue : value) {

            for (final RoleEnumBackOffice roleEnum : this.authorizedRoles) {
                if (roleEnum.equals(enumValue)) {
                    correctRole = true;
                }
            }

            if (!correctRole) {
                return false;
            }
        }

        return true;
    }
}
