/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import fr.hatvp.registre.commons.dto.activite.ExerciceComptableBlacklistedDto;
import fr.hatvp.registre.commons.dto.backoffice.EspaceCollaboratifDto;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;

import java.util.Date;
import java.util.List;

/**
 * DTO de l'entité EspaceOrganisationEntity.
 *
 * @version $Revision$ $Date${0xD}
 */

public class EspaceOrganisationBlacklistedDto extends EspaceCollaboratifDto{


	/** Type identifiant national (SIREN/RNA). */
	private TypeIdentifiantNationalEnum typeIdentifiantNational;
	

	
    /** Date de validation de l'espace. */
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
    private Date inscriptionEffectiveDate;

    private List<ExerciceComptableBlacklistedDto> listeExercices;
    


    public Date getInscriptionEffectiveDate() {
        return inscriptionEffectiveDate;
    }

    public void setInscriptionEffectiveDate(Date inscriptionEffectiveDate) {
        this.inscriptionEffectiveDate = inscriptionEffectiveDate;
    }





	public List<ExerciceComptableBlacklistedDto> getListeExercices() {
		return listeExercices;
	}

	public void setListeExercices(List<ExerciceComptableBlacklistedDto> listeExercices) {
		this.listeExercices = listeExercices;
	}


	/**
	 * Accesseur en lecture du champ <code>typeIdentifiantNational</code>.
	 *
	 * @return le champ <code>typeIdentifiantNational</code>.
	 */
	public TypeIdentifiantNationalEnum getTypeIdentifiantNational() {
		return this.typeIdentifiantNational;
	}

	/**
	 * Accesseur en écriture du champ <code>typeIdentifiantNational</code>.
	 *
	 * @param typeIdentifiantNational
	 *            la valeur à écrire dans <code>typeIdentifiantNational</code>.
	 */
	public void setTypeIdentifiantNational(final TypeIdentifiantNationalEnum typeIdentifiantNational) {
		this.typeIdentifiantNational = typeIdentifiantNational;
	}
    

    
}
