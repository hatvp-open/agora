/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists.backoffice;


public enum StatutDemandeOrganisationEnum {

	A_TRAITER, TRAITEE_ORGANISATION_TROUVEE, TRAITEE_ORGANISATION_CREEE, TRAITEE_ORGANISATION_REFUSEE,COMPLEMENT_DEMANDE_ENVOYEE, TOUTES_LES_DEMANDES

}
