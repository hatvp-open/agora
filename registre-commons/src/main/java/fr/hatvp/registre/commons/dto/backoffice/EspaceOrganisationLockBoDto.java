/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import java.util.Date;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

/**
 * Dto de l'entité EspaceOrganisationLockBoEntity.
 *
 * @version $Revision$ $Date${0xD}
 */
public class EspaceOrganisationLockBoDto
    extends AbstractCommonDto
{

    /**
     * Id Espace Organisation.
     */
    private Long espaceOrganisationId;

    /**
     * Id Utilisateur BO.
     */
    private Long utilisateurBoId;

    /**
     * Nom complet Utilisateur BO.
     */
    private String utilisateurBoName;

    /**
     * Date lock.
     */
    private Date lockTimeStart;

    /**
     * Temps d'attente restant (s).
     */
    private Long lockTimeRemain;

    /**
     * Gets espace organisation id.
     *
     * @return the espace organisation id
     */
    public Long getEspaceOrganisationId()
    {
        return this.espaceOrganisationId;
    }

    /**
     * Sets espace organisation id.
     *
     * @param espaceOrganisationId the espace organisation id
     */
    public void setEspaceOrganisationId(final Long espaceOrganisationId)
    {
        this.espaceOrganisationId = espaceOrganisationId;
    }

    /**
     * Gets utilisateur bo id.
     *
     * @return the utilisateur bo id
     */
    public Long getUtilisateurBoId()
    {
        return this.utilisateurBoId;
    }

    /**
     * Sets utilisateur bo id.
     *
     * @param utilisateurBoId the utilisateur bo id
     */
    public void setUtilisateurBoId(final Long utilisateurBoId)
    {
        this.utilisateurBoId = utilisateurBoId;
    }

    /**
     * Gets utilisateur bo name.
     *
     * @return the utilisateur bo name
     */
    public String getUtilisateurBoName()
    {
        return this.utilisateurBoName;
    }

    /**
     * Sets utilisateur bo name.
     *
     * @param utilisateurBoName the utilisateur bo name
     */
    public void setUtilisateurBoName(final String utilisateurBoName)
    {
        this.utilisateurBoName = utilisateurBoName;
    }

    /**
     * Gets lock time start.
     *
     * @return the lock time start
     */
    public Date getLockTimeStart()
    {
        return this.lockTimeStart;
    }

    /**
     * Sets lock time start.
     *
     * @param lockTimeStart the lock time start
     */
    public void setLockTimeStart(final Date lockTimeStart)
    {
        this.lockTimeStart = lockTimeStart;
    }

    /**
     * Gets lock time remain.
     *
     * @return the lock time remain
     */
    public Long getLockTimeRemain()
    {
        return this.lockTimeRemain;
    }

    /**
     * Sets lock time remain.
     *
     * @param lockTimeRemain the lock time remain
     */
    public void setLockTimeRemain(final Long lockTimeRemain)
    {
        this.lockTimeRemain = lockTimeRemain;
    }
}
