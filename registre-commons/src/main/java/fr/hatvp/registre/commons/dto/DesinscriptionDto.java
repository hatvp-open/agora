/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.hatvp.registre.commons.lists.OrigineSaisieEnum;
import fr.hatvp.registre.commons.utils.LocalDateDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateSerializer;

import java.time.LocalDate;
import java.util.List;

public class DesinscriptionDto extends AbstractCommonDto{


    /**
     * Espace Organisation ayant demandé la desinscription
     */
    private EspaceOrganisationDto espaceOrganisation;

    /** Date de cessation. */
    @JsonFormat( pattern = "ddMMyyyy", timezone = "Europe/Paris")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate cessationDate;

    /**  motif de la désinscription. */
    private String motif;

    /**  observation déclarant de la désinscription. */
    private String observationDeclarant;

    /**  observation de la désinscription. */
    private String observation;

    /** Piece de la désinscription */
    private List<DesinscriptionPieceDto> pieces;
    
    private OrigineSaisieEnum origineSaisie = null;


    public LocalDate getCessationDate() {
        return cessationDate;
    }

    public void setCessationDate(LocalDate cessationDate) {
        this.cessationDate = cessationDate;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getObservationDeclarant() {
        return observationDeclarant;
    }

    public void setObservationDeclarant(String observationDeclarant) {
        this.observationDeclarant = observationDeclarant;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public EspaceOrganisationDto getEspaceOrganisation() {
        return espaceOrganisation;
    }

    public void setEspaceOrganisation(EspaceOrganisationDto espaceOrganisation) {
        this.espaceOrganisation = espaceOrganisation;
    }

    public List<DesinscriptionPieceDto> getPieces() {
        return pieces;
    }

    public void setPieces(List<DesinscriptionPieceDto> pieces) {
        this.pieces = pieces;
    }

	public OrigineSaisieEnum getOrigineSaisie() {
		return origineSaisie;
	}

	public void setOrigineSaisie(OrigineSaisieEnum origineSaisie) {
		this.origineSaisie = origineSaisie;
	}
    
}
