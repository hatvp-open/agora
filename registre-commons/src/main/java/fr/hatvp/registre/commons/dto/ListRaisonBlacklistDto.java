/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import fr.hatvp.registre.commons.lists.PublicationRelanceTypeEnum;

import java.util.ArrayList;
import java.util.List;

public class ListRaisonBlacklistDto {
    private List<PublicationRelanceTypeEnum> raisonBlacklistEnumList = new ArrayList<>();

    public List<PublicationRelanceTypeEnum> getRaisonBlacklistEnumList() {
        return raisonBlacklistEnumList;
    }

    public void setRaisonBlacklistEnumList(List<PublicationRelanceTypeEnum> raisonBlacklistEnumList) {
        this.raisonBlacklistEnumList = raisonBlacklistEnumList;
    }

    {
        for (final PublicationRelanceTypeEnum raisonBlacklistEnum : PublicationRelanceTypeEnum.values()) {
            this.raisonBlacklistEnumList.add(raisonBlacklistEnum);
        }
    }
}
