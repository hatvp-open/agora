

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import javax.validation.constraints.Size;

import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.validators.civility.Civility;

/**
 * DTO de l'entité DirigeantEntity.
 *
 * @version $Revision$ $Date${0xD}
 */
public class DirigeantDto
    extends AbstractCommonDto
{

    /** Civilité. */
    @Civility(value = {CiviliteEnum.MME, CiviliteEnum.M})
    private CiviliteEnum civilite;

    /** Nom dirigeant */
    @Size(max = 150)
    private String nom;

    /** Prénom Dirigeant */
    @Size(max = 150)
    private String prenom;

    /** Fonctions exercées. */
    @Size(max = 150)
    private String fonction;

    /** Id espace organisation */
    private Long espaceOrganisationId;
    
    private Integer sortOrder;

    /**
     * Gets nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets nom.
     *
     * @param nom the nom
     */
    public void setNom(final String nom)
    {
        this.nom = nom;
    }

    /**
     * Gets prenom.
     *
     * @return the prenom
     */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * Sets prenom.
     *
     * @param prenom the prenom
     */
    public void setPrenom(final String prenom)
    {
        this.prenom = prenom;
    }

    /**
     * Gets espace organisation id.
     *
     * @return the espace organisation id
     */
    public Long getEspaceOrganisationId()
    {
        return this.espaceOrganisationId;
    }

    /**
     * Sets espace organisation id.
     *
     * @param espaceOrganisationId the espace organisation id
     */
    public void setEspaceOrganisationId(final Long espaceOrganisationId)
    {
        this.espaceOrganisationId = espaceOrganisationId;
    }

    /**
     * Gets civilite.
     *
     * @return the civilite
     */
    public CiviliteEnum getCivilite()
    {
        return this.civilite;
    }

    /**
     * Sets civilite.
     *
     * @param civilite the civilite
     */
    public void setCivilite(final CiviliteEnum civilite)
    {
        this.civilite = civilite;
    }

    /**
     * Gets fonction.
     *
     * @return the fonction
     */
    public String getFonction()
    {
        return this.fonction;
    }

    /**
     * Sets fonction.
     *
     * @param fonction the fonction
     */
    public void setFonction(final String fonction)
    {
        this.fonction = fonction;
    }

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}   
    
}
