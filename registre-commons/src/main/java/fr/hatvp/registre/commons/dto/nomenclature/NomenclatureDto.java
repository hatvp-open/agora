/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.nomenclature;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.hatvp.registre.commons.lists.NomenclatureEnum;

public class NomenclatureDto {

	private Map<NomenclatureEnum, List<?>> nomenclature = new HashMap<>();

	/**
	 * @return the nomenclature
	 */
	public Map<NomenclatureEnum, List<?>> getNomenclature() {
		return nomenclature;
	}

	/**
	 * @param nomenclature
	 *            the nomenclature to set
	 */
	public void setNomenclature(Map<NomenclatureEnum, List<?>> nomenclature) {
		this.nomenclature = nomenclature;
	}

}
