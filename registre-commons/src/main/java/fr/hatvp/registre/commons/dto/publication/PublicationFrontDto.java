/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication;

import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.ALWAYS)
public class PublicationFrontDto extends PublicationDto {

    /** Logo de l'espace organisation. */
    private byte[] logo;

    /** Type MIME du logo de l'espace organisation. */
    private String logoType;

    /**
     * Accesseur en lecture du champ <code>logo</code>.
     *
     * @return le champ <code>logo</code>.
     */
    public byte[] getLogo() {
        return this.logo;
    }

    /**
     * Accesseur en écriture du champ <code>logo</code>.
     *
     * @param logo la valeur à écrire dans <code>logo</code>.
     */
    public void setLogo(final byte[] logo) {
        this.logo = logo;
    }

    /**
     * Accesseur en lecture du champ <code>logoType</code>.
     *
     * @return le champ <code>logoType</code>.
     */
    public String getLogoType() {
        return this.logoType;
    }

    /**
     * Accesseur en écriture du champ <code>logoType</code>.
     *
     * @param logoType la valeur à écrire dans <code>logoType</code>.
     */
    public void setLogoType(final String logoType) {
        this.logoType = logoType;
    }
}
