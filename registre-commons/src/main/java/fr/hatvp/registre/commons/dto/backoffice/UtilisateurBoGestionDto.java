/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.backoffice.RoleEnumBackOffice;
import fr.hatvp.registre.commons.validators.role.BackOfficeRole;

/**
 * Dto de l'entité Utilsateur backoffice avec la descente du password pour le front.
 * @version $Revision$ $Date${0xD}
 */
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UtilisateurBoGestionDto
    extends AbstractCommonDto
{

    /** mot de passe del'utilisateur. */
    private String password;

    /** nom de l'utilisateur. */
    @NotEmpty
    private String nom;

    /** prénom de l'utilisateur. */
    @NotEmpty
    private String prenom;

    /** email de l'utilisateur. */
    @NotEmpty
    @Email
    private String email;

    /** etat du compte de l'utilisateur. */
    private Boolean compteActive;

    /** etat du compte de l'utilisateur. */
    private Boolean compteSupprime;

    /** les roles de l'utilisateur. */
    @BackOfficeRole
    private List<RoleEnumBackOffice> roles = new ArrayList<>();

    /**
     * Accesseur en lecture du champ <code>password</code>.
     * @return le champ <code>password</code>.
     */
    public String getPassword()
    {
        return this.password;
    }

    /**
     * Accesseur en écriture du champ <code>password</code>.
     * @param password la valeur à écrire dans <code>password</code>.
     */
    public void setPassword(final String password)
    {
        this.password = password;
    }

    /**
     * Accesseur en lecture du champ <code>nom</code>.
     * @return le champ <code>nom</code>.
     */
    public String getNom()
    {
        return this.nom;
    }

    /**
     * Accesseur en écriture du champ <code>nom</code>.
     * @param nom la valeur à écrire dans <code>nom</code>.
     */
    public void setNom(final String nom)
    {
        this.nom = nom;
    }

    /**
     * Accesseur en lecture du champ <code>prenom</code>.
     * @return le champ <code>prenom</code>.
     */
    public String getPrenom()
    {
        return this.prenom;
    }

    /**
     * Accesseur en écriture du champ <code>prenom</code>.
     * @param prenom la valeur à écrire dans <code>prenom</code>.
     */
    public void setPrenom(final String prenom)
    {
        this.prenom = prenom;
    }

    /**
     * Accesseur en lecture du champ <code>email</code>.
     * @return le champ <code>email</code>.
     */
    public String getEmail()
    {
        return this.email;
    }

    /**
     * Accesseur en écriture du champ <code>email</code>.
     * @param email la valeur à écrire dans <code>email</code>.
     */
    public void setEmail(final String email)
    {
        this.email = email;
    }

    /**
     * Accesseur en lecture du champ <code>compteActive</code>.
     * @return le champ <code>compteActive</code>.
     */
    public Boolean getCompteActive()
    {
        return this.compteActive;
    }

    /**
     * Accesseur en écriture du champ <code>compteActive</code>.
     * @param compteActive la valeur à écrire dans <code>compteActive</code>.
     */
    public void setCompteActive(final Boolean compteActive)
    {
        this.compteActive = compteActive;
    }

    /**
     * Accesseur en lecture du champ <code>roles</code>.
     * @return le champ <code>roles</code>.
     */
    public List<RoleEnumBackOffice> getRoles()
    {
        return this.roles;
    }

    /**
     * Accesseur en écriture du champ <code>roles</code>.
     * @param roles la valeur à écrire dans <code>roles</code>.
     */
    public void setRoles(final List<RoleEnumBackOffice> roles)
    {
        this.roles = roles;
    }

    /**
     * Accesseur en lecture du champ <code>compteSupprime</code>.
     * @return le champ <code>compteSupprime</code>.
     */
    public Boolean getCompteSupprime()
    {
        return this.compteSupprime;
    }

    /**
     * Accesseur en écriture du champ <code>compteSupprime</code>.
     * @param compteSupprime la valeur à écrire dans <code>compteSupprime</code>.
     */
    public void setCompteSupprime(final Boolean compteSupprime)
    {
        this.compteSupprime = compteSupprime;
    }

}
