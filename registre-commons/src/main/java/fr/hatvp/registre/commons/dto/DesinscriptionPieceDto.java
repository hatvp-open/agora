/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.hatvp.registre.commons.lists.OrigineVersementEnum;

import java.util.Date;

public class DesinscriptionPieceDto extends AbstractCommonDto {

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm", timezone = "Europe/Paris")
    private Date dateVersementPiece;

    private OrigineVersementEnum origineVersement;

    private String nomFichier;

    @JsonIgnore
    private byte[] contenuFichier;

    private String mediaType;

    @JsonIgnore
    private Long declarantId;

    public Date getDateVersementPiece() {
        return dateVersementPiece;
    }

    public void setDateVersementPiece(Date dateVersementPiece) {
        this.dateVersementPiece = dateVersementPiece;
    }

    public OrigineVersementEnum getOrigineVersement() {
        return origineVersement;
    }

    public void setOrigineVersement(OrigineVersementEnum origineVersement) {
        this.origineVersement = origineVersement;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public void setNomFichier(String nomFichier) {
        this.nomFichier = nomFichier;
    }

    public byte[] getContenuFichier() {
        return contenuFichier;
    }

    public void setContenuFichier(byte[] contenuFichier) {
        this.contenuFichier = contenuFichier;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public Long getDeclarantId() {
        return declarantId;
    }

    public void setDeclarantId(Long declarantId) {
        this.declarantId = declarantId;
    }
}
