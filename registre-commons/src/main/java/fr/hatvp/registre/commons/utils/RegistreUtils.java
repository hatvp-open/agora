/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.utils;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.codec.Base64;

import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;


public class RegistreUtils {

    //FIXME Déplacer toutes les fonctions vers le Singleton RegistreRandomUtils !!!

    /**
     * Coder une chaine de caractères en 64 bit
     *
     * @param content contenu à coder.
     * @return une chaine de caractère.
     */
    public static String codeTo64(final String content) {
        final byte[] encodedBytes = Base64.encode(content.getBytes());
        return new String(encodedBytes);
    }

    /**
     * Coder un tableau de bit.
     *
     * @param content contenu à coder.
     * @return une chaine de caractère.
     */
    public static String codeTo64(final byte[] content) {
        final byte[] encodedBytes = Base64.encode(content);
        return new String(encodedBytes);
    }

    /**
     * Décoder une chaine de caractères codée en 64bit
     *
     * @param content contenu à décoder.
     * @return une chaine de caractère.
     */
    public static String decode64ToString(final String content) {
        final byte[] decodedBytes = Base64.decode(content.getBytes());
        return new String(decodedBytes);
    }

    /**
     * Crée un Long aléatoire.
     *
     * @param scale nombre de chiffre
     * @return un Long random
     */
    public static Long randomLong(final int scale) {
        return new Integer(randomString(scale).hashCode()).longValue();
    }


    /**
     * Crée un Float aléatoire.
     *
     * @param scale nombre de chiffre
     * @return un Long random
     */
    public static Float randomFloat(final int scale) {
        Random rand = new Random();
        Float floatNumber = rand.nextFloat();
        BigDecimal bd = new BigDecimal(Float.toString(floatNumber));
        bd = bd.setScale(scale, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    /**
     * Crée un String aléatoire.
     *
     * @param size nombre de caractère
     * @return un String random
     */
    public static String randomString(final int size) {
        return RandomStringUtils.randomAlphanumeric(size);
    }

    /**
     * Crée un tableau de byt aléatoire;
     *
     * @param size taille du tableau.
     * @return un tableau de byte aléatoire.
     */
    public static byte[] randomByteArray(final int size) {
        final SecureRandom random = new SecureRandom();
        final byte[] bytes = new byte[size];
        random.nextBytes(bytes);
        return bytes;
    }

    /**
     * Générer une date ancienne à celle d'aujourd'hui
     *
     * @param nombreDeJourAdeduire nombre de jour à déduire.
     * @return la date.
     */
    public static Date generateOldDate(final int nombreDeJourAdeduire) {
        return Date.from(LocalDate.now().minusDays(nombreDeJourAdeduire)
            .atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Générer une date supérieur à celle d'aujourd'hui
     *
     * @param nombreDeJourRajouter nombre de jour à rajouter.
     * @return la date.
     */
    public static Date generateNewDate(final int nombreDeJourRajouter) {
        final LocalDateTime ldt = LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.systemDefault())
            .plusDays(nombreDeJourRajouter);
        return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Cette méthode permet de changer aléatoirement les positions de caractères ds un String.
     *
     * @param input string à mélanger.
     * @return string mélangé.
     */
    public static String shuffle(final String input) {
        final List<Character> characters = new ArrayList<>();
        for (final char c : input.toCharArray()) {
            characters.add(c);
        }
        final StringBuilder output = new StringBuilder(input.length());
        while (!characters.isEmpty()) {
            final int randPicker = (int) (Math.random() * characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }

    /**
     * vérifier si la date
     *
     * @param ancienneDate l'ancienne date a testé.
     * @return true si l'ancienne date a expiré.
     */
    public static boolean isDateExpire(final Date ancienneDate) {
        final LocalDate dateToVerify = ancienneDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        return !dateToVerify.isAfter(LocalDate.now());
    }

    /**
     * True ou false
     *
     * @return true ou false aléatoirement
     */
    public static boolean randomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }

    /**
     * Normalisation des noms et prénoms.
     *
     * @param noms liste des noms à normaliser
     * @return tableau contenant dans l'ordre les noms et prénom normalisés.
     */
    public static List<String> normaliserUnGroupeDeNom(final String... noms) {
        if (noms.length == 0) {
            return null;
        }

        final List<String> names = new ArrayList<>();
        for (final String nom : noms) {
            names.add(normaliser(nom));
        }
        return names;
    }

    /**
     * Normaliser un prenom.
     *
     * @param prenom prenom à normaliser.
     * @return prenom normalisé.
     */
    public static String normaliser(String prenom) {
        // Saisie de plusieurs prénoms séparés par des espaces possible
        String[] prenoms = prenom.split(" ");
        // Filtre les chaînes vides (donc filtre les espaces redondants)
        prenoms = Arrays.stream(prenoms).filter(s -> !s.isEmpty()).toArray(String[]::new);

        // Pour chaque prénom saisi
        for (int i = 0; i < prenoms.length; i++) {
            // Prise en compte des cas de prénoms composés
            String[] prenomTab = prenoms[i].split("-");
            // Filtre les chaînes vides (donc filtre les traits d'union redondants)
            prenomTab = Arrays.stream(prenomTab).filter(s -> !s.isEmpty()).toArray(String[]::new);
            // Reconstitution du prénom composé avec la bonne casse

            if (prenomTab.length > 0) {
                prenoms[i] = prenomTab[0].substring(0, 1).toUpperCase() + prenomTab[0].substring(1).toLowerCase();
                for (int j = 1; j < prenomTab.length; j++) {
                    prenoms[i] = prenoms[i] + "-" + prenomTab[j].substring(0, 1).toUpperCase() +
                        prenomTab[j].substring(1).toLowerCase();
                }
            }
        }

        // Reconstitution de la liste des prénoms
        if (prenoms.length > 0) {
            prenom = prenoms[0];
            for (int i = 1; i < prenoms.length; i++) {
                prenom = prenom + " " + prenoms[i];
            }
        }

        return prenom;
    }

    /**
     * Concaténer le contenu d'une liste de string dans un seul string, séparé par des virgules.
     *
     * @param listToImplode liste à concaténer.
     * @return une chaine de caractère contenant l'ensemble de la liste.
     */
    public static String implodeListWithComma(final List<?> listToImplode) {

        final StringBuilder result = new StringBuilder();

        for (final Object o : listToImplode) {
            if (o instanceof SecteurActiviteEnum) {
                result.append(StringUtils.appendIfMissing(((SecteurActiviteEnum) o).name(), ","));
            } else if (o instanceof NiveauInterventionEnum) {
                result.append(StringUtils.appendIfMissing(((NiveauInterventionEnum) o).name(), ","));
            } else {
                throw new ClassCastException();
            }
        }

        return result.toString();
    }
}
