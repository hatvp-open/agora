/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.exceptions;

/**
 * Classe des business exceptions.
 *
 */
public class TechnicalException
    extends RuntimeException
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = 4197565037821992094L;

    /**
     * Constructeur.
     */
    public TechnicalException()
    {
        super();
    }

    /**
     * Constructeur.
     * @param message message d'erreur
     * @param cause cause de l'erreur
     */
    public TechnicalException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Constructeur.
     * @param message message d'erreur
     */
    public TechnicalException(final String message)
    {
        super(message);
    }

    public TechnicalException(final Throwable cause)
    {
        super(cause);
    }

}