/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.utilities;

import java.time.Clock;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

@Component
public class LocalDateNowBuilder {

	private Clock clock = Clock.systemDefaultZone();

	public LocalDate getLocalDateNow() {
		return LocalDate.now(clock);
	}

	/**
	 * @param clock
	 *            the clock to set
	 */
	public void setClock(Clock clock) {
		this.clock = clock;
	}

}
