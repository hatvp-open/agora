/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite;



public class ExerciceComptableBlacklistedDto extends ExerciceComptableSimpleDto {


	
	private String raisonBlacklistage;
	
	private Integer nbJoursBlacklistFA;
	private Integer nbJoursBlacklistMA;
	

	public String getRaisonBlacklistage() {
		return raisonBlacklistage;
	}

	public void setRaisonBlacklistage(String raisonBlacklistage) {
		this.raisonBlacklistage = raisonBlacklistage;
	}

	public Integer getNbJoursBlacklistFA() {
		return nbJoursBlacklistFA;
	}

	public void setNbJoursBlacklistFA(Integer nbJoursBlacklistFA) {
		this.nbJoursBlacklistFA = nbJoursBlacklistFA;
	}

	public Integer getNbJoursBlacklistMA() {
		return nbJoursBlacklistMA;
	}

	public void setNbJoursBlacklistMA(Integer nbJoursBlacklistMA) {
		this.nbJoursBlacklistMA = nbJoursBlacklistMA;
	}

	
	

	





}
