/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

/**
 * Classe d'énumération des identifiants de cartes Front de l'écran Informations Générales.
 *
 * @version $Revision$ $Date${0xD}
 */
public enum IdentifiantCardInfosGeneralesEnum
{

    ASSO_APPARTENANCE("asso_appartenance"),
    DIRIGEANTS("dirigeants"),
    DONNEES("donnees"),
    INFOS_CONTACT("infos_contact"),
    LOCALISATION("localisation"),
    PROFILE_ORGA("profile_orga"),
    WEB("web"),
    PRESENTATION_INTERET("representation_interet"),
    EXERCICE_COMPTABLE("exercice_comptable");

    private final String label;

    /**
     * Constructeur.
     *
     * @param label
     */
    IdentifiantCardInfosGeneralesEnum(final String label)
    {
        this.label = label;
    }

    @Override
    public String toString()
    {
        return this.label;
    }
}
