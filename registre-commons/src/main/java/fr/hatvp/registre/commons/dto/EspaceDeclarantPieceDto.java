/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.hatvp.registre.commons.lists.OrigineVersementEnum;

import java.util.Date;

/**
 * DTO pour la récupération des {@link fr.hatvp.registre.persistence.entity.piece.EspaceDeclarantPieceEntity}
 * 
 * @version $Revision$ $Date${0xD}
 */
public class EspaceDeclarantPieceDto
    extends AbstractCommonDto
{

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm", timezone = "Europe/Paris")
    private Date dateVersementPiece;

    private OrigineVersementEnum origineVersement;
    
    private String nomFichier;

    @JsonIgnore
    private byte[] contenuFichier;

    private String mediaType;

    @JsonIgnore
    private Long declarantId;
    
    /**
     * Gets dateVersementPiece
     * @return the dateVersementPiece
     */
    public Date getDateVersementPiece()
    {
        return dateVersementPiece;
    }

    /**
     * Sets dateVersementPiece 
     * @param dateVersementPiece the dateVersementPiece to set
     */
    public void setDateVersementPiece(Date dateVersementPiece)
    {
        this.dateVersementPiece = dateVersementPiece;
    }

    /**
     * Gets origineVersement
     * @return the origineVersement
     */
    public OrigineVersementEnum getOrigineVersement()
    {
        return origineVersement;
    }

    /**
     * Sets origineVersement 
     * @param origineVersement the origineVersement to set
     */
    public void setOrigineVersement(OrigineVersementEnum origineVersement)
    {
        this.origineVersement = origineVersement;
    }

    /**
     * Gets nomFichier
     * @return the nomFichier
     */
    public String getNomFichier()
    {
        return nomFichier;
    }

    /**
     * Sets nomFichier 
     * @param nomFichier the nomFichier to set
     */
    public void setNomFichier(String nomFichier)
    {
        this.nomFichier = nomFichier;
    }

    /**
     * Gets mediaType
     * @return the mediaType
     */
    public String getMediaType()
    {
        return mediaType;
    }

    /**
     * Sets mediaType 
     * @param mediaType the mediaType to set
     */
    public void setMediaType(String mediaType)
    {
        this.mediaType = mediaType;
    }

    /**
     * Gets contenuFichier
     * @return the contenuFichier
     */
    public byte[] getContenuFichier()
    {
        return contenuFichier;
    }

    /**
     * Sets contenuFichier 
     * @param contenuFichier the contenuFichier to set
     */
    public void setContenuFichier(byte[] contenuFichier)
    {
        this.contenuFichier = contenuFichier;
    }

    /**
     * Gets declarantId
     * @return the declarantId
     */
    public Long getDeclarantId()
    {
        return declarantId;
    }

    /**
     * Sets declarantId 
     * @param declarantId the declarantId to set
     */
    public void setDeclarantId(Long declarantId)
    {
        this.declarantId = declarantId;
    }

}
