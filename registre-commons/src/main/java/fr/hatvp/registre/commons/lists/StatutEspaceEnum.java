/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JsonNode;

import fr.hatvp.registre.commons.dto.EnumAsDto;

/**
 *
 * Classe d'énumération des status de validation des espaces organisations.
 *
 * @version $Revision$ $Date${0xD}
 */
public enum StatutEspaceEnum {

	NOUVEAU("Nouveau"),
	EN_TRAITEMENT("En traitement"),
	EN_ATTENTE_VALIDATION("En attente de validation"),
	COMPLEMENT_DEMANDE_ENVOYEE("Complément demandé"),
	COMPLEMENT_RECU("Complément reçu"),
	ACCEPTEE("Acceptée"),
	REFUSEE("Refusée"),
    ORGANISATION_SANS_ESPACE("Organisation sans espace"),
	DESINSCRIT("Désinscrit"),
	DESINSCRIPTION_DEMANDEE("Désinscription demandée"),
    DESINSCRIPTION_VALIDEE_HATVP ("Désinscription validée"),
    COMPLEMENTS_DEMANDE_DESINSCRIPTION_RECU ("Compléments demande de désinscription reçu"),
    COMPLEMENTS_DEMANDE_DESINSCRIPTION ("Compléments demande de désinscription envoyé");

	private final String label;

	StatutEspaceEnum(final String label) {
		this.label = label;
	}

	public String getValue() {
		return this.label;
	}

	@JsonCreator
	public static StatutEspaceEnum fromNode(final JsonNode node) {
		if (!node.has("code")) {
			return null;
		}
		final String name = node.get("code").asText();
		return StatutEspaceEnum.valueOf(name);
	}

	@JsonValue
	public EnumAsDto getStatutEspaceEnumDto() {
		return new EnumAsDto(this.name(), this.label);
	}

}
