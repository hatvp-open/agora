/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum; 

/**
 * DTO de recherche des publications.
 * @version $Revision$ $Date${0xD}
 */
public class PublicationRechercheDto
    extends AbstractCommonDto {

    /** Identifiant national. */
    @JsonProperty("identifiantNational")
    private String nationalId;

    /** Dénomination de l'organisation. */
    private String denomination;
    
    /** Nom d'usage de l'organisation. */
    private String nomUsage;
	/** Nom d'usage Interne de l'organisation. */
	private String nomUsageHatvp;
	private String ancienNomHatvp;
	/** Sigle de l'organisation. */
	private String sigleHatvp;
    /** Code Catégorie de l'organisation SIREn/RNA/HATVP ... */
    private TypeOrganisationEnum categorieOrganisation;

    /** La date de la 1ere publication. */
    @JsonProperty("datePremierePublication")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
    private Date creationDate;

    /** La date de la 1ere publication. */
    @JsonProperty("dateDernierePublicationActivite")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
    private Date dateDernierePublicationActivite;

	private Boolean isActivitesPubliees;



    /**
     * Accesseur en lecture du champ <code>codeCategorieOrganisation</code>.
     *
     * @return le champ <code>codeCategorieOrganisation</code>.
     */
    public TypeOrganisationEnum getCategorieOrganisation() {
        return this.categorieOrganisation;
    }

    public Date getDateDernierePublicationActivite() {
		return dateDernierePublicationActivite;
	}

	public void setDateDernierePublicationActivite(Date dateDernierePublicationActivite) {
		this.dateDernierePublicationActivite = dateDernierePublicationActivite;
	}

	/**
     * Accesseur en écriture du champ <code>codeCategorieOrganisation</code>.
     *
     * @param codeCategorieOrganisation la valeur à écrire dans <code>codeCategorieOrganisation</code>.
     */
    public void setCategorieOrganisation(final TypeOrganisationEnum codeCategorieOrganisation) {
        this.categorieOrganisation = codeCategorieOrganisation;
    }

    /**
     * /**
     * Accesseur en lecture du champ <code>denomination</code>.
     *
     * @return le champ <code>denomination</code>.
     */
    public String getDenomination() {
        return this.denomination;
    }

    /**
     * Accesseur en écriture du champ <code>denomination</code>.
     *
     * @param denomination la valeur à écrire dans <code>denomination</code>.
     */
    public void setDenomination(final String denomination) {
        this.denomination = denomination;
    }

    /**
     * Accesseur en lecture du champ <code>identifiantNational</code>.
     *
     * @return le champ <code>identifiantNational</code>.
     */
    public String getNationalId() {
        return this.nationalId;
    }

    /**
     * Accesseur en écriture du champ <code>identifiantNational</code>.
     *
     * @param nationalId la valeur à écrire dans <code>identifiantNational</code>.
     */
    public void setNationalId(final String nationalId) {
        this.nationalId = nationalId;
    }

    /**
     * Accesseur en lecture du champ <code>creationDate</code>.
     *
     * @return le champ <code>creationDate</code>.
     */
    public Date getCreationDate() {
        return this.creationDate;
    }

    /**
     * Accesseur en écriture du champ <code>creationDate</code>.
     *
     * @param creationDate la valeur à écrire dans <code>creationDate</code>.
     */
    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

	/**
	 * @return the nomUsage
	 */
	public String getNomUsage() {
		return nomUsage;
	}

	/**
	 * @param nomUsage the nomUsage to set
	 */
	public void setNomUsage(String nomUsage) {
		this.nomUsage = nomUsage;
	}
	/**
	 * Accesseur en lecture du champ <code>nomUsageHatvp</code>.
	 *
	 * @return le champ <code>nomUsageHatvp</code>.
	 */
	public String getNomUsageHatvp() {
		return this.nomUsageHatvp;
	}

	/**
	 * Accesseur en écriture du champ <code>nomUsageHatvp</code>.
	 *
	 * @param nomUsageHatvp
	 *            la valeur à écrire dans <code>nomUsageHatvp</code>.
	 */
	public void setNomUsageHatvp(final String nomUsageHatvp) {
		this.nomUsageHatvp = nomUsageHatvp;
	}
	
	/**
	 * Accesseur en lecture du champ <code>sigleHatvp</code>.
	 *
	 * @return le champ <code>sigleHatvp</code>.
	 */
	public String getSigleHatvp() {
		return this.sigleHatvp;
	}

	/**
	 * Accesseur en écriture du champ <code>sigleHatvp</code>.
	 *
	 * @param sigleHatvp
	 *            la valeur à écrire dans <code>sigleHatvp</code>.
	 */
	public void setSigleHatvp(final String sigleHatvp) {
		this.sigleHatvp = sigleHatvp;
	}
	
	public String getAncienNomHatvp() {
		return this.ancienNomHatvp;
	}
	public void setAncienNomHatvp(final String ancienNomHatvp) {
		this.ancienNomHatvp = ancienNomHatvp;
	}
	public void setIsActivitesPubliees(Boolean b) {
		this.isActivitesPubliees= b;
		
	}

	public Boolean getIsActivitesPubliees() {
		return isActivitesPubliees;
	}



}
