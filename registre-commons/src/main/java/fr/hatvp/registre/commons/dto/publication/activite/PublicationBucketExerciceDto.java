/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication.activite;

import com.fasterxml.jackson.annotation.JsonInclude;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

/**
 * Dto de publication contenant un exercice publié et un historique des precedentes publications de cet exercice
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PublicationBucketExerciceDto extends AbstractCommonDto
{
    /** dernière publication de l'exercice. */
    private PublicationExerciceDto publicationCourante;

    /** historique des publications de l'exercice. */
//    private List<PublicationExerciceDto> historique = new ArrayList<>();

    public PublicationExerciceDto getPublicationCourante()
    {
        return publicationCourante;
    }
    public void setPublicationCourante(PublicationExerciceDto publicationCourante)
    {
        this.publicationCourante = publicationCourante;
    }
//	public List<PublicationExerciceDto> getHistorique() {
//		return historique;
//	}
//	public void setHistorique(List<PublicationExerciceDto> historique) {
//		this.historique = historique;
//	}
    
}
