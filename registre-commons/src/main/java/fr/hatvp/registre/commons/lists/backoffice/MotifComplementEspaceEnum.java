/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists.backoffice;

/**
 * Classe d'énumération des motifs de refus de création des espaces organisations.
 *
 * @version $Revision$ $Date${0xD}
 */
public enum MotifComplementEspaceEnum
{
    ID_CONFORME_MANDAT_NON_CONFORME("ID représentant conforme / Mandat non conforme"),
    PIECE_ID_NON_CONFORME("ID représentant conforme / Mandat conforme / Pièce d'identité du représentant non conforme"),
    ID_NON_CONFORME("ID Représentant non conforme"),
    AUTRE("Autre");

    private final String label;

    /**
     * Constructeur.
     *
     * @param value
     *         identifiant motif
     */
    MotifComplementEspaceEnum(final String label)
    {
        this.label = label;
    }

    @Override
    public String toString()
    {
        return this.label;
    }
}
