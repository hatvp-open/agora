/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.hatvp.registre.commons.dto.backoffice.EspaceCollaboratifDto;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.utils.LocalDateTimeDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateTimeSerializer;

/**
 * DTO de l'entité ClientEntity.
 *
 * @version $Revision$ $Date${0xD}
 */
public class AssoClientDto
		extends AbstractCommonDto {
	
	/** id de l'organisation. */
	protected Long organisationId;
	
	/** espace_organisation de l'organisation affiliée/cliente **/
	protected Long espaceOrganisationIdDeOrdganisation;
	
	/** id de l'espace organisation. */
	protected Long espaceOrganisationId;	
	
	/** dénomination de l'organisation. */
	@Size(max = 150)
	protected String denomination;
	
    /** Nom d'usage de l'organisation. */
    @Size(max = 150)
    private String nomUsage;
	
	/** identifiant national de l'organisation. */
	protected String nationalId;
	
	/** Type identifiant national (SIREN/RNA) */
	protected TypeIdentifiantNationalEnum originNationalId;
	
	/** EC quand il existe **/
	protected EspaceCollaboratifDto espaceCollaboratifDto;
	
    /**
     * Date d'ajout du client
     */
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    protected LocalDateTime dateAjout;

    /**
     * Date de désactivation du client
     */
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    protected LocalDateTime dateDesactivation;

    /**
     * Ancien client oui/non
     */
    protected Boolean isAncienClient;
    
    protected Date datePremierePublication;
    
    protected Date dateDernierePublication;
	
	/**
	 * periode en cours permettant de déterminer date activiation, date desactivation et statut ancien client
	 */
    protected PeriodeActiviteClientDto periodeActiviteClientEnCours;
	
    protected List<PeriodeActiviteClientDto> periodeActiviteClientList;
    
    protected String commentaire;
	
	/**
	 * Date de désinscription
	 */
	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
    private Date dateCessation;
	
	protected Date dateDernierePublicationPeriodeEncours;

    /**
	 * Accesseur en lecture du champ <code>denomination</code>.
	 *
	 * @return le champ <code>denomination</code>.
	 */
	public String getDenomination() {
		return this.denomination;
	}
	
	/**
	 * Accesseur en écriture du champ <code>denomination</code>.
	 *
	 * @param denomination
	 * 		la valeur à écrire dans <code>denomination</code>.
	 */
	public void setDenomination(final String denomination) {
		this.denomination = denomination;
	}
	
	/**
	 * Accesseur en lecture du champ <code>nationalId</code>.
	 *
	 * @return le champ <code>nationalId</code>.
	 */
	public String getNationalId() {
		return this.nationalId;
	}
	
	/**
	 * Accesseur en écriture du champ <code>nationalId</code>.
	 *
	 * @param nationalId
	 * 		la valeur à écrire dans <code>nationalId</code>.
	 */
	public void setNationalId(final String nationalId) {
		this.nationalId = nationalId;
	}
	
	/**
	 * Accesseur en lecture du champ <code>organisationId</code>.
	 *
	 * @return le champ <code>organisationId</code>.
	 */
	public Long getOrganisationId() {
		return this.organisationId;
	}
	
	/**
	 * Accesseur en écriture du champ <code>organisationId</code>.
	 *
	 * @param organisationId
	 * 		la valeur à écrire dans <code>organisationId</code>.
	 */
	public void setOrganisationId(final Long organisationId) {
		this.organisationId = organisationId;
	}
	
	/**
	 * Accesseur en lecture du champ <code>espaceOrganisationId</code>.
	 *
	 * @return le champ <code>espaceOrganisationId</code>.
	 */
	public Long getEspaceOrganisationId() {
		return this.espaceOrganisationId;
	}
	
	/**
	 * Accesseur en écriture du champ <code>espaceOrganisationId</code>.
	 *
	 * @param espaceOrganisationId
	 * 		la valeur à écrire dans <code>espaceOrganisationId</code>.
	 */
	public void setEspaceOrganisationId(final Long espaceOrganisationId) {
		this.espaceOrganisationId = espaceOrganisationId;
	}
	
	/**
	 * Accesseur en lecture du champ <code>originNationalId</code>.
	 *
	 * @return le champ <code>originNationalId</code>.
	 */
	public TypeIdentifiantNationalEnum getOriginNationalId() {
		return this.originNationalId;
	}
	
	/**
	 * Accesseur en écriture du champ <code>originNationalId</code>.
	 *
	 * @param originNationalId
	 * 		la valeur à écrire dans <code>originNationalId</code>.
	 */
	public void setOriginNationalId(final TypeIdentifiantNationalEnum originNationalId) {
		this.originNationalId = originNationalId;
	}

	public String getNomUsage() {
		return nomUsage;
	}

	public void setNomUsage(String nomUsage) {
		this.nomUsage = nomUsage;
	}

    public LocalDateTime getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(LocalDateTime dateAjout) {
        this.dateAjout = dateAjout;
    }

    public LocalDateTime getDateDesactivation() {
        return dateDesactivation;
    }

    public void setDateDesactivation(LocalDateTime dateDesactivation) {
        this.dateDesactivation = dateDesactivation;
    }

    public Boolean getAncienClient() {
        return isAncienClient;
    }

    public void setAncienClient(Boolean isAncienClient) {
        this.isAncienClient = isAncienClient;
    }

	public Long getEspaceOrganisationIdDeOrdganisation() {
		return espaceOrganisationIdDeOrdganisation;
	}

	public void setEspaceOrganisationIdDeOrdganisation(Long espaceOrganisationIdDeOrdganisation) {
		this.espaceOrganisationIdDeOrdganisation = espaceOrganisationIdDeOrdganisation;
	}

	public EspaceCollaboratifDto getEspaceCollaboratifDto() {
		return espaceCollaboratifDto;
	}

	public void setEspaceCollaboratifDto(EspaceCollaboratifDto espaceCollaboratifDto) {
		this.espaceCollaboratifDto = espaceCollaboratifDto;
	}

	public Date getDatePremierePublication() {
		return datePremierePublication;
	}

	public void setDatePremierePublication(Date datePremierePublication) {
		this.datePremierePublication = datePremierePublication;
	}

	public PeriodeActiviteClientDto getPeriodeActiviteClientEnCours() {
		return periodeActiviteClientEnCours;
	}

	public void setPeriodeActiviteClientEnCours(PeriodeActiviteClientDto periodeActiviteClientEnCours) {
		this.periodeActiviteClientEnCours = periodeActiviteClientEnCours;
	}

	public List<PeriodeActiviteClientDto> getPeriodeActiviteClientList() {
		return periodeActiviteClientList;
	}

	public void setPeriodeActiviteClientList(List<PeriodeActiviteClientDto> periodeActiviteClientList) {
		this.periodeActiviteClientList = periodeActiviteClientList;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Date getDateCessation() {
		return dateCessation;
	}

	public void setDateCessation(Date dateCessation) {
		this.dateCessation = dateCessation;
	}

	public Date getDateDernierePublicationPeriodeEncours() {
		return dateDernierePublicationPeriodeEncours;
	}

	public void setDateDernierePublicationPeriodeEncours(Date dateDernierePublicationPeriodeEncours) {
		this.dateDernierePublicationPeriodeEncours = dateDernierePublicationPeriodeEncours;
	}

	public Date getDateDernierePublication() {
		return dateDernierePublication;
	}

	public void setDateDernierePublication(Date dateDernierePublication) {
		this.dateDernierePublication = dateDernierePublication;
	}	
    
}
