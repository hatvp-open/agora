/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationExerciceHistoriqueDto;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.utils.LocalDateDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateSerializer;

public class ExerciceComptableEtDeclarationDto extends AbstractCommonDto {

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateDebut;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateFin;

	private Long chiffreAffaire;

	private Boolean hasNotChiffreAffaire;

	private Long montantDepense;

	private Integer nombreSalaries;
	private Boolean isPublication;
	private List<ActiviteRepresentationInteretSimpleDto> activiteSimpleDto = new ArrayList<>();

	private Long espaceOrganisationId;
	private StatutPublicationEnum statut;

	private boolean noActivite;
	private String dateFirstCommMoyens;
	private String dateLastChangeMoyens;

	private String commentaire;
	
	private List<PublicationExerciceHistoriqueDto> exerciceHistoriquePublicationList;
	
	/**
	 * @return the dateDebut
	 */
	public LocalDate getDateDebut() {
		return dateDebut;
	}

	/**
	 * @param dateDebut
	 *            the dateDebut to set
	 */
	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	/**
	 * @return the dateFin
	 */
	public LocalDate getDateFin() {
		return dateFin;
	}

	/**
	 * @param dateFin
	 *            the dateFin to set
	 */
	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	/**
	 * @return the chiffreAffaire
	 */
	public Long getChiffreAffaire() {
		return chiffreAffaire;
	}

	/**
	 * @param chiffreAffaire
	 *            the chiffreAffaire to set
	 */
	public void setChiffreAffaire(Long chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	/**
	 * @return the hasNotChiffreAffaire
	 */
	public Boolean getHasNotChiffreAffaire() {
		return hasNotChiffreAffaire;
	}

	/**
	 * @param hasNotChiffreAffaire
	 *            the hasNotChiffreAffaire to set
	 */
	public void setHasNotChiffreAffaire(Boolean hasNotChiffreAffaire) {
		this.hasNotChiffreAffaire = hasNotChiffreAffaire;
	}

	/**
	 * @return the montantDepense
	 */
	public Long getMontantDepense() {
		return montantDepense;
	}

	/**
	 * @param montantDepense
	 *            the montantDepense to set
	 */
	public void setMontantDepense(Long montantDepense) {
		this.montantDepense = montantDepense;
	}

	/**
	 * @return the nombreSalaries
	 */
	public Integer getNombreSalaries() {
		return nombreSalaries;
	}

	/**
	 * @param nombreSalaries
	 *            the nombreSalaries to set
	 */
	public void setNombreSalaries(Integer nombreSalaries) {
		this.nombreSalaries = nombreSalaries;
	}

	/**
	 * @return the isPublication
	 */
	public Boolean getIsPublication() {
		return isPublication;
	}

	/**
	 * @param isPublication
	 *            the isPublication to set
	 */
	public void setIsPublication(Boolean isPublication) {
		this.isPublication = isPublication;
	}

	/**
	 * @return the activiteSimpleDto
	 */
	public List<ActiviteRepresentationInteretSimpleDto> getActiviteSimpleDto() {
		return activiteSimpleDto;
	}

	/**
	 * @param activiteSimpleDto
	 *            the activiteSimpleDto to set
	 */
	public void setActiviteSimpleDto(List<ActiviteRepresentationInteretSimpleDto> activiteSimpleDto) {
		this.activiteSimpleDto = activiteSimpleDto;
	}

	/**
	 * @return the espaceOrganisationId
	 */
	public Long getEspaceOrganisationId() {
		return espaceOrganisationId;
	}

	/**
	 * @param espaceOrganisationId
	 *            the espaceOrganisationId to set
	 */
	public void setEspaceOrganisationId(Long espaceOrganisationId) {
		this.espaceOrganisationId = espaceOrganisationId;
	}

	/**
	 * @return the statut
	 */
	public StatutPublicationEnum getStatut() {
		return statut;
	}

	/**
	 * @param statut
	 *            the statut to set
	 */
	public void setStatut(StatutPublicationEnum statut) {
		this.statut = statut;
	}

	/**
	 * @return the noActivite
	 */
	public boolean isNoActivite() {
		return noActivite;
	}

	/**
	 * @param noActivite
	 *            the noActivite to set
	 */
	public void setNoActivite(boolean noActivite) {
		this.noActivite = noActivite;
	}

    public String getDateFirstCommMoyens() {
        return dateFirstCommMoyens;
    }

    public void setDateFirstCommMoyens(String dateFirstCommMoyens) {
        this.dateFirstCommMoyens = dateFirstCommMoyens;
    }

    public String getDateLastChangeMoyens() {
        return dateLastChangeMoyens;
    }

    public void setDateLastChangeMoyens(String dateLastChangeMoyens) {
        this.dateLastChangeMoyens = dateLastChangeMoyens;
    }

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public List<PublicationExerciceHistoriqueDto> getExerciceHistoriquePublicationList() {
		return exerciceHistoriquePublicationList;
	}

	public void setExerciceHistoriquePublicationList(List<PublicationExerciceHistoriqueDto> exerciceHistoriquePublicationList) {
		this.exerciceHistoriquePublicationList = exerciceHistoriquePublicationList;
	}	
	
}
