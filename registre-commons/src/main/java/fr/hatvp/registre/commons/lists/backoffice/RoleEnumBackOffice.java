/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists.backoffice;

/**
 * Enumération de tous les roles du backoffice.
 *
 * @version $Revision$ $Date${0xD}
 */
public enum RoleEnumBackOffice {

	VALIDATION_ESPACES_COLLABORATIFS,
	GESTION_COMPTES_DECLARANTS,
	GESTION_CONTENUS_PUBLIES,
	GESTION_ESPACES_COLLABORATIFS_VALIDES,
	GESTION_REFERENTIEL_ORGANISATIONS,
	MODIFICATION_DES_RAISONS_SOCIALES,
	MODIFICATION_EMAIL_DECLARANT,
	GESTION_MANDATS,
	RESTRICTION_ACCES,
	ADMINISTRATEUR,
	MANAGER,
	DATA_DOWNLOADER,
	ETAT_ACTIVITES

}
