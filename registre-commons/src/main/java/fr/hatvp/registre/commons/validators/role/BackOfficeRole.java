/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.validators.role;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import fr.hatvp.registre.commons.lists.backoffice.RoleEnumBackOffice;

/**
 * Annotation de validation des roles backoffice.
 *
 * @version $Revision$ $Date${0xD}
 */
@Documented
@Constraint(validatedBy = RoleValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
public @interface BackOfficeRole {
	String message() default "Role(s) not valid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	RoleEnumBackOffice[] value() default { RoleEnumBackOffice.RESTRICTION_ACCES, RoleEnumBackOffice.GESTION_COMPTES_DECLARANTS, RoleEnumBackOffice.GESTION_ESPACES_COLLABORATIFS_VALIDES,
			RoleEnumBackOffice.VALIDATION_ESPACES_COLLABORATIFS, RoleEnumBackOffice.GESTION_CONTENUS_PUBLIES, RoleEnumBackOffice.GESTION_REFERENTIEL_ORGANISATIONS,
			RoleEnumBackOffice.MANAGER, RoleEnumBackOffice.MODIFICATION_DES_RAISONS_SOCIALES, RoleEnumBackOffice.MODIFICATION_EMAIL_DECLARANT, RoleEnumBackOffice.ADMINISTRATEUR,
			RoleEnumBackOffice.GESTION_MANDATS, RoleEnumBackOffice.DATA_DOWNLOADER, RoleEnumBackOffice.ETAT_ACTIVITES };
}
