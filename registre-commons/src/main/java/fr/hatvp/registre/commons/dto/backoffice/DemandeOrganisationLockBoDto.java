/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import java.util.Date;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

/**
 * Dto de l'entité DemandeOrganisationLockBoEntity.
 *
 * @version $Revision$ $Date${0xD}
 */
public class DemandeOrganisationLockBoDto
    extends AbstractCommonDto
{

    /**
     * Id Demande Organisation.
     */
    private Long demandeOrganisationId;

    /**
     * Id Utilisateur BO.
     */
    private Long utilisateurBoId;

    /**
     * Nom complet Utilisateur BO.
     */
    private String utilisateurBoName;

    /**
     * Date lock.
     */
    private Date lockTimeStart;

    /**
     * Temps d'attente restant (s).
     */
    private Long lockTimeRemain;

    /**
     * Gets utilisateur bo id.
     *
     * @return the utilisateur bo id
     */
    public Long getUtilisateurBoId()
    {
        return this.utilisateurBoId;
    }

    /**
     * Sets utilisateur bo id.
     *
     * @param utilisateurBoId the utilisateur bo id
     */
    public void setUtilisateurBoId(final Long utilisateurBoId)
    {
        this.utilisateurBoId = utilisateurBoId;
    }

    /**
     * Gets utilisateur bo name.
     *
     * @return the utilisateur bo name
     */
    public String getUtilisateurBoName()
    {
        return this.utilisateurBoName;
    }

    /**
     * Sets utilisateur bo name.
     *
     * @param utilisateurBoName the utilisateur bo name
     */
    public void setUtilisateurBoName(final String utilisateurBoName)
    {
        this.utilisateurBoName = utilisateurBoName;
    }

    /**
     * Gets lock time start.
     *
     * @return the lock time start
     */
    public Date getLockTimeStart()
    {
        return this.lockTimeStart;
    }

    /**
     * Sets lock time start.
     *
     * @param lockTimeStart the lock time start
     */
    public void setLockTimeStart(final Date lockTimeStart)
    {
        this.lockTimeStart = lockTimeStart;
    }

    /**
     * Gets lock time remain.
     *
     * @return the lock time remain
     */
    public Long getLockTimeRemain()
    {
        return this.lockTimeRemain;
    }

    /**
     * Sets lock time remain.
     *
     * @param lockTimeRemain the lock time remain
     */
    public void setLockTimeRemain(final Long lockTimeRemain)
    {
        this.lockTimeRemain = lockTimeRemain;
    }

    /**
     * Gets demande organisation id.
     *
     * @return the demande organisation id
     */
    public Long getDemandeOrganisationId()
    {
        return this.demandeOrganisationId;
    }

    /**
     * Sets demande organisation id.
     *
     * @param demandeOrganisationId the demande organisation id
     */
    public void setDemandeOrganisationId(final Long demandeOrganisationId)
    {
        this.demandeOrganisationId = demandeOrganisationId;
    }
}
