/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketExerciceDto;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO de l'open data des publication.
 */
public class PublicationBucketDto
        extends AbstractCommonDto {

    /** Logos de l'espace corporate. */
    private byte[] logo;
    
    /** Type MIME du logo de l'espace organisation. */
    private String logoType;

    /** dernière publication de l'espace. */
    private PublicationDto publicationCourante;

    /** historique des publications de l'espace. */
//    private List<PublicationDto> historique = new ArrayList<>();

    /** historique des publications de l'espace. */
    private List<PublicationBucketExerciceDto> exercices = new ArrayList<>();
    /**
     * Accesseur en lecture du champ <code>logo</code>.
     *
     * @return le champ <code>logo</code>.
     */
    public byte[] getLogo() {
        return this.logo;
    }

    /**
     * Accesseur en écriture du champ <code>logo</code>.
     *
     * @param logo la valeur à écrire dans <code>logo</code>.
     */
    public void setLogo(final byte[] logo) {
        this.logo = logo;
    }

//    /**
//     * Accesseur en lecture du champ <code>historique</code>.
//     *
//     * @return le champ <code>historique</code>.
//     */
//    public List<PublicationDto> getHistorique() {
//        return this.historique;
//    }

    /**
     * Accesseur en lecture du champ <code>logoType</code>.
     *
     * @return le champ <code>logoType</code>.
     */
    public String getLogoType() {
        return this.logoType;
    }

    /**
     * Accesseur en écriture du champ <code>logoType</code>.
     *
     * @param logoType la valeur à écrire dans <code>logoType</code>.
     */
    public void setLogoType(final String logoType) {
        this.logoType = logoType;
    }

    /**
     * Accesseur en lecture du champ <code>publicationCourrante</code>.
     *
     * @return le champ <code>publicationCourrante</code>.
     */
    public PublicationDto getPublicationCourante() {
        return this.publicationCourante;
    }

    /**
     * Accesseur en écriture du champ <code>publicationCourrante</code>.
     *
     * @param publicationCourrante la valeur à écrire dans <code>publicationCourrante</code>.
     */
    public void setPublicationCourante(final PublicationDto publicationCourante) {
        this.publicationCourante = publicationCourante;
    }

    /**
     * Accesseur en écriture du champ <code>historique</code>.
     *
     * @param historique la valeur à écrire dans <code>historique</code>.
     */
//    public void setHistorique(final List<PublicationDto> historique) {
//        this.historique = historique;
//    }

    public List<PublicationBucketExerciceDto> getExercices() {
        return exercices;
    }

    public void setExercices(List<PublicationBucketExerciceDto> exercices) {
        this.exercices = exercices;
    }
}
