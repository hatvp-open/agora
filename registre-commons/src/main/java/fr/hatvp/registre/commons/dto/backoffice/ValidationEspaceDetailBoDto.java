/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;

public class ValidationEspaceDetailBoDto extends AbstractCommonDto {

	/** Id de l'inscription. */
	private Long validationInscriptionId;

	/** Denomination de l'organisation */
	private String denomination;

	/** nom d'usage */
	private String nomUsage;

	/** nom d'usage HATVP */
	private String nomUsageHatvp;

	/** sigle HATVP */
	private String sigleHatvp;

	/** ancien nom HATVP */
	private String ancienNomHatvp;

	/** date de création */
	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
	private Date creationDate;

	/** Nom complet créateur espace organisation */
	private String createurNomComplet;

	/** mail du créateur de l'EC */
	private String createurEmail;

	/** identifiant siren, rna ou hatvp */
	private String nationalId;

	/** type d'identifiant */
	private TypeIdentifiantNationalEnum typeIdentifiantNational;

	/** adresse */
	private String adresse;

	/** code postal */
	private String codePostal;

	/** ville */
	private String ville;

	/** pays */
	private String pays;

	/** liste des dirigeants */
	private List<String> dirigeants;

	/** si le créateur est le représentant légal */
	private boolean representantLegal;

	/** Statut Validation Espace. */
	private StatutEspaceEnum statut;

	/** id du créateur */
	private Long createurId;

	private Long organisationId;
	
	private boolean nonDeclarationTiers;
	private boolean nonDeclarationOrgaAppartenance;
	private byte[] logo;
	private String logoType;
	private boolean relancesBloquees;
	private Boolean nonPublierMonAdressePhysique;
	private String secteursActivites;
	private String niveauIntervention;
	private String emailDeContact;
	private Boolean nonPublierMonAdresseEmail;
	private String telephoneDeContact;
	private Boolean nonPublierMonTelephoneDeContact;
	private String lienSiteWeb;
	private String lienPageTwitter;
	private String lienPageFacebook;
	private String lienPageLinkedin;
	private String finExerciceFiscal;
	private Long chiffreAffaire;
	private Integer anneeChiffreAffaire;
	private Long chiffreAffaireRepresentationInteret;
	private Integer anneeChiffreAffaireRepresentationInteret;
	private Long budgetTotal;
	private Integer anneeBudget;
	private Long depenses;
	private Integer anneeDepenses;
	private Integer nPersonnesEmployees;
	private Boolean nonExerciceComptable;

	/**
	 * @return the validationInscriptionId
	 */
	public Long getValidationInscriptionId() {
		return validationInscriptionId;
	}

	/**
	 * @param validationInscriptionId
	 *            the validationInscriptionId to set
	 */
	public void setValidationInscriptionId(Long validationInscriptionId) {
		this.validationInscriptionId = validationInscriptionId;
	}

	/**
	 * @return the denomination
	 */
	public String getDenomination() {
		return denomination;
	}

	/**
	 * @param denomination
	 *            the denomination to set
	 */
	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	/**
	 * @return the nomUsage
	 */
	public String getNomUsage() {
		return nomUsage;
	}

	/**
	 * @param nomUsage
	 *            the nomUsage to set
	 */
	public void setNomUsage(String nomUsage) {
		this.nomUsage = nomUsage;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the createurNomComplet
	 */
	public String getCreateurNomComplet() {
		return createurNomComplet;
	}

	/**
	 * @param createurNomComplet
	 *            the createurNomComplet to set
	 */
	public void setCreateurNomComplet(String createurNomComplet) {
		this.createurNomComplet = createurNomComplet;
	}

	/**
	 * @return the createurEmail
	 */
	public String getCreateurEmail() {
		return createurEmail;
	}

	/**
	 * @param createurEmail
	 *            the createurEmail to set
	 */
	public void setCreateurEmail(String createurEmail) {
		this.createurEmail = createurEmail;
	}

	/**
	 * @return the nationalId
	 */
	public String getNationalId() {
		return nationalId;
	}

	/**
	 * @param nationalId
	 *            the nationalId to set
	 */
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	/**
	 * @return the typeIdentifiantNational
	 */
	public TypeIdentifiantNationalEnum getTypeIdentifiantNational() {
		return typeIdentifiantNational;
	}

	/**
	 * @param typeIdentifiantNational
	 *            the typeIdentifiantNational to set
	 */
	public void setTypeIdentifiantNational(TypeIdentifiantNationalEnum typeIdentifiantNational) {
		this.typeIdentifiantNational = typeIdentifiantNational;
	}

	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse
	 *            the adresse to set
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/**
	 * @return the codePostal
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * @param codePostal
	 *            the codePostal to set
	 */
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * @param ville
	 *            the ville to set
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * @return the pays
	 */
	public String getPays() {
		return pays;
	}

	/**
	 * @param pays
	 *            the pays to set
	 */
	public void setPays(String pays) {
		this.pays = pays;
	}

	/**
	 * @return the dirigeants
	 */
	public List<String> getDirigeants() {
		return dirigeants;
	}

	/**
	 * @param dirigeants
	 *            the dirigeants to set
	 */
	public void setDirigeants(List<String> dirigeants) {
		this.dirigeants = dirigeants;
	}

	/**
	 * @return the representantLegal
	 */
	public boolean isRepresentantLegal() {
		return representantLegal;
	}

	/**
	 * @param representantLegal
	 *            the representantLegal to set
	 */
	public void setRepresentantLegal(boolean representantLegal) {
		this.representantLegal = representantLegal;
	}

	/**
	 * @return the statut
	 */
	public StatutEspaceEnum getStatut() {
		return statut;
	}

	/**
	 * @param statut
	 *            the statut to set
	 */
	public void setStatut(StatutEspaceEnum statut) {
		this.statut = statut;
	}

	/**
	 * @return the createurId
	 */
	public Long getCreateurId() {
		return createurId;
	}

	/**
	 * @param createurId
	 *            the createurId to set
	 */
	public void setCreateurId(Long createurId) {
		this.createurId = createurId;
	}

	public String getNomUsageHatvp() {
		return nomUsageHatvp;
	}

	public void setNomUsageHatvp(String nomUsageHatvp) {
		this.nomUsageHatvp = nomUsageHatvp;
	}

	public String getSigleHatvp() {
		return sigleHatvp;
	}

	public void setSigleHatvp(String sigleHatvp) {
		this.sigleHatvp = sigleHatvp;
	}

	public String getAncienNomHatvp() {
		return ancienNomHatvp;
	}

	public void setAncienNomHatvp(String ancienNomHatvp) {
		this.ancienNomHatvp = ancienNomHatvp;
	}

	public Long getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}

	public boolean isNonDeclarationTiers() {
		return nonDeclarationTiers;
	}

	public void setNonDeclarationTiers(boolean nonDeclarationTiers) {
		this.nonDeclarationTiers = nonDeclarationTiers;
	}

	public boolean isNonDeclarationOrgaAppartenance() {
		return nonDeclarationOrgaAppartenance;
	}

	public void setNonDeclarationOrgaAppartenance(boolean nonDeclarationOrgaAppartenance) {
		this.nonDeclarationOrgaAppartenance = nonDeclarationOrgaAppartenance;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public String getLogoType() {
		return logoType;
	}

	public void setLogoType(String logoType) {
		this.logoType = logoType;
	}

	public boolean isRelancesBloquees() {
		return relancesBloquees;
	}

	public void setRelancesBloquees(boolean relancesBloquees) {
		this.relancesBloquees = relancesBloquees;
	}

	public Boolean getNonPublierMonAdressePhysique() {
		return nonPublierMonAdressePhysique;
	}

	public void setNonPublierMonAdressePhysique(Boolean nonPublierMonAdressePhysique) {
		this.nonPublierMonAdressePhysique = nonPublierMonAdressePhysique;
	}

	public String getSecteursActivites() {
		return secteursActivites;
	}

	public void setSecteursActivites(String secteursActivites) {
		this.secteursActivites = secteursActivites;
	}

	public String getNiveauIntervention() {
		return niveauIntervention;
	}

	public void setNiveauIntervention(String niveauIntervention) {
		this.niveauIntervention = niveauIntervention;
	}

	public String getEmailDeContact() {
		return emailDeContact;
	}

	public void setEmailDeContact(String emailDeContact) {
		this.emailDeContact = emailDeContact;
	}

	public Boolean getNonPublierMonAdresseEmail() {
		return nonPublierMonAdresseEmail;
	}

	public void setNonPublierMonAdresseEmail(Boolean nonPublierMonAdresseEmail) {
		this.nonPublierMonAdresseEmail = nonPublierMonAdresseEmail;
	}

	public String getTelephoneDeContact() {
		return telephoneDeContact;
	}

	public void setTelephoneDeContact(String telephoneDeContact) {
		this.telephoneDeContact = telephoneDeContact;
	}

	public Boolean getNonPublierMonTelephoneDeContact() {
		return nonPublierMonTelephoneDeContact;
	}

	public void setNonPublierMonTelephoneDeContact(Boolean nonPublierMonTelephoneDeContact) {
		this.nonPublierMonTelephoneDeContact = nonPublierMonTelephoneDeContact;
	}

	public String getLienSiteWeb() {
		return lienSiteWeb;
	}

	public void setLienSiteWeb(String lienSiteWeb) {
		this.lienSiteWeb = lienSiteWeb;
	}

	public String getLienPageTwitter() {
		return lienPageTwitter;
	}

	public void setLienPageTwitter(String lienPageTwitter) {
		this.lienPageTwitter = lienPageTwitter;
	}

	public String getLienPageFacebook() {
		return lienPageFacebook;
	}

	public void setLienPageFacebook(String lienPageFacebook) {
		this.lienPageFacebook = lienPageFacebook;
	}

	public String getLienPageLinkedin() {
		return lienPageLinkedin;
	}

	public void setLienPageLinkedin(String lienPageLinkedin) {
		this.lienPageLinkedin = lienPageLinkedin;
	}

	public String getFinExerciceFiscal() {
		return finExerciceFiscal;
	}

	public void setFinExerciceFiscal(String finExerciceFiscal) {
		this.finExerciceFiscal = finExerciceFiscal;
	}

	public Long getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(Long chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	public Integer getAnneeChiffreAffaire() {
		return anneeChiffreAffaire;
	}

	public void setAnneeChiffreAffaire(Integer anneeChiffreAffaire) {
		this.anneeChiffreAffaire = anneeChiffreAffaire;
	}

	public Long getChiffreAffaireRepresentationInteret() {
		return chiffreAffaireRepresentationInteret;
	}

	public void setChiffreAffaireRepresentationInteret(Long chiffreAffaireRepresentationInteret) {
		this.chiffreAffaireRepresentationInteret = chiffreAffaireRepresentationInteret;
	}

	public Integer getAnneeChiffreAffaireRepresentationInteret() {
		return anneeChiffreAffaireRepresentationInteret;
	}

	public void setAnneeChiffreAffaireRepresentationInteret(Integer anneeChiffreAffaireRepresentationInteret) {
		this.anneeChiffreAffaireRepresentationInteret = anneeChiffreAffaireRepresentationInteret;
	}

	public Long getBudgetTotal() {
		return budgetTotal;
	}

	public void setBudgetTotal(Long budgetTotal) {
		this.budgetTotal = budgetTotal;
	}

	public Integer getAnneeBudget() {
		return anneeBudget;
	}

	public void setAnneeBudget(Integer anneeBudget) {
		this.anneeBudget = anneeBudget;
	}

	public Long getDepenses() {
		return depenses;
	}

	public void setDepenses(Long depenses) {
		this.depenses = depenses;
	}

	public Integer getAnneeDepenses() {
		return anneeDepenses;
	}

	public void setAnneeDepenses(Integer anneeDepenses) {
		this.anneeDepenses = anneeDepenses;
	}

	public Integer getnPersonnesEmployees() {
		return nPersonnesEmployees;
	}

	public void setnPersonnesEmployees(Integer nPersonnesEmployees) {
		this.nPersonnesEmployees = nPersonnesEmployees;
	}

	public Boolean getNonExerciceComptable() {
		return nonExerciceComptable;
	}

	public void setNonExerciceComptable(Boolean nonExerciceComptable) {
		this.nonExerciceComptable = nonExerciceComptable;
	}
	
	

}
