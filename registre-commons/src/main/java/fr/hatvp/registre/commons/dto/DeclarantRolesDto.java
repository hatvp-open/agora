

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;


public class DeclarantRolesDto
        extends AbstractCommonDto {
    /** Rôle Administrateur */
    private Boolean administrator;

    /** Rôle Publicateur */
    private Boolean publisher;

    /** Rôle Contributeur */
    private Boolean contributor;

    /** Compte verouillé */
    private Boolean locked;

    public Boolean isAdministrator() {
        return this.administrator;
    }

    public void setAdministrator(final Boolean administrator) {
        this.administrator = administrator;
    }

    public Boolean isPublisher() {
        return this.publisher;
    }

    public void setPublisher(final Boolean publisher) {
        this.publisher = publisher;
    }

    public Boolean isContributor() {
        return this.contributor;
    }

    public void setContributor(final Boolean contributor) {
        this.contributor = contributor;
    }

    public Boolean isLocked() {
        return this.locked;
    }

    public void setLocked(final Boolean locked) {
        this.locked = locked;
    }
}
