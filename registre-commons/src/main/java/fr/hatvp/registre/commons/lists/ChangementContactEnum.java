/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 *
 * Classe d'énumération des roles Front-Office;
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ChangementContactEnum {

	// Roles front-office.
	AJOUT_OPERATIONNEL("Ajout d'un nouveau contact opérationnel"), REMPLACEMENT_OPERATIONNEL("Remplacement d'un nouveau contact opérationnel"), CHANGEMENT_REPRESENTANT(
			"Changement de représentant légal");

	private final String label;

	ChangementContactEnum(final String label) {
		this.label = label;
	}

	@JsonValue
	public String getName() {
		return this.name();
	}

	public String getLabel() {
		return this.label;
	}
}
