/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

public class TiersDto extends AbstractCommonDto {

	@NotNull
	private Long organisationId;

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private String denomination;

	public TiersDto(Long organisationId, String denomination) {
		this.organisationId = organisationId;
		this.denomination = denomination;
	}	

	public TiersDto() {
	}

	/**
	 * @return the organisationId
	 */
	public Long getOrganisationId() {
		return organisationId;
	}

	/**
	 * @param organisationId
	 *            the organisationId to set
	 */
	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}

	/**
	 * @return the denomination
	 */
	public String getDenomination() {
		return denomination;
	}

	/**
	 * @param denomination
	 *            the denomination to set
	 */
	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}
}
