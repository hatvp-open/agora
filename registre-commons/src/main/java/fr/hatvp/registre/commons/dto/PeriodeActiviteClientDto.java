/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers.DateDeserializer;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.commons.utils.LocalDateDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateSerializer;
import fr.hatvp.registre.commons.utils.LocalDateTimeDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateTimeSerializer;

/**
 * DTO de l'entité ClientEntity.
 *
 * @version $Revision$ $Date${0xD}
 */
public class PeriodeActiviteClientDto
		extends AbstractCommonDto {
	
	private Long clientId;		

	@JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime dateAjout;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime dateDesactivation;
	
	@JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate dateFinContrat;

	private String commentaire;
	
	@JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
	private Date datePublicationActivation;
	
	@JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
	private Date datePublicationDesactivation;
	
	private StatutPublicationEnum statut;

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public LocalDateTime getDateAjout() {
		return dateAjout;
	}

	public void setDateAjout(LocalDateTime dateAjout) {
		this.dateAjout = dateAjout;
	}

	public LocalDateTime getDateDesactivation() {
		return dateDesactivation;
	}

	public void setDateDesactivation(LocalDateTime dateDesactivation) {
		this.dateDesactivation = dateDesactivation;
	}
	
	public LocalDate getDateFinContrat() {
		return dateFinContrat;
	}

	public void setDateFinContrat(LocalDate dateFinContrat) {
		this.dateFinContrat = dateFinContrat;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public StatutPublicationEnum getStatut() {
		return statut;
	}

	public void setStatut(StatutPublicationEnum statut) {
		this.statut = statut;
	}

	public Date getDatePublicationActivation() {
		return datePublicationActivation;
	}

	public void setDatePublicationActivation(Date datePublicationActivation) {
		this.datePublicationActivation = datePublicationActivation;
	}

	public Date getDatePublicationDesactivation() {
		return datePublicationDesactivation;
	}

	public void setDatePublicationDesactivation(Date datePublicationDesactivation) {
		this.datePublicationDesactivation = datePublicationDesactivation;
	} 
    
	
}
