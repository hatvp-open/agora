/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication;

import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;

import java.util.ArrayList;
import java.util.List;


public class PubActiviteDto {

    /** Champ secteurs des activités. */
    private List<SecteurActiviteEnum> listSecteursActivites = new ArrayList<>();

    /** Champ niveaux des interventions. */
    private List<NiveauInterventionEnum> listNiveauIntervention = new ArrayList<>();

    public List<SecteurActiviteEnum> getListSecteursActivites() {
        return this.listSecteursActivites;
    }

    public void setListSecteursActivites(
        final List<SecteurActiviteEnum> listSecteursActivites) {
        this.listSecteursActivites = listSecteursActivites;
    }

    public List<NiveauInterventionEnum> getListNiveauIntervention() {
        return this.listNiveauIntervention;
    }

    public void setListNiveauIntervention(
        final List<NiveauInterventionEnum> listNiveauIntervention) {
        this.listNiveauIntervention = listNiveauIntervention;
    }
}
