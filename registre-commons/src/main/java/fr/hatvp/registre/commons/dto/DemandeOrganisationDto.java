/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.util.Date;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import fr.hatvp.registre.commons.dto.backoffice.DemandeOrganisationLockBoDto;
import fr.hatvp.registre.commons.lists.ContextDemandeOrganisationEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;
import fr.hatvp.registre.commons.lists.backoffice.StatutDemandeOrganisationEnum;

/**
 * DTO de l'entité DemandeOrganisationEntity.
 *
 * @version $Revision$ $Date${0xD}
 */
public class DemandeOrganisationDto
    extends AbstractCommonDto
{

    /** Context. */
    private ContextDemandeOrganisationEnum context;

    /** Statut de traitement de la demande. */
    private StatutDemandeOrganisationEnum statut = StatutDemandeOrganisationEnum.A_TRAITER;

    /** Date de création de la demande. */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy à hh:mm")
    private Date dateCreation = new Date();

    /** Id Espace Organisation. */
    private Long espaceOrganisationId;

    /** Id declarant. */
    private Long declarantId;

    /** Nom complet declarant. */
    private String declarantFullname;

    /** Nom complet du dernier agent y accédant. */
    private String dernierAgentFullname;

    /** Dénomination Organisation. */
    private String denomination;

    /** Type Organisation. */
    private TypeOrganisationEnum type;

    /** Identifiant Organisation. */
    private String identifiant;

    /** Dirigeant Organisation. */
    private String dirigeant;

    /** Adresse Organisation. */
    private String adresse;

    /** Code postal Organisation. */
    @Size(max = 5)
    private String codePostal;

    /** Ville Organisation. */
    private String ville;

    /** Pays Organisation. */
    private String pays;

    /** Commentaire. */
    @Size(max = 500)
    private String commentaire;

    /** Objet Verrou sur l'objet */
    private DemandeOrganisationLockBoDto lock;

    /**
     * Gets context.
     *
     * @return the context
     */
    public ContextDemandeOrganisationEnum getContext()
    {
        return this.context;
    }

    /**
     * Sets context.
     *
     * @param context the context
     */
    public void setContext(final ContextDemandeOrganisationEnum context)
    {
        this.context = context;
    }

    /**
     * Gets espace organisation id.
     *
     * @return the espace organisation id
     */
    public Long getEspaceOrganisationId()
    {
        return this.espaceOrganisationId;
    }

    /**
     * Sets espace organisation id.
     *
     * @param espaceOrganisationId the espace organisation id
     */
    public void setEspaceOrganisationId(final Long espaceOrganisationId)
    {
        this.espaceOrganisationId = espaceOrganisationId;
    }

    /**
     * Gets declarant id.
     *
     * @return the declarant id
     */
    public Long getDeclarantId()
    {
        return this.declarantId;
    }

    /**
     * Sets declarant id.
     *
     * @param declarantId the declarant id
     */
    public void setDeclarantId(final Long declarantId)
    {
        this.declarantId = declarantId;
    }

    /**
     * Gets denomination.
     *
     * @return the denomination
     */
    public String getDenomination()
    {
        return this.denomination;
    }

    /**
     * Sets denomination.
     *
     * @param denomination the denomination
     */
    public void setDenomination(final String denomination)
    {
        this.denomination = denomination;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public TypeOrganisationEnum getType()
    {
        return this.type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(final TypeOrganisationEnum type)
    {
        this.type = type;
    }

    /**
     * Gets identifiant.
     *
     * @return the identifiant
     */
    public String getIdentifiant()
    {
        return this.identifiant;
    }

    /**
     * Sets identifiant.
     *
     * @param identifiant the identifiant
     */
    public void setIdentifiant(final String identifiant)
    {
        this.identifiant = identifiant;
    }

    /**
     * Gets dirigeant.
     *
     * @return the dirigeant
     */
    public String getDirigeant()
    {
        return this.dirigeant;
    }

    /**
     * Sets dirigeant.
     *
     * @param dirigeant the dirigeant
     */
    public void setDirigeant(final String dirigeant)
    {
        this.dirigeant = dirigeant;
    }

    /**
     * Gets adresse.
     *
     * @return the adresse
     */
    public String getAdresse()
    {
        return this.adresse;
    }

    /**
     * Sets adresse.
     *
     * @param adresse the adresse
     */
    public void setAdresse(final String adresse)
    {
        this.adresse = adresse;
    }

    /**
     * Gets code postal.
     *
     * @return the code postal
     */
    public String getCodePostal()
    {
        return this.codePostal;
    }

    /**
     * Sets code postal.
     *
     * @param codePostal the code postal
     */
    public void setCodePostal(final String codePostal)
    {
        this.codePostal = codePostal;
    }

    /**
     * Gets ville.
     *
     * @return the ville
     */
    public String getVille()
    {
        return this.ville;
    }

    /**
     * Sets ville.
     *
     * @param ville the ville
     */
    public void setVille(final String ville)
    {
        this.ville = ville;
    }

    /**
     * Gets pays.
     *
     * @return the pays
     */
    public String getPays()
    {
        return this.pays;
    }

    /**
     * Sets pays.
     *
     * @param pays the pays
     */
    public void setPays(final String pays)
    {
        this.pays = pays;
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire()
    {
        return this.commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param commentaire the commentaire
     */
    public void setCommentaire(final String commentaire)
    {
        this.commentaire = commentaire;
    }

    /**
     * Gets date creation.
     *
     * @return the date creation
     */
    public Date getDateCreation()
    {
        return this.dateCreation;
    }

    /**
     * Sets date creation.
     *
     * @param dateCreation the date creation
     */
    public void setDateCreation(final Date dateCreation)
    {
        this.dateCreation = dateCreation;
    }

    /**
     * Gets statut.
     *
     * @return the statut
     */
    public StatutDemandeOrganisationEnum getStatut()
    {
        return this.statut;
    }

    /**
     * Sets statut.
     *
     * @param statut the statut
     */
    public void setStatut(final StatutDemandeOrganisationEnum statut)
    {
        this.statut = statut;
    }

    /**
     * Gets declarant fullname.
     *
     * @return the declarant fullname
     */
    public String getDeclarantFullname()
    {
        return this.declarantFullname;
    }

    /**
     * Sets declarant fullname.
     *
     * @param declarantFullname the declarant fullname
     */
    public void setDeclarantFullname(final String declarantFullname)
    {
        this.declarantFullname = declarantFullname;
    }

    /**
     * Gets lock.
     *
     * @return the lock
     */
    public DemandeOrganisationLockBoDto getLock()
    {
        return this.lock;
    }

    /**
     * Sets lock.
     *
     * @param lock the lock
     */
    public void setLock(final DemandeOrganisationLockBoDto lock)
    {
        this.lock = lock;
    }

    /**
     * Gets dernier agent fullname.
     *
     * @return the dernier agent fullname
     */
    public String getDernierAgentFullname()
    {
        return this.dernierAgentFullname;
    }

    /**
     * Sets dernier agent fullname.
     *
     * @param dernierAgentFullname the dernier agent fullname
     */
    public void setDernierAgentFullname(final String dernierAgentFullname)
    {
        this.dernierAgentFullname = dernierAgentFullname;
    }
}
