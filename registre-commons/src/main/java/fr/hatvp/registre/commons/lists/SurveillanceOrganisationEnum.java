/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JsonNode;

import fr.hatvp.registre.commons.dto.EnumAsDto;

public enum SurveillanceOrganisationEnum {

	DEMANDE_INSCRIPTION("demande d'inscription", "démarche administrative"),
	COMPLEMENT_DEMANDE("réponse à une demande de complément", "démarche administrative"),
	DEMANDE_MANDAT("demande de changement de mandat de gestion", "démarche administrative"),
	COMPLEMENT_MANDAT("réponse à une demande de complément de changement de mandat de gestion", "démarche administrative"),
	DEMANDE_REFERENTIEL("demande de création d'une entité dans le référentiel des organisations", "démarche administrative"),

	AJOUT_COLLABORATEUR("ajout d'un collaborateur", "modification de contenu"),
	AJOUT_DIRIGEANT("ajout d'un dirigeant", "modification de contenu"),
	AJOUT_CLIENT("ajout d'un tiers", "modification de contenu"),
	AJOUTE_COMME_CLIENT("ajouté comme tiers", "modification de contenu"),
	AJOUT_SECTEUR_ACTIVITE("ajout d'un secteur activité", "modification de contenu"),
	PUBLICATION_IDENTITE("publication de l'identité", "modification de contenu"),
	CREATION_FICHE("création d'une fiche d’activités", "modification de contenu"),
	PUBLICATION_FICHE("communication d'une fiche d'activités", "modification de contenu"),
	DECLARATION_NULLE("communication d'une déclaration d'activité nulle", "modification de contenu"),
	PUBLICATION_MOYEN("communication de moyens alloués", "modification de contenu");

	private final String label;

	private final String categorie;

	private SurveillanceOrganisationEnum(String label, String categorie) {
		this.label = label;
		this.categorie = categorie;
	}

	public String getLabel() {
		return label;
	}

	public String getCategorie() {
		return categorie;
	}

	@JsonCreator
	public static SurveillanceOrganisationEnum fromNode(final JsonNode node) {
		if (!node.has("code")) {
			return null;
		}

		final String name = node.get("code").asText();

		return SurveillanceOrganisationEnum.valueOf(name);
	}

	@JsonValue
	public EnumAsDto getSurveillanceOrganisationDto() {
		return new EnumAsDto(this.name(), this.label, this.categorie);
	}

}
