/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JsonNode;

import fr.hatvp.registre.commons.dto.EnumAsDto;

/**
 * Classe d'énumération des principaux secteur d'activités de représentation d'interets.
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonFormat(shape = Shape.OBJECT)
public enum SecteurActiviteEnum {

    ETRANGER("Affaires étrangères, coopération internationale", 1),
    AGRI("Agriculture, alimentation, pêche", 2),
    CULTURE("Arts, patrimoine culturel", 3),
    FINANCE("Banques, assurances, secteur financier", 4),
    COMMERCE("Commerce extérieur", 5),
    CONSO("Concurrence, consommation", 6),
    SECURITE("Défense, sécurité", 7),
    ECONOMIE("Economie", 8),
    EDUCATION("Education, enseignement supérieur, formation", 9),
    EMPLOI("Travail, emploi, solidarité", 10),
    ENERGIE("Energie", 11),
    RECHERCHE("Recherche, innovation", 12),
    FINANCES("Fiscalité, finances publiques", 13),
    ENTREPRISE("Gouvernance d’entreprise", 14),
    ENVIRONNEMENT("Environnement", 15),
    JUSTICE("Justice", 16),
    AMENAGEMENT("Logement, aménagement du territoire, urbanisme", 17),
    MEDIA("Médias, industries culturelles", 18),
    NUMERIQUE("Numérique", 19),
    OUTREMER("Outre-mer", 20),
    PUBLIC("Pouvoirs publics, institutions, fonction publique", 21),
    PROREG("Professions réglementées", 22),
    PROPINT("Propriété intellectuelle", 23),
    MIGRA("Questions migratoires", 24),
    SANTE("Santé, sécurité sociale", 25),
    SOCIETE("Questions de société", 26),
    SPORTS("Sports, loisirs, tourisme", 27),
    COM("Communications électroniques", 28),
    TRANSPORTS("Transports, logistique", 29);

    private final String label;
    private final int ordre;

    SecteurActiviteEnum(final String label, final int ordre)
    {
        this.label = label;
        this.ordre = ordre;
    }

    @JsonCreator
    public static SecteurActiviteEnum fromNode(final JsonNode node)
    {
        if (!node.has("code")) {
            return null;
        }

        final String name = node.get("code").asText();

        return SecteurActiviteEnum.valueOf(name);
    }

    @JsonValue
    public EnumAsDto getNiveauInterventionDto()
    {
        return new EnumAsDto(this.name(), this.label, this.ordre);
    }
}