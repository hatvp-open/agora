/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;

/**
 * DTO de l'entité Publication, Meta données.
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PublicationMetaDto
    extends AbstractCommonDto
{
    /** Date de création. */
    @JsonFormat(pattern = "dd/MM/yyyy à HH:mm:ss", timezone = "Europe/Paris")
    private Date publicationDate;

    /** référence au publisher. */
    private String publicateurNom;

    /** Date de dépublication. */
    @JsonFormat(pattern = "dd/MM/yyyy à HH:mm:ss", timezone = "Europe/Paris")
    private Date depublicationDate;

    /** nom complet dépublicateur. */
    private String depublicateurNom;

    /** Date de republication. */
    @JsonFormat(pattern = "dd/MM/yyyy à HH:mm:ss", timezone = "Europe/Paris")
    private Date republicationDate;

    /** nom complet republicateur. */
    private String republicateurNom;

    /** Statut de la publication. */
    private StatutPublicationEnum statut;

    /** Id de l'espace orgaisation */
    private Long espaceOrganisationId;

    /** Données publication. */
    private PublicationFrontDto publication;

    /**
     * Gets publication date.
     *
     * @return the publication date
     */
    public Date getPublicationDate()
    {
        return this.publicationDate;
    }

    /**
     * Sets publication date.
     *
     * @param publicationDate the publication date
     */
    public void setPublicationDate(final Date publicationDate)
    {
        this.publicationDate = publicationDate;
    }

    /**
     * Gets publicateur nom.
     *
     * @return the publicateur nom
     */
    public String getPublicateurNom()
    {
        return this.publicateurNom;
    }

    /**
     * Sets publicateur nom.
     *
     * @param publicateurNom the publicateur nom
     */
    public void setPublicateurNom(final String publicateurNom)
    {
        this.publicateurNom = publicateurNom;
    }

    /**
     * Gets depublication date.
     *
     * @return the depublication date
     */
    public Date getDepublicationDate()
    {
        return this.depublicationDate;
    }

    /**
     * Sets depublication date.
     *
     * @param depublicationDate the depublication date
     */
    public void setDepublicationDate(final Date depublicationDate)
    {
        this.depublicationDate = depublicationDate;
    }

    /**
     * Gets depublicateur nom.
     *
     * @return the depublicateur nom
     */
    public String getDepublicateurNom()
    {
        return this.depublicateurNom;
    }

    /**
     * Sets depublicateur nom.
     *
     * @param depublicateurNom the depublicateur nom
     */
    public void setDepublicateurNom(final String depublicateurNom)
    {
        this.depublicateurNom = depublicateurNom;
    }

    /**
     * Gets republication date.
     *
     * @return the republication date
     */
    public Date getRepublicationDate()
    {
        return this.republicationDate;
    }

    /**
     * Sets republication date.
     *
     * @param republicationDate the republication date
     */
    public void setRepublicationDate(final Date republicationDate)
    {
        this.republicationDate = republicationDate;
    }

    /**
     * Gets republicateur nom.
     *
     * @return the republicateur nom
     */
    public String getRepublicateurNom()
    {
        return this.republicateurNom;
    }

    /**
     * Sets republicateur nom.
     *
     * @param republicateurNom the republicateur nom
     */
    public void setRepublicateurNom(final String republicateurNom)
    {
        this.republicateurNom = republicateurNom;
    }

    /**
     * Gets statut.
     *
     * @return the statut
     */
    public StatutPublicationEnum getStatut()
    {
        return this.statut;
    }

    /**
     * Sets statut.
     *
     * @param statut the statut
     */
    public void setStatut(final StatutPublicationEnum statut)
    {
        this.statut = statut;
    }

    /**
     * Gets espace organisation id.
     *
     * @return the espace organisation id
     */
    public Long getEspaceOrganisationId()
    {
        return this.espaceOrganisationId;
    }

    /**
     * Sets espace organisation id.
     *
     * @param espaceOrganisationId the espace organisation id
     */
    public void setEspaceOrganisationId(final Long espaceOrganisationId)
    {
        this.espaceOrganisationId = espaceOrganisationId;
    }

    /**
     * Gets publication.
     *
     * @return the publication
     */
    public PublicationFrontDto getPublication()
    {
        return this.publication;
    }

    /**
     * Sets publication.
     *
     * @param publication the publication
     */
    public void setPublication(final PublicationFrontDto publication)
    {
        this.publication = publication;
    }
}
