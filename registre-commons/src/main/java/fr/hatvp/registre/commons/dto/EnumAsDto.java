/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

/**
 * Classe de représentation des enums sous forme de dto. Utilisé pour la personnalisations de la sérialisation.
 *
 * @version $Revision$ $Date${0xD}
 */
public class EnumAsDto {

	private final String code;
	private final String label;
	private String categorie;
	private int ordre;

	public EnumAsDto(final String code, final String label, final int ordre) {
		this.code = code;
		this.label = label;
		this.ordre = ordre;
	}

	public EnumAsDto(final String code, final String label, final String categorie) {
		this.code = code;
		this.label = label;
		this.categorie = categorie;
	}

	public EnumAsDto(final String code, final String label) {
		this.code = code;
		this.label = label;
	}

	public String getCode() {
		return this.code;
	}

	public String getLabel() {
		return this.label;
	}

	public int getOrdre() {
		return this.ordre;
	}

	public String getCategorie() {
		return categorie;
	}

}
