/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat; 

/**
 * DTO de recherche des publications.
 *
 * @version $Revision$ $Date${0xD}
 */
public class PublicationRechercheBlacklistDto
    extends PublicationRechercheDto {


    private String exerciceBlackliste;
 
    private String infosNonDeclarees;


    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "Europe/Paris")
    private Date echeance;


	public String getExerciceBlackliste() {
		return exerciceBlackliste;
	}


	public void setExerciceBlackliste(String exerciceBlackliste) {
		this.exerciceBlackliste = exerciceBlackliste;
	}


	public String getInfosNonDeclarees() {
		return infosNonDeclarees;
	}


	public void setInfosNonDeclarees(String infosNonDeclarees) {
		this.infosNonDeclarees = infosNonDeclarees;
	}


	public Date getEcheance() {
		return echeance;
	}


	public void setEcheance(Date echeance) {
		this.echeance = echeance;
	}







}
