/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

/**
 * 
 * Classe d'énumération des types d'identifiants national;
 * @version $Revision$ $Date${0xD}
 */
public enum TypeIdentifiantNationalEnum {
    SIREN, RNA, HATVP, DUNS
}
