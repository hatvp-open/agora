/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.utils;

import java.io.IOException;

import fr.hatvp.registre.commons.dto.DesinscriptionPieceDto;
import fr.hatvp.registre.commons.dto.EspaceDeclarantPieceDto;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;
import org.springframework.web.multipart.MultipartFile;

import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.exceptions.GlobalServerException;
import fr.hatvp.registre.commons.exceptions.TechnicalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;

/**
 * Regroupe les méthodes utiles pour traiter les pièces chargées dans l'application.
 */
public class PiecesUtils {

    private static final String[] allowedFileExtensions = {"image/jpg", "image/jpeg", "image/png", "application/pdf", "application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document"};

    /**
     * Valide que les critères exigés pour le fichier soumis dans l'API sont respectés (taille et type).
     */
    public static void checkConformitePiece(MultipartFile piece)
    {

        if(piece == null) {
            return;
        }

        // Vérification du fichier
        // 1. ContentType
        // 2. Extension dans le nom de fichier
        // 3. Correspondance avec l'extension calculé par le MagicNumber (premiers caractères de l'entête fichier)
        if( !StringUtils.containsAny(piece.getContentType(), PiecesUtils.allowedFileExtensions) ||
            !PiecesUtils.checkFileExtensionAndType(piece, PiecesUtils.allowedFileExtensions)){
            throw new BusinessGlobalException(ErrorMessageEnum.EXTENSION_INCORRECTE.getMessage());
        }

        // Vérification de la taille
        if( piece.getSize() > FileUtils.ONE_MB * 5 ){
            throw new BusinessGlobalException(ErrorMessageEnum.LE_FICHIER_DEPASSE_LA_TAILLE_AUTORISEE.getMessage(), 413);
        }
    }

    /**
     * Vérifie que le fichier passé en paramètre correspond aux mime types autorisés dans l'application.
     * Vérification faite par Tika qui va lire le fichier pour en déterminer le type.
     * 
     * @param file le fichier à vérifier.
     * @param allowedType la liste des mime types autorisés
     * @return vrai si le fichier est autorisé, faux sinon.
     */
    public static boolean checkFileExtensionAndType(MultipartFile file, String[] allowedType)
    {
        final Tika tika = new Tika();

        try {
            // Vérification des extensions case insensitive
            if(!StringUtils.containsAny(tika.detect(file.getOriginalFilename()), allowedType)) {
                return false;
            }
            // Vérification du contenu binaire
            return StringUtils.containsAny(tika.detect(file.getBytes()), allowedType);
        } catch(IOException e){
            throw new TechnicalException(ErrorMessageEnum.ERREUR_FICHIER.getMessage(), e);
        }
    }
    
    /**
     * Transfère le contenu d'un MultipartFile vers un EspaceOrganisationPieceDto 
     * @param file fichier a mapper
     * @return un EspaceOrganisationPieceDto pour la couche service de l'application.
     * @throws IOException 
     */
    public static EspaceOrganisationPieceDto mapToDto(final MultipartFile file) {
        if(file == null) {
            return null;
        }

        EspaceOrganisationPieceDto espaceOrganisationPieceDto = new EspaceOrganisationPieceDto();
        try {
            espaceOrganisationPieceDto.setContenuFichier(file.getBytes());
            espaceOrganisationPieceDto.setMediaType(file.getContentType());
            espaceOrganisationPieceDto.setNomFichier(file.getOriginalFilename());
        } catch (IOException e) {
            throw new GlobalServerException(ErrorMessageEnum.ENREGISTREMENT_FICHIER_ERREUR.getMessage(), e); 
        }
        
        return espaceOrganisationPieceDto;
    }

    /**
     * Transfère le contenu d'un MultipartFile vers un EspaceDeclarantPieceDto
     * @param file fichier a mapper
     * @return un EspaceDeclarantPieceDto pour la couche service de l'application.
     * @throws IOException
     */
    public static EspaceDeclarantPieceDto mapToEspaceDeclarantPieceDto(final MultipartFile file) {
        if(file == null) {
            return null;
        }

        EspaceDeclarantPieceDto espaceDeclarantPieceDto = new EspaceDeclarantPieceDto();
        try {
            espaceDeclarantPieceDto.setContenuFichier(file.getBytes());
            espaceDeclarantPieceDto.setMediaType(file.getContentType());
            espaceDeclarantPieceDto.setNomFichier(file.getOriginalFilename());
        } catch (IOException e) {
            throw new GlobalServerException(ErrorMessageEnum.ENREGISTREMENT_FICHIER_ERREUR.getMessage(), e);
        }

        return espaceDeclarantPieceDto;
    }

    /**
     * Transfère le contenu d'un MultipartFile vers un EspaceOrganisationPieceDto
     * @param file fichier a mapper
     * @return un EspaceOrganisationPieceDto pour la couche service de l'application.
     * @throws IOException
     */
    public static DesinscriptionPieceDto mapToDesinscriptionDto(final MultipartFile file) {
        if(file == null) {
            return null;
        }

        DesinscriptionPieceDto desinscriptionPieceDto = new DesinscriptionPieceDto();
        try {
            desinscriptionPieceDto.setContenuFichier(file.getBytes());
            desinscriptionPieceDto.setMediaType(file.getContentType());
            desinscriptionPieceDto.setNomFichier(file.getOriginalFilename());
        } catch (IOException e) {
            throw new GlobalServerException(ErrorMessageEnum.ENREGISTREMENT_FICHIER_ERREUR.getMessage(), e);
        }

        return desinscriptionPieceDto;
    }
}
