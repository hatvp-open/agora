/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite;

import java.util.List;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActiviteHistoriqueDto;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;

public class ActiviteRepresentationInteretSimpleDto extends AbstractCommonDto {

	private String idFiche;

	private String objet;

	private StatutPublicationEnum statut;

	private List<String> domainesIntervention;

	private String nomCreateur;

	private Long exerciceComptable;

	private String dateFirstComm;

    private String dateLastChange;
    
    private String dateCreation;
    
    private List<PublicationActiviteHistoriqueDto> activiteHistoriquePublicationList;


	/**
	 * @return the idFiche
	 */
	public String getIdFiche() {
		return idFiche;
	}

	/**
	 * @param idFiche
	 *            the idFiche to set
	 */
	public void setIdFiche(String idFiche) {
		this.idFiche = idFiche;
	}

	/**
	 * @return the objet
	 */
	public String getObjet() {
		return objet;
	}

	/**
	 * @param objet
	 *            the objet to set
	 */
	public void setObjet(String objet) {
		this.objet = objet;
	}

	/**
	 * @return the domainesIntervention
	 */
	public List<String> getDomainesIntervention() {
		return domainesIntervention;
	}

	/**
	 * @param domainesIntervention
	 *            the domainesIntervention to set
	 */
	public void setDomainesIntervention(List<String> domainesIntervention) {
		this.domainesIntervention = domainesIntervention;
	}

	/**
	 * @return the nomCreateur
	 */
	public String getNomCreateur() {
		return nomCreateur;
	}

	/**
	 * @param nomCreateur
	 *            the nomCreateur to set
	 */
	public void setNomCreateur(String nomCreateur) {
		this.nomCreateur = nomCreateur;
	}

	/**
	 * @return the exerciceComptable
	 */
	public Long getExerciceComptable() {
		return exerciceComptable;
	}

	/**
	 * @param exerciceComptable
	 *            the exerciceComptable to set
	 */
	public void setExerciceComptable(Long exerciceComptable) {
		this.exerciceComptable = exerciceComptable;
	}

	/**
	 *
	 * @return statut
	 */
	public StatutPublicationEnum getStatut() {
		return statut;
	}

	/**
	 *
	 * @param statut
	 */
	public void setStatut(StatutPublicationEnum statut) {
		this.statut = statut;
	}

    public String getDateFirstComm() {
        return dateFirstComm;
    }

    public void setDateFirstComm(String dateFirstComm) {
        this.dateFirstComm = dateFirstComm;
    }

    public String getDateLastChange() {
        return dateLastChange;
    }

    public void setDateLastChange(String dateLastChange) {
        this.dateLastChange = dateLastChange;
    }

	public String getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}

	public List<PublicationActiviteHistoriqueDto> getActiviteHistoriquePublicationList() {
		return activiteHistoriquePublicationList;
	}

	public void setActiviteHistoriquePublicationList(
			List<PublicationActiviteHistoriqueDto> activiteHistoriquePublicationList) {
		this.activiteHistoriquePublicationList = activiteHistoriquePublicationList;
	}
    
    
}
