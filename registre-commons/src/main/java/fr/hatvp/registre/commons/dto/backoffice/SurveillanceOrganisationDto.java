/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;

import java.util.List;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;

public class SurveillanceOrganisationDto extends AbstractCommonDto {
	private String idNational;

	private List<SurveillanceOrganisationEnum> typeSurveillance;

	private List<Long> surveillantUsers;
	private List<Long> surveillantGroups;
	private String motif;

	public SurveillanceOrganisationDto() {
	}

	public SurveillanceOrganisationDto(Long id, List<SurveillanceOrganisationEnum> typeSurveillance, List<Long> surveillant,List<Long> groupes, String motif) {
		this.id = id;
		this.typeSurveillance = typeSurveillance;
		this.surveillantUsers = surveillant;
		this.surveillantGroups = groupes;
		this.motif = motif;
	}

	/**
	 * @return the idNational
	 */
	public String getIdNational() {
		return idNational;
	}

	/**
	 * @param idNational
	 *            the idNational to set
	 */
	public void setIdNational(String idNational) {
		this.idNational = idNational;
	}

	/**
	 * @return the typeSurveillance
	 */
	public List<SurveillanceOrganisationEnum> getTypeSurveillance() {
		return typeSurveillance;
	}

	/**
	 * @param typeSurveillance
	 *            the typeSurveillance to set
	 */
	public void setTypeSurveillance(List<SurveillanceOrganisationEnum> typeSurveillance) {
		this.typeSurveillance = typeSurveillance;
	}

	/**
	 * @return the surveillant
	 */
	public List<Long> getSurveillantUsers() {
		return surveillantUsers;
	}

	/**
	 * @param surveillant
	 *            the surveillant to set
	 */
	public void setSurveillantUsers(List<Long> surveillant) {
		this.surveillantUsers = surveillant;
	}

	/**
	 * @return the groupesSurveillant
	 */
	public List<Long> getSurveillantGroups() {
		return surveillantGroups;
	}

	/**
	 * @param groupesSurveillant the groupesSurveillant to set
	 */
	public void setSurveillantGroups(List<Long> groupesSurveillant) {
		this.surveillantGroups = groupesSurveillant;
	}

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }
}
