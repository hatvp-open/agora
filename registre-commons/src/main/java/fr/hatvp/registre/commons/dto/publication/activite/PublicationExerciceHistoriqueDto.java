/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication.activite;

import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.hatvp.registre.commons.dto.publication.activite.common.PublicationMetaDto;
import fr.hatvp.registre.commons.utils.LocalDateDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateSerializer;

/**
    * DTO de publication de l'entité ExerciceComptable
    *
    * @version $Revision$ $Date${0xD}
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PublicationExerciceHistoriqueDto extends PublicationMetaDto
{
	@JsonProperty("dateCreation")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
    private Date creationDate;
	
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateDebut;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateFin;

    private String chiffreAffaire;

    private Boolean hasNotChiffreAffaire;

    private String montantDepense;

    private Integer nombreSalaries;

    private Long exerciceId;

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public String getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(String chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	public Boolean getHasNotChiffreAffaire() {
		return hasNotChiffreAffaire;
	}

	public void setHasNotChiffreAffaire(Boolean hasNotChiffreAffaire) {
		this.hasNotChiffreAffaire = hasNotChiffreAffaire;
	}

	public String getMontantDepense() {
		return montantDepense;
	}

	public void setMontantDepense(String montantDepense) {
		this.montantDepense = montantDepense;
	}

	public Integer getNombreSalaries() {
		return nombreSalaries;
	}

	public void setNombreSalaries(Integer nombreSalaries) {
		this.nombreSalaries = nombreSalaries;
	}

	public Long getExerciceId() {
		return exerciceId;
	}

	public void setExerciceId(Long exerciceId) {
		this.exerciceId = exerciceId;
	}
	
}
