/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.validators.civility;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import fr.hatvp.registre.commons.lists.CiviliteEnum;

/**
 * 
 * Classe de validdation de la civilité.
 * 
 * @version $Revision$ $Date${0xD}
 */
public class CivilityValidator
    implements ConstraintValidator<Civility, CiviliteEnum>
{

    /** Liste des civilités authorisées. */
    private CiviliteEnum[] allowedTypes;

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(final Civility constraintAnnotation)
    {
        this.allowedTypes = constraintAnnotation.value();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid(final CiviliteEnum value, final ConstraintValidatorContext context)
    {

        if (value == null) {
            return true;
        }

        for (final CiviliteEnum type : this.allowedTypes) {

            if (type.name().equals(value.name())) {
                return true;
            }

        }

        return false;
    }
}
