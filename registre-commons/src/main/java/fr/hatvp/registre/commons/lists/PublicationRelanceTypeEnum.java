/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

public enum PublicationRelanceTypeEnum {

	FA("fiche d'activités"), MA("moyen alloués"), FAMA("fiche d'activités et moyen alloués");

	private String label;

	private PublicationRelanceTypeEnum(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

}
