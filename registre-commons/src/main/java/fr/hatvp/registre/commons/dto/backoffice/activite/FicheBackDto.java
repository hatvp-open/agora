/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice.activite;

import java.util.List;
import java.util.Map;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

public class FicheBackDto extends AbstractCommonDto{

	private String idFiche;
	private String objet;

	private Map<String, List<String>> domaines;

	private String statut;
	private Boolean isPublication;

	private List<ActionBackDto> actions;

	/**
	 * @return the idFiche
	 */
	public String getIdFiche() {
		return idFiche;
	}

	/**
	 * @param idFiche
	 *            the idFiche to set
	 */
	public void setIdFiche(String idFiche) {
		this.idFiche = idFiche;
	}

	/**
	 * @return the objet
	 */
	public String getObjet() {
		return objet;
	}

	/**
	 * @param objet
	 *            the objet to set
	 */
	public void setObjet(String objet) {
		this.objet = objet;
	}

	/**
	 * @return the domaines
	 */
	public Map<String, List<String>> getDomaines() {
		return domaines;
	}

	/**
	 * @param domaines
	 *            the domaines to set
	 */
	public void setDomaines(Map<String, List<String>> domaines) {
		this.domaines = domaines;
	}

	/**
	 * @return the statut
	 */
	public String getStatut() {
		return statut;
	}

	/**
	 * @param statut
	 *            the statut to set
	 */
	public void setStatut(String statut) {
		this.statut = statut;
	}

	/**
	 * @return the actions
	 */
	public List<ActionBackDto> getActions() {
		return actions;
	}

	/**
	 * @param actions
	 *            the actions to set
	 */
	public void setActions(List<ActionBackDto> actions) {
		this.actions = actions;
	}
	
	/**
	 * @return the isPublication
	 */
	public Boolean getIsPublication() {
		return isPublication;
	}

	/**
	 * @param isPublication
	 *            the isPublication to set
	 */
	public void setIsPublication(Boolean isPublication) {
		this.isPublication = isPublication;
	}

}
