/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * DTO de l'entité OrganisationEntity.
 */
public class OrganisationDto
        extends AbstractCommonDto {

    /** Identifiant National */
    @Size(max = 10)
    private String nationalId;

    /** Identifiant SIREN. */
    @Size(max = 9)
    private String siren;

    /** Identifiant DUNS. */
    @Size(max = 9)
    private String dunsNumber;

    /** Identifiant RNA. */
    @Size(max = 10)
    private String rna;

    /** Identifiant HATVP. */
    @Size(max = 10)
    private String hatvpNumber;

    /** Type identifiant national (SIREN/RNA) */
    private TypeIdentifiantNationalEnum originNationalId;

    /** Denomination de l'organisation */
    @NotBlank
    @Size(max = 150)
    private String denomination;

    /** Nom d'usage de l'organisation */
    @Size(max = 150)
    private String nomUsageSiren;

    /** Adresse organisation */
    @Size(max = 500)
    private String adresse;

    /** Code Postal organisation */
    @Size(max = 10)
    private String codePostal;

    /** Ville organisation */
    @Size(max = 150)
    private String ville;

    /** Pays organisation */
    @Size(max = 150)
    private String pays;

    /** Si l'espace collaboratif existe pour l'organisation. */
    private Boolean organizationSpaceExist;

    /** Si l'organisation existe dans la table organisation. */
    private Boolean organisationExist;

    /** Adresse du site Internet déclarée par l'association. */
    private String siteWeb;

    /** Date de création de la demande. */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy à HH:mm:ss")
    private Date creationDate = new Date();

    /**
     * Accesseur en lecture du champ <code>nationalId</code>.
     *
     * @return le champ <code>nationalId</code>.
     */
    public String getNationalId() {
        return this.nationalId;
    }

    /**
     * Accesseur en écriture du champ <code>nationalId</code>.
     *
     * @param nationalId la valeur à écrire dans <code>nationalId</code>.
     */
    public void setNationalId(final String nationalId) {
        this.nationalId = nationalId;
    }

    /**
     * Accesseur en lecture du champ <code>originNationalId</code>.
     *
     * @return le champ <code>originNationalId</code>.
     */
    public TypeIdentifiantNationalEnum getOriginNationalId() {
        return this.originNationalId;
    }

    /**
     * Accesseur en écriture du champ <code>originNationalId</code>.
     *
     * @param originNationalId la valeur à écrire dans <code>originNationalId</code>.
     */
    public void setOriginNationalId(final TypeIdentifiantNationalEnum originNationalId) {
        this.originNationalId = originNationalId;
    }

    /**
     * Accesseur en lecture du champ <code>denomination</code>.
     *
     * @return le champ <code>denomination</code>.
     */
    public String getDenomination() {
        return this.denomination;
    }

    /**
     * Accesseur en écriture du champ <code>denomination</code>.
     *
     * @param denomination la valeur à écrire dans <code>denomination</code>.
     */
    public void setDenomination(final String denomination) {
        this.denomination = denomination;
    }

    /**
     * Accesseur en lecture du champ <code>nomUsageSiren</code>.
     *
     * @return le champ <code>nomUsageSiren</code>.
     */
    public String getNomUsageSiren() {
        return this.nomUsageSiren;
    }

    public void setNomUsageSiren(final String nomUsageSiren) {
        this.nomUsageSiren = nomUsageSiren;
    }

    /**
     * Accesseur en lecture du champ <code>adresse</code>.
     *
     * @return le champ <code>adresse</code>.
     */
    public String getAdresse() {
        return this.adresse;
    }

    /**
     * Accesseur en écriture du champ <code>adresse</code>.
     *
     * @param adresse la valeur à écrire dans <code>adresse</code>.
     */
    public void setAdresse(final String adresse) {
        this.adresse = adresse;
    }

    /**
     * Accesseur en lecture du champ <code>codePostal</code>.
     *
     * @return le champ <code>codePostal</code>.
     */
    public String getCodePostal() {
        return this.codePostal;
    }

    /**
     * Accesseur en écriture du champ <code>codePostal</code>.
     *
     * @param codePostal la valeur à écrire dans <code>codePostal</code>.
     */
    public void setCodePostal(final String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * Accesseur en lecture du champ <code>ville</code>.
     *
     * @return le champ <code>ville</code>.
     */
    public String getVille() {
        return this.ville;
    }

    /**
     * Accesseur en écriture du champ <code>ville</code>.
     *
     * @param ville la valeur à écrire dans <code>ville</code>.
     */
    public void setVille(final String ville) {
        this.ville = ville;
    }

    /**
     * Accesseur en lecture du champ <code>pays</code>.
     *
     * @return le champ <code>pays</code>.
     */
    public String getPays() {
        return this.pays;
    }

    /**
     * Accesseur en écriture du champ <code>pays</code>.
     *
     * @param pays la valeur à écrire dans <code>pays</code>.
     */
    public void setPays(final String pays) {
        this.pays = pays;
    }

    /**
     * Accesseur en lecture du champ <code>organizationSpaceExist</code>.
     *
     * @return le champ <code>organizationSpaceExist</code>.
     */
    public Boolean getOrganizationSpaceExist() {
        return this.organizationSpaceExist;
    }

    /**
     * Accesseur en écriture du champ <code>organizationSpaceExist</code>.
     *
     * @param organizationSpaceExist la valeur à écrire dans <code>organizationSpaceExist</code>.
     */
    public void setOrganizationSpaceExist(final Boolean organizationSpaceExist) {
        this.organizationSpaceExist = organizationSpaceExist;
    }

    /**
     * Accesseur en lecture du champ <code>siren</code>.
     *
     * @return le champ <code>siren</code>.
     */
    public String getSiren() {
        return this.siren;
    }

    /**
     * Accesseur en écriture du champ <code>siren</code>.
     *
     * @param siren la valeur à écrire dans <code>siren</code>.
     */
    public void setSiren(final String siren) {
        this.siren = siren;
    }

    /**
     * Accesseur en lecture du champ <code>dunsNumber</code>.
     *
     * @return le champ <code>dunsNumber</code>.
     */
    public String getDunsNumber() {
        return this.dunsNumber;
    }

    /**
     * Accesseur en écriture du champ <code>dunsNumber</code>.
     *
     * @param dunsNumber la valeur à écrire dans <code>dunsNumber</code>.
     */
    public void setDunsNumber(final String dunsNumber) {
        this.dunsNumber = dunsNumber;
    }

    /**
     * Accesseur en lecture du champ <code>rna</code>.
     *
     * @return le champ <code>rna</code>.
     */
    public String getRna() {
        return this.rna;
    }

    /**
     * Accesseur en écriture du champ <code>rna</code>.
     *
     * @param rna la valeur à écrire dans <code>rna</code>.
     */
    public void setRna(final String rna) {
        this.rna = rna;
    }

    /**
     * Accesseur en lecture du champ <code>hatvpNumber</code>.
     *
     * @return le champ <code>hatvpNumber</code>.
     */
    public String getHatvpNumber() {
        return this.hatvpNumber;
    }

    /**
     * Accesseur en écriture du champ <code>hatvpNumber</code>.
     *
     * @param hatvpNumber la valeur à écrire dans <code>hatvpNumber</code>.
     */
    public void setHatvpNumber(final String hatvpNumber) {
        this.hatvpNumber = hatvpNumber;
    }

    /**
     * Accesseur en lecture du champ <code>organisationExist</code>.
     *
     * @return le champ <code>organisationExist</code>.
     */
    public Boolean getOrganisationExist() {
        return this.organisationExist;
    }

    /**
     * Accesseur en écriture du champ <code>organisationExist</code>.
     *
     * @param organisationExist la valeur à écrire dans <code>organisationExist</code>.
     */
    public void setOrganisationExist(final Boolean organisationExist) {
        this.organisationExist = organisationExist;
    }

    /**
     * Accesseur en lecture du champ <code>siteWeb</code>.
     *
     * @return le champ <code>siteWeb</code>.
     */
    public String getSiteWeb() {
        return this.siteWeb;
    }

    /**
     * Accesseur en écriture du champ <code>siteWeb</code>.
     *
     * @param siteWeb la valeur à écrire dans <code>siteWeb</code>.
     */
    public void setSiteWeb(final String siteWeb) {
        this.siteWeb = siteWeb;
    }

    /**
     * Gets creation date.
     *
     * @return the creation date
     */
    public Date getCreationDate() {
        return this.creationDate;
    }

    /**
     * Sets creation date.
     *
     * @param creationDate the creation date
     */
    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }
}
