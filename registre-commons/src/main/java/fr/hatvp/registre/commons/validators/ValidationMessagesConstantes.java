/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.validators;

/**
 *
 * Classe de constants pour les messages d'erreurs de validation de l'API.
 *
 * @version $Revision$ $Date${0xD}
 */
public class ValidationMessagesConstantes
{

    public final static String NOM_OBLIGATOIRE = "Nom obligatoire.";
    public final static String PRENOM_OBLIGATOIRE = "Prénom obligatoire.";
    public final static String CIVILITE_OBLIGATOIRE = "Civilité obligatoire.";
    public final static String EMAIL_OBLIGATOIRE = "Courriel obligatoire.";
    public final static String EMAIL_INCORRECTE = "Courriel incorrect.";
    public final static String DATE_NAISSANCE_INVALIDE = "Date de naissance invalide.";
    public final static String TELEPHONE_OBLIGATOIRE = "Numéro de téléphone obligatoire.";
    public final static String CATEGORIE_INCORRECTE = "La catégorie doit être 'PERSO' ou 'PRO'.";
    public final static String CIVILITE_INCORRECTE = "La civilité doit être 'M' ou 'MME'.";
    public final static String MOT_DE_PASSE_OBLIGATOIRE = "Mot de passe obligatoire.";
    public final static String MOT_DE_PASSE_INCORRECTE = "Le mot de passe doit contenir au moins 10 caractères dont chiffres et lettres.";

    public final static String ADRRESSE_EMAIL_INTROUVABLE = "Adrresse courriel introuvable.";
    public final static String LE_CHAMP_PREV_PASSWORD_N_EST_PAS_AUTORISE = "Le champ 'prev_password' n'est pas autorisé dans ce contexte car l'utilisateur n'est pas connnecté.";
    public final static String TOKEN_DE_VALIDATION_INTROUVABLE = "Token de validation introuvable.";
    public final static String MOT_DE_PASSE_INCORRECT = "Mot de passe incorrect.";

    public final static String LE_FORMAT_DU_MAIL_N_EST_PAS_VALIDE = "Le format de l'adresse courriel n'est pas valide.";
    public final static String LE_PATTERN_DU_MOT_DE_PASSE_INVALIDE = "Le mot de passe doit contenir au moins 10 caractères dont chiffres et lettres.";
    public final static String CATEGORY_INCORRECT = "Catégorie incorrecte.";
    public final static String ANCIEN_MOT_DE_PASSE_INCORRECT = "Ancien mot de passe incorrect.";

    public final static String ERREUR_DE_DONNEES_DECLARANT = "Erreur de données 'Declarant'.";
    public final static String ERREUR_DE_DONNEES_ESPACE_ORGANISATION = "Erreur de données 'espace collaboratif'.";

    public final static String MSG_TAILLE_LIMITE_FICHIER_LOGO = "Le logo chargé dépasse la taille maximale autorisée.";
    public final static String MSG_EXTENSIONS_ACCEPTEES = "Les extensions autorisées sont : PNG, JPEG et JPG.";

}
