/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite.batch;

import java.time.LocalDateTime;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.PublicationRelanceNiveauEnum;
import fr.hatvp.registre.commons.lists.PublicationRelanceTypeEnum;

public class PublicationRelanceDto extends AbstractCommonDto {

	public PublicationRelanceDto() {
	}

	public PublicationRelanceDto(Long exerciceComptableId, Long contactOpId, PublicationRelanceNiveauEnum niveau, PublicationRelanceTypeEnum type, boolean envoye) {
		this.exerciceComptableId = exerciceComptableId;
		this.contactOpId = contactOpId;
		this.niveau = niveau;
		this.type = type;
		this.envoye = envoye;
	}

	private LocalDateTime creationDate;
	
	private Long exerciceComptableId;

	private Long contactOpId;

	private PublicationRelanceNiveauEnum niveau;

	private PublicationRelanceTypeEnum type;

	private boolean envoye;

	public Long getExerciceComptableId() {
		return exerciceComptableId;
	}

	public void setExerciceComptableId(Long exerciceComptableId) {
		this.exerciceComptableId = exerciceComptableId;
	}

	public Long getContactOpId() {
		return contactOpId;
	}

	public void setContactOpId(Long contactOpId) {
		this.contactOpId = contactOpId;
	}

	public PublicationRelanceNiveauEnum getNiveau() {
		return niveau;
	}

	public void setNiveau(PublicationRelanceNiveauEnum niveau) {
		this.niveau = niveau;
	}

	public PublicationRelanceTypeEnum getType() {
		return type;
	}

	public void setType(PublicationRelanceTypeEnum type) {
		this.type = type;
	}

	public boolean isEnvoye() {
		return envoye;
	}

	public void setEnvoye(boolean envoye) {
		this.envoye = envoye;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	
}
