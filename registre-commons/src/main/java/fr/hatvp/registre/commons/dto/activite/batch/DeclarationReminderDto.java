/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite.batch;

import java.time.LocalDate;
import java.util.List;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.PublicationRelanceNiveauEnum;
import fr.hatvp.registre.commons.lists.PublicationRelanceTypeEnum;

public class DeclarationReminderDto extends AbstractCommonDto {

	private boolean aucunePublicationMoyens = false;

	private boolean aucunePublicationActivites = false;

	private PublicationRelanceNiveauEnum niveauRelance;

	private PublicationRelanceTypeEnum typeRelance;

	private Long espaceOrganisationId;

	private Long exerciceCompatbleId;

	private String denomination;

	private LocalDate dateDebut;

	private LocalDate dateFin;

	private long nombreFiche;

	private List<ContactOpMailingDto> contactOp;

	private LocalDate dateRelance;

	public DeclarationReminderDto() {
	}

	public DeclarationReminderDto(PublicationRelanceNiveauEnum niveauRelance, PublicationRelanceTypeEnum typeRelance, String denomination, LocalDate dateDebut, LocalDate dateFin,
			long nombreFiche) {
		this.niveauRelance = niveauRelance;
		this.typeRelance = typeRelance;
		this.denomination = denomination;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.nombreFiche = nombreFiche;
	}

	public boolean isAucunePublicationMoyens() {
		return aucunePublicationMoyens;
	}

	public void setAucunePublicationMoyens(boolean aucunePublicationMoyens) {
		this.aucunePublicationMoyens = aucunePublicationMoyens;
	}

	public boolean isAucunePublicationActivites() {
		return aucunePublicationActivites;
	}

	public void setAucunePublicationActivites(boolean aucunePublicationActivites) {
		this.aucunePublicationActivites = aucunePublicationActivites;
	}

	public Long getEspaceOrganisationId() {
		return espaceOrganisationId;
	}

	public void setEspaceOrganisationId(Long espaceOrganisationId) {
		this.espaceOrganisationId = espaceOrganisationId;
	}

	public Long getExerciceCompatbleId() {
		return exerciceCompatbleId;
	}

	public void setExerciceCompatbleId(Long exerciceCompatbleId) {
		this.exerciceCompatbleId = exerciceCompatbleId;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public long getNombreFiche() {
		return nombreFiche;
	}

	public void setNombreFiche(long nombreFiche) {
		this.nombreFiche = nombreFiche;
	}

	public List<ContactOpMailingDto> getContactOp() {
		return contactOp;
	}

	public void setContactOp(List<ContactOpMailingDto> contactOp) {
		this.contactOp = contactOp;
	}

	public PublicationRelanceNiveauEnum getNiveauRelance() {
		return niveauRelance;
	}

	public void setNiveauRelance(PublicationRelanceNiveauEnum niveauRelance) {
		this.niveauRelance = niveauRelance;
	}

	public PublicationRelanceTypeEnum getTypeRelance() {
		if (this.typeRelance == null) {
			if (this.aucunePublicationActivites && this.aucunePublicationMoyens) {
				this.typeRelance = PublicationRelanceTypeEnum.FAMA;
			} else if (this.aucunePublicationActivites) {
				this.typeRelance = PublicationRelanceTypeEnum.FA;
			} else if (this.aucunePublicationMoyens) {
				this.typeRelance = PublicationRelanceTypeEnum.MA;
			}
		}
		return this.typeRelance;
	}

	public void setTypeRelance(PublicationRelanceTypeEnum typeRelance) {
		this.typeRelance = typeRelance;
	}

	public LocalDate getDateRelance() {
		return dateRelance;
	}

	public void setDateRelance(LocalDate dateRelance) {
		this.dateRelance = dateRelance;
	}

}
