/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JsonNode;

import fr.hatvp.registre.commons.dto.EnumAsDto;

/**
 *
 * Classe d'énumération des statuts de demande
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonFormat(shape = Shape.OBJECT)
public enum StatutDemandeEnum {

	NOUVELLE("Nouvelle"),
	EN_TRAITEMENT("En traitement"),
	EN_ATTENTE_VALIDATION("En attente de validation"),
	COMPLEMENT_DEMANDE_ENVOYEE("Complément demandé"),
	COMPLEMENT_RECU("Complément reçu"),
	ACCEPTEE("Acceptée"),
	REFUSEE("Refusée");

	private final String label;

	/**
	 * Constructeur.
	 *
	 * @param label
	 */
	StatutDemandeEnum(final String label) {
		this.label = label;
	}

	/**
	 * Accesseur en lecture du champ <code>label</code>.
	 *
	 * @return le champ <code>label</code>.
	 */
	public String getValue() {
		return this.label;
	}

	@JsonCreator
	public static StatutDemandeEnum fromNode(final JsonNode node) {
		if (!node.has("code")) {
			return null;
		}

		final String name = node.get("code").asText();

		return StatutDemandeEnum.valueOf(name);
	}

	@JsonValue
	public EnumAsDto getStatutDemandeDto() {
		return new EnumAsDto(this.name(), this.label);
	}

}
