/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication.activite;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import fr.hatvp.registre.commons.dto.publication.activite.common.PublicationMetaDto;
/**
 * DTO de publication de l'entité ActiviteRepresentationInteret utilisé pour la recherche et la page d'accueil du répertoire sur le site hatvp
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PublicationActiviteRechercheDto extends PublicationMetaDto
{
    /** Date de création. */
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Paris")
    private Date publicationDate;
    
    private String identifiantFiche;

    @NotNull
    @Size(min = 1)
    private String objet;

    @NotNull
    @Size(min = 1, max = 5)
    private List<String> domainesIntervention;

    private String nom_organisation;
    
    private String num_organisation;

    public String getIdentifiantFiche()
    {
        return identifiantFiche;
    }
    public void setIdentifiantFiche(String identifiantFiche)
    {
        this.identifiantFiche = identifiantFiche;
    }

    public String getObjet()
    {
        return objet;
    }
    public void setObjet(String objet)
    {
        this.objet = objet;
    }
    public List<String> getDomainesIntervention()
    {
        return domainesIntervention;
    }
    public void setDomainesIntervention(List<String> domainesIntervention)
    {
        this.domainesIntervention = domainesIntervention;
    }
	public String getNom_organisation() {
		return nom_organisation;
	}
	public void setNom_organisation(String nom_organisation) {
		this.nom_organisation = nom_organisation;
	}
	public String getNum_organisation() {
		return num_organisation;
	}
	public void setNum_organisation(String num_organisation) {
		this.num_organisation = num_organisation;
	}

    
    
    
}
