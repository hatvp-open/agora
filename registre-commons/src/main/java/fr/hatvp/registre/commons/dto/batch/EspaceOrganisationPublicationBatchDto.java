/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.batch;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;

/**
 * DTO pour manipuler uniquement les données nécessaires au batch de publication.
 * 
 * @version $Revision$ $Date${0xD}
 */
public class EspaceOrganisationPublicationBatchDto
    extends AbstractCommonDto {

    /** Ce flag indique si l'espace organisation a fait une nouvelle publication ou non. */
    private Boolean newPublication;

    /** Ce flag indique si l'espace doit être publié sur le site institutionnel. */
    private Boolean publicationEnabled;

    /** Identifiant national de l'organisation. */
    private String nationalId;

    /** Type identifiant national (SIREN/RNA). */
    private TypeIdentifiantNationalEnum typeIdentifiantNational;

    /** Statut de l'espace **/
    private StatutEspaceEnum statut;

    /**
     * Accesseur en lecture du champ <code>nationalId</code>.
     *
     * @return le champ <code>nationalId</code>.
     */
    public String getNationalId() {
        return this.nationalId;
    }

    /**
     * Accesseur en écriture du champ <code>nationalId</code>.
     *
     * @param nationalId
     * la valeur à écrire dans <code>nationalId</code>.
     */
    public void setNationalId(final String nationalId) {
        this.nationalId = nationalId;
    }

    /**
     * Accesseur en lecture du champ <code>typeIdentifiantNational</code>.
     *
     * @return le champ <code>typeIdentifiantNational</code>.
     */
    public TypeIdentifiantNationalEnum getTypeIdentifiantNational() {
        return this.typeIdentifiantNational;
    }

    /**
     * Accesseur en écriture du champ <code>typeIdentifiantNational</code>.
     *
     * @param typeIdentifiantNational
     * la valeur à écrire dans <code>typeIdentifiantNational</code>.
     */
    public void setTypeIdentifiantNational(
        final TypeIdentifiantNationalEnum typeIdentifiantNational) {
        this.typeIdentifiantNational = typeIdentifiantNational;
    }

    /**
     * @return the newPublication
     */
    public Boolean getNewPublication()
    {
        return newPublication;
    }
    
    /**
     * @return the newPublication
     */
    public Boolean isNewPublication()
    {
        return newPublication;
    }

    /**
     * @param newPublication the newPublication to set
     */
    public void setNewPublication(Boolean newPublication)
    {
        this.newPublication = newPublication;
    }

    /**
     * @return the publicationEnabled
     */
    public Boolean getPublicationEnabled()
    {
        return publicationEnabled;
    }

    /**
     * @param publicationEnabled the publicationEnabled to set
     */
    public void setPublicationEnabled(Boolean publicationEnabled)
    {
        this.publicationEnabled = publicationEnabled;
    }

    public StatutEspaceEnum getStatut() {
        return statut;
    }

    public void setStatut(StatutEspaceEnum statut) {
        this.statut = statut;
    }
}
