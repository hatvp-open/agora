/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite;

import java.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.utils.LocalDateDeserializer;
import fr.hatvp.registre.commons.utils.LocalDateSerializer;

public class ExerciceComptableDatesDto extends AbstractCommonDto {

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateDebut;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateFin;

	private Long espaceOrganisationId;

	
	
	public ExerciceComptableDatesDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExerciceComptableDatesDto(LocalDate dateDebut, LocalDate dateFin,Long espaceOrganisationId) {
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.espaceOrganisationId = espaceOrganisationId;
	}

	/**
	 * @return the dateDebut
	 */
	public LocalDate getDateDebut() {
		return dateDebut;
	}

	/**
	 * @param dateDebut
	 *            the dateDebut to set
	 */
	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	/**
	 * @return the dateFin
	 */
	public LocalDate getDateFin() {
		return dateFin;
	}

	/**
	 * @param dateFin
	 *            the dateFin to set
	 */
	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}



	/**
	 * @return the espaceOrganisationId
	 */
	public Long getEspaceOrganisationId() {
		return espaceOrganisationId;
	}

	/**
	 * @param espaceOrganisationId
	 *            the espaceOrganisationId to set
	 */
	public void setEspaceOrganisationId(Long espaceOrganisationId) {
		this.espaceOrganisationId = espaceOrganisationId;
	}

}
