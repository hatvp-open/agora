/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.hatvp.registre.commons.dto.CollaborateurDto;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.validators.civility.Civility;

/**
 * DTO de l'entité CollaborateurEntity utilisée pour la publication.
 * Permet de nommer / ignorer les champs pour le Json de la publication. 
 * 
 * @version $Revision$ $Date${0xD}
 */
public class PubCollaborateurDto
    extends CollaborateurDto {

    @JsonIgnore
    private Long id;
    
    @JsonIgnore
    private Integer version;
    
    @JsonIgnore
    private Integer sortOrder;

    @Civility(value = {CiviliteEnum.MME, CiviliteEnum.M})
    private CiviliteEnum civilite;

    @JsonProperty("nom")
    private String nom;

    @JsonProperty("prenom")
    private String prenom;

    @JsonIgnore
    private String email;

    @JsonProperty("fonction")
    private String fonction;

    @JsonIgnore
    private Boolean inscrit;

    @JsonIgnore
    private Boolean actif;

    @JsonIgnore
    private Long declarantId;

    @JsonIgnore
    private Long espaceOrganisationId;

}
