/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.validators.category;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import fr.hatvp.registre.commons.dto.DeclarantContactDto;
import fr.hatvp.registre.commons.utils.ValidateUtils;

/**
 */
public class CategoryValidator implements ConstraintValidator<CategoryMailPhone, List<DeclarantContactDto>> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(CategoryMailPhone constraintAnnotation) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid(List<DeclarantContactDto> value, ConstraintValidatorContext context)
    {

        return value == null || value.isEmpty() || ValidateUtils.isContactListCategoryValid(value);

    }
}
