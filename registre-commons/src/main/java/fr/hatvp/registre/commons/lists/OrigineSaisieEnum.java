/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

/**
 * Permet d'enregistrer l'utilisateur à l'origine d'une saisie 
 *
 * @version $Revision$ $Date${0xD}
 */
public enum OrigineSaisieEnum {

	DECLARANT("Saisie faite par une déclarant"),
    AGENT("Saisie faite par un agent de l'HATVP");

	private final String label;

	OrigineSaisieEnum(final String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}

}
