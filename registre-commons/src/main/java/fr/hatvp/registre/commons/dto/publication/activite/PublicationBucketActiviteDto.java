/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.publication.activite;

import com.fasterxml.jackson.annotation.JsonInclude;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;
/**
 * Dto de publication contenant une activité publié et un historique des precedentes publications de cet activités
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PublicationBucketActiviteDto
    extends AbstractCommonDto
{
    /** dernière publication de l'activite. */
    private PublicationActiviteDto publicationCourante;

    /** historique des publications de l'activite. */
//    private List<PublicationActiviteDto> historique = new ArrayList<>();

    public PublicationActiviteDto getPublicationCourante()
    {
        return publicationCourante;
    }
    public void setPublicationCourante(PublicationActiviteDto publicationCourante)
    {
        this.publicationCourante = publicationCourante;
    }
//	public List<PublicationActiviteDto> getHistorique() {
//		return historique;
//	}
//	public void setHistorique(List<PublicationActiviteDto> historique) {
//		this.historique = historique;
//	}
    
    
}
