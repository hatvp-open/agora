/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.lists;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JsonNode;
import fr.hatvp.registre.commons.dto.EnumAsDto;

/**
 * Classe d'énumération des niveaux d'interventions tous secteur confondu des représentations d'interet.
 *
 * @version $Revision$ $Date${0xD}
 */
@JsonFormat(shape = Shape.OBJECT)
public enum NiveauInterventionEnum {

    LOCAL("Local", 1),
    NATIONAL("National", 2),
    EUROPEEN("Européen", 3),
    MONDIAL("Mondial", 4);

    private final String label;
    private final int ordre;

    NiveauInterventionEnum(final String label, final int ordre) {
        this.label = label;
        this.ordre = ordre;
    }

    public String getLabel() {
        return this.label;
    }

    @JsonCreator
    public static NiveauInterventionEnum fromNode(final JsonNode node) {
        if (!node.has("code")) {
            return null;
        }

        final String name = node.get("code").asText();

        return NiveauInterventionEnum.valueOf(name);
    }

    @JsonValue
    public EnumAsDto getNiveauInterventionDto() {
        return new EnumAsDto(this.name(), this.label, this.ordre);
    }
}
