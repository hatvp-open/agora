/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto;

import java.util.Date;

/**
 * 
 *
 * @version $Revision$ $Date${0xD}
 */
public class ChiffresSurUnePeriodeDto
		extends AbstractCommonDto {
	
	private Date dateDebutPeriode;
	
	private Date dateFinPeriode;
	/**
	 * nombre de fiches d’activités déposées par les représentants d’intérêts sur une période
	 */
	private Integer nbActiviteDeposeesRI;
	/**
	 * nombre de représentants ayant déclaré des activités sur une période
	 */
	private Integer nbRIDeclareActivite;
	

	public Date getDateDebutPeriode() {
		return dateDebutPeriode;
	}

	public void setDateDebutPeriode(Date dateDebutPeriode) {
		this.dateDebutPeriode = dateDebutPeriode;
	}

	public Date getDateFinPeriode() {
		return dateFinPeriode;
	}

	public void setDateFinPeriode(Date dateFinPeriode) {
		this.dateFinPeriode = dateFinPeriode;
	}

	public Integer getNbActiviteDeposeesRI() {
		return nbActiviteDeposeesRI;
	}

	public void setNbActiviteDeposeesRI(Integer nbActiviteDeposeesRI) {
		this.nbActiviteDeposeesRI = nbActiviteDeposeesRI;
	}

	public Integer getNbRIDeclareActivite() {
		return nbRIDeclareActivite;
	}

	public void setNbRIDeclareActivite(Integer nbRIDeclareActivite) {
		this.nbRIDeclareActivite = nbRIDeclareActivite;
	}

}
