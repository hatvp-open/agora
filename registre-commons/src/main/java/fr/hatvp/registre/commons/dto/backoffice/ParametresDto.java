/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.backoffice;
import com.fasterxml.jackson.annotation.JsonInclude;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import org.springframework.stereotype.Component;

/**
 * Dto de l'entité de parametrage du mode.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ParametresDto
    extends AbstractCommonDto
{
    /** etat du mode */
    private Boolean active;

    /** nombre limite de déclarants simultané sur l'appli */
    private Integer nombreLimite;

    public Boolean getActive()
    {
        return active;
    }
    public void setActive(Boolean active)
    {
        this.active = active;
    }
    public Integer getNombreLimite()
    {
        return nombreLimite;
    }
    public void setNombreLimite(Integer nombreLimite)
    {
        this.nombreLimite = nombreLimite;
    }
}
