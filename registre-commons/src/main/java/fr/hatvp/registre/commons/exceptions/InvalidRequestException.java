/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.exceptions;

import org.springframework.validation.Errors;

/**
 * 
 * Classe d'exception pour les requetes invalides.
 *
 */
@SuppressWarnings("serial")
public class InvalidRequestException
    extends RuntimeException
{
    private final Errors errors;

    public InvalidRequestException(final String message, final Errors errors)
    {
        super(message);
        this.errors = errors;
    }

    public Errors getErrors()
    {
        return this.errors;
    }
}
