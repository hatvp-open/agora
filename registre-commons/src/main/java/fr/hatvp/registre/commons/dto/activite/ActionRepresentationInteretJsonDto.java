/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.dto.activite;

import java.util.List;
import java.util.Set;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

public class ActionRepresentationInteretJsonDto extends AbstractCommonDto {

	private Set<String> reponsablesPublics;

	private Set<String> decisionsConcernees;

	private Set<String> actionsMenees;

	private String actionMeneeAutre;
	
	private String responsablePublicAutre;

	private List<String> tiers;

	private String observation;

	public Set<String> getReponsablesPublics() {
		return reponsablesPublics;
	}

	public void setReponsablesPublics(Set<String> reponsablesPublics) {
		this.reponsablesPublics = reponsablesPublics;
	}

	public Set<String> getDecisionsConcernees() {
		return decisionsConcernees;
	}

	public void setDecisionsConcernees(Set<String> decisionsConcernees) {
		this.decisionsConcernees = decisionsConcernees;
	}

	public Set<String> getActionsMenees() {
		return actionsMenees;
	}

	public void setActionsMenees(Set<String> actionsMenees) {
		this.actionsMenees = actionsMenees;
	}

	public String getActionMeneeAutre() {
		return actionMeneeAutre;
	}

	public void setActionMeneeAutre(String actionMeneeAutre) {
		this.actionMeneeAutre = actionMeneeAutre;
	}

	public String getResponsablePublicAutre() {
		return responsablePublicAutre;
	}

	public void setResponsablePublicAutre(String responsablePublicAutre) {
		this.responsablePublicAutre = responsablePublicAutre;
	}

	public List<String> getTiers() {
		return tiers;
	}

	public void setTiers(List<String> tiers) {
		this.tiers = tiers;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}


}
