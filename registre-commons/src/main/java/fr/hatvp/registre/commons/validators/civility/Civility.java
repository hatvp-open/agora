/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.validators.civility;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import fr.hatvp.registre.commons.lists.CiviliteEnum;

/**
 * 
 * Annotation de validation de la civilité.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Documented
@Constraint(validatedBy = CivilityValidator.class)
@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface Civility
{
    String message() default "Civility is not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    CiviliteEnum[] value() default {};

}