/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test.dto;

import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import org.agileware.test.PropertiesTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * Classe de test des {@link EspaceOrganisationDto}.
 * @version $Revision$ $Date${0xD}
 */

public class EspaceOrganisationDtoTest {

    /**
     * Validateur Spring
     */
    private static Validator validator;

    /**
     * Id de l'organisation utilisé pour les tests
     */
    private final Long ESPACE_ORGANISATION_ID = 1L;

    /**
     * Initialisation des tests.
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        // Init DTO Validator
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    /**
     * Test Validateur DTO OK
     */
    @Test
    public void testDtoValidationOK() throws Exception {
        final EspaceOrganisationDto espaceOrganisationDto = new EspaceOrganisationDto();
        espaceOrganisationDto.setId(this.ESPACE_ORGANISATION_ID);

        espaceOrganisationDto.setDenomination(RegistreUtils.randomString(150));
        espaceOrganisationDto.setNationalId(RegistreUtils.randomString(10));
        espaceOrganisationDto.setTypeIdentifiantNational(TypeIdentifiantNationalEnum.SIREN);
        espaceOrganisationDto.setCategorieOrganisation(TypeOrganisationEnum.ASSOC);

        espaceOrganisationDto.setAdresse(RegistreUtils.randomString(500));
        espaceOrganisationDto.setCodePostal(RegistreUtils.randomString(5));
        espaceOrganisationDto.setVille(RegistreUtils.randomString(10));
        espaceOrganisationDto.setPays(RegistreUtils.randomString(20));
        espaceOrganisationDto.setNonPublierMonAdressePhysique(true);

        espaceOrganisationDto.setTelephoneDeContact(RegistreUtils.randomString(15));
        espaceOrganisationDto.setNonPublierMonTelephoneDeContact(true);
        espaceOrganisationDto.setEmailDeContact(RegistreUtils.randomString(50));
        espaceOrganisationDto.setNonPublierMonAdresseEmail(true);

        espaceOrganisationDto.setChiffreAffaire(99999999999L);
        espaceOrganisationDto.setAnneeChiffreAffaire(2013);
        espaceOrganisationDto.setDepenses(10000000L);
        espaceOrganisationDto.setnPersonnesEmployees(999);

        espaceOrganisationDto
            .getListSecteursActivites().add(SecteurActiviteEnum.AGRI);
        espaceOrganisationDto
            .getListNiveauIntervention().add(NiveauInterventionEnum.EUROPEEN);

        espaceOrganisationDto.setLienSiteWeb(RegistreUtils.randomString(50));
        espaceOrganisationDto.setLienPageLinkedin(RegistreUtils.randomString(50));
        espaceOrganisationDto.setLienPageFacebook(RegistreUtils.randomString(50));
        espaceOrganisationDto.setLienPageTwitter(RegistreUtils.randomString(50));

        final Set<ConstraintViolation<EspaceOrganisationDto>> violations = validator
            .validate(espaceOrganisationDto);

        Assert.assertTrue(violations.isEmpty());
    }

    /**
     * Test Validateur DTO KO
     * Si taille au delà des limites du Validateur @Size
     */
    @Test
    public void testDtoValidationKO() throws Exception {
        final EspaceOrganisationDto dto = new EspaceOrganisationDto();
        dto.setId(this.ESPACE_ORGANISATION_ID);

        dto.setDenomination(RegistreUtils.randomString(151));
        dto.setNationalId(RegistreUtils.randomString(151));
        dto.setTypeIdentifiantNational(TypeIdentifiantNationalEnum.SIREN);
        dto.setCategorieOrganisation(TypeOrganisationEnum.ASSOC);

        dto.setAdresse(RegistreUtils.randomString(501));
        dto.setCodePostal(RegistreUtils.randomString(11));
        dto.setVille(RegistreUtils.randomString(151));
        dto.setPays(RegistreUtils.randomString(151));
        dto.setNonPublierMonAdressePhysique(true);

        dto.setTelephoneDeContact(RegistreUtils.randomString(20));
        dto.setNonPublierMonTelephoneDeContact(true);
        dto.setEmailDeContact(RegistreUtils.randomString(151));
        dto.setNonPublierMonAdresseEmail(true);

        dto.setChiffreAffaire(1000000000000L);
        dto.setAnneeChiffreAffaire(1000);
        dto.setDepenses(1000000000L);
        dto.setnPersonnesEmployees(1000);

        dto.setLienSiteWeb(RegistreUtils.randomString(151));
        dto.setLienPageLinkedin(RegistreUtils.randomString(151));
        dto.setLienPageFacebook(RegistreUtils.randomString(151));
        dto.setLienPageTwitter(RegistreUtils.randomString(151));

        final Set<ConstraintViolation<EspaceOrganisationDto>> violations = validator
            .validate(dto);
        Assert.assertEquals(16, violations.size());
    }

    /**
     * Tester tous les getter et setter de la classe.
     *
     * @throws Exception si le test échoue.
     */
    @Test
    public void testProperties() throws Exception {
        final PropertiesTester tester = new PropertiesTester();
        tester.setNameExclusions("logo");
        tester.testAll(EspaceOrganisationDto.class);
    }

}
