/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test.dto;

import fr.hatvp.registre.commons.dto.DeclarantContactDto;
import fr.hatvp.registre.commons.lists.CategoryTelephoneMail;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import org.agileware.test.PropertiesTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * Classe de test des {@link DeclarantContactDto}.
 *
 * @version $Revision$ $Date${0xD}
 */

public class DeclarantContactDtoTest {

    /**
     * Validateur Spring
     */
    private static Validator validator;

    /**
     * Id de l'entité utilisé pour les tests
     */
    private final Long DECLARANT_CONTACT_ID = 1L;

    /**
     * Initialisation des tests.
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        // Init DTO Validator
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    /**
     * Test Validateur DTO OK
     */
    @Test
    public void testDtoValidationOK() throws Exception {
        final DeclarantContactDto declarantContactDto = new DeclarantContactDto();
        declarantContactDto.setId(this.DECLARANT_CONTACT_ID);

        declarantContactDto.setEmail(RegistreUtils.randomString(150));
        declarantContactDto.setTelephone(RegistreUtils.randomString(15));
        declarantContactDto.setCategorie(CategoryTelephoneMail.PRO);

        final Set<ConstraintViolation<DeclarantContactDto>> violations = validator
            .validate(declarantContactDto);

        Assert.assertTrue(violations.isEmpty());
    }

    /**
     * Test Validateur DTO KO
     * Si taille au delà des limites du Validateur @Size
     */
    @Test
    public void testDtoValidationKO() throws Exception {
        final DeclarantContactDto declarantContactDto = new DeclarantContactDto();
        declarantContactDto.setId(this.DECLARANT_CONTACT_ID);

        declarantContactDto.setEmail(RegistreUtils.randomString(151));
        declarantContactDto.setTelephone(RegistreUtils.randomString(16));
        declarantContactDto.setCategorie(CategoryTelephoneMail.PERSO);

        final Set<ConstraintViolation<DeclarantContactDto>> violations = validator
            .validate(declarantContactDto);

        Assert.assertEquals(2, violations.size());
    }

    /**
     * Tester tous les getter et setter de la classe.
     *
     * @throws Exception si le test échoue.
     */
    @Test
    public void testProperties() throws Exception {
        final PropertiesTester tester = new PropertiesTester();
        tester.testAll(DeclarantContactDto.class);
    }

}
