/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test.dto;

import fr.hatvp.registre.commons.dto.CollaborateurDto;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import org.agileware.test.PropertiesTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * Classe de test des {@link CollaborateurDto}.
 * @version $Revision$ $Date${0xD}
 */

public class CollaborateurDtoTest {

    /**
     * Validateur Spring
     */
    private static Validator validator;

    /**
     * Id de l'entité utilisé pour les tests
     */
    private final Long COLLABORATEUR_ID = 1L;

    /**
     * Initialisation des tests.
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        // Init DTO Validator
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    /**
     * Test Validateur DTO OK
     */
    @Test
    public void testDtoValidationOK() throws Exception {
        final CollaborateurDto collaborateurDto = new CollaborateurDto();
        collaborateurDto.setId(this.COLLABORATEUR_ID);

        collaborateurDto.setEmail(RegistreUtils.randomString(150));
        collaborateurDto.setNom(RegistreUtils.randomString(150));
        collaborateurDto.setPrenom(RegistreUtils.randomString(150));
        collaborateurDto.setCivilite(CiviliteEnum.M);

        final Set<ConstraintViolation<CollaborateurDto>> violations = validator
            .validate(collaborateurDto);

        Assert.assertTrue(violations.isEmpty());
    }

    /**
     * Test Validateur DTO KO
     * Si taille au delà des limites du Validateur @Size
     */
    @Test
    public void testDtoValidationKO() throws Exception {
        final CollaborateurDto collaborateurDto = new CollaborateurDto();
        collaborateurDto.setId(this.COLLABORATEUR_ID);

        collaborateurDto.setNom(RegistreUtils.randomString(151));
        collaborateurDto.setPrenom(RegistreUtils.randomString(151));
        collaborateurDto.setCivilite(CiviliteEnum.M);

        final Set<ConstraintViolation<CollaborateurDto>> violations = validator
            .validate(collaborateurDto);

        Assert.assertEquals(3, violations.size());
    }

    /**
     * Tester tous les getter et setter de la classe.
     *
     * @throws Exception si le test échoue.
     */
    @Test
    @Ignore
    public void testProperties() throws Exception {
        final PropertiesTester tester = new PropertiesTester();
        tester.testAll(CollaborateurDto.class);
    }

}
