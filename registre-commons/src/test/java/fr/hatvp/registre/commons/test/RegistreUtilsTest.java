/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fr.hatvp.registre.commons.lists.NiveauInterventionEnum;
import fr.hatvp.registre.commons.lists.SecteurActiviteEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;

/**
 * Classe de test de l'utilitaire {@link RegistreUtils}.
 * @version $Revision$ $Date${0xD}
 */

public class RegistreUtilsTest {
    /**
     * Test de la méthode {@link RegistreUtils#normaliser(String)}
     */
    @Test
    public void testNormalisation() {
        final String name1 = "jean marc";
        final String name2 = "jean-marc";
        final String name3 = "jean";
        final String expectedName1 = "Jean Marc";
        final String expectedName2 = "Jean-Marc";
        final String expectedName3 = "Jean";

        final String normName1 = RegistreUtils.normaliser(name1);
        final String normName2 = RegistreUtils.normaliser(name2);
        final String normName3 = RegistreUtils.normaliser(name3);

        Assert.assertEquals(expectedName1, normName1);
        Assert.assertEquals(expectedName2, normName2);
        Assert.assertEquals(expectedName3, normName3);
    }

    /**
     * Test de la méthode {@link RegistreUtils#normaliserUnGroupeDeNom(String...)}
     */
    @Test
    public void testNormalisationParGroupe() {
        final String name1 = "jean marc";
        final String name2 = "jean-marc";
        final String name3 = "jean";
        final String expectedName1 = "Jean Marc";
        final String expectedName2 = "Jean-Marc";
        final String expectedName3 = "Jean";

        final List<String> normName = RegistreUtils.normaliserUnGroupeDeNom(name1, name2, name3);

        assert normName != null;
        Assert.assertEquals(expectedName1, normName.get(0));
        Assert.assertEquals(expectedName2, normName.get(1));
        Assert.assertEquals(expectedName3, normName.get(2));
    }

    /**
     * Test de la méthode {@link TypeOrganisationEnum#typesOrganisationsAsJson()}
     */
    @Test
    public void testJsonTypesOrganisation() {
        final String jsonOrga = TypeOrganisationEnum.typesOrganisationsAsJson();
        Assert.assertNotNull(jsonOrga);
    }

    /**
     * Test de la méthode {@link RegistreUtils#implodeListWithComma(List)}
     */
    @Test
    public void testImplodeListWithComma() {
        Assert.assertTrue(RegistreUtils.implodeListWithComma(new ArrayList<>()).isEmpty());

        final List<Object> listToImplode = new ArrayList<>();
        listToImplode.add(SecteurActiviteEnum.AGRI);
        listToImplode.add(NiveauInterventionEnum.EUROPEEN);
        final String result = RegistreUtils.implodeListWithComma(listToImplode);

        Assert.assertEquals(2, result.split(",").length);
    }
}
