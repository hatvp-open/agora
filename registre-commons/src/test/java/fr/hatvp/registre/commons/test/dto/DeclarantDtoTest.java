/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test.dto;

import java.text.SimpleDateFormat;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.agileware.test.PropertiesTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;

/**
 * Classe de test des {@link DeclarantDto}.
 *
 * @version $Revision$ $Date${0xD}
 */

public class DeclarantDtoTest {

    /**
     * Validateur Spring
     */
    private static Validator validator;

    /**
     * Id de l'entité utilisé pour les tests
     */
    private final Long DECLARANT_ID = 1L;

    /**
     * Initialisation des tests.
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        // Init DTO Validator
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    /**
     * Test Validateur DTO OK
     */
    @Test
    public void testDtoValidationOK() throws Exception {
        final DeclarantDto declarantDto = new DeclarantDto();
        declarantDto.setId(this.DECLARANT_ID);

        declarantDto.setPrenom("Prénom de test");
        declarantDto.setNom("Nom de test");
        declarantDto.setTelephone("0123456789");
        declarantDto.setCivility(CiviliteEnum.M);
        declarantDto.setPassword("Password1234");
        declarantDto.setPreviousPassword("Password1234");

        declarantDto.setEmail("ABC@DEF.GH");
        
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        declarantDto.setBirthDate(simpleDateFormat.parse("30/03/1990"));

        final Set<ConstraintViolation<DeclarantDto>> violations = validator.validate(declarantDto);
        
        violations.forEach(constraint -> System.out.println(constraint.getMessage()));
        Assert.assertTrue(violations.isEmpty());
        
    }

    /**
     * Test Validateur DTO KO
     * Si taille au delà des limites du Validateur @Size
     */
    @Test
    public void testDtoValidationKO() throws Exception {
        final DeclarantDto declarantDto = new DeclarantDto();
        declarantDto.setId(this.DECLARANT_ID);

        declarantDto.setPrenom(RegistreUtils.randomString(151));
        declarantDto.setNom(RegistreUtils.randomString(151));
        declarantDto.setTelephone(RegistreUtils.randomString(16));
        declarantDto.setCivility(CiviliteEnum.MME);
        declarantDto.setPassword(RegistreUtils.randomString(151));
        declarantDto.setPreviousPassword(RegistreUtils.randomString(151));
        declarantDto.setEmail(RegistreUtils.randomString(151));

        final Set<ConstraintViolation<DeclarantDto>> violations = validator.validate(declarantDto);
        Assert.assertEquals(8, violations.size());
    }

    /**
     * Tester tous les getter et setter de la classe.
     *
     * @throws Exception si le test échoue.
     */
    @Test
    public void testProperties() throws Exception {
        final PropertiesTester tester = new PropertiesTester();
        tester.testAll(DeclarantDto.class);
    }

}
