/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test.dto;

import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import org.agileware.test.PropertiesTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * Classe de test des {@link OrganisationDto}.
 *
 * @version $Revision$ $Date${0xD}
 */

public class OrganisationDtoTest {

    /**
     * Validateur Spring
     */
    private static Validator validator;

    /**
     * Id de l'organisation utilisé pour les tests
     */
    private final Long ID_ORGANISATION_TEST = 1L;

    /**
     * Id national utilisé pour les test.
     */
    private final String ID_NATIONAL_TEST = "145236987";

    /**
     * Initialisation des tests.
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        // Init DTO Validator
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    /**
     * Test Validateur DTO OK
     */
    @Test
    public void testDtoValidationOK() throws Exception {
        final OrganisationDto organisationDto = new OrganisationDto();
        organisationDto.setId(this.ID_ORGANISATION_TEST);
        organisationDto.setOrganizationSpaceExist(false);
        organisationDto.setDenomination(RegistreUtils.randomString(150));

        organisationDto.setNationalId(RegistreUtils.randomString(10));
        organisationDto.setSiren(RegistreUtils.randomString(9));
        organisationDto.setRna("w" + RegistreUtils.randomString(9));
        organisationDto.setHatvpNumber("H" + RegistreUtils.randomString(9));
        organisationDto.setDunsNumber(RegistreUtils.randomString(9));

        organisationDto.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
        organisationDto.setAdresse(RegistreUtils.randomString(500));
        organisationDto.setCodePostal(RegistreUtils.randomString(5));
        organisationDto.setVille(RegistreUtils.randomString(10));

        final Set<ConstraintViolation<OrganisationDto>> violations = validator
            .validate(organisationDto);

        Assert.assertTrue(violations.isEmpty());
    }

    /**
     * Test Validateur DTO KO
     * Si taille au delà des limites du Validateur @Size
     */
    @Test
    public void testDtoValidationKO() throws Exception {
        final OrganisationDto organisationDto = new OrganisationDto();
        organisationDto.setId(this.ID_ORGANISATION_TEST);
        organisationDto.setOrganizationSpaceExist(false);
        organisationDto.setDenomination(RegistreUtils.randomString(151));

        organisationDto.setNationalId(RegistreUtils.randomString(10));
        organisationDto.setSiren(RegistreUtils.randomString(10));
        organisationDto.setRna("w" + RegistreUtils.randomString(9));
        organisationDto.setHatvpNumber("H" + RegistreUtils.randomString(10));
        organisationDto.setDunsNumber(RegistreUtils.randomString(9));

        organisationDto.setOriginNationalId(TypeIdentifiantNationalEnum.SIREN);
        organisationDto.setAdresse(RegistreUtils.randomString(501));
        organisationDto.setCodePostal(RegistreUtils.randomString(5));
        organisationDto.setVille(RegistreUtils.randomString(10));

        final Set<ConstraintViolation<OrganisationDto>> violations = validator
            .validate(organisationDto);
        Assert.assertEquals(4, violations.size());
    }

    /**
     * Tester tous les getter et setter de la classe.
     *
     * @throws Exception si le test échoue.
     */
    @Test
    public void testProperties() throws Exception {
        final PropertiesTester tester = new PropertiesTester();
        tester.testAll(OrganisationDto.class);
    }

}
