/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test;

import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import fr.hatvp.registre.commons.utils.RegistreRandomUtils;

/**
 * Classe de test de l'utilitaire {@link RegistreRandomUtils}.
 *
 * @version $Revision$ $Date${0xD}
 */

public class RegistreRandomUtilsTest
{
   @InjectMocks
   RegistreRandomUtils registreRandomUtils;

    /**
     * Initialisation des tests.
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        // Process mock annotations
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test de la méthode {@link RegistreRandomUtils#generateRandomNumber(Integer)}
     */
    @Test
    public void testGenerateRandomNumber() {
        Random random = new Random();
        Integer length = random.nextInt(9) + 1;
        Integer generatedNumber = registreRandomUtils.generateRandomNumber(length);
        Integer min = ((int) Math.pow(10, length - 1));
        Integer max = (9 * (int) Math.pow(10, length - 1)) - 1 + min;
        Assert.assertTrue(min <= generatedNumber && generatedNumber <= max);
    }
}
