/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * Classe de configuration des test.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Configuration
@ComponentScan(basePackages = "fr.hatvp.registre.commons")
public class CommonsTestConfig
{

}
