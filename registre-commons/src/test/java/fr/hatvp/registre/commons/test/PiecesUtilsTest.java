/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.utils.PiecesUtils;

/**
 * Classe de test de l'utilitaire {@link PiecesUtils}.
 *
 * @version $Revision$ $Date${0xD}
 */

public class PiecesUtilsTest {
	/**
	 * Test de la méthode {@link PiecesUtils#checkConformitePiece(MultipartFile)}
	 */
	@Test
	public void testCheckConformitePiece() throws IOException {
		InputStream inputStream1 = this.getClass().getClassLoader().getResourceAsStream("test.jpg");
		MockMultipartFile file1 = new MockMultipartFile("test", "test.jpg", "image/jpg", inputStream1);

		InputStream inputStream2 = this.getClass().getClassLoader().getResourceAsStream("test.jpeg");
		MockMultipartFile file2 = new MockMultipartFile("test", "test.jpeg", "image/jpeg", inputStream2);

		InputStream inputStream3 = this.getClass().getClassLoader().getResourceAsStream("test.png");
		MockMultipartFile file3 = new MockMultipartFile("test", "test.png", "image/png", inputStream3);

		InputStream inputStream4 = this.getClass().getClassLoader().getResourceAsStream("test.pdf");
		MockMultipartFile file4 = new MockMultipartFile("pj", "pj.pdf", "application/pdf", inputStream4);

		PiecesUtils.checkConformitePiece(file1);
		PiecesUtils.checkConformitePiece(file2);
		PiecesUtils.checkConformitePiece(file3);
		PiecesUtils.checkConformitePiece(file4);
	}

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFile() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.txt", "text/plain", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

	// JPG HACK

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFileWithNameManipulationJPG() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.jpg", "text/plain", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFileWithContentTypeManipulationJPG() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.txt", "image/jpg", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFileWithContentTypeAndNameManipulationJPG() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.jpg", "image/jpg", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

	// JPEG HACK

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFileWithNameManipulationJPEG() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.jpeg", "text/plain", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFileWithContentTypeManipulationJPEG() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.txt", "image/jpeg", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFileWithContentTypeAndNameManipulationJPEG() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.jpeg", "image/jpeg", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

	// PNG HACK

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFileWithNameManipulationPNG() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.png", "text/plain", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFileWithContentTypeManipulationPNG() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.txt", "image/png", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFileWithContentTypeAndNameManipulationPNG() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.png", "image/png", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

	// PDF HACK

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFileWithNameManipulationPDF() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.pdf", "text/plain", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFileWithContentTypeManipulationPDF() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.txt", "application/pdf", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

	@Test(expected = BusinessGlobalException.class)
	public void testForbiddenFileWithContentTypeAndNameManipulationPDF() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.txt");
		MockMultipartFile file = new MockMultipartFile("test", "test.pdf", "application/pdf", inputStream);
		PiecesUtils.checkConformitePiece(file);
	}

}
