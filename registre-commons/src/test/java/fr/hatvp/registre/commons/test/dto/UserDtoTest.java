/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.hatvp.registre.commons.dto.DeclarantContactDto;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.lists.CategoryTelephoneMail;
import fr.hatvp.registre.commons.lists.CiviliteEnum;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe de test des {@link DeclarantDto}.
 *
 * @version $Revision$ $Date${0xD}
 */

public class UserDtoTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDtoTest.class);

    @Test
    public void testUserAttribute() throws JsonProcessingException {

        final DeclarantDto declarantDto = new DeclarantDto();
        declarantDto.setPrenom("Djenane");
        declarantDto.setNom("Wail");
        declarantDto.setBirthDate(
            Date.from(LocalDate.of(1992, 10, 4).atStartOfDay().toInstant(ZoneOffset.UTC)));

        declarantDto.setPassword("test");
        declarantDto.setActivated(true);
        declarantDto.setCivility(CiviliteEnum.M);
        declarantDto.setEmail("wail.djenane-externe@hatvp.fr");
        declarantDto.setEmailTemp("tanguy.vonstebut-externe@hatvp.fr");

        final List<DeclarantContactDto> phones = new ArrayList<>();
        phones.add(getContact(null, "0751XXXXXXX",
            CategoryTelephoneMail.PERSO));

        final List<DeclarantContactDto> emailsComplement = new ArrayList<>();
        emailsComplement.add(getContact(null, "test.test@test.com",
            CategoryTelephoneMail.PRO));

        declarantDto.setEmailComplement(emailsComplement);
        declarantDto.setPhone(phones);

        Assert.assertEquals("Djenane", declarantDto.getPrenom());
        Assert.assertEquals("Wail", declarantDto.getNom());
        Assert.assertEquals(
            Date.from(LocalDate.of(1992, 10, 4).atStartOfDay().toInstant(ZoneOffset.UTC)),
            declarantDto.getBirthDate());
        Assert.assertEquals(true, declarantDto.isActivated());
        Assert.assertEquals(CiviliteEnum.M, declarantDto.getCivility());
        Assert.assertEquals("wail.djenane-externe@hatvp.fr", declarantDto.getEmail());
        Assert.assertEquals("tanguy.vonstebut-externe@hatvp.fr", declarantDto.getEmailTemp());
        Assert.assertEquals("test", declarantDto.getPassword());

        Assert.assertEquals(phones, declarantDto.getPhone());
        Assert.assertEquals(emailsComplement, declarantDto.getEmailComplement());

        final String result = new ObjectMapper().writeValueAsString(declarantDto);
        LOGGER.info(result);

    }

    @Test
    public void validatePassword() {
        // Minimum 10 characters at least 1 Alphabet, 1 Number and 1 Special Character:
        final String PASSWORD_PATTERN = "^(?=.*?[a-z])(?=.*?[0-9]).{10,}$";

        final String passwordGood = "test412d@s";
        final String passwordFail = "test";

        Assert.assertEquals(true, passwordGood.matches(PASSWORD_PATTERN));
        Assert.assertEquals(false, passwordFail.matches(PASSWORD_PATTERN));
    }

    /**
     * Créer une nstance de {@link DeclarantContactDto}
     */
    private DeclarantContactDto getContact(final String telephone, final String email,
                                           final CategoryTelephoneMail type) {
        final DeclarantContactDto declarantContactDto = new DeclarantContactDto();
        declarantContactDto.setTelephone(telephone);
        declarantContactDto.setEmail(email);
        declarantContactDto.setCategorie(type);

        return declarantContactDto;
    }
}
