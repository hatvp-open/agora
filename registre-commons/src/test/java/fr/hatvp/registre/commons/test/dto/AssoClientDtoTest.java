/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test.dto;

import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import org.agileware.test.PropertiesTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * Classe de test des {@link AssoClientDto}.
 *
 * @version $Revision$ $Date${0xD}
 */

public class AssoClientDtoTest {

    /**
     * Validateur Spring
     */
    private static Validator validator;

    /**
     * Id de l'entité utilisé pour les tests
     */
    private final Long ASSO_CLIENT_ID = 1L;

    /**
     * Initialisation des tests.
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        // Init DTO Validator
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    /**
     * Test Validateur DTO OK
     */
    @Test
    public void testDtoValidationOK() throws Exception {
        final AssoClientDto assoClientDto = new AssoClientDto();
        assoClientDto.setId(this.ASSO_CLIENT_ID);

        assoClientDto.setDenomination(RegistreUtils.randomString(150));

        final Set<ConstraintViolation<AssoClientDto>> violations = validator
            .validate(assoClientDto);

        Assert.assertTrue(violations.isEmpty());
    }

    /**
     * Test Validateur DTO KO
     * Si taille au delà des limites du Validateur @Size
     */
    @Test
    public void testDtoValidationKO() throws Exception {
        final AssoClientDto assoClientDto = new AssoClientDto();
        assoClientDto.setId(this.ASSO_CLIENT_ID);

        assoClientDto.setDenomination(RegistreUtils.randomString(151));

        final Set<ConstraintViolation<AssoClientDto>> violations = validator
            .validate(assoClientDto);

        Assert.assertEquals(1, violations.size());
    }

    /**
     * Tester tous les getter et setter de la classe.
     *
     * @throws Exception si le test échoue.
     */
    @Test
    @Ignore
    public void testProperties() throws Exception {
        final PropertiesTester tester = new PropertiesTester();
        tester.testAll(AssoClientDto.class);
    }
}
