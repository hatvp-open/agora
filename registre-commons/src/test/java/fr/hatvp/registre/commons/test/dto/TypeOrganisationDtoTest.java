/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test.dto;

import fr.hatvp.registre.commons.dto.TypeOrganisationDto;
import org.agileware.test.PropertiesTester;
import org.junit.Test;

/**
 * Classe de test des {@link TypeOrganisationDto}.
 * @version $Revision$ $Date${0xD}
 */

public class TypeOrganisationDtoTest {
    /**
     * Tester tous les getter et setter de la classe.
     *
     * @throws Exception si le test échoue.
     */
    @Test
    public void testProperties() throws Exception {
        final PropertiesTester tester = new PropertiesTester();
        tester.testAll(TypeOrganisationDto.class);
    }
}
