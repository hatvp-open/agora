/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test.backoffice.dto;

import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import fr.hatvp.registre.commons.dto.backoffice.EspaceOrganisationLockBoDto;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;
import org.agileware.test.PropertiesTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Date;
import java.util.Set;

/**
 *
 * Classe de test de validation des {@link CommentBoDto}.
 *
 * @version $Revision$ $Date${0xD}
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {EspaceOrganisationLockBoDto.class},
        loader = AnnotationConfigContextLoader.class)
public class CommentaireBoDtoTest {

    /** Id de test. */
    private static final Long ID = 1L;

    /** Version de test. */
    private static final int VERSION = 0;

    /** Auteur de test. */
    private static final String TEST_AUTEUR = "test auteur";

    /** Message de test. */
    private static final String TEST_MESSAGE = "Test message";

    /** Validateur Spring */
    private static Validator validator;

    /**
     * Initialisation des tests.
     *
     * @throws Exception exception d'initialisation.
     */
    @Before
    public void before() throws Exception
    {
        // Init DTO Validator
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    /**
     * Test Validateur DTO OK
     */
    @Test
    public void testDtoValidationOK() throws Exception
    {
        final CommentBoDto commentaireBoDtoTest = new CommentBoDto();
        commentaireBoDtoTest.setId(ID);
        commentaireBoDtoTest.setVersion(VERSION);
        commentaireBoDtoTest.setAuteur(TEST_AUTEUR);
        commentaireBoDtoTest.setDateCreation(new Date());
        commentaireBoDtoTest.setMessage(TEST_MESSAGE);
        commentaireBoDtoTest.setContextId(ID);

        final Set<ConstraintViolation<CommentBoDto>> violations = CommentaireBoDtoTest.validator
            .validate(commentaireBoDtoTest);
        Assert.assertTrue(violations.isEmpty());
    }

    /**
     * Test Validateur DTO KO - {@link UtilisateurBoDto} Email incorrect.
     */
    @Test
    public void testDtoValidationEmailKO() throws Exception
    {
        final CommentBoDto commentaireBoDtoTest = new CommentBoDto();
        commentaireBoDtoTest.setId(ID);
        commentaireBoDtoTest.setVersion(VERSION);
        commentaireBoDtoTest.setAuteur(null);
        commentaireBoDtoTest.setDateCreation(new Date());
        commentaireBoDtoTest.setMessage(null);

        final Set<ConstraintViolation<CommentBoDto>> violations = CommentaireBoDtoTest.validator
            .validate(commentaireBoDtoTest);
        Assert.assertTrue(!violations.isEmpty());
    }

    /**
     * Tester tous les getter et setter de la classe.
     *
     * @throws Exception si le test échoue.
     */
    @Test
    public void testProperties() throws Exception {
        PropertiesTester tester = new PropertiesTester();
        tester.testAll(CommentBoDto.class);
    }
}
