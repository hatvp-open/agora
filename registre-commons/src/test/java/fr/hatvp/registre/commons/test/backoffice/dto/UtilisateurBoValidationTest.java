/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test.backoffice.dto;

import fr.hatvp.registre.commons.dto.backoffice.EspaceOrganisationLockBoDto;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;
import fr.hatvp.registre.commons.lists.backoffice.RoleEnumBackOffice;
import fr.hatvp.registre.commons.utils.RegistreUtils;
import org.agileware.test.PropertiesTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 
 * Classe de test de validation des {@link UtilisateurBoDto}.
 * 
 * @version $Revision$ $Date${0xD}
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {EspaceOrganisationLockBoDto.class},
        loader = AnnotationConfigContextLoader.class)
public class UtilisateurBoValidationTest
{

    /** Email de test. */
    private static final String EMAIL = "test@test.test";

    /** Id de test. */
    private static final long ID = 1L;

    /** Version de test. */
    private static final int VERSION = 0;

    /** Validateur Spring */
    private static Validator validator;

    /**
     * Initialisation des tests.
     *
     * @throws Exception exception d'initialisation.
     */
    @Before
    public void before() throws Exception
    {
        // Init DTO Validator
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    /**
     * Test Validateur DTO OK
     */
    @Test
    public void testDtoValidationOK() throws Exception
    {
        final UtilisateurBoDto utilisateurBoDto = new UtilisateurBoDto();
        utilisateurBoDto.setPassword(RegistreUtils.randomString(30));
        utilisateurBoDto.setEmail(EMAIL);
        utilisateurBoDto.setCompteSupprime(false);
        utilisateurBoDto.setId(ID);
        utilisateurBoDto.setCompteActive(true);
        utilisateurBoDto.setNom(RegistreUtils.randomString(20));
        utilisateurBoDto.setPrenom(RegistreUtils.randomString(20));
        utilisateurBoDto.setVersion(VERSION);

        final List<RoleEnumBackOffice> roleEnumBackOffices = new ArrayList<>();
        roleEnumBackOffices.add(RoleEnumBackOffice.RESTRICTION_ACCES);
        utilisateurBoDto.setRoles(roleEnumBackOffices);

        final Set<ConstraintViolation<UtilisateurBoDto>> violations = UtilisateurBoValidationTest.validator
                .validate(utilisateurBoDto);
        Assert.assertTrue(violations.isEmpty());
    }

    /**
     * Test Validateur DTO KO - {@link UtilisateurBoDto} Email incorrect.
     */
    @Test
    public void testDtoValidationEmailKO() throws Exception
    {
        final UtilisateurBoDto utilisateurBoDto = new UtilisateurBoDto();
        utilisateurBoDto.setPassword(RegistreUtils.randomString(30));
        utilisateurBoDto.setEmail(RegistreUtils.randomString(20));
        utilisateurBoDto.setCompteSupprime(false);
        utilisateurBoDto.setId(ID);
        utilisateurBoDto.setCompteActive(true);
        utilisateurBoDto.setNom(RegistreUtils.randomString(20));
        utilisateurBoDto.setPrenom(RegistreUtils.randomString(20));
        utilisateurBoDto.setVersion(VERSION);

        final List<RoleEnumBackOffice> roleEnumBackOffices = new ArrayList<>();
        roleEnumBackOffices.add(RoleEnumBackOffice.RESTRICTION_ACCES);
        utilisateurBoDto.setRoles(roleEnumBackOffices);

        final Set<ConstraintViolation<UtilisateurBoDto>> violations = UtilisateurBoValidationTest.validator
                .validate(utilisateurBoDto);

        Assert.assertTrue(!violations.isEmpty());
    }

    /**
     * Tester tous les getter et setter de la classe.
     *
     * @throws Exception si le test échoue.
     */
    @Test
    public void testProperties() throws Exception {
        PropertiesTester tester = new PropertiesTester();
        tester.testAll(UtilisateurBoDto.class);
    }
}
