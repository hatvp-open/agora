/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.commons.test.backoffice.dto;

import fr.hatvp.registre.commons.dto.backoffice.EspaceOrganisationLockBoDto;
import org.agileware.test.PropertiesTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * Classe de test des {@link EspaceOrganisationLockBoDto}.
 *
 * @version $Revision$ $Date${0xD}
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {EspaceOrganisationLockBoDto.class},
        loader = AnnotationConfigContextLoader.class)
public class EspaceOrganisationLockBoDtoTest {

    /**
     * Tester tous les getter et setter de la classe.
     *
     * @throws Exception si le test échoue.
     */
    @Test
    public void testProperties() throws Exception {
        PropertiesTester tester = new PropertiesTester();
        tester.testAll(EspaceOrganisationLockBoDto.class);
    }
}
