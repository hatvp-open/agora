/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.espace')

    .controller('ClientCtrl', ['blockUI','$scope', '$rootScope', '$mdDialog', 'periodeActiviteClientList', 'EspaceService', 
        function (blockUI,$scope, $rootScope, $mdDialog, periodeActiviteClientList, EspaceService) {
            var vm = $scope;

            vm.images_dir = $rootScope.images_dir;
            vm.periodeActiviteClientList = periodeActiviteClientList;

            vm.cancel = function () {
                $mdDialog.cancel();
            };


        }]);