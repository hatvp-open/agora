
/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';
angular.module('hatvpRegistre.espace')

/**
 * Controlleur pour modale de contact HATVP
 */
    .controller('AjoutCoCtrl', ['$scope', '$state', '$rootScope', '$mdDialog', '$mdToast', 'blockUI', 'EspaceService','DeclarantsService','espace',
        function ($scope, $state, $rootScope, $mdDialog, $mdToast, blockUI, EspaceService,DeclarantsService,espace) {
            var vm = $scope;
            vm.espace  =espace;
            var root = $rootScope;
            vm.maxFileSizeInMB = root.getMaxUploadSizeInMB();
            vm.files01 = [];
            vm.files02 = [];
            vm.files03 = [];
            vm.nouveau_co;
            vm.message_confirmation = "Vous êtes désormais le contact opérationnel de "+vm.espace.denomination;
            
            
            vm.chargerDeclarant = function (email) {
                DeclarantsService.getDeclarantByMail(email).then(function (res) {
                	if (res.data!="") vm.nouveau_co = res.data;
                	else {
                        $mdDialog.show(
                                $mdDialog.alert()
                                    .parent(angular.element(document.body))
                                    .clickOutsideToClose(true)
                                    .title('Pas de résultat')
                                    .textContent("aucun déclarant n'existe avec cette adresse email")
                                    .ok('OK')
                            );
                	}
                }).finally(function () {
                    blockUI.stop();
                });
            };

            /**
             * Fermer modale.
             */
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            /**
             * Envoyer la demande.
             * @param valid si formulaire valide
             * @param ev $event
             */
            vm.submit = function (valid) {
                if (valid) {
                    blockUI.start();
                    
                    var formData = new FormData();
                    vm.files01.length > 0 && formData.append('file1', vm.files01[0].lfFile);
                    vm.files02.length > 0 && !vm.representantLegal && formData.append('file2', vm.files02[0].lfFile);
                    vm.files03.length > 0 && formData.append('file3', vm.files03[0].lfFile);
                    formData.append('espaceOrganisationId',vm.espace.id);
                    formData.append('declarantId',vm.nouveau_co.id);
                    formData.append('messageConfirmation', vm.message_confirmation);
                    EspaceService.ajoutCO(formData).then(function (res) {
                        //upload success
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Contact opérationnel ajouté')
                                .textContent(vm.nouveau_co.prenom+" "+vm.nouveau_co.nom+" a été ajouté comme contact opérationnel de "+vm.espace.denomination)
                                .ok('OK')
                        );

                    }).finally(function () {
                        blockUI.stop();
                        $state.reload();
                    })
                }
            };

        }]);