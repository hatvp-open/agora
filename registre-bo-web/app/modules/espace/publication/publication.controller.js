/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.espace')

    .controller('PublicationCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'blockUI', '$q', 'publication', 'publicationEspace', 'locked', '$mdDialog', 'EnvConf', 'EspaceService', 'TABLE_OPTIONS',
        function ($scope, $rootScope, $state, $stateParams, blockUI, $q, publication, publicationEspace, locked, $mdDialog, EnvConf, EspaceService, TABLE_OPTIONS) {
            var vm = $scope;

            /***********************************************************
             * Initialisation des variables
             ***********************************************************/
            vm.images_dir = $rootScope.images_dir;

            vm.publication = publication;
            vm.publicationEspace = publicationEspace;
            vm.locked = locked;

            blockUI.start();
            EspaceService.getAllTypeOrganisation().then(function (res) {
                vm.typesOrganisation = res.data;
            }).finally(function (res) {
                blockUI.stop();
            });

            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            vm.publicationTableOptions = angular.copy(TABLE_OPTIONS);
            vm.publicationData = publication.publication;
            vm.dirigeants = vm.publicationData.dirigeants;
            vm.associations = vm.publicationData.affiliations;
            vm.clients = vm.publicationData.clients;
            vm.collaborateurs = vm.publicationData.collaborateurs;
            vm.secteurActivite = vm.publicationData.activites.listSecteursActivites;
            vm.niveauIntervention = vm.publicationData.activites.listNiveauIntervention;

            /**
             * Les block de la publication.
             */
            vm.block = {
                dirigeant: EnvConf.views_dir + "/espace/publication/cards/dirigeant.html",
                collaborateur: EnvConf.views_dir + "/espace/publication/cards/collaborateur.html",
                client: EnvConf.views_dir + "/espace/publication/cards/client.html",
                association: EnvConf.views_dir + "/espace/publication/cards/association.html",
                activite: EnvConf.views_dir + "/espace/publication/cards/activite_interet.html",
                finance: EnvConf.views_dir + "/espace/publication/cards/finance.html",
                internet: EnvConf.views_dir + "/espace/publication/cards/internet.html",
                contact: EnvConf.views_dir + "/espace/publication/cards/contact.html",
                localisation: EnvConf.views_dir + "/espace/publication/cards/localisation.html",
                profil: EnvConf.views_dir + "/espace/publication/cards/profil.html"
            };

            /**
             * Republier une publication dépubliée
             * @param ev
             */
            vm.republierPublication = function (ev) {
                var title = "Republication",
                    message = "La republication d’un contenu provoque sa publication sur le site internet de la HATVP." +
                        " Confirmez-vous cette action ?";

                var confirm = $mdDialog.confirm()
                    .title(title)
                    .textContent(message)
                    .ariaLabel(title)
                    .targetEvent(ev)
                    .ok('Confirmer')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function () {
                    EspaceService.republierPublication(vm.publication.id).then(function (res) {
                        vm.publication.statut = 'REPUBLIEE';
                        var alert = $mdDialog.alert()
                            .title("Publication republiée")
                            .textContent("La publication est maintenant republiée.")
                            .ariaLabel("Publication republiée")
                            .targetEvent(ev)
                            .ok('OK');

                        $mdDialog.show(alert);
                        $state.reload();
                    });
                });
            };

            /**
             * Dépublier une publication (re)publiée
             * @param ev
             */
            vm.depublierPublication = function (ev) {
                var title = "Dé-publication",
                    message = "La dé-publication d’un contenu provoque sa suppression du site internet de la HATVP." +
                        " Confirmez-vous cette action ?";

                var confirm = $mdDialog.confirm()
                    .title(title)
                    .textContent(message)
                    .ariaLabel(title)
                    .targetEvent(ev)
                    .ok('Confirmer')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function () {
                    EspaceService.depublierPublication(vm.publication.id).then(function (res) {
                        vm.publication.statut = 'DEPUBLIEE';
                        var alert = $mdDialog.alert()
                            .title("Publication dépubliée")
                            .textContent("La publication est maintenant dépubliée.")
                            .ariaLabel("Publication dépubliée")
                            .targetEvent(ev)
                            .ok('OK');

                        $mdDialog.show(alert);
                        $state.reload();
                    });
                });
            };

        }]);