/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.espace')

    .controller('FicheCtrl', ['blockUI','$scope', '$rootScope', '$mdDialog', 'ficheId', 'locked', 'EspaceService', 'QualiteService', 'activiteHistoriquePublicationList', 'EnvConf', 'HTML',
        function (blockUI,$scope, $rootScope, $mdDialog, ficheId, locked, EspaceService, QualiteService, activiteHistoriquePublicationList, EnvConf, HTML) {
            var vm = $scope;
            vm.locked = locked;
            vm.images_dir = $rootScope.images_dir;
            if(activiteHistoriquePublicationList){
                vm.activiteHistoriquePublicationList = activiteHistoriquePublicationList;
            }

            if(activiteHistoriquePublicationList == null){
                EspaceService.getFiche(ficheId).then(function (res) {
                    vm.fiche = res.data;
                    QualiteService.getLastQualifObjetByFicheId(vm.fiche.idFiche).then(function (res) {
                        vm.qualif = res.data;
                    }).finally(function () {
                    });
    
                }).finally(function () {
                });
            }           

            vm.cancel = function () {
                $mdDialog.cancel();
            };

            vm.getKey = function (value, map) {
                for (var key in map) {
                    if (map[key] == value) {
                        return key;
                    }
                }
            };     
            
            /**
             * Dépublication d'une fiche
             * @param ev
             */
            vm.depublierFiche = function (fiche) {
                var title = "Dé-publication Fiche d'activités",
                    message = "Vous êtes sur le point de dé-publier  la fiche ayant pour objet <b>"+fiche.objet +
                        "</b>.<br> Confirmez-vous cette action ?";

                var confirm = $mdDialog.confirm()
                    .title(title)
                    .htmlContent(message)
                    .ariaLabel(title)
                    .ok('Confirmer')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function () {
                    blockUI.start();
                    EspaceService.depublierFiche(fiche.id).then(function (res) {
                        $mdDialog.show($mdDialog.alert()
                            .title("fiche dépubliée")
                            .textContent("La fiche est dépubliée")
                            .ariaLabel("fiche dépubliée")
                            .ok("OK")
                            .clickOutsideToClose(true));
                    }).finally(function () {
                        blockUI.stop();
                        $state.reload();
                    });
                });
            };


            /**
             * Republication d'une fiche
             * @param ev
             */
            vm.republierFiche = function (fiche) {
                var title = "Republication globale d'une fiche",
                    message = "Vous êtes sur le point de republier la fiche ayant pour objet <b>"+fiche.objet +
                        "</b>.<br> Confirmez-vous cette action ?";

                var confirm = $mdDialog.confirm()
                    .title(title)
                    .htmlContent(message)
                    .ariaLabel(title)
                    .ok('Confirmer')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function () {
                    blockUI.start();
                    EspaceService.republierFiche(fiche.id).then(function (res) {
                        $mdDialog.show($mdDialog.alert()
                            .title("Publications activées")
                            .textContent("La fiche sera republiée dans 15 minutes")
                            .ariaLabel("Publications activées")
                            .ok("OK")
                            .clickOutsideToClose(true));
                    }).finally(function () {
                        blockUI.stop();
                        $state.reload();
                    });
                });
            };
            /**
             * Affiche la fiche historisée à partir des données de publication
             * @param {*} ev 
             * @param {*} publicationActiviteId 
             */
            vm.openFiche = function (ev, publicationActiviteId) {
                    $mdDialog.show({
                        controller: 'FicheHistoriseCtrl',
                        templateUrl: EnvConf.views_dir + HTML.ESPACE.FICHE,
                        locals: {
                            publicationActiviteId: publicationActiviteId,
                        },
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        fullscreen: true
                    });
               
            };
        }])
        
        .controller('FicheHistoriseCtrl', ['blockUI','$scope', '$rootScope', '$mdDialog', 'EspaceService', 'EnvConf', 'HTML','publicationActiviteId',
        function (blockUI,$scope, $rootScope, $mdDialog, EspaceService, EnvConf, HTML, publicationActiviteId) {
            var vm = $scope;
            vm.images_dir = $rootScope.images_dir;

            EspaceService.getHistoriqueFiche(publicationActiviteId).then(function (res) {
                vm.fiche = res.data;
            }).finally(function () {
            });

            vm.getKey = function (value, map) {
                for (var key in map) {
                    if (map[key] == value) {
                        return key;
                    }
                }
            }; 

            vm.cancel = function () {
                $mdDialog.cancel();
            };
        }]);