/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.espace')

/**
 * Controlleur de la page contenat la liste des espaces organisations.
 */
    .controller('EspaceCtrl', ['blockUI', '$q', '$scope', '$state', '$stateParams', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'USER_ROLES', 'EspaceService',
        function (blockUI, $q, $scope, $state, $stateParams, $log, $mdDialog, $mdToast, EnvConf, HTML, TABLE_OPTIONS, USER_ROLES, EspaceService) {

            /***********************************************************
             * Initialisation.
             ***********************************************************/
            var vm = $scope;
            vm.espaces = [];
            vm.espacesTableOptions = angular.copy(TABLE_OPTIONS);
            vm.search = false;
            vm.typeDeRecherche = "";

            /***********************************************************
             * Déclaration des fonctions.
             ***********************************************************/
            var initEspacesTable = function (page, limit) {
                // reinit des variables de recherche
                vm.search = false;
                vm.typeDeRecherche = "";
                vm.denomination = "";
                vm.dateCreation = "";
                vm.nombreDeclarant = "";
                vm.identifiant = "";
                vm.administrateurs = "";

                blockUI.start();
                $q.all([
                    vm.espacesTableOptions.promise = EspaceService.getValidatedEspacePaginated(page, limit),
                    vm.espacesTableOptions.promise.then(function (res) {
                        vm.espaces = res.data.dtos.map(function (e) {
                            return {
                                actif: e.actif,
                                id: e.id,
                                organisationId: e.organisationId,
                                publication: e.isPublication,
                                denomination: e.denomination,
                                nomUsage: e.nomUsage,
                                nomUsageHatvp: e.nomUsageHatvp,
                                sigleHatvp: e.sigleHatvp,
                                ancienNomHatvp: e.ancienNomHatvp,
                                finExerciceFiscal: e.finExerciceFiscal,
                                identifiant: e.nationalId,
                                relancesBloquees: e.relancesBloquees,
                                administrateurs: e.administrateurs.map(function (i) {
                                    return i.identite;
                                }).join(", "),
                                admins: e.administrateurs.map(function (i) {
                                    return {
                                        id: i.id,
                                        identite: i.identite,
                                        version: i.version
                                    };
                                }),
                                nombreDeclarant: e.nombreDeclarant,
                                dateCreation: e.creationDate,
                                data: {
                                    id: e.id,
                                    denomination: e.denomination,
                                    nomUsage: e.nomUsage,
                                    nomUsageHatvp: e.nomUsageHatvp,
                                    sigleHatvp: e.sigleHatvp,
                                    ancienNomHatvp: e.ancienNomHatvp,
                                    finExerciceFiscal: e.finExerciceFiscal,
                                    jourFin: e.finExerciceFiscal === undefined ? undefined : e.finExerciceFiscal.split("-")[0] < 10 ? e.finExerciceFiscal.split("-")[0].substring(1) : e.finExerciceFiscal.split("-")[0],
                                    moisFin: e.finExerciceFiscal === undefined ? undefined : e.finExerciceFiscal.split("-")[1] < 10 ? e.finExerciceFiscal.split("-")[1].substring(1) : e.finExerciceFiscal.split("-")[1]
                                },
                                desinscriptionAgent: e.desinscription_agent,
                                dateDesinscription: e.dateDesinscription
                            };
                        });
                        vm.espaces.totalNumber = res.data.resultTotalNumber;
                    })
                ]).then(function () {
                }).finally(function () {
                    blockUI.stop();
                });
            };
            var searchTable = function (type, page, limit) {
                vm.search = true;
                vm.typeDeRecherche = type;

                switch (type) {
                    case 'denomination':
                        vm.espacesTableOptions.promise = EspaceService.getValidatedEspacePaginatedParRecherche(type, vm.denomination, page, limit);
                        break;
                    case 'identifiant':
                        vm.espacesTableOptions.promise = EspaceService.getValidatedEspacePaginatedParRecherche(type, vm.identifiant, page, limit);
                        break;
                    case 'administrateurs':
                        vm.espacesTableOptions.promise = EspaceService.getValidatedEspacePaginatedParRecherche(type, vm.administrateurs, page, limit);
                        break;
                    case 'nbrDeclarants':
                        vm.espacesTableOptions.promise = EspaceService.getValidatedEspacePaginatedParRecherche(type, vm.nombreDeclarant, page, limit);
                        break;
                    case 'dateCreation':
                        vm.espacesTableOptions.promise = EspaceService.getValidatedEspacePaginatedParRecherche(type, vm.dateCreation.replace('/', '-'), page, limit);
                        break;
                    default:
                        vm.espacesTableOptions.promise = EspaceService.getValidatedEspacePaginated(page, limit);
                }

                vm.espacesTableOptions.promise.then(function (res) {
                    vm.espaces = res.data.dtos.map(function (e) {
                        return {
                            actif: e.actif,
                            id: e.id,
                            organisationId: e.organisationId,
                            publication: e.isPublication,
                            denomination: e.denomination,
                            nomUsage: e.nomUsage,
                            nomUsageHatvp: e.nomUsageHatvp,
                            sigleHatvp: e.sigleHatvp,
                            ancienNomHatvp: e.ancienNomHatvp,
                            finExerciceFiscal: e.finExerciceFiscal,
                            identifiant: e.nationalId,
                            relancesBloquees: e.relancesBloquees,
                            administrateurs: e.administrateurs.map(function (i) {
                                return i.identite;
                            }).join(", "),
                            admins: e.administrateurs.map(function (i) {
                                return {
                                    id: i.id,
                                    identite: i.identite,
                                    version: i.version
                                };
                            }),
                            nombreDeclarant: e.nombreDeclarant,
                            dateCreation: e.creationDate,
                            data: {
                                id: e.id,
                                denomination: e.denomination,
                                nomUsage: e.nomUsage,
                                nomUsageHatvp: e.nomUsageHatvp,
                                sigleHatvp: e.sigleHatvp,
                                ancienNomHatvp: e.ancienNomHatvp,
                                finExerciceFiscal: e.finExerciceFiscal,
                                jourFin: e.finExerciceFiscal === undefined ? undefined : e.finExerciceFiscal.split("-")[0] < 10 ? e.finExerciceFiscal.split("-")[0].substring(1) : e.finExerciceFiscal.split("-")[0],
                                moisFin: e.finExerciceFiscal === undefined ? undefined : e.finExerciceFiscal.split("-")[1] < 10 ? e.finExerciceFiscal.split("-")[1].substring(1) : e.finExerciceFiscal.split("-")[1]
                            },
                            desinscriptionAgent: e.desinscription_agent,
                            dateDesinscription: e.dateDesinscription
                        };
                    });
                    vm.espaces.totalNumber = res.data.resultTotalNumber;

                    vm.saveSearch = {
                        search: vm.search,
                        typeDeRecherche: vm.typeDeRecherche,
                        denomination: vm.denomination,
                        dateCreation: vm.dateCreation,
                        nombreDeclarant: vm.nombreDeclarant,
                        identifiant: vm.identifiant,
                        administrateurs: vm.administrateurs
                    };
                });

                blockUI.start();
                $q.all([
                    vm.espacesTableOptions.promise
                ]).finally(function () {
                    blockUI.stop();
                });
            };

            /**
             * Appel paginé pour la liste des mandants
             * @param denomination
             * @param page
             * @param limit
             * @returns {*}
             */
            vm.onPaginateSearch = function (page, limit) {
                searchTable(vm.typeDeRecherche, page, limit);
            };

            /**
             * Reset du tableau sans recherches
             */
            vm.reset = function () {
                initEspacesTable(vm.espacesTableOptions.page, vm.espacesTableOptions.limit);
            };

            vm.callSearch = function (typeDeRecherche) {
                vm.typeDeRecherche = typeDeRecherche;
                searchTable(typeDeRecherche, 1, vm.espacesTableOptions.limit);
            };

            //Go to espace details page.
            vm.espaceDetails = function (event, espaceContent) {
                $state.go('espaceDetails', {
                    espace: espaceContent,
                    menuBarPreviousTitle: 'Liste dses espace organisations validé',
                    returnState: 'espaces',
                    cardOrga: {active: false},
                    saveSearch: vm.saveSearch
                });
            };

            /**
             * Appel paginé pour la liste des espaces
             * @param page
             * @param limit
             * @returns {*}
             */
            vm.onPaginate = function (page, limit) {
                initEspacesTable(page, limit);
            };

            /***********************************************************
             * Initialisation du controller
             ***********************************************************/
            if ($stateParams.saveSearch !== null && $stateParams.saveSearch.search) {
                vm.search = $stateParams.saveSearch.search;
                vm.typeDeRecherche = $stateParams.saveSearch.typeDeRecherche;
                vm.denomination = $stateParams.saveSearch.denomination;
                vm.dateCreation = $stateParams.saveSearch.dateCreation;
                vm.nombreDeclarant = $stateParams.saveSearch.nombreDeclarant;
                vm.identifiant = $stateParams.saveSearch.identifiant;
                vm.administrateurs = $stateParams.saveSearch.administrateurs;
                searchTable(vm.typeDeRecherche, 1, vm.espacesTableOptions.limit);
            } else {
                initEspacesTable(vm.espacesTableOptions.page, vm.espacesTableOptions.limit);
            }
        }])
    /**
     * Controlleur de la page contenant la liste des espaces organisations.
     */
    .controller('EspaceDetailsCtrl', ['$stateParams', 'blockUI', '$q', '$scope', '$state', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'API', 'HTML', 'TABLE_OPTIONS', 'USER_ROLES', 'MANDAT_GESTION_TYPE', 'MANDAT_GESTION_STATUT', 'EspaceDetailsScope', 'EspaceService','DesinscriptionService',
        function ($stateParams, blockUI, $q, $scope, $state, $log, $mdDialog, $mdToast, EnvConf, API, HTML, TABLE_OPTIONS, USER_ROLES, MANDAT_GESTION_TYPE, MANDAT_GESTION_STATUT, EspaceDetailsScope, EspaceService, DesinscriptionService) {

            /***********************************************************
             * Initialisation.
             ***********************************************************/
            var vm = $scope;
            vm.nombreJoursDebut = 31;
            vm.nombreJoursFin = 31;
            vm.joursList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
            vm.moisList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
            vm.moisListLabel = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
            vm.selected = [];

            EspaceDetailsScope.store('EspaceDetailsCtrl', $scope);

            /**
             * URL pour le service de téléchargement des pièces.
             */
            vm.pieceDownloadUrl = EnvConf.api + API.corporate.root + API.corporate.pieces.root + API.corporate.pieces.download;
            /**
             * URL pour le service de téléchargement des pièces de désinscription.
             */
            vm.pieceDesinscriptionDownloadUrl = EnvConf.api + API.corporate.desinscription.root + API.corporate.desinscription.download;

            if ($stateParams.espace !== null && $stateParams.menuBarPreviousTitle !== null && $stateParams.returnState !== null && $stateParams.cardOrga !== null) {
                vm.espace = {};
                vm.espace = $stateParams.espace;
                vm.cardOrga = $stateParams.cardOrga;
                vm.menuBarPreviousTitle = $stateParams.menuBarPreviousTitle;
                vm.returnState = $stateParams.returnState;
            } else {
                $state.go('espaces');
            }

            vm.views = {
                PIECES: EnvConf.views_dir + HTML.ESPACE.TABS.PIECES_LIST,
                DECLARANT: EnvConf.views_dir + HTML.ESPACE.TABS.DECLARANT_LIST,
                PUBLICATION: EnvConf.views_dir + HTML.ESPACE.TABS.PUBLICATION_LIST,
                RAPPORT: EnvConf.views_dir + HTML.ESPACE.TABS.RAPPORT_LIST,
                CLIENT: EnvConf.views_dir + HTML.ESPACE.TABS.CLIENT_LIST,
                COLLABORATEUR: EnvConf.views_dir + HTML.ESPACE.TABS.COLLABORATEUR_LIST,
                AFFILIATION: EnvConf.views_dir + HTML.ESPACE.TABS.AFFILIATION_LIST,
                RELANCES: EnvConf.views_dir + HTML.ESPACE.TABS.RELANCES_LIST
            };

            vm.declarants = [];
            vm.declarantsTableOptions = angular.copy(TABLE_OPTIONS);

            vm.publications = [];
            vm.publicationsTableOptions = angular.copy(TABLE_OPTIONS);

            vm.clients = [];
            vm.clientsTableOptions = angular.copy(TABLE_OPTIONS);

            vm.collaborateurs = [];
            vm.collaborateursTableOptions = angular.copy(TABLE_OPTIONS);

            vm.affiliations = [];
            vm.affiliationsOptions = angular.copy(TABLE_OPTIONS);

            vm.rapport = {};
            vm.exercices = [];
            vm.activites = [];
            vm.activitesTableOptions = angular.copy(TABLE_OPTIONS);

            vm.piecesMandant = [];
            vm.mandatsStatus = angular.copy(MANDAT_GESTION_TYPE);

            /***********************************************************
             * Déclaration des fonctions.
             ***********************************************************/

            var initData = function () {
                blockUI.start();
                $q.all([
                    EspaceService.getEspaceOrganisationDeclarant(vm.espace.id).then(function (res) {
                        vm.declarants = res.data.map(function (e) {
                            return {
                                id: e.declarantId,
                                civilite: e.civilite,
                                nom: e.declarantNomComplet,
                                email: e.declarantEmail,
                                date_naissance: e.dateNaissance,
                                date_validation: e.dateValidation,
                                date_demande: e.dateDemande,
                                etat_refus: angular.isDefined(e.dateSuppression),
                                etat_demande: angular.isDefined(e.dateValidation),
                                etat_activation: e.actif,
                                contributor: e.roles.includes(USER_ROLES.contributor),
                                publisher: e.roles.includes(USER_ROLES.publisher),
                                administrator: e.roles.includes(USER_ROLES.administrator),
                                locked: e.verrouille,
                                currentOrgaId: e.espaceOrganisationId,
                                fav: e.favori,
                                declarantInscriptionId: e.id
                            };
                        });
                    }),

                    EspaceService.getOneEspace(vm.espace.id).then(function (res) {
                        vm.espaceContent = res.data.espaceOrganisation;
                        vm.espaceContent.publicationIdentiteList = res.data.publicationIdentiteList;
                        vm.espace.data.adresse = res.data.espaceOrganisation.adresse; 
                        vm.espace.data.codePostal = res.data.espaceOrganisation.codePostal;
                        vm.espace.data.ville = res.data.espaceOrganisation.ville;
                        vm.espace.data.pays = res.data.espaceOrganisation.pays;
                        vm.espace.data.telephoneDeContact = res.data.espaceOrganisation.telephoneDeContact;
                        vm.espace.data.emailDeContact = res.data.espaceOrganisation.emailDeContact;
                        vm.espace.data.lienSiteWeb = res.data.espaceOrganisation.lienSiteWeb;
                        vm.espace.data.lienPageLinkedin = res.data.espaceOrganisation.lienPageLinkedin;
                        vm.espace.data.lienPageTwitter = res.data.espaceOrganisation.lienPageTwitter;
                        vm.espace.data.lienPageFacebook = res.data.espaceOrganisation.lienPageFacebook;
                    }),
                    EspaceService.getPublications(vm.espace.id).then(function (res) {
                            vm.publications = res.data;
                    }),

                    EspaceService.getNomenclatures().then(function (res) {
                        vm.nomenclatures = res.data;

                        EspaceService.getRapports(vm.espace.id).then(function (res) {

                            vm.exercices = res.data.map(function (e) {

                                if (toDate(e.dateFin) > new Date() && toDate(e.dateDebut) < new Date()) {
                                    vm.rapport.exercice = e;
                                    vm.rapport.exercice.periode = e.dateDebut + " au " + e.dateFin;
                                    vm.activites = vm.rapport.exercice.activites;
                                }

                                return {
                                    id: e.id,
                                    dateDebut: e.dateDebut,
                                    dateFin: e.dateFin,
                                    chiffreAffaire: e.hasNotChiffreAffaire ? "Organisation sans chiffre d'affaires" : (e.chiffreAffaire == undefined) ? "Non renseigné" : vm.nomenclatures.nomenclature.CHIFFRE_AFFAIRE[e.chiffreAffaire - 1].libelle,
                                    hasNotChiffreAffaire: e.hasNotChiffreAffaire,
                                    montantDepense: (e.montantDepense == undefined) ? "Non renseigné" : vm.nomenclatures.nomenclature.MONTANT_DEPENSE[e.montantDepense - 1].libelle,
                                    nombreSalaries: (e.nombreSalaries == undefined) ? "Non renseigné" : e.nombreSalaries,
                                    tempToSort: toDate(e.dateDebut),
                                    periode: e.dateDebut + " au " + e.dateFin,
                                    statut: e.statut.label,
                                    publication: e.isPublication,
                                    noActivite: e.noActivite,
                                    relancesBloquees: e.relancesBloquees,
                                    dateFirstCommMoyens: Date.parse(e.dateFirstCommMoyens),
                                    dateLastChangeMoyens: Date.parse(e.dateLastChangeMoyens),
                                    activites: e.activiteSimpleDto.map(function (a) {
                                        return {
                                            id: a.id,
                                            objet: a.objet,
                                            statut: a.statut.label,
                                            domainesIntervention: a.domainesIntervention.join(', '),
                                            nomCreateur: a.nomCreateur,
                                            firstPub: Date.parse(a.dateFirstComm),
                                            lastPub: Date.parse(a.dateLastChange),
                                            dateCreation: a.dateCreation,
                                            idFiche: a.idFiche,
                                            activiteHistoriquePublicationList: a.activiteHistoriquePublicationList.map(function (p) {
                                                return {
                                                    publicationActiviteId: p.publicationActiviteId,
                                                    objet: p.objet,
                                                    dateCreation: p.dateCreation
                                                }
                                            }),   
                                        }
                                    }),
                                    isPublished: e.isPublished,
                                    exerciceHistoriquePublicationList: e.exerciceHistoriquePublicationList.map(function (a) {
                                        return {
                                            dateCreation: a.dateCreation,
                                            chiffreAffaire: a.chiffreAffaire != 'null' ? a.chiffreAffaire : "Organisation sans chiffre d'affaires",
                                            montantDepense: a.montantDepense,
                                            nombreSalaries: a.nombreSalaries
                                        }
                                    })
                                };
                            });
                            if (!vm.rapport.exercice && res.data.length > 0) {
                                vm.rapport.exercice = res.data.reduce(function (max, p) {
                                    if (toDate(p.dateFin) > toDate(max.dateFin)) {
                                        return p;
                                    } else {
                                        return max;
                                    }
                                });
                                vm.rapport.exercice.periode = vm.rapport.exercice.dateDebut + " au " + vm.rapport.exercice.dateFin;
                                vm.activites = vm.rapport.exercice.activites;
                            }
                            //Après suppression activité on raffiche la période sur laquelle on était
                            if($stateParams.exercice != undefined){
                                vm.rapport.exercice = $stateParams.exercice;
                            }

                        })
                    }),
                    EspaceService.getComplement(vm.espace.id).then(function (res) {
                        vm.complement = res.data;
                    }),
                    EspaceService.getPiecesCreation(vm.espace.id).then(function (res) {
                        vm.piecesCreation = res.data;
                    }),
                    DesinscriptionService.getPiecesDesinscription(vm.espace.id).then(function (res) {
                        vm.piecesDesinscription = res.data;
                    }),
                    EspaceService.getPiecesMandant(vm.espace.id).then(function (res) {
                        res.data.map(function (p) {

                            if (p.identitePieceRepresentantLegal !== undefined) {

                                vm.identitePieceRepresentantLegal = {
                                    idDemande: p.id,
                                    id: p.identitePieceRepresentantLegal.id,
                                    nomFichier: p.identitePieceRepresentantLegal.nomFichier,
                                    dateVersementPiece: p.identitePieceRepresentantLegal.dateVersementPiece,
                                    typeDemande: vm.mandatsStatus[p.typeDemande],
                                    status: p.statut.label
                                };
                                vm.piecesMandant.push(vm.identitePieceRepresentantLegal);
                            }

                            if (p.mandatPieceRepresentantLegal !== undefined) {

                                vm.mandatPieceRepresentantLegal = {
                                    idDemande: p.id,
                                    id: p.mandatPieceRepresentantLegal.id,
                                    nomFichier: p.mandatPieceRepresentantLegal.nomFichier,
                                    dateVersementPiece: p.mandatPieceRepresentantLegal.dateVersementPiece,
                                    typeDemande: vm.mandatsStatus[p.typeDemande],
                                    status: p.statut.label
                                };
                                vm.piecesMandant.push(vm.mandatPieceRepresentantLegal);
                            }

                            p.complementPieceRepresentantLegal.forEach(function (piece) {

                                vm.complementRepresentantLegal = {
                                    idDemande: p.id,
                                    id: piece.id,
                                    nomFichier: piece.nomFichier,
                                    dateVersementPiece: piece.dateVersementPiece,
                                    typeDemande: vm.mandatsStatus[p.typeDemande],
                                    status: p.statut.label
                                };

                                vm.piecesMandant.push(vm.complementRepresentantLegal);
                            });

                        });
                    }),
                    EspaceService.getClients(vm.espace.id).then(function(res) {
                        vm.clients = res.data.map(function (e) {
                            return {
                                id: e.espaceOrganisationIdDeOrdganisation,
                                denomination: e.denomination,
                                nomUsage: e.nomUsage,
                                nationalId: e.nationalId,
                                originNationalId: e.originNationalId,
                                dateAjout: e.dateAjout,
                                dateDesactivation: e.dateDesactivation,
                                isAncienClient: e.ancienClient,
                                espaceOrganisationIdDeOrdganisation : e.espaceOrganisationIdDeOrdganisation,
                                espaceCollaboratif : e.espaceCollaboratifDto,
                                datePremierePublication: e.datePremierePublication,
                                dateDernierePublication: e.dateDernierePublication,
                                periodeActiviteClientList: e.periodeActiviteClientList,
                                periodeActiviteClientEnCours: e.periodeActiviteClientEnCours
                            };
                        });
                    }),

                    EspaceService.getCollaborateurs(vm.espace.id).then(function(res){
                        vm.collaborateurs = res.data.map(function (e){
                            return{
                                id: e.declarantId,
                                declarantId: e.declarantId,
                                civilite: e.civilite,
                                nom: e.nom,
                                prenom: e.prenom,
                                email: e.email,
                                fonction: e.fonction,
                                inscrit: e.inscrit,
                                actif: e.actif,
                                dateAjout: e.enregistrementDate,
                                dateDesactivation: e.dateDesactivation,
                                datePublication: e.datePublication
                            };
                        });
                    }),
                    EspaceService.getAffiliations(vm.espace.id).then(function(res) {
                        vm.affiliations = res.data.map(function (e) {
                            return {
                                id: e.espaceOrganisationIdDeOrdganisation,
                                denomination: e.denomination,
                                nationalId: e.nationalId,
                                originNationalId: e.originNationalId,
                                espaceOrganisationIdDeOrdganisation : e.espaceOrganisationIdDeOrdganisation,
                                espaceCollaboratif : e.espaceCollaboratifDto,
                                datePremierePublication: e.datePremierePublication
                            };
                        });
                    })

                ]).then(function () {
                }).finally(function () {
                    blockUI.stop();
                });
            };

            vm.getMandatesDetails = function (idMandat) {
                $state.go('mandatesDetails', {
                    idMandat: idMandat
                })
            };


            /**
             * Dépublication des publications de l'espace
             * @param ev
             */
            vm.depublierPublicationsEspace = function (ev) {
                var title = "Dé-publication globale",
                    message = "Vous êtes sur le point de dé-publier l’ensemble des publications de l’espace collaboratif." +
                        " Confirmez-vous cette action ?";

                var confirm = $mdDialog.confirm()
                    .title(title)
                    .textContent(message)
                    .ariaLabel(title)
                    .targetEvent(ev)
                    .ok('Confirmer')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function () {
                    blockUI.start();
                    EspaceService.depublierPublicationsEspace(vm.espace.id).then(function (res) {
                        $mdDialog.show($mdDialog.alert()
                            .title("Publications désactivées")
                            .textContent("Les publications sont maintenant désactivées sur l'espace collaboratif")
                            .ariaLabel("Publications désactivées")
                            .targetEvent(ev)
                            .ok("OK")
                            .clickOutsideToClose(true));
                        vm.espace.publication = false;
                    }).finally(function () {
                        blockUI.stop();
                    });
                });
            };


            /**
             * Dépublication des moyens d'un exercice
             * @param ev
             */
            vm.depublierExercice = function (exercice) {
                var title = "Dé-publication Moyens alloués",
                    message = "Vous êtes sur le point de dé-publier  l'exercice <b>" + exercice.periode +
                        "</b>.<br> Cette dépublication va entrainer la dépublication des moyens ainsi que des fiches d'activités de cet exercice, confirmez-vous cette action ?";

                var confirm = $mdDialog.confirm()
                    .title(title)
                    .htmlContent(message)
                    .ariaLabel(title)
                    .ok('Confirmer')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function () {
                    blockUI.start();
                    EspaceService.depublierExercice(exercice.id).then(function (res) {
                        $mdDialog.show($mdDialog.alert()
                            .title("exercice dépublié")
                            .textContent("L'exercice est dépublié")
                            .ariaLabel("exercice dépublié")
                            .ok("OK")
                            .clickOutsideToClose(true));
                        //vm.espace.publication = false;
                    }).finally(function () {
                        blockUI.stop();
                        $state.reload();
                    });
                });
            };


            /**
             * Republication d'un exercice
             * @param ev
             */
            vm.republierExercice = function (exercice) {
                var title = "Republication globale d'un exercice",
                    message = "Vous êtes sur le point de republier l’ensemble de l'exercice  <b>" + exercice.periode +
                        "</b>.<br> Cela va entrainer la republication des moyens et des fiches d'activités de cet exercice, confirmez-vous cette action ?";

                var confirm = $mdDialog.confirm()
                    .title(title)
                    .htmlContent(message)
                    .ariaLabel(title)
                    .ok('Confirmer')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function () {
                    blockUI.start();
                    EspaceService.republierExercice(exercice.id).then(function (res) {
                        $mdDialog.show($mdDialog.alert()
                            .title("Publications activées")
                            .textContent("L'exercice sera republié dans 15 minutes")
                            .ariaLabel("Publications activées")
                            .ok("OK")
                            .clickOutsideToClose(true));
                    }).finally(function () {
                        blockUI.stop();
                        $state.reload();
                    });
                });
            };

            vm.setRelancesBloquees = function(){
                blockUI.start();
                if (vm.espace.relancesBloquees){
                    EspaceService.bloquerRelances(vm.espace.id).then(function (res) {
                        $mdDialog.show($mdDialog.alert()
                            .title("Relances bloquées")
                            .textContent("Les relances sont bloquées pour cet espace")
                            .ariaLabel("Relances bloquées")
                            .ok("OK")
                            .clickOutsideToClose(true));
                    }).finally(function () {
                        blockUI.stop();
                    });
                }
                else {
                    EspaceService.deBloquerRelances(vm.espace.id).then(function (res) {
                        $mdDialog.show($mdDialog.alert()
                            .title("Relances débloquées")
                            .textContent("Les relances sont débloquées pour cet espace")
                            .ariaLabel("Relances débloquées")
                            .ok("OK")
                            .clickOutsideToClose(true));
                    }).finally(function () {
                        blockUI.stop();
                    });
                }
            };
            /**
             * Republication des publications de l'espace
             * @param ev
             */
            vm.republierPublicationsEspace = function (ev) {
                var title = "Republication globale",
                    message = "Vous êtes sur le point de republier l’ensemble des publications de l’espace collaboratif." +
                        " Confirmez-vous cette action ?";

                var confirm = $mdDialog.confirm()
                    .title(title)
                    .textContent(message)
                    .ariaLabel(title)
                    .targetEvent(ev)
                    .ok('Confirmer')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function () {
                    blockUI.start();
                    EspaceService.republierPublicationsEspace(vm.espace.id).then(function (res) {
                        $mdDialog.show($mdDialog.alert()
                            .title("Publications activées")
                            .textContent("Les publications sont maintenant activées sur l'espace collaboratif")
                            .ariaLabel("Publications activées")
                            .targetEvent(ev)
                            .ok("OK")
                            .clickOutsideToClose(true));
                        vm.espace.publication = true;
                    }).finally(function () {
                        blockUI.stop();
                    });
                });
            };

            /**
             * Ouvrir les détails d'une publication
             * @param ev event
             * @param vo objet publication
             */
            vm.openPublication = function (ev, vo) {
                $mdDialog.show({
                    controller: 'PublicationCtrl',
                    templateUrl: EnvConf.views_dir + HTML.ESPACE.PUBLICATION,
                    locals: {
                        publication: vo,
                        publicationEspace: vm.espace.publication,
                        locked: vm.espace.locked
                    },
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            };


            //Go to declarant details page.
            vm.declarantDetails = function (event, declarantContent, espaceDorigine, admin) {
                //gestion des collaborateurs qui n'ont pas d'espace
                if(declarantContent.id>0){
                    blockUI.start();
                    $scope.espaceDorigine= espaceDorigine;

                    $state.go('declarantDetails', {
                        declarant: declarantContent,
                        espaceDorigine: espaceDorigine,
                        returnState: 'espaceDetails',
                        menuBarPreviousTitle: 'Retour à l\'espace collaboratif',
                    });
                }
            };

            vm.openDeclarant = function (declarant, ev) {
                $mdDialog.show({
                    controller: 'DeclarantEditCtrl',
                    locals: {
                        item: declarant
                    },
                    templateUrl: EnvConf.views_dir + HTML.DECLARANTS.DECLARANT_EDIT,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: true
                }).finally(function () {
                    refresh();
                });
            };
            vm.ajoutCO = function (ev) {
                $mdDialog.show({
                    targetEvent: ev,
                    controller: 'AjoutCoCtrl',
                    locals: {
                        espace: vm.espace
                    },
                    templateUrl: EnvConf.views_dir + HTML.ESPACE.AJOUT_CO,
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    escapeToClose: false,
                    fullscreen: false,
                    disableParentScroll: false
                });
            };
            /**
             * Afficher les détails d'une fiche en cours
             * @param ev event
             * @param vo objet publication
             */
            vm.openFiche = function (ev, ficheId) {
                $mdDialog.show({
                    controller: 'FicheCtrl',
                    templateUrl: EnvConf.views_dir + HTML.ESPACE.FICHE,
                    locals: {
                        ficheId: ficheId,
                        locked : vm.espace.locked,
                        activiteHistoriquePublicationList: null
                    },
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            };

            vm.getActiviteHistorique = function (ev, fiche) {
                $mdDialog.show({
                    controller: 'FicheCtrl',
                    templateUrl: EnvConf.views_dir + HTML.ESPACE.FICHE_HISTORIQUE,
                    locals: {
                        ficheId: fiche.id,
                        activiteHistoriquePublicationList: fiche.activiteHistoriquePublicationList,
                        locked : vm.espace.locked
                    },
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            };

            /**
             * suppresion d'une activité
             */
            vm.deleteActivite = function (id) {

                var confirm = $mdDialog.confirm()
                    .title("Suppression d'une activité de représentation d'intêret")
                    .textContent("Etes-vous sûr(e) de vouloir supprimer cette activité de représentation d'intérêts pour l'exercice du " + vm.rapport.exercice.periode + " ?")
                    .ariaLabel("Confirmation de la suppression")
                    .ok("Supprimer")
                    .cancel("Annuler");

                $mdDialog.show(confirm).then(function () {
                    blockUI.start();
                    EspaceService.deleteActivite(id).then(function () {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Activité de représentation d'intêret supprimée")
                                .textContent("L'activité de représentation d'intêret a bien été supprimée.")
                                .ariaLabel("Données supprimées")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        );
                    }).finally(function () {
                        initData();
                        blockUI.stop();
                    });
                });

            };

            /**
             * Publication des informations générales de l'organisation
             * @param ev event html du clic
             * @param corporate l'objet organisation avec modifications à publier
             */
            vm.submitCorporateData = function (ev, valid, card, data) {
                if (data.jourFin && data.moisFin) {
                    data.finExerciceFiscal = (data.jourFin < 10 ? "0" + data.jourFin : data.jourFin) + "-" + (data.moisFin < 10 ? "0" + data.moisFin : data.moisFin);
                }

                if (valid) {
                    // Service modification
                    blockUI.start();
                    EspaceService.setOrganisation(card, data).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Informations mises à jour")
                                .textContent("Vos modifications ont été enregistrées avec succès.")
                                .ariaLabel("Informations mises à jour")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        ).then(function (res) {
                            if ($stateParams.saveSearch !== null) {
                                vm.saveSearch = $stateParams.saveSearch;
                                $state.go('espaces', {
                                    returnState: 'espaces',
                                    saveSearch: vm.saveSearch
                                });
                            } else {
                                $state.go("espaces");
                            }
                        });
                    }, function (err) {
                        // annulation des modifications si erreur
                        $state.reload();
                    }).finally(function () {
                        blockUI.stop();
                    });
                }
            };

            /**
             * Reset les champs pour la modification des infos d'un espace collaboratif
             */
            vm.resetCardOrga = function () {
                vm.cardOrga.active = false;
                vm.espace.data.nomUsage = vm.espace.nomUsage;
                vm.espace.data.nomUsageHatvp = vm.espace.nomUsageHatvp;
                vm.espace.data.sigleHatvp = vm.espace.sigleHatvp;
                vm.espace.data.ancienNomHatvp = vm.espace.ancienNomHatvp;
            };

            /**
             * Historique des dénominations et noms d'usage
             * @param {*} ev 
             * @param {*} espace 
             */
            vm.getIdentityPublicationHistorique = function (ev, espace) {
               $mdDialog.show({
                    controller: function () {
                        this.parent = $scope;
                    },
                    controllerAs: 'ctrl',
                    scope: $scope,
                    preserveScope: true,
                    templateUrl: EnvConf.views_dir + HTML.ESPACE.ESPACE_HISTORIQUE_IDENTITE,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });

            };

            vm.cancel = function () {
                $mdDialog.cancel();
            };

            function toDate(dateStr) {
                var parts = dateStr.split("-");
                return new Date(parts[2], parts[1] - 1, parts[0]);
            };

            //fonction de mises à jour des dates de l'exercice comptable
            vm.updateDates = function (ev, exercice) {
                $mdDialog.show({
                    controller: 'DateExerciceCtrl',
                    templateUrl: EnvConf.views_dir + HTML.ESPACE.RAPPORT_MODIF_DATES,
                    locals: {
                        exercice: exercice
                    },
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            };

            vm.espaceDetails = function (event, espaceContent) {

                //cas des affiliés ayant un espace, on redirige vers leur espace
                if(espaceContent.espaceOrganisationIdDeOrdganisation > 0){
                    vm.espace = espaceContent.espaceCollaboratif;
                    vm.espace.admins = espaceContent.espaceCollaboratif.contacts.map(function (i) {
                        return {
                            id: i.id,
                            identite: i.identite,
                            actif:i.actif,
                            version: i.version
                        };
                    });
                    vm.espace.contacts = espaceContent.espaceCollaboratif.contacts.map(function (c) {
                        return c.identite;
                    }).join(", ");
                    vm.espace.dateCreation =  espaceContent.espaceCollaboratif.creationDate;
                    $state.go('espaceDetails', {
                        espace: vm.espace,
                        menuBarPreviousTitle: 'Liste des espaces collaboratifs',
                        returnState: 'espaces',
                        cardOrga: {active: false},
                        saveSearch: vm.saveSearch,
                        espaceId: espaceContent.id
                    });
                }
            };

            vm.editDetailPeriodeActiviteDialog = function (ev, client) {
                $mdDialog.show({
                    controller: 'ClientCtrl',
                    templateUrl: EnvConf.views_dir + HTML.ESPACE.CLIENT_DETAIL,
                    locals: {
                        periodeActiviteClientList: client.periodeActiviteClientList,
                    },
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            };          

            vm.toggle = function (item, list) {
              var idx = list.indexOf(item);
              if (idx > -1) {
                list.splice(idx, 1);
              }
              else {
                list.push(item);
              }
            };
      
            vm.exists = function (item, list) {
              return list.indexOf(item) > -1;
            };

            /**
             * suppresion d'une ou plusieur activités
             */
             vm.deleteActiviteBatch = function () {
                 $stateParams.exercice =  vm.rapport.exercice;
                var activiteIdAmodifierList = vm.filterIdActionTrueInList(vm.selected);
                var confirm = $mdDialog.confirm()
                    .title("Suppression d'une ou plusieurs activités de représentation d'intêret")
                    .textContent("Etes-vous sûr(e) de vouloir supprimer ces activités de représentation d'intérêts pour l'exercice du " + vm.rapport.exercice.periode + " ?")
                    .ariaLabel("Confirmation de la suppression")
                    .ok("Supprimer")
                    .cancel("Annuler");

                $mdDialog.show(confirm).then(function () {
                    blockUI.start();
                    EspaceService.deleteActivite(activiteIdAmodifierList).then(function () {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Activités de représentation d'intêret supprimées")
                                .textContent("La ou les activités de représentation d'intêret ont bien été supprimées.")
                                .ariaLabel("Données supprimées")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        );
                    }).finally(function () {
                        initData();
                        vm.selected = [];
                        blockUI.stop();
                    });
                });

            };

            /**
             * Filtre les id de list pour retenir ceux dont le champ action est true.
             * @param list la liste à filtrer.
             * @return la liste filtrée.
             */
             vm.filterIdActionTrueInList = function (list) {
                var newList = [];
                for (var i = 0; i < list.length; i++) {
                    if (list[i].action === true) {
                        newList.push(list[i].id);
                    }
                }
                return newList;
            };
            /**
             * Filtre les éléments de list pour retenir ceux dont le champ action est true.
             * @param list la liste à filtrer.
             * @return la liste filtrée.
             */
            vm.filterActionTrueInList = function (list) {
                var newList = [];
                for (var i = 0; i < list.length; i++) {
                    if (list[i].action === true) {
                        newList.push(list[i]);
                    }
                }
                return newList;
            };

            /**
             * republication d'une ou plusieurs activités
             */
             vm.publishBatch = function () {
                $stateParams.exercice =  vm.rapport.exercice;
                if(!vm.checkDroitRepublication(vm.selected)){
                    $mdDialog.show($mdDialog.alert()
                            .title("Publications activées")
                            .textContent("Impossible republier le lot sélectionné car au moins une des activitités n'est pas au statut Mise à jour non publiée ou dépubliée et ne peut être republiée")
                            .ariaLabel("Publications activées")
                            .ok("OK")
                            .clickOutsideToClose(true));

                }else{
                    var activiteIdAmodifierList = vm.filterIdActionTrueInList(vm.selected);

                    var title = "Republication globale d'une fiche",
                    message = "Vous êtes sur le point de republier une ou plusieurs fiches<b>" +
                        "</b>.<br> Confirmez-vous cette action ?";

                    var confirm = $mdDialog.confirm()
                        .title(title)
                        .htmlContent(message)
                        .ariaLabel(title)
                        .ok('Confirmer')
                        .cancel('Annuler');

                    $mdDialog.show(confirm).then(function () {
                        blockUI.start();
                        EspaceService.republierListeFiche(activiteIdAmodifierList).then(function (res) {
                            $mdDialog.show($mdDialog.alert()
                                .title("Publications activées")
                                .textContent("Les seront republiées dans les 60 minutes")
                                .ariaLabel("Publications activées")
                                .ok("OK")
                                .clickOutsideToClose(true));
                        }).finally(function () {
                            initData();
                            vm.selected = [];
                            blockUI.stop();
                        });
                    }); 
                }                          

            };
            /**
             * Vérification que tous les activités sélectionnées ont un statut "non publiblé"
             * @param {*} list 
             * @returns 
             */
            vm.checkDroitRepublication = function (list) {                
                for (var i = 0; i < list.length; i++) {
                    if (list[i].statut === 'Non publiée' || list[i].statut === 'Publiée' || list[i].statut === 'Mise à jour publiée' || list[i].statut === 'Republiée') {
                        return false;
                    }                    
                }
                return true;                
            };
            /**
             * Vérification que tous les activités sélectionnées ont un statut "publiblé"
             * @param {*} list 
             * @returns 
             */
            vm.checkDroitDepublication = function (list) {                
                for (var i = 0; i < list.length; i++) {
                    if (list[i].statut === 'Non publiée' || list[i].statut === 'Dépubliée' || list[i].statut === 'Mise à jour non publiée') {
                        return false;
                    }                    
                }
                return true;                
            };           

            /**
             * Dépublication d'une ou plusieurs activités
             * @param {*} listActivite 
             */
            vm.unpublishBatch = function () {
                $stateParams.exercice =  vm.rapport.exercice;
                if(!vm.checkDroitDepublication(vm.selected)){
                    $mdDialog.show($mdDialog.alert()
                            .title("Dépublications activées")
                            .textContent("Impossible dépublier le lot sélectionné car au moins une des activitités n'est pas au statut publiée, Mise à jour publiée ou Republiée et ne peut être Dépublié")
                            .ariaLabel("Dépublications activées")
                            .ok("OK")
                            .clickOutsideToClose(true));

                }else{
                    var activiteIdAmodifierList = vm.filterIdActionTrueInList(vm.selected);

                    var title = "Dé-publication Fiche d'activités",
                    message = "Vous êtes sur le point de dé-publier une ou plusieurs fiches<b>" +
                        "</b>.<br> Confirmez-vous cette action ?";

                    var confirm = $mdDialog.confirm()
                        .title(title)
                        .htmlContent(message)
                        .ariaLabel(title)
                        .ok('Confirmer')
                        .cancel('Annuler');

                    $mdDialog.show(confirm).then(function () {
                        blockUI.start();
                        EspaceService.depublierListFiche(activiteIdAmodifierList).then(function (res) {
                            $mdDialog.show($mdDialog.alert()
                                .title("fiche dépubliée")
                                .textContent("Les fiches ont étés dépubliées")
                                .ariaLabel("fiche dépubliée")
                                .ok("OK")
                                .clickOutsideToClose(true));
                        }).finally(function () {
                            initData();
                            vm.selected = [];
                            blockUI.stop();
                        });
                    }); 
                }
            };

            vm.getHistoriqueExercice = function (ev, exercice) {
                $mdDialog.show({
                    controller: 'ExerciceCtrl',
                    templateUrl: EnvConf.views_dir + HTML.ESPACE.RAPPORT_HISTORIQUE,
                    locals: {
                        exercice: exercice
                    },
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });                
            };

            /***********************************************************
             * Initialisation du controller
             ***********************************************************/
            if (angular.isDefined(vm.espace)) {
                initData();
            }

        }]).controller('ExerciceCtrl', ['$stateParams', 'blockUI', '$q', '$scope', '$rootScope', '$state', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'API', 'HTML', 'EspaceDetailsScope', 'EspaceService','exercice',
        function ($stateParams, blockUI, $q, $scope, $rootScope, $state, $log, $mdDialog, $mdToast, EnvConf, API, HTML,   EspaceDetailsScope, EspaceService, exercice) {

    
            var vm = $scope;
    
            vm.images_dir = $rootScope.images_dir;
            vm.espace = $state.params.espace;
            vm.exercice = exercice;
    
           
    
            vm.cancel = function () {
                $mdDialog.cancel();
            };
    
            
    
        }]).controller('DateExerciceCtrl', ['$scope', '$state', '$rootScope', 'blockUI', '$q', '$mdDialog', 'EspaceService','SurveillanceService', 'exercice',
    function ($scope, $state, $rootScope, blockUI, $q, $mdDialog, EspaceService,SurveillanceService, exercice) {

        var vm = $scope;


        vm.espace = $state.params.espace;
        vm.exercice = exercice;

        vm.nombreJoursDebut = 31;
        vm.nombreJoursFin = 31;
        vm.joursList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
        vm.moisList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        vm.moisListLabel = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
        vm.anneeList = getYearsArray(new Date("Jan 1 2000"), new Date("Jan 1 2100"));

        vm.jourDebutExercice = vm.exercice.dateDebut.split('-')[0] < 10 ? vm.exercice.dateDebut.split('-')[0].substring(1) : vm.exercice.dateDebut.split('-')[0];
        vm.moisDebutExercice = vm.exercice.dateDebut.split('-')[1] < 10 ? vm.exercice.dateDebut.split('-')[1].substring(1) : vm.exercice.dateDebut.split('-')[1];
        vm.anneeDebutExercice = vm.exercice.dateDebut.split('-')[2];

        vm.jourFinExercice = vm.exercice.dateFin.split('-')[0] < 10 ? vm.exercice.dateFin.split('-')[0].substring(1) : vm.exercice.dateFin.split('-')[0];
        vm.moisFinExercice = vm.exercice.dateFin.split('-')[1] < 10 ? vm.exercice.dateFin.split('-')[1].substring(1) : vm.exercice.dateFin.split('-')[1];
        vm.anneeFinExercice = vm.exercice.dateFin.split('-')[2];

        function getYearsArray(startDate, endDate) {
            var startYear = startDate.getYear() + 1900,
                endYear = endDate.getYear() + 1900,
                yearsArray = [];

            for (var year = startYear; year <= endYear; year++) {
                yearsArray.push(year);
            }
            return yearsArray;
        }

        function getNbJoursDuMois(mois) {
            var moisChoisis = mois || 0;
            return 31 - ((moisChoisis == 2) ? (3) : ((moisChoisis - 1) % 7 % 2));
        };

        vm.majNbJours = function (date) {
            if (date === 'debut') {
                if (vm.moisDebutExercice !== undefined) {
                    vm.nombreJoursDebut = getNbJoursDuMois(vm.moisDebutExercice);
                    if (vm.nombreJoursDebut < vm.jourDebutExercice) {
                        vm.jourDebutExercice = vm.nombreJoursDebut;
                    }
                    return vm.moisDebutExercice;
                } else {
                    return 0;
                }
            }
            else {
                if (vm.moisFinExercice !== undefined) {
                    this.nombreJoursFin = getNbJoursDuMois(vm.moisFinExercice);
                    if (vm.nombreJoursFin < vm.jourFinExercice) {
                        vm.jourFinExercice = vm.nombreJoursFin;
                    }
                    return vm.moisDebutExercice;
                } else {
                    return 0;
                }
            }
        };

        vm.cancel = function () {
            $mdDialog.cancel();
        };

        /**
         * Validation de la date de cloture
         * @param ev event html du clic
         * @param corporate l'objet organisation avec modifications à publier
         */
        vm.submitDatesExercice = function () {
            vm.exercice.dateDebut = ((vm.jourDebutExercice < 10) ? "0" + vm.jourDebutExercice : vm.jourDebutExercice) + "-" + ((vm.moisDebutExercice < 10) ? "0" + vm.moisDebutExercice : vm.moisDebutExercice) + "-" + vm.anneeDebutExercice;
            vm.exercice.dateFin = ((vm.jourFinExercice < 10) ? "0" + vm.jourFinExercice : vm.jourFinExercice) + "-" + ((vm.moisFinExercice < 10) ? "0" + vm.moisFinExercice : vm.moisFinExercice) + "-" + vm.anneeFinExercice;
            // Service modification
            blockUI.start();
            delete vm.exercice.activiteSimpleDto;
            EspaceService.updateDatesExercice(vm.exercice, vm.espace.id).then(function (res) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .title("Informations enregistrées")
                        .textContent("Vos modifications ont été enregistrées avec succès.")
                        .ariaLabel("Informations mises à jour")
                        .ok('OK')
                        .clickOutsideToClose(true)
                ).then(function (res) {
                    $state.reload();
                });
            }, function (err) {
                // annulation des modifications si erreur
                $state.reload();
            }).finally(function () {
                blockUI.stop();
            });
        };

    }])
    .controller('EspaceDetailsMenuBarCtrl', ['$stateParams', 'blockUI', '$q', '$scope', '$state', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'API', 'HTML', 'TABLE_OPTIONS', 'USER_ROLES', 'MANDAT_GESTION_TYPE', 'EspaceDetailsScope', 'EspaceService','SurveillanceService', 'UsersService', 'GroupService', 'DesinscriptionService',
        function ($stateParams, blockUI, $q, $scope, $state, $log, $mdDialog, $mdToast, EnvConf, API, HTML, TABLE_OPTIONS, USER_ROLES, MANDAT_GESTION_TYPE, EspaceDetailsScope, EspaceService,SurveillanceService, UsersService, GroupService, DesinscriptionService) {

            var vm = $scope;
            vm.file = [];
            vm.espaceOrganisation = EspaceDetailsScope.get('EspaceDetailsCtrl').espace;
            vm.hasPubli = 0;
            vm.desinscription;
            vm.maxFileSizeInMB = vm.getMaxUploadSizeInMB();
            vm.demande_complement_desinscription_msg = "";
            vm.returnState = EspaceDetailsScope.get('EspaceDetailsCtrl').returnState;
            vm.menuBarPreviousTitle = EspaceDetailsScope.get('EspaceDetailsCtrl').menuBarPreviousTitle;

            if ($stateParams.saveSearch !== null) {
                vm.saveSearch = $stateParams.saveSearch;
                $state.get('espaceDetails').data.returnState = 'espaces({saveSearch: ' + JSON.stringify(vm.saveSearch) + '})';
                vm.returnState = 'espaces({saveSearch: ' + JSON.stringify(vm.saveSearch) + '})';
            } else {
                $state.get('espaceDetails').data.returnState = 'espaces';
                vm.returnState = $state.get('espaceDetails').data.returnState;
            }

            vm.desinscrire = function (ev){
                EspaceService.hasPublished(vm.espaceOrganisation.id).then(function (res) {
                    vm.hasPubli = res.data;
                });
                DesinscriptionService.getDesinscription(vm.espaceOrganisation.id).then(function (res) {
                    vm.desinscription = res.data;
                }).then(function (res) {
                    $mdDialog.show({
                        controller: function () {
                            this.parent = $scope;
                        },
                        controllerAs: 'ctrl',
                        scope: $scope,
                        preserveScope: true,
                        templateUrl: EnvConf.views_dir + "/espace/tabs/desinscription_form.html",
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        fullscreen: true
                    });
                });
            };
            vm.saveDesinscription = function (){
                var formData = new FormData();
                angular.forEach(vm.file, function (obj) {
                    formData.append("files", obj.lfFile);
                });
                if(!vm.desinscription.cessationDate.includes('-')){
                    var day = vm.desinscription.cessationDate.slice(0, 2);
                    var month = vm.desinscription.cessationDate.slice(2, 4);
                    var year = vm.desinscription.cessationDate.slice(4,8);
                    vm.desinscription.cessationDate = day + "-" + month + "-" + year;
                }
                formData.append("desinscription", JSON.stringify(vm.desinscription));
                formData.append("espaceOrganisationId", vm.espaceOrganisation.id);
                DesinscriptionService.saveDesinscription(formData).then(function (res) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Désinscription prise en compte")
                            .textContent("La désinscription de l'espace "+vm.espaceOrganisation.denomination+" à bien été prise en compte.")
                            .ariaLabel("Désinscription prise en compte")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    );
                }).finally(function () {
                    $state.go('espaces');
                });
            };

            vm.rejectDemande = function(){
                DesinscriptionService.rejectDemande(vm.desinscription.id).then(function (res){
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Désinscription refusée")
                            .textContent("La désinscription de l'espace "+vm.espaceOrganisation.denomination+" à bien été refusée.")
                            .ariaLabel("Désinscription refusée")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    );
                }).finally(function () {
                    $state.go('espaces');
                })
            };

            vm.askforcomplement = function(msg, valid){
                if (valid) {// si formulaire valide
                    blockUI.start();

                    // on met à jour la demande désincription
                    var formData = new FormData();
                    angular.forEach(vm.file, function (obj) {
                        formData.append("files", obj.lfFile);
                    });
                    formData.append("desinscription", JSON.stringify(vm.desinscription));
                    formData.append("espaceOrganisationId", vm.espaceOrganisation.id);
                    DesinscriptionService.saveDesinscription(formData).then(function (res) {
                        DesinscriptionService.askforcomplement(msg, vm.desinscription.id,vm.currentUser().id).then(function (res) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Complément désinscription")
                                    .textContent("Un complément d'information pour la désinscription de l'espace " + vm.espaceOrganisation.denomination + " a été demandé.")
                                    .ariaLabel("Complément désinscription")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            ).then(function () {
                                $state.go('espaces');
                            });
                        }).finally(function () {
                            blockUI.stop();
                        });

                    });
                }
            };

            vm.askforcomplementDialog = function(){
                $mdDialog.show({
                    controller: function () {
                        this.parent = $scope;
                    },
                    controllerAs: 'ctrl',
                    scope: $scope,
                    preserveScope: true,
                    templateUrl: EnvConf.views_dir + "/espace/tabs/demande_complement_desinscription.html",
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            };

            vm.switchEspaceState = function (event) {
                var espaceId = vm.espaceOrganisation.id;
                blockUI.start();
                $q.all([
                    EspaceService.switchDeclarantAccountStatus(espaceId).then(function (res) {
                        var confirmMessage = generateAlertDialog(event, vm.espaceOrganisation.actif ? "Inactivation de l'espace organisation" : "Activation de l'espace organisation", "Le statut d’activation de l'espace [" + EspaceDetailsScope.get('EspaceDetailsCtrl').espace.denomination + "]  a été modifié", "Changement de statut de l'espace");
                        $mdDialog.show(confirmMessage).then(function () {
                            vm.actif = res.data.espaceOrganisation.espaceActif;
                            EspaceDetailsScope.get('EspaceDetailsCtrl').espace.actif = res.data.espaceOrganisation.espaceActif;
                        });
                    })
                ]).then(function () {
                }).finally(function () {
                    $log.debug(JSON.stringify(EspaceDetailsScope.get('EspaceDetailsCtrl').declarants));
                    blockUI.stop();
                });
            };

            /** Generate alert modal. */
            var generateAlertDialog = function (event, title, message, label) {
                return $mdDialog.alert()
                    .title(title)
                    .textContent(message)
                    .ariaLabel(label)
                    .targetEvent(event)
                    .ok("OK")
                    .clickOutsideToClose(true);
            };

            vm.surveillance;
            vm.putEspaceUnderWatch = function (ev) {

                SurveillanceService.getSurveillanceByOrganisationId(vm.espaceOrganisation.organisationId).then(function (res) {
                    vm.surveillance = res.data;
                })

                UsersService.getAllBackOfficeUsers().then(function (res) {
                    vm.users = res.data;
                })
                GroupService.getAllGroupAndUsers().then(function(res){
                    vm.groups = res.data;
                })

                EspaceService.getStatutEnum().then(function (res) {
                    vm.statuts = res.data.surveillanceOrganisationEnumList;
                })

                $mdDialog.show({
                    controller: function () {
                        this.parent = $scope;
                    },
                    controllerAs: 'ctrl',
                    scope: $scope,
                    preserveScope: true,
                    templateUrl: EnvConf.views_dir + "/espace/surveillance.html",
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            };

            vm.validate = function () {
                blockUI.start();
                if (vm.surveillance.id !=null){
                    SurveillanceService.updateSurveillance(vm.surveillance).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Mise sous surveillance modifiée")
                                .textContent("Mise sous surveillance modifiée")
                                .ariaLabel("Mise sous surveillance modifiée")
                                .ok('OK')
                                .clickOutsideToClose(true));
                    }).finally(function () {
                        blockUI.stop();
                    });
                }
                else {
                    SurveillanceService.saveSurveillance(vm.surveillance).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Mise sous surveillance effectuée")
                                .textContent("Mise sous surveillance effectuée")
                                .ariaLabel("Mise sous surveillance effectuée")
                                .ok('OK')
                                .clickOutsideToClose(true));
                    }).finally(function () {
                        blockUI.stop();
                    });
                }

            };

            vm.closeModal = function (ev) {
                $mdDialog.hide();
            };

        }])
    .factory('EspaceDetailsScope', ['$rootScope', function ($rootScope) {
        var mem = {};

        return {
            store: function (key, value) {
                $rootScope.$emit('scope.stored', key);
                mem[key] = value;
            },
            get: function (key) {
                return mem[key];
            }
        }
    }]);