/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.comment')

/**
 * Controlleur de la page contenat la liste des déclarants.
 */
    .controller('CommentCtrl', ['$location', '$anchorScroll', '$scope', 'blockUI', '$q', '$log', 'EnvConf', 'HTML', 'CommentService', '$mdDialog',
        function ($location, $anchorScroll, $scope, blockUI, $q, $log, EnvConf, HTML, CommentService, $mdDialog) {
            var vm = this;

            $scope.comment = {};
            $scope.comments = [];
            vm.templateUrl = EnvConf.views_dir + HTML.COMMENT;

            $scope.update = {};
            $scope.old = {};
            $scope.locked = false;

/////////////////////////////////////////////////////////////////////////////////////
//
// Récupération des commentaires.
//
/////////////////////////////////////////////////////////////////////////////////////
            /**
             * Initialiser la liste des commentaires des déclarants.
             */
            function initDeclarantCommentData(id) {
                var data = CommentService.getAllCommentByDeclarantId(id).then(function (res) {
                    return $scope.comments = res.data;
                });
                getAllComments(data);
            }

            /**
             * Initialiser la liste des commentaires des déclarants.
             */
            function initEspaceCommentData(id) {
                var data = CommentService.getAllCommentByEspaceId(id).then(function (res) {
                    return $scope.comments = res.data;
                });
                getAllComments(data);
            }

            /**
             * Initialiser la liste des commentaires des demandes de changement de mandants
             */
            function initDemandeChangementMandantCommentData(id) {
                var data = CommentService.getAllCommentByDemandeChangementMandantId(id).then(function (res) {
                    return $scope.comments = res.data;
                });
                getAllComments(data);
            }

            /**
             * Initialiser la liste des commentaires des déclarants.
             */
            function initValidationCommentData(id) {
                var data = CommentService.getAllCommentByValidationId(id).then(function (res) {
                    return $scope.comments = res.data;
                });
                getAllComments(data);
            }

            /**
             * Initialiser la liste des commentaires des demandes.
             */
            function initDemandeCommentData(id) {
                var data = CommentService.getAllCommentByDemandeId(id).then(function (res) {
                    return $scope.comments = res.data;
                });
                getAllComments(data);
            }

            /**
             * Méthode commune de récupération de commentaires.
             */
            function getAllComments(data) {
                blockUI.start();
                $q.all([
                    $scope.comments = data
                ]).then(function () {
                }).finally(function () {
                    if($scope.$parent.espace){
                        $scope.locked = $scope.$parent.espace.locked;
                    }else if ($scope.$parent.validation_vo){
                        $scope.locked = $scope.$parent.validation_vo.locked;
                    }
                    
                    if ($scope.comments.length > 0) {
                        scrollDown('anchor_bottom' + $scope.comments[$scope.comments.length - 1].id);
                    }
                    blockUI.stop();
                });
            }

/////////////////////////////////////////////////////////////////////////////////////
//
// Ajout de commentaire.
//
/////////////////////////////////////////////////////////////////////////////////////

            /**
             * Méthode commune d'ajout de commentaire.
             */
            $scope.addComment = function ($event) {
                if ($scope.comment.message.length === 0) return false;

                $scope.comment.contextId = vm.current;
                blockUI.start();
                var functionToExecute;

                switch (vm.context) {
                    case 1: //déclarant
                        functionToExecute = CommentService.addDeclarantComment($scope.comment);
                        break;
                    case 2://espace organisation
                        functionToExecute = CommentService.addEspaceComment($scope.comment);
                        break;
                    case 3://validation
                        functionToExecute = CommentService.addValidationComment($scope.comment);
                        break;
                    case 4://demande
                        functionToExecute = CommentService.addDemandeComment($scope.comment);
                        break;
                    case 5://demande de changement de mandant
                        functionToExecute = CommentService.addDemandeChangementMandantComment($scope.comment);
                        break;
                    default:
                        return false;
                }

                $q.all([
                    functionToExecute
                ]).then(function () {
                }).finally(function () {
                    //refresh comment list
                    refreshContent(vm.context);
                    $scope.comment = {};
                    blockUI.stop();
                });
            };

/////////////////////////////////////////////////////////////////////////////////////
//
// modifocation de commentaire.
//
/////////////////////////////////////////////////////////////////////////////////////

            /**
             * Méthode commune de modification de commentaire.
             */
            $scope.modifyComment = function (evt, comment) {

                $scope.update = comment;
                $scope.old = comment.message;

                $mdDialog.show({
                    targetEvent: evt,
                    controller: function () {
                        this.parent = $scope;
                    },
                    controllerAs: 'ctrl',
                    scope: $scope,
                    preserveScope: true,
                    templateUrl: EnvConf.views_dir + HTML.COMMENT_UPDATE,
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    escapeToClose: true
                });

            };

            /**
             * Méthode commune de modification de commentaire.
             */
            $scope.updateComment = function (evt, comment) {

                blockUI.start();
                var functionToExecute;

                switch (vm.context) {
                    case 1: //déclarant
                        functionToExecute = CommentService.updateDeclarantComment(comment);
                        break;
                    case 2://espace organisation
                        functionToExecute = CommentService.updateEspaceComment(comment);
                        break;
                    case 3://validation
                        functionToExecute = CommentService.updateValidationComment(comment);
                        break;
                    case 4://demande
                        functionToExecute = CommentService.updateDemandeComment(comment);
                        break;
                    case 5://demande de changement de mandant
                        functionToExecute = CommentService.updateDemandeChangementMandantComment(comment);
                        break;
                    default:
                        return false;
                }

                $q.all([
                    functionToExecute
                ]).then(function () {
                }).finally(function () {
                    //refresh comment list
                    refreshContent(vm.context);
                    blockUI.stop();
                    $mdDialog.hide();
                });
            };

/////////////////////////////////////////////////////////////////////////////////////
//
// Suppression de commentaire.
//
/////////////////////////////////////////////////////////////////////////////////////

            /**
             * Méthode commune de suppression de commentaire.
             */
            $scope.deleteComment = function (commentId) {

                var donePublier = $mdDialog.confirm().title("Attention").textContent("Supprimer ce commentaire ?")
                    .ariaLabel("Attention").ok('Confirmer').cancel('Annuler');
                $mdDialog.show(donePublier).then(function () {
                    blockUI.start();
                    var functionToExecute;

                    switch (vm.context) {
                        case 1: //déclarant
                            functionToExecute = CommentService.deleteDeclarantComment(commentId);
                            break;
                        case 2://espace organisation
                            functionToExecute = CommentService.deleteEspaceComment(commentId);
                            break;
                        case 3://validation
                            functionToExecute = CommentService.deleteValidationComment(commentId);
                            break;
                        case 4://demande
                            functionToExecute = CommentService.deleteDemandeComment(commentId);
                            break;
                        case 5://demande de changement de mandant
                            functionToExecute = CommentService.deleteDemandeChangementMandantComment(commentId);
                            break;
                        default:
                            return false;
                    }

                    $q.all([
                        functionToExecute
                    ]).then(function () {
                    }).finally(function () {
                        //refresh comment list
                        refreshContent(vm.context);
                        blockUI.stop();
                    });
                });
            };

/////////////////////////////////////////////////////////////////////////////////////
//
// Initialisation des services de récupération, suivant le context du component.
//
/////////////////////////////////////////////////////////////////////////////////////
            /**
             * Appel de la méthode init data.
             */
            function refreshContent(context) {
                switch (context) {
                    case 1: //déclarant
                        initDeclarantCommentData(vm.current);
                        break;
                    case 2://espace organisation
                        initEspaceCommentData(vm.current);
                        break;
                    case 3://validation
                        initValidationCommentData(vm.current);
                        break;
                    case 4://demande
                        initDemandeCommentData(vm.current);
                        break;
                    case 5://demande de changement de mandat
                        initDemandeChangementMandantCommentData(vm.current);
                    default:
                        return false;
                }
            };

            //Scroll down
            function scrollDown(id) {
                $location.hash(id);
                $anchorScroll();
            };

            $scope.closeModal = function (evt) {
                $scope.update.message = $scope.old;
                $mdDialog.hide();
            };

            if (angular.isDefined(vm.context) && angular.isDefined(vm.current)) {
                refreshContent(vm.context);
            }

        }]);