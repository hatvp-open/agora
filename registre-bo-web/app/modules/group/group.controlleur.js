/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.group')
    .controller('GroupCtrl', ['blockUI', '$q', '$rootScope', '$scope', '$state', '$stateParams', 'TABLE_OPTIONS','GroupService', '$mdDialog', 'EnvConf','HTML',
        function (blockUI, $q, $rootScope, $scope, $state, $stateParams, TABLE_OPTIONS, GroupService, $mdDialog, EnvConf, HTML) {
            var vm = $scope;

            vm.groupTableOptions = angular.copy(TABLE_OPTIONS);
            vm.groupTableOptions.limit = 10;

            vm.group = [];
            vm.search = false;
            vm.searchType="";
            vm.libelleSearch="";
            vm.utilisateurBoDtoSearch="";


            vm.initTable = function () {

                vm.search = false;
                vm.searchType ="";
                vm.saveSearch = {};
                vm.libelleSearch="";
                vm.utilisateurBoDtoSearch="";

                blockUI.start();

                GroupService.getAllGroupAndUsers().then(function (res) {
                    vm.group = res.data.map(function (e) {
                        return {
                            id: e.id,
                            libelle: e.libelle,
                            utilisateurBoDto: e.utilisateurBoDto.map(function (i) {
                                return {
                                    id: i.id,
                                    nom: i.nom,
                                    prenom: i.prenom,
                                    email: e.email,
                                    compteActive: !e.compteActive,
                                    compteSupprime: e.compteSupprime
                                };
                            })
                        }
                    });
                    vm.group.totalNumber = res.data.resultTotalNumber;
                }).finally(function () {
                    blockUI.stop();
                })
            };

            vm.searchTable = function (type, page, occurrence) {
                vm.searchType = type;

                switch (type) {
                    case 'libelle':
                        if (vm.libelleSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = GroupService.getGroupsAndusersBySearchPaginated(type, vm.libelleSearch, page, occurrence);
                        break;

                    case 'utilisateurBoDto':
                        if (vm.utilisateurBoDtoSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = GroupService.getGroupsAndusersBySearchPaginated(type, vm.utilisateurBoDtoSearch, page, occurrence);
                        break;
                }
                vm.promise.then(function (res) {
                    vm.group = res.data.dtos.map(function (e) {
                        return {
                            id: e.id,
                            libelle: e.libelle,
                            utilisateurBoDto: e.utilisateurBoDto.map(function (i) {
                                return {
                                    id: i.id,
                                    nom: i.nom,
                                    prenom: i.prenom,
                                    email: e.email,
                                    compteActive: !e.compteActive,
                                    compteSupprime: e.compteSupprime
                                };
                            })
                        };
                    });
                    vm.group.totalNumber = res.data.resultTotalNumber;

                    vm.saveSearch = {
                        search: vm.search,
                        searchType: vm.searchType,
                        libelleSearch : vm.libelleSearch,
                        utilisateurBoDtoSearch : vm.utilisateurBoDtoSearch,
                        limit: vm.groupTableOptions.limit,
                        page: vm.groupTableOptions.page

                    };
                });
            };
            vm.onPaginate = function (page, occurrence) {
                vm.initTable();
            };
            vm.onPaginateSearch = function (page, limit) {
                vm.searchTable(vm.searchType, page, limit);
            };

            /**
             * Edit group modal.
             *
             * @param ev click event.
             */
            vm.editGroup = function (ev, group) {
                $mdDialog.show({
                    controller: 'GroupsEditCtrl',
                    locals: {
                        item: group,
                        edit: true,
                        saveSearch : vm.saveSearch
                    },
                    templateUrl: EnvConf.views_dir + HTML.GROUP.EDIT_GROUP,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                }).then(function () {
                    //$state.reload();
                });
            };
            /**
             * Add group modal
             * @param ev
             */
            vm.addUserGroup = function (ev) {
                $mdDialog.show({
                    controller: 'GroupsEditCtrl',
                    locals: {
                        item: {},
                        edit: false
                    },
                    templateUrl: EnvConf.views_dir + HTML.GROUP.EDIT_GROUP,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: false
                }).then(function () {
                    //$state.reload();
                });
            };

            vm.callSearch = function (type) {
                vm.searchType = type;
                vm.searchTable(type, vm.groupTableOptions.page, vm.groupTableOptions.limit);
            };
            if ($stateParams.saveSearch !== null && $stateParams.saveSearch.search) {
                vm.search = $stateParams.saveSearch.search;
                vm.searchType = $stateParams.saveSearch.searchType;
                vm.libelleSearch = $stateParams.saveSearch.libelleSearch;
                vm.utilisateurBoDtoSearch = $stateParams.saveSearch.utilisateurBoDtoSearch;
                vm.groupTableOptions.limit = $stateParams.saveSearch.limit;
                vm.groupTableOptions.page = $stateParams.saveSearch.page;

                vm.searchTable(vm.searchType, vm.groupTableOptions.page,  vm.groupTableOptions.limit);
            } else {
                if ($stateParams.saveSearch !== null && $stateParams.saveSearch.limit && $stateParams.saveSearch.page) {
                    vm.groupTableOptions.limit = $stateParams.saveSearch.limit;
                    vm.groupTableOptions.page = $stateParams.saveSearch.page;
                }
                vm.initTable();
            }
        }])

    .controller('GroupsEditCtrl', ['$log', '$scope', '$state', '$rootScope', '$mdDialog', 'edit', 'GroupService', 'UsersService', 'item',
        function ($log, $scope, $state, $rootScope, $mdDialog, edit, GroupService, UsersService, item) {
            var vm = $scope;

            vm.groupData = item;
            vm.usersList = [];
            vm.isEditMode = edit;
            vm.saveSearch = false;
            vm.images_dir = $rootScope.images_dir;

            if(!vm.groupData.utilisateurBoDto){
                vm.groupData.utilisateurBoDto = [];
            }

            UsersService.getAllBackOfficeUsers().then(function (res) {
                vm.usersList = res.data.map(function (e) {
                    return{
                        id: e.id,
                        nom: e.nom,
                        prenom: e.prenom,
                        email: e.email,
                        compteActive: !e.compteActive,
                        compteSupprime: e.compteSupprime
                    }
                });
                for(var i=0;i<=vm.groupData.utilisateurBoDto.length-1;i++){
                    for(var j=0;j<=vm.usersList.length-1;j++){
                        if(vm.usersList[j].id === vm.groupData.utilisateurBoDto[i].id){
                            vm.usersList.splice(j,1);
                        }
                    }
                }
            });

            vm.cancel = function () {
                $mdDialog.cancel();
                var current = $state.current;
                $state.transitionTo(current, {
                    saveSearch: vm.saveSearch
                }, {reload: true, inherit: true, notify: true});
            };

            var generateDialogMessage = function (event, title, message, label) {
                return $mdDialog.confirm()
                    .title(title)
                    .textContent(message)
                    .ariaLabel(label)
                    .targetEvent(event)
                    .ok("OK");
            };

            /**
             * Envoie du formulaire d'édition
             * @param ev
             */
            vm.submitEditGroup = function (ev) {
                GroupService.updateGroup(vm.groupData).then(function (res) {
                    vm.cancel();
                })
            };

            /**
             * Suppression d'un groupe
             * @param ev
             * @param groupId
             */
            vm.deleteUserGroup = function (ev, groupId) {
                var confirm = $mdDialog.confirm()
                    .title("Suppression de groupe")
                    .textContent("Voulez-vous supprimer ce groupe?")
                    .ariaLabel("Suppression de groupe")
                    .targetEvent(ev)
                    .ok('Supprimer')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function (result) {
                    GroupService.deleteGroup(groupId).then(function (res) {
                        var confirmMessage = generateDialogMessage(ev, "Suppression de groupe", "Le groupe a été supprimé avec succès", "Suppression de groupe");
                        $mdDialog.show(confirmMessage).then(function () {
                            vm.cancel();
                        });
                    })
                });

            };

            /**
             * Envoie du formulaire d'ajout
             */
            vm.submitAddGroup = function (ev) {
                GroupService.addGroup(vm.groupData).then(function (res) {
                    var confirm = generateDialogMessage(ev, "Ajout d'un nouveau groupe", "Le nouveau groupe d'utilisateur ["+res.data.libelle+"] a bien été créé", "Ajout d'un nouveau groupe");
                    $mdDialog.show(confirm).then(function (res) {
                        vm.cancel();
                    });
                })
            };

            vm.printUsers = function () {
                var utilisateurs = "";
                if(vm.groupData.utilisateurBoDto){
                    for(var i=0;i<=vm.groupData.utilisateurBoDto.length-1;i++){
                        utilisateurs+= vm.groupData.utilisateurBoDto[i].prenom+" "+vm.groupData.utilisateurBoDto[i].nom;
                        if(!(i===vm.groupData.utilisateurBoDto.length-1)) utilisateurs+=',';
                    }
                }
                return utilisateurs;
            };

            vm.isEmpty = function (list) {
                if(list.length === 0) return false; return true;
            }
        }]);