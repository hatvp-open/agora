/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.users')

    .controller('UsersCtrl', ['blockUI', '$q', '$scope', '$state', '$stateParams', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'USER_BO_ROLES', 'UsersService','AuthService',
        function (blockUI, $q, $scope, $state, $stateParams, $log, $mdDialog, $mdToast, EnvConf, HTML, TABLE_OPTIONS, USER_BO_ROLES, UsersService,AuthService) {
            var vm = $scope;

            /**
             * Gestion des utilisateurs
             */

            vm.utilisateurs = [];
            vm.search = false;
            vm.typeDeRecherche = "";
            vm.utilisateursTableOptions = angular.copy(TABLE_OPTIONS);
            vm.showLockedUsers = false;

            var initUsersTable = function (page, limit) {
                vm.search = false;
                vm.typeDeRecherche = "";
                vm.utilisateursIdentifiantKeywords = "";
                vm.utilisateursEmailKeywords = "";

                vm.utilisateursTableOptions.promise = UsersService.getPaginated(page, limit);

                vm.utilisateursTableOptions.promise.then(function (res) {
                    vm.utilisateurs = res.data.dtos.map(function (e) {
                        return {
                            id: e.id,
                            nom: e.nom,
                            prenom: e.prenom,
                            name: e.nom + " " + e.prenom,
                            email: e.email,
                            verrouille: !e.compteActive,
                            deleted: e.compteSupprime,
                            version: e.version,
                            validationEspaceCollaboratif: e.roles.includes(USER_BO_ROLES.VALIDATION_ESPACES_COLLABORATIFS),
                            gestionComptesDclarants: e.roles.includes(USER_BO_ROLES.GESTION_COMPTES_DECLARANTS),
                            gestionPublications: e.roles.includes(USER_BO_ROLES.GESTION_CONTENUS_PUBLIES),
                            gestionEspaceCollaboratifs: e.roles.includes(USER_BO_ROLES.GESTION_ESPACES_COLLABORATIFS_VALIDES),
                            gestionReferentielOrganisations: e.roles.includes(USER_BO_ROLES.GESTION_REFERENTIEL_ORGANISATIONS),
                            modificationRaisonsSociales: e.roles.includes(USER_BO_ROLES.MODIFICATION_DES_RAISONS_SOCIALES),
                            traitementMandatDeGestion: e.roles.includes(USER_BO_ROLES.GESTION_MANDATS),
                            modificationEmails: e.roles.includes(USER_BO_ROLES.MODIFICATION_EMAIL_DECLARANT),
                            restrictionAcces: e.roles.includes(USER_BO_ROLES.RESTRICTION_ACCES),
                            manager: e.roles.includes(USER_BO_ROLES.MANAGER),
                            administrateur: e.roles.includes(USER_BO_ROLES.ADMINISTRATEUR),
                            dataDownloader: e.roles.includes(USER_BO_ROLES.DATA_DOWNLOADER),
                            etatActivites: e.roles.includes(USER_BO_ROLES.ETAT_ACTIVITES)
                        };
                    });
                    vm.utilisateurs.totalNumber = res.data.resultTotalNumber;
                });
                    blockUI.start();
                    $q.all([
                        vm.utilisateursTableOptions.promise
                    ]).finally(function () {
                        blockUI.stop();
                    });

            };

            /** Generation tableau de recherche **/
            var searchTable = function (type, page, limit) {
                vm.typeDeRecherche = type;

                switch(type) {
                    case 'identifiant':
                        if (vm.utilisateursIdentifiantKeywords === "") {
                            return;
                        }
                        vm.search = true;
                        vm.utilisateursTableOptions.promise = UsersService.getPaginatedParRecherche(type, vm.utilisateursIdentifiantKeywords, page, limit);
                        break;
                    case 'email':
                        if (vm.utilisateursEmailKeywords === "") {
                            return;
                        }
                        vm.search = true;
                        vm.utilisateursTableOptions.promise = UsersService.getPaginatedParRecherche(type, vm.utilisateursEmailKeywords, page, limit);
                        break;
                    default:
                        vm.utilisateursTableOptions.promise = UsersService.getPaginated(page, limit);
                }

                vm.utilisateursTableOptions.promise.then(function (res) {
                    vm.utilisateurs = res.data.dtos.map(function (e) {
                        return {
                            id: e.id,
                            nom: e.nom,
                            prenom: e.prenom,
                            name: e.nom + " " + e.prenom,
                            email: e.email,
                            verrouille: !e.compteActive,
                            deleted: e.compteSupprime,
                            version: e.version,
                            validationEspaceCollaboratif: e.roles.includes(USER_BO_ROLES.VALIDATION_ESPACES_COLLABORATIFS),
                            gestionComptesDclarants: e.roles.includes(USER_BO_ROLES.GESTION_COMPTES_DECLARANTS),
                            gestionPublications: e.roles.includes(USER_BO_ROLES.GESTION_CONTENUS_PUBLIES),
                            gestionEspaceCollaboratifs: e.roles.includes(USER_BO_ROLES.GESTION_ESPACES_COLLABORATIFS_VALIDES),
                            gestionReferentielOrganisations: e.roles.includes(USER_BO_ROLES.GESTION_REFERENTIEL_ORGANISATIONS),
                            modificationRaisonsSociales: e.roles.includes(USER_BO_ROLES.MODIFICATION_DES_RAISONS_SOCIALES),
                            traitementMandatDeGestion: e.roles.includes(USER_BO_ROLES.GESTION_MANDATS),
                            modificationEmails: e.roles.includes(USER_BO_ROLES.MODIFICATION_EMAIL_DECLARANT),
                            restrictionAcces: e.roles.includes(USER_BO_ROLES.RESTRICTION_ACCES),
                            manager: e.roles.includes(USER_BO_ROLES.MANAGER),
                            administrateur: e.roles.includes(USER_BO_ROLES.ADMINISTRATEUR),
                            dataDownloader: e.roles.includes(USER_BO_ROLES.DATA_DOWNLOADER),
                            etatActivites: e.roles.includes(USER_BO_ROLES.ETAT_ACTIVITES)
                        };
                    });
                    vm.utilisateurs.totalNumber = res.data.resultTotalNumber;

                    vm.saveSearch = {
                        search: vm.search,
                        typeDeRecherche: vm.typeDeRecherche,
                        utilisateursIdentifiantKeywords : vm.utilisateursIdentifiantKeywords,
                        utilisateursEmailKeywords : vm.utilisateursEmailKeywords
                    };

                });
                blockUI.start();
                $q.all([
                    vm.utilisateursTableOptions.promise
                ]).finally(function () {
                    blockUI.stop();
                });
            };

            /**
             * Initialisations
             */

            if($stateParams.saveSearch !== null && $stateParams.saveSearch.search) {
                vm.search = $stateParams.saveSearch.search;
                vm.typeDeRecherche = $stateParams.saveSearch.typeDeRecherche;
                vm.utilisateursIdentifiantKeywords = $stateParams.saveSearch.utilisateursIdentifiantKeywords;
                vm.utilisateursEmailKeywords = $stateParams.saveSearch.utilisateursEmailKeywords;
                searchTable(vm.typeDeRecherche, 1, vm.utilisateursTableOptions.limit);
            } else {
                initUsersTable(vm.utilisateursTableOptions.page, vm.utilisateursTableOptions.limit);
            }

            /**
             * Edit user account modal.
             *
             * @param ev click event.
             * @param index user index in the table.
             */
            vm.editAccount = function (ev, utilisateur) {
                $mdDialog.show({
                    controller: 'UsersEditCtrl',
                    locals: {
                        item: utilisateur,
                        edit: true,
                        saveSearch : vm.saveSearch
                    },
                    templateUrl: EnvConf.views_dir + HTML.USERS.EDIT_ACCOUNT,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                }).then(function () {
                    //$state.reload();
                });
            };

            /**
             * Add user account modal.
             *
             * @param ev click event.
             */
            vm.addAccount = function (ev) {
                $mdDialog.show({
                    controller: 'UsersEditCtrl',
                    locals: {
                        item: {},
                        edit: false
                    },
                    templateUrl: EnvConf.views_dir + HTML.USERS.EDIT_ACCOUNT,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: false
                }).then(function () {
                    //$state.reload();
                });
            };

            /**
             * Appel paginé du tableau
             * @param page
             * @param limit
             * @returns {*}
             */
            vm.onPaginate = function (page, limit) {
                initUsersTable(page, limit);
            };

            /**
             * Appel paginé du tableau avec recherche d'une colonne
             * @param denomination
             * @param page
             * @param limit
             * @returns {*}
             */
            vm.onPaginateSearch = function (page, limit) {
                initUsersTable(page, limit);
            };

            /**
             * Reset du tableau sans recherches
             */
            vm.reset = function() {
                initUsersTable(vm.utilisateursTableOptions.page, vm.utilisateursTableOptions.limit);
            };

            vm.callSearch = function(typeDeRecherche) {
                vm.typeDeRecherche = typeDeRecherche;
                searchTable(typeDeRecherche, 1, vm.utilisateursTableOptions.limit);
            };
        }])

    /**
     * Controlleur pour modale modification/ajout utilisateur.
     */
    .controller('UsersEditCtrl', ['$log', '$scope', '$state', '$rootScope', '$mdDialog', 'item', 'edit', 'UsersService', 'USER_BO_ROLES_TEXT', 'USER_BO_ROLES',
        function ($log, $scope, $state, $rootScope, $mdDialog, item, edit, UsersService, USER_BO_ROLES_TEXT, USER_BO_ROLES) {
            var vm = $scope;

            vm.roles = USER_BO_ROLES_TEXT;
            vm.userData = item;
            vm.isEditMode = edit;
            vm.saveSearch = false;
            vm.images_dir = $rootScope.images_dir;

            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
                var current = $state.current;
                $state.transitionTo(current, {
                    saveSearch: vm.saveSearch
                }, { reload: true, inherit: true, notify: true });
            };

            //////////////////////////////////////////////////////
            //
            // Edit account modal
            //
            //////////////////////////////////////////////////////

            var generateDialogMessage = function (event, title, message, label) {
                return $mdDialog.confirm()
                    .title(title)
                    .textContent(message)
                    .ariaLabel(label)
                    .targetEvent(event)
                    .ok("OK");
            };

            vm.generateNewPassword = function (ev, userId) {
                UsersService.generateNewPassword(userId).then(function (res) {
                    var confirm = generateDialogMessage(ev, "Modification du mot de passe", "Le mot de passe de [" + res.data.nom + " " + res.data.prenom + "] est désormais (" + res.data.password + "). Merci de lui communiquer.", "Modification de mot de passe");

                    $mdDialog.show(confirm).then(function (result) {
                        vm.cancel();
                    });
                });
            };

            vm.deleteUserAccount = function (ev, userId) {
                var confirm = $mdDialog.confirm()
                    .title("Suppression de compte")
                    .textContent("Voulez-vous supprimer ce compte?")
                    .ariaLabel("Suppression de compte")
                    .targetEvent(ev)
                    .ok('Supprimer')
                    .cancel('Annuler');
            	if (this.isEditable()){
                $mdDialog.show(confirm).then(function (result) {
                    UsersService.deleteUser(userId).then(function (res) {
                        var confirmMessage = generateDialogMessage(ev, "Suppression de compte", "Le compte de [" + vm.userData.nom + " " + vm.userData.prenom + "] a été supprimé avec succès", "Suppression de compte");
                        $mdDialog.show(confirmMessage).then(function () {
                            vm.cancel();
                        });
                    })
                });
              	}
            	else {
                    var confirm = generateDialogMessage(ev, "Action Impossible","Vous ne pouvez pas supprimer votre propre compte");

                    $mdDialog.show(confirm).then(function (result) {
                        vm.cancel();
                    });
            	}
            };

            vm.switchAccountState = function (ev, userId) {
            	if (this.isEditable()){
                    UsersService.activateDeactivateAccount(userId).then(function (res) {
                        vm.userData.verrouille = !res.data.compteActive;
                    })
            	}
            	else {
                    var confirm = generateDialogMessage(ev, "Action Impossible","Vous ne pouvez pas désactiver votre propre compte");

                    $mdDialog.show(confirm).then(function (result) {
                        vm.cancel();
                    });
            	}

            };

            vm.editUserEmail = function (email) {
                var user = {email: email};

            };
			vm.isEditable = function(){
				return $rootScope.currentUser().id != vm.userData.id;
			};
			
            vm.submitEditUser = function (ev) {
                vm.userData.roles = getRoles();
                vm.userData.compteActive = !vm.userData.verrouille;
                vm.userData.compteSupprime = vm.userData.deleted;
                UsersService.updateUSer(vm.userData).then(function (res) {
                    vm.cancel();
                })
            };

            //////////////////////////////////////////////////////
            //
            // Add account modal
            //
            //////////////////////////////////////////////////////

            /**
             * Envoi du formulaire de l'ajout d'un utilisateur.
             */
            vm.submitAddUser = function (ev) {
                vm.userData.roles = getRoles();
                UsersService.addUser(vm.userData).then(function (res) {
                    var confirm = generateDialogMessage(ev, "Ajout d'un nouveau utilisateur", "Le mot de passe de [" + res.data.nom + " " + res.data.prenom + "] est désormais (" + res.data.password + "). Merci de lui communiquer.", "Ajout d'un nouveau utilisateur");

                    $mdDialog.show(confirm).then(function (res) {
                        vm.cancel();
                    });
                })
            };

            /**
             * Préparer la nouvelle liste de role de l'utilisateur.
             */
            function getRoles() {
                var roles = [];

                if (vm.userData.validationEspaceCollaboratif) {
                    roles.push(USER_BO_ROLES.VALIDATION_ESPACES_COLLABORATIFS);
                }
                if (vm.userData.gestionComptesDclarants) {
                    roles.push(USER_BO_ROLES.GESTION_COMPTES_DECLARANTS);
                }
                if (vm.userData.gestionPublications) {
                    roles.push(USER_BO_ROLES.GESTION_CONTENUS_PUBLIES);
                }
                if (vm.userData.gestionEspaceCollaboratifs) {
                    roles.push(USER_BO_ROLES.GESTION_ESPACES_COLLABORATIFS_VALIDES);
                }
                if (vm.userData.gestionReferentielOrganisations) {
                    roles.push(USER_BO_ROLES.GESTION_REFERENTIEL_ORGANISATIONS);
                }
                if (vm.userData.modificationRaisonsSociales) {
                    roles.push(USER_BO_ROLES.MODIFICATION_DES_RAISONS_SOCIALES);
                }
                if (vm.userData.traitementMandatDeGestion) {
                    roles.push(USER_BO_ROLES.GESTION_MANDATS)
                }
                if (vm.userData.modificationEmails) {
                    roles.push(USER_BO_ROLES.MODIFICATION_EMAIL_DECLARANT);
                }
                if (vm.userData.restrictionAcces) {
                    roles.push(USER_BO_ROLES.RESTRICTION_ACCES);
                }
                if (vm.userData.manager) {
                    roles.push(USER_BO_ROLES.MANAGER);
                }
                if (vm.userData.administrateur) {
                    roles.push(USER_BO_ROLES.ADMINISTRATEUR);
                }
                if (vm.userData.dataDownloader) {
                    roles.push(USER_BO_ROLES.DATA_DOWNLOADER);
                }
                if (vm.userData.etatActivites) {
                    roles.push(USER_BO_ROLES.ETAT_ACTIVITES);
                }
                return roles;
            }
        }]);