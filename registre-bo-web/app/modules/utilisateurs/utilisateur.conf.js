/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.users', [
    'hatvpRegistre.service',
    'hatvpRegistre.env_conf_autogenerated',
    'blockUI'
])

    .config(['$stateProvider', 'EnvConf', 'HTML', 'USER_BO_ROLES', function ($stateProvider, EnvConf, HTML, USER_BO_ROLES) {
        $stateProvider.state('users', {
            url: '/gestion/utilisateurs',
            templateUrl: EnvConf.views_dir + HTML.USERS.HOME,
            controller: 'UsersCtrl',
            params: {
                saveSearch: null
            },
            data: {
                requireLogin: true,
                roles: [USER_BO_ROLES.ADMINISTRATEUR]
            }
        });
    }]);