/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.validation', [
    'hatvpRegistre.service',
    'hatvpRegistre.env_conf_autogenerated'
])

    .config(['$stateProvider', 'EnvConf', 'HTML', 'USER_BO_ROLES',
        function ($stateProvider, EnvConf, HTML, USER_BO_ROLES) {
            $stateProvider.state('validation', {
                url: '/validation',
                templateUrl: EnvConf.views_dir + HTML.VALIDATION,
                controller: 'ValidationCtrl',
                params: {
                    saveSearch: null
                },
                data: {
                    requireLogin: true,
                    roles: [USER_BO_ROLES.VALIDATION_ESPACES_COLLABORATIFS]
                }
            });
        }]);