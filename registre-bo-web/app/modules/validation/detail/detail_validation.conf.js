/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.validation')

    .config(['$stateProvider', 'EnvConf', 'HTML', 'USER_BO_ROLES',
        function ($stateProvider, EnvConf, HTML, USER_BO_ROLES) {
            $stateProvider.state('detail_validation', {
                url: '/validation/detail/:id',
                views: {
                    "": {
                        templateUrl: EnvConf.views_dir + HTML.DETAIL_VALIDATION,
                        controller: 'DetailValidationCtrl'
                    },
                    "menubar": {
                        templateUrl: EnvConf.views_dir + HTML.MENUBAR_DETAILS,
                        controller: 'DetailsMenubarCtrl'
                    }
                },
                params: {
                    id: null,
                    validationInscriptionId: null,
                    saveSearch: null,
                    locked: false
                },
                data: {
                    requireLogin: true,
                    returnState: null,
                    menuBarPreviousTitle: "Listes des espaces collaboratifs",
                    menuBarActionsTemplateUrl: EnvConf.views_dir + HTML.MENUBAR_VALIDATION,
                    isDetailValidation: true,
                    roles: [USER_BO_ROLES.VALIDATION_ESPACES_COLLABORATIFS, USER_BO_ROLES.MANAGER]
                }
            });
        }]);