/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.validation')

    .controller('DetailValidationCtrl', ['$scope', '$state', '$stateParams', 'blockUI', '$q', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'API', 'HTML', 'TABLE_OPTIONS', 'DetailValidationScope', 'ValidationEspaceService', 'EspaceService', 'ETAT_ESPACE_ORGANISATION', 'DeclarantsService', '$window',
        function ($scope, $state, $stateParams, blockUI, $q, $log, $mdDialog, $mdToast, EnvConf, API, HTML, TABLE_OPTIONS, DetailValidationScope, ValidationEspaceService, EspaceService, ETAT_ESPACE_ORGANISATION, DeclarantsService, $window) {
            var vm = $scope;

            DetailValidationScope.store('DetailValidationCtrl', $scope);

            /**
             * Déclarations de variables et de constantes.
             */
            vm.validation_vo = {};
            vm.validation_to = {};
            vm.declarant = [];

            /**
             * URL pour le service de téléchargement des pièces.
             */
            vm.pieceDownloadUrl = EnvConf.api + API.corporate.root + API.corporate.pieces.root + API.corporate.pieces.download;

            //identifiant de l'espace organisatione en cours de validation en cours de traitement
            vm.validationId = $stateParams.id;
            //identifiant de l'inscriptione en cours de validation en cours de traitement

            if ($stateParams.validationInscriptionId === null) {
                $state.go('espaces');
            } else {
                vm.validationInscriptionId = $stateParams.validationInscriptionId;
            }

            // liste des champs de l'organisation
            vm.formFields = [
                {name: 'denomination', label: "Dénomination de l'entité", editable: $stateParams.locked?false:true, required: true},
                {name: 'nomUsage', label: "Nom d'usage de l'entité", editable: $stateParams.locked?false:true, required: false},
                {name: 'nomUsageHatvp', label: "Nom d'usage HATVP", editable: $stateParams.locked?false:true, required: false},
                {name: 'sigleHatvp', label: "Sigle HATVP", editable: $stateParams.locked?false:true, required: false},
                {name: 'ancienNomHatvp', label: "Ancien Nom HATVP", editable: $stateParams.locked?false:true, required: false},
                {name: 'date_creation', label: "Date d'inscription"},
                {name: 'nom_demandeur', label: "Identité du demandeur", redirect: true},
                {name: 'email_demandeur', label: "Email du demandeur"},
                {name: 'nationalId', label: "Identifiant National"},
                {name: 'typeIdentifiantNational', label: "Type Identifiant national"},
                {name: 'adresse', label: "Adresse", editable: $stateParams.locked?false:true, multiline: true, max: "500", required: true},
                {name: 'codePostal', label: "Code postal", editable: $stateParams.locked?false:true, pattern: "\\d+", max: "10", required: true},
                {name: 'ville', label: "Ville", editable: $stateParams.locked?false:true, max: "150", required: true},
                {name: 'pays', label: "Pays", editable: $stateParams.locked?false:true, max: "150", required: true},
                {name: 'nonPublierMonAdressePhysique', label: "Non publication adresse"},
                {name: 'emailDeContact', label: "Email de contact", editable: $stateParams.locked?false:true, max: "150", required: false},
                {name: 'nonPublierMonAdresseEmail', label: "Non publication email"},
                {name: 'telephoneDeContact', label: "Telephone de dontact", editable: $stateParams.locked?false:true, max: "150", required: false},
                {name: 'nonPublierMonTelephoneDeContact', label: "Non publication téléphone"},
                {name: 'lienSiteWeb', label: "Site web", editable: $stateParams.locked?false:true, max: "150", required: false},
                {name: 'lienPageTwitter', label: "Twitter", editable: $stateParams.locked?false:true, max: "150", required: false},
                {name: 'lienPageFacebook', label: "Facebook", editable: $stateParams.locked?false:true, max: "150", required: false},
                {name: 'lienPageLinkedin', label: "Linkedin", editable: $stateParams.locked?false:true, max: "150", required: false},
                {name: 'finExerciceFiscal', label: "Fin exercice", editable: $stateParams.locked?false:true, required: false},
                {name: 'nonExerciceComptable', label: "Non exercice"},
                {name: 'dirigeants', label: "Dirigeants", multiline: true, required: true}
            ];

            vm.etats = Object.values(ETAT_ESPACE_ORGANISATION);

            /**
             * Fonctions privées
             */

            var initData = function () {
                blockUI.start();
                $q.all([
                    ValidationEspaceService.getById($stateParams.id).then(function (res) {

                        vm.validation_to = res.data;
                        vm.validation_vo = {
                            denomination: res.data.denomination,
                            nomUsage: res.data.nomUsage,
                            nomUsageHatvp: res.data.nomUsageHatvp,
                            sigleHatvp: res.data.sigleHatvp,
                            ancienNomHatvp: res.data.ancienNomHatvp,
                            date_creation: res.data.creationDate,
                            nom_demandeur: res.data.createurNomComplet,
                            email_demandeur: res.data.createurEmail,
                            nationalId: res.data.nationalId,
                            typeIdentifiantNational: res.data.typeIdentifiantNational,
                            adresse: res.data.adresse,
                            codePostal: res.data.codePostal,
                            ville: res.data.ville,
                            pays: res.data.pays,
                            dirigeants: res.data.dirigeants.join(', '),
                            representantLegal: res.data.representantLegal,
                            codeStatut: res.data.statut,
                            statut: ETAT_ESPACE_ORGANISATION[res.data.statut.code],
                            createurId: res.data.createurId,
                            organisationId: res.data.organisationId,
                            nonPublierMonAdressePhysique: res.data.nonPublierMonAdressePhysique ? 'Oui' : 'Non', 
                            emailDeContact: res.data.emailDeContact, 
                            nonPublierMonAdresseEmail: res.data.nonPublierMonAdresseEmail ? 'Oui' : 'Non', 
                            telephoneDeContact: res.data.telephoneDeContact,  
                            nonPublierMonTelephoneDeContact: res.data.nonPublierMonTelephoneDeContact  ? 'Oui' : 'Non', 
                            lienSiteWeb: res.data.lienSiteWeb,  
                            lienPageTwitter: res.data.lienPageTwitter, 
                            lienPageFacebook: res.data.lienPageFacebook,  
                            lienPageLinkedin: res.data.lienPageLinkedin,  
                            finExerciceFiscal: res.data.finExerciceFiscal,    
                            nonExerciceComptable: res.data.nonExerciceComptable ? 'Oui' : 'Non', 
                            locked: $stateParams.locked
                        };
                    }, function (err) {blockUI.stop();
                        $state.go('espaces');
                    }),
                    EspaceService.getComplement($stateParams.id).then(function (res) {
                        vm.complement = res.data;
                    }, function (err) {
                        $state.go('espaces');
                    }),
                    EspaceService.getPiecesCreation($stateParams.id).then(function (res) {
                        vm.piecesCreation = res.data;
                    }, function (err) {
                        $state.go('espaces');
                    })
                ]).finally(function () {
                    blockUI.stop();
                });
            };

            /**
             * Fonctions publiques sur le scope
             */

            // Modification des données organisation et état espace organisation
            vm.submitCorporateData = function (ev, validation_vo) {

                // mapping des changements de l'organisation
                vm.validation_to.denomination = validation_vo.denomination;
                vm.validation_to.nomUsage = validation_vo.nomUsage;
                vm.validation_to.nomUsageHatvp = validation_vo.nomUsageHatvp;
                vm.validation_to.sigleHatvp = validation_vo.sigleHatvp;
                vm.validation_to.ancienNomHatvp = validation_vo.ancienNomHatvp;
                vm.validation_to.adresse = validation_vo.adresse;
                vm.validation_to.codePostal = validation_vo.codePostal;
                vm.validation_to.ville = validation_vo.ville;
                vm.validation_to.pays = validation_vo.pays;

                for (var etat in ETAT_ESPACE_ORGANISATION) {
                    if (validation_vo.statut === ETAT_ESPACE_ORGANISATION[etat]) {
                        // vm.validation_to.statut = etat;
                        vm.validation_to.statut = {code: etat, label: ETAT_ESPACE_ORGANISATION[etat]};
                    }
                }

                // Service modification
                blockUI.start();
                ValidationEspaceService.update(vm.validation_to).then(function (res) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Informations mises à jour")
                            .textContent("Les informations de " + res.data.denomination + " ont été enregistrées avec succès.")
                            .ariaLabel("Informations mises à jour")
                            .targetEvent(ev)
                            .ok('OK')
                            .clickOutsideToClose(true)
                    ).then(function () {
                        $state.go('espaces');
                    });
                    vm.validation_to = res.data;
                }).finally(function () {
                    blockUI.stop();
                });
            };

            vm.goToDeclarant = function (id) {
                blockUI.start();
                $q.all([
                    DeclarantsService.getDeclarantById(id).then(function (res) {
                        vm.declarant.id = res.data.id;
                        vm.declarant.version = res.data.version;
                        vm.declarant.civilite = res.data.civilite;
                        vm.declarant.nom = res.data.nom;
                        vm.declarant.prenom = res.data.prenom;
                        vm.declarant.email = res.data.email;
                        vm.declarant.actif = res.data.activated;
                        vm.declarant.emails_complement = res.data.emails_complement;
                        vm.declarant.telephones = res.data.telephones;
                        vm.declarant.lastConnexionDate = res.data.lastConnexionDate;
                        vm.declarant.organisations = res.data.listeOrganisationClient.join(",");
                    })
                ]).then(function () {

                    $state.go('declarantDetails', {
                        declarant: vm.declarant,
                        returnState: 'detail_validation({id: ' + vm.validationId + ', validationInscriptionId: ' + vm.validationInscriptionId + '})',
                        menuBarPreviousTitle: 'Retour aux détails pour ' + vm.validation_vo.denomination
                    });

                }).finally(function () {
                    blockUI.stop();
                });
            };

            /**
             * Initialisation
             */
            initData();

        }])
    .controller('DetailValidationMenuBarCtrl', ['$scope', '$state', '$stateParams', 'blockUI', '$q', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'API', 'HTML', 'TABLE_OPTIONS', 'DetailValidationScope', 'ValidationEspaceService', 'EspaceService', 'UsersService', 'GroupService','SurveillanceService',
        function ($scope, $state, $stateParams, blockUI, $q, $log, $mdDialog, $mdToast, EnvConf, API, HTML, TABLE_OPTIONS, DetailValidationScope, ValidationEspaceService, EspaceService, UsersService, GroupService,SurveillanceService) {

            var vm = $scope;
            vm.locked = $stateParams.locked;

            if ($stateParams.saveSearch !== null) {
                vm.saveSearch = $stateParams.saveSearch;
                $state.get('detail_validation').data.returnState = 'espaces({saveSearch: ' + JSON.stringify(vm.saveSearch) + '})';
                vm.returnState = 'espaces({saveSearch: ' + JSON.stringify(vm.saveSearch) + '})';
            } else {
                $state.get('detail_validation').data.returnState = 'espaces';
                vm.returnState = $state.get('detail_validation').data.returnState;
            }

            // Demander complément pour un espace organisation
            vm.demandeComplement = function (ev) {
                $mdDialog.show({
                    controller: 'DemandeComplementCtrl',
                    templateUrl: EnvConf.views_dir + HTML.DEMANDE_COMPLEMENT_VALIDATION,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                        validation_to: DetailValidationScope.get('DetailValidationCtrl').validation_to
                    }
                });
            };

            // Valider demande espace organisation
            vm.validateEspace = function (ev) {
                var confirm = $mdDialog.confirm()
                    .title("Confirmation validation")
                    .textContent("Vous êtes sur le point de valider un espace collaboratif nouvellement créé.")
                    .ariaLabel("Confirmation validation")
                    .targetEvent(ev)
                    .ok("Confirmer la validation")
                    .cancel("Annuler");

                $mdDialog.show(confirm).then(function () {
                    blockUI.start();
                    ValidationEspaceService.validate(DetailValidationScope.get('DetailValidationCtrl').validation_to.id).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Validation espace collaboratif")
                                .textContent("L'espace de " + DetailValidationScope.get('DetailValidationCtrl').validation_to.denomination + " a été validé.")
                                .ariaLabel("Validation espace collaboratif")
                                .targetEvent(ev)
                                .ok('OK')
                                .clickOutsideToClose(true)
                        ).then(function () {
                            $state.go('espaces');
                        });
                    }).finally(function () {
                        blockUI.stop();
                    });
                }, function () {
                });
            };

            // Rejeter demande espace organisation
            vm.reject = function (ev) {
                $mdDialog.show({
                    controller: 'MotifValidationCtrl',
                    templateUrl: EnvConf.views_dir + HTML.MOTIF_VALIDATION,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                        validation_to: DetailValidationScope.get('DetailValidationCtrl').validation_to
                    }
                });
            };

            vm.putEspaceUnderWatch = function (ev) {

                SurveillanceService.getSurveillanceByOrganisationId(DetailValidationScope.get('DetailValidationCtrl').validation_vo.organisationId).then(function (res) {
                    vm.surveillance = res.data;
                })

                UsersService.getAllBackOfficeUsers().then(function (res) {
                    vm.users = res.data;
                })

                EspaceService.getStatutEnum().then(function (res) {
                    vm.statuts = res.data.surveillanceOrganisationEnumList;
                })

                GroupService.getAllGroupAndUsers().then(function(res){
                    vm.groups = res.data;
                })

                $mdDialog.show({
                    controller: function () {
                        this.parent = $scope;
                    },
                    controllerAs: 'ctrl',
                    scope: $scope,
                    preserveScope: true,
                    templateUrl: EnvConf.views_dir + "/espace/surveillance.html",
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            };

            vm.validate = function () {
                blockUI.start();
                if (vm.surveillance.id !=null){
                    SurveillanceService.updateSurveillance(vm.surveillance).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Mise sous surveillance modifiée")
                                .textContent("Mise sous surveillance modifiée")
                                .ariaLabel("Mise sous surveillance modifiée")
                                .ok('OK')
                                .clickOutsideToClose(true));
                    }).finally(function () {
                        blockUI.stop();
                    });
                }
                else {
                    SurveillanceService.saveSurveillance(vm.surveillance).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Mise sous surveillance effectuée")
                                .textContent("Mise sous surveillance effectuée")
                                .ariaLabel("Mise sous surveillance effectuée")
                                .ok('OK')
                                .clickOutsideToClose(true));
                    }).finally(function () {
                        blockUI.stop();
                    });
                }

            };

            vm.closeModal = function (ev) {
                $mdDialog.hide();
            };

        }])
    .controller('MotifValidationCtrl', ['$scope', '$rootScope', 'blockUI', '$state', '$stateParams', '$mdDialog', 'MOTIF_REFUS_VALIDATION', 'ValidationEspaceService', 'validation_to', 'CommentService',
        function ($scope, $rootScope, blockUI, $state, $stateParams, $mdDialog, MOTIF_REFUS_VALIDATION, ValidationEspaceService, validation_to, CommentService) {
            var vm = $scope;

            /**
             * Constantes
             */
            vm.images_dir = $rootScope.images_dir;
            vm.motifs = Object.values(MOTIF_REFUS_VALIDATION);
            vm.selected_motif = vm.motifs[0];
            vm.motif_msg = "";

            $scope.comment = {};

            /**
             * Fonctions du scope
             */

            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            vm.submit = function (msg, valid) {
                if (valid) {// si formulaire valide

                    var confirm = $mdDialog.confirm()
                        .title("Confirmation refus")
                        .textContent("Vous êtes sur le point de refuser une inscription à un nouvel espace collaboratif.")
                        .ariaLabel("Confirmation refus")
                        .ok("Confirmer le refus")
                        .cancel("Annuler");

                    $mdDialog.show(confirm).then(function () {

                        var comment = {
                            message: "Refus : " + vm.motif_msg,
                            contextId: validation_to.validationInscriptionId
                        };

                        blockUI.start();
                        ValidationEspaceService.reject(validation_to.id, vm.motif_msg).then(function (res) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Validation espace collaboratif")
                                    .textContent("La validation de " + validation_to.denomination + " a été refusée.")
                                    .ariaLabel("Validation espace collaboratif")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            ).then(function () {
                                CommentService.addValidationComment(comment);
                                $state.go('espaces');
                            });
                        }).finally(function () {
                            blockUI.stop();
                        });
                    });
                }
            };

            vm.motifSelectionChange = function () {
                ValidationEspaceService.getMotifTemplate(validation_to.id, vm.selected_motif).then(function (res) {
                    vm.motif_msg = res.data;
                });
            };
        }
    ])

    /**
     * Controller utilisé pour la modale de demande de complément d'information.
     * Permet la modification de l'email à envoyer au déclarant.
     */
    .controller('DemandeComplementCtrl', ['$scope', '$rootScope', 'blockUI', '$state', '$stateParams', '$mdDialog', 'MOTIF_DEMANDE_COMPLEMENT', 'ValidationEspaceService', 'validation_to', 'CommentService', '$http', '$filter',
        function ($scope, $rootScope, blockUI, $state, $stateParams, $mdDialog, MOTIF_DEMANDE_COMPLEMENT, ValidationEspaceService, validation_to, CommentService) {
            var vm = $scope;

            //#####################################//
            // Constantes
            //#####################################//
            vm.images_dir = $rootScope.images_dir;
            vm.motifs = Object.values(MOTIF_DEMANDE_COMPLEMENT);
            vm.selected_motif = "";
            vm.motif_msg = "";

            //#####################################//
            // Fonctions du scope
            //#####################################//

            /**
             * Ferme la modale.
             */
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            /**
             * Soumet le formulaire.
             */
            vm.submit = function (msg, valid) {
                if (valid) {// si formulaire valide
                    blockUI.start();

                    var comment = {
                        message: "Complément : " + vm.demande_complement_msg,
                        contextId: validation_to.validationInscriptionId
                    };

                    ValidationEspaceService.demandeComplement(validation_to.id, vm.demande_complement_msg).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Complément espace collaboratif")
                                .textContent("Un complément d'information pour la création de l'espace de " + validation_to.denomination + " a été demandé.")
                                .ariaLabel("Complément espace collaboratif")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        ).then(function () {
                            CommentService.addValidationComment(comment);
                            $state.go('espaces');
                        });
                    }).finally(function () {
                        blockUI.stop();
                    });
                }
            };

            /**
             * Met à jour le contenu du courriel de demande de complément à envoyer.
             */
            vm.motifSelectionChange = function () {
                ValidationEspaceService.getDemandeComplementTemplate(validation_to.id, vm.selected_motif).then(function (res) {
                    vm.demande_complement_msg = res.data;
                });
            };
        }
    ])
    .factory('DetailValidationScope', ['$rootScope', function ($rootScope) {
        var mem = {};

        return {
            store: function (key, value) {
                $rootScope.$emit('scope.stored', key);
                mem[key] = value;
            },
            get: function (key) {
                return mem[key];
            }
        }
    }]);