/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.validation')

    .controller('ValidationCtrl', ['$filter', '$rootScope', '$scope', '$state', '$stateParams', '$log', 'blockUI', '$q', '$mdDialog', '$mdToast', '$timeout', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'ValidationEspaceService', 'EspaceLockService', 'ETAT_ESPACE_ORGANISATION',
        function ($filter, $rootScope, $scope, $state, $stateParams, $log, blockUI, $q, $mdDialog, $mdToast, $timeout, EnvConf, HTML, TABLE_OPTIONS, ValidationEspaceService, EspaceLockService, ETAT_ESPACE_ORGANISATION) {

            var vm = $scope;

            /**
             * Constantes
             */

            vm.validations = [];
            vm.search = false;
            vm.typeDeRecherche = "";
            vm.validationsTableOptions = angular.copy(TABLE_OPTIONS);
            vm.validationsTableOptions.limit = 50;
            vm.statuts = [
                {id: "NOUVELLE", label: "Nouvelle"},
                {id: "EN_TRAITEMENT", label: "En traitement"},
                {id: "EN_ATTENTE_VALIDATION", label: "En attente de validation"},
                {id: "COMPLEMENT_DEMANDE_ENVOYEE", label: "Complément demandé"},
                {id: "COMPLEMENT_RECU", label: "Complément reçu"},
            ];
            vm.statutRecherche = "";

            /**
             * Fonctions
             */

            var initTable = function (page, limit) {
                // reinit des variables de recherche
                vm.search = false;
                vm.typeDeRecherche = "";
                vm.raisonSocialeKeywords = "";
                vm.identifantDeclarantKeywords = "";
                vm.dateKeywords = "";
                vm.statutRecherche = "";

                vm.validationsTableOptions.promise = ValidationEspaceService.getPaginated(page, limit);
                vm.validationsTableOptions.promise.then(function (res) {
                    vm.validations = res.data.dtos.map(function (v) {
                        return {
                            id: v.id,
                            validationInscriptionId: v.validationInscriptionId,
                            version: angular.isDefined(v.lock) ? v.lock.version : null,
                            espaceOrganisationId: v.id,
                            raisonSociale: v.denomination,
                            identifantDeclarant: v.createurNomComplet,
                            dateDemande: v.creationDate,
                            etat: ETAT_ESPACE_ORGANISATION[v.statut],
                            locked: v.lock !== undefined && v.lock !== null && !angular.equals(v.lock.utilisateurBoId, $rootScope.currentUser().id),
                            comment: v.commente ? "Ceci est le dernier commentaire" : null,
                            boUserId: angular.isDefined(v.lock) ? v.lock.utilisateurBoId : null
                        };
                    });
                    vm.validations.totalNumber = res.data.resultTotalNumber;
                });

                blockUI.start();
                $q.all([
                    vm.validationsTableOptions.promise
                ]).finally(function () {
                    blockUI.stop();
                });
            };

            /** Generation tableau de recherche **/
            var searchTable = function (type, page, limit) {
                vm.search = true;
                vm.typeDeRecherche = type;

                switch(type) {
                    case 'denomination':
                        vm.validationsTableOptions.promise = ValidationEspaceService.getPaginatedParRecherche(type, vm.raisonSocialeKeywords, page, limit);
                        break;
                    case 'demandeur':
                        vm.validationsTableOptions.promise = ValidationEspaceService.getPaginatedParRecherche(type, vm.identifantDeclarantKeywords, page, limit);
                        break;
                    case 'dateDemande':
                        vm.validationsTableOptions.promise = ValidationEspaceService.getPaginatedParRecherche(type, vm.dateKeywords.replace('/', '-'), page, limit);
                        break;
                    case 'statut':
                        vm.validationsTableOptions.promise = ValidationEspaceService.getPaginatedParRecherche(type, vm.statutRecherche, page, limit);
                        break;
                    default:
                        vm.validationsTableOptions.promise = ValidationEspaceService.getPaginated(page, limit);
                }

                vm.validationsTableOptions.promise.then(function (res) {
                    vm.validations = res.data.dtos.map(function (v) {
                        return {
                            id: v.id,
                            validationInscriptionId: v.validationInscriptionId,
                            version: angular.isDefined(v.lock) ? v.lock.version : null,
                            espaceOrganisationId: v.id,
                            raisonSociale: v.denomination,
                            identifantDeclarant: v.createurNomComplet,
                            dateDemande: v.creationDate,
                            etat: ETAT_ESPACE_ORGANISATION[v.statut],
                            locked: v.lock !== undefined && v.lock !== null && !angular.equals(v.lock.utilisateurBoId, $rootScope.currentUser().id),
                            comment: v.commente ? "Ceci est le dernier commentaire" : null,
                            boUserId: angular.isDefined(v.lock) ? v.lock.utilisateurBoId : null
                        };
                    });
                    vm.validations.totalNumber = res.data.resultTotalNumber;
                });

                vm.saveSearch = {
                    search: vm.search,
                    typeDeRecherche: vm.typeDeRecherche,
                    raisonSocialeKeywords: vm.raisonSocialeKeywords,
                    identifantDeclarantKeywords: vm.identifantDeclarantKeywords,
                    dateKeywords: vm.dateKeywords,
                    statutRecherche: vm.statutRecherche
                };

                blockUI.start();
                $q.all([
                    vm.validationsTableOptions.promise
                ]).finally(function () {
                    blockUI.stop();
                });
            };

            /**
             * Réinitialise le lock sur une ligne.
             */
            function setLock(validationContent) {
                validationContent.locked = false;
                EspaceLockService.setLock(validationContent.id).then(function (res) {
                    $state.go('detail_validation', {
                        id: validationContent.id,
                        validationInscriptionId: validationContent.validationInscriptionId,
                        saveSearch: vm.saveSearch,
                        returnState: 'validation({saveSearch: ' + vm.saveSearch + '})'
                });
                });
            }

            /**
             * Mise à jour du lock sur une ligne.
             */
            function updateLock(validationContent) {
                validationContent.locked = false;
                EspaceLockService.updateLock(validationContent).then(function (res) {
                    validationContent.version = res.data.version;
                    $state.go('detail_validation', {
                        id: validationContent.id,
                        validationInscriptionId: validationContent.validationInscriptionId,
                        saveSearch: vm.saveSearch
                    });
                });
            }

            /**
             * Vérifier si le lock existe toujours.
             */
            function checkLock(validationContent) {
                if (!angular.equals($rootScope.currentUser().id, validationContent.boUserId)) {
                    EspaceLockService.getLock(validationContent.id).then(function (res) {
                        if (res.data.lockTimeRemain > 0) {
                            // verrouiller l'accès à la ligne
                            validationContent.locked = true;
                            var lockRemainingTime = $filter('number')(res.data.lockTimeRemain / 60, 0);
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Validation en cours")
                                    .textContent("La validation de cet espace collaboratif est en cours d’analyse par " + res.data.utilisateurBoName
                                        + ". Elle sera de nouveau accessible dans " + lockRemainingTime + " minutes.")
                                    .ariaLabel("Validation en cours")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            );
                            // déverrouiller la ligne après le temps imparti
                            $timeout(function () {
                                validationContent.locked = false;
                            }, res.data.lockTimeRemain * 1000);
                        }
                        else {
                            setLock(validationContent);
                        }
                    });
                }
            }

            vm.editValidationEspace = function (validationContent) {

                if (!angular.equals(validationContent.etat, ETAT_ESPACE_ORGANISATION.NOUVELLE) && !$rootScope.isManager()) {
                    return false;
                }

                if (angular.isDefined(validationContent.boUserId)) {
                    if (angular.equals($rootScope.currentUser().id, validationContent.boUserId)) {
                        updateLock(validationContent);
                    } else {
                        checkLock(validationContent);
                    }
                } else {
                    checkLock(validationContent);
                }
            };

            /**
             * Appel paginé pour la liste des mandants
             * @param page
             * @param limit
             * @returns {*}
             */
            vm.onPaginate = function (page, limit) {
                initTable(page, limit);
            };

            /**
             * Appel paginé pour la liste des mandants
             * @param denomination
             * @param page
             * @param limit
             * @returns {*}
             */
            vm.onPaginateSearch = function (page, limit) {
                searchTable(vm.typeDeRecherche, page, limit);
            };

            /**
             * Reset du tableau sans recherches
             */
            vm.reset = function() {
                initTable(vm.validationsTableOptions.page, vm.validationsTableOptions.limit);
            };

            vm.callSearch = function(typeDeRecherche) {
                vm.typeDeRecherche = typeDeRecherche;
                searchTable(typeDeRecherche, 1, vm.validationsTableOptions.limit);
            };

            vm.statutChanged = function(statut) {
                vm.statutRecherche = statut;
                searchTable('statut', 1, vm.validationsTableOptions.limit);
            };

            /**
             * Initialisations
             */
            if($stateParams.saveSearch !== null && $stateParams.saveSearch.search) {
                vm.search = $stateParams.saveSearch.search;
                vm.typeDeRecherche = $stateParams.saveSearch.typeDeRecherche;
                vm.raisonSocialeKeywords = $stateParams.saveSearch.raisonSocialeKeywords;
                vm.identifantDeclarantKeywords = $stateParams.saveSearch.identifantDeclarantKeywords;
                vm.dateKeywords = $stateParams.saveSearch.dateKeywords;
                vm.statutRecherche = $stateParams.saveSearch.statutRecherche;
                searchTable(vm.typeDeRecherche, 1, vm.validationsTableOptions.limit);
            } else {
                initTable(vm.validationsTableOptions.page, vm.validationsTableOptions.limit);
            }

        }]);