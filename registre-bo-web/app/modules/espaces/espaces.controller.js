/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.espaces')
    .controller('EspacesCtrl', ['blockUI', '$q', '$rootScope', '$scope', '$state', '$stateParams', '$log', '$mdDialog', '$mdToast', '$filter', '$timeout', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'USER_ROLES', 'EspacesService', 'EspaceLockService',
        function (blockUI, $q, $rootScope, $scope, $state, $stateParams, $log, $mdDialog, $mdToast, $filter, $timeout, EnvConf, HTML, TABLE_OPTIONS, USER_ROLES, EspacesService, EspaceLockService) {

            var vm = $scope;

            vm.espacesTableOptions = angular.copy(TABLE_OPTIONS);

            vm.statuts = [];

            vm.search = false;
            vm.searchType = "";
            vm.denominationSearch = "";
            vm.identifiantSearch = "";
            vm.contactsSearch = "";
            vm.creationDateSearch = "";
            vm.validationDateSearch = "";
            vm.statutSearch = '';          

            vm.initTable = function () {

                vm.search = false;
                vm.searchType = "";
                vm.denominationSearch = "";
                vm.identifiantSearch = "";
                vm.contactsSearch = "";
                vm.creationDateSearch = "";
                vm.validationDateSearch = "";
                vm.statutSearch = '';
                vm.saveSearch = {};

                blockUI.start();

                // Permet de retourner sur l'EC d'origine depuis la fiche déclarant du CO
                if($rootScope.espaceDorigine != undefined){
                    vm.espace = $rootScope.espaceDorigine;
                    $rootScope.espaceDorigine = undefined;
                    $state.go('espaceDetails', {
                        espace: vm.espace,
                        menuBarPreviousTitle: 'Liste des espaces collaboratifs',
                        returnState: 'espaces',
                        cardOrga: {active: false},
                        saveSearch: vm.saveSearch,
                        espaceId: vm.espace.id
                    });
                }                

                EspacesService.getStatutType().then(function (res) {
                    vm.statuts = res.data.statutEspaceEnumList;
                });

                EspacesService.getNotValidatedEspacePaginated(vm.espacesTableOptions.page, vm.espacesTableOptions.limit).then(function (res) {
                    vm.espaces = res.data.dtos.map(function (e) {
                        return {
                            id: e.id,
                            inscriptionId: e.inscriptionId,
                            organisationId: e.organisationId,
                            denomination: e.denomination,
                            nomUsage: e.nomUsage,
                            nomUsageHatvp: e.nomUsageHatvp,
                            ancienNomHatvp: e.ancienNomHatvp,
                            sigleHatvp: e.sigleHatvp,
                            identifiant: e.identifiant,
                            contacts: e.contacts.map(function (c) {
                                return c.identite;
                            }).join(", "),
                            admins: e.contacts.map(function (i) {
                                return {
                                    id: i.id,
                                    identite: i.identite,
                                    actif:i.actif
                                };
                            }),
                            creationDate: e.creationDate,
                            validationDate: e.validationDate,
                            statut: e.statut.label,
                            finExerciceFiscal: e.finExerciceFiscal,
                            publication: e.isPublication,
                            actif: e.actif,
                            relancesBloquees: e.relancesBloquees,
                            locked: e.lock !== undefined && e.lock !== null && !angular.equals(e.lock.utilisateurBoId, $rootScope.currentUser().id),
                            boUserId: angular.isDefined(e.lock) ? e.lock.utilisateurBoId : null,
                            desinscriptionAgent: e.desinscriptionAgent,
                            dateDesinscription: e.dateDesinscription
                        }

                    });

                    vm.espaces.totalNumber = res.data.resultTotalNumber;
                }).finally(function () {
                    blockUI.stop();
                })
            };

            vm.searchTable = function (type, page, occurrence) {
                vm.searchType = type;

                switch (type) {
                    case 'denomination':
                        if (vm.denominationSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = EspacesService.getEspaceBySearchPaginated(type, vm.denominationSearch, page, occurrence);
                        break;
                    case 'identifiant':
                        if (vm.identifiantSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = EspacesService.getEspaceBySearchPaginated(type, vm.identifiantSearch, page, occurrence);
                        break;
                    case 'contacts':
                        if (vm.contactsSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = EspacesService.getEspaceBySearchPaginated(type, vm.contactsSearch, page, occurrence);
                        break;
                    case 'creationDate':
                        if (vm.creationDateSearch === undefined) {
                            return;
                        }
                        vm.search = true;
                        vm.promise = EspacesService.getEspaceBySearchPaginated(type, vm.creationDateSearch, page, occurrence);
                        break;
                    case 'validationDate':
                        if (vm.validationDateSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = EspacesService.getEspaceBySearchPaginated(type, vm.validationDateSearch, page, occurrence);
                        break;
                    case 'statut':
                        vm.search = true;
                        vm.promise = EspacesService.getEspaceBySearchPaginated(type, vm.statutSearch, page, occurrence);
                        break;
                }

                vm.promise.then(function (res) {
                    vm.espaces = res.data.dtos.map(function (e) {
                        return {
                            id: e.id,
                            inscriptionId: e.inscriptionId,
                            organisationId: e.organisationId,
                            denomination: e.denomination,
                            nomUsage: e.nomUsage,
                            nomUsageHatvp: e.nomUsageHatvp,
                            ancienNomHatvp: e.ancienNomHatvp,
                            sigleHatvp: e.sigleHatvp,
                            identifiant: e.identifiant,
                            contacts: e.contacts.map(function (c) {
                                return c.identite;
                            }).join(", "),
                            admins: e.contacts.map(function (i) {
                                return {
                                    id: i.id,
                                    identite: i.identite,
                                    actif:i.actif
                                };
                            }),
                            creationDate: e.creationDate,
                            validationDate: e.validationDate,
                            statut: e.statut.label,
                            finExerciceFiscal: e.finExerciceFiscal,
                            publication: e.isPublication,
                            actif: e.actif,
                            relancesBloquees: e.relancesBloquees,
                            dateCreation: e.creationDate,
                            data: {
                                id: e.id,
                                denomination: e.denomination,
                                nomUsage: e.nomUsage,
                                nomUsageHatvp: e.nomUsageHatvp,
                                sigleHatvp: e.sigleHatvp,
                                ancienNomHatvp: e.ancienNomHatvp,
                                finExerciceFiscal: e.finExerciceFiscal,
                                jourFin: e.finExerciceFiscal === undefined ? undefined : e.finExerciceFiscal.split("-")[0] < 10 ? e.finExerciceFiscal.split("-")[0].substring(1) : e.finExerciceFiscal.split("-")[0],
                                moisFin: e.finExerciceFiscal === undefined ? undefined : e.finExerciceFiscal.split("-")[1] < 10 ? e.finExerciceFiscal.split("-")[1].substring(1) : e.finExerciceFiscal.split("-")[1]
                            },
                            locked: e.lock !== undefined && e.lock !== null && !angular.equals(e.lock.utilisateurBoId, $rootScope.currentUser().id),
                            boUserId: angular.isDefined(e.lock) ? e.lock.utilisateurBoId : null,
                            desinscriptionAgent: e.desinscriptionAgent,
                            dateDesinscription: e.dateDesinscription
                        };
                    });
                    vm.espaces.totalNumber = res.data.resultTotalNumber;

                    vm.saveSearch = {
                        search: vm.search,
                        searchType: vm.searchType,
                        denominationSearch: vm.denominationSearch,
                        identifiantSearch: vm.identifiantSearch,
                        contactsSearch: vm.contactsSearch,
                        creationDateSearch: vm.creationDateSearch,
                        validationDateSearch: vm.validationDateSearch,
                        statutSearch: vm.statutSearch,
                        limit: vm.espacesTableOptions.limit,
                        page: vm.espacesTableOptions.page
                    };
                });

                EspacesService.getStatutType().then(function (res) {
                    vm.statuts = res.data.statutEspaceEnumList;
                });
            };

            vm.onPaginate = function (page, occurrence) {
                vm.initTable();
            };

            vm.onPaginateSearch = function (page, limit) {
                vm.searchTable(vm.searchType, page, limit);
            };

            vm.callSearch = function (type) {
                vm.searchType = type;
                vm.searchTable(type, 1, vm.espacesTableOptions.limit);
            };

            vm.statutChanged = function (statut) {
                vm.statutSearch = statut;
                vm.searchTable('statut', 1, vm.espacesTableOptions.limit);
            };

            vm.getDetails = function (space) {
                if (!vm.search) {
                    vm.saveSearch = {
                        search: vm.search,
                        limit: vm.espacesTableOptions.limit,
                        page: vm.espacesTableOptions.page
                    };
                }

                if (space.statut === 'Acceptée' || space.statut === 'Désinscription demandée' || space.statut === 'Désinscrit'
                    || space.statut === 'Désinscription validée' || space.statut === 'Compléments demande de désinscription reçu'
                    || space.statut === 'Compléments demande de désinscription envoyé') {
                    $state.go('espaceDetails', { 
                        espace: space,
                        menuBarPreviousTitle: 'Liste des espaces collaboratifs',
                        returnState: 'espaces',
                        cardOrga: {active: false},
                        saveSearch: vm.saveSearch,
                        espaceId: space.id
                    });
                } else {
                    $state.go('detail_validation', {
                        id: space.id,
                        locked: space.locked,
                        validationInscriptionId: space.inscriptionId,
                        saveSearch: vm.saveSearch,
                        returnState: 'validation({saveSearch: ' + vm.saveSearch + '})'
                    });
                }
            };

            vm.accessDetails = function (event, space) {
                if (angular.isDefined(space.boUserId)) {
                    if (angular.equals($rootScope.currentUser().id, space.boUserId)) {
                        vm.updateLock(space);
                    } else {
                        vm.checkLock(space);
                    }
                } else {
                    vm.checkLock(space);
                }
            };

            vm.updateLock = function (space) {
                space.locked = false;
                EspaceLockService.updateLock(space).then(function (res) {
                    space.version = res.data.version;
                    vm.getDetails(space);
                });
            };
            //1er arrivé récupère les droit de modif les autres juste les droits en lecture
            vm.checkLock = function (space) {
                if (!angular.equals($rootScope.currentUser().id, space.boUserId)) {
                    EspaceLockService.getLock(space.id).then(function (res) {
                        if (res.data.lockTimeRemain > 0) {
                            // verrouiller l'accès à la ligne
                            space.locked = true;
                            var lockRemainingTime = $filter('number')(res.data.lockTimeRemain / 60, 0);
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Validation en cours")
                                    .textContent("L'agent  " + res.data.utilisateurBoName + ' a la main sur cette fiche. Votre accès est uniquement en consultation.' 
                                        + " Elle sera de nouveau accessible dans " + lockRemainingTime + " minutes.")
                                    .ariaLabel("Validation en cours")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            );
                            vm.getDetails(space);
                            // déverrouiller la ligne après le temps imparti
                            $timeout(function () {
                                space.locked = false;
                            }, res.data.lockTimeRemain * 1000);
                        }
                        else {
                            vm.setLock(space);
                        }
                    });
                }
            };

            vm.setLock = function(space) {
                space.locked = false;
                EspaceLockService.setLock(space.id).then(function (res) {
                    vm.getDetails(space);
                });
            };

            if ($stateParams.saveSearch !== null && $stateParams.saveSearch.search) {
                vm.search = $stateParams.saveSearch.search;
                vm.searchType = $stateParams.saveSearch.searchType;
                vm.denominationSearch = $stateParams.saveSearch.denominationSearch;
                vm.identifiantSearch = $stateParams.saveSearch.identifiantSearch;
                vm.contactsSearch = $stateParams.saveSearch.contactsSearch;
                vm.creationDateSearch = $stateParams.saveSearch.creationDateSearch;
                vm.validationDateSearch = $stateParams.saveSearch.validationDateSearch;
                vm.statutSearch = $stateParams.saveSearch.statutSearch;
                vm.espacesTableOptions.limit = $stateParams.saveSearch.limit;
                vm.espacesTableOptions.page = $stateParams.saveSearch.page;

                vm.searchTable(vm.searchType, vm.espacesTableOptions.page,  vm.espacesTableOptions.limit);
            } else {
                if ($stateParams.saveSearch !== null && $stateParams.saveSearch.limit && $stateParams.saveSearch.page) {
                    vm.espacesTableOptions.limit = $stateParams.saveSearch.limit;
                    vm.espacesTableOptions.page = $stateParams.saveSearch.page;
                }
                vm.initTable();
            }

        }]);