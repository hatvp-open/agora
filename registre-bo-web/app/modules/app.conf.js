/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre')

.config(['$compileProvider', '$locationProvider', '$urlRouterProvider', '$mdThemingProvider',
    '$mdDateLocaleProvider', '$httpProvider', '$mdDialogProvider',
    function ($compileProvider, $locationProvider, $urlRouterProvider, $mdThemingProvider,
              $mdDateLocaleProvider, $httpProvider, $mdDialogProvider) {
    // Angular Material Datepicker fix for Angular 1.6.x
    // $compileProvider.preAssignBindingsEnabled(true);

    // url paths settings
    $locationProvider.hashPrefix('!');
    $urlRouterProvider.otherwise("/home");

    // Gestion de thèmes
    $mdThemingProvider.alwaysWatchTheme(true);

    // Authentication header tokens filter
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('csrfInterceptor');
    $httpProvider.interceptors.push('serverErrorInterceptor');

    // Datepicker format settings
    $mdDateLocaleProvider.formatDate = function(date) {
        if (date != undefined)
            return new Date(date.getTime() + Math.abs(date.getTimezoneOffset()*60000))
                .toLocaleDateString('fr-FR');
        return null;
    };

}]);