/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.home')

    .controller('LoginCtrl', ['$scope', '$log', '$window', '$mdDialog', '$mdToast', '$location', 'AuthService', '$rootScope', '$state', '$timeout',
        function ($scope, $log, $window, $mdDialog, $mdToast, $location, AuthService, $rootScope, $state, $timeout) {
            var vm = $scope;

            vm.authModel = {
                email: "",
                password: ""
            };

            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
                $state.go('home');
            };

            vm.login = function (ev, valid) {
                if (valid) {// Si formulaire valide
                    $mdDialog.hide();
                    AuthService.init().then(function (res) {
                        AuthService.authenticate(vm.authModel).then(function (response) {
                            // done = true;
                            if (response.status === 200) {
                                // Authentification success
                                $mdToast.show(
                                    $mdToast.simple()
                                        .textContent('Vous êtes maintenant authentifié.')
                                        .action('Fermer')
                                        .position('top right')
                                        .hideDelay(8000)
                                );
                                $log.info("Auth succeeded, user data is " + JSON.stringify(response.data));
                                if ($rootScope.targetState)
                                    $state.go($rootScope.targetState, {}, {reload: true});
                                else
                                    $state.go('home', {}, {reload: true});
                            }
                        });
                    });
                }
                else {
                    $log.error("Formulaire d'authentification non valide!")
                }

            };

            vm.forgotPassword = function (email, ev) {
                $mdDialog.hide();
                var confirm = $mdDialog.prompt()
                    .title('Mot de passe oublié')
                    .textContent('Veuillez entrer votre email principal:')
                    .placeholder('Email principal')
                    .ariaLabel('Email principal')
                    .initialValue(email)
                    .targetEvent(ev)
                    .ok('Récupérer')
                    .cancel('Abandonner');

                $mdDialog.show(confirm).then(function (email_result) {
                    if (email_result.length > 0) {
                        AuthService.init().then(function (res) {
                            // ProfileService.init_password_1(email_result).then(function (res) {
                            //     $mdDialog.show(
                            //         $mdDialog.alert()
                            //             .targetEvent(ev)
                            //             .title("Mot de passe oublié")
                            //             .textContent("Un email de réinitialisation de votre mot de passe a été envoyé à l'adresse email "
                            //                 + vm.authModel.email)
                            //             .ariaLabel("Mot de passe oublié")
                            //             .ok('OK')
                            //             .clickOutsideToClose(true)
                            //     );
                            // });
                        });
                    }
                });
            };
        }]);