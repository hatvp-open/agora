/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.home')

    .config(['$stateProvider', 'EnvConf', 'HTML', function ($stateProvider, EnvConf, HTML) {
        $stateProvider.state('login', {
            url: "/login",
            templateUrl: EnvConf.views_dir + HTML.HOME.LOGIN,
            controller: 'LoginCtrl'
        });
    }]);