/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.home')

    .controller('HomeCtrl', ['blockUI', '$q', '$scope', '$rootScope', '$state', '$stateParams', 'AuthService', 'EnvConf', 'HTML',
        function (blockUI, $q, $scope, $rootScope, $state, $stateParams, AuthService, EnvConf, HTML) {
            var vm = $scope;
            var root = $rootScope;

            /**
             * Home view options.
             */
            vm.loginView = true;
            vm.home_views = {
                login: EnvConf.views_dir + HTML.HOME.LOGIN,
                signup: EnvConf.views_dir + HTML.HOME.SIGNUP
            };

            // Corporate home forward when user authenticated
            if (root.isConnected()) {
                if(root.isAdministrateur()){
                    $state.go('users');
                }else if(root.isGestionnaireCompteDeclarant()){
                    $state.go('declarant');
                } else if (root.isGestionnaireEspaceCollaboratifValides()){
                    $state.go('espace');
                } else if (root.isValidationEspaceCollaboratif()){
                    $state.go('validation');
                } else{
                    $state.go('no-where');
                }
            }

        }])
    .controller('HomelessCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'AuthService', 'EnvConf', 'HTML',
    function ($scope, $rootScope, $state, $stateParams, AuthService, EnvConf, HTML) {

    }])

    /**
     * Controlleur de la page Home avec affichage des messages d'erreur
     */
    .controller('HomeMsgCtrl', ['$scope', '$rootScope', '$log', '$state', '$stateParams', '$mdDialog', '$mdToast',
        function ($scope, $rootScope, $log, $state, $stateParams, $mdDialog, $mdToast) {
            $mdDialog.hide();

            switch ($stateParams.entity) {
                case 'authError':
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Erreur d'authentification. Veuillez réessayer.")
                            .action('Fermer')
                            .highlightAction(true)
                            .position('top right')
                            .hideDelay(8000)
                    );
                    $state.go('home');
                    break;
                case 'sessionError':
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Session expirée")
                            .textContent("Veuillez vous réauthentifier.")
                            .ariaLabel("Session expirée")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    );
                    $rootScope.logout();
                    break;
                case 'technicalError':
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Service indisponible")
                            .textContent("Le service est indisponible pour le moment. Veuillez joindre votre administrateur.")
                            .ariaLabel("Erreur technique")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    ).then(function () {
                        $state.go('home');
                    });
                    break;
                case 'appError':
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Erreur")
                            .textContent($stateParams.msg)
                            .ariaLabel("Erreur")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    ).then(function () {
                        $state.go('home');
                    });
                    break;
                default:
                    $state.go('home');
                    break;
            }
        }]);