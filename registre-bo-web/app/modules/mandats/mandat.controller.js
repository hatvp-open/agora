/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.mandates')
    .controller('MandatesCtrl', ['blockUI', '$q', '$scope', '$state', '$stateParams', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'USER_BO_ROLES', 'MANDAT_GESTION_TYPE', 'MANDAT_GESTION_STATUT', 'MandatesService',
        function (blockUI, $q, $scope, $state, $stateParams, $log, $mdDialog, $mdToast, EnvConf, HTML, TABLE_OPTIONS, USER_BO_ROLES, MANDAT_GESTION_TYPE, MANDAT_GESTION_STATUT, MandatesService) {
            var vm = $scope;

            vm.mandats = [];

            vm.mandatsTableOptions = angular.copy(TABLE_OPTIONS);

            vm.mandatsStatus = angular.copy(MANDAT_GESTION_TYPE);

            vm.statutRecherche = "";
            vm.statuts = [
                {id: "NOUVELLE", label: "Nouvelle"},
                {id: "COMPLEMENT_DEMANDE", label: "Complément demandé"},
                {id: "COMPLEMENT_RECU", label: "Complément reçu"},
                {id: "ACCEPTEE", label: "Acceptée"},
                {id: "REFUSEE", label: "Refusée"},
            ];
            vm.types = ["AJOUT_OPERATIONNEL", "REMPLACEMENT_OPERATIONNEL", "CHANGEMENT_REPRESENTANT"];

            vm.types = [
                {id: "AJOUT_OPERATIONNEL", label: "Ajout d'un nouveau contact opérationnel"},
                {id: "REMPLACEMENT_OPERATIONNEL", label: "Remplacement d'un contact opérationnel"},
                {id: "CHANGEMENT_REPRESENTANT", label: "Changement de représentant légal"}
            ];

            vm.typeRecherche = "";

            vm.demandes = [];
            vm.search = false;
            vm.typeDeRecherche = "";

            var initTable = function () {

                vm.search = false;
                vm.typeDeRecherche = "";
                vm.denomination = "";
                vm.declarantFullname = "";
                vm.statutRecherche = "";
                vm.typeRecherche = "";
                vm.dateDemande = "";
                vm.statut = "";


                vm.mandatsTableOptions.promise = MandatesService.getPaginated(vm.mandatsTableOptions.page, vm.mandatsTableOptions.limit);
                vm.mandatsTableOptions.promise.then(function (res) {
                    vm.mandats = res.data.dtos.map(function (m) {
                        return {
                            id: m.id,
                            raisonSociale: m.raisonSociale,
                            demandeur: m.declarantDemande.nom + " " + m.declarantDemande.prenom,
                            typeDemande: vm.mandatsStatus[m.typeDemande],
                            dateDemande: m.dateDemande,
                            statut: m.statut
                        };
                    });
                    vm.mandats.totalNumber = res.data.resultTotalNumber;
                });

                blockUI.start();
                $q.all([
                    vm.mandatsTableOptions.promise
                ]).finally(function () {
                    blockUI.stop();
                });
            };

            /** Generation tableau de recherche **/
            var searchTable = function (type, page, limit) {
                vm.search = true;
                vm.typeDeRecherche = type;

                switch (type) {
                    case 'denomination':
                        if (vm.denomination === "") {
                            vm.search = false;
                            return;
                        }
                        vm.mandatsTableOptions.promise = MandatesService.getPaginatedParRecherche(type, vm.denomination, page, limit);
                        break;
                    case 'declarantFullName':
                        if (vm.declarantFullname === "") {
                            vm.search = false;
                            return;
                        }
                        vm.mandatsTableOptions.promise = MandatesService.getPaginatedParRecherche(type, vm.declarantFullname, page, limit);
                        break;
                    case 'typeDemande':
                        vm.mandatsTableOptions.promise = MandatesService.getPaginatedParRecherche(type, vm.typeRecherche, page, limit);
                        break;
                    case 'dateDemande':
                        if (vm.dateKeywords === undefined) {
                            vm.search = false;
                            return;
                        }
                        vm.mandatsTableOptions.promise = MandatesService.getPaginatedParRecherche(type, vm.dateKeywords, page, limit);
                        break;
                    case 'statut':
                        vm.mandatsTableOptions.promise = MandatesService.getPaginatedParRecherche(type, vm.statutRecherche, page, limit);
                        break;
                    default:
                        vm.mandatsTableOptions.promise = MandatesService.getPaginated(page, limit);
                }

                vm.mandatsTableOptions.promise.then(function (res) {
                    vm.mandats = res.data.dtos.map(function (m) {
                        return {
                            id: m.id,
                            raisonSociale: m.raisonSociale,
                            demandeur: m.declarantDemande.nom + " " + m.declarantDemande.prenom,
                            typeDemande: vm.mandatsStatus[m.typeDemande],
                            dateDemande: m.dateDemande,
                            statut: m.statut
                        };
                    });
                    vm.mandats.totalNumber = res.data.resultTotalNumber;

                    vm.saveSearch = {
                        search: vm.search,
                        typeDeRecherche: vm.typeDeRecherche,
                        denomination: vm.denomination,
                        declarantFullname: vm.declarantFullname,
                        typeRecherche: vm.typeRecherche,
                        dateKeywords: vm.dateKeywords,
                        statutRecherche: vm.statutRecherche
                    };

                });

                blockUI.start();
                $q.all([
                    vm.mandatsTableOptions.promise
                ]).finally(function () {
                    blockUI.stop();
                });
            };


            vm.getMandatesDetails = function (idMandat, typeMandat) {
                $state.go('mandatesDetails', {
                    idMandat: idMandat,
                    typeMandat: typeMandat,
                    saveSearch: vm.saveSearch
                })
            };

            vm.onPaginate = function () {
                initTable();
            };

            /**
             * Appel paginé pour la liste
             * @param denomination
             * @param page
             * @param limit
             * @returns {*}
             */
            vm.onPaginateSearch = function (page, limit) {
                searchTable(vm.typeDeRecherche, page, limit);
            };

            /**
             * Reset du tableau sans recherches
             */
            vm.reset = function () {
                initTable();
            };

            vm.callSearch = function (typeDeRecherche) {
                vm.typeDeRecherche = typeDeRecherche;
                searchTable(typeDeRecherche, 1, vm.mandatsTableOptions.limit);
            };

            vm.statutChanged = function (statut) {
                vm.statutRecherche = statut;
                searchTable('statut', 1, vm.mandatsTableOptions.limit);
            };

            vm.demandeChanged = function (type) {
                vm.typeRecherche = type;
                searchTable('typeDemande', 1, vm.mandatsTableOptions.limit);
            };

            if ($stateParams.saveSearch !== null && $stateParams.saveSearch.search) {
                vm.search = $stateParams.saveSearch.search;
                vm.typeDeRecherche = $stateParams.saveSearch.typeDeRecherche;
                vm.denomination = $stateParams.saveSearch.denomination;
                vm.declarantFullname = $stateParams.saveSearch.declarantFullname;
                vm.statutRecherche = $stateParams.saveSearch.statutRecherche;
                vm.typeRecherche = $stateParams.saveSearch.typeRecherche;
                vm.dateDemande = $stateParams.saveSearch.dateDemande;
                vm.statut = $stateParams.saveSearch.statut;
                searchTable(vm.typeDeRecherche, 1, vm.mandatsTableOptions.limit);
            } else {
                initTable();
            }

        }])

    .controller('MandatesDetailsCtrl', ['blockUI', '$q', '$stateParams', '$scope', '$state', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'API', 'MANDAT_GESTION_TYPE', 'USER_BO_ROLES', 'TABLE_OPTIONS', 'MandatesService', 'EspaceService',
        function (blockUI, $q, $stateParams, $scope, $state, $log, $mdDialog, $mdToast, EnvConf, HTML, API, MANDAT_GESTION_TYPE, USER_BO_ROLES, TABLE_OPTIONS, MandatesService, EspaceService) {

            var vm = $scope;

            vm.id = $stateParams.idMandat;

            vm.mandatsStatus = angular.copy(MANDAT_GESTION_TYPE);

            vm.pieceDownloadUrl = EnvConf.api + API.corporate.root + API.corporate.pieces.root + API.corporate.pieces.download;

            vm.pieces = [];


            var initData = function () {
                blockUI.start();
                $q.all([
                    MandatesService.getMandateById(vm.id).then(function (res) {
                        vm.mandate = res.data;
                        vm.mandate.raisonSocialeId = res.data.espaceOrganisationId;
                        vm.mandate.raisonSociale = res.data.raisonSociale;
                        vm.mandate.demandeurId = res.data.declarantDemande.id;
                        vm.mandate.demandeur = res.data.declarantDemande.nom + " " + res.data.declarantDemande.prenom;
                        vm.mandate.dateDemande = res.data.dateDemande;
                        vm.mandate.typeDemande = vm.mandatsStatus[res.data.typeDemande];

                        vm.contactOpShow = res.data.typeDemande === MANDAT_GESTION_TYPE.CHANGEMENT_REPRESENTANT;

                        vm.mandate.motifDemande = res.data.motifDemande;
                        vm.mandate.motifDemande = angular.isDefined(res.data.motifDemande) ? res.data.motifDemande : 'Pas de commentaire';

                        if (res.data.declarantPromotion !== undefined) {
                            vm.mandate.nouveauContactOpId = res.data.declarantPromotion.id;
                            vm.mandate.nouveauContactOp = res.data.declarantPromotion.nom + " " + res.data.declarantPromotion.prenom;
                        }

                        vm.demandeurComplet = res.data.declarantDemande;
                        vm.nouveauContactComplet = res.data.declarantDemande;

                        if (res.data.identitePieceRepresentantLegal !== undefined) {
                            vm.identitePieceRepresentantLegal = {
                                id: res.data.identitePieceRepresentantLegal.id,
                                nomFichier: res.data.identitePieceRepresentantLegal.nomFichier,
                                dateVersementPiece: res.data.identitePieceRepresentantLegal.dateVersementPiece
                            };
                            vm.pieces.push(vm.identitePieceRepresentantLegal);
                        }

                        if (res.data.mandatPieceRepresentantLegal !== undefined) {
                            vm.mandatPieceRepresentantLegal = {
                                id: res.data.mandatPieceRepresentantLegal.id,
                                nomFichier: res.data.mandatPieceRepresentantLegal.nomFichier,
                                dateVersementPiece: res.data.mandatPieceRepresentantLegal.dateVersementPiece
                            };
                            vm.pieces.push(vm.mandatPieceRepresentantLegal);
                        }

                        res.data.complementPieceRepresentantLegal.forEach(function (piece) {
                            vm.pieces.push(piece);
                        });

                    })
                ]).finally(function () {
                    blockUI.stop();
                });
            };

            //Go to espace details page.
            vm.getRaisonSociale = function (id) {
                $q.all([
                    EspaceService.getOneEspace(id).then(function (res) {
                        vm.espaceContent = res.data.espaceOrganisation;
                        vm.espace = {
                            id: res.data.espaceOrganisation.id,
                            version: res.data.espaceOrganisation.version,
                            actif: res.data.espaceOrganisation.espaceActif,
                            publication: res.data.espaceOrganisation.publication,
                            denomination: res.data.organisationData.denomination,
                            nomUsage: res.data.espaceOrganisation.nomUsage,
                            identifiant: res.data.organisationData.nationalId,
                            organisationId: res.data.organisationData.id,
                            administrateurs: res.data.espaceOrganisation.administrateurs
                                .map(function (e) {
                                    return e.civilite + " " + e.nom + " " + e.prenom;
                                }).join(", "),
                            nbre_declarant: res.data.espaceOrganisation.nbrDeclarant,
                            date_creation: res.data.espaceOrganisation.creationDate,
                            representantLegal: res.data.espaceOrganisation.representantLegal,
                            validationInscriptionId: res.data.validationInscriptionId,
                            admins: res.data.espaceOrganisation.administrateurs
                                .map(function (e) {
                                    var d = {};
                                    d.identite = e.civilite + " " + e.nom + " " + e.prenom;
                                    d.id = e.id;
                                    return d;
                                }),
                            data: {
                                id: res.data.organisationData.id,
                                nomUsage: res.data.espaceOrganisation.nomUsage
                            }
                        }
                    })
                ]).finally(function () {
                    $state.go('espaceDetails', {
                        cardOrga: {active: false},
                        espace: vm.espace,
                        returnState: 'mandates',
                        menuBarPreviousTitle: 'Liste des mandats de gestion'
                    });
                })

            };

            // go to declaran details page
            vm.getDeclarant = function (declarantContent) {
                $state.go('declarantDetails', {
                    declarant: declarantContent,
                    returnState: 'mandates',
                    menuBarPreviousTitle: 'Liste des mandats de gestion'
                });
            };

            if (vm.id === null) {
                $state.go('mandates');
            } else {
                initData();
            }
        }])

    .controller('MandatesMenuBarCtrl', ['blockUI', '$q', '$stateParams', '$scope', '$state', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'USER_BO_ROLES', 'MandatesService',
        function (blockUI, $q, $stateParams, $scope, $state, $log, $mdDialog, $mdToast, EnvConf, HTML, TABLE_OPTIONS, USER_BO_ROLES, MandatesService) {

            var vm = $scope;

            vm.id = $stateParams.idMandat;
            vm.typeMandat = $stateParams.typeMandat;

            if ($stateParams.saveSearch !== null) {
                vm.saveSearch = $stateParams.saveSearch;
                $state.get('mandatesDetails').data.returnState = 'mandates({saveSearch: ' + JSON.stringify(vm.saveSearch) + '})';
                vm.returnState = 'mandates({saveSearch: ' + JSON.stringify(vm.saveSearch) + '})';
            } else {
                $state.get('mandatesDetails').data.returnState = 'mandates';
                vm.returnState = $state.get('mandatesDetails').data.returnState;
            }


            var generateAlertDialog = function (event, title, message, label) {
                return $mdDialog.alert()
                    .title(title)
                    .textContent(message)
                    .ariaLabel(label)
                    .targetEvent(event)
                    .ok("OK")
                    .clickOutsideToClose(true);
            };

            vm.rejectMandate = function (event) {
                blockUI.start();
                $q.all([
                    MandatesService.rejectMandate(vm.id)
                ]).finally(function () {
                    $mdDialog.show(generateAlertDialog(event, "Demande Refusée", "La demande a été refusée", "Mandat de gestion"));
                    $state.go('mandates');
                });
                blockUI.stop();
            };

            vm.validateMandate = function (event) {
                blockUI.start();
                $q.all([
                    MandatesService.validateMandate(vm.id)
                ]).finally(function () {
                    $mdDialog.show(generateAlertDialog(event, "Demande Validée", "La demande a été validée", "Mandat de gestion"));
                    $state.go('mandates');
                });
                blockUI.stop();
            };
            // Demander complément pour un espace organisation
            vm.demandeComplement = function (ev) {
                $mdDialog.show({
                    controller: 'DemandeComplementMandatCtrl',
                    templateUrl: EnvConf.views_dir + HTML.DEMANDE_COMPLEMENT_MANDATS,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                        idMandat: vm.id,
                        typeMandat: vm.typeMandat
                    }
                });
            };


        }]).controller('DemandeComplementMandatCtrl', ['$scope', '$rootScope', 'blockUI', '$state', '$stateParams', '$mdDialog', 'MOTIF_DEMANDE_COMPLEMENT_AJOUT_CHANGEMENT_CO', 'MOTIF_DEMANDE_COMPLEMENT_CHANGEMENT_REPRESENTANT', 'MandatesService', 'idMandat', 'typeMandat', 'CommentService', '$http', '$filter',
    function ($scope, $rootScope, blockUI, $state, $stateParams, $mdDialog, MOTIF_DEMANDE_COMPLEMENT_AJOUT_CHANGEMENT_CO, MOTIF_DEMANDE_COMPLEMENT_CHANGEMENT_REPRESENTANT, MandatesService, idMandat, typeMandat, CommentService) {
        var vm = $scope;

        //#####################################//
        // Constantes
        //#####################################//
        vm.images_dir = $rootScope.images_dir;
        if (typeMandat === 'Changement de représentant légal') vm.motifs = Object.values(MOTIF_DEMANDE_COMPLEMENT_CHANGEMENT_REPRESENTANT);
        else vm.motifs = Object.values(MOTIF_DEMANDE_COMPLEMENT_AJOUT_CHANGEMENT_CO);
        vm.typeMandat = typeMandat;

        vm.selected_motif = "";
        vm.motif_msg = "";

        //#####################################//
        // Fonctions du scope
        //#####################################//

        /**
         * Ferme la modale.
         */
        vm.cancel = function () {
            $mdDialog.cancel();
        };

        /**
         * Soumet le formulaire.
         */
        vm.submit = function (msg, valid) {
            if (valid) {// si formulaire valide
                blockUI.start();

                var comment = {
                    message: "Complément : " + vm.demande_complement_msg,
                    contextId: idMandat
                };

                MandatesService.demandeComplement(idMandat, vm.demande_complement_msg).then(function (res) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Complément changements des mandats de gestion")
                            .textContent("Un complément d'information a été demandé.")
                            .ariaLabel("Complément changements des mandats de gestion")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    ).then(function () {
                        CommentService.addDemandeChangementMandantComment(comment);
                        $state.go('mandates');
                    });
                }).finally(function () {
                    blockUI.stop();
                });
            }
        };

        /**
         * Met à jour le contenu du courriel de demande de complément à envoyer.
         */
        vm.motifSelectionChange = function () {
            MandatesService.getDemandeComplementTemplate(idMandat, vm.selected_motif).then(function (res) {
                vm.demande_complement_msg = res.data;
            });
        };


    }
]);