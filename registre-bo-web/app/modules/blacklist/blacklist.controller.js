/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.blacklist')
    .controller('BlacklistCtrl', ['blockUI', '$q', '$rootScope', '$scope', '$state', '$stateParams', '$log', '$mdDialog', '$mdToast', '$filter', '$timeout', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'USER_ROLES', 'BlacklistService', 'EspaceLockService',
        function (blockUI, $q, $rootScope, $scope, $state, $stateParams, $log, $mdDialog, $mdToast, $filter, $timeout, EnvConf, HTML, TABLE_OPTIONS, USER_ROLES, BlacklistService, EspaceLockService) {

            var vm = $scope;
            vm.raisonBlacklist = [];

            vm.blacklist =[];
            vm.blacklistTableOptions = angular.copy(TABLE_OPTIONS);
            vm.blacklistTableOptions.limit = 10;
            vm.search = false;
            vm.searchType ="";
            vm.denominationSearch = "";
            vm.identifiantSearch = "";
            vm.creationDateSearch = "";
            vm.validationDateSearch = "";
            vm.dateExCourantSearch = ""; // format "du dd/mm/yyyy au dd/mm/yyyy"
            vm.nbJoursBlacklistSearch = "";
            vm.raisonBlacklistSearch = "";
            vm.dateExPrecedentSearch = ""; // format "du dd/mm/yyyy au dd/mm/yyyy"
            vm.nbJoursBlacklistExPrecedentFASearch = "";
            vm.nbJoursBlacklistExPrecedentMASearch = "";
            vm.nbJoursBlacklistTotalFASearch = "";
            vm.nbJoursBlacklistTotalMASearch = "";
            vm.lienEspaceOrgaSearch = "";

            vm.initTable = function () {

                vm.search = false;
                vm.searchType ="";
                vm.denominationSearch = "";
                vm.identifiantSearch = "";
                vm.creationDateSearch = "";
                vm.validationDateSearch = "";
                vm.dateExCourantSearch = "";
                vm.nbJoursBlacklistSearch = "";
                vm.raisonBlacklistSearch = "";
                vm.dateExPrecedentSearch = "";
                vm.nbJoursBlacklistExPrecedentFASearch = "";
                vm.nbJoursBlacklistExPrecedentMASearch = "";
                vm.nbJoursBlacklistTotalFASearch = "";
                vm.nbJoursBlacklistTotalMASearch = "";
                vm.lienEspaceOrgaSearch = "";
                vm.saveSearch = {};

                blockUI.start();

                BlacklistService.getTypeRaisonBlacklist().then(function (res) {
                    vm.raisonBlacklist = res.data.raisonBlacklistEnumList;
                });

                BlacklistService.getEspaceBlacklisted(vm.blacklistTableOptions.page, vm.blacklistTableOptions.limit).then(function (res) {
                    vm.blacklist = res.data.dtos.map(function (e) {
                        return {
                            id: e.id,
                            //denomination: e.nomUsage ? e.nomUsage : e.nomUsageHatvp ? e.nomUsageHatvp : e.denomination,
                            denomination: e.denomination,
                            nomUsage: e.nomUsage,
                            nomUsageHatvp: e.nomUsageHatvp,
                            ancienNomHatvp: e.ancienNomHatvp,
                            sigleHatvp: e.sigleHatvp,		
                            identifiant: e.identifiant,
                            creationDate: e.creationDate,
                            validationDate: e.validationDate,
                            dateExCourant: ('du '+e.listeExercices['0'].dateDebut+' au '+e.listeExercices['0'].dateFin).replace(/-/g,'/'), // du dd/mm/yyyy au dd/mm/yyyy
                            nbJoursBlacklist :e.listeExercices['0'].raisonBlacklistage=='fiche d\'activités et moyen alloués'? e.listeExercices['0'].nbJoursBlacklistFA:e.listeExercices['0'].nbJoursBlacklistFA+e.listeExercices['0'].nbJoursBlacklistMA,
                            raisonBlacklist :e.listeExercices['0'].raisonBlacklistage,
                            dateExPrecedent :e.listeExercices['1']!=null?  ('du '+e.listeExercices['1'].dateDebut+' au '+e.listeExercices['1'].dateFin).replace(/-/g,'/'):'pas d\'exercice précédent', // du dd/mm/yyyy au dd/mm/yyyy
                            nbJoursBlacklistExPrecedentFA :(e.listeExercices['1']!=null && e.listeExercices['1'].raisonBlacklistage !=null)? e.listeExercices['1'].nbJoursBlacklistFA:'pas d\'informations',
                            nbJoursBlacklistExPrecedentMA :(e.listeExercices['1']!=null && e.listeExercices['1'].raisonBlacklistage !=null)? e.listeExercices['1'].nbJoursBlacklistMA:'pas d\'informations',
                            nbJoursBlacklistTotalFA :e.listeExercices['1']!=null? e.listeExercices['0'].nbJoursBlacklistFA+e.listeExercices['1'].nbJoursBlacklistFA: e.listeExercices['0'].nbJoursBlacklistFA,
                            nbJoursBlacklistTotalMA :e.listeExercices['1']!=null? e.listeExercices['0'].nbJoursBlacklistMA +e.listeExercices['1'].nbJoursBlacklistMA: e.listeExercices['0'].nbJoursBlacklistMA,
                            inscriptionId: e.inscriptionId,
                            organisationId: e.organisationId,
                            
                            contacts: e.contacts.map(function (c) {
                                return c.identite;
                            }).join(", "),
                            admins: e.contacts.map(function (i) {
                                return {
                                    id: i.id,
                                    identite: i.identite,
                                    actif:i.actif
                                };
                            }),


                            statut: e.statut.label,
                            finExerciceFiscal: e.finExerciceFiscal,
                            publication: e.isPublication,
                            actif: e.actif,
                            relancesBloquees: e.relancesBloquees,
                            locked: e.lock !== undefined && e.lock !== null && !angular.equals(e.lock.utilisateurBoId, $rootScope.currentUser().id),
                            boUserId: angular.isDefined(e.lock) ? e.lock.utilisateurBoId : null
                        }

                    });

                    vm.blacklist.totalNumber = res.data.resultTotalNumber;
                }).finally(function () {
                    blockUI.stop();
                })
            };
            vm.searchTable = function (type, page, occurrence) {
                vm.searchType = type;

                switch (type) {
                    case 'denomination':
                        if (vm.denominationSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = BlacklistService.getEspaceBySearchPaginated(type, vm.denominationSearch, page, occurrence);
                        break;

                    case 'identifiant':
                        if (vm.identifiantSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = BlacklistService.getEspaceBySearchPaginated(type, vm.identifiantSearch, page, occurrence);
                        break;
                         
                    case 'raisonBlacklist':
                        if (vm.raisonBlacklistSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = BlacklistService.getEspaceBySearchPaginated(type, vm.raisonBlacklistSearch, page, occurrence);
                        break;
                }
                vm.promise.then(function (res) {
                    vm.blacklist = res.data.dtos.map(function (e) {
                        return {
                            //denomination: e.nomUsage ? e.nomUsage : e.nomUsageHatvp ? e.nomUsageHatvp : e.denomination,
                            denomination: e.denomination,
                            nomUsage: e.nomUsage,
                            nomUsageHatvp: e.nomUsageHatvp,
                            ancienNomHatvp: e.ancienNomHatvp,
                            sigleHatvp: e.sigleHatvp,
                            id: e.id,
                            inscriptionId: e.inscriptionId,
                            organisationId: e.organisationId,
                            identifiant: e.identifiant,
                            admins: e.contacts.map(function (i) {
                                return {
                                    id: i.id,
                                    identite: i.identite,
                                    actif:i.actif
                                };
                            }),
                            creationDate: e.creationDate,
                            validationDate: e.validationDate,
                            statut: e.statut.label,
                            finExerciceFiscal: e.finExerciceFiscal,
                            publication: e.isPublication,
                            actif: e.actif,
                            relancesBloquees: e.relancesBloquees,
                            desinscriptionAgent: e.desinscription_agent,
                            dateDesinscription: e.dateDesinscription,
                            dateExCourant: ('du '+e.listeExercices['0'].dateDebut+' au '+e.listeExercices['0'].dateFin).replace(/-/g,'/'), // du dd/mm/yyyy au dd/mm/yyyy
                            nbJoursBlacklist :e.listeExercices['0'].raisonBlacklistage=='fiche d\'activités et moyen alloués'? e.listeExercices['0'].nbJoursBlacklistFA:e.listeExercices['0'].nbJoursBlacklistFA+e.listeExercices['0'].nbJoursBlacklistMA,
                            raisonBlacklist :e.listeExercices['0'].raisonBlacklistage,
                            dateExPrecedent :e.listeExercices['1']!=null?  ('du '+e.listeExercices['1'].dateDebut+' au '+e.listeExercices['1'].dateFin).replace(/-/g,'/'):'pas d\'exercice précédent', // du dd/mm/yyyy au dd/mm/yyyy
                            nbJoursBlacklistExPrecedentFA :(e.listeExercices['1']!=null && e.listeExercices['1'].raisonBlacklistage !=null)? e.listeExercices['1'].nbJoursBlacklistFA:'pas d\'informations',
                            nbJoursBlacklistExPrecedentMA :(e.listeExercices['1']!=null && e.listeExercices['1'].raisonBlacklistage !=null)? e.listeExercices['1'].nbJoursBlacklistMA:'pas d\'informations',
                            nbJoursBlacklistTotalFA :e.listeExercices['1']!=null? e.listeExercices['0'].nbJoursBlacklistFA+e.listeExercices['1'].nbJoursBlacklistFA: e.listeExercices['0'].nbJoursBlacklistFA,
                            nbJoursBlacklistTotalMA :e.listeExercices['1']!=null? e.listeExercices['0'].nbJoursBlacklistMA +e.listeExercices['1'].nbJoursBlacklistMA: e.listeExercices['0'].nbJoursBlacklistMA,
                            locked: e.lock !== undefined && e.lock !== null && !angular.equals(e.lock.utilisateurBoId, $rootScope.currentUser().id),
                            boUserId: angular.isDefined(e.lock) ? e.lock.utilisateurBoId : null
                        };
                    });
                    vm.blacklist.totalNumber = res.data.resultTotalNumber;

                    vm.saveSearch = {
                        search: vm.search,
                        searchType: vm.searchType,
                        denominationSearch : vm.denominationSearch,
                        identifiantSearch : vm.identifiantSearch,
                        raisonBlacklistSearch : vm.raisonBlacklistSearch,
                        limit: vm.blacklistTableOptions.limit,
                        page: vm.blacklistTableOptions.page

                    };
                });
                BlacklistService.getTypeRaisonBlacklist().then(function (res) {
                    vm.raisonBlacklist = res.data.raisonBlacklistEnumList;
                });
            };

            vm.onPaginate = function (page, occurrence) {
                vm.initTable();
            };

            vm.onPaginateSearch = function (page, limit) {
                vm.searchTable(vm.searchType, page, limit);
            };

            vm.callSearch = function (type) {
                vm.searchType = type;
                vm.searchTable(type, vm.blacklistTableOptions.page, vm.blacklistTableOptions.limit);
            };

            vm.raisonChanged = function (raison) {
                vm.raisonBlacklistSearch = raison;
                vm.searchTable('raisonBlacklist', 1, vm.blacklistTableOptions.limit);
            };

            vm.getDetails = function (space) {
                if (!vm.search) {
                    vm.saveSearch = {
                        search: vm.search,
                        limit: vm.blacklistTableOptions.limit,
                        page: vm.blacklistTableOptions.page
                    };
                }
                $state.go('espaceDetails', {
                    espace: space,
                    menuBarPreviousTitle: 'Liste des espaces collaboratifs',
                    returnState: 'espaces',
                    cardOrga: {active: false},
                    saveSearch: vm.saveSearch
                });

            };

            vm.accessDetails = function (event, space) {
                if (angular.isDefined(space.boUserId)) {
                    if (angular.equals($rootScope.currentUser().id, space.boUserId)) {
                        vm.updateLock(space);
                    } else {
                        vm.checkLock(space);
                    }
                } else {
                    vm.checkLock(space);
                }
            };

            vm.updateLock = function (space) {
                space.locked = false;
                EspaceLockService.updateLock(space).then(function (res) {
                    space.version = res.data.version;
                    vm.getDetails(space);
                });
            };

            vm.checkLock = function (space) {
                if (!angular.equals($rootScope.currentUser().id, space.boUserId)) {
                    EspaceLockService.getLock(space.id).then(function (res) {
                        if (res.data.lockTimeRemain > 0) {
                            // verrouiller l'accès à la ligne
                            space.locked = true;
                            var lockRemainingTime = $filter('number')(res.data.lockTimeRemain / 60, 0);
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Validation en cours")
                                    .textContent("La validation de cet espace collaboratif est en cours d’analyse par " + res.data.utilisateurBoName
                                        + ". Elle sera de nouveau accessible dans " + lockRemainingTime + " minutes.")
                                    .ariaLabel("Validation en cours")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            );
                            // déverrouiller la ligne après le temps imparti
                            $timeout(function () {
                                space.locked = false;
                            }, res.data.lockTimeRemain * 1000);
                        }
                        else {
                            vm.setLock(space);
                        }
                    });
                }
            };

            vm.setLock = function(space) {
                space.locked = false;
                EspaceLockService.setLock(space.id).then(function (res) {
                    vm.getDetails(space);
                });
            };
            if ($stateParams.saveSearch !== null && $stateParams.saveSearch.search) {
                vm.search = $stateParams.saveSearch.search;
                vm.searchType = $stateParams.saveSearch.searchType;
                vm.denominationSearch = $stateParams.saveSearch.denominationSearch;
                vm.identifiantSearch = $stateParams.saveSearch.identifiantSearch;
                vm.creationDateSearch = $stateParams.saveSearch.creationDateSearch;
                vm.validationDateSearch = $stateParams.saveSearch.validationDateSearch;
                vm.dateExCourantSearch = $stateParams.saveSearch.dateExCourantSearch;
                vm.nbJoursBlacklistSearch = $stateParams.saveSearch.nbJoursBlacklistSearch;
                vm.raisonBlacklistSearch = $stateParams.saveSearch.raisonBlacklistSearch;
                vm.dateExPrecedentSearch = $stateParams.saveSearch.dateExPrecedentSearch;
                vm.nbJoursBlacklistExPrecedentFASearch = $stateParams.saveSearch.nbJoursBlacklistExPrecedentFASearch;
                vm.nbJoursBlacklistExPrecedentMASearch = $stateParams.saveSearch.nbJoursBlacklistExPrecedentMASearch;
                vm.nbJoursBlacklistTotalFASearch = $stateParams.saveSearch.nbJoursBlacklistTotalFASearch;
                vm.nbJoursBlacklistTotalMASearch = $stateParams.saveSearch.nbJoursBlacklistTotalMASearch;
                vm.lienEspaceOrgaSearch = $stateParams.saveSearch.lienEspaceOrgaSearch;
                vm.blacklistTableOptions.limit = $stateParams.saveSearch.limit;
                vm.blacklistTableOptions.page = $stateParams.saveSearch.page;

                vm.searchTable(vm.searchType, vm.blacklistTableOptions.page,  vm.blacklistTableOptions.limit);
            } else {
                if ($stateParams.saveSearch !== null && $stateParams.saveSearch.limit && $stateParams.saveSearch.page) {
                    vm.blacklistTableOptions.limit = $stateParams.saveSearch.limit;
                    vm.blacklistTableOptions.page = $stateParams.saveSearch.page;
                }
                vm.initTable();
            }
    }]);