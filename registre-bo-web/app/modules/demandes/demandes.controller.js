/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.demandes')

    .controller('DemandesCtrl', ['$filter', '$rootScope', '$scope', '$state', '$stateParams', '$log', 'blockUI', '$q', '$mdDialog', '$mdToast', '$timeout', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'DemandesOrganisationService', 'DemandeLockService', 'STATUT_DEMANDE_ORGANISATION',
        function ($filter, $rootScope, $scope, $state, $stateParams, $log, blockUI, $q, $mdDialog, $mdToast, $timeout, EnvConf, HTML, TABLE_OPTIONS, DemandesOrganisationService, DemandeLockService, STATUT_DEMANDE_ORGANISATION) {
            var vm = $scope;

            /**
             * Constantes
             */

            vm.demandes = [];
            vm.search = false;
            vm.typeDeRecherche = "";
            vm.demandesTableOptions = angular.copy(TABLE_OPTIONS);

            vm.statuts = [
                {id: "A_TRAITER", label: "A traiter"},
                {id: "TRAITEE_ORGANISATION_TROUVEE", label: "Traitée: Organisation trouvée"},
                {id: "TRAITEE_ORGANISATION_CREEE", label: "Traitée: Organisation créée"},
                {id: "TRAITEE_ORGANISATION_REFUSEE", label: "Traitée: Organisation refusée"},
                {id: "COMPLEMENT_DEMANDE_ENVOYEE", label: "Complément demandé"},
                {id: "TOUTES_LES_DEMANDES", label: "Toute les demandes"}
            ];

            vm.statutRecherche = "";

            /**
             * Fonctions
             */

            var initTable = function () {
                // reinit des variables de recherche
                vm.search = false;
                vm.typeDeRecherche = "";
                vm.declarantFullnameKeywords = "";
                vm.statutRecherche = "";
                vm.denomination = "";
                vm.dernierAccess = "";


                vm.demandesTableOptions.promise = DemandesOrganisationService.getPaginatedDemandes("TOUTES_LES_DEMANDES", vm.demandesTableOptions.page, vm.demandesTableOptions.limit);
                vm.demandesTableOptions.promise.then(function (res) {
                    vm.demandes = res.data.dtos.map(function (d) {
                        return {
                            id: d.id,
                            version: angular.isDefined(d.lock) ? d.lock.version : null,
                            dateCreation: d.dateCreation,
                            declarantFullname: d.declarantFullname,
                            denomination: d.denomination,
                            adresse: d.adresse,
                            codePostal: d.codePostal,
                            pays: d.pays,
                            type: d.type,
                            ville: d.ville,
                            dernierAgentFullname: d.dernierAgentFullname,
                            statut: STATUT_DEMANDE_ORGANISATION[d.statut],
                            locked: d.lock !== undefined && d.lock !== null && !angular.equals(d.lock.utilisateurBoId, $rootScope.currentUser().id),
                            comment: d.commente ? "Ceci est le dernier commentaire" : null,
                            boUserId: angular.isDefined(d.lock) ? d.lock.utilisateurBoId : null,
                            boUserName: angular.isDefined(d.lock) ? d.lock.utilisateurBoName : null
                        };
                    });
                    vm.demandes.totalNumber = res.data.resultTotalNumber;
                });

                blockUI.start();
                $q.all([
                    vm.demandesTableOptions.promise
                ]).finally(function () {
                    blockUI.stop();
                });
            };
            /** Generation tableau de recherche **/
            var searchTable = function (type, page, limit) {
                vm.typeDeRecherche = type;

                switch (type) {
                    case 'denomination':
                        if (vm.denomination === "") {
                            return;
                        }
                        vm.search = true;
                        vm.demandesTableOptions.promise = DemandesOrganisationService.getPaginatedParRecherche(type, vm.denomination, page, limit);
                        break;
                    case 'demandeur':
                        if (vm.declarantFullnameKeywords === "") {
                            return;
                        }
                        vm.search = true;
                        vm.demandesTableOptions.promise = DemandesOrganisationService.getPaginatedParRecherche(type, vm.declarantFullnameKeywords, page, limit);
                        break;
                    case 'dateDemande':
                        if (vm.dateKeywords === undefined) {
                            return;
                        }
                        vm.search = true;
                        vm.demandesTableOptions.promise = DemandesOrganisationService.getPaginatedParRecherche(type, vm.dateKeywords, page, limit);
                        break;
                    case 'statut':
                        vm.search = true;
                        vm.demandesTableOptions.promise = DemandesOrganisationService.getPaginatedParRecherche(type, vm.statutRecherche, page, limit);
                        break;
                    case 'dernierAccess':
                        if (vm.dernierAccess === "") {
                            return;
                        }
                        vm.search = true;
                        vm.demandesTableOptions.promise = DemandesOrganisationService.getPaginatedParRecherche(type, vm.dernierAccess, page, limit);
                        break;
                    default:
                        vm.demandesTableOptions.promise = DemandesOrganisationService.getPaginated(page, limit);
                }
                vm.demandesTableOptions.promise.then(function (res) {
                    vm.demandes = res.data.dtos.map(function (d) {
                        return {
                            id: d.id,
                            version: angular.isDefined(d.lock) ? d.lock.version : null,
                            dateCreation: d.dateCreation,
                            declarantFullname: d.declarantFullname,
                            denomination: d.denomination,
                            adresse: d.adresse,
                            codePostal: d.codePostal,
                            pays: d.pays,
                            type: d.type,
                            ville: d.ville,
                            dernierAgentFullname: d.dernierAgentFullname,
                            statut: STATUT_DEMANDE_ORGANISATION[d.statut],
                            locked: d.lock !== undefined && d.lock !== null && !angular.equals(d.lock.utilisateurBoId, $rootScope.currentUser().id),
                            comment: d.commente ? "Ceci est le dernier commentaire" : null,
                            boUserId: angular.isDefined(d.lock) ? d.lock.utilisateurBoId : null,
                            boUserName: angular.isDefined(d.lock) ? d.lock.utilisateurBoName : null
                        };
                    });
                    vm.demandes.totalNumber = res.data.resultTotalNumber;

                    vm.saveSearch = {
                        search: vm.search,
                        typeDeRecherche: vm.typeDeRecherche,
                        declarantFullnameKeywords: vm.declarantFullnameKeywords,
                        statutRecherche: vm.statutRecherche,
                        denomination: vm.denomination,
                        dernierAccess: vm.dernierAccess
                    };
                });

                blockUI.start();
                $q.all([
                    vm.demandesTableOptions.promise
                ]).finally(function () {
                    blockUI.stop();
                });
            };

            /**
             * Réinitialise le lock sur une ligne.
             */
            function setLock(demande) {
                demande.locked = false;
                DemandeLockService.setLock(demande.id).then(function (res) {
                    $state.go('detail_demandes', {
                        id: demande.id,
                        traitee: demande.statut !== STATUT_DEMANDE_ORGANISATION.A_TRAITER,
                        saveSearch: vm.saveSearch
                    });
                });
            }

            /**
             * Mise à jour du lock sur une ligne.
             */
            function updateLock(demande) {
                demande.locked = false;
                DemandeLockService.updateLock(demande).then(function (res) {
                    demande.version = res.data.version;
                    $state.go('detail_demandes', {
                        id: demande.id,
                        traitee: demande.statut !== STATUT_DEMANDE_ORGANISATION.A_TRAITER,
                        saveSearch: vm.saveSearch
                    });
                });
            }

            /**
             * Vérifier si le lock existe toujours.
             */
            function checkLock(demande) {
                if (!angular.equals($rootScope.currentUser().id, demande.boUserId)) {
                    DemandeLockService.getLock(demande.id).then(function (res) {
                        if (res.data.lockTimeRemain > 0) {
                            // verrouiller l'accès à la ligne
                            demande.locked = true;
                            var lockRemainingTime = $filter('number')(res.data.lockTimeRemain / 60, 0);
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Validation en cours!")
                                    .textContent("La validation de cet espace collaboratif est en cours d’analyse par " + res.data.utilisateurBoName
                                        + ". Elle sera de nouveau accessible dans " + lockRemainingTime + " minutes.")
                                    .ariaLabel("Validation en cours")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            );
                            // déverrouiller la ligne après le temps imparti
                            $timeout(function () {
                                demande.locked = false;
                            }, res.data.lockTimeRemain * 1000);
                        }
                        else {
                            setLock(demande);
                        }
                    });
                }
            }

            /**
             * Edition d'une demande d'organisation
             * @param id
             * @returns {boolean}
             */
            vm.editDemande = function (id) {

                var demande = vm.demandes.filter(function (e) {
                    return e.id === id;
                })[0];

                if (!angular.equals(demande.statut, STATUT_DEMANDE_ORGANISATION.A_TRAITER) && !$rootScope.isManager()) {
                    return false;
                }

                if (angular.isDefined(demande.boUserId)) {
                    if (angular.equals($rootScope.currentUser().id, demande.boUserId)) {
                        updateLock(demande);
                    } else {
                        checkLock(demande);
                    }
                } else {
                    checkLock(demande);
                }
            };

            vm.onPaginate = function () {
                initTable();
            };
            /**
             * Appel paginé pour la liste des mandants
             * @param denomination
             * @param page
             * @param limit
             * @returns {*}
             */
            vm.onPaginateSearch = function (page, limit) {
                searchTable(vm.typeDeRecherche, page, limit);
            };
            /**
             * Reset du tableau sans recherches
             */
            vm.reset = function () {
                initTable();
            };

            vm.callSearch = function (typeDeRecherche) {
                vm.typeDeRecherche = typeDeRecherche;
                searchTable(typeDeRecherche, 1, vm.demandesTableOptions.limit);
            };

            vm.statutChanged = function (statut) {
                vm.statutRecherche = statut;
                searchTable('statut', 1, vm.demandesTableOptions.limit);
            };


//            vm.getPaginatedStatus = function() {
//                vm.status.push(vm.status.shift());
//                vm.onPaginate();
//            };

            /**
             * Initialisations
             */
            if ($stateParams.saveSearch !== null && $stateParams.saveSearch.search) {
                vm.search = $stateParams.saveSearch.search;
                vm.typeDeRecherche = $stateParams.saveSearch.typeDeRecherche;
                vm.declarantFullnameKeywords = $stateParams.saveSearch.declarantFullnameKeywords;
                vm.statutRecherche = $stateParams.saveSearch.statutRecherche;
                vm.denomination = $stateParams.saveSearch.denomination;
                vm.dernierAccess = $stateParams.saveSearch.dernierAccess;
                searchTable(vm.typeDeRecherche, 1, vm.demandesTableOptions.limit);
            } else {
                initTable();
            }
        }]);