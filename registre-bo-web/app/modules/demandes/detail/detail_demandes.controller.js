/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.demandes')

    .controller('DetailDemandesCtrl', ['$scope', '$state', '$stateParams', 'blockUI', '$q', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'DetailDemandesScope', 'DemandesOrganisationService', 'STATUT_DEMANDE_ORGANISATION', 'EspaceService',
        function ($scope, $state, $stateParams, blockUI, $q, $log, $mdDialog, $mdToast, EnvConf, HTML, TABLE_OPTIONS, DetailDemandesScope, DemandesOrganisationService, STATUT_DEMANDE_ORGANISATION, EspaceService) {
            var vm = $scope;

            DetailDemandesScope.store('DetailDemandesCtrl', $scope);

            /**
             * Déclarations de variables et de constantes
             */

            vm.views = {
                LIST: EnvConf.views_dir + HTML.DEMANDES_DETAIL.TABS.LIST,
                FORM: EnvConf.views_dir + HTML.DEMANDES_DETAIL.TABS.FORM
            };

            vm.hatvpTableOptions = angular.copy(TABLE_OPTIONS);

            //identifiant de la demande organisation en cours de traitement
            if ($stateParams.id === null) {
                $state.go('demandes');
            }
            vm.demandeId = $stateParams.id;
            vm.traitee = $stateParams.traitee;
            vm.organisation = {};

            /**
             * Init VO.
             */
            blockUI.start();
            $q.all([
                DemandesOrganisationService.getById(vm.demandeId).then(function (res) {
                    var d = res.data;

                    vm.demande = {
                        id: d.id,
                        version: angular.isDefined(d.lock) ? d.lock.version : null,
                        espaceOrganisationId: d.espaceOrganisationId,
                        dateCreation: d.dateCreation,
                        declarantFullname: d.declarantFullname,
                        denomination: d.denomination,
                        nomUsage: d.nomUsage,
                        adresse: d.adresse,
                        codePostal: d.codePostal,
                        pays: d.pays,
                        type: d.type,
                        ville: d.ville,
                        dernierAgentFullname: d.dernierAgentFullname,
                        commentaire: d.commentaire,
                        dirigeant: d.dirigeant,
                        idOrgaRecherche: d.identifiant,
                        statut: STATUT_DEMANDE_ORGANISATION[d.statut],
                        boUserId: angular.isDefined(d.lock) ? d.lock.utilisateurBoId : null,
                        boUserName: angular.isDefined(d.lock) ? d.lock.utilisateurBoName : null
                    };

                    vm.traitee = $stateParams.traitee !== null ? $stateParams.traitee : (vm.demande.statut !== STATUT_DEMANDE_ORGANISATION.A_TRAITER);
                }).finally(function (res) {
                    EspaceService.getAllTypeOrganisation().then(function (res) {
                        angular.forEach(res.data, function (val, key) {
                            if (val.code === vm.demande.type) {
                                vm.libeleTypeOrganisation = val.label;
                            }
                        });
                    })
                })

            ]).finally(function (res) {
                blockUI.stop();
            });

            DemandesOrganisationService.getHatvp().then(function (res) {
                vm.hatvpList = res.data;
            });

            // /**
            //  * Clôturer demande: organisation trouvée
            //  * @param ev
            //  */
            // vm.cloturerTrouvee = function (ev) {
            //     DemandesOrganisationService.colturerTrouvee(vm.demandeId).then(function (res) {
            //         $mdDialog.show(
            //             $mdDialog.alert()
            //                 .targetEvent(ev)
            //                 .title("Demande clôturée: Organisation trouvée")
            //                 .textContent("La demande de création d'organisation " + vm.demande.denomination + " est maintenant clôturée.")
            //                 .ariaLabel("Demande clôturée: Organisation trouvée")
            //                 .ok('OK')
            //                 .clickOutsideToClose(true)
            //         ).then(function () {
            //             $state.go('demandes');
            //         });
            //     });
            // };
            //
            // /**
            //  * Clôturer demande: demande refusée
            //  * @param ev
            //  */
            // vm.cloturerRefusee = function (ev) {
            //     DemandesOrganisationService.colturerRefusee(vm.demandeId).then(function (res) {
            //         $mdDialog.show(
            //             $mdDialog.alert()
            //                 .targetEvent(ev)
            //                 .title("Demande clôturée: Refusée")
            //                 .textContent("La demande de création d'organisation " + vm.demande.denomination + " est maintenant clôturée.")
            //                 .ariaLabel("Demande clôturée: Refusée")
            //                 .ok('OK')
            //                 .clickOutsideToClose(true)
            //         ).then(function () {
            //             $state.go('demandes');
            //         });
            //     });
            // };
            //
            // /**
            //  * Rouvrir demande
            //  * @param ev
            //  */
            // vm.rouvrir = function (ev) {
            //     DemandesOrganisationService.rouvrir(vm.demandeId).then(function (res) {
            //         $mdDialog.show(
            //             $mdDialog.alert()
            //                 .targetEvent(ev)
            //                 .title("Demande réouverte")
            //                 .textContent("La demande de création d'organisation " + vm.demande.denomination + " est maintenant réouverte.")
            //                 .ariaLabel("Demande réouverte")
            //                 .ok('OK')
            //                 .clickOutsideToClose(true)
            //         ).then(function () {
            //             $state.go('demandes');
            //         });
            //     });
            // };

            /**
             * Création organisation et clôture de la demande
             * @param valid
             * @param ev
             */
            vm.creerOrganisation = function (valid, ev) {
                if (valid) {
                    DemandesOrganisationService.creerOrganisation(vm.demandeId, vm.organisation).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .targetEvent(ev)
                                .title("Demande clôturée: Créée")
                                .textContent("La demande a été traitée. Une nouvelle organisation a été créée avec le numéro " + res.data.hatvpNumber)
                                .ariaLabel("Demande clôturée: Créée")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        ).then(function () {
                            $state.go('demandes');
                        });
                    });
                }
            };

        }])
    .controller('DetailDemandesMenuBarCtrl', ['$scope', '$state', '$stateParams', 'blockUI', '$q', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'DetailDemandesScope', 'DemandesOrganisationService', 'STATUT_DEMANDE_ORGANISATION', 'EspaceService',
        function ($scope, $state, $stateParams, blockUI, $q, $log, $mdDialog, $mdToast, EnvConf, HTML, TABLE_OPTIONS, DetailDemandesScope, DemandesOrganisationService, STATUT_DEMANDE_ORGANISATION, EspaceService) {
            var vm = $scope;

            vm.traitee = DetailDemandesScope.get('DetailDemandesCtrl').traitee;

            if($stateParams.saveSearch !== null) {
                vm.saveSearch = $stateParams.saveSearch;
                $state.get('detail_demandes').data.returnState = 'demandes({saveSearch: ' + JSON.stringify(vm.saveSearch) + '})';
                vm.returnState = 'demandes({saveSearch: ' + JSON.stringify(vm.saveSearch) + '})';
            } else {
                $state.get('detail_demandes').data.returnState = 'demandes';
                vm.returnState = $state.get('detail_demandes').data.returnState;
            }

            /**
             * Clôturer demande: organisation trouvée
             * @param ev
             */
            vm.cloturerTrouvee = function (ev) {
                DemandesOrganisationService.colturerTrouvee(DetailDemandesScope.get('DetailDemandesCtrl').demandeId).then(function (res) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .targetEvent(ev)
                            .title("Demande clôturée: Organisation trouvée")
                            .textContent("La demande de création d'organisation " + DetailDemandesScope.get('DetailDemandesCtrl').demande.denomination + " est maintenant clôturée.")
                            .ariaLabel("Demande clôturée: Organisation trouvée")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    ).then(function () {
                        $state.go('demandes');
                    });
                });
            };

            /**
             * Clôturer demande: demande refusée
             * @param ev
             */
            vm.cloturerRefusee = function (ev) {
                DemandesOrganisationService.colturerRefusee(DetailDemandesScope.get('DetailDemandesCtrl').demandeId).then(function (res) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .targetEvent(ev)
                            .title("Demande clôturée: Refusée")
                            .textContent("La demande de création d'organisation " + DetailDemandesScope.get('DetailDemandesCtrl').demande.denomination + " est maintenant clôturée.")
                            .ariaLabel("Demande clôturée: Refusée")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    ).then(function () {
                        $state.go('demandes');
                    });
                });
            };

            /**
             * Rouvrir demande
             * @param ev
             */
            vm.rouvrir = function (ev) {
                DemandesOrganisationService.rouvrir(DetailDemandesScope.get('DetailDemandesCtrl').demandeId).then(function (res) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .targetEvent(ev)
                            .title("Demande réouverte")
                            .textContent("La demande de création d'organisation " + DetailDemandesScope.get('DetailDemandesCtrl').demande.denomination + " est maintenant réouverte.")
                            .ariaLabel("Demande réouverte")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    ).then(function () {
                        $state.go('demandes');
                    });
                });
            };
            // Demander complément pour un espace organisation
            vm.demandeComplement = function (ev) {
                $mdDialog.show({
                    controller: 'DemandeComplementDemandeOrgaCtrl',
                    templateUrl: EnvConf.views_dir + HTML.DEMANDE_COMPLEMENT_ORGANISATION,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                    	idDemande : DetailDemandesScope.get('DetailDemandesCtrl').demandeId
                    }
                });
            };

        }
    ])
                .controller('DemandeComplementDemandeOrgaCtrl', ['$scope', '$rootScope', 'blockUI', '$state', '$stateParams', '$mdDialog', 'MOTIF_DEMANDE_ORGANISATION', 'DemandesOrganisationService', 'idDemande', 'CommentService', '$http', '$filter',
        function ($scope, $rootScope, blockUI, $state, $stateParams, $mdDialog, MOTIF_DEMANDE_ORGANISATION, DemandesOrganisationService,idDemande, CommentService) {
            var vm = $scope;

            //#####################################//
            // Constantes
            //#####################################//
            vm.images_dir = $rootScope.images_dir;
            vm.motifs = Object.values(MOTIF_DEMANDE_ORGANISATION);
            vm.selected_motif = "";
            vm.motif_msg = "";

            //#####################################//
            // Fonctions du scope
            //#####################################//

            /**
             * Ferme la modale.
             */
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            /**
             * Soumet le formulaire.
             */
            vm.submit = function (msg, valid) {
                if (valid) {// si formulaire valide
                    blockUI.start();

                    var comment = {
                        message: "Complément : " + vm.demande_complement_msg,
                        contextId: idDemande
                    };

                    DemandesOrganisationService.demandeComplement(idDemande, vm.demande_complement_msg).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Complément demande d'organisation")
                                .textContent("Un complément d'information a été demandé.")
                                .ariaLabel("Complément demande d'organisation")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        ).then(function () {
                            CommentService.addDemandeComment(comment);
                            $state.go('demandes');
                        });
                    }).finally(function () {
                        blockUI.stop();
                    });
                }
            };


        } ]) 
    .factory('DetailDemandesScope', ['$rootScope', function ($rootScope) {
        var mem = {};

        return {
            store: function (key, value) {
                $rootScope.$emit('scope.stored', key);
                mem[key] = value;
            },
            get: function (key) {
                return mem[key];
            }
        }
    }]);