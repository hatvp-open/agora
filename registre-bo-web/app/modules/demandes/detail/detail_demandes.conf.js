/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.demandes')

    .config(['$stateProvider', 'EnvConf', 'HTML', 'USER_BO_ROLES',
        function ($stateProvider, EnvConf, HTML, USER_BO_ROLES) {
            $stateProvider.state('detail_demandes', {
                url: '/demandes/detail/:id',
                views: {
                    "": {
                        templateUrl: EnvConf.views_dir + HTML.DETAIL_DEMANDES,
                        controller: 'DetailDemandesCtrl'
                    },
                    "menubar": {
                        templateUrl: EnvConf.views_dir + HTML.MENUBAR_DETAILS,
                        controller: 'DetailsMenubarCtrl'
                    }
                },
                params: {
                    id: null,
                    traitee: null,
                    saveSearch : null
                },
                data: {
                    requireLogin: true,
                    returnState: 'demandes',
                    menuBarPreviousTitle: "Détails demande d'ajout d'organisation",
                    menuBarActionsTemplateUrl: EnvConf.views_dir + HTML.MENUBAR_DEMANDES,
                    isDetailDemandes: true,
                    roles: [USER_BO_ROLES.GESTION_REFERENTIEL_ORGANISATIONS, USER_BO_ROLES.MANAGER]
                }
            });
        }]);