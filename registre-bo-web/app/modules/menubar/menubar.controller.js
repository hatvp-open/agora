/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.menubar', [])

    .controller('DetailsMenubarCtrl', ['$scope', '$state',
        function ($scope, $state) {
            var vm = $scope;

            vm.returnState = $state.current.data.returnState;
            vm.menuBarPreviousTitle = $state.current.data.menuBarPreviousTitle;
            vm.menuBarActionsTemplateUrl = $state.current.data.menuBarActionsTemplateUrl;

            vm.isDeclarantSection = $state.current.data.isDeclarantSection;
            vm.isDetailValidation = $state.current.data.isDetailValidation;

        }]);