/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.declarant')

/**
 * Controlleur de la page contenat la liste des déclarants.
 */
    .controller('DeclarantCtrl', ['blockUI', '$q', '$scope', '$state', '$stateParams', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'USER_BO_ROLES', 'DeclarantsService',
        function (blockUI, $q, $scope, $state, $stateParams, $log, $mdDialog, $mdToast, EnvConf, HTML, TABLE_OPTIONS, USER_BO_ROLES, DeclarantsService) {

            /***********************************************************
             * Initialisation des variables
             ***********************************************************/
            var vm = $scope;
            vm.declarants = [];
            vm.declarantsTableOptions = angular.copy(TABLE_OPTIONS);

            vm.search = false;
            vm.typeDeRecherche = "";

            /***********************************************************
             * Déclaration des fonctions.
             ***********************************************************/

            var initDeclarantsTable = function () {
                // reinit des variables de recherche
                vm.search = false;
                vm.typeDeRecherche = "";
                vm.nom = "";
                vm.email = "";
                vm.prenom = "";
                vm.organisations = "";

                blockUI.start();
                $q.all([
                    vm.declarantsTableOptions.promise = DeclarantsService.getPaginatedDeclarants(vm.declarantsTableOptions.page, vm.declarantsTableOptions.limit),
                    vm.declarantsTableOptions.promise.then(function (res) {

                        vm.declarants = res.data.dtos.map(function (e) {
                            return {
                                id: e.id,
                                version: e.version,
                                civilite: e.civilite,
                                nom: e.nom,
                                prenom: e.prenom,
                                email: e.email,
                                organisations: e.listeOrganisationClient.join(","),
                                actif: e.activated,
                                emails_complement: e.emails_complement,
                                telephones: e.telephones,
                                lastConnexionDate: e.lastConnexionDate
                            };
                        });
                        vm.declarants.totalNumber = res.data.resultTotalNumber;
                    })
                ]).then(function () {
                }).finally(function () {
                    blockUI.stop();
                });
            };

            var searchTable = function (type, page, limit) {
                vm.typeDeRecherche = type;

                switch (type) {
                    case 'nom':
                        if (vm.nom === "") {
                            return;
                        }
                        vm.search = true;
                        vm.declarantsTableOptions.promise = DeclarantsService.getPaginatedDeclarantsParRecherche(type, vm.nom, page, limit);
                        break;
                    case 'prenom':
                        if (vm.prenom === "") {
                            return;
                        }
                        vm.search = true;
                        vm.declarantsTableOptions.promise = DeclarantsService.getPaginatedDeclarantsParRecherche(type, vm.prenom, page, limit);
                        break;
                    case 'email':
                        if (vm.email === "") {
                            return;
                        }
                        vm.search = true;
                        vm.declarantsTableOptions.promise = DeclarantsService.getPaginatedDeclarantsParRecherche(type, vm.email, page, limit);
                        break;
                    case 'organisations':
                        if (vm.organisations === "") {
                            return;
                        }
                        vm.search = true;
                        vm.declarantsTableOptions.promise = DeclarantsService.getPaginatedDeclarantsParRecherche(type, vm.organisations, page, limit);
                        break;
                    default:
                        vm.declarantsTableOptions.promise = DeclarantsService.getPaginatedDeclarants(page, limit);
                }

                vm.declarantsTableOptions.promise.then(function (res) {

                    vm.declarants = res.data.dtos.map(function (e) {
                        return {
                            id: e.id,
                            version: e.version,
                            civilite: e.civilite,
                            nom: e.nom,
                            prenom: e.prenom,
                            email: e.email,
                            organisations: e.listeOrganisationClient.join(","),
                            actif: e.activated,
                            emails_complement: e.emails_complement,
                            telephones: e.telephones,
                            lastConnexionDate: e.lastConnexionDate
                        };
                    });
                    vm.declarants.totalNumber = res.data.resultTotalNumber;

                    vm.saveSearch = {
                        search : vm.search,
                        typeDeRecherche : vm.typeDeRecherche,
                        nom : vm.nom,
                        email : vm.email,
                        prenom: vm.prenom,
                        organisations : vm.organisations
                    };
                });

                blockUI.start();
                $q.all([
                    vm.declarantsTableOptions.promise
                ]).finally(function () {
                    blockUI.stop();
                });
            };

            /**
             * Appel paginé pour la liste des declarants
             * @param typeDeRecherche
             * @param page
             * @param limit
             * @returns {*}
             */
            vm.onPaginateSearch = function (page, limit) {
                searchTable(vm.typeDeRecherche, page, limit);
            };

            /**
             * Reset du tableau sans recherches
             */
            vm.reset = function () {
                initDeclarantsTable();
            };

            vm.callSearch = function (typeDeRecherche) {
                vm.typeDeRecherche = typeDeRecherche;
                searchTable(typeDeRecherche, 1, vm.declarantsTableOptions.limit);
            };

            //Go to declarant details page.
            vm.declarantDetails = function (event, declarantContent, espaceDorigine, admin) {
                $state.go('declarantDetails', {
                    declarant: declarantContent,
                    espaceDorigine: espaceDorigine,
                    returnState: 'espaceDetails',
                    menuBarPreviousTitle: 'Retour à l\'espace collaboratif',
                });
            };

            vm.onPaginate = function (page, limit) {
                initDeclarantsTable();
            };

            /***********************************************************
             * Initialisation du controller
             ***********************************************************/
            if($stateParams.saveSearch !== null && $stateParams.saveSearch.search) {
                vm.search = $stateParams.saveSearch.search;
                vm.typeDeRecherche = $stateParams.saveSearch.typeDeRecherche;
                vm.nom = $stateParams.saveSearch.nom;
                vm.email = $stateParams.saveSearch.email;
                vm.prenom = $stateParams.saveSearch.prenom;
                vm.organisations = $stateParams.saveSearch.organisations;
                searchTable(vm.typeDeRecherche, 1, vm.declarantsTableOptions.limit);
            } else {
                initDeclarantsTable();
            }
        }])

    /**
     * Controlleur de la page des informations du déclarant.
     */
    .controller('EditDeclarantCtrl', ['$rootScope', 'blockUI', '$q', '$stateParams', '$filter', '$scope', '$state', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'USER_BO_ROLES', 'EditDeclarantScope', 'DeclarantsService', 'API', 'EspaceService',
        function ($rootScope, blockUI, $q, $stateParams, $filter, $scope, $state, $log, $mdDialog, $mdToast, EnvConf, HTML, TABLE_OPTIONS, USER_BO_ROLES, EditDeclarantScope, DeclarantsService, API, EspaceService) {

            /***********************************************************
             * Initialisation des variables
             ***********************************************************/

            var vm = $scope;

            EditDeclarantScope.store('EditDeclarantCtrl', $scope);

            vm.pieces = [];
            /**
             * URL pour le service de téléchargement des pièces.
             */
            vm.pieceDownloadUrl = EnvConf.api + API.declarant.root + API.declarant.pieces.root + API.declarant.pieces.download;

            if ($stateParams.declarant !== null && $stateParams.returnState !== null) {
                blockUI.stop();
                vm.declarant = $stateParams.declarant;

                vm.returnState = $stateParams.returnState;
                vm.menuBarPreviousTitle = $stateParams.menuBarPreviousTitle;
                $scope.espace = $stateParams.espaceDorigine;

                vm.user = {
                    id: vm.declarant.id,
                    version: vm.declarant.version,
                    civilite: "",
                    nom: "",
                    prenom: "",
                    date_naissance: null,
                    email: "",
                    emails_complement: [],
                    telephones: [],
                    password: ""
                };
            } else {
                blockUI.stop();
                $state.go("declarant");
            }

            var userTemp = {};

            vm.views = {
                COLLABORATION_SPACES: EnvConf.views_dir + HTML.DECLARANTS.CORPORATE_LIST,
                DECLARANT_PIECES: EnvConf.views_dir + HTML.DECLARANTS.PIECES_LIST
            };

            vm.e_collaboratifs = [];

            vm.e_collaboratifsTableOptions = angular.copy(TABLE_OPTIONS);


            /***********************************************************
             * Déclaration des fonctions.
             ***********************************************************/
            var initData = function () {
                blockUI.start();
                $q.all([
                    DeclarantsService.getDeclarantById(vm.user.id).then(function (res) {
                        vm.user = res.data;
                        vm.addEmail();
                        vm.addTel();
                        userTemp = angular.copy(vm.user);
                    }),
                    DeclarantsService.getPieces(vm.user.id).then(function (res) {
                        vm.pieces = res.data;
                    })
                ]).then(function () {
                }).finally(function () {
                    blockUI.stop();
                });
            };

            /** Informations de contact du déclarant.*/
            vm.addEmail = function () {
                vm.user.emails_complement.push({email: '', categorie: ''});
            };

            vm.remEmail = function (i) {
                vm.user.emails_complement.splice(i, 1);
            };

            vm.addTel = function () {
                vm.user.telephones.push({telephone: '', categorie: ''});
            };

            vm.remTel = function (i) {
                vm.user.telephones.splice(i, 1);
            };

            /** Generate alert modal. */
            var generateAlertDialog = function (event, title, message, label) {
                return $mdDialog.alert()
                    .title(title)
                    .textContent(message)
                    .ariaLabel(label)
                    .targetEvent(event)
                    .ok("OK")
                    .clickOutsideToClose(true);
            };

            /** Ensemble des espaces collaboratifs. */
            var initEspaceCollaboratifTable = function () {
                vm.e_collaboratifsTableOptions.promise = DeclarantsService.getAllEspacesCollaboratifs(vm.user.id);
                vm.e_collaboratifsTableOptions.promise.then(function (res) {
                    vm.e_collaboratifs = res.data.map(function (e) {
                        return {
                            id: e.espaceOrganisationId,
                            version: e.version,
                            denomination: e.organisationDenomination,
                            nomUsage: e.nomUsage,
                            verrouille: e.verrouille,
                            actif: e.espaceOrganisationActif,
                            favori: e.favori,
                            dateDemande: e.dateDemande,
                            dateValidation: e.dateValidation,
                            identifiant: e.organisationIdentifiantNational,
                            organisationId: e.organizationId,
                            administrateurs: e.administrateurs
                                .map(function (e) {
                                    return e.civilite + " " + e.nom + " " + e.prenom;
                                }).join(", "),
                            // nbre_declarant: e.nbrDeclarant,
                            dateCreation: e.espaceOrganisationDateCreation,
                            finExerciceFiscal: e.finExerciceFiscal,
                            mandat: EnvConf.api + e.espaceOrganisationMandat,
                            admins: e.administrateurs
                                .map(function (e) {
                                    var d = {};
                                    d.identite = e.civilite + " " + e.nom + " " + e.prenom;
                                    d.id = e.id;
                                    return d;
                                }),
                            data: {
                                id: e.espaceOrganisationId,
                                nomUsage: e.nomUsage,
                                finExerciceFiscal: e.finExerciceFiscal,
                                jourFin: e.finExerciceFiscal === undefined ? undefined : e.finExerciceFiscal.split("-")[0] < 10 ? e.finExerciceFiscal.split("-")[0].substring(1) : e.finExerciceFiscal.split("-")[0],
                                moisFin: e.finExerciceFiscal === undefined ? undefined : e.finExerciceFiscal.split("-")[1] < 10 ? e.finExerciceFiscal.split("-")[1].substring(1) : e.finExerciceFiscal.split("-")[1]
                            }

                        };
                    });
                })
            };

            /**
             * Supprimer les données de contact vide.
             */
            function filterEmails(element) {
                return ((element.categorie !== undefined && element.categorie.length != 0) && (element.email !== undefined && element.email.length != 0));
            }

            function filterPhones(element) {
                return ((element.categorie !== undefined && element.categorie.length != 0) && (element.telephone !== undefined && element.telephone.length != 0));
            }


            /** Modifier Email. */
            vm.editEmail = function (ev) {
                var confirm = $mdDialog.prompt()
                    .title('Modifier adresse email principale:')
                    .textContent('Veuillez saisir la nouvelle adresse email principale:')
                    .placeholder('Email')
                    .ariaLabel('Email principale')
                    .initialValue(vm.user.email)
                    .targetEvent(ev)
                    .ok('Modifier')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function (result) {
                    vm.user.email = result;
                    DeclarantsService.updateEmail(vm.user).then(function (response) {
                        $mdDialog.hide();
                        $mdDialog.show(
                            generateAlertDialog(ev, "Confirmation Email", "Un email de validation vient d'être envoyé à l'adresse " + result, "Confirmation Email")
                        );
                        userTemp = angular.copy(vm.user);
                    });
                });
            };

            /** Valider nouvel Email. */
            vm.validateEmail = function (ev) {
                var confirm = $mdDialog.confirm()
                    .title('Valider la nouvelle adresse email')
                    .textContent('Veuillez confirmer la validation de la nouvelle adresse: ' + vm.user.emailTemp)
                    .targetEvent(ev)
                    .ok('confirmer')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function (result) {
                    DeclarantsService.validateEmail(vm.user).then(function (response) {
                        $mdDialog.hide();
                        $mdDialog.show(
                            generateAlertDialog(ev, "Confirmation du nouvel Email", "Nouvel eMail validé: " + vm.user.emailTemp, "Confirmation Email")
                        );
                        userTemp = angular.copy(vm.user);
                        vm.user.email = vm.user.emailTemp;
                        vm.user.emailTemp = null;
                    });
                });
            };

            /** Modifier Données déclarant.*/
            vm.editDeclarant = function (ev, dataFormValid) {
                if (vm.user.date_naissance.length == 8) {
                    var day = vm.user.date_naissance.slice(0, 2);
                    var month = vm.user.date_naissance.slice(2, 4);
                    var year = vm.user.date_naissance.slice(4, 9);
                    vm.user.date_naissance = day + "/" + month + "/" + year;
                }
                vm.dataToSend = angular.copy(vm.user);
                //filtrer l'objet à enregistrer.
                vm.dataToSend.emails_complement = vm.dataToSend.emails_complement.filter(filterEmails);
                vm.dataToSend.telephones = vm.dataToSend.telephones.filter(filterPhones);
                //filrer l'objet de référence.
                userTemp.emails_complement = userTemp.emails_complement.filter(filterEmails);
                userTemp.telephones = userTemp.telephones.filter(filterPhones);

                //si les deux objets sont identiques => aucune modification détecté
                if (angular.equals(userTemp, vm.dataToSend)) {
                    dataFormValid = false;
                }

                //Si les données saisies sont correctes, envoyer le formualaire.
                if (dataFormValid) {
                    DeclarantsService.updateAccount(vm.dataToSend).then(function (res) {
                        vm.user.version = res.data.version;
                        $mdDialog.show(
                            generateAlertDialog(ev, "Modification de compte", "Le compte de [" + vm.user.nom + " " + vm.user.prenom + "] a été modifié avec succès", "Modification de compte")
                        );
                        userTemp = angular.copy(vm.user);
                    });
                }
            };

            /** Détails de l'espace collaboratif. */
            vm.corporateDetails = function (event, espaceContent) {
                if ($rootScope.isGestionnaireEspaceCollaboratifValides()) {
                    $q.all([
                        EspaceService.getOneEspace(espaceContent.id).then(function (res) {
                            vm.espaceContent = res.data.espaceOrganisation;
                            vm.espace = {
                                id: res.data.espaceOrganisation.id,
                                version: res.data.espaceOrganisation.version,
                                actif: res.data.espaceOrganisation.espaceActif,
                                publication: res.data.espaceOrganisation.publication,
                                denomination: res.data.organisationData.denomination,
                                nomUsage: res.data.espaceOrganisation.nomUsage,
                                nomUsageHatvp: res.data.espaceOrganisation.nomUsageHatvp,
                                sigleHatvp: res.data.espaceOrganisation.sigleHatvp,
                                ancienNomHatvp: res.data.espaceOrganisation.ancienNomHatvp,
                                finExerciceFiscal: res.data.espaceOrganisation.finExerciceFiscal,
                                identifiant: res.data.organisationData.nationalId,
                                organisationId: res.data.organisationData.id,
                                relancesBloquees: res.data.espaceOrganisation.relancesBloquees,
                                administrateurs: res.data.espaceOrganisation.administrateurs
                                    .map(function (e) {
                                        return e.civilite + " " + e.nom + " " + e.prenom;
                                    }).join(", "),
                                nbre_declarant: res.data.espaceOrganisation.nbrDeclarant,
                                date_creation: res.data.espaceOrganisation.creationDate,
                                representantLegal: res.data.espaceOrganisation.representantLegal,
                                validationInscriptionId: res.data.validationInscriptionId,
                                admins: res.data.espaceOrganisation.administrateurs
                                    .map(function (e) {
                                        var d = {};
                                        d.identite = e.civilite + " " + e.nom + " " + e.prenom;
                                        d.id = e.id;
                                        return d;
                                    }),
                                data: {
                                    id: res.data.espaceOrganisation.id,
                                    nomUsage: res.data.espaceOrganisation.nomUsage,
                                    nomUsageHatvp: res.data.espaceOrganisation.nomUsageHatvp,
                                    sigleHatvp: res.data.espaceOrganisation.sigleHatvp,
                                    ancienNomHatvp: res.data.espaceOrganisation.ancienNomHatvp,
                                    finExerciceFiscal: res.data.espaceOrganisation.finExerciceFiscal,
                                    jourFin: res.data.espaceOrganisation.finExerciceFiscal === undefined ? undefined : res.data.espaceOrganisation.finExerciceFiscal.split("-")[0] < 10 ? res.data.espaceOrganisation.finExerciceFiscal.split("-")[0].substring(1) : res.data.espaceOrganisation.finExerciceFiscal.split("-")[0],
                                    moisFin: res.data.espaceOrganisation.finExerciceFiscal === undefined ? undefined : res.data.espaceOrganisation.finExerciceFiscal.split("-")[1] < 10 ? res.data.espaceOrganisation.finExerciceFiscal.split("-")[1].substring(1) : res.data.espaceOrganisation.finExerciceFiscal.split("-")[1]
                                },
                                desinscriptionAgent: res.data.espaceOrganisation.desinscription_agent,
                                dateDesinscription: res.data.espaceOrganisation.dateDesinscription
                            }
                        })                              
                    ]).finally(function () {
                        $state.go('espaceDetails', {
                            cardOrga: {active: false},
                            espace: vm.espace,
                            menuBarPreviousTitle: 'Liste des déclarants',
                            returnState: 'declarant'
                        });
                    })
    
                }
                return false;
            };

            /***********************************************************
             * Initialisation du controller
             ***********************************************************/
            if (angular.isDefined(vm.user.id)) {
                initData();
                initEspaceCollaboratifTable();
            }
        }])

    /**
     * Controlleur pour modale de modification d'un Déclarant
     */
    .controller('DeclarantEditCtrl', ['$scope', '$stateParams', '$state', '$rootScope', 'item', '$mdDialog', '$mdToast', 'DeclarantsService', 'blockUI', 'EspaceService',
        function ($scope, $stateParams, $state, $rootScope, item, $mdDialog, $mdToast, DeclarantsService, blockUI, EspaceService) {
            var vm = $scope;

            /***********************************************************
             * Initialisation du scope.
             ***********************************************************/
            // déclarant info
            vm.item = angular.copy(item);
            // images path
            vm.images_dir = $rootScope.images_dir;
            // espace organisation en cours
            vm.espace = $stateParams.espace;

            /***********************************************************
             * Déclaration des fonctions dans le scope.
             ***********************************************************/
            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            /**
             * Appel service validation des données de modification
             */
            vm.submit = function (declarant, dirtyForm, ev) {
                if (dirtyForm) {
                    // fonction de mise à jour des droits
                    var update = function () {
                        blockUI.start();
                        EspaceService.setAccessRoles(declarant.declarantInscriptionId,
                            declarant.currentOrgaId,
                            declarant.administrator,
                            declarant.publisher,
                            declarant.contributor,
                            declarant.locked)
                            .then(function (res) {
                                $mdDialog.hide();
                                $mdToast.show(
                                    $mdToast.simple()
                                        .theme('info')
                                        .textContent("Les droits ont été mis à jour pour " + declarant.nom + ".")
                                        .action('Fermer')
                                        .position('top right')
                                        .hideDelay(8000)
                                );
                            })
                            .finally(function () {
                                blockUI.stop();
                                $state.reload();
                            });
                    };

                    update();
                }
                else
                    $mdDialog.hide();
            };

        }])
    .controller('DeclarantMenuBarCtrl', ['$rootScope', 'blockUI', '$q', '$stateParams', '$filter', '$scope', '$state', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'USER_BO_ROLES', 'EditDeclarantScope', 'DeclarantsService',
        function ($rootScope, blockUI, $q, $stateParams, $filter, $scope, $state, $log, $mdDialog, $mdToast, EnvConf, HTML, TABLE_OPTIONS, USER_BO_ROLES, EditDeclarantScope, DeclarantsService) {

            var vm = $scope;            

            vm.actif = EditDeclarantScope.get('EditDeclarantCtrl').declarant.actif;

            vm.returnState = EditDeclarantScope.get('EditDeclarantCtrl').returnState;
            vm.menuBarPreviousTitle = EditDeclarantScope.get('EditDeclarantCtrl').menuBarPreviousTitle;
            vm.espaceDorigine = $stateParams.espaceDorigine;

            if($stateParams.saveSearch !== null) {
                //redirection vers la liste déclarant filtrée
                vm.saveSearch = $stateParams.saveSearch;
                $state.get('declarantDetails').data.returnState = 'declarant({saveSearch: ' + JSON.stringify(vm.saveSearch) + '})';
                vm.returnState = 'declarant({saveSearch: ' + JSON.stringify(vm.saveSearch) + '})';
            } else if(vm.espaceDorigine != null){
                //redirection vers l'EC d'origine
                vm.saveSearch = $stateParams.saveSearch;
                vm.returnState = 'espaces({espaceId: ' + JSON.stringify(vm.espaceDorigine.id) + '})';
                $rootScope.espaceDorigine = vm.espaceDorigine;
            }else {
                //redircetion vers les liste de tous les déclarants
                $state.get('declarantDetails').data.returnState = 'declarant';
                vm.returnState = $state.get('declarantDetails').data.returnState;
            }

            /**
             * Activer Désactiver un compte déclarant.
             */
            vm.swicthDeclarantAccountState = function (event) {


                DeclarantsService.switchDeclarantAccountStatus(EditDeclarantScope.get('EditDeclarantCtrl').declarant.id).then(function (res) {
                    var nomAafficher = EditDeclarantScope.get('EditDeclarantCtrl').declarant.identite ? EditDeclarantScope.get('EditDeclarantCtrl').declarant.identite : (EditDeclarantScope.get('EditDeclarantCtrl').declarant.nom + " " + EditDeclarantScope.get('EditDeclarantCtrl').declarant.prenom );
                    var confirmMessage = generateAlertDialog(event, EditDeclarantScope.get('EditDeclarantCtrl').declarant.actif ? 'Inactivation de compte' : 'Activation de compte', "La statut d’activation du compte de [" + nomAafficher + "]  a été modifié", "Inactivation de compte");
                    $mdDialog.show(confirmMessage).then(function () {
                        vm.actif ? vm.actif = false : vm.actif = true;
                        EditDeclarantScope.get('EditDeclarantCtrl').declarant.actif = vm.actif;
                        localStorage.removeItem("declarant");
                        localStorage.setItem("declarant", JSON.stringify(vm.declarant));
                    });
                });
            };

            /** Generate alert modal. */
            var generateAlertDialog = function (event, title, message, label) {
                return $mdDialog.alert()
                    .title(title)
                    .textContent(message)
                    .ariaLabel(label)
                    .targetEvent(event)
                    .ok("OK")
                    .clickOutsideToClose(true);
            };

        }])
    .factory('EditDeclarantScope', ['$rootScope', function ($rootScope) {
        var mem = {};

        return {
            store: function (key, value) {
                $rootScope.$emit('scope.stored', key);
                mem[key] = value;
            },
            get: function (key) {
                return mem[key];
            }
        }
    }]);