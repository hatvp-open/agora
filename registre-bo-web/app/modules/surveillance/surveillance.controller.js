/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.surveillance')
    .controller('SurveillanceCtrl', ['blockUI', '$q', '$scope', '$state', '$stateParams', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'USER_BO_ROLES', 'MANDAT_GESTION_TYPE', 'ETAT_ESPACE_ORGANISATION', 'SurveillanceService', 'UsersService', 'EspaceService', 'EspacesService','GroupService',
        function (blockUI, $q, $scope, $state, $stateParams, $log, $mdDialog, $mdToast, EnvConf, HTML, TABLE_OPTIONS, USER_BO_ROLES, MANDAT_GESTION_TYPE, ETAT_ESPACE_ORGANISATION, SurveillanceService, UsersService, EspaceService, EspacesService,GroupService) {
            var vm = $scope;

            vm.surveillanceTableOptions = angular.copy(TABLE_OPTIONS);
            vm.surveillanceTableOptions.limit = 10;
            vm.statuts = [];

            vm.statutRecherche = "";

            vm.statutsSearch = [];

            vm.initTable = function () {

                vm.search = false;
                vm.typeDeRecherche = "";
                vm.denominationSearch = "";
                vm.nomUsageSearch = "";
                vm.statutSearch = "";
                vm.typeRecherche = "";
                vm.dateSurveillanceSearch = "";
                vm.motifSearch="";
                vm.statut = "";
                blockUI.start();
                vm.surveillanceTableOptions.promise = SurveillanceService.getPaginated(vm.surveillanceTableOptions.page, vm.surveillanceTableOptions.limit);
                vm.surveillanceTableOptions.promise.then(function (res) {

                    vm.surveillances = res.data.dtos.map(function (s) {
                        return {
                            id: s.id,
                            espaceId: s.espaceId,
                            organisationId: s.organisationId,
                            denomination: s.denomination,
                            nomUsage: s.nomUsage,
                            surveillants:s.surveillants,
                            surveillantsGroupes: s.surveillantsGroupes,
                            typeSurveillance: s.typeSurveillance,
                            statut: s.statut ? s.statut.label : "ORGANISATION SANS ESPACE",
                            dateSurveillance: s.dateSurveillance,
                            motif: s.motif
                        };
                    });
                    vm.surveillances.totalNumber = res.data.resultTotalNumber;
                }).finally(function () {
                    blockUI.stop();
                });
                GroupService.getAllGroupAndUsers().then(function(res){
                	vm.groups = res.data;
                });
                UsersService.getAllBackOfficeUsers().then(function (res) {
                    vm.users = res.data;
                });

                EspaceService.getStatutEnum().then(function (res) {
                    vm.statuts = res.data.surveillanceOrganisationEnumList;
                });

                EspacesService.getStatutType().then(function (res) {
                    vm.statutsSearch = res.data.statutEspaceEnumList;
                });
            };

            vm.searchTable = function (type, page, limit) {
                vm.typeDeRecherche = type;

                switch (type) {
                    case 'denomination':
                        if (vm.denominationSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.surveillanceTableOptions.promise = SurveillanceService.getPaginatedParRecherche(type, vm.denominationSearch, page, limit);
                        break;
                    case 'nomUsage':
                        if (vm.nomUsageSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.surveillanceTableOptions.promise = SurveillanceService.getPaginatedParRecherche(type, vm.nomUsageSearch, page, limit);
                        break;
                    case 'dateSurveillance':
                        if (vm.dateSurveillanceSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.surveillanceTableOptions.promise = SurveillanceService.getPaginatedParRecherche(type, vm.dateSurveillanceSearch, page, limit);
                        break;
                    case 'statut':
                        vm.search = true;
                        vm.surveillanceTableOptions.promise = SurveillanceService.getPaginatedParRecherche(type, vm.statutSearch, page, limit);
                        break;
                    case 'motif':
                        if(vm.motifSearch === ""){
                            return;
                        }
                        vm.search = true;
                        vm.surveillanceTableOptions.promise = SurveillanceService.getPaginatedParRecherche(type, vm.motifSearch, page, limit);
                        break;
                    default:
                        vm.surveillanceTableOptions.promise = SurveillanceService.getPaginated(page, limit);
                }

                vm.surveillanceTableOptions.promise.then(function (res) {
                    vm.surveillances = res.data.dtos.map(function (s) {
                        return {
                            id: s.id,
                            espaceId: s.espaceId,
                            organisationId: s.organisationId,
                            denomination: s.denomination,
                            nomUsage: s.nomUsage,
                            surveillants:s.surveillants,
                            surveillantsGroupes: s.surveillantsGroupes,
                            typeSurveillance: s.typeSurveillance,
                            statut: s.statut ? s.statut.label : "ORGANISATION SANS ESPACE",
                            dateSurveillance: s.dateSurveillance,
                            motif: s.motif
                        };
                    });
                    vm.surveillances.totalNumber = res.data.resultTotalNumber;

                    vm.saveSearch = {
                        search: vm.search,
                        typeDeRecherche: vm.typeDeRecherche,
                        denominationSearch: vm.denominationSearch,
                        nomUsageSearch: vm.nomUsageSearch,
                        dateSurveillanceSearch: vm.dateSurveillanceSearch,
                        statutSearch: vm.statutSearch,
                        motifSearch: vm.motifSearch
                    };
                });
            };

            vm.onPaginate = function () {
                vm.initTable();
            };

            vm.onPaginateSearch = function (page, limit) {
                vm.searchTable(vm.typeDeRecherche, page, limit);
            };

            vm.reset = function () {
                vm.initTable();
            };

            vm.callSearch = function (typeDeRecherche) {
                vm.typeDeRecherche = typeDeRecherche;
                vm.searchTable(typeDeRecherche, 1, vm.surveillanceTableOptions.limit);
            };

            vm.statutChanged = function (statut) {
                vm.statutSearch = statut;
                vm.searchTable('statut', 1, vm.surveillanceTableOptions.limit);
            };

            vm.openUpdateSurveillance = function (ev, surveillance) {
                vm.surveillance = surveillance;
                vm.surveillance.boolUserOrGroup = true;

                $mdDialog.show({
                    controller: function () {
                        this.parent = $scope;
                    },
                    controllerAs: 'ctrl',
                    scope: $scope,
                    preserveScope: true,
                    templateUrl: EnvConf.views_dir + "/surveillance/update_surveillance.html",
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            };

            vm.removeSurveillance = function (ev, surveillance) {
                surveillance.typeSurveillance.length = 0;
                surveillance.surveillants.length = 0;
                surveillance.surveillantsGroupes.length = 0;

                blockUI.start();
                SurveillanceService.deleteSurveillance(surveillance.id).then(function (res) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Mise sous surveillance annulée")
                            .textContent("Mise sous surveillance annulée")
                            .ariaLabel("Mise sous surveillance annulée")
                            .ok('OK')
                            .clickOutsideToClose(true));
                }).finally(function () {
                    if (vm.search) {
                        vm.searchTable(vm.typeDeRecherche, 1, vm.surveillanceTableOptions.limit)
                    } else {
                        vm.initTable();
                    }
                    blockUI.stop();
                });
            };

            vm.validate = function () {
                blockUI.start();
                SurveillanceService.saveSurveillance(vm.surveillance).then(function (res) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Mise sous surveillance effectuée")
                            .textContent("Mise sous surveillance effectuée")
                            .ariaLabel("Mise sous surveillance effectuée")
                            .ok('OK')
                            .clickOutsideToClose(true));
                }).finally(function () {
                    blockUI.stop();
                });
            };
            vm.updateSurveillance = function () {
                blockUI.start();
                SurveillanceService.updateSurveillance(vm.surveillance).then(function (res) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Surveillance mise à jour ")
                            .textContent("Surveillance mise à jour")
                            .ariaLabel("Surveillance mise à jour")
                            .ok('OK')
                            .clickOutsideToClose(true));
                }).finally(function () {
                    blockUI.stop();
                });
            };
            vm.putListOrganisationUnderWatch = function (ev) {
                vm.list = [];

                $mdDialog.show({
                    controller: function () {
                        this.parent = $scope;
                    },
                    controllerAs: 'ctrl',
                    scope: $scope,
                    preserveScope: true,
                    templateUrl: EnvConf.views_dir + "/surveillance/surveillance_ajout_clients.html",
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            };

            vm.submitList = function (ev) {
                SurveillanceService.verifyList(vm.list).then(function (res) {
                    vm.listeOrganisationsASurveiller = res.data.map(function(s){
                        return {
                            organisationId:s.organisationId,
                            espaceId:s.espaceId,
                            denomination:s.denomination,
                            nomUsage:s.nomUsage,
                            statut:s.statut,
                            surveillantsGroupes:s.surveillantsGroupes,
                            surveillants:s.surveillants,
                            typeSurveillance:s.typeSurveillance,
                            id:s.id
                        }
                    });
                    //vm.result = res.data;
                    $mdDialog.show({
                        controller: function () {
                            this.parent = $scope;
                        },
                        controllerAs: 'ctrl',
                        scope: $scope,
                        preserveScope: true,
                        templateUrl: EnvConf.views_dir + "/surveillance/surveillance_validation_ajouts.html",
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        fullscreen: true
                    });

                });


            };

            vm.removeOneFromList = function (element) {
                vm.result.splice(vm.result.indexOf(element), 1);
            };

            vm.saveSurveillanceList = function () {
                blockUI.start();
                var list = vm.listeOrganisationsASurveiller.filter(function (element) {
                    if (!element.error) {
                        return element;
                    }
                });
                list.forEach(function (element) {
                    element.motif = vm.motif;
                });
                SurveillanceService.saveList(list).then().finally(function () {
                    vm.initTable();
                    blockUI.stop();
                    $mdDialog.hide();
                });
            };

            vm.checkValidityList = function () {
                var list = vm.listeOrganisationsASurveiller.filter(function (element) {
                    if (!element.error) {
                        return element;
                    }
                });

                if (list.length === 0) {
                    return true;
                } else {
                    return false;
                }
            };

            vm.closeModal = function (ev) {
                vm.reset();
                $mdDialog.hide();
            };

            vm.initTable();

        }
    ]);