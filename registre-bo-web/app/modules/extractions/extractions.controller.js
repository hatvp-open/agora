/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.extractions')

    .controller('ExtractionsCtrl', ['$filter', '$rootScope', '$scope', '$state', '$stateParams', '$log', 'blockUI', '$q', '$mdDialog', '$mdToast', '$timeout', 'EnvConf', 'HTML', 'API',
        function ($filter, $rootScope, $scope, $state, $stateParams, $log, blockUI, $q, $mdDialog, $mdToast, $timeout, EnvConf, HTML, API) {
            var vm = $scope;

            /**
             * Constantes
             */
            vm.downloadExtractionUrl = EnvConf.api + API.extractions.root + API.extractions.download;

            /**
             * Formulaire saisie période de calcule des chiffres
             */
            vm.getChiffresPeriodeFormulaire = function () {                
                $state.go('extractionsPeriode', { });      

            };

        }])

        .controller('ExtractionsPeriodeCtrl', ['$filter', '$rootScope', '$scope', '$state', '$stateParams', '$log', 'blockUI', '$q', '$mdDialog', '$mdToast', '$timeout', 'EnvConf', 'HTML', 'API', 'ExtractionsService',
        function ($filter, $rootScope, $scope, $state, $stateParams, $log, blockUI, $q, $mdDialog, $mdToast, $timeout, EnvConf, HTML, API, ExtractionsService) {
            var vm = $scope;            
            vm.chiffresSurUnePeriode = {};            


            vm.getChiffresPeriode = function (chiffresSurUnePeriode) {
                vm.chiffresSurUnePeriode = chiffresSurUnePeriode;
                
                ExtractionsService.getChiffreDeLaPeriode(vm.chiffresSurUnePeriode).then(function (res) {
                    vm.chiffresSurUnePeriode.nbActiviteDeposeesRI = res.data.nbActiviteDeposeesRI;
                    vm.chiffresSurUnePeriode.nbRIDeclareActivite = res.data.nbRIDeclareActivite;
                })                
            };

        }]);    