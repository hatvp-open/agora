/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.qualite')
    .controller('QualiteCtrl', ['blockUI', '$q', '$rootScope', '$scope', '$state', '$stateParams', '$log', '$mdDialog', '$mdToast', '$filter', '$timeout', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'USER_ROLES', 'QualiteService',
        function (blockUI, $q, $rootScope, $scope, $state, $stateParams, $log, $mdDialog, $mdToast, $filter, $timeout, EnvConf, HTML, TABLE_OPTIONS, USER_ROLES, QualiteService) {

            var vm = $scope;


            vm.objets =[];
            vm.objetsTableOptions = angular.copy(TABLE_OPTIONS);
            vm.objetsTableOptions.limit = 10;
            vm.search = false;
            vm.searchType ="";
            vm.idOrganisationSearch = "";
            vm.idFicheSearch = "";
            vm.objetSearch = "";
            vm.denominationSearch = "";

            vm.initTable = function () {

                vm.search = false;
                vm.searchType ="";
                vm.idOrganisationSearch = "";
                vm.idFicheSearch = "";
                vm.objetSearch = "";
                vm.denominationSearch = "";
                vm.saveSearch = {};

                blockUI.start();


                QualiteService.getObjets(vm.objetsTableOptions.page, vm.objetsTableOptions.limit).then(function (res) {
                    vm.objets = res.data.dtos.map(function (e) {
                        return {
                            enregistrementDate: e.dateEnregistrement,
                            idOrganisation: e.identifiantNational,
                            idFiche: e.idFiche,
                            objet: e.sentence,
                            note: e.valid,
                            coefficientConfiance: e.confidence,
                            denomination: e.denomination,
                            nomUsage: e.nomUsage
                        }
                    });

                    vm.objets.totalNumber = res.data.resultTotalNumber;
                }).finally(function () {
                    blockUI.stop();
                })
            };
            vm.searchTable = function (type, page, occurrence) {
                vm.searchType = type;

                switch (type) {
                    case 'denomination':
                        if (vm.denominationSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = QualiteService.getObjetsBySearchPaginated(type, vm.denominationSearch, page, occurrence);
                        break;
                    case 'idOrganisation':
                        if (vm.idOrganisationSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = QualiteService.getObjetsBySearchPaginated(type, vm.idOrganisationSearch, page, occurrence);
                        break;

                    case 'idFiche':
                        if (vm.idFicheSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = QualiteService.getObjetsBySearchPaginated(type, vm.idFicheSearch, page, occurrence);
                        break;
                         
                    case 'objet':
                        if (vm.objetSearch === "") {
                            return;
                        }
                        vm.search = true;
                        vm.promise = QualiteService.getObjetsBySearchPaginated(type, vm.objetSearch, page, occurrence);
                        break;
                }
                vm.promise.then(function (res) {
                    vm.objets = res.data.dtos.map(function (e) {
                        return {
                            enregistrementDate: e.dateEnregistrement,
                            idOrganisation: e.identifiantNational,
                            idFiche: e.idFiche,
                            objet: e.sentence,
                            note: e.valid,
                            coefficientConfiance: e.confidence,
                            denomination: e.denomination,
                            nomUsage: e.nomUsage
                        }
                    });

                    vm.objets.totalNumber = res.data.resultTotalNumber;

                    vm.saveSearch = {
                        search: vm.search,
                        searchType: vm.searchType,
                        idOrganisationSearch: vm.idOrganisationSearch,
                        idFicheSearch: vm.idFicheSearch,
                        objetSearch: vm.objetSearch,
                        denominationSearch: vm.denominationSearch,
                        limit: vm.objetsTableOptions.limit,
                        page: vm.objetsTableOptions.page

                    };
                });
            };
            vm.onPaginate = function (page, occurrence) {
                vm.initTable();
            };

            vm.onPaginateSearch = function (page, limit) {
                vm.searchTable(vm.searchType, page, limit);
            };

            vm.callSearch = function (type) {
                vm.searchType = type;
                vm.searchTable(type, 1, vm.objetsTableOptions.limit);
            };

            vm.getDetails = function (space) {
                if (!vm.search) {
                    vm.saveSearch = {
                        search: vm.search,
                        limit: vm.objetsTableOptions.limit,
                        page: vm.objetsTableOptions.page
                    };
                }
                $state.go('espaceDetails', {
                    espace: space,
                    menuBarPreviousTitle: 'Liste des espaces collaboratifs',
                    returnState: 'espaces',
                    cardOrga: {active: false},
                    saveSearch: vm.saveSearch
                });

            };

            if ($stateParams.saveSearch !== null && $stateParams.saveSearch.search) {
                vm.search = $stateParams.saveSearch.search;
                vm.searchType = $stateParams.saveSearch.searchType;
                vm.idOrganisationSearch =  $stateParams.saveSearch.idOrganisationSearch;
                vm.idFicheSearch =  $stateParams.saveSearch.idFicheSearch;
                vm.objetSearch =  $stateParams.saveSearch.objetSearch;
                vm.denominationSearch = $stateParams.saveSearch.denominationSearch;
                vm.objetsTableOptions.limit = $stateParams.saveSearch.limit;
                vm.objetsTableOptions.page = $stateParams.saveSearch.page;

                vm.searchTable(vm.searchType, vm.objetsTableOptions.page,  vm.objetsTableOptions.limit);
            } else {
                if ($stateParams.saveSearch !== null && $stateParams.saveSearch.limit && $stateParams.saveSearch.page) {
                    vm.objetsTableOptions.limit = $stateParams.saveSearch.limit;
                    vm.objetsTableOptions.page = $stateParams.saveSearch.page;
                }
                vm.initTable();
            }
    }]);