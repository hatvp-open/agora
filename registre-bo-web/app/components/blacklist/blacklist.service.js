/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')
        .service('BlacklistService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf) {

                return {

                    getEspaceBlacklisted: function (pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + '/blacklist/blacklisted/'+pageNumber+"/"+resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    getEspaceBySearchPaginated: function (type, keyword, page, occurrence) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + '/blacklist' + "/type/" + type + "/recherche/" + keyword + "/page/" + page + "/occurrence/" + occurrence).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getAllSpaceNotPaginated: function (){
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + '/blacklist/download').then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getTypeRaisonBlacklist: function (){
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + '/blacklist/raisonBlacklist/types').then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }
                }

            }]);
})();