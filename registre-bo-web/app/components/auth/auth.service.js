/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function() {
    'use strict';

    angular.module('hatvpRegistre.service')

    .service('AuthService', ['$http', '$q', '$log', '$cookies', '$window','EnvConf', 'API',
        function ($http, $q, $log, $cookies, $window, EnvConf, API) {

        const LOGIN_URL = EnvConf.api + API.login;
        const LOGOUT_URL = EnvConf.api + API.logout;
        const INIT_URL = EnvConf.api + API.version;
        const COMPTEURS_URL = EnvConf.api + API.compteurs;
        const ACCOUNT_URL = EnvConf.api + API.user.root;

        var saveUser = function(user) {
            // Session storage
            $window.sessionStorage.setItem('user', JSON.stringify(user));

            return user;
        };

        return {
            getUser: function() {
                return JSON.parse($window.sessionStorage.getItem('user'));
            },

            refresh: function() {
                var deferred = $q.defer();

                $http.get(ACCOUNT_URL).then(function(res) {
                    saveUser(res.data);
                    deferred.resolve(res);
                }, function(err) {
                    $window.sessionStorage.removeItem('user');
                    deferred.reject(err);
                });

                return deferred.promise;
            },

            isConnected: function() {
                return !!$window.sessionStorage.getItem('user');
            },

            // Initialisation token CSRF
            init: function() {

                var deferred = $q.defer();

                $http.get(INIT_URL).then(function(res) {
                    deferred.resolve(res);
                }, function(err) {
                    deferred.reject(err);
                });

                return deferred.promise;

            },
            // Initialisation des compteurs des demandes
            setCompteurs: function() {

                var deferred = $q.defer();

                $http.get(COMPTEURS_URL).then(function(res) {
                    deferred.resolve(res);

                }, function(err) {
                    deferred.reject(err);
                });

                return deferred.promise;

            },
            authenticate: function(config) {
                var deferred = $q.defer();

                var contentTypeHeader = {'Content-Type': 'application/x-www-form-urlencoded'};
                var header = {headers: contentTypeHeader};

                $http.post(LOGIN_URL, "email=" + config.email + "" +
                    "&password=" + encodeURIComponent(config.password) +
                    "&_csrf=" + $cookies.get('X-XSRF-TOKEN'), header).then(
                    function(res) {
                    res.data = saveUser(res.data);

                    // récupération nouvelle valeur Token CSRF
                    $http.get(INIT_URL).then(function() {
                        deferred.resolve(res);
                    }, function(err) {
                        deferred.reject(err);
                    });
                }, function(response) {
                    deferred.reject(response);
                });

                return deferred.promise;
            },

            logout: function() {
                var deferred = $q.defer();

                $http.get(LOGOUT_URL).then(function(res) {
                    $window.sessionStorage.removeItem('user');
                    $log.info("Logout success");
                    deferred.resolve(res);
                }, function(err) {
                    $window.sessionStorage.removeItem('user');
                    $log.error("Logout error!");
                    deferred.reject(err);
                });

                return deferred.promise;
            }
        };

    }]);

})();