/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('UsersService', ['$http', '$q', '$log', '$cookies', 'EnvConf', 'API',
            function ($http, $q, $log, $cookies, EnvConf, API) {

                var BO_USERS_URL = EnvConf.api + API.user.root;
                var BO_GET_ALL_USERS_URL = EnvConf.api + API.user.root + API.user.all;
                var BO_ACTIVATE_ACCOUNT_URL = EnvConf.api + API.user.root + API.user.activate;
                var BO_GENERATE_NEW_USER_URL = EnvConf.api + API.user.root + API.user.password;
                var BO_NEW_NEW_USER_URL = EnvConf.api + API.user.root + API.user.add;


                /**
                 * Récupérer la liste des utilisateurs back-office.
                 * @returns {Promise}
                 */
                return {
                    /**
                     * Get all users.
                     *
                     * @returns {Promise}
                     */
                    getAllBackOfficeUsers: function () {

                        var deferred = $q.defer();

                        $http.get(BO_GET_ALL_USERS_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer une page de la liste des users backoffice
                     * @param pageNumber
                     * @param resultPerPage
                     * @returns {Promise}
                     */
                    getPaginated: function (pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(BO_GET_ALL_USERS_URL + "/page/" + pageNumber + "/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });
                        return deferred.promise;
                    },

                    /**
                     * Récupérer une page de la liste des espaces corporate non supprimés du déclarant pour une colonne contenant les caractères donnés
                     * @param keywords
                     * @param pageNumber
                     * @param resultPerPage
                     * @returns {Promise}
                     */
                    getPaginatedParRecherche: function (type, keywords, pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(BO_GET_ALL_USERS_URL  + "/" + type + "/" + keywords + "/page/" + pageNumber + "/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });
                        return deferred.promise;
                    },

                    /**
                     * Delete user account.
                     *
                     * @param userId user id to delete.
                     * @returns {Promise}
                     */
                    deleteUser: function (userId) {
                        var deferred = $q.defer();

                        $http.delete(BO_USERS_URL + "/" + userId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Add new back-office user.
                     *
                     * @param user user to add.
                     */
                    addUser: function (user) {
                        var deferred = $q.defer();

                        $http.post(BO_NEW_NEW_USER_URL, user).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    /**
                     * Update user account.
                     *
                     * @param user new user data.
                     * @returns {Promise}
                     */
                    updateUSer: function (user) {
                        var deferred = $q.defer();

                        $http.put(BO_USERS_URL, user).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    /**
                     * Activate / Deactivate user account.
                     *
                     * @param userId user id to update.
                     * @returns {Promise}
                     */
                    activateDeactivateAccount: function (userId) {
                        var deferred = $q.defer();

                        $http.put(BO_ACTIVATE_ACCOUNT_URL + "/" + userId).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    /**
                     * Generate new password for a user.
                     *
                     * @param userId user id to be updated.
                     */
                    generateNewPassword: function (userId) {
                        var deferred = $q.defer();

                        $http.put(BO_GENERATE_NEW_USER_URL + "/" + userId).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    }
                }
            }]);

})();