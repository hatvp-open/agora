/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('DeclarantsService', ['$http', '$q', '$log', '$cookies', 'EnvConf', 'API',
            function ($http, $q, $log, $cookies, EnvConf, API) {

                var BO_DECLARANT_URL = EnvConf.api + API.declarant.root;
                var BO_DECLARANT_PAGE_URL = EnvConf.api + API.declarant.root + API.declarant.paginated;
                var BO_GET_DECLARANT_URL = EnvConf.api + API.declarant.root + API.declarant.all;
                var BO_GET_COLLABORATE_SPACES_URL = EnvConf.api + API.declarant.root + API.declarant.spaces;
                var BO_UPDATE_DECLARANT_ACCOUNT_STATUS_URL = EnvConf.api + API.declarant.root + API.declarant.activate;
                var BO_UPDATE_DECLARANT_EMAIL_URL = EnvConf.api + API.declarant.root + API.declarant.email;
                var BO_UPDATE_DECLARANT_CHANGE_EMAIL_URL = EnvConf.api + API.declarant.root + API.declarant.emailChange;
                var BO_DECLARANT_PIECES_URL = EnvConf.api + API.declarant.root + API.declarant.pieces.root + API.declarant.pieces.all;


                /**
                 * Récupérer la liste des decarants .
                 * @returns {Promise}
                 */
                return {
                    /**
                     * récupérer un déclarant par id.
                     *
                     * @returns {Promise}
                     */
                    getDeclarantById: function (id) {

                        var deferred = $q.defer();

                        $http.get(BO_DECLARANT_URL + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * récupérer un déclarant par mail.
                     *
                     * @returns {Promise}
                     */
                    getDeclarantByMail: function (emailRecherche) {

                        var deferred = $q.defer();
                        $http.post(BO_DECLARANT_URL + "/mail/", emailRecherche).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * récupérer tous les déclarants.
                     *
                     * @returns {Promise}
                     */
                    getAllDeclarants: function () {

                        var deferred = $q.defer();

                        $http.get(BO_GET_DECLARANT_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * récupérer une liste paginée de tous les déclarants.
                     *
                     * @returns {Promise}
                     */
                    getPaginatedDeclarants: function (pageNumber, resultPerPage) {

                        var deferred = $q.defer();

                        $http.get(BO_DECLARANT_PAGE_URL + "/" + pageNumber + "/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * récupérer une liste paginée de tous les déclarants.
                     *
                     * @returns {Promise}
                     */
                    getPaginatedDeclarantsParRecherche: function (type, keywords, pageNumber, resultPerPage) {

                        var deferred = $q.defer();

                        $http.get(BO_DECLARANT_PAGE_URL + "/" + type + "/" + keywords + "/" + pageNumber + "/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },



                    /**
                     * Récupérer la liste de tous les espaces collaboratifs auxquels le déclarante appartient.
                     *
                     * @param declarantId id du déclarant.
                     * @returns {Promise}
                     */
                    getAllEspacesCollaboratifs: function (declarantId) {
                        var deferred = $q.defer();

                        $http.get(BO_GET_COLLABORATE_SPACES_URL + declarantId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Activer / Désactiver le compte déclarant.
                     *
                     * @param declarantId id du déclarant.
                     */
                    switchDeclarantAccountStatus: function (declarantId) {
                        var deferred = $q.defer();

                        $http.put(BO_UPDATE_DECLARANT_ACCOUNT_STATUS_URL + declarantId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Modifier l'email principale d'un déclarant.
                     *
                     * @returns {Promise}
                     * @param user
                     */
                    updateEmail: function (user) {
                        var deferred = $q.defer();

                        $http.put(BO_UPDATE_DECLARANT_EMAIL_URL, {email: user.email, id: user.id}).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    /**
                     * Valider manuellement la modification de l'email principale d'un déclarant.
                     *
                     * @returns {Promise}
                     * @param user
                     */
                    validateEmail: function (user) {
                        var deferred = $q.defer();

                        $http.put(BO_UPDATE_DECLARANT_CHANGE_EMAIL_URL, {emailTemp: user.emailTemp, id: user.id}).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    /**
                     * Mise à jours des données du déclarant.
                     *
                     * @param user données à mettre à jour.
                     * @returns {Promise}
                     */
                    updateAccount: function (user) {
                        var deferred = $q.defer();

                        $http.put(BO_DECLARANT_URL, user).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    /** Retourne la liste des pièces versées lors de la demande de création de l'espace. */
                    getPieces: function (id) {
                        var deferred = $q.defer();

                        $http.get(BO_DECLARANT_PIECES_URL + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }
                }
            }]);

})();