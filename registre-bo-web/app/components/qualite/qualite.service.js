/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')
        .service('QualiteService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf) {

                return {

                    getObjets: function (pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + '/qualif/qualified/'+pageNumber+"/"+resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    getAllObjets: function () {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + '/qualif/qualified/all').then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    getObjetsBySearchPaginated: function (type, keyword, page, occurrence) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + '/qualif/qualified' + "/type/" + type + "/recherche/" + keyword + "/page/" + page + "/occurence/" + occurrence).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    getLastQualifObjetByFicheId: function (idFiche) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + '/qualif/qualified/idFiche/'+idFiche).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }
                }

            }]);
})();