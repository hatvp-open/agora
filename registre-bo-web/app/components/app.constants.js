/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.service')

    .constant('TABLE_OPTIONS', {
        filter: {},
        labels: {page: 'Page : ', rowsPerPage: 'Résultats par page : ', of: 'de'},
        order: 'id',
        limit: 100,
        limitOptions: [10, 20, 50, 100],
        page: 1
    })

    .constant('MOTIF_REFUS_VALIDATION', {
        DEMANDE_NON_SERIEUSE: {
            label: 'Demande non sérieuse',
            value: 1
        },
        ID_CONFORME_MANDAT_NON_CONFORME: {
            label: "ID représentant conforme / Mandat non conforme",
            value: 2
        },
        PIECE_ID_NON_CONFORME: {
            label: "ID représentant conforme / Mandat conforme / Pièce d'identité du représentant non conforme",
            value: 3
        },
        ID_NON_CONFORME: {
            label: "ID Représentant non conforme",
            value: 4
        },
        AUTRE: {
            label: "Autre",
            value: 0
        }
    })

    .constant('MOTIF_DEMANDE_COMPLEMENT', {
        ID_CONFORME_MANDAT_NON_CONFORME: {
            label: "ID représentant conforme / Mandat non conforme",
            code: "ID_CONFORME_MANDAT_NON_CONFORME"
        },
        PIECE_ID_NON_CONFORME: {
            label: "ID représentant conforme / Mandat conforme / Pièce d'identité du représentant non conforme",
            code: "PIECE_ID_NON_CONFORME"
        },
        ID_NON_CONFORME: {
            label: "ID Représentant non conforme",
            code: "ID_NON_CONFORME"
        },
        AUTRE: {
            label: "Autre",
            code: "AUTRE"
        }
    })
    .constant('MOTIF_DEMANDE_COMPLEMENT_AJOUT_CHANGEMENT_CO', {
        MANDAT_NON_CONFORME: {
            type: "AJOUT_CHANGEMENT_CO",
            label: "Mandat non conforme",
            code: "ID_CONFORME_MANDAT_NON_CONFORME"
        },

        AUTRE: {
            type: "AUTRE",
            label: "Autre",
            code: "AUTRE"
        }
    })

    .constant('MOTIF_DEMANDE_COMPLEMENT_CHANGEMENT_REPRESENTANT', {
        ID_REPRESENTANT_NON_VERIFIABLE: {
            label: "ID représentant non vérifiable",
            code: "ID_NON_CONFORME"
        },
        MANDAT_NON_CONFORME_ID_CONFORME: {
            label: "ID représentant conforme / Mandat non conforme",
            code: "ID_CONFORME_MANDAT_NON_CONFORME"
        },

        ID_NON_CONFORME: {
            type: "CHANGEMENT_REPRESENTANT",
            label: "ID Représentant non conforme",
            code: "PIECE_ID_NON_CONFORME"
        },
        AUTRE: {
            type: "AUTRE",
            label: "Autre",
            code: "AUTRE"
        }
    })
    .constant('MOTIF_DEMANDE_ORGANISATION', {
        RENSEIGNEMENTS_INSUFFISANTS: {
            label: "Renseignements insuffisants / Justificatifs d’existence légale nécessaires",
            code: "RENSEIGNEMENTS_INSUFFISANTS"
        },
        AUTRE: {
            type: "AUTRE",
            label: "Autre",
            code: "AUTRE"
        }
    })
    .constant('USER_ROLES', {
        administrator: 'ADMINISTRATEUR',
        publisher: 'PUBLICATEUR',
        contributor: 'CONTRIBUTEUR'
    })

    .constant('ETAT_ESPACE_ORGANISATION', {
        NOUVEAU: "Nouveau",
        EN_TRAITEMENT: "En traitement",
        EN_ATTENTE_VALIDATION: "En attente de validation",
        COMPLEMENT_DEMANDE_ENVOYEE: "Complément demandé",
        COMPLEMENT_RECU: "Complément reçu",
        ACCEPTEE: "Acceptée",
        REFUSEE: "Refusée"
    })

    .constant('STATUT_DEMANDE_ORGANISATION', {
        A_TRAITER: "A traiter",
        TRAITEE_ORGANISATION_TROUVEE: "Traitée: Organisation trouvée",
        TRAITEE_ORGANISATION_CREEE: "Traitée: Organisation créée",
        TRAITEE_ORGANISATION_REFUSEE: "Traitée: Organisation refusée",
        COMPLEMENT_DEMANDE_ENVOYEE: "Complément demandé",
        TOUTES_LES_DEMANDES: "Status"
    })


    .constant("MANDAT_GESTION_TYPE", {
        AJOUT_OPERATIONNEL: "Ajout d'un nouveau contact opérationnel",
        REMPLACEMENT_OPERATIONNEL: "Remplacement d'un contact opérationnel",
        CHANGEMENT_REPRESENTANT: "Changement de représentant légal"
    })

    .constant("MANDAT_GESTION_STATUT", {
        NOUVEAU: "Nouveau",
        VALIDEE: "Validée",
        REFUSEE: "Refusée",
        TOUTES_LES_DEMANDES: "Status"
    })

    .constant('USER_BO_ROLES', {
        VALIDATION_ESPACES_COLLABORATIFS: 'VALIDATION_ESPACES_COLLABORATIFS',
        GESTION_MANDATS: 'GESTION_MANDATS',
        GESTION_COMPTES_DECLARANTS: 'GESTION_COMPTES_DECLARANTS',
        GESTION_CONTENUS_PUBLIES: 'GESTION_CONTENUS_PUBLIES',
        GESTION_ESPACES_COLLABORATIFS_VALIDES: 'GESTION_ESPACES_COLLABORATIFS_VALIDES',
        GESTION_REFERENTIEL_ORGANISATIONS: 'GESTION_REFERENTIEL_ORGANISATIONS',
        MODIFICATION_DES_RAISONS_SOCIALES: 'MODIFICATION_DES_RAISONS_SOCIALES',
        MODIFICATION_EMAIL_DECLARANT: 'MODIFICATION_EMAIL_DECLARANT',
        RESTRICTION_ACCES: 'RESTRICTION_ACCES',
        ADMINISTRATEUR: 'ADMINISTRATEUR',
        MANAGER: 'MANAGER',
        DATA_DOWNLOADER: 'DATA_DOWNLOADER',
        ETAT_ACTIVITES: 'ETAT_ACTIVITES'
    })

    .constant('USER_BO_ROLES_TEXT', {
        VALIDATION_ESPACES_COLLABORATIFS: 'Validation des espaces collaboratifs',
        GESTION_COMPTES_DECLARANTS: 'Gestion des comptes déclarants',
        GESTION_CONTENUS_PUBLIES: 'Gestion des contenus publiés',
        GESTION_ESPACES_COLLABORATIFS_VALIDES: 'Gestion des espaces collaboratifs',
        GESTION_REFERENTIEL_ORGANISATIONS: 'Gestion du référentiel des organisations',
        MODIFICATION_DES_RAISONS_SOCIALES: 'Modification des raisons sociales',
        GESTION_MANDATS: 'Traitement des mandats de gestion',
        MODIFICATION_EMAIL_DECLARANT: 'Modification d\'email déclarant',
        RESTRICTION_ACCES: 'Restriction d\'accès',
        ADMINISTRATEUR: 'Administrateur',
        MANAGER: 'Manager',
        DATA_DOWNLOADER: 'Data downloader',
        ETAT_ACTIVITES: 'État des activités'
    })

    .constant('API', {
        version: "/version",
        login: "/login",
        logout: "/logout",
        compteurs: "/compteurs",
        user: {
            root: "/account",
            all: "/all",
            activate: "/activate",
            password: "/password",
            add: "/add",
            email: "/email"
        },
        declarant: {
            root: "/declarant",
            all: "/all",
            paginated: "/page",
            spaces: "/espace/all/",
            activate: "/activate/",
            email: "/email",
            emailChange: "/change/email",
            pieces: {
                root: "/pieces",
                download: "/download",
                all: "/all"
            }
        },
        corporate: {
            unsubscribe: "/unsubscribe",
            root: "/corporate",
            lock: "/lock",
            pending: "/pending",
            confirmed: "/confirmed",
            declarant: "/declarant",
            client: "/client",
            collaborateur: "/collaborateur",
            affiliation: "/affiliation",
            validation: "/switch/activation/",
            update: "/update",
            hasPublished: "/hasPublished",
            demandes: {
                root: "/organisation",
                lock: "/lock",
                all: "/demandes",
                hatvp: "/hatvp",
                demande: "/demande",
                paginated: "/page",
                nontraitees: "/nontraitees"
            },
            mandats: {
                root: "/mandats",
                pending: "/nontraitees",
                organisation: "/organisation"
            },
            pieces: {
                root: "/pieces",
                complement: "/complement",
                creation: "/creation",
                mandant: "/mandant",
                download: "/download"
            },
            type_orga: "/type/organisation",
            gestion_espace: {
                root: "/gestion",
                roles: "/access",
            },
            desinscription: {
                root: "/desinscription",
                desinscription_agent: "/agent",
                desinscription_pieces: "/pieces",
                download: "/download",
                reject: "/reject",
                complement: "/complement",
                update_complement: "/update_complement",
            }
        },
        comment: {
            root: "/comment",
            declarant: "/comment/declarant",
            espace: "/comment/espace",
            validation: "/comment/validation",
            demande: "/comment/demande",
            demandeMandant: "/comment/demandeChangementMandant",
            all: "/all"
        },
        search: {
            root: "/search"
        },
        declaration: {
            root: "/rapport",
            exercice:{
            	root:"/exercice",
            	update:"/update"
            },
            activite:{
            	suppression:"/fiche/suppression"
            }
        },
        limitation: {
            root: "/limitation",
            state: {
                get:"/activation/state",
                switch:"/activation/switch",
                limite:"/activation/limite"
            }
        },
        extractions: {
            root: "/extractions",
            download: "/download",
            periode: "/extractions/download/periode"
        }
    })

    .constant('HTML', {
        HOME: {
            HOME: "/home/home.html",
            LOGIN: "/home/login/login.html",
            HOMELESS: "/home/error.html"
        },
        LOGIN: "/login/login.html",
        USERS: {
            HOME: "/utilisateurs/utilisateurs.html",
            EDIT_ACCOUNT: '/utilisateurs/edit_account.html'
        },
        MANDATES: {
            HOME: "/mandats/mandats.html",
            DETAIL: '/mandats/detail_mandat.html',
            MENU_BAR: "/mandats/menubar.html"
        },
        DECLARANTS: {
            HOME: "/declarants/declarants.html",
            MENUBAR: '/declarants/menubar_actions.html',
            CORPORATE_LIST: "/declarants/corporate_list.html",
            EDIT_DSCLARANT: '/declarants/declarant_details.html',
            PERSONAL_DATA: '/declarants/declarant_info.html',
            MENU_BAR: '/declarants/menu_bar.html',
            DECLARANT_EDIT: "/declarants/declarant_edit.dialog.html",
            PIECES_LIST: "/declarants/tabs/declarant_pieces_list.html"
        },
        ESPACE: {
            HOME: "/espace/espace_list.html",
            EDIT_ESPACE: "/espace/espace_details.html",
            ESPACE_HISTORIQUE_IDENTITE: "/espace/espace_detail_historique.html",
            TABS: {
                PIECES_LIST: "/espace/tabs/pieces_list.html",
                DECLARANT_LIST: "/espace/tabs/declarant_list.html",
                PUBLICATION_LIST: "/espace/tabs/publication_list.html",
                RAPPORT_LIST: "/espace/tabs/rapport_list.html",
                CLIENT_LIST: "/espace/tabs/client_list.html",
                COLLABORATEUR_LIST: "/espace/tabs/collaborateur_list.html",
                AFFILIATION_LIST: "/espace/tabs/affiliation_list.html",
                RELANCES_LIST: "/espace/tabs/relances_list.html"
            },
            RAPPORT_MODIF_DATES: "/espace/modif_dates_exercice.html",
            RAPPORT_HISTORIQUE: "/espace/rapport/exercice_historique.html",
            PUBLICATION: "/espace/publication/publication.html",
            FICHE: "/espace/fiche/fiche.html",
            FICHE_HISTORIQUE: "/espace/fiche/fiche_historique.html",
            CLIENT_DETAIL: "/espace/client/detail.html",
            AJOUT_CO:"/espace/ajout_co/ajout_co.html",
            MENU_BAR: "/espace/menu_bar.html"
        },
        ESPACES: {
          HOME: "/espaces/espaces.html"
        },
        SURVEILLANCE: {
            HOME: "/surveillance/surveillance.html"
        },
        BLACKLIST:{
            HOME: "/blacklist/blacklist.html"
        },
        GROUP:{
          HOME: "/group/group.html",
          EDIT_GROUP: "/group/edit_group.html"
        },
        CONTROLE_QUALITE:{
            HOME: "/qualite/qualite.html"
        },
        MENUBAR_DETAILS: "/menubar/menubar.html",
        COMPTEURS: "/compteurs/compteurs.html",
        COMMENT: "/comment/comment.html",
        COMMENT_UPDATE: "/comment/update_comment.html",
        VALIDATION: "/validation/validation.html",
        DETAIL_VALIDATION: "/validation/detail/detail_validation.html",
        MENUBAR_VALIDATION: "/validation/detail/menubar_actions.html",
        MOTIF_VALIDATION: "/validation/detail/motifs/motifs_refus_validation.html",
        DEMANDE_COMPLEMENT_VALIDATION: "/validation/detail/complement/demande_complement.html",
        DEMANDES: "/demandes/demandes.html",
        EXTRACTIONS: "/extractions/extractions.html",
        EXTRACTIONS_PERIODE: "/extractions/extractions_periode.html",
        DETAIL_DEMANDES: "/demandes/detail/detail_demandes.html",
        MENUBAR_DEMANDES: "/demandes/detail/menubar_actions.html",
        DEMANDES_DETAIL: {
            TABS: {
                LIST: "/demandes/detail/tabs/list.html",
                FORM: "/demandes/detail/tabs/form.html"
            }
        },
        DEMANDE_COMPLEMENT_MANDATS: "/mandats/complement/demande_complement.html",
        DEMANDE_COMPLEMENT_ORGANISATION: "/demandes/complement/demande_complement.html",
    });