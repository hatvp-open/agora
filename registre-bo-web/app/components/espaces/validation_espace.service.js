/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('ValidationEspaceService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf, API) {

                var CORPORATE_URL = EnvConf.api + API.corporate.root;
                var PENDING_URL = CORPORATE_URL + API.corporate.pending;

                return {

                    /**
                     * Récupérer la liste des espaces corporate non supprimés du déclarant
                     * @returns {Promise}
                     */
                    getAll: function () {
                        var deferred = $q.defer();

                        $http.get(PENDING_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer une page de la liste des espaces corporate non supprimés du déclarant
                     * @param pageNumber
                     * @param resultPerPage
                     * @returns {Promise}
                     */
                    getPaginated: function (pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(PENDING_URL + "/page/" + pageNumber + "/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });
                        return deferred.promise;
                    },

                    /**
                     * Récupérer une page de la liste des espaces corporate non supprimés du déclarant pour une colonne contenant les caractères donnés
                     * @param keywords
                     * @param pageNumber
                     * @param resultPerPage
                     * @returns {Promise}
                     */
                    getPaginatedParRecherche: function (type, keywords, pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(PENDING_URL  + "/" + type + "/" + keywords + "/page/" + pageNumber + "/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });
                        return deferred.promise;
                    },

                    /**
                     * Récupérer l'espace corporate en cours de validation
                     * @returns {Promise}
                     */
                    getById: function (id) {
                        var deferred = $q.defer();

                        $http.get(PENDING_URL + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer l'espace corporate en cours de validation
                     * @returns {Promise}
                     */
                    update: function (validation) {
                        var deferred = $q.defer();

                        $http.put(PENDING_URL, validation).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Valider demande création espace collaboratif
                     * @returns {Promise}
                     */
                    validate: function (id) {
                        var deferred = $q.defer();

                        $http.post(PENDING_URL + "/" + id + "/accept").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Valider demande création espace collaboratif
                     * @returns {Promise}
                     */
                    reject: function (id, message) {
                        var deferred = $q.defer();

                        $http.post(PENDING_URL + "/" + id + "/reject", message).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Demande de complément pour la création d'un espace collaboratif
                     * @returns {Promise}
                     */
                    demandeComplement: function (id, message) {
                        var deferred = $q.defer();

                        $http.post(PENDING_URL + "/" + id + "/complement", message).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer template motif sélectionné
                     * @returns {Promise}
                     */
                    getMotifTemplate: function (id, idMotif) {
                        var deferred = $q.defer();

                        $http.get(PENDING_URL + "/" + id + "/motif/" + idMotif).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer template courriel demande de complément
                     * @returns {Promise}
                     */
                    getDemandeComplementTemplate: function (id, codeMotif) {
                        var deferred = $q.defer();

                        $http.get(PENDING_URL + "/" + id + "/demandeComplementTemplate/" + codeMotif).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }

                }

            }]);

})();