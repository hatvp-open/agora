/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('DemandesOrganisationService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf, API) {

                var CORPORATE_URL = EnvConf.api + API.corporate.root;
                var DEMANDE_URL = CORPORATE_URL + API.corporate.demandes.root + API.corporate.demandes.demande;
                var ORGANISATIONS_HATVP_URL = CORPORATE_URL + API.corporate.demandes.root + API.corporate.demandes.hatvp;

                return {
                    /**
                     * Récupérer une liste paginée des demandes d'ajout d'organisation 
                     * @returns {Promise}
                     */
                    getPaginatedDemandes: function (status, pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(CORPORATE_URL + "/organisation/demandes/" + status + "/page/" + pageNumber + "/occurrence/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer une liste paginée des demandes d'ajout d'organisation par declarant
                     * @param keywords
                     * @param pageNumber
                     * @param resultPerPage
                     * @returns {Promise}
                     */
                    getPaginatedParRecherche: function (type, keywords, pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(CORPORATE_URL + "/organisation/demandes/" + type + "/" + keywords + "/page/" + pageNumber + "/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });
                        return deferred.promise;
                    },

                    /**
                     * Récupérer la demande d'ajout en cours d'edition
                     * @returns {Promise}
                     */
                    getById: function (id) {
                        var deferred = $q.defer();

                        $http.get(DEMANDE_URL + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer la liste des organisations HATVP créées
                     * @returns {Promise}
                     */
                    getHatvp: function () {
                        var deferred = $q.defer();

                        $http.get(ORGANISATIONS_HATVP_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Clôturer demande création organisation
                     * Organisation trouvée
                     * @returns {Promise}
                     */
                    colturerTrouvee: function (id) {
                        var deferred = $q.defer();

                        $http.put(DEMANDE_URL + "/" + id, 'TRAITEE_ORGANISATION_TROUVEE').then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Clôturer demande création organisation
                     * Organisation refusée
                     * @returns {Promise}
                     */
                    colturerRefusee: function (id) {
                        var deferred = $q.defer();

                        $http.put(DEMANDE_URL + "/" + id, 'TRAITEE_ORGANISATION_REFUSEE').then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Rouvrir demande création organisation
                     * @returns {Promise}
                     */
                    rouvrir: function (id) {
                        var deferred = $q.defer();

                        $http.put(DEMANDE_URL + "/" + id, 'A_TRAITER').then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Création nouvelle organisation
                     * Organisation refusée
                     * @returns {Promise}
                     */
                    creerOrganisation: function (id, vo) {
                        var deferred = $q.defer();

                        $http.post(DEMANDE_URL + "/" + id, vo).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    
                    /**
                     * Demande de complément 
                     * @returns {Promise}
                     */
                    demandeComplement: function (id, message) {
                        var deferred = $q.defer();

                        $http.post(DEMANDE_URL + "/complement/"+id, message).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }

                }

            }]);

})();