/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('EspaceLockService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf, API) {

                var CORPORATE_URL = EnvConf.api + API.corporate.root;
                var LOCK_URL = CORPORATE_URL + API.corporate.lock;

                return {

                    /**
                     * Vérifier s'il y a un lock sur l'espace corporate
                     * @returns {Promise}
                     */
                    getLock: function (id) {
                        var deferred = $q.defer();

                        $http.get(LOCK_URL + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Appliquer un lock sur l'espace corporate
                     * @returns {Promise}
                     */
                    setLock: function (id) {
                        var deferred = $q.defer();

                        $http.post(LOCK_URL + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * Mise à jour du lock sur l'espace corporate
                     * @returns {Promise}
                     */
                    updateLock: function (lock) {
                        var deferred = $q.defer();

                        $http.put(LOCK_URL, lock).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }

                }

            }]);

})();