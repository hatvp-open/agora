/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('DemandeLockService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf, API) {

                var DEMANDE_URL = EnvConf.api + API.corporate.root + API.corporate.demandes.root;
                var LOCK_URL = DEMANDE_URL + API.corporate.demandes.lock;

                return {

                    /**
                     * Vérifier s'il y a un lock sur la demande organisation
                     * @returns {Promise}
                     */
                    getLock: function (id) {
                        var deferred = $q.defer();

                        $http.get(LOCK_URL + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Appliquer un lock sur la demande organisation
                     * @returns {Promise}
                     */
                    setLock: function (id) {
                        var deferred = $q.defer();

                        $http.post(LOCK_URL + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * Mise à jour du lock sur la demande organisation
                     * @returns {Promise}
                     */
                    updateLock: function (lock) {
                        var deferred = $q.defer();

                        $http.put(LOCK_URL, lock).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }

                }

            }]);

})();