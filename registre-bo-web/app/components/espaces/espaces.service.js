/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')
        .service('EspacesService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf, API) {



                return {

                    getNotValidatedEspacePaginated: function (page, occurrence) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + '/espace/unvalidated/page/' + page + '/occurrence/' + occurrence).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getEspaceBySearchPaginated: function (type, keyword, page, occurrence) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + '/espace' + "/type/" + type + "/recherche/" + keyword + "/page/" + page + "/occurence/" + occurrence).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getStatutType: function () {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + '/espace/statut/types').then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }
                }

            }]);
})();