/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('DesinscriptionService', ['$http', '$q', 'EnvConf', 'API',
            function ($http, $q, EnvConf, API) {
                const BO_API = EnvConf.api;
                const DESINSCRIPTION = BO_API + API.corporate.desinscription.root;
                const DESINSCRIPTION_AGENT = DESINSCRIPTION + API.corporate.desinscription.desinscription_agent;
                const DESINSCRIPTION_PIECES = DESINSCRIPTION + API.corporate.desinscription.desinscription_pieces;
                const REJECT_DEMANDE = DESINSCRIPTION + API.corporate.desinscription.reject;
                const ASK_FOR_COMPLEMENT = DESINSCRIPTION + API.corporate.desinscription.complement;

                return {
                    saveDesinscription: function (formData) {
                        var deferred = $q.defer();

                        $http.post(DESINSCRIPTION_AGENT, formData, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        }).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getDesinscription: function (espaceId) {
                        var deferred = $q.defer();

                        $http.get(DESINSCRIPTION + "/" + espaceId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getPiecesDesinscription: function (espaceId) {
                        var deferred = $q.defer();

                        $http.get(DESINSCRIPTION_PIECES + "/" + espaceId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    rejectDemande: function (desinscriptionId) {
                        var deferred = $q.defer();

                        $http.get(REJECT_DEMANDE + "/" + desinscriptionId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    askforcomplement: function (msg, desinscriptionId, currentUserId) {
                        var deferred = $q.defer();

                        $http.post(ASK_FOR_COMPLEMENT + "/" + desinscriptionId + "/" + currentUserId, msg).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                }
            }
        ]);
})();
