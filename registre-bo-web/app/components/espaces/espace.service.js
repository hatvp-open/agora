/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('EspaceService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf, API) {

                var ESPACE_GET_URL = EnvConf.api + API.corporate.root + API.corporate.confirmed;
                var ESPACE_DECLARANT_GET_URL = EnvConf.api + API.corporate.root + API.corporate.declarant;
                var ESPACE_ACTIVATION_PUT_URL = EnvConf.api + API.corporate.root + API.corporate.validation;
                var ESPACE_PUBLICATIONS = EnvConf.api + API.corporate.root;
                var ESPACE_CLIENTS = EnvConf.api + API.corporate.root + API.corporate.client;
                var ESPACE_COLLABORATEURS = EnvConf.api + API.corporate.root + API.corporate.collaborateur;
                var ESPACE_AFFILIATIONS = EnvConf.api + API.corporate.root + API.corporate.affiliation;
                var ESPACE_PIECES_COMPLEMENT_URL = EnvConf.api + API.corporate.root + API.corporate.pieces.root + API.corporate.pieces.complement;
                var ESPACE_PIECES_CREATION_URL = EnvConf.api + API.corporate.root + API.corporate.pieces.root + API.corporate.pieces.creation;
                var TYPE_ORGA_URL = EnvConf.api + API.corporate.root + API.corporate.type_orga;
                var ESPACE_GESTION_URL = ESPACE_PUBLICATIONS + API.corporate.gestion_espace.root;
                var ROLES_URL = ESPACE_GESTION_URL + API.corporate.gestion_espace.roles;
                var CORPORATE_URL = EnvConf.api + API.corporate.root;
                var CORPORATE_UPDATE_URL = CORPORATE_URL + API.corporate.update;
                var MANDAT_ESPACE_URL = EnvConf.api + API.corporate.mandats.root + API.corporate.mandats.organisation;
                var UPDATE_EXERCICE_URL = EnvConf.api + API.declaration.root + API.declaration.exercice.update;
                var AJOUT_CO_URL = EnvConf.api + API.corporate.root + '/addCo';
                var HAS_PUBLISHED = EnvConf.api + API.corporate.root + API.corporate.hasPublished;
                var SUPPRESSION_ACTIVITE_URL = EnvConf.api + API.declaration.root + API.declaration.activite.suppression;



                return {

                    /**
                     * Récupérer une page de la liste des espaces organisations validés.
                     * @returns {Promise}
                     */
                    getValidatedEspacePaginated: function (pageNumber, resultPerPage) {
                        return this.commonGetService(ESPACE_GET_URL + "/page/" + pageNumber + "/" + resultPerPage);
                    },

                    /**
                     * Récupérer une page de la liste des espaces corporate non supprimés du déclarant pour une colonne contenant les caractères donnés
                     * @param keywords
                     * @param pageNumber
                     * @param resultPerPage
                     * @returns {Promise}
                     */
                    getValidatedEspacePaginatedParRecherche: function (type, keywords, pageNumber, resultPerPage) {
                        var deferred = $q.defer();
                        $http.get(ESPACE_GET_URL  + "/" + type + "/" + keywords + "/page/" + pageNumber + "/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });
                        return deferred.promise;
                    },

                    /**
                     * Récupérer un espace organisation.
                     * @returns {Promise}
                     */
                    getOneEspace: function (id) {
                        return this.commonGetService(ESPACE_GET_URL + "/" + id);
                    },

                    /**
                     * Récupération des déclarants de l'espace organisation.
                     *
                     * @param espaceId id de l'espace organisation.
                     * @returns {Promise}
                     */
                    getEspaceOrganisationDeclarant: function (espaceId) {
                        return this.commonGetService(ESPACE_DECLARANT_GET_URL + "/" + espaceId);
                    },

                    /**
                     * Récupération des clients en fonction de l'espace ID
                     *
                     * @param espaceId
                     * @returns {*|Promise}
                     */
                    getClients: function(espaceId){
                      return this.commonGetService(ESPACE_CLIENTS +  "/" + espaceId);
                    },

                    /**
                     * Récupération des collaborateurs en fonction de l'espace ID
                     * @param espaceId
                     * @returns {*|Promise}
                     */
                    getCollaborateurs:function(espaceId){
                        return this.commonGetService(ESPACE_COLLABORATEURS + "/" + espaceId);
                    },

                    /**
                     * Récupération des affiliations en fonction de l'espace ID
                     *
                     * @param espaceId
                     * @returns {*|Promise}
                     */
                    getAffiliations: function(espaceId){
                        return this.commonGetService(ESPACE_AFFILIATIONS +  "/" + espaceId);
                      },

                    /**
                     * Récupération des publications de l'espace organisation.
                     *
                     * @param espaceId id de l'espace organisation.
                     * @returns {Promise}
                     */
                    getPublications: function (espaceId) {
                        return this.commonGetService(ESPACE_PUBLICATIONS + "/" + espaceId + "/publications");
                    },

                    /**
                     * Modifier état des publications de l'espace organisation.
                     *
                     * @param espaceId id de l'espace organisation.
                     * @returns {Promise}
                     */
                    depublierPublicationsEspace: function (espaceId) {
                        var link = ESPACE_PUBLICATIONS + "/" + espaceId + "/publications/depublier";
                        var deferred = $q.defer();

                        $http.put(link).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Modifier état des publications de l'espace organisation.
                     *
                     * @param espaceId id de l'espace organisation.
                     * @returns {Promise}
                     */
                    republierPublicationsEspace: function (espaceId) {
                        var link = ESPACE_PUBLICATIONS + "/" + espaceId + "/publications/republier";
                        var deferred = $q.defer();

                        $http.put(link).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Modifier état publication.
                     *
                     * @param id id de la publication.
                     * @returns {Promise}
                     */
                    depublierPublication: function (id) {
                        var link = ESPACE_PUBLICATIONS + "/publications/" + id + "/depublier";
                        var deferred = $q.defer();

                        $http.put(link).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * depublier un exercice.
                     *
                     * @param id id de l'exercice.
                     * @returns {Promise}
                     */
                    depublierExercice: function (id) {
                        var deferred = $q.defer();

                        $http.put(ESPACE_PUBLICATIONS+ "/exercice/" + id +"/depublier").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * republier un exercice.
                     *
                     * @param id id de l'exercice.
                     * @returns {Promise}
                     */
                    republierExercice: function (id) {
                        var deferred = $q.defer();

                        $http.put(ESPACE_PUBLICATIONS+ "/exercice/" + id +"/republier").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * depublier fiche activite.
                     *
                     * @param id id de la fiche.
                     * @returns {Promise}
                     */
                    depublierFiche: function (id) {
                        var deferred = $q.defer();

                        $http.put(ESPACE_PUBLICATIONS+ "/fiche/" + id +"/depublier").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * depublier fiche activite.
                     *
                     * @param id id de la fiche.
                     * @returns {Promise}
                     */
                     depublierListFiche: function (activiteIdList) {
                        var deferred = $q.defer();

                        $http.put(ESPACE_PUBLICATIONS+ "/fichelist/" + activiteIdList +"/depublier").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * depublier fiche activite.
                     *
                     * @param id id de la fiche.
                     * @returns {Promise}
                     */
                    republierFiche: function (id) {
                        var deferred = $q.defer();

                        $http.put(ESPACE_PUBLICATIONS+ "/fiche/" + id +"/republier").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * republier des fiches activités en mass.
                     *
                     * @param activiteIdList liste d'id activité à (re)pulier.
                     * @returns {Promise}
                     */
                     republierListeFiche: function (activiteIdList) {
                        var deferred = $q.defer();

                        $http.put(ESPACE_PUBLICATIONS+ "/fichelist/" + activiteIdList +"/republier").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Suppression fiche d'activité
                     * @param {*} activiteIdList 
                     */
                    deleteActivite: function (activiteIdList) {
                        var deferred = $q.defer();

                        $http.delete(SUPPRESSION_ACTIVITE_URL + "/" + activiteIdList).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },


                    /**
                     * Modifier état publication.
                     *
                     * @param id id de la publication.
                     * @returns {Promise}
                     */
                    republierPublication: function (id) {
                        var link = ESPACE_PUBLICATIONS + "/publications/" + id + "/republier";
                        var deferred = $q.defer();

                        $http.put(link).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer les déclarations d'activité
                     *
                     * @param id de l'espace organisation.
                     * @returns {Promise}
                     */
                    getRapports: function (id) {
                        return this.commonGetService(EnvConf.api + "/rapport" + "/" + id);
                    },

                    /**
                     * Récupérer les déclarations d'activité
                     *
                     * @param id de l'espace organisation.
                     * @returns {Promise}
                     */
                    getFiche: function (id) {
                        return this.commonGetService(EnvConf.api + "/rapport/fiche" + "/" + id);
                    },

                    /**
                     * Récupérer les déclarations d'activité
                     *
                     * @param id de la publication de l'activité
                     * @returns {Promise}
                     */
                     getHistoriqueFiche: function (id) {
                        return this.commonGetService(EnvConf.api + "/rapport/historiquefiche" + "/" + id);
                    },

                    /**
                     * mise a jour des dates d'un exercice comptable
                     *
                     * @param id de l'espace organisation.
                     * @returns {Promise}
                     */
                    updateDatesExercice: function (exercice,espaceId) {
                        var deferred = $q.defer();

                        $http.put(UPDATE_EXERCICE_URL + "/" + exercice.id, exercice
                        ).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Retourne les nomenclatures utilisés par le service.
                     *
                     * @returns {Promise}
                     */
                    getNomenclatures: function () {
                        return this.commonGetService(EnvConf.api + "/nomenclature/declaration/all");
                    },

                    /**
                     * Activer / Désactiver l'espace organisation.
                     *
                     * @param espaceId id de l'espace organisation.
                     */
                    switchDeclarantAccountStatus: function (espaceId) {
                        var deferred = $q.defer();

                        $http.put(ESPACE_ACTIVATION_PUT_URL + espaceId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * Service commun de récupération de données.
                     *
                     * @param link service url.
                     * @returns {Promise}
                     */
                    commonGetService: function (link) {
                        var deferred = $q.defer();

                        $http.get(link).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getAllTypeOrganisation: function () {
                        var deferred = $q.defer();

                        $http.get(TYPE_ORGA_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /** Retourne la liste des pièces complémentaires pour un espace donné. */
                    getComplement: function (espaceId) {
                        var deferred = $q.defer();

                        $http.get(ESPACE_PIECES_COMPLEMENT_URL + "/" + espaceId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /** Retourne la liste des pièces versées lors de la demande de création de l'espace. */
                    getPiecesCreation: function (espaceId) {
                        var deferred = $q.defer();

                        $http.get(ESPACE_PIECES_CREATION_URL + "/" + espaceId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /** Retourne la liste des pièces versées lors d'une demande de changement de mandant. */
                    getPiecesMandant: function (espaceId) {
                        var deferred = $q.defer();

                        $http.get(MANDAT_ESPACE_URL + "/" + espaceId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Modifications des rôles du déclarant par rapport à l'espace corporate actuel
                     * @param id
                     * @param administrator
                     * @param publisher
                     * @param contributor
                     * @param locked
                     * @returns {Promise}
                     */
                    setAccessRoles: function (id, organisationId, administrator, publisher, contributor, locked) {
                        var deferred = $q.defer();

                        $http.put(ROLES_URL + "/" + id + "/" + organisationId, {
                            administrator: administrator,
                            publisher: publisher,
                            contributor: contributor,
                            locked: locked
                        }).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * MAJ Espace Organisation
                     * @param organisation
                     * @param card
                     * @returns {Promise}
                     */
                    setOrganisation: function (card, data) {
                        var deferred = $q.defer();

                        $http.put(CORPORATE_UPDATE_URL + "/" + card, data).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },


                    bloquerRelances: function (id) {
                        var deferred = $q.defer();

                        $http.put(CORPORATE_URL + "/" + id+"/bloquer_relances").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    deBloquerRelances: function (id) {
                        var deferred = $q.defer();

                        $http.put(CORPORATE_URL + "/" + id+"/debloquer_relances").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getStatutEnum: function () {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + "/corporate/organisation/surveillance/types").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    ajoutCO:function (formData) {
                        var deferred = $q.defer();
                        $http.post(AJOUT_CO_URL, formData, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        }).then(function (response) {
                            deferred.resolve(response);
                        }, function (response) {
                            deferred.reject(response);
                        });
                        return deferred.promise;
                    },

                    hasPublished: function (espaceId) {
                        var deferred = $q.defer();

                        $http.get(HAS_PUBLISHED + '/' + espaceId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }
                }
            }]);

})();
