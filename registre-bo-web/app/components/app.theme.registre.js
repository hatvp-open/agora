/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 * 
 * Palette de couelurs ADEL suivant la charte graphique
 *
 */
'use strict';

angular.module('hatvpRegistre')

.config(['$mdThemingProvider', function ($mdThemingProvider) {

    var customPrimary = {
        '50': '#68aaf7',
        '100': '#1f2f4e',
        '200': '#1b2b4b',
        '300': '#0b1a5d',
        '400': '#051639',
        '500': '#1F2F4E',
        '600': '#151f3a',
        '700': '#101929',
        '800': '#090e17',
        '900': '#020305',
        'A100': '#4e72b8',
        'A200': '#6080c0',
        'A400': '#728fc7',
        'A700': '#000000'
    };
    $mdThemingProvider.definePalette('customPrimary', customPrimary);

    var customAccent = {
        '50': '#210505',
        '100': '#370809',
        '200': '#4d0c0c',
        '300': '#640f10',
        '400': '#7a1213',
        '500': '#901617',
        '600': '#bc1c1d',
        '700': '#d22021',
        '800': '#df2c2e',
        '900': '#e34244',
        'A100': '#bc1c1d',
        'A200': '#354052',
        'A400': '#901617',
        'A700': '#e6595a'
    };
    $mdThemingProvider.definePalette('customAccent', customAccent);

    var customWarn = {
        '50': '#f997b8',
        '100': '#f87fa7',
        '200': '#f76797',
        '300': '#f64e86',
        '400': '#f43676',
        '500': '#eb1630',
        '600': '#eb0d57',
        '700': '#d30b4e',
        '800': '#bb0a45',
        '900': '#a2093c',
        'A100': '#fbafc8',
        'A200': '#fcc7d9',
        'A400': '#fde0ea',
        'A700': '#eb1630'
    };
    $mdThemingProvider.definePalette('customWarn', customWarn);

    var customBackground = {
        '50': '#ffffff',
        '100': '#ffffff',
        '200': '#ffffff',
        '300': '#ffffff',
        '400': '#ffffff',
        '500': '#F2F2F2',
        '600': '#e5e5e5',
        '700': '#d8d8d8',
        '800': '#cccccc',
        '900': '#bfbfbf',
        'A100': '#ffffff',
        'A200': '#ffffff',
        'A400': '#ffffff',
        'A700': '#b2b2b2'
    };
    $mdThemingProvider.definePalette('customBackground', customBackground);


    $mdThemingProvider.theme('registre')
        .primaryPalette('customPrimary')
        .accentPalette('customAccent')
        .warnPalette('customWarn')
        .backgroundPalette('customBackground');

}]);