/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre')

        .factory('serverErrorInterceptor', ['$log', '$q', '$location', '$injector',
            function ($log, $q, $location, $injector) {

                return {

                    'responseError': function (err) {
                        var state = $injector.get('$state');
                        var mdDialog = $injector.get('$mdDialog');

                        var status_type = String(err.status).charAt(0);

                        switch (status_type) {
                            case '4': // App Errors
                                switch (err.status) {
                                    case 401:// unauthorized
                                        mdDialog.show(
                                            mdDialog.alert()
                                                .title("Erreur authentification!")
                                                .textContent("Erreur d'authentification!")
                                                .ariaLabel("Erreur authentification")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        ).then(function () {
                                            state.go('home', {}, {reload: true});
                                        });
                                        break;

                                    case 403: // forbidden
                                        mdDialog.show(
                                            mdDialog.alert()
                                                .title("Droits insuffisants")
                                                .textContent("Vous n'avez pas les droits nécéssaires pour accéder à cette page.")
                                                .ariaLabel("Droits insuffisants")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        );
                                        break;

                                    case 400: // bad request
                                    case 404: // not found
                                    case 409: // conflict
                                    case 413: // Size overflow
                                    case 422: // entity error
                                        mdDialog.show(
                                            mdDialog.alert()
                                                .title("Données incorrectes!")
                                                .textContent(err.data.message)
                                                .ariaLabel("Données incorrectes")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        );
                                        break;

                                    /* Erreur Optimistic Lock */
                                    case 412:
                                        mdDialog.show(
                                            mdDialog.alert()
                                                .title("Conflit de données")
                                                .textContent("Les données actuelles ne sont plus à jour! " +
                                                    "Veuillez raffraichir la page pour récupérer la dernière version.")
                                                .ariaLabel("Conflit de données")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        );
                                        break;

                                    /* Erreurs session */
                                    case 418:// csrf token error
                                    case 419:// session expired

                                        state.go('homeMessage', {
                                            entity: 'sessionError'
                                        }, {reload: true});
                                        break;

                                    // Autres erreurs non gérées
                                    default:
                                        mdDialog.show(
                                            mdDialog.alert()
                                                .title("Erreur technique!")
                                                .textContent(err.status + ":" + err.statusText + " - " + err.data.message)
                                                .ariaLabel("Erreur technique")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        );
                                        break;
                                }
                                break;
                            case '5': // Server Errors
                                mdDialog.show(
                                    mdDialog.alert()
                                        .title("Erreur serveur!")
                                        .textContent("Veuillez joindre votre administrateur ou réessayer dans quelques instants.")
                                        .ariaLabel("Erreur serveur")
                                        .ok('OK')
                                        .clickOutsideToClose(true)
                                ).then(function () {
                                    state.go('home', {}, {reload: true});
                                });
                                break;
                        }


                        return $q.reject(err);
                    }
                };
            }]);

})();