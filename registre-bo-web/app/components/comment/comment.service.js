/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('CommentService', ['$http', '$q', '$log', '$cookies', 'EnvConf', 'API',
            function ($http, $q, $log, $cookies, EnvConf, API) {

                var BO_DECLARANT_COMMENT_GET_URL = EnvConf.api + API.comment.declarant + API.comment.all;
                var BO_ESPACE_COMMENT_GET_URL = EnvConf.api + API.comment.espace + API.comment.all;
                var BO_VALIDATION_COMMENT_GET_URL = EnvConf.api + API.comment.validation + API.comment.all;
                var BO_DEMANDE_COMMENT_GET_URL = EnvConf.api + API.comment.demande + API.comment.all;
                var BO_DEMANDE_CHANGEMENT_MANDANT_COMMENT_GET_URL = EnvConf.api + API.comment.demandeMandant + API.comment.all;

                var BO_DECLARANT_COMMENT_POST_URL = EnvConf.api + API.comment.declarant;
                var BO_ESPACE_COMMENT_POST_URL = EnvConf.api + API.comment.espace;
                var BO_VALIDATION_COMMENT_POST_URL = EnvConf.api + API.comment.validation;
                var BO_DEMANDE_COMMENT_POST_URL = EnvConf.api + API.comment.demande;
                var BO_DEMANDE_CHANGEMENT_MANDANT_COMMENT_POST_URL = EnvConf.api + API.comment.demandeMandant;

                return {
                    /**
                     * récupérer les commentaires d'un déclarant.
                     *
                     * @returns {Promise}
                     */
                    getAllCommentByDeclarantId: function (id) {
                        return this.getAllComment(BO_DECLARANT_COMMENT_GET_URL, id);
                    },
                    /**
                     * récupérer les commentaires d'un déclarant.
                     *
                     * @returns {Promise}
                     */
                    getAllCommentByEspaceId: function (id) {
                        return this.getAllComment(BO_ESPACE_COMMENT_GET_URL, id);
                    },
                    /**
                     * récupérer les commentaires d'un déclarant.
                     *
                     * @returns {Promise}
                     */
                    getAllCommentByValidationId: function (id) {
                        return this.getAllComment(BO_VALIDATION_COMMENT_GET_URL, id);
                    },
                    /**
                     * récupérer les commentaires d'une demande.
                     *
                     * @returns {Promise}
                     */
                    getAllCommentByDemandeId: function (id) {
                        return this.getAllComment(BO_DEMANDE_COMMENT_GET_URL, id);
                    },
                    /**
                     * récupérer les commentaires d'une demande de changement de mandant.
                     *
                     * @returns {Promise}
                     */
                    getAllCommentByDemandeChangementMandantId: function (id) {
                        return this.getAllComment(BO_DEMANDE_CHANGEMENT_MANDANT_COMMENT_GET_URL, id);
                    },
                    /**
                     * Méthode commune de récupération des commentaires;
                     *
                     * @param link api url.
                     * @param id contextId
                     * @returns {Promise}
                     */
                    getAllComment: function (link, id) {

                        var deferred = $q.defer();

                        $http.get(link + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Ajouter un commentaire pour un déclarant;
                     *
                     * @param comment commentaire à ajouter.
                     * @returns {Promise}
                     */
                    addDeclarantComment: function (comment) {
                        return this.addCommentComment(BO_DECLARANT_COMMENT_POST_URL, comment);
                    },

                    /**
                     * Ajouter un commentaire pour un espace organisation;
                     *
                     * @param comment commentaire à ajouter.
                     * @returns {Promise}
                     */
                    addEspaceComment: function (comment) {
                        return this.addCommentComment(BO_ESPACE_COMMENT_POST_URL, comment);

                    },

                    /**
                     * Ajouter un commentaire pour un validation d'une inscription;
                     *
                     * @param comment commentaire à ajouter.
                     * @returns {Promise}
                     */
                    addValidationComment: function (comment) {
                        return this.addCommentComment(BO_VALIDATION_COMMENT_POST_URL, comment);
                    },

                    /**
                     * Ajouter un commentaire pour une demande;
                     *
                     * @param comment commentaire à ajouter.
                     * @returns {Promise}
                     */
                    addDemandeComment: function (comment) {
                        return this.addCommentComment(BO_DEMANDE_COMMENT_POST_URL, comment);
                    },

                    /**
                     * Ajouter un commentaire pour une demande de changement de mandat;
                     *
                     * @param comment commentaire à ajouter.
                     * @returns {Promise}
                     */
                    addDemandeChangementMandantComment: function (comment) {
                        return this.addCommentComment(BO_DEMANDE_CHANGEMENT_MANDANT_COMMENT_POST_URL, comment);
                    },

                    /**
                     * Méthode commune d'ajout de commentaire;
                     *
                     * @param link lien de l'api.
                     * @param comment commentaire à ajouter.
                     * @returns {Promise}
                     */
                    addCommentComment: function (link, comment) {
                        var deferred = $q.defer();

                        $http.post(link, comment).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Modifier un commentaire pour un déclarant;
                     *
                     * @param comment commentaire à modifier.
                     * @returns {Promise}
                     */
                    updateDeclarantComment: function (comment) {
                        return this.updateCommentComment(BO_DECLARANT_COMMENT_POST_URL, comment);
                    },

                    /**
                     * Modifier un commentaire pour un espace organisation;
                     *
                     * @param comment commentaire à modifier.
                     * @returns {Promise}
                     */
                    updateEspaceComment: function (comment) {
                        return this.updateCommentComment(BO_ESPACE_COMMENT_POST_URL, comment);

                    },

                    /**
                     * Modifier un commentaire pour un validation d'une inscription;
                     *
                     * @param comment commentaire à modifier.
                     * @returns {Promise}
                     */
                    updateValidationComment: function (comment) {
                        return this.updateCommentComment(BO_VALIDATION_COMMENT_POST_URL, comment);
                    },

                    /**
                     * Modifier un commentaire pour une demande;
                     *
                     * @param comment commentaire à modifier.
                     * @returns {Promise}
                     */
                    updateDemandeComment: function (comment) {
                        return this.updateCommentComment(BO_DEMANDE_COMMENT_POST_URL, comment);
                    },

                    /**
                     * Modifier un commentaire pour une demande de changement de mandat;
                     *
                     * @param comment commentaire à ajouter.
                     * @returns {Promise}
                     */
                    updateDemandeChangementMandantComment: function (comment) {
                        return this.updateCommentComment(BO_DEMANDE_CHANGEMENT_MANDANT_COMMENT_POST_URL, comment);
                    },

                    /**
                     * Méthode commune dd modification de commentaire;
                     *
                     * @param link lien de l'api.
                     * @param comment commentaire à modifier.
                     * @returns {Promise}
                     */
                    updateCommentComment: function (link, comment) {
                        var deferred = $q.defer();

                        $http.put(link + "/" + comment.id, comment).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Supprimer un commentaire pour un déclarant;
                     *
                     * @param commentId commentaire à supprimer.
                     * @returns {Promise}
                     */
                    deleteDeclarantComment: function (commentId) {
                        return this.deleteCommentComment(BO_DECLARANT_COMMENT_POST_URL, commentId);
                    },

                    /**
                     * Supprimer un commentaire pour un espace organisation;
                     *
                     * @param commentId commentaire à supprimer.
                     * @returns {Promise}
                     */
                    deleteEspaceComment: function (commentId) {
                        return this.deleteCommentComment(BO_ESPACE_COMMENT_POST_URL, commentId);

                    },

                    /**
                     * Supprimer un commentaire pour un validation d'une inscription;
                     *
                     * @param commentId commentaire à supprimer.
                     * @returns {Promise}
                     */
                    deleteValidationComment: function (commentId) {
                        return this.deleteCommentComment(BO_VALIDATION_COMMENT_POST_URL, commentId);
                    },

                    /**
                     * Supprimer un commentaire pour une demande;
                     *
                     * @param commentId commentaire à supprimer.
                     * @returns {Promise}
                     */
                    deleteDemandeComment: function (commentId) {
                        return this.deleteCommentComment(BO_DEMANDE_COMMENT_POST_URL, commentId);
                    },

                    /**
                     * Supprimmer un commentaire pour une demande de changement de mandat;
                     *
                     * @param commentId commentaire à ajouter.
                     * @returns {Promise}
                     */
                    deleteDemandeChangementMandantComment: function (commentId) {
                        return this.deleteCommentComment(BO_DEMANDE_CHANGEMENT_MANDANT_COMMENT_POST_URL, commentId);
                    },

                    /**
                     * Méthode commune de suppression de commentaire;
                     *
                     * @param link lien de l'api.
                     * @param commentId commentaire à supprimer.
                     * @returns {Promise}
                     */
                    deleteCommentComment: function (link, commentId) {
                        var deferred = $q.defer();

                        $http.delete(link + "/" + commentId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }

                }

            }]);
})();