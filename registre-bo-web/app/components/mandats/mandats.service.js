/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')
        .service('MandatesService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf, API) {

                var MANDATS_URL = EnvConf.api + API.corporate.mandats.root + API.corporate.mandats.pending;
                var MANDAT_URL = EnvConf.api + API.corporate.mandats.root;


                return {

                    /**
                     * Récupérer une liste paginée des demandes de gestion de mandats
                     * @returns {Promise}
                     */
                    getMandates: function () {
                        var deferred = $q.defer();

                        $http.get(MANDATS_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer une liste paginée des demandes de gestion de mandats par statut
                     * @returns {Promise}
                     */
                    getPaginated: function (pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(MANDAT_URL + "/page/" + pageNumber + "/occurrence/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer une liste paginée
                     * @param keywords
                     * @param pageNumber
                     * @param resultPerPage
                     * @returns {Promise}
                     */
                    getPaginatedParRecherche: function (type, keywords, pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(MANDAT_URL + "/" + type + "/" + keywords + "/page/" + pageNumber + "/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });
                        return deferred.promise;
                    },

                    getMandateById: function (id) {
                        var deferred = $q.defer();

                        $http.get(MANDAT_URL + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    rejectMandate: function (id) {
                        var deferred = $q.defer();

                        $http.put(MANDAT_URL + "/refuser/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    validateMandate: function (id) {
                        var deferred = $q.defer();

                        $http.put(MANDAT_URL + "/valider/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * Demande de complément pour la création d'un espace collaboratif
                     * @returns {Promise}
                     */
                    demandeComplement: function (id, message) {
                        var deferred = $q.defer();

                        $http.post(MANDAT_URL + "/complement/"+id, message).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer template courriel demande de complément
                     * @returns {Promise}
                     */
                    getDemandeComplementTemplate: function (id, codeMotif) {
                        var deferred = $q.defer();

                        $http.get(MANDAT_URL + "/" + id + "/demandeComplementTemplate/" + codeMotif).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }

                }

            }]);
})();