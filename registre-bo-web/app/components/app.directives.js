/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre')

.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
})

.directive('exportToCsv', ['BlacklistService', function(BlacklistService){
  	return {
    	restrict: 'A',
        bindToController: true,
    	link: function (scope, element, attrs) {
    	    var blacklistAll= [];
    		var el = element[0];
	        element.bind('click', function(e){
                BlacklistService.getAllSpaceNotPaginated().then(function (res) {
                    blacklistAll = res.data.map(function(e){
                        return {
                            id: e.id,
                            denomination: e.nomUsage ? e.nomUsage : e.nomUsageHatvp ? e.nomUsageHatvp : e.denomination,
                            identifiant: e.identifiant,
                            creationDate: e.creationDate,
                            validationDate: e.validationDate,
                            dateExCourant: ('du '+e.listeExercices['0'].dateDebut+' au '+e.listeExercices['0'].dateFin).replace(/-/g,'/'), // du dd/mm/yyyy au dd/mm/yyyy
                            nbJoursBlacklist :e.listeExercices['0'].raisonBlacklistage=='fiche d\'activités et moyen alloués'? e.listeExercices['0'].nbJoursBlacklistFA:e.listeExercices['0'].nbJoursBlacklistFA+e.listeExercices['0'].nbJoursBlacklistMA,
                            raisonBlacklist :e.listeExercices['0'].raisonBlacklistage,
                            dateExPrecedent :e.listeExercices['1']!=null?  ('du '+e.listeExercices['1'].dateDebut+' au '+e.listeExercices['1'].dateFin).replace(/-/g,'/'):'pas d\'exercice précédent', // du dd/mm/yyyy au dd/mm/yyyy
                            nbJoursBlacklistExPrecedentFA :(e.listeExercices['1']!=null && e.listeExercices['1'].raisonBlacklistage !=null)? e.listeExercices['1'].nbJoursBlacklistFA:'pas d\'informations',
                            nbJoursBlacklistExPrecedentMA :(e.listeExercices['1']!=null && e.listeExercices['1'].raisonBlacklistage !=null)? e.listeExercices['1'].nbJoursBlacklistMA:'pas d\'informations',
                            nbJoursBlacklistTotalFA :e.listeExercices['1']!=null? e.listeExercices['0'].nbJoursBlacklistFA+e.listeExercices['1'].nbJoursBlacklistFA: e.listeExercices['0'].nbJoursBlacklistFA,
                            nbJoursBlacklistTotalMA :e.listeExercices['1']!=null? e.listeExercices['0'].nbJoursBlacklistMA +e.listeExercices['1'].nbJoursBlacklistMA: e.listeExercices['0'].nbJoursBlacklistMA
                        }
                    });
                    var table = blacklistAll;
                    var csvString = 'ID,Denomination ou nom usage,identifiant,date introduction, date de publication, dates exercice courant, nombre jours blacklistes, raison blacklistage, date de exercice precedent, nombre de jours blackliste exercice precedent pour FA,nombre de jours blackliste exercice precedent pour MA,nombre de jours blackliste total pour FA,nombre de jours blackliste total pour MA,\r\n';
                    for(var i=0; i<table.length;i++){
                        var line = '';
                        for (var index in table[i]) {
                            if (line != '') line += ','
                            line += table[i][index];
                        }
                        csvString += line + '\r\n';

                        //csvString = csvString.substring(0,csvString.length - 1);
                        //csvString = csvString + "\n";
                    }

                    //csvString = csvString.substring(0, csvString.length - 1);
                    var a = document.createElement("a");
                    a.href = 'data:attachment/csv;charset=utf-8,' + encodeURI(csvString);
                    a.target = '_blank';
                    a.download = 'blacklistes.csv';
                    document.body.appendChild(a);
                    a.click();
                });
	        });
    	}
  	}
	}])
.directive('exportSurveillance', ['SurveillanceService', function(SurveillanceService){
  	return {
    	restrict: 'A',
        bindToController: true,
    	link: function (scope, element, attrs) {
    	    var surveillanceAll= [];
    		var el = element[0];
	        element.bind('click', function(e){
	        	SurveillanceService.getAllNotPaginated().then(function (res) {
	        		surveillanceAll = res.data.map(function(s){
                        return {
                        	espaceId: s.espaceId?s.espaceId:'pas d\'espace' ,
                            denomination: s.nomUsage ? s.nomUsage : s.denomination,
                    		organisationId: s.organisationId,
                    		statut: s.statut? s.statut.label: 'pas de statut',
                    		dateSurveillance: s.dateSurveillance,
                           
                        }
                    });
                    var table = surveillanceAll;
                    var csvString = 'espaceId,Denomination ou nom usage,organisationId,statut, dateSurveillance,\r\n';
                    for(var i=0; i<table.length;i++){
                        var line = '';
                        for (var index in table[i]) {
                            if (line != '') line += ','
                            line += table[i][index];
                        }
                        csvString += line + '\r\n';

                    }


                    var a = document.createElement("a");
                    a.href = 'data:attachment/csv;charset=utf-8,' + encodeURI(csvString);
                    a.target = '_blank';
                    a.download = 'surveillance.csv';
                    document.body.appendChild(a);
                    a.click();
                });
	        });
    	}
  	}
	}])
    .directive('exportAntoinette', ['QualiteService', function(QualiteService){
        return {
            restrict: 'A',
            bindToController: true,
            link: function (scope, element, attrs) {
                var qualifs = [];
                var el = element[0];
                element.bind('click', function(e){
                    QualiteService.getAllObjets().then(function (res) {
                        qualifs = res.data.dtos.map(function (e) {
                            return {
                                enregistrementDate: e.dateEnregistrement,
                                idOrganisation: e.identifiantNational,
                                idFiche: e.idFiche,
                                objet: e.sentence,
                                note: e.valid,
                                coefficientConfiance: e.confidence,
                                denomination: e.denomination,
                                nomUsage: e.nomUsage
                            }
                        });
                        var table = qualifs;
                        var csvString = 'Date enregistrement| Identifiant National| Id Fiche| Objet| Note| Coefficient| Denomination| Nom usage \r\n';
                        for(var i=0; i<table.length;i++){
                            var line = '';
                            for (var index in table[i]) {
                                if (line != '') line += '|'
                                line += table[i][index];
                            }
                            csvString += line + '\r\n';

                            //csvString = csvString.substring(0,csvString.length - 1);
                            //csvString = csvString + "\n";
                        }

                        //csvString = csvString.substring(0, csvString.length - 1);
                        var a = document.createElement("a");
                        a.href = 'data:attachment/csv;charset=utf-8,' + encodeURI(csvString);
                        a.target = '_blank';
                        a.download = 'antoinette.csv';
                        document.body.appendChild(a);
                        a.click();
                    });
                });
            }
        }
    }]);