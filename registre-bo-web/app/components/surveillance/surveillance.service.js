/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')
        .service('SurveillanceService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf, API) {

                return {


                    getSurveillanceByOrganisationId: function (organisationId) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + "/corporate/organisation/surveillance" + "/" + organisationId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer une liste paginée des demandes de gestion de mandats par statut
                     * @returns {Promise}
                     */
                    getPaginated: function (pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + "/corporate/organisation/surveillance/detail/list" + "/page/" + pageNumber + "/occurence/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * Récupérer une liste paginée des demandes de gestion de mandats par statut
                     * @returns {Promise}
                     */
                    getAllNotPaginated: function (pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + "/corporate/organisation/surveillance/download").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    verifyList: function (list) {
                        var deferred = $q.defer();

                        $http.post(EnvConf.api + "/corporate/organisation/surveillance/verify", list).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    saveList: function(list) {
                        var deferred = $q.defer();

                        $http.post(EnvConf.api + "/corporate/organisation/surveillance/list/", list).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    saveSurveillance: function (surveillance) {
                        var deferred = $q.defer();

                        $http.post(EnvConf.api + "/corporate/organisation/surveillance/", surveillance).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    updateSurveillance: function (surveillance) {
                        var deferred = $q.defer();

                        $http.put(EnvConf.api + "/corporate/organisation/surveillance/", surveillance).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    deleteSurveillance: function (surveillanceId) {
                        var deferred = $q.defer();

                        $http.delete(EnvConf.api + "/corporate/organisation/surveillance/"+surveillanceId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /**
                     * Récupérer une liste paginée
                     * @param keywords
                     * @param pageNumber
                     * @param resultPerPage
                     * @returns {Promise}
                     */
                    getPaginatedParRecherche: function (type, keyword, pageNumber, resultPerPage) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + "/corporate/organisation/surveillance/detail/list" + "/type/" + type + "/recherche/" + keyword + "/page/" + pageNumber + "/occurence/" + resultPerPage).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });
                        return deferred.promise;
                    }

                }

            }]);
})();