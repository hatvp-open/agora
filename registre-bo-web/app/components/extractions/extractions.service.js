/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('ExtractionsService', ['$http', '$q', '$log', '$cookies', 'EnvConf', 'API',
            function ($http, $q, $log, $cookies, EnvConf, API) {

                var EXTRACTION_PERIODE_URL = EnvConf.api + API.extractions.periode;
                /**
                 * Récupérer la liste des chiffres d'une période
                 * @returns {Promise}
                 */
                return {                  

                    /**
                     *
                     * @param chiffresSurUnePeriode new user data.
                     * @returns {Promise}
                     */
                    getChiffreDeLaPeriode: function (chiffresSurUnePeriode) {
                        var deferred = $q.defer();

                        $http.put(EXTRACTION_PERIODE_URL, chiffresSurUnePeriode).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });
                        return deferred.promise;
                    },
                                       
                }
            }]);

})();