/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')
        .service('GroupService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf) {

                return {
                    getAllGroupAndUsers: function () {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + "/group/all").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getGroupsAndusersBySearchPaginated: function (type, keyword, page, occurrence) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + '/group' + "/type/" + type + "/keyword/" + keyword + "/page/" + page + "/occurrence/" + occurrence).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    updateGroup: function (group) {
                        var deferred = $q.defer();

                        $http.put(EnvConf.api + "/group/editGroup", group).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    deleteGroup: function (groupId) {
                        var deferred = $q.defer();

                        $http.get(EnvConf.api + "/group/deleteGroup/"+groupId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    addGroup: function (group) {
                        var deferred = $q.defer();

                        $http.put(EnvConf.api + "/group/addGroup",group).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }
                }
            }]);
})();