/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.group;


import fr.hatvp.registre.business.backoffice.GroupUtilisateurBoService;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.GroupBoDto;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/group")
public class GroupRestService {
    private final GroupUtilisateurBoService groupUtilisateurBoService;

    public GroupRestService(GroupUtilisateurBoService groupUtilisateurBoService) {
        this.groupUtilisateurBoService = groupUtilisateurBoService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<GroupBoDto>> getAllGroupAndUsers() {
        return ResponseEntity.ok(groupUtilisateurBoService.getAllGroupAndUsers());

    }

    @GetMapping("/type/{type}/keyword/{keyword}/page/{page}/occurrence/{occurrence}")
    public ResponseEntity<PaginatedDto<GroupBoDto>> getGroupsAndusersBySearchPaginated(@PathVariable("type") final String type, @PathVariable("keyword") final String keyword,
                                                                                       @PathVariable("page") final Integer page, @PathVariable("occurrence") final Integer occurrence){
        return ResponseEntity.ok(groupUtilisateurBoService.getGroupsAndusersBySearchPaginated(type,keyword,page,occurrence));
    }

    @PutMapping("/editGroup")
    public ResponseEntity<GroupBoDto> updateGroup(@RequestBody final GroupBoDto groupBoDto){

        final GroupBoDto updatedGroupBoDto = this. groupUtilisateurBoService.addOrUpdateGroup(groupBoDto);

        return new ResponseEntity<>(updatedGroupBoDto, HttpStatus.ACCEPTED);
    }

    @GetMapping("/deleteGroup/{id}")
    public ResponseEntity<Void> deleteGroup(@PathVariable final Long id){

        this.groupUtilisateurBoService.deleteGroup(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/addGroup")
    public ResponseEntity<GroupBoDto> addGroup(@RequestBody final GroupBoDto groupBoDto){

        final GroupBoDto newGroupBoDto = this.groupUtilisateurBoService.addOrUpdateGroup(groupBoDto);

        return new ResponseEntity<>(newGroupBoDto, HttpStatus.CREATED);
    }
}
