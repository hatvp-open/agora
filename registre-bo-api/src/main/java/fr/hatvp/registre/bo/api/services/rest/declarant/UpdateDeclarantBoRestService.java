/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.declarant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.backoffice.DeclarantBoService;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.utils.ValidatePatternUtils;


@RestController
@RequestMapping("/declarant")
@PreAuthorize("hasRole('ROLE_GESTION_COMPTES_DECLARANTS')")
public class UpdateDeclarantBoRestService {

    private final DeclarantBoService declarantBoService;

    @Autowired
    public UpdateDeclarantBoRestService(final DeclarantBoService declarantBoService) {
        this.declarantBoService = declarantBoService;
    }

    /**
     * Activer / Désactive rle compte déclarant.
     *
     * @param declarantId id du déclarant.
     * @return declarant DTO.
     */
    @PutMapping("/activate/{declarantId}")
    @PreAuthorize("hasRole('ROLE_RESTRICTION_ACCES')")
    public ResponseEntity<DeclarantDto> switchEnableState(@PathVariable final Long declarantId) {
        final DeclarantDto declarantDto = new DeclarantDto();
        declarantDto.setId(declarantId);
        return new ResponseEntity<>(this.declarantBoService.switchActivationCompte(declarantDto),
            HttpStatus.ACCEPTED);
    }

    /**
     * Cette méthode permet de mettre à jour de l'mail prinicipal d'un déclarant.
     *
     * @param declarantDto objet déclarant en entrée.
     * @return les informations du déclarant misent à jour.
     */
    @PutMapping("/email")
    @PreAuthorize("hasRole('ROLE_MODIFICATION_EMAIL_DECLARANT')")
    public ResponseEntity<DeclarantDto> updateDeclarantEmail(
        @RequestBody final DeclarantDto declarantDto) {

        if ((declarantDto.getEmail() != null) && !declarantDto.getEmail().isEmpty()
        		&& !ValidatePatternUtils.validateEmail(declarantDto.getEmail())) {
            // EMAIL pattern: RFC 5322
           throw new BusinessGlobalException("Adresse email non valide. ex: xxxx@xxxx.xxx");
        }

        return new ResponseEntity<>(this.declarantBoService.updateEmailDeclarant(declarantDto),
            HttpStatus.ACCEPTED);
    }

    /**
     * Cette méthode permet bien de valider le changement de mail demandé par le déclarant au niveau front,
     * dans le cas où il fait un recours "SAV", notamment si son délais d'expiration pour l'utilisation
     * du lien d'activation est expiré.
     *
     * @param declarantDto objet déclarant en entrée.
     * @return les informations du déclarant mises à jour.
     */
    @PutMapping("/change/email")
    @PreAuthorize("hasRole('ROLE_MODIFICATION_EMAIL_DECLARANT')")
    public ResponseEntity<DeclarantDto> validateDeclarantEmailUpdate(
        @RequestBody final DeclarantDto declarantDto) {

        if ((declarantDto.getEmailTemp() != null) && !declarantDto.getEmailTemp().isEmpty()
        		&& !ValidatePatternUtils.validateEmail(declarantDto.getEmailTemp())) {
            // EMAIL pattern: RFC 5322
           throw new BusinessGlobalException("Adresse email non valide. ex: xxxx@xxxx.xxx");
        }

        return new ResponseEntity<>(this.declarantBoService.validateDeclarantEmailUpdate(declarantDto),
            HttpStatus.ACCEPTED);
    }

    /**
     * Modification du compte de déclarant.
     *
     * @param declarantDto declarant dto.
     * @return un {@link DeclarantDto} modifié.
     */
    @PutMapping
    public ResponseEntity<DeclarantDto> updateDeclarantAccount(@RequestBody final DeclarantDto declarantDto) {
        return ResponseEntity.accepted()
            .body(this.declarantBoService.updateDeclarant(declarantDto));
    }
}