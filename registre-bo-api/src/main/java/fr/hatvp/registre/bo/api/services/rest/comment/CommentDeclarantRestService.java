/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.comment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.backoffice.comment.CommentDeclarantBoService;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;

/**
 * Classe de controlleur pour les commentaire des déclarants.
 *
 * @version $Revision$ $Date${0xD}
 */

@RestController
@RequestMapping("/comment/declarant")
public class CommentDeclarantRestService {

	/** Service des commentaires des déclarants. */
	private final CommentDeclarantBoService commentDeclarantBoService;

	/** Initialisation du service. */
	@Autowired
	public CommentDeclarantRestService(CommentDeclarantBoService commentDeclarantBoService) {
		this.commentDeclarantBoService = commentDeclarantBoService;
	}

	/**
	 *
	 * @param declarantId
	 *            id du déclarant.
	 * @return liste des commentaires du déclarant.
	 */
	@GetMapping("/all/{declarantId}")
	public ResponseEntity<List<CommentBoDto>> getCommentBydeclarantId(@PathVariable final Long declarantId) {
		return ResponseEntity.ok().body(commentDeclarantBoService.getAllComment(declarantId));
	}

	/**
	 *
	 * @param comment
	 *            commentaire à sauvegarder.
	 * @return commentaire sauvegardé.
	 */
	@PostMapping
	public ResponseEntity<CommentBoDto> saveComment(@RequestBody final CommentBoDto comment) {
		return new ResponseEntity<>(commentDeclarantBoService.addComment(comment), HttpStatus.CREATED);
	}

	/**
	 * Modifier un commentaire.
	 *
	 * @param commentId
	 *            id du commentaire.
	 */
	@PutMapping("/{commentId}")
	ResponseEntity<CommentBoDto> updateComment(@PathVariable final Long commentId, @RequestBody final CommentBoDto comment) {
		return new ResponseEntity<>(commentDeclarantBoService.updateComment(comment), HttpStatus.OK);
	}

	/**
	 * Supprimer un commentaire.
	 * 
	 * @param commentId
	 *            id du commentaire.
	 */
	@DeleteMapping("/{commentId}")
	public ResponseEntity<Void> deleteComment(@PathVariable final Long commentId) {
		commentDeclarantBoService.delete(commentId);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
