

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.espace;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.DemandeOrganisationService;
import fr.hatvp.registre.business.OrganisationService;
import fr.hatvp.registre.business.utils.SiretSirenRnaHatvpValidation;
import fr.hatvp.registre.commons.dto.DemandeOrganisationDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.backoffice.StatutDemandeOrganisationEnum;

/**
 * Contrôleur REST pour les Demandes d'ajout d'une Organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/organisation")
@PreAuthorize("hasRole('ROLE_GESTION_REFERENTIEL_ORGANISATIONS')")
public class DemandeOrganisationRestService {

	/** Service DemandeOrganisation */
	@Autowired
	private DemandeOrganisationService demandeOrganisationService;

	/** Service DemandeOrganisation */
	@Autowired
	private OrganisationService organisationService;

	/**
	 * Récupérer liste de toutes les demandes d'ajout d'organisation.
	 *
	 * @return liste des demandes
	 */
	@GetMapping("/demandes")
	public ResponseEntity<List<DemandeOrganisationDto>> getDemandes() {
		return ResponseEntity.ok(this.demandeOrganisationService.getDemandesOrganisation());
	}

	/**
	 * Récupérer une liste paginée de toutes les demandes d'ajout d'organisation en fonction du status.
	 *
	 * @param status
	 * @param pageNumber
	 * @param resultPerPage
	 * @return liste paginée des demandes selon le status
	 */
	@GetMapping("/demandes/{status}/page/{pageNumber}/occurrence/{resultPerPage}")
	public ResponseEntity<PaginatedDto<DemandeOrganisationDto>> getPaginatedDemandesByStatus(@PathVariable final StatutDemandeOrganisationEnum status, @PathVariable final Integer pageNumber,
			@PathVariable final Integer resultPerPage) {

		final PaginatedDto<DemandeOrganisationDto> paginatedDto;

		if (StatutDemandeOrganisationEnum.TOUTES_LES_DEMANDES.equals(status)) {
			paginatedDto = this.demandeOrganisationService.getPaginatedDemandesOrganisation(pageNumber, resultPerPage);
		} else {
			paginatedDto = this.demandeOrganisationService.getPaginatedDemandesOrganisationByStatus(status, pageNumber, resultPerPage);
		}

		return ResponseEntity.ok(paginatedDto);
	}

	/**
	 * Récupérer une demande d'ajout d'organisation par id.
	 *
	 * @return liste des demandes
	 */
	@GetMapping("/demande/{id}")
	public ResponseEntity<DemandeOrganisationDto> getDemande(@PathVariable final Long id) {
		return ResponseEntity.ok(this.demandeOrganisationService.getDemandeOrganisation(id));
	}

	/**
	 * Clôturer une demande organisation.
	 *
	 * @param id
	 *            identifiant demande organisation à clôturer
	 * @param statut
	 *            statut clôture demande si Organisation refusée ou trouvée
	 */
	@PutMapping("/demande/{id}")
	public ResponseEntity<Void> cloturerDemande(@PathVariable final Long id, @RequestBody final String statut) {
		if (statut.equals(StatutDemandeOrganisationEnum.TRAITEE_ORGANISATION_REFUSEE.name())) {
			this.demandeOrganisationService.cloturerDemande(id, StatutDemandeOrganisationEnum.TRAITEE_ORGANISATION_REFUSEE);
		} else if (statut.equals(StatutDemandeOrganisationEnum.TRAITEE_ORGANISATION_TROUVEE.name())) {
			this.demandeOrganisationService.cloturerDemande(id, StatutDemandeOrganisationEnum.TRAITEE_ORGANISATION_TROUVEE);
		} else if (statut.equals(StatutDemandeOrganisationEnum.A_TRAITER.name())) {
			this.demandeOrganisationService.cloturerDemande(id, StatutDemandeOrganisationEnum.A_TRAITER);
		}

		return ResponseEntity.accepted().build();
	}

	/**
	 * Clôturer demande avec création d'une nouvelle organisation.
	 *
	 * @param id
	 *            identifiant demande organisation
	 * @return numéro HATVP unique généré
	 */
	@PostMapping(value = "/demande/{id}")
	public ResponseEntity<OrganisationDto> cloturerDemandeCreationOrganisation(@PathVariable final Long id, @RequestBody final OrganisationDto organisation) {
		if (!StringUtils.isEmpty(organisation.getSiren()) && (new SiretSirenRnaHatvpValidation(organisation.getSiren()).getType().equals("WRONGNUMBER")
				|| new SiretSirenRnaHatvpValidation(organisation.getSiren()).getType().equals("NOTVALID"))) {
			throw new BusinessGlobalException(ErrorMessageEnum.ORGANISATION_PARAM_INCORRECT.getMessage());
		}

		if (!StringUtils.isEmpty(organisation.getRna()) && (new SiretSirenRnaHatvpValidation(organisation.getRna()).getType().equals("WRONGNUMBER")
				|| new SiretSirenRnaHatvpValidation(organisation.getRna()).getType().equals("NOTVALID"))) {
			throw new BusinessGlobalException(ErrorMessageEnum.ORGANISATION_PARAM_INCORRECT.getMessage());
		}

		if (organisation.getDunsNumber() != null && organisation.getDunsNumber().length() != 9) {
			throw new BusinessGlobalException(ErrorMessageEnum.ORGANISATION_PARAM_INCORRECT.getMessage());
		}

		final String hatvpNumber = this.organisationService.creerOrganisation(organisation);
		this.demandeOrganisationService.cloturerDemande(id, StatutDemandeOrganisationEnum.TRAITEE_ORGANISATION_CREEE);

		final OrganisationDto res = new OrganisationDto();
		res.setHatvpNumber(hatvpNumber);

		return ResponseEntity.accepted().body(res);
	}

	/**
	 * Récupérer la liste de toutes les organisations avec un numéro HATVP.
	 *
	 * @return liste des organisations HATVP.
	 */
	@GetMapping("/hatvp")
	public ResponseEntity<List<OrganisationDto>> getOrganisationsHatvp() {
		return ResponseEntity.ok(this.organisationService.getHatvpOrganisations());
	}

	/**
	 * Récupérer une page spécifique de la liste Paginée
	 *
	 * @param pageNumber
	 * @param resultPerPage
	 * @return une page de la liste des dtos
	 */
	@GetMapping("/demandes/{type}/{keywords}/page/{page_no}/{result_per_page}")
	@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
	public ResponseEntity<PaginatedDto<DemandeOrganisationDto>> getEspacesEnAttenteParRecherche(@PathVariable("type") final String type, @PathVariable("keywords") final String keywords,
			@PathVariable("page_no") final Integer pageNumber, @PathVariable("result_per_page") final Integer resultPerPage) {
		return ResponseEntity.ok(this.demandeOrganisationService.getDemandesOrganisationParRecherche(type, keywords, pageNumber, resultPerPage));
	}

	/**
	 * Demander des pièces complémentaires
	 *
	 * @param id
	 *            identifiant demande organisation
	 * @param corpsEmail
	 *            l'email à envoyer
	 */
	@PostMapping("/demande/complement/{id}")
	public ResponseEntity<Void> complement(@PathVariable final Long id, @RequestBody final String corpsEmail) {
		this.demandeOrganisationService.demandeComplementInformation(id, corpsEmail);
		return ResponseEntity.accepted().build();
	}

}
