/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.declarant;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.backoffice.DeclarantBoService;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;

/**
 * Controleur de gestion des déclarants.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/declarant")
@PreAuthorize("hasRole('ROLE_GESTION_COMPTES_DECLARANTS')")
public class GetDeclarantBoRestService {

	private final DeclarantBoService declarantBoService;

	@Autowired
	public GetDeclarantBoRestService(final DeclarantBoService declarantBoService) {
		this.declarantBoService = declarantBoService;
	}

	/**
	 * Récupérer un déclrant par son id.
	 *
	 * @param declarantId
	 *            id du déclarant.
	 * @return un {@link DeclarantDto}.
	 */
	@GetMapping("/{declarantId}")
	@PreAuthorize("hasRole('ROLE_GESTION_COMPTES_DECLARANTS')")
	public ResponseEntity<DeclarantDto> getDeclarantById(@PathVariable Long declarantId) {
		return new ResponseEntity<>(this.declarantBoService.findOne(declarantId), HttpStatus.OK);
	}
	/**
	 * Récupérer un déclrant par son mail.
	 *
	 * @param mail
	 *            mail du déclarant.
	 * @return un {@link DeclarantDto}.
	 */
	@PostMapping("/mail")
	@PreAuthorize("hasRole('ROLE_GESTION_COMPTES_DECLARANTS')")
	public ResponseEntity<DeclarantDto> getDeclarantByMail(@RequestBody @Valid final String emailRecherche) {
		DeclarantDto declarant = this.declarantBoService.loadDeclarantByEmail(emailRecherche);
		return new ResponseEntity<>(declarant, HttpStatus.OK);
	}
	/**
	 * Récupérer la liste de tous les déclarants.
	 *
	 * @return liste de {@link DeclarantDto}.
	 */
	@GetMapping("/all")
	@PreAuthorize("hasRole('ROLE_GESTION_COMPTES_DECLARANTS')")
	public ResponseEntity<List<DeclarantDto>> getAllDeclarant() {
		return new ResponseEntity<>(this.declarantBoService.findAllDeclarant(), HttpStatus.OK);
	}

	/**
	 * Récupérer une liste paginée de tous les déclarants.
	 *
	 * @return le {@link PaginatedDto}.
	 */
	@GetMapping("/page/{pageNumber}/{resultPerPage}")
	@PreAuthorize("hasRole('ROLE_GESTION_COMPTES_DECLARANTS')")
	public ResponseEntity<PaginatedDto<DeclarantDto>> getPaginatedDeclarant(@PathVariable Integer pageNumber,
			@PathVariable Integer resultPerPage) {

		PaginatedDto<DeclarantDto> paginatedDeclarant = this.declarantBoService.findPaginatedDeclarant(pageNumber,
				resultPerPage);

		return new ResponseEntity<>(paginatedDeclarant, HttpStatus.OK);
	}

    /**
     * Récupérer une liste paginée de tous les déclarants avec recherche.
     *
     * @return le {@link PaginatedDto}.
     */
    @GetMapping("/page/{type}/{keywords}/{pageNumber}/{resultPerPage}")
    @PreAuthorize("hasRole('ROLE_GESTION_COMPTES_DECLARANTS')")
    public ResponseEntity<PaginatedDto<DeclarantDto>> getPaginatedDeclarantParRecherche(
        @PathVariable("type") final String type,
        @PathVariable("keywords") final String keywords,
        @PathVariable Integer pageNumber,
        @PathVariable Integer resultPerPage) {

        PaginatedDto<DeclarantDto> paginatedDeclarant = this.declarantBoService.findPaginatedDeclarantParRecherche(type, keywords, pageNumber,
            resultPerPage);

        return new ResponseEntity<>(paginatedDeclarant, HttpStatus.OK);
    }

	/**
	 * liste des espaces collaboratifs du déclarant .
	 *
	 * @param declarantId
	 *            id du déclarant.
	 * @return liste de {@link InscriptionEspaceDto}
	 */
	@GetMapping("/espace/all/{declarantId}")
	@PreAuthorize("hasRole('ROLE_GESTION_COMPTES_DECLARANTS')")
	public ResponseEntity<List<InscriptionEspaceDto>> getAllDeclarantEspace(@PathVariable final Long declarantId) {
		return ResponseEntity.ok().body(this.declarantBoService.getAllDeclarantEspaceOrganisation(declarantId));
	}
}
