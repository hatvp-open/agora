/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.espace;

import java.util.Set;
import java.util.TreeSet;

import fr.hatvp.registre.business.InscriptionEspaceService;
import fr.hatvp.registre.commons.dto.DeclarantRolesDto;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Cette classe permet de gérer les espaces organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/gestion")
public class GestionRolesEspaceOrganisationRestService
{

    /** Service des espaces corporate. */
    @Autowired
    private InscriptionEspaceService inscriptionEspaceService;

    /**
     * Mettre à jour les rôles et verrou d'une inscription.
     *
     * @param id identifiant inscription espace.
     * @return 201 accepté.
     */
    @PreAuthorize("hasRole('ROLE_GESTION_COMPTES_DECLARANTS')")
    @PutMapping("/access/{inscriptionId}/{espaceOrganisationId}")
    public ResponseEntity<Void> setAccessRoles(
        @PathVariable final Long inscriptionId,
        @RequestBody final DeclarantRolesDto declarantRolesDto,
        @PathVariable final Long espaceOrganisationId)
    {
        final Set<RoleEnumFrontOffice> roles = new TreeSet<>();
        if (Boolean.TRUE.equals(declarantRolesDto.isAdministrator())) {
            roles.add(RoleEnumFrontOffice.ADMINISTRATEUR);
        }
        if (Boolean.TRUE.equals(declarantRolesDto.isPublisher())) {
            roles.add(RoleEnumFrontOffice.PUBLICATEUR);
        }
        if (Boolean.TRUE.equals(declarantRolesDto.isContributor())) {
            roles.add(RoleEnumFrontOffice.CONTRIBUTEUR);
        }

        this.inscriptionEspaceService.setDeclarantRoles(inscriptionId, espaceOrganisationId, roles, declarantRolesDto.isLocked());

        return ResponseEntity.accepted().build();
    }
}
