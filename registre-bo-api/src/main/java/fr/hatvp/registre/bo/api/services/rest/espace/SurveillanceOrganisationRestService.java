/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.espace;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.SurveillanceService;
import fr.hatvp.registre.commons.dto.ListTypeSurveillanceDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.SurveillanceDetailDto;
import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;

@RestController
@RequestMapping("/corporate/organisation")
public class SurveillanceOrganisationRestService {

	@Autowired
	private SurveillanceService surveillanceService;

	/**
	 * recupere une surveillance par l'organisationID
	 */
	@GetMapping("surveillance/{organisationId}")
	public ResponseEntity<SurveillanceDetailDto> getSurveillanceByOrganisationID(@PathVariable Long organisationId) {
		SurveillanceDetailDto dto = this.surveillanceService.getSurveillance(organisationId);
		return ResponseEntity.ok(dto);
	}
    /**
     * recupere la liste des surveillance paginée
     */
    @GetMapping("surveillance/detail/list/page/{page}/occurence/{occurence}")
    public ResponseEntity<PaginatedDto<SurveillanceDetailDto>> getSurveillancesPaginated(@PathVariable Integer page, @PathVariable Integer occurence) {
        PaginatedDto<SurveillanceDetailDto> listeDetail = this.surveillanceService.getSurveillancesPaginated(page, occurence);
        return ResponseEntity.ok(listeDetail);
    }

    /**
     * Récupérer une page spécifique de la liste Paginée
     *
     * @param pageNumber
     * @param resultPerPage
     * @return une page de la liste des dtos
     */
    @GetMapping("surveillance/detail/list/type/{type}/recherche/{keywords}/page/{page_no}/occurence/{result_per_page}")
    public ResponseEntity<PaginatedDto<SurveillanceDetailDto>> getSurveillancePaginatedBySearch (@PathVariable("type") final String type,
                                                                                                 @PathVariable("keywords") final String keywords, @PathVariable("page_no") final Integer pageNumber, @PathVariable("result_per_page") final Integer resultPerPage) {
        return ResponseEntity.ok(this.surveillanceService.getSurveillancePaginatedBySearch(type, keywords, pageNumber, resultPerPage));
    }


    /**
     * permet le telechargement de la liste des surveillances
     */
    @GetMapping("surveillance/download")
    public ResponseEntity<List<SurveillanceDetailDto>> getSurveillances() {

        return ResponseEntity.ok(this.surveillanceService.getSurveillances());
    }




    /**
     * recupere les différents types de surveillances
     */
    @GetMapping("/surveillance/types")
    public ResponseEntity<ListTypeSurveillanceDto> getTypeSurveillance() {

        final ListTypeSurveillanceDto dto = new ListTypeSurveillanceDto();

        for (final SurveillanceOrganisationEnum statutActiviteEnum : SurveillanceOrganisationEnum.values()) {
            dto.getSurveillanceOrganisationEnumList().add(statutActiviteEnum);
        }

        return ResponseEntity.ok(dto);
    }

	/**
	 *
	 */
	@PostMapping("/surveillance")
	public ResponseEntity<Void> saveSurveillance(@RequestBody final SurveillanceDetailDto surveillance) {

		this.surveillanceService.saveSurveillance(surveillance);

		return ResponseEntity.ok().build();
	}


    /**
     *
     */
    @PostMapping("/surveillance/list")
    public ResponseEntity<Void> putListOrganisationUnderWatch(@RequestBody final List<SurveillanceDetailDto> listSurveillance) {

        this.surveillanceService.saveListeSurveillance(listSurveillance);

        return ResponseEntity.ok().build();
    }



    /**
     *
     */
    @PostMapping(path = "/surveillance/verify")
    public ResponseEntity<List<SurveillanceDetailDto>> verifyListOrganisation(@RequestBody final List<String> organisationIdList) {
        List<SurveillanceDetailDto> surveillances = this.surveillanceService.verifyListOrganisation(organisationIdList);
        return ResponseEntity.ok(surveillances);
    }
    @PutMapping("/surveillance")
    public ResponseEntity<SurveillanceDetailDto> updateSurveillance(@RequestBody final SurveillanceDetailDto surveillance) {

        SurveillanceDetailDto dto = this.surveillanceService.updateSurveillance(surveillance);

        return ResponseEntity.ok(dto);
    }
    /**
     *
     */
    @DeleteMapping("/surveillance/{surveillanceId}")
    public ResponseEntity<Void> deleteSurveillance(@PathVariable final Long surveillanceId) {

        this.surveillanceService.deleteSurveillance(surveillanceId);

        return ResponseEntity.ok().build();
    }






}
