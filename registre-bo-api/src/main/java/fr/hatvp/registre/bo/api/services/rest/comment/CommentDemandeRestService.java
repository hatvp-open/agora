/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.comment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.backoffice.comment.CommentDemandeBoService;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;

/**
 * Classe de controlleur pour les commentaire des demandes.
 *
 *
 * @version $Revision$ $Date${0xD}
 */

@RestController
@RequestMapping("/comment/demande")
public class CommentDemandeRestService {

	/** Service des commentaires des demandes. */
	@Autowired
	private CommentDemandeBoService commentDemandeBoService;

	/**
	 *
	 * @param demandeId
	 *            id de la demande.
	 * @return liste des commentaires de la demande.
	 */
	@GetMapping("/all/{demandeId}")
	public ResponseEntity<List<CommentBoDto>> getCommentByDemandeId(@PathVariable final Long demandeId) {
		return ResponseEntity.ok().body(this.commentDemandeBoService.getAllComment(demandeId));
	}

	/**
	 *
	 * @param comment
	 *            commentaire à sauvegarder.
	 * @return commentaire sauvegardé.
	 */
	@PostMapping
	public ResponseEntity<CommentBoDto> saveComment(@RequestBody final CommentBoDto comment) {
		return new ResponseEntity<>(this.commentDemandeBoService.addComment(comment), HttpStatus.CREATED);
	}

	/**
	 * Modifier un commentaire.
	 *
	 * @param commentId
	 *            id du commentaire.
	 */
	@PutMapping("/{commentId}")
	ResponseEntity<CommentBoDto> updateComment(@PathVariable final Long commentId, @RequestBody final CommentBoDto comment) {
		return new ResponseEntity<>(commentDemandeBoService.updateComment(comment), HttpStatus.OK);
	}

	/**
	 * Supprimer un commentaire.
	 * 
	 * @param commentId
	 *            id du commentaire.
	 */
	@DeleteMapping("/{commentId}")
	public ResponseEntity<Void> deleteComment(@PathVariable final Long commentId) {
		this.commentDemandeBoService.delete(commentId);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
