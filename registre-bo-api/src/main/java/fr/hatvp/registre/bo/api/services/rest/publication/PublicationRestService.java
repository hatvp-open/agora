/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.publication;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.PublicationExerciceService;
import fr.hatvp.registre.business.PublicationService;
import fr.hatvp.registre.commons.dto.publication.PublicationBucketDto;
import fr.hatvp.registre.commons.dto.publication.PublicationRechercheDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketExerciceDto;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;

/**
 * Cette classe permet de visualiser le rendu des publications au format JSON.
 * Elle est utilisée à des fins de test pour la recette de l'application.
 * 
 * FIXME : à supprimer car inutile a priori depuis la mise en place d'efinitive de la publication.
 * 
 *
 */
@RestController
@RequestMapping("/publication")
public class PublicationRestService
{

	/**
	 * Service pour la publication
	 */
	@Autowired
	private PublicationService publicationService;

    @Autowired
    private PublicationExerciceService publicationExerciceService;
	/**
	 * API pour tester la liste des catégories publiées.
	 * @return tableau JSON
	 */
    @GetMapping(value="/organisations", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getTypesOrganisationsAsJson()
    {
        return ResponseEntity.ok(TypeOrganisationEnum.typesOrganisationsAsJson());
    }

    /**
     * API pour tester le document de publication d'une espace collaboratif.
     * @param espaceId
     * @return la publication au format JSON
     */
    @GetMapping("/espace/{espaceId}")
    public ResponseEntity<PublicationBucketDto> getPublicationByEspaceIdAsJson(@PathVariable final Long espaceId)
    {
    	if(espaceId == null) {
    		return ResponseEntity.badRequest().build();
    	} else {
    		return ResponseEntity.ok(publicationService.getPublicationLivrable(espaceId));
    	}
    }

    /**
     * API pour tester le document de publication d'un exercice.
     * @param exerciceId
     * @return la publication de l'exercice au format JSON
     */
    @GetMapping("/exercice/{exerciceId}")
    public ResponseEntity<PublicationBucketExerciceDto> getPublicationByExerciceIdAsJson(@PathVariable final Long exerciceId)
    {
        if(exerciceId == null) {
            return ResponseEntity.badRequest().build();
        } else {
            return ResponseEntity.ok(publicationExerciceService.getPublicationLivrableExercice(exerciceId,true));
        }
    }

    /**
     * API pour tester le document de publication pour la recherche des organisations.
     * @return le document au format JSON
     */
    @GetMapping("/recherche")
    public ResponseEntity<List<PublicationRechercheDto>> getPublicationRechercheAsJson()
    {
   		return ResponseEntity.ok(publicationService.getPublicationsRechercheLivrable());
    }
}