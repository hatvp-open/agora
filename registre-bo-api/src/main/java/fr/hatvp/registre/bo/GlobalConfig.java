/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *
 * Cette classe permet la configuration globale de l'application.
 *
 * @version $Revision$ $Date${0xD}
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "fr.hatvp.registre")
@PropertySource(value = "classpath:application.properties", encoding = "UTF-8")
@ImportResource("classpath:applicationContext-jpa.xml")
public class GlobalConfig extends WebMvcConfigurerAdapter {
	/**
	 * Méthode permettant de configurer le Bean des placeHolders
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	/**
	 * Méthode permettant de configurer le Bean de téléchargement de fichier.
	 */
	@Bean
	public CommonsMultipartResolver multipartResolver() {
		final CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setDefaultEncoding("UTF-8");
		return multipartResolver;
	}

	/**
	 * Cette méthode permet de configurer le codeur du mot de passe.
	 * 
	 * @return L'objet permettant de crypter le mot de passe.
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}