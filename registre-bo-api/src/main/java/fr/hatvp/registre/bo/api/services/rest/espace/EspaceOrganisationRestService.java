/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.espace;

import static fr.hatvp.registre.commons.lists.IdentifiantCardInfosGeneralesEnum.PROFILE_ORGA;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.hatvp.registre.bo.api.security.AccountUserDetails;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.PublicationService;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.exceptions.InvalidRequestException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.lists.IdentifiantCardInfosGeneralesEnum;


/**
 * Controlleur qui permet de gérer l'espace organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate")
public class EspaceOrganisationRestService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EspaceOrganisationRestService.class);

	/** Service pour Espace Organisation */
	@Autowired
	private EspaceOrganisationService espaceOrganisationService;

	@Autowired
    private PublicationService publicationService;

	/**
	 * Mettre à jour les informations générales pour une organisation.
	 *
	 * @param cardId
	 *            identifiant de la card côté front
	 * @param espaceOrganisation
	 *            DTO de l'espace organisation
	 *            l'identifiant de l'espace courant en session à modifier
	 * @param bindingResult
	 *            objet bindings jackson
	 * @return 202 accepted
	 */
	@PutMapping("/update/{cardId}")
	@PreAuthorize("hasRole('ROLE_GESTION_ESPACES_COLLABORATIFS_VALIDES')")
	public ResponseEntity<EspaceOrganisationDto> updateData(@PathVariable final IdentifiantCardInfosGeneralesEnum cardId,
			@RequestBody @Valid final EspaceOrganisationDto espaceOrganisation, final BindingResult bindingResult) {

		EspaceOrganisationDto espaceOrganisationDto = null;

		// Validations de surface selon carte validée et MAJ données de la carte
		switch (cardId) {
			case PROFILE_ORGA:
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("MAJ Carte Infos Générales: {}" , PROFILE_ORGA.toString());
				}
				if (espaceOrganisation.getId() == null) {
					throw new InvalidRequestException(ErrorMessageEnum.DONNEES_INCORRECTES.getMessage(), bindingResult);
				}
	
				espaceOrganisationDto = this.espaceOrganisationService.updateProfileOrga(espaceOrganisation, true);
				break;
			default:
				break;	
		}
		return ResponseEntity.accepted().body(espaceOrganisationDto);
	}
	
    /**
     * Bloquer les relances d'un espace
     * @param espaceId 
     * @param authentication objet spring security d'authentification
     * @return response
     */
    @PutMapping("/{espaceId}/bloquer_relances")
    public ResponseEntity<Void> bloquerRelances(@PathVariable final Long espaceId, final Authentication authentication)
    {
        final Long userId = ((AccountUserDetails) authentication.getPrincipal()).getId();

        this.espaceOrganisationService.bloquerRelances(espaceId, userId);

        return ResponseEntity.accepted().build();
    }
    
    /**
     * Debloquer les relances d'un espace
     * @param espaceId 
     * @param authentication objet spring security d'authentification
     * @return response
     */
    @PutMapping("/{espaceId}/debloquer_relances")
    public ResponseEntity<Void> debloquerRelances(@PathVariable final Long espaceId, final Authentication authentication)
    {
        final Long userId = ((AccountUserDetails) authentication.getPrincipal()).getId();

        this.espaceOrganisationService.debloquerRelances(espaceId, userId);

        return ResponseEntity.accepted().build();
    }
    
    /**
	 * Chargement des fichiers relatifs au nouveau CO
	 *
	 * @param file1
	 *            fichier de pièce d'identité à charger.
	 * @param file2
	 *            fichier de mandat à charger.
	 * @param file3
	 *            justificatif de mandat à charger.
	 * @return http status 200 si bien chargé et données enregistrées, 404: si des éléments sont introuvable dans la requête
	 */
    @PostMapping("/addCo")
	public ResponseEntity<Void> ajoutCO(@RequestParam(required = false) final MultipartFile file1, @RequestParam(required = false) final MultipartFile file2,
			@RequestParam(required = false) final MultipartFile file3,@RequestParam final Long espaceOrganisationId,@RequestParam final Long declarantId, @RequestParam final String messageConfirmation) {
		this.espaceOrganisationService.ajouterContactOperationnel(file1,file2,file3,espaceOrganisationId,declarantId,messageConfirmation);
		return ResponseEntity.accepted().build();
	}

    /**
     * Retourne 0 si aucune publi, 1 si publi identité, 2 si publi activite
     */
    @GetMapping("/hasPublished/{espaceId}")
    public ResponseEntity<Integer> hasPublished (@PathVariable long espaceId){
        Integer published = this.publicationService.hasPublishedWithEspaceId(espaceId);
        return ResponseEntity.ok(published);
    }

}
