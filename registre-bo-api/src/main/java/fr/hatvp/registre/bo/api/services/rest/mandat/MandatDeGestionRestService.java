/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.mandat;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.DemandeChangementMandantService;
import fr.hatvp.registre.commons.dto.DemandeChangementMandantDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.lists.backoffice.MotifComplementEspaceEnum;

/**
 * Fournit les ressources REST pour le traitement des mandats de gestion
 *
 */
@RestController
@RequestMapping("/mandats")
@PreAuthorize("hasRole('ROLE_GESTION_MANDATS')")
public class MandatDeGestionRestService {

	@Autowired
	private DemandeChangementMandantService demandeChangementMandantService;

	/**
	 * Récupérer la liste des demande de changement des mandats de gestion
	 *
	 * @return liste des demandes de changement des mandats de gestion
	 */
	@GetMapping("/page/{pageNumber}/occurrence/{resultPerPage}")
	public ResponseEntity<PaginatedDto<DemandeChangementMandantDto>> getDemandesChangementMandantEnCours(@PathVariable final Integer pageNumber, @PathVariable final Integer resultPerPage) {

		final PaginatedDto<DemandeChangementMandantDto> changementMandantDtos = this.demandeChangementMandantService.getPaginatedDemandesChangementMandant(pageNumber, resultPerPage);
		return ResponseEntity.ok(changementMandantDtos);
	}

	/**
	 * Récupérer une page spécifique de la liste Paginée
	 *
	 * @param pageNumber
	 * @param resultPerPage
	 * @return une page de la liste des dtos
	 */
	@GetMapping("/{type}/{keywords}/page/{page_no}/{result_per_page}")
	@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
	public ResponseEntity<PaginatedDto<DemandeChangementMandantDto>> getEspacesEnAttenteParRecherche(@PathVariable("type") final String type, @PathVariable("keywords") final String keywords,
			@PathVariable("page_no") final Integer pageNumber, @PathVariable("result_per_page") final Integer resultPerPage) {
		return ResponseEntity.ok(this.demandeChangementMandantService.getDemandesParRecherche(type, keywords, pageNumber, resultPerPage));
	}

	/**
	 * Récupérer une demande de changement de mandat de gestion
	 *
	 * @param id
	 *            demande de changement de mandat de gestion
	 * @return une demande de changement de mandat de gestion
	 */
	@GetMapping("/{id}")
	public ResponseEntity<DemandeChangementMandantDto> getDemandeChangementMandantById(@PathVariable final Long id) {

		DemandeChangementMandantDto changementMandantDto = this.demandeChangementMandantService.getDemandeChangementMandant(id);
		return ResponseEntity.ok(changementMandantDto);
	}

	/**
	 * Récupérer la liste des demande de changement des mandats de gestion par espace organisation Utilisé par le back-office
	 *
	 * @param id
	 *            espace organisation
	 * @return une liste des demande de changement de mandat de gestion pour un espace organisation
	 */
	@GetMapping("/organisation/{id}")
	public ResponseEntity<List<DemandeChangementMandantDto>> getDemandesChangementMandantByEspaceOrganisation(@PathVariable Long id) {

		List<DemandeChangementMandantDto> changementMandantDtos = this.demandeChangementMandantService.getAllDemandesChangementByEspaceOrganisation(id);
		return ResponseEntity.ok(changementMandantDtos);
	}

	/**
	 * Refuser une demande de changement de mandant
	 *
	 * @param id
	 *            demande de changement de mandat de gestion
	 * @return reponse http 200
	 */
	@PutMapping("/refuser/{id}")
	public ResponseEntity<Void> refuserDemandeChangementMandant(@PathVariable final Long id) {

		this.demandeChangementMandantService.rejeterDemande(id);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Accepter une demande de changement de mandant
	 *
	 * @param id
	 *            demande de changement de mandat de gestion
	 * @return reponse http 200
	 */
	@PutMapping("/valider/{id}")
	public ResponseEntity<Void> validerDemandeChangementMandant(@PathVariable final Long id) {

		this.demandeChangementMandantService.validerDemande(id);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Demander des pièces complémentaires pour la création d'un espace collaboratif.
	 *
	 * @param id
	 *            demande de changement de mandat de gestion
	 * @param corpsEmail
	 *            l'email à envoyer
	 */
	@PostMapping("/complement/{id}")
	public ResponseEntity<Void> complement(@PathVariable final Long id, @RequestBody final String corpsEmail) {
		this.demandeChangementMandantService.demandeComplementInformation(id, corpsEmail);
		return ResponseEntity.accepted().build();
	}

	/**
	 * Récupérer le template de l'email pour demande de pièces complémentaires.
	 *
	 * @param id
	 *            demande de changement de mandat de gestion
	 * @return Contenu du telmplate de demande de complément.
	 */
	@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
	@GetMapping(path = "/{id}/demandeComplementTemplate/{codeMotifComplement}", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> getDemandeComplementTemplate(@PathVariable final Long id, @PathVariable final MotifComplementEspaceEnum codeMotifComplement) {
		return ResponseEntity.ok(this.demandeChangementMandantService.generateDemandeComplementMessage(id, codeMotifComplement));
	}

}
