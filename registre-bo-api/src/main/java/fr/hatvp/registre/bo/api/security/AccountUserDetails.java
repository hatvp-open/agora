/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.security;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;

/**
 *
 * Cette classe permet d'initialiser les données de connexion.
 *
 * @version $Revision$ $Date${0xD}
 */
@Component
public class AccountUserDetails
    implements MyUserDetails
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -7307537566163438986L;

    /** Données de l'utilisateur connecté. */
    private final UtilisateurBoDto account;

    /** LOGGER de la classe. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountUserDetails.class);

    /** initialisation de l'objet contenant les données de l'utilisateur connecté. */
    AccountUserDetails(final UtilisateurBoDto account)
    {
        this.account = account;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        final ArrayList<GrantedAuthority> authorities = new ArrayList<>();

        this.account.getRoles()
                .forEach(r -> authorities.add(new SimpleGrantedAuthority("ROLE_" + r.name())));

        authorities.forEach(grantedAuthority -> LOGGER
            .info("Utilisateur (BO) connecté avec les roles : {}", grantedAuthority.getAuthority()));

        return authorities;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPassword()
    {
        return this.account.getPassword();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUsername()
    {
        return this.account.getEmail();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAccountNonLocked()
    {
        return !this.account.getCompteSupprime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEnabled()
    {
        return this.account.getCompteActive();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getId()
    {
        return this.account.getId();
    }

}
