/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.utilisateur;

import javax.validation.Valid;

import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoGestionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import fr.hatvp.registre.business.backoffice.UtilisateurBoService;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;

/**
 * Classe de mise à jours des utilisateurs.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/account")
@PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
public class UpdateUtilisateurRestService
{

    /**
     * Service de l'utilisateur backoffice.
     */
    private UtilisateurBoService utilisateurBoService;

    /**
     * Initialisation du service.
     *
     * @param utilisateurBoService services des utilisateurs bo.
     */
    @Autowired
    public void setUtilisateurBoService(final UtilisateurBoService utilisateurBoService)
    {
        this.utilisateurBoService = utilisateurBoService;
    }

    /**
     * Mise à jour des données utulisateur.
     *
     * @param utilisateurBoDto objet contenant l'email à mettre à jour.
     * @return utilisateur mis à jour.
     */
    @PutMapping
    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    public ResponseEntity<UtilisateurBoDto> updateAccount(
            @RequestBody @Valid final UtilisateurBoDto utilisateurBoDto)
    {

        final UtilisateurBoDto updatedUtilisateurBoDto = this.utilisateurBoService
                .majUtilisateurBo(utilisateurBoDto);

        updatedUtilisateurBoDto.setPassword(null);
        return new ResponseEntity<>(updatedUtilisateurBoDto, HttpStatus.ACCEPTED);
    }

    /**
     * Activer et désactiver le compte utilisateur.
     *
     * @param id id du compte utilisateur.
     */
    @PutMapping("/activate/{id}")
    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    public ResponseEntity<UtilisateurBoDto> activateDeactivateAccount(@PathVariable final Long id)
    {

        final UtilisateurBoDto utilisateurBoDto = this.utilisateurBoService.findOne(id);
        return new ResponseEntity<>(
                this.utilisateurBoService.switchActivationCompte(utilisateurBoDto),
                HttpStatus.ACCEPTED);
    }

    /**
     * Générer un nouveau mot de passe utilisateur.
     *
     * @param id id du compte utilisateur.
     */
    @PutMapping("/password/{id}")
    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    public ResponseEntity<UtilisateurBoGestionDto> generateNewPassword(@PathVariable final Long id)
    {

        final UtilisateurBoGestionDto utilisateurBoDto = this.utilisateurBoService.updatePassword(id);

        return new ResponseEntity<>(utilisateurBoDto, HttpStatus.ACCEPTED);
    }

}
