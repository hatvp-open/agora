/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.espace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.bo.api.security.CurrentUserId;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceBoSimpleDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceDetailBoDto;
import fr.hatvp.registre.commons.lists.backoffice.MotifComplementEspaceEnum;
import fr.hatvp.registre.commons.lists.backoffice.MotifRefusEspaceEnum;

/**
 * Cette classe permet d'accéder aux services de gestion des demandes de création d'espace.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate")
public class ValidationEspaceRestService {

	/** Service des espaces corporate. */
	@Autowired
	private EspaceOrganisationService espaceOrganisationService;

	/**
	 * Récupérer une page spécifique de la liste Paginée des demandes de création d'espace pas encore validées
	 *
	 * @param pageNumber
	 * @param resultPerPage
	 * @return une page de la liste des dtos
	 */
	@GetMapping("/pending/page/{page_no}/{result_per_page}")
	@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
	public ResponseEntity<PaginatedDto<ValidationEspaceBoSimpleDto>> getEspacesEnAttente(@PathVariable("page_no") final Integer pageNumber,
			@PathVariable("result_per_page") final Integer resultPerPage) {
		return ResponseEntity.ok(this.espaceOrganisationService.getEspacesOrganisationEnAttente(pageNumber, resultPerPage));
	}

	/**
	 * Récupérer un espace organisation non validé par id
	 *
	 * @param id
	 *            identifiant de l'espace corporate
	 * @return dto de l'espace corporate
	 */
	@GetMapping("/pending/{id}")
	@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
	public ResponseEntity<ValidationEspaceDetailBoDto> getEspaceEnCoursDeValidation(@PathVariable final Long id) {
		return ResponseEntity.ok(this.espaceOrganisationService.getEspaceOrganisationEnCoursDeValidation(id));
	}

	/**
	 * Récupérer le contenu du template de rejet à partir de l'id du motif envoyé en paramètre.
	 *
	 * @param id
	 *            id de l'espace organisation.
	 * @param idMotif
	 *            id du motif.
	 * @return Contenu du telmplate de rejet.
	 */
	@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
	@GetMapping(path = "/pending/{id}/motif/{idMotif}", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> getMotifRefusEspace(@PathVariable final Long id, @PathVariable final int idMotif) {
		return ResponseEntity.ok(this.espaceOrganisationService.generateRejectMessage(id, MotifRefusEspaceEnum.getMotifRefusEspaceEnumById(idMotif)));
	}

	/**
	 * Récupérer le template de l'email pour demande de pièces complémentaires.
	 *
	 * @param id
	 *            id de l'espace organisation.
	 * @return Contenu du telmplate de demande de complément.
	 */
	@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
	@GetMapping(path = "/pending/{id}/demandeComplementTemplate/{codeMotifComplement}", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> getDemandeComplementTemplate(@PathVariable final Long id, @PathVariable final MotifComplementEspaceEnum codeMotifComplement) {
		return ResponseEntity.ok(this.espaceOrganisationService.generateDemandeComplementMessage(id, codeMotifComplement));
	}

	/**
	 * Mettre à jour les informations de l'organisation de l'espace
	 *
	 * @return dto de l'espace corporate mis à jour
	 */
	@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
	@PutMapping("/pending")
	public ResponseEntity<ValidationEspaceDetailBoDto> updateEspaceEnCoursDeValidation(@RequestBody final ValidationEspaceDetailBoDto dto, @CurrentUserId final Long currentUserId) {
		return ResponseEntity.ok(this.espaceOrganisationService.updateEspaceOrganisationEnCoursDeValidation(dto, currentUserId));
	}

	/**
	 * Valider une demande de création d'un nouvel espace corporate
	 *
	 * @param id
	 *            identifiant de l'espace corporate
	 */
	@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
	@PostMapping("/pending/{id}/accept")
	public ResponseEntity<Void> validateEspace(@PathVariable final Long id, @CurrentUserId final Long currentUserId) {
		this.espaceOrganisationService.validateCreationRequest(id, currentUserId);
		return ResponseEntity.accepted().build();
	}

	/**
	 * Refuser une demande de création d'un espace collaboratif.
	 *
	 * @param id
	 *            identifiant de l'espace collaboratif
	 */
	@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
	@PostMapping("/pending/{id}/reject")
	public ResponseEntity<Void> invalidateEspace(@PathVariable final Long id, @RequestBody final String motif, @CurrentUserId final Long currentUserId) {
		this.espaceOrganisationService.rejectCreationRequest(id, currentUserId, motif);
		return ResponseEntity.accepted().build();
	}

	/**
	 * Demander des pièces complémentaires pour la création d'un espace collaboratif.
	 *
	 * @param id
	 *            identifiant de l'espace collaboratif
	 * @param corpsEmail
	 *            l'email à envoyer au créateur de l'espace collaboratif
	 */
	@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
	@PostMapping("/pending/{id}/complement")
	public ResponseEntity<Void> complement(@PathVariable final Long id, @RequestBody final String corpsEmail) {
		this.espaceOrganisationService.demandeComplementInformation(id, corpsEmail);
		return ResponseEntity.accepted().build();
	}

	/**
	 * Récupérer une page spécifique de la liste PagINée des demandes de création d'espace pas encore validées
	 *
	 * @param pageNumber
	 * @param resultPerPage
	 * @return une page de la liste des dtos
	 */
	@GetMapping("/pending/{type}/{keywords}/page/{page_no}/{result_per_page}")
	@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
	public ResponseEntity<PaginatedDto<ValidationEspaceBoSimpleDto>> getEspacesEnAttenteParRecherche(@PathVariable("type") final String type, @PathVariable("keywords") final String keywords,
			@PathVariable("page_no") final Integer pageNumber, @PathVariable("result_per_page") final Integer resultPerPage) {
		return ResponseEntity.ok(this.espaceOrganisationService.getEspacesOrganisationEnAttenteParRecherche(type, keywords, pageNumber, resultPerPage));
	}
}
