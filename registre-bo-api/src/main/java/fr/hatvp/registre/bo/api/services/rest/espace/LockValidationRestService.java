/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.espace;

import fr.hatvp.registre.bo.api.security.AccountUserDetails;
import fr.hatvp.registre.business.backoffice.EspaceOrganisationLockBoService;
import fr.hatvp.registre.commons.dto.backoffice.EspaceOrganisationLockBoDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Cette classe permet de gérer le lock sur les des demandes de création d'espace.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate")
@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_MANAGER')")
public class LockValidationRestService {

    /**
     * Service des locks.
     */
    @Autowired
    private EspaceOrganisationLockBoService espaceOrganisationLockBoService;

    /**
     * Vérifier si un lock est appliqué sur un espace organisation.
     *
     * @param id id de l'espace organisation
     * @return objet lock sur l'espace s'il existe, null sinon
     */
    @GetMapping("/lock/{id}")
    public ResponseEntity<EspaceOrganisationLockBoDto> getLockOnEspace(@PathVariable final Long id) {
        return ResponseEntity.ok(this.espaceOrganisationLockBoService.getLockOn(id));
    }

    /**
     * Appliquer un lock sur un espace organisation.
     *
     * @param id id de l'espace organisation
     */
    @PostMapping("/lock/{id}")
    public ResponseEntity<Void> setLockOnEspace(@PathVariable final Long id,
                                                final Authentication authentication) {
        final Long connectedUserId = ((AccountUserDetails) authentication.getPrincipal()).getId();

        Optional.ofNullable(this.espaceOrganisationLockBoService.getLockOn(id))
            .ifPresent(espaceOrganisationLockBoDto -> {
                throw new BusinessGlobalException(
                    "Un lock est déjà en cours sur cet espace par "
                        + espaceOrganisationLockBoDto.getUtilisateurBoName()
                        + "! L'entité sera de nouveau accessible dans "
                        + espaceOrganisationLockBoDto.getLockTimeRemain()
                        + " secondes.");
            });

        this.espaceOrganisationLockBoService.setLockOn(id, connectedUserId);
        return ResponseEntity.accepted().build();
    }

    /**
     * Appliquer un lock sur un espace organisation.
     *
     * @param dto du lock de l'espace organisation
     */
    @PutMapping("/lock")
    public ResponseEntity<EspaceOrganisationLockBoDto> updateLockOnEspace(
        @RequestBody final EspaceOrganisationLockBoDto dto,
        final Authentication authentication) {
        final Long connectedUserId = ((AccountUserDetails) authentication.getPrincipal()).getId();

        return new ResponseEntity<>(this.espaceOrganisationLockBoService.updateLockOnEspace(dto, connectedUserId),
            HttpStatus.ACCEPTED);
    }
}
