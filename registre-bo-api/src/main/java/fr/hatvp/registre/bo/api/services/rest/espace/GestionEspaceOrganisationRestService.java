/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.espace;

import java.util.List;

import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.dto.CollaborateurDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceBoDto;
import fr.hatvp.registre.commons.dto.backoffice.ConsultationEspaceBoSimpleDto;

/**
 * Cette classe permet de gérer les espaces organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate")
@PreAuthorize("hasRole('ROLE_GESTION_ESPACES_COLLABORATIFS_VALIDES')")
public class GestionEspaceOrganisationRestService {

	/** Service des espaces corporate. */
	private final EspaceOrganisationService espaceOrganisationService;

	/** Initialisation du service. */
	@Autowired
	public GestionEspaceOrganisationRestService(final EspaceOrganisationService espaceOrganisationService) {
		this.espaceOrganisationService = espaceOrganisationService;
	}


	/**
	 * Récupérer d'un espace organisation validé.
	 *
	 * @return un espace organisation.
	 */
	@GetMapping("/confirmed/{id}")
	public ResponseEntity<ValidationEspaceBoDto> getConfirmedEspaceOrganizationById(@PathVariable Long id) {
		return ResponseEntity.ok(this.espaceOrganisationService.getEspacesOrganisationConfirmeById(id));
	}

	/**
	 * Récupérer une page de liste des demandes de création d'espace organisation validés.
	 *
	 * @param pageNumber
	 * @param resultPerPage
	 * @return liste des espaces organisation.
	 */
	@GetMapping("/confirmed/page/{page_no}/{result_per_page}")
	public ResponseEntity<PaginatedDto<ConsultationEspaceBoSimpleDto>> getConfirmedEspaceOrganization(@PathVariable("page_no") final Integer pageNumber,
			@PathVariable("result_per_page") final Integer resultPerPage) {
		return ResponseEntity.ok(this.espaceOrganisationService.getEspacesOrganisationsConfirmes(pageNumber, resultPerPage));
	}

	/**
	 * Récupérer une page spécifique de la liste PagINée des demandes de création d'espace pas encore validées
	 *
	 * @param pageNumber
	 * @param resultPerPage
	 * @return une page de la liste des dtos
	 */
	@GetMapping("/confirmed/{type}/{keywords}/page/{page_no}/{result_per_page}")
	public ResponseEntity<PaginatedDto<ConsultationEspaceBoSimpleDto>> getConfirmedEspaceOrganizationParRecherche(@PathVariable("type") final String type,
			@PathVariable("keywords") final String keywords, @PathVariable("page_no") final Integer pageNumber, @PathVariable("result_per_page") final Integer resultPerPage) {
		return ResponseEntity.ok(this.espaceOrganisationService.getEspacesOrganisationsConfirmesParRecherche(type, keywords, pageNumber, resultPerPage));
	}

	/**
	 * Activer / Désactiver l'espace organisation.
	 *
	 * @param espaceId
	 *            id de l'espace organisation.
	 */
	@PreAuthorize("hasRole('ROLE_RESTRICTION_ACCES')")
	@PutMapping("/switch/activation/{espaceId}")
	public ResponseEntity<ValidationEspaceBoDto> switchValidationEspaceOrganization(@PathVariable final Long espaceId) {
		return new ResponseEntity<>(this.espaceOrganisationService.switchActivationStatus(espaceId), HttpStatus.ACCEPTED);
	}

	/**
	 * Récupérer liste des déclarants de l'espace organisation.
	 *
	 * @return liste des déclarants.
	 */
	@GetMapping("/declarant/{espaceId}")
	public ResponseEntity<List<InscriptionEspaceDto>> getDeclarantEspaceOrganization(@PathVariable final Long espaceId) {
		return ResponseEntity.ok(this.espaceOrganisationService.getEspaceOrganisationDeclarant(espaceId));
	}

    @GetMapping("/client/{espaceId}")
    public ResponseEntity<List<AssoClientDto>> getClientsEspaceOrganisation (@PathVariable final Long espaceId){
	    return ResponseEntity.ok(this.espaceOrganisationService.getEspaceOrganisationClients(espaceId));
    }

    @GetMapping("/collaborateur/{espaceId}")
    public ResponseEntity<List<CollaborateurDto>> getCollaborateursEspaceOrganisation (@PathVariable final Long espaceId){
        return ResponseEntity.ok(this.espaceOrganisationService.getEspaceOrganisationCollaborateurs(espaceId));
    }
    
    @GetMapping("/affiliation/{espaceId}")
    public ResponseEntity<List<AssoClientDto>> getAffiliationsEspaceOrganisation (@PathVariable final Long espaceId){
        return ResponseEntity.ok(this.espaceOrganisationService.getEspaceOrganisationAffiliations(espaceId));
    }

}
