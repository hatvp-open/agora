/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.exceptions;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import fr.hatvp.registre.commons.dto.backoffice.erreur.ErrorResource;
import fr.hatvp.registre.commons.dto.erreur.ErrorResourceDto;
import fr.hatvp.registre.commons.dto.erreur.FieldErrorResourceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;

/**
 * Clase de gestion globale des exceptions.
 *
 * @version $Revision$ $Date${0xD}
 */
@ControllerAdvice
public class RegistreBoExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(RegistreBoExceptionHandler.class);

	/**
	 * Cette méthode permet de gérer les exceptions d'unicité {@link NonUniqueResultException}.
	 *
	 * @return http status 409.
	 */
	@ExceptionHandler(EntityExistsException.class)
	public ResponseEntity<ErrorResource> notUniqueException(final EntityExistsException e) {
		LOGGER.error(e.getMessage(), e);
		final ErrorResource errorResource = new ErrorResource();
		errorResource.setCode(HttpServletResponse.SC_CONFLICT);
		errorResource.setMessage(e.getMessage());
		return new ResponseEntity<>(errorResource, HttpStatus.CONFLICT);
	}

	/**
	 * Cette méthode renvoie 404 dans le cas de {@link EntityNotFoundException} {@link EntityNotFoundException}.
	 *
	 * @return http status 404.
	 */
	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<ErrorResource> entityNotFound(final EntityNotFoundException e) {
		LOGGER.error(e.getMessage(), e);
		final ErrorResource errorResource = new ErrorResource();
		errorResource.setCode(HttpServletResponse.SC_NOT_FOUND);
		errorResource.setMessage(e.getMessage());
		return new ResponseEntity<>(errorResource, HttpStatus.NOT_FOUND);
	}

	/**
	 * Cette méthode renvoie 400 dans le cas de {@link BusinessGlobalException}
	 *
	 * @return http status 400.
	 */
	@ExceptionHandler(BusinessGlobalException.class)
	public ResponseEntity<ErrorResource> businessGlobalException(final BusinessGlobalException e) {
		LOGGER.error(e.getMessage(), e);
		final ErrorResource errorResource = new ErrorResource();
		errorResource.setCode(HttpServletResponse.SC_BAD_REQUEST);
		errorResource.setMessage(e.getMessage());
		return new ResponseEntity<>(errorResource, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Méthode Générique pour tous les {@link RuntimeException} non traitées.
	 *
	 * @return http status 400.
	 */
	@ExceptionHandler(OptimisticLockingFailureException.class)
	public ResponseEntity<ErrorResource> optimisticLockingException(final OptimisticLockingFailureException e) {
		LOGGER.error(e.getMessage(), e);
		final ErrorResource errorResource = new ErrorResource();
		errorResource.setCode(HttpServletResponse.SC_PRECONDITION_FAILED);
		errorResource.setMessage(e.getMessage());
		return new ResponseEntity<>(errorResource, HttpStatus.PRECONDITION_FAILED);
	}

	/**
	 * Méthode Générique pour tous les {@link RuntimeException} non traitées.
	 *
	 * @return http status 400.
	 */
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ErrorResource> allRuntimeExceptions(final RuntimeException e) {
		LOGGER.error(e.getMessage(), e);
		final ErrorResource errorResource = new ErrorResource();
		errorResource.setCode(HttpServletResponse.SC_BAD_REQUEST);
		errorResource.setMessage(e.getMessage());
		return new ResponseEntity<>(errorResource, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Méthode Générique pour tous les {@link PersistenceException} non traitées.
	 *
	 * @return http status 503.
	 */
	@ExceptionHandler(PersistenceException.class)
	public ResponseEntity<ErrorResource> allPersistenceExceptions(final PersistenceException e) {
		LOGGER.error(e.getMessage(), e);
		final ErrorResource errorResource = new ErrorResource();
		errorResource.setCode(HttpServletResponse.SC_CONFLICT);
		errorResource.setMessage(e.getMessage());
		return new ResponseEntity<>(errorResource, HttpStatus.CONFLICT);
	}

	/**
	 * Méthode de traitement des erreurs de validations.
	 *
	 * @return http status 400.
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorResourceDto processMethodArgumentNotValidError(MethodArgumentNotValidException ex) {
		LOGGER.error(ex.getMessage(), ex);
		List<FieldErrorResourceDto> fieldErrors = ex.getBindingResult().getFieldErrors().stream().map(error -> {
			return new FieldErrorResourceDto(error.getObjectName(), error.getField(), error.getCode(), error.getDefaultMessage());
		}).collect(Collectors.toList());
		// TODO modifier le message de ErrorResourceDto: deuxième paramètre
		return new ErrorResourceDto("Erreur de validation", "Erreur de validation", fieldErrors);
	}

	/**
	 * Méthode de traitement des erreurs de json invalide lors d'une requête.
	 *
	 * @return http status 400.
	 */
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorResourceDto processHttpMessageNotReadableNotReadableError(HttpMessageNotReadableException ex) {
		return new ErrorResourceDto("Erreur: requête HTTP mal formulé", ex.getMessage());
	}

}
