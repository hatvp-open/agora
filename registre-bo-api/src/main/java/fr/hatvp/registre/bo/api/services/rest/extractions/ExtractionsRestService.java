/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.extractions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.tika.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.backoffice.ExtractionService;
import fr.hatvp.registre.commons.dto.ChiffresSurUnePeriodeDto;

/**
 * Cette classe permet de telecharger les extractions
 *
 * @version $Revision$ $Date${0xD}
 */



@RestController
@RequestMapping("/extractions")
public class ExtractionsRestService
{
    private static final String FICHIER1 = "1_Liste_des_espaces_collaboratifs.csv";
    private static final String FICHIER2 = "2_Liste_des_fiches_activites.csv";
    private static final String FICHIER3 = "3_Liste_des_comptes_personnels_crees.csv";
    private static final String FICHIER4 = "4_Liste_des_demandes_ajout_referentiel.csv";
    private static final String FICHIER5 = "5_nombre_fiches_activites.csv";
    
    @Autowired
	private ExtractionService extractionService;

    /**
     * Télécharger le fichier .csv d'une extraction
     *
     * @return le fichier .csv
     */
    @GetMapping(path = "/download/{numero}")
    @PreAuthorize("hasRole('DATA_DOWNLOADER')")
    public ResponseEntity<Void> downloadFile(@PathVariable final int numero, HttpServletResponse response) throws IOException {
        File file = null;

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();

        switch(numero) {
            case 1:
                file = new File(Objects.requireNonNull(classloader.getResource(FICHIER1)).getFile());
                break;
            case 2:
                file = new File(Objects.requireNonNull(classloader.getResource(FICHIER2)).getFile());
                break;
            case 3:
                file = new File(Objects.requireNonNull(classloader.getResource(FICHIER3)).getFile());
                break;
            case 4:
                file = new File(Objects.requireNonNull(classloader.getResource(FICHIER4)).getFile());
                break;
            case 5:
                file = new File(Objects.requireNonNull(classloader.getResource(FICHIER5)).getFile());
                break;

                default:
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        if(!file.exists()){
            String errorMessage = "Le fichier n'existe pas !";
            OutputStream outputStream = response.getOutputStream();
            outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")));
            outputStream.close();
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        String mimeType= URLConnection.guessContentTypeFromName(file.getName());
        if(mimeType==null){
            mimeType = "application/octet-stream";
        }

        response.reset();
        response.resetBuffer();
        response.setContentType(mimeType);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
        response.setContentLength((int)file.length());

        try (InputStream inputStream = new FileInputStream(file)) {
            OutputStream out = response.getOutputStream();
            IOUtils.copy(inputStream, out);
            out.flush();
            out.close();
        }
        response.flushBuffer();
        return ResponseEntity.ok().build();
    }


    /**
     * Télécharger l'archive des 5 fichiers
     *
     * @return une archive zip contenant les 5 fichiers
     */
    @GetMapping(path = "/download/all")
    @PreAuthorize("hasRole('DATA_DOWNLOADER')")
    public ResponseEntity<Void> downloadAll(HttpServletResponse response) throws IOException {

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();

        //setting headers
        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader("Content-Disposition", "attachment; filename=\"extractions_agora.zip\"");

        ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream());

        ArrayList<File> files = new ArrayList<>(5);
        files.add(new File(Objects.requireNonNull(classloader.getResource(FICHIER1)).getFile()));
        files.add(new File(Objects.requireNonNull(classloader.getResource(FICHIER2)).getFile()));
        files.add(new File(Objects.requireNonNull(classloader.getResource(FICHIER3)).getFile()));
        files.add(new File(Objects.requireNonNull(classloader.getResource(FICHIER4)).getFile()));
        files.add(new File(Objects.requireNonNull(classloader.getResource(FICHIER5)).getFile()));

        for (File file : files) {
            zipOutputStream.putNextEntry(new ZipEntry(file.getName()));
            FileInputStream fileInputStream = new FileInputStream(file);

            IOUtils.copy(fileInputStream, zipOutputStream);

            fileInputStream.close();
            zipOutputStream.closeEntry();
        }

        zipOutputStream.close();

        response.flushBuffer();
        return ResponseEntity.ok().build();
    }

    @PutMapping("/download/periode")
    @PreAuthorize("hasRole('DATA_DOWNLOADER')")
	public ResponseEntity<ChiffresSurUnePeriodeDto> getChiffreDeLaPeriode(@RequestBody @Valid ChiffresSurUnePeriodeDto chiffresSurUnePeriodeDto) throws ParseException {//faut +1 sur date fin
    	ChiffresSurUnePeriodeDto chiffresSurUnePeriodeDtoRetour = extractionService.getChiffreSurPeriode(chiffresSurUnePeriodeDto);
		return ResponseEntity.accepted().body(chiffresSurUnePeriodeDtoRetour);
	}
}