/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.nomenclature;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.NomenclatureService;
import fr.hatvp.registre.commons.dto.nomenclature.NomenclatureDto;

@RestController
@RequestMapping("/nomenclature/declaration")
public class NomenclatureRestService {

	@Autowired
	private NomenclatureService nomenclatureService;

	@GetMapping("/all")
	private ResponseEntity<NomenclatureDto> getNomenclature() {

		return ResponseEntity.ok().body(nomenclatureService.getNomenclatures());
	}
}
