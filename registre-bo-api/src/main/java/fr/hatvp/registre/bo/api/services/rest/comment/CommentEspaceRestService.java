/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.comment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.backoffice.comment.CommentEspaceOrganisationBoService;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;

/**
 * Classe de controlleur pour les commentaire des espace organisations.
 *
 *
 * @version $Revision$ $Date${0xD}
 */

@RestController
@RequestMapping("/comment/espace")
public class CommentEspaceRestService {

	/** Serice des commentaires des espaces organisations. */
	private final CommentEspaceOrganisationBoService commentEspaceOrganisationBoService;

	/** Initialisaztion du service. */
	@Autowired
	public CommentEspaceRestService(CommentEspaceOrganisationBoService commentEspaceOrganisationBoService) {
		this.commentEspaceOrganisationBoService = commentEspaceOrganisationBoService;
	}

	/**
	 *
	 * @param espaceOrganisation
	 *            id de l'espace organisation.
	 * @return liste des commentaires du déclarant.
	 */
	@GetMapping("/all/{espaceOrganisation}")
	public ResponseEntity<List<CommentBoDto>> getCommentBydeclarantId(@PathVariable final Long espaceOrganisation) {
		return ResponseEntity.ok().body(commentEspaceOrganisationBoService.getAllComment(espaceOrganisation));
	}

	/**
	 *
	 * @param comment
	 *            commentaire à sauvegarder.
	 * @return commentaire sauvegardé.
	 */
	@PostMapping
	public ResponseEntity<CommentBoDto> saveComment(@RequestBody final CommentBoDto comment) {
		return new ResponseEntity<>(commentEspaceOrganisationBoService.addComment(comment), HttpStatus.CREATED);
	}

	/**
	 * Modifier un commentaire.
	 *
	 * @param commentId
	 *            id du commentaire.
	 */
	@PutMapping("/{commentId}")
	ResponseEntity<CommentBoDto> updateComment(@PathVariable final Long commentId, @RequestBody final CommentBoDto comment) {
		return new ResponseEntity<>(commentEspaceOrganisationBoService.updateComment(comment), HttpStatus.OK);
	}

	/**
	 * Supprimer un commentaire.
	 *
	 * @param commentId
	 *            id du commentaire.
	 */
	@DeleteMapping("/{commentId}")
	public ResponseEntity<Void> deleteComment(@PathVariable final Long commentId) {
		commentEspaceOrganisationBoService.delete(commentId);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
