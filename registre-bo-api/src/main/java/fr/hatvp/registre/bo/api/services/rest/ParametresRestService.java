/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.backoffice.ParametresService;
import fr.hatvp.registre.commons.dto.backoffice.ParametresDto;

/**
 * Cette classe permet de modifier les parametres du mode
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/limitation")
public class ParametresRestService
{
	@Autowired ParametresService parametresService;

	/**
	 * activer ou desactiver le mode.
	 *
	 * @return msg mode activé ou non
	 */
	@GetMapping("/activation/switch")
	@PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
	public ResponseEntity<String> setActivation() {
		Boolean state = parametresService.switchActivation();
		return ResponseEntity.ok("Mode activé : " + (state?"Oui":"Non"));
	}

	/**
	 * activer ou desactiver le mode.
	 *
	 * @return msg mode activé ou non
	 */
	@GetMapping("/activation/limite/{number}")
	@PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
	public ResponseEntity<String> setLimite(@PathVariable Integer number) {
		Integer nb = parametresService.setLimite(number);
		return ResponseEntity.ok("Nombre limite de déclarants : " + nb);
	}

	/**
	 * etat actuel du mode
	 *
	 * @return ParametresDto
	 */
	@GetMapping("/activation/state")
	public ResponseEntity<ParametresDto> getState() {
		ParametresDto params = parametresService.getState();
		return ResponseEntity.ok(params);
	}
}