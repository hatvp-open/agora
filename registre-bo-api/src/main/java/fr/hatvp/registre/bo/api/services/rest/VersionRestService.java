/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Cette classe permet de connaître le numéro de version de l'application. 
 * On l'utilise pour permettre à l'aplication front d'initialiser les token (session et xsrf).
 * 
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/version")
public class VersionRestService
{
    @Value("${app.version}")
    private String appVersion;
    
    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> initSession()
    {
        String jsonResult = "{ \"version\": \"" + this.appVersion + "\" }";
        return ResponseEntity.ok(jsonResult);
    }
}