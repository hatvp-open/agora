/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 *
 * Cettte classe permet de configurer spring security.
 *
 * @version $Revision$ $Date${0xD}
 */
@Configuration
@CrossOrigin
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration
    extends WebSecurityConfigurerAdapter
{

    /** Configuration de la validation de compte. */
    private final UserDetailsService userDetailService;

    /** Configuration du succès d'authentification. */
    private final AuthSuccess authSuccess;

    /** Configuration de l'echec d'authentification. */
    private final AuthFailure authFailure;

    /** Configuration des accès refusés. */
    private final CustomAccessDenied customAccessDenied;

    /** Configuration du succès de logout. */
    private final CustomLogoutSuccess customLogoutSuccess;

    /** Configuration du handler du logout. */
    private final CustomLogoutHandler logoutHandler;

    /** Configuration du point d'entrée de l'application. */
    private final EntryPointUnAuthorisedHandler entryPointUnAuthorisedHandler;

    @Value(value = "${app.csrf.token.path}")
    private String cookiePath;

    /** Initialisation des configurations. */
    @Autowired
    public SecurityConfiguration(final AuthSuccess authSuccess,
            final UserDetailsService userDetailService, final AuthFailure authFailure,
            final CustomAccessDenied customAccessDenied,
            final CustomLogoutSuccess customLogoutSuccess, final CustomLogoutHandler logoutHandler,
            final EntryPointUnAuthorisedHandler entryPointUnAuthorisedHandler)
    {
        this.authSuccess = authSuccess;
        this.userDetailService = userDetailService;
        this.authFailure = authFailure;
        this.customAccessDenied = customAccessDenied;
        this.customLogoutSuccess = customLogoutSuccess;
        this.logoutHandler = logoutHandler;
        this.entryPointUnAuthorisedHandler = entryPointUnAuthorisedHandler;
    }

    /**
     * Méthode d'initialisation de la configuration globale de spring secu.
     *
     * @param auth objet d'autehntification.
     * @throws Exception erreur d'initialisation.
     */
    @Autowired
    public void configureGlobalSecurity(final AuthenticationManagerBuilder auth) throws Exception
    {
        auth.authenticationProvider(this.authenticationProvider());
    }

    /**
     * Cette méthode permet d'ajouter le header XSRF dans toutes les requêtes filtrées par spring
     * security.
     * Cet objet est injecté dans la configuration globale.
     *
     * @return retourne l'objet de configuration des entete des requetes.
     */
    private CsrfTokenRepository csrfTokenRepository()
    {
        final HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName("X-XSRF-TOKEN");
        return repository;
    }

    /**
     * Cette méthode permet de configurer le codeur du mot de passe.
     * @return L'objet permettant de crypter le mot de passe.
     */
    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder();
    }

    /**
     * Cette méthode permet de préparer le provider d'authentification.
     *
     * @return provider d'authentification.
     */
    @Bean
    public DaoAuthenticationProvider authenticationProvider()
    {
        final DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(this.userDetailService);
        authenticationProvider.setPasswordEncoder(this.passwordEncoder());
        return authenticationProvider;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure(final HttpSecurity http) throws Exception
    {

        http.addFilterAfter(new CsrfTokenConfiguration(this.cookiePath), CsrfFilter.class).csrf()
                .csrfTokenRepository(this.csrfTokenRepository()).and().authorizeRequests()
                .antMatchers("/version").permitAll().antMatchers(HttpMethod.OPTIONS, "/**")
                .permitAll().anyRequest().authenticated().and().formLogin()
                .usernameParameter("email").passwordParameter("password")
                .successHandler(this.authSuccess).failureHandler(this.authFailure).and().logout()
                .addLogoutHandler(this.logoutHandler)
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessHandler(this.customLogoutSuccess).and().exceptionHandling()
                .accessDeniedHandler(this.customAccessDenied)
                .authenticationEntryPoint(this.entryPointUnAuthorisedHandler);
    }
}
