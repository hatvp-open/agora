/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.qualite;
import fr.hatvp.registre.business.QualificationObjetService;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.activite.QualificationObjetDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controlleur Rest qui permet de gérer les déclarations d'activitée.
 *
 */
@RestController
@RequestMapping("/qualif")
public class QualiteRestService
{

    @Autowired
    private QualificationObjetService qualificationObjetService;

    /**
     * Récupérer la liste des qualifications
     *
     * @return response avec liste des objets qualifiés
     */
    @GetMapping("/qualified/{page_no}/{result_per_page}")
    public ResponseEntity<PaginatedDto<QualificationObjetDto>> getQualif(@PathVariable("page_no") final Integer pageNumber,
        @PathVariable("result_per_page") final Integer resultPerPage) {
        return ResponseEntity.ok(qualificationObjetService.getQualif(pageNumber, resultPerPage));

    }

    /**
     * Récupérer la liste des qualifications
     *
     * @return response avec liste des objets qualifiés
     */
    @GetMapping("/qualified/all")
    public ResponseEntity<PaginatedDto<QualificationObjetDto>> getAllQualif() {
        return ResponseEntity.ok(qualificationObjetService.getAllQualif());
    }

    /**
     * Récupère la liste des objets qualifiés par recherche
     *
     * @param type
     * @param keyword
     * @param page
     * @param resultPerPage
     * @return response avec la liste
     */
    @GetMapping("/qualified/type/{type}/recherche/{keyword}/page/{page}/occurence/{result_per_page}")
    public ResponseEntity<PaginatedDto<QualificationObjetDto>>getQualifParRecherchePaginated(@PathVariable("type") final String type, @PathVariable("keyword") final String keyword,
        @PathVariable("page") final Integer page, @PathVariable("result_per_page") final Integer resultPerPage){
        return ResponseEntity.ok(qualificationObjetService.getQualifParRecherche(type, keyword, page, resultPerPage));
    }

    /**
     * Récupérer la liste des qualifications
     *
     * @return response avec liste des objets qualifiés
     */
    @GetMapping("/qualified/idFiche/{idFiche}")
    public ResponseEntity<QualificationObjetDto> getQualif(@PathVariable("idFiche") final String idFiche) {
        return ResponseEntity.ok(qualificationObjetService.getLastQualiObjetByFicheId(idFiche));
    }
}