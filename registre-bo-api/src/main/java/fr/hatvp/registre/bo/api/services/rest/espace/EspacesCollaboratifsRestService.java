/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.espace;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.commons.dto.ListStatutEspaceDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.EspaceCollaboratifDto;

@RestController
@RequestMapping("/espace")
public class EspacesCollaboratifsRestService {

	private final EspaceOrganisationService espaceOrganisationService;

	public EspacesCollaboratifsRestService(EspaceOrganisationService espaceOrganisationService) {
		this.espaceOrganisationService = espaceOrganisationService;
	}

	@GetMapping("/unvalidated/page/{page}/occurrence/{occurrence}")
	public ResponseEntity<PaginatedDto<EspaceCollaboratifDto>> getNotValidatedEspacePaginated(@PathVariable final Integer page, @PathVariable final Integer occurrence) {
		PaginatedDto<EspaceCollaboratifDto> list = this.espaceOrganisationService.getNotValidatedEspacePaginated(page, occurrence);
		return ResponseEntity.ok(list);
	}

	@GetMapping("/type/{type}/recherche/{keyword}/page/{page}/occurence/{occurrence}")
	public ResponseEntity<PaginatedDto<EspaceCollaboratifDto>> getEspaceBySearchPaginated(@PathVariable final String type, @PathVariable final String keyword,
			@PathVariable final Integer page, @PathVariable final Integer occurrence) {
		PaginatedDto<EspaceCollaboratifDto> list = this.espaceOrganisationService.getEspacesBySearch(type, keyword, page, occurrence);
		return ResponseEntity.ok(list);
	}

	@GetMapping("/statut/types")
	public ResponseEntity<ListStatutEspaceDto> getTypeStatutEspace() {
		return ResponseEntity.ok(new ListStatutEspaceDto());
	}

}
