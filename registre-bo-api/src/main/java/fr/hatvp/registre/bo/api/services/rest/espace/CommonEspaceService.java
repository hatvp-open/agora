/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.espace;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.OrganisationService;
import fr.hatvp.registre.commons.dto.TypeOrganisationDto;

/**
 * Contrôleur REST pour les services transverses.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate")
public class CommonEspaceService {

    /** Service des organisations. */
    @Autowired
    private OrganisationService organisationService;

    @GetMapping("/type/organisation")
    public ResponseEntity<List<TypeOrganisationDto>> getTypesOrganisations() {
        final List<TypeOrganisationDto> loDto = this.organisationService
            .getListeTypesOrganisations();
        return new ResponseEntity<>(loDto, HttpStatus.OK);
    }
}
