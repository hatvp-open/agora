/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest;

import java.util.HashMap;
import java.util.Map;

import fr.hatvp.registre.business.DesinscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.DemandeChangementMandantService;
import fr.hatvp.registre.business.DemandeOrganisationService;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.commons.lists.StatutDemandeEnum;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;

/**
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/compteurs")
public class CompteursRestService {

	/** Service DemandeOrganisation */
	@Autowired
	private DemandeOrganisationService demandeOrganisationService;
	/** Service des espaces corporate. */
	@Autowired
	private EspaceOrganisationService espaceOrganisationService;

	@Autowired
	private DemandeChangementMandantService demandeChangementMandantService;

	@Autowired
    private DesinscriptionService desinscriptionService;

	@GetMapping
	public ResponseEntity<Map<String, Integer>> initCompteurs() {
		Map<String, Integer> compteurs = new HashMap<>();
		compteurs.put("mandats", demandeChangementMandantService.getNombreDemandesParStatut(StatutDemandeEnum.NOUVELLE));
		compteurs.put("mandatsComplements", demandeChangementMandantService.getNombreDemandesParStatut(StatutDemandeEnum.COMPLEMENT_RECU));
		compteurs.put("creationsEC", espaceOrganisationService.getNombreDemandesParStatut(StatutEspaceEnum.NOUVEAU));
		compteurs.put("creationsECComplements", espaceOrganisationService.getNombreDemandesParStatut(StatutEspaceEnum.COMPLEMENT_RECU));
		compteurs.put("demandesOrga", demandeOrganisationService.getNombreDemandesStatutNouvelle());
		compteurs.put("demandesDesinscription", desinscriptionService.getNombreDemandeDesinscription());
        compteurs.put("complementsDemandeDesinscriptionRecu", desinscriptionService.getNombreComplementsDemandeDesinscriptionReçu());
		return new ResponseEntity<>(compteurs, HttpStatus.OK);
	}
}