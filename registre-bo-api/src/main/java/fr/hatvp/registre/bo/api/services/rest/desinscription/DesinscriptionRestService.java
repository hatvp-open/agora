/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.desinscription;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.hatvp.registre.bo.api.security.CurrentUserId;
import fr.hatvp.registre.business.DesinscriptionService;
import fr.hatvp.registre.business.ExerciceComptableService;
import fr.hatvp.registre.business.GestionPieceDesinscriptionService;
import fr.hatvp.registre.commons.dto.DesinscriptionDto;
import fr.hatvp.registre.commons.dto.DesinscriptionPieceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.ErrorMessageEnum;
import fr.hatvp.registre.commons.utils.PiecesUtils;

@RestController
@RequestMapping("/desinscription")
public class DesinscriptionRestService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DesinscriptionRestService.class);

    DesinscriptionService desinscriptionService;

    @Autowired
    GestionPieceDesinscriptionService gestionPieceDesinscriptionService;
    
    @Autowired
    ExerciceComptableService exerciceComptableService;

    public DesinscriptionRestService(DesinscriptionService desinscriptionService) {
        this.desinscriptionService = desinscriptionService;
    }

    @PostMapping("/agent")
    public ResponseEntity<DesinscriptionDto> saveDesinscription (@RequestParam(required = false) final List<MultipartFile> files, @RequestParam final String desinscription, @RequestParam final Long espaceOrganisationId, @CurrentUserId Long userId){
        final ObjectMapper mapper = new ObjectMapper();
        DesinscriptionDto desinscriptionDto;
        ArrayList<DesinscriptionPieceDto> pieces = new ArrayList<>();
        LOGGER.info("Save desinscription pour espcage {} ", espaceOrganisationId);
        try {
	        desinscriptionDto = mapper.readValue(desinscription, DesinscriptionDto.class);
	        // Si au moins une activité ou un moyen ont été publié après la date de cessation on empêche la désinscription
	        boolean publicationApresCessation = this.exerciceComptableService.hasPublishedAfterCessationDate(espaceOrganisationId, desinscriptionDto.getCessationDate());
	        if(publicationApresCessation){
	            throw new BusinessGlobalException(ErrorMessageEnum.DESINSCRIPTION_INTERDITE.getMessage());
	        }	        
	        
	        for(MultipartFile file : files){
	            pieces.add(PiecesUtils.mapToDesinscriptionDto(file));
	        }
        
            desinscriptionDto = this.desinscriptionService.saveDesinscription(desinscriptionDto, pieces, userId, espaceOrganisationId);
            return ResponseEntity.ok(desinscriptionDto);
        } catch (IOException e) {
            LOGGER.error(ErrorMessageEnum.ECHEC_ENREGISTREMENT_DESINSCRIPTION.getMessage(), e);
            throw new BusinessGlobalException(ErrorMessageEnum.ECHEC_ENREGISTREMENT_DESINSCRIPTION.getMessage());
        } catch (final BusinessGlobalException e) {
            throw new BusinessGlobalException(e.getMessage());
        }
    }

    @GetMapping("/{espaceId}")
    public ResponseEntity<DesinscriptionDto> getDesinscription(@PathVariable long espaceId){
        DesinscriptionDto dto = this.desinscriptionService.getDesinscription(espaceId);
        return ResponseEntity.ok(dto);
    }

    @GetMapping("/pieces/{espaceId}")
    public ResponseEntity<List<DesinscriptionPieceDto>> getPiecesDesinscription(@PathVariable long espaceId){
        List<DesinscriptionPieceDto> desinscriptionPieceDtos = this.desinscriptionService.getPiecesDesinscription(espaceId);
        return ResponseEntity.ok(desinscriptionPieceDtos);
    }

    /**
     * Téléchargement d'une pièce selon son identifiant.
     *
     * @param pieceId
     * @param response
     * @throws IOException
     */
    @RequestMapping(path = "/download/{pieceId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void telechargerPiecesDesinscription(@PathVariable final Long pieceId, final HttpServletResponse response) throws IOException {
        DesinscriptionPieceDto piece = this.gestionPieceDesinscriptionService.findOne(pieceId);

        response.setHeader("Content-Disposition", "attachment;filename=\"" + piece.getNomFichier() + "\"");
        response.getOutputStream().write(piece.getContenuFichier());
        response.flushBuffer();
    }

    @GetMapping("/reject/{desinscriptionId}")
    public void rejectDemande(@PathVariable long desinscriptionId){
        this.desinscriptionService.rejectDemande(desinscriptionId);
    }

    @PostMapping("/complement/{desinscriptionId}/{currentUserId}")
    public void askForComplement(@PathVariable long desinscriptionId, @PathVariable long currentUserId, @RequestBody String msg){
        this.desinscriptionService.askForComplement(desinscriptionId, msg, currentUserId);
    }

}
