/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.blacklist;

import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.commons.dto.EspaceOrganisationBlacklistedDto;
import fr.hatvp.registre.commons.dto.ListRaisonBlacklistDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Cette classe permet de gérer les espaces blacklisté.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/blacklist")
public class BlacklistRestService {

    private final EspaceOrganisationService espaceOrganisationService;

    public BlacklistRestService(EspaceOrganisationService espaceOrganisationService) {
        this.espaceOrganisationService = espaceOrganisationService;
    }

    /**
     * Récupérer la liste des espaces blacklisté.
     *
     * @return response avec liste des espaces blackisté
     */
    @GetMapping("/blacklisted/{page_no}/{result_per_page}")
    public ResponseEntity<PaginatedDto<EspaceOrganisationBlacklistedDto>> getEspacesBlacklisted(@PathVariable("page_no") final Integer pageNumber,
                                                                                                @PathVariable("result_per_page") final Integer resultPerPage) {
        return ResponseEntity.ok(espaceOrganisationService.getEspacesOrganisationBlacklisted(pageNumber, resultPerPage));

    }

    /**
     * Récupère la liste des espaces blacklisté par recherche
     *
     * @param type
     * @param keyword
     * @param page
     * @param occurrence
     * @return response avec la liste
     */
    @GetMapping("/type/{type}/recherche/{keyword}/page/{page}/occurrence/{occurrence}")
    public ResponseEntity<PaginatedDto<EspaceOrganisationBlacklistedDto>> getEspacesBlacklistedParRecherchePaginated(@PathVariable("type") final String type, @PathVariable("keyword") final String keyword,
                                                                                                            @PathVariable("page") final Integer page, @PathVariable("occurrence") final Integer occurrence){
        return ResponseEntity.ok(espaceOrganisationService.getEspacesBlacklistedParRecherche(type,keyword,page,occurrence));
    }

    @GetMapping("/download")
    public ResponseEntity<List<EspaceOrganisationBlacklistedDto>> getAllSpaceNotPaginated(){
        return ResponseEntity.ok(espaceOrganisationService.getAllSpaceNotPaginated());
    }

    @GetMapping("/raisonBlacklist/types")
    public ResponseEntity<ListRaisonBlacklistDto> getTypeRaisonBlacklist() {
        return ResponseEntity.ok(new ListRaisonBlacklistDto());
    }
}
