/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.utilisateur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.backoffice.UtilisateurBoService;

/**
 *
 * Cette classe permet de supprimer des utilisateurs BO.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/account")
@PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
public class DeleteUtilisateurRestService
{

    /**
     * Service de l'utilisateur backoffice.
     */
    private UtilisateurBoService utilisateurBoService;

    /**
     * Initialisation du service.
     *
     * @param utilisateurBoService services des utilisateurs bo.
     */
    @Autowired
    public void setUtilisateurBoService(final UtilisateurBoService utilisateurBoService)
    {
        this.utilisateurBoService = utilisateurBoService;
    }

    /**
     * Supprimer un utilisateur.*
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    public ResponseEntity<Void> deleteUser(@PathVariable final Long id)
    {
        this.utilisateurBoService.supprimerUtilisateur(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
