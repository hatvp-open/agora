/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.utilisateur;

import javax.validation.Valid;

import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoGestionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.backoffice.UtilisateurBoService;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;

/**
 * 
 * Cette classe permet d'ajouter des utilisateurs BO.
 * 
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/account")
@PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
public class AddUtilisateurRestService
{

    /** Service de l'utilisateur backoffice. */
    private UtilisateurBoService utilisateurBoService;

    /**
     * Initialisation du service.
     *
     * @param utilisateurBoService services des utilisateurs bo.
     */
    @Autowired
    public void setUtilisateurBoService(final UtilisateurBoService utilisateurBoService)
    {
        this.utilisateurBoService = utilisateurBoService;
    }

    /**
     * Ajouter un utilisateur bo.
     *
     * @return utilisateur créé.
     */
    @PostMapping("/add")
    @PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
    public ResponseEntity<UtilisateurBoGestionDto> addBackOfficeUser(
            @RequestBody @Valid final UtilisateurBoDto utilisateurBoDto)
    {
        final UtilisateurBoGestionDto account = this.utilisateurBoService
                .ajouterUtilisateurBo(utilisateurBoDto);

        account.setCompteSupprime(null);
        return new ResponseEntity<>(account, HttpStatus.CREATED);
    }

}
