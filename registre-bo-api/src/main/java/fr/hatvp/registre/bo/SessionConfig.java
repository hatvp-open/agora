/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

/**
 * Cette classe permet de gérer le temps d'expiration de la session.
 *
 * @version $Revision$ $Date${0xD}
 */

@Configuration
public class SessionConfig
    implements HttpSessionListener
{

    private static Logger LOGGER = LoggerFactory.getLogger(SessionConfig.class);

    // @Value("${app.session.timeout}")
    private final int sessionTimeOut = 30;

    /**
     * {@inheritDoc}
     */
    @Override
    public void sessionCreated(final HttpSessionEvent event)
    {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("==== Session is created with timeout of ({}) ====", this.sessionTimeOut);
        }
        event.getSession().setMaxInactiveInterval(this.sessionTimeOut * 60);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sessionDestroyed(final HttpSessionEvent event)
    {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("==== Session is destroyed ====");
        }
    }

}