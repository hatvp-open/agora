/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import fr.hatvp.registre.business.backoffice.UtilisateurBoService;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;

/**
 * 
 * Cette classe permet de configurer la logique d'authentification.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Component
public class UserDetailServiceImpl
    implements UserDetailsService
{

    /** LOGGER de la classe. */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailServiceImpl.class);

    /** Service de l'utilisateur backoffice. */
    private UtilisateurBoService utilisateurBoService;

    /** Initialisation du service. */
    @Autowired
    public void setUtilisateurBoService(final UtilisateurBoService utilisateurBoService)
    {
        this.utilisateurBoService = utilisateurBoService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserDetails loadUserByUsername(final String username)
    {

        final UtilisateurBoDto account = this.utilisateurBoService.findUtilisateurByEmail(username);

        if (account == null) {
            LOGGER.info("L'email saisi ({}) est introuvable:)", username);
            throw new UsernameNotFoundException("Utilisateur introuvable: " + username);
        }

        LOGGER.info("Tentative de connexion avec l'email: {}", username);
        return new AccountUserDetails(account);
    }

}
