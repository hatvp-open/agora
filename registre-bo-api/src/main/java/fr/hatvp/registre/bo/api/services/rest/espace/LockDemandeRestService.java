/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.espace;

import fr.hatvp.registre.bo.api.security.AccountUserDetails;
import fr.hatvp.registre.business.backoffice.DemandeOrganisationLockBoService;
import fr.hatvp.registre.commons.dto.backoffice.DemandeOrganisationLockBoDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Cette classe permet de gérer le lock sur les des demandes de création d'organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/organisation")
@PreAuthorize("hasAnyRole('ROLE_GESTION_REFERENTIEL_ORGANISATIONS', 'ROLE_MANAGER')")
public class LockDemandeRestService {

    /**
     * Service des locks.
     */
    @Autowired
    private DemandeOrganisationLockBoService demandeOrganisationLockBoService;

    /**
     * Vérifier si un lock est appliqué sur une demande organisation.
     *
     * @param id id de la demande organisation
     * @return objet lock sur la demande si existe, null sinon
     */
    @GetMapping("/lock/{id}")
    public ResponseEntity<DemandeOrganisationLockBoDto> getLockOnDemande(
        @PathVariable final Long id) {
        return ResponseEntity.ok(this.demandeOrganisationLockBoService.getLockOn(id));
    }

    /**
     * Appliquer un lock sur une demande organisation.
     *
     * @param id id de la demande organisation
     */
    @PostMapping("/lock/{id}")
    public ResponseEntity<Void> setLockOnDemande(@PathVariable final Long id,
                                                 final Authentication authentication) {
        final Long connectedUserId = ((AccountUserDetails) authentication.getPrincipal()).getId();

        Optional.ofNullable(this.demandeOrganisationLockBoService.getLockOn(id))
            .ifPresent(demandeOrganisationLockBoDto -> {
                throw new BusinessGlobalException(
                    "Un lock est déjà en cours sur cette demande par "
                        + demandeOrganisationLockBoDto.getUtilisateurBoName()
                        + "! L'entité sera de nouveau accessible dans "
                        + demandeOrganisationLockBoDto.getLockTimeRemain()
                        + " secondes.");
            });

        this.demandeOrganisationLockBoService.setLockOn(id, connectedUserId);
        return ResponseEntity.accepted().build();
    }

    /**
     * Appliquer un lock sur une demande organisation.
     *
     * @param dto du lock de la demande organisation
     */
    @PutMapping("/lock")
    public ResponseEntity<DemandeOrganisationLockBoDto> updateLockOnDemande(
        @RequestBody final DemandeOrganisationLockBoDto dto,
        final Authentication authentication) {
        final Long connectedUserId = ((AccountUserDetails) authentication.getPrincipal()).getId();

        return new ResponseEntity<>(
            this.demandeOrganisationLockBoService.updateLockOnDemande(dto, connectedUserId),
            HttpStatus.ACCEPTED);
    }
}
