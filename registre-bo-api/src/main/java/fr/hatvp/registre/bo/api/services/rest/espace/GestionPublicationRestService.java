/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.espace;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import fr.hatvp.registre.bo.api.security.AccountUserDetails;
import fr.hatvp.registre.business.EspaceOrganisationService;
import fr.hatvp.registre.business.PublicationActiviteService;
import fr.hatvp.registre.business.PublicationExerciceService;
import fr.hatvp.registre.business.PublicationService;
import fr.hatvp.registre.commons.dto.publication.PublicationMetaDto;

/**
 * Cette classe permet de gérer les publications.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate")
@PreAuthorize("hasRole('ROLE_GESTION_CONTENUS_PUBLIES')")
public class GestionPublicationRestService
{

    /** Service des espaces corporate. */
    @Autowired
    private EspaceOrganisationService espaceOrganisationService;

    /** Service des publications. */
    @Autowired
    private PublicationService publicationService;
    
    /** Service des publications Activite. */
    @Autowired
    private PublicationActiviteService publicationActiviteService;
    
    /** Service des publications exercice. */
    @Autowired
    private PublicationExerciceService publicationExerciceService;

    /**
     * Récupérer liste des publications d'un espace.
     *
     * @param espaceId identifiant espace corporate
     * @return response avec liste des publications
     */
    @GetMapping("/{espaceId}/publications")
    public ResponseEntity<List<PublicationMetaDto>> getPublicationsByEspace(
            @PathVariable final Long espaceId)
    {
        return ResponseEntity.ok(this.publicationService.getPublicationsEspace(espaceId));
    }

    /**
     * Dépublier une publication.
     * @param id identifiant publication
     * @param authentication objet spring security d'authentification
     * @return response
     */
    @PutMapping("/publications/{id}/depublier")
    public ResponseEntity<Void> depublierPublication(@PathVariable final Long id,
            final Authentication authentication)
    {
        final Long userId = ((AccountUserDetails) authentication.getPrincipal()).getId();

        this.publicationService.depublier(id, userId);

        return ResponseEntity.accepted().build();
    }

    
    /**
     * Republier une publication.
     * @param id identifiant publication
     * @param authentication objet spring security d'authentification
     * @return response
     */
    @PutMapping("/publications/{id}/republier")
    public ResponseEntity<Void> republierPublication(@PathVariable final Long id,
            final Authentication authentication)
    {
        final Long userId = ((AccountUserDetails) authentication.getPrincipal()).getId();

        this.publicationService.republier(id, userId);

        return ResponseEntity.accepted().build();
    }

    
    
    /**
     * Dépublier les publications d'un espace.
     * @param espaceId identifiant espace organisation
     * @return response
     */
    @PutMapping("/{espaceId}/publications/depublier")
    public ResponseEntity<Void> depublierPublicationsEspace(@PathVariable final Long espaceId)
    {
        this.espaceOrganisationService.setPublicationState(espaceId, false);
        return ResponseEntity.accepted().build();
    }

    /**
     * Republier les publications d'un espace.
     * @param espaceId identifiant espace organisation
     * @return response
     */
    @PutMapping("/{espaceId}/publications/republier")
    public ResponseEntity<Void> republierPublicationsEspace(@PathVariable final Long espaceId)
    {
        this.espaceOrganisationService.setPublicationState(espaceId, true);
        return ResponseEntity.accepted().build();
    }
    
    
    
    /**
     * Dépublier une fiche.
     * @param activiteId identifiant fiche
     * @param authentication objet spring security d'authentification
     * @return response
     */
    @PutMapping("/fiche/{activiteId}/depublier")
    public ResponseEntity<Void> depublierFiche(@PathVariable final Long activiteId, final Authentication authentication)
    {
        final Long userId = ((AccountUserDetails) authentication.getPrincipal()).getId();

        this.publicationActiviteService.depublierActivite(activiteId, userId);

        return ResponseEntity.accepted().build();
    }
    

    
    /**
     * Républier une fiche.
     * @param activiteId identifiant fiche
     * @param authentication objet spring security d'authentification
     * @return response
     */
    @PutMapping("/fiche/{activiteId}/republier")
    public ResponseEntity<Void> republierFiche(@PathVariable final Long activiteId, final Authentication authentication)
    {
        final Long userId = ((AccountUserDetails) authentication.getPrincipal()).getId();

        this.publicationActiviteService.republierActivite(activiteId, userId);

        return ResponseEntity.accepted().build();
    }
    
    /**
     * Dépublier un exercice (moyens + fiches)
     * @param exerciceId 
     * @param authentication objet spring security d'authentification
     * @return response
     */
    @PutMapping("/exercice/{exerciceId}/depublier")
    public ResponseEntity<Void> depublierPublicationExercice(@PathVariable final Long exerciceId, final Authentication authentication)
    {
        final Long userId = ((AccountUserDetails) authentication.getPrincipal()).getId();

        this.publicationExerciceService.depublierExercice(exerciceId, userId);

        return ResponseEntity.accepted().build();
    }
    
    /**
     * Republier un exercice (moyens + fiches)
     * @param exerciceId 
     * @param authentication objet spring security d'authentification
     * @return response
     */
    @PutMapping("/exercice/{exerciceId}/republier")
    public ResponseEntity<Void> republierPublicationExercice(@PathVariable final Long exerciceId, final Authentication authentication)
    {
        final Long userId = ((AccountUserDetails) authentication.getPrincipal()).getId();

        this.publicationExerciceService.republierExercice(exerciceId, userId);

        return ResponseEntity.accepted().build();
    }
    
    /**
     * Républier une liste de ficheq.
     * @param activiteIdList liste d'identifiant fiche
     * @param authentication objet spring security d'authentification
     * @return response
     */
    @PutMapping("/fichelist/{activiteIdList}/republier")
    public ResponseEntity<Void> republierListFiche(@PathVariable final Long[] activiteIdList, final Authentication authentication)
    {
        final Long userId = ((AccountUserDetails) authentication.getPrincipal()).getId();
        
        if(activiteIdList.length == 1) {
        	this.publicationActiviteService.republierActivite(activiteIdList[0], userId);
        }else {
        	this.publicationActiviteService.republierListActivite(activiteIdList, userId);
        }
        

        return ResponseEntity.accepted().build();
    }
    
    /**
     * Dépublier une fiche.
     * @param activiteId identifiant fiche
     * @param authentication objet spring security d'authentification
     * @return response
     */
    @PutMapping("/fichelist/{activiteIdList}/depublier")
    public ResponseEntity<Void> depublierListFiche(@PathVariable final Long[] activiteIdList, final Authentication authentication)
    {
        final Long userId = ((AccountUserDetails) authentication.getPrincipal()).getId();
        
        if(activiteIdList.length == 1) {
        	this.publicationActiviteService.depublierActivite(activiteIdList[0], userId);
        }else {
        	this.publicationActiviteService.depublierListActivite(activiteIdList, userId);
        }
        

        return ResponseEntity.accepted().build();
    }

}
