/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.espace;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.GestionPiecesEspaceOrganisationService;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;

/**
 * Controlleur qui permet de gérer les pièces de l'espace organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/corporate/pieces")
@PreAuthorize("hasAnyRole('ROLE_VALIDATION_ESPACES_COLLABORATIFS', 'ROLE_GESTION_ESPACES_COLLABORATIFS_VALIDES')")
public class EspaceOrganisationPiecesRestService {

	/** Service pour pour la gestion des pièces de l'espace. */
	@Autowired
	private GestionPiecesEspaceOrganisationService gestionPiecesEspaceOrganisationService;

	/**
	 * Récupération de la liste des pièces complémentaires d'un espace.
	 *
	 * @param espaceId
	 * @return la liste des pièces.
	 */
	@GetMapping("/complement/{espaceId}")
	public ResponseEntity<List<EspaceOrganisationPieceDto>> getListePiecesComplementaires(@PathVariable final Long espaceId) {
		return ResponseEntity.ok(this.gestionPiecesEspaceOrganisationService.getListePiecesComplementairesEspaceOrganisation(espaceId));
	}

	/**
	 * Récupération de la liste des pièces versées pour la création d'un espace.
	 *
	 * @param espaceId
	 * @return la liste des pièces.
	 */
	@GetMapping("/creation/{espaceId}")
	public ResponseEntity<List<EspaceOrganisationPieceDto>> getListePiecesCreationEspace(@PathVariable final Long espaceId) {
		return ResponseEntity.ok(this.gestionPiecesEspaceOrganisationService.getListePiecesCreationEspaceOrganisation(espaceId));
	}

	/**
	 * Récupération de la liste des pièces versées pour une demande de changement de
	 * mandant.
	 *
	 * @param espaceId
	 * @return la liste des pièces.
	 */
	@GetMapping("/mandant/{espaceId}")
	public ResponseEntity<List<EspaceOrganisationPieceDto>> getListePiecesDemandeChangementMandant(@PathVariable final Long espaceId) {
		return ResponseEntity.ok(this.gestionPiecesEspaceOrganisationService.getListePiecesCreationEspaceOrganisation(espaceId));
	}

	/**
	 * Téléchargement d'une pièce selon son identifiant.
	 *
	 * @param pieceId
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(path = "/download/{pieceId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public void telechargerPieceComplementaire(@PathVariable final Long pieceId, final HttpServletResponse response) throws IOException {
		EspaceOrganisationPieceDto piece = this.gestionPiecesEspaceOrganisationService.findOne(pieceId);

		response.setHeader("Content-Disposition", "attachment;filename=\"" + piece.getNomFichier() + "\"");
		response.getOutputStream().write(piece.getContenuFichier());
		response.flushBuffer();
	}
}
