/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.utilisateur;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.bo.api.security.AccountUserDetails;
import fr.hatvp.registre.business.backoffice.UtilisateurBoService;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;

/**
 *
 * Cette classe permet de récupérer des utilisateurs BO.
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/account")
public class GetUtilisateurRestService
{

	/**
	 * Service de l'utilisateur backoffice.
	 */
	private UtilisateurBoService utilisateurBoService;

	/**
	 * Initialisation du service.
	 *
	 * @param utilisateurBoService services des utilisateurs bo.
	 */
	@Autowired
	public void setUtilisateurBoService(final UtilisateurBoService utilisateurBoService)
	{
		this.utilisateurBoService = utilisateurBoService;
	}

	/**
	 * Récupérer les données de l'utilisateur connecté.
	 *
	 * @param authentication objet auth du user connecté.
	 * @return les données de l'utilisateur connecté.
	 */
	@GetMapping
	public ResponseEntity<UtilisateurBoDto> getConnectedUserData(
			final Authentication authentication)
	{

		final Long connectedUserId = ((AccountUserDetails) authentication.getPrincipal()).getId();

		final UtilisateurBoDto account = this.utilisateurBoService.findOne(connectedUserId);

		return new ResponseEntity<>(account, HttpStatus.OK);
	}

	/**
	 * @return la liste de tous les utilisateurs bo.
	 */
	@GetMapping("/all")
	@PreAuthorize("hasRole('ROLE_ADMINISTRATEUR')")
	public ResponseEntity<List<UtilisateurBoDto>> getAllBoUsers()
	{
		final List<UtilisateurBoDto> utilisateurBoDtos = this.utilisateurBoService.findAll()
				.stream().map(u -> {
					u.setPassword(null);
					return u;
				})
				.filter(u -> u.getCompteSupprime().equals(Boolean.FALSE))
                .filter(u -> u.getCompteActive().equals(Boolean.TRUE))
				.sorted(Comparator.comparing(AbstractCommonDto::getId))
				.collect(Collectors.toList());

		return new ResponseEntity<>(utilisateurBoDtos, HttpStatus.OK);
	}

	/**
	 * Récupérer une page de liste des utilisateurs back office.
	 *
	 * @param pageNumber
	 * @param resultPerPage
	 * @return liste des espaces organisation.
	 */
	@GetMapping("/all/page/{page_no}/{result_per_page}")
	public ResponseEntity<PaginatedDto<UtilisateurBoDto>> getAllBoUsersPaginated(@PathVariable("page_no") final Integer pageNumber, @PathVariable("result_per_page") final Integer resultPerPage) {
		return ResponseEntity.ok(this.utilisateurBoService.getAllBoUsersPaginated(pageNumber, resultPerPage));
	}

	/**
	 * Récupérer une page spécifique de la liste PagINée des demandes de création d'espace pas encore validées
	 *
	 * @param type
	 * @param keywords
	 * @param pageNumber
	 * @param resultPerPage
	 * @return une page de la liste des dtos
	 */
	@GetMapping("/all/{type}/{keywords}/page/{page_no}/{result_per_page}")
	public ResponseEntity<PaginatedDto<UtilisateurBoDto>> getAllBoUsersPaginatedParRecherche(
			@PathVariable("type") final String type,
			@PathVariable("keywords") final String keywords,
			@PathVariable("page_no") final Integer pageNumber,
			@PathVariable("result_per_page") final Integer resultPerPage) {
		return ResponseEntity.ok(this.utilisateurBoService.getAllBoUsersPaginatedParRecherche(type, keywords, pageNumber, resultPerPage));
	}
}
