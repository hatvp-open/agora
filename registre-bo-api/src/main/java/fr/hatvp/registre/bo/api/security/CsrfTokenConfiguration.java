/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

/**
 * 
 * Cette classe configure l'injection du token CSRF.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Configuration
public class CsrfTokenConfiguration
    extends OncePerRequestFilter
{

    /** Chemin vers le XSRF token. */
    private String tokenPath;

    /** Constructeur de la classe. */
    public CsrfTokenConfiguration(final String tokenPath)
    {
        this.tokenPath = tokenPath;
    }

    /** Constructeur vide de la classe. */
    public CsrfTokenConfiguration()
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doFilterInternal(final HttpServletRequest request,
            final HttpServletResponse response, final FilterChain filterChain)
        throws ServletException,
        IOException
    {
        final CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
        if (csrf != null) {
            Cookie cookie = WebUtils.getCookie(request, "X-XSRF-TOKEN");
            final String token = csrf.getToken();
            if ((cookie == null) || ((token != null) && !token.equals(cookie.getValue()))) {
                cookie = new Cookie("X-XSRF-TOKEN", token);
                cookie.setPath(this.tokenPath);
                response.addCookie(cookie);
            }
        }
        filterChain.doFilter(request, response);
    }

}
