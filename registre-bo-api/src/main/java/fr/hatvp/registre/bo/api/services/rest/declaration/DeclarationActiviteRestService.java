/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.declaration;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.DeclarationActiviteService;
import fr.hatvp.registre.business.ExerciceComptableService;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableDatesDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableEtDeclarationDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableSimpleDto;
import fr.hatvp.registre.commons.dto.backoffice.activite.FicheBackDto;

/**
 * Fournit les ressources REST pour le traitement des rapports d'activité
 *
 */
@RestController
@RequestMapping("/rapport")
public class DeclarationActiviteRestService {

	private final ExerciceComptableService exerciceComptableService;

	private final DeclarationActiviteService declarationActiviteService;

	public DeclarationActiviteRestService(ExerciceComptableService exerciceComptableService, DeclarationActiviteService declarationActiviteService) {
		this.exerciceComptableService = exerciceComptableService;
		this.declarationActiviteService = declarationActiviteService;
	}

	/**
	 * recupère une liste des exercices comptables avec une liste simplifiée des activités liées.
	 *
	 * @param currentEspaceOrganisationId
	 * @return une liste des exercices comptables avec une liste simplifiée des activités liées
	 */
	@GetMapping("/{espaceOrganisationId}")
	public ResponseEntity<List<ExerciceComptableEtDeclarationDto>> getExerciceComptableSimple(@PathVariable final Long espaceOrganisationId) {
		List<ExerciceComptableEtDeclarationDto> exercicesComptable = exerciceComptableService.findAllWithDeclarationByEspaceOrganisationIdBack(espaceOrganisationId);
		return ResponseEntity.ok(exercicesComptable);
	}

	/**
	 * recupère une fiche d'activités.
	 *
	 * @param ficheId
	 * @return une fiche d'activités
	 */
	@GetMapping("/fiche/{ficheId}")
	public ResponseEntity<FicheBackDto> getFiche(@PathVariable final Long ficheId) {
		FicheBackDto fiche = declarationActiviteService.getFicheBO(ficheId);
		return ResponseEntity.ok(fiche);
	}

	/**
	 * met à jour l'exercice comptable passé en paramètre
	 *
	 * @param exerciceId
	 * @param exerciceComptable
	 * @param currentEspaceOrganisationId
	 * @return un exercice comptable
	 */
	@PutMapping("/update/{exerciceId}")
	public ResponseEntity<ExerciceComptableSimpleDto> updateDatesExerciceComptable(@PathVariable final Long exerciceId,
			@RequestBody @Valid final ExerciceComptableDatesDto exerciceComptable) {
		ExerciceComptableSimpleDto exerciceComptableDto = exerciceComptableService.updateDatesExerciceComptable(exerciceId, exerciceComptable,
				exerciceComptable.getEspaceOrganisationId());
		return ResponseEntity.accepted().body(exerciceComptableDto);
	}
	/**
	 * 
	 * @param activiteId
	 * @return
	 */
	@DeleteMapping("/fiche/suppression/{activiteIdList}")
	public ResponseEntity<Void> suppressionActivite(@PathVariable final Long[] activiteIdList) {
		if(activiteIdList.length == 1) {
			this.declarationActiviteService.deleteActiviteBO(activiteIdList[0]);
		}else {
			this.declarationActiviteService.deleteListActiviteBO(activiteIdList);
		}
		
		return ResponseEntity.accepted().build();
	}
	
	/**
	 * recupère une fiche d'activités historisée.
	 *
	 * @param ficheId
	 * @return une fiche d'activités
	 */
	@GetMapping("/historiquefiche/{publicationFicheId}")
	public ResponseEntity<FicheBackDto> getFicheHistorise(@PathVariable final Long publicationFicheId) {
		FicheBackDto fiche = declarationActiviteService.getFicheHistoriseBO(publicationFicheId);
		return ResponseEntity.ok(fiche);
	}

}
