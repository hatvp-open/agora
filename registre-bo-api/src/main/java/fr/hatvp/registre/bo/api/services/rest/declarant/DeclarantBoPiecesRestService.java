/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.declarant;
import fr.hatvp.registre.business.GestionPiecesEspaceDeclarantService;
import fr.hatvp.registre.commons.dto.EspaceDeclarantPieceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Controlleur qui permet de gérer les pièces du déclarant
 *
 * @version $Revision$ $Date${0xD}
 */
@RestController
@RequestMapping("/declarant/pieces")
public class DeclarantBoPiecesRestService
{
    /** Service pour pour la gestion des pièces du déclarant. */
    @Autowired
    private GestionPiecesEspaceDeclarantService gestionPiecesEspaceDeclarantService;

    /**
     * Récupération de la liste des pièces versées pour la création d'un espace.
     * @param espaceId
     * @return la liste des pièces.
     */
    @GetMapping("/all/{declarantId}")
    public ResponseEntity<List<EspaceDeclarantPieceDto>> getListePieces(@PathVariable final Long declarantId) {
        return ResponseEntity.ok(this.gestionPiecesEspaceDeclarantService.getListePiecesAllEspaceDeclarant(declarantId));
    }

    /**
     * Téléchargement d'une pièce selon son identifiant.
     * @param pieceId
     * @param response
     * @throws IOException
     */
    @RequestMapping(path = "/download/{pieceId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void telechargerPiece(
        @PathVariable final Long pieceId,
        final HttpServletResponse response) throws IOException
    {
        EspaceDeclarantPieceDto piece = this.gestionPiecesEspaceDeclarantService.findOne(pieceId);

        response.setHeader("Content-Disposition", "attachment;filename=\""
            + piece.getNomFichier() + "\"");
        response.getOutputStream()
            .write(piece.getContenuFichier());
        response.flushBuffer();
    }
}