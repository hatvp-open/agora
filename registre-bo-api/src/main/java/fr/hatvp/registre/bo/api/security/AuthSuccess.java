/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.hatvp.registre.business.backoffice.UtilisateurBoService;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;

/**
 * 
 * Cette classe configure le succès d'authentification.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Component
public class AuthSuccess
    implements AuthenticationSuccessHandler
{

    /** Servic de l'utilisateur backoffice. */
    private UtilisateurBoService utilisateurBoService;

    /** Initialisation du service. */
    @Autowired
    public void setUtilisateurBoService(final UtilisateurBoService utilisateurBoService)
    {
        this.utilisateurBoService = utilisateurBoService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request,
            final HttpServletResponse response, final Authentication authentication)
        throws IOException,
        ServletException
    {
        final UtilisateurBoDto utilisateurBoDto = this.utilisateurBoService
                .findUtilisateurByEmail(authentication.getName());

        final ObjectMapper mapper = new ObjectMapper();
        utilisateurBoDto.setPassword(null);

        response.getWriter().write(mapper.writeValueAsString(utilisateurBoDto));

        response.setStatus(HttpServletResponse.SC_OK);
    }
}
