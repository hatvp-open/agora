/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.hatvp.registre.commons.dto.backoffice.erreur.ErrorResource;

/**
 * 
 * Cette classe configure l'erreur d'authentification.
 * 
 * @version $Revision$ $Date${0xD}
 */
@Component
public class AuthFailure
    extends SimpleUrlAuthenticationFailureHandler
{

    /** LOGGER de classe. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthFailure.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAuthenticationFailure(final HttpServletRequest request,
            final HttpServletResponse response, final AuthenticationException exception)
        throws IOException,
        ServletException
    {
        super.onAuthenticationFailure(request, response, exception);

        LOGGER.info("Erreur d'authentification: {}" , exception.getMessage());

        final ErrorResource error = new ErrorResource();
        error.setCode(HttpServletResponse.SC_UNAUTHORIZED);

        if (exception instanceof BadCredentialsException) {
            error.setMessage("Données d'authentification incorrectes!");
        }
        else if (exception instanceof DisabledException) {
            error.setMessage("Compte inactif!");
        }
        else {
            error.setMessage(exception.getMessage());
        }

        final ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(error));

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
}
