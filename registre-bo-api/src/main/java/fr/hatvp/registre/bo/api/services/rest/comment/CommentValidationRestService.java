/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.bo.api.services.rest.comment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.hatvp.registre.business.backoffice.comment.CommentValidationBoService;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;

/**
 * Classe de controlleur pour les commentaire des validation d'EO.
 *
 *
 * @version $Revision$ $Date${0xD}
 */

@RestController
@RequestMapping("/comment/validation")
public class CommentValidationRestService {

	/** Service des commentaires des validations des inscriptions. */
	private final CommentValidationBoService commentValidationBoService;

	/** Initialisation du service. */
	@Autowired
	public CommentValidationRestService(CommentValidationBoService commentValidationBoService) {
		this.commentValidationBoService = commentValidationBoService;
	}

	/**
	 *
	 * @param inscriptionId
	 *            id de l'inscription.
	 * @return liste des commentaires du déclarant.
	 */
	@GetMapping("/all/{inscriptionId}")
	public ResponseEntity<List<CommentBoDto>> getCommentBydeclarantId(@PathVariable final Long inscriptionId) {
		return ResponseEntity.ok().body(commentValidationBoService.getAllComment(inscriptionId));
	}

	/**
	 *
	 * @param comment
	 *            commentaire à sauvegarder.
	 * @return commentaire sauvegardé.
	 */
	@PostMapping
	public ResponseEntity<CommentBoDto> saveComment(@RequestBody final CommentBoDto comment) {
		return new ResponseEntity<>(commentValidationBoService.addComment(comment), HttpStatus.CREATED);
	}

	/**
	 * Modifier un commentaire.
	 *
	 * @param commentId
	 *            id du commentaire.
	 */
	@PutMapping("/{commentId}")
	ResponseEntity<CommentBoDto> updateComment(@PathVariable final Long commentId, @RequestBody final CommentBoDto comment) {
		return new ResponseEntity<>(commentValidationBoService.updateComment(comment), HttpStatus.OK);
	}

	/**
	 * Supprimer un commentaire.
	 *
	 * @param commentId
	 *            id du commentaire.
	 */
	@DeleteMapping("/{commentId}")
	ResponseEntity<Void> deleteComment(@PathVariable final Long commentId) {
		commentValidationBoService.delete(commentId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
