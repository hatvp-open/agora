/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import java.util.List;

import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.SurveillanceDetailDto;

public interface SurveillanceService {
    /**
     * Methode de sauvegarde/MAJ d'une surveillance
     */
    void saveSurveillance(SurveillanceDetailDto surveillance);

    SurveillanceDetailDto updateSurveillance(SurveillanceDetailDto surveillance);

    void deleteSurveillance(Long surveillanceId);

    PaginatedDto<SurveillanceDetailDto> getSurveillancesPaginated(Integer page, Integer occurence);

    List<SurveillanceDetailDto> getSurveillances();

    PaginatedDto<SurveillanceDetailDto> getSurveillancePaginatedBySearch(String type, String searchTerm, Integer pageNumber, Integer resultPerPage);

    List<SurveillanceDetailDto> verifyListOrganisation(List<String> nationalIds);


    void saveListeSurveillance(List<SurveillanceDetailDto> listSurveillance);

    SurveillanceDetailDto getSurveillance(Long organisationId);
}
