/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.backoffice;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.business.CommonService;
import fr.hatvp.registre.commons.dto.backoffice.EspaceOrganisationLockBoDto;

/**
 * Interface de gestion des locks sur les espaces organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
public interface EspaceOrganisationLockBoService
    extends CommonService<EspaceOrganisationLockBoDto>
{

    /**
     * Récupérer le lock d'une validation.
     *
     * @param espaceOrganisationId espace organisation id
     * @return les informations de la validation
     */
    EspaceOrganisationLockBoDto getLockOn(final Long espaceOrganisationId);

    /**
     * Mettre le lock en place.
     *
     * @param espaceOrganisationId id de l'espace organisation
     * @param connectedUserId id de l'utilisateur bo connecté
     */
    void setLockOn(final Long espaceOrganisationId, final Long connectedUserId);

    /**
     * Mise à jour du lock.
     *
     * @param dto DTO du lock de l'espace organisation
     * @param connectedUserId id de l'utilisateur bo connecté
     * @return dto
     */
    EspaceOrganisationLockBoDto updateLockOnEspace(EspaceOrganisationLockBoDto dto, Long connectedUserId);

}
