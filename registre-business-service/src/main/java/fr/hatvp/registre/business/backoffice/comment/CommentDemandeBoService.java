/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.backoffice.comment;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;

/**
 * Interface de gestion des commentaires des demandes.
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
public interface CommentDemandeBoService
    extends CommonCommentService<CommentBoDto>
{

}
