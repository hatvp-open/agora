/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.backoffice;

import fr.hatvp.registre.business.CommonService;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.GroupBoDto;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public interface GroupUtilisateurBoService extends CommonService<GroupBoDto> {

    /**
     * addOrUpdateGroup signature for GroupUtilisateurBoImpl
     * @param groupBoDto
     * @return
     */
    GroupBoDto addOrUpdateGroup(GroupBoDto groupBoDto);

    /**
     * DeleteGroup signature for GroupUtilisateurBoImpl
     * @param id
     */
    void deleteGroup(Long id);


    /**
     * Returns all groups with their respective users paginated
     * @return
     */
    List<GroupBoDto> getAllGroupAndUsers();

    /**
     * Returns all groups with their respective users paginated by search
     * @param type
     * @param keyword
     * @param page
     * @param occurrence
     * @return
     */
    PaginatedDto<GroupBoDto> getGroupsAndusersBySearchPaginated(String type, String keyword, int page, int occurrence);
}
