/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketExerciceDto;

public interface PublicationExerciceService
{
    /**
     * Construire le livrable de publication pour un exercice.
     *
     * @param exerciceId id de l'exercice de l'espace organisation.
     * @param withHistorique : indique si on retourne l'historique de publication des exercices et activités
     * @return le livrable de publication.
     */
    PublicationBucketExerciceDto getPublicationLivrableExercice(final Long exerciceId, boolean withHistorique);

    /**
     * Enregistrer la publication de l'exercice
     *
     * @param connectedUser               informations du publicateur.
     * @param currentEspaceOrganisationId id de l'espace organisation.
     * @param exerciceId                  id de l'exercice de l'espace organisation.
     */
    void savePublicationExercice(final Long connectedUser, final Long currentEspaceOrganisationId,
        final Long exerciceId);

    
    /**
     * Dépublier un exercice.
     *
     * @param id     identifiant exercice
     * @param userId identifiant utilisateur BO
     */
	void depublierExercice(Long exerciceId, Long userId);

	void republierExercice(Long exerciceId, Long userId);
}
