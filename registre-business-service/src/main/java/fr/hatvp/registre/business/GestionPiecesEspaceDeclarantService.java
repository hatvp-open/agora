/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import fr.hatvp.registre.commons.dto.EspaceDeclarantPieceDto;
import fr.hatvp.registre.commons.lists.OrigineVersementEnum;

import java.util.List;

/**
 * Service de gestions des pièces attachées au Declarant
 * 
 * @version $Revision$ $Date${0xD}
 */
public interface GestionPiecesEspaceDeclarantService
    extends CommonService<EspaceDeclarantPieceDto>
{

    /**
     * Sauvegarde 1 pièce  soumises par un déclarant pour lui même.
     *
     * @param idDeclarant déclarant à l'origine du versement.
     * @param piece la piece à enregistrer.
     */
    EspaceDeclarantPieceDto verserPiece(final Long idDeclarant, EspaceDeclarantPieceDto piece, OrigineVersementEnum origine);

    /**
     * Renvoie la liste des pièces versées pour le declarant.
     * @param declarantId le declarant.
     * @return la liste des pièces.
     */
    List<EspaceDeclarantPieceDto> getListePiecesEspaceDeclarant(
        Long declarantId);

    /**
     * Renvoie la liste des pièces versées pour et par le declarant.
     * @param declarantId le declarant.
     * @return la liste des pièces.
     */
    List<EspaceDeclarantPieceDto> getListePiecesAllEspaceDeclarant(
        Long declarantId);
}
