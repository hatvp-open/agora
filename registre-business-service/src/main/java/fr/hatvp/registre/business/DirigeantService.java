/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import fr.hatvp.registre.commons.dto.DirigeantDto;

import java.util.List;

/**
 * Service de gestion des pieces attachés à l'espace collaboratif.
 * 
 * @version $Revision$ $Date${0xD}
 */
public interface DirigeantService
    extends CommonService<DirigeantDto> {

    /**
     * Récupérer liste des Dirigeants pour un espace organisation.
     *
     * @param espaceOrganisationId identifiant de l'espace organisation
     * @return liste des Dirigeants
     */
    List<DirigeantDto> getAllDirigeants(final Long espaceOrganisationId);

    /**
     * Ajouter un nouveau Dirigeant.
     *
     * @param dirigeant informations du Dirigeant
     * @return objet Dirigeant créé
     */
    DirigeantDto addDirigeant(final DirigeantDto dirigeant, final Long espaceOrganisationId);

    /**
     * Supprimer le dirigeant s'il appartient à l'espace passé en paramètre.
     *
     * @param id identifiant du dirigeant à supprimer
     */
    void deleteDirigeant(final Long dirigeantId, final Long espaceOrganisationId);

    /**
     * Modifier les données d'un dirigeant.
     *
     * @param dto donnée à mettre à jour.
     * @return données mises à jour.
     */
    DirigeantDto editDirigeant(DirigeantDto dto, final Long espaceOrganisationId);
    
    /**
     * Remonter le Dirigeant en hierarchie.
     *
     * @param id identifiant du Dirigeant
     */
    void remonterDirigeant(final Long id, final Long espaceOrganisationId);

    /**
     * Faire descendre le Dirigeant en hierarchie.
     *
     * @param id identifiant du Dirigeant
     */
    void descendreDirigeant(final Long id, final Long espaceOrganisationId);


}
