/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;

/**
 * Service de gestions des pièces attachées à l'espace organisation
 * 
 * @version $Revision$ $Date${0xD}
 */
public interface GestionPiecesEspaceOrganisationService
    extends CommonService<EspaceOrganisationPieceDto>
{

    /**
     * Sauvegarde jusqu'à 4 pièces complémentaires soumises par un déclarant pour une organisation donnée.
     * 
     * @param idEspaceOrganisation espace pour lequel les pieces sont versées.
     * @param idDeclarant déclarant à l'origine du versement.
     * @param pieces la listes des pieces à enregistrer.
     */
    void verserPiecesComplementaires(
            final Long idEspaceOrganisation, 
            final Long idDeclarant, 
            List<EspaceOrganisationPieceDto> pieces);

    /**
     * Renvoie la liste des pièces complémentaires versées pour l'espace.
     * @param espaceOrganisationId l'espace consulté.
     * @return la liste des pièces complémentaires.
     */
    List<EspaceOrganisationPieceDto> getListePiecesComplementairesEspaceOrganisation(Long espaceOrganisationId);

    /**
     * Renvoie la liste des pièces versées pour l'espace à l'inscription.
     * @param espaceOrganisationId l'espace consulté.
     * @return la liste des pièces complémentaires.
     */
	List<EspaceOrganisationPieceDto> getListePiecesCreationEspaceOrganisation(Long espaceOrganisationId);

    /**
     * génére le pdf de l'attestation de désinscription
     *
     * @param currentEspaceOrganisationId
     * @return
     */
    Pair<byte[], String> buildPdf(Long currentEspaceOrganisationId);
    /**
     * Génère le PDF ajouté en PJ de l'email de désinscription d'une organisation n'ayant pas
     * publié d'activité ni de moyen
     * @param espaceOrganisationDto
     * @return
     */
    byte[] buildPdfPieceJointeDesinscriptionNoActivite(EspaceOrganisationDto espaceOrganisationDto);
}
