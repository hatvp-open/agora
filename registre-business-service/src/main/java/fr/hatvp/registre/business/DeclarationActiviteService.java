/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import fr.hatvp.registre.commons.dto.activite.ActiviteRepresentationInteretDto;
import fr.hatvp.registre.commons.dto.backoffice.activite.FicheBackDto;

/**
 * Interface de gestion des déclarations d'activitée.
 *
 */
public interface DeclarationActiviteService {

	/**
	 * récupére une déclaration d'activitée
	 *
	 * @param ficheId
	 * @param currentEspaceOrganisationId
	 * @return une déclaration d'activitée
	 */
	ActiviteRepresentationInteretDto getDeclarationActivite(Long ficheId);

	/**
	 * enregistre une déclaration d'activitée
	 *
	 * @param activiteRepresentationInteret
	 * @param currentEspaceOrganisationId
	 * @param currentUserId
	 * @return une déclaration d'activitée
	 */
	ActiviteRepresentationInteretDto createDeclarationActivite(ActiviteRepresentationInteretDto activiteRepresentationInteret, Long currentUserId,
			final Long currentEspaceOrganisationId);

	/**
	 * met à jour une déclaration d'activitée
	 *
	 * @param ficheId
	 * @param activiteRepresentationInteret
	 * @param currentEspaceOrganisationId
	 * @return une déclaration d'activitée
	 */
	ActiviteRepresentationInteretDto updateDeclarationActivite(ActiviteRepresentationInteretDto activiteRepresentationInteret, Long currentEspaceOrganisationId);

	/**
	 * supprime une déclaration d'activitée (uniquement si celle ci n'a pas été publiée)
	 *
	 * @param ficheId
	 * @param currentEspaceOrganisationId
	 * @return rien
	 */
	void deleteDeclarationActivite(Long ficheId);

	/**
	 * récupére une déclaration d'activitée pour l'afficher en back-office
	 *
	 * @param ficheId
	 * @param currentEspaceOrganisationId
	 * @return une déclaration d'activitée
	 */
	FicheBackDto getFicheBO(Long ficheId);
	
	/**
	 * Suppression d'activité publiée ou non à partir du BO
	 * @param activiteId
	 */
	void deleteActiviteBO(Long activiteId);
	
	void deleteListActiviteBO(Long[] activiteIdList);
	/**
	 * retourne une ancienne version d'une fiche
	 * @param publicationFicheId
	 * @return
	 */
	FicheBackDto getFicheHistoriseBO(Long publicationFicheId);

}
