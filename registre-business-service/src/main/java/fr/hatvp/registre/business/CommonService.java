/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import java.util.List;

import fr.hatvp.registre.commons.dto.AbstractCommonDto;

/**
 * Interface générique des services de l'application.
 * 
 * @version $Revision$ $Date${0xD}
 */
public interface CommonService<Dto extends AbstractCommonDto>
{

    /**
     * Méthode de sauvegarde d'une nouvelle entité générique.
     * @param dto Le Dto à enregistrer
     * @return un Dto.
     */
    Dto save(Dto dto);

    /**
     * Méthode générique de récupération d'une entité par son id.
     * @param id L'identifiant de l'entité souhaité
     * @return un Dto.
     */
    Dto findOne(final Long id);

    /**
     * Méthode générique de récupération de toutes les entités.
     * @return liste de Dto.
     */
    List<Dto> findAll();

    /**
     * Méthode générique de suppression.
     * @param id L'identifiant de l'entité à supprimer
     */
    void delete(final Long id);
}
