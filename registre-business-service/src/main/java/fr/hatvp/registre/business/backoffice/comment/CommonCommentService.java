/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.backoffice.comment;

import java.util.List;

import fr.hatvp.registre.business.CommonService;
import fr.hatvp.registre.commons.dto.AbstractCommonDto;
import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;

/**
 * Interface commune de gestion des commentaires.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface CommonCommentService<S extends AbstractCommonDto> extends CommonService<S> {

	List<CommentBoDto> getAllComment(Long targetId);

	S addComment(S commentBoDto);

	CommentBoDto updateComment(CommentBoDto commentBoDto);

}
