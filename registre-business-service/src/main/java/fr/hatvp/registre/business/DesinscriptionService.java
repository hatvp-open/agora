/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import java.util.List;

import fr.hatvp.registre.commons.dto.DesinscriptionDto;
import fr.hatvp.registre.commons.dto.DesinscriptionPieceDto;

public interface DesinscriptionService extends CommonService<DesinscriptionDto> {

    DesinscriptionDto saveDesinscription(DesinscriptionDto desinscriptionDto, List<DesinscriptionPieceDto> piece, Long userId, Long espaceOrganisationId);

    DesinscriptionDto getDesinscription(long espaceId);

    List<DesinscriptionPieceDto> getPiecesDesinscription (long espaceId);

    Integer getNombreDemandeDesinscription();

    Integer getNombreComplementsDemandeDesinscriptionReçu();

    void rejectDemande(long desinscriptionId);

    void askForComplement(long desinscriptionId, String msg, long currentUserId);

    void verserPiecesComplementairesDesinscription(Long idEspaceOrganisation, Long idDeclarant, List<DesinscriptionPieceDto> pieces);

    void sendMailDesinscriptionNoActivite(long espaceId);
    
    void saveDesinscritStatut();

}
