/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import fr.hatvp.registre.commons.dto.ListStatutPubDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableDatesDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableEtDeclarationDto;
import fr.hatvp.registre.commons.dto.activite.ExerciceComptableSimpleDto;
import fr.hatvp.registre.commons.dto.activite.batch.DeclarationReminderDto;
import fr.hatvp.registre.commons.dto.activite.batch.PublicationRelanceDto;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
/**
 * Interface de gestion des exercices comptables
 *
 */
public interface ExerciceComptableService {

	/**
	 * recupère une liste des exercices comptables simplifiés.
	 *
	 * @param currentEspaceOrganisationId
	 * @return une liste des exercices comptables simplifiés
	 */
	List<ExerciceComptableSimpleDto> findAllBasicByEspaceOrganisationId(Long currentEspaceOrganisationId);

	/**
	 * recupère une liste des exercices comptables avec une liste simplifiée des activités liées.
	 *
	 * @param currentEspaceOrganisationId
	 * @return une liste des exercices comptables avec une liste simplifiée des activités liées
	 */
	List<ExerciceComptableEtDeclarationDto> findAllWithDeclarationByEspaceOrganisationId(Long currentEspaceOrganisationId);

	/**
	 * met à jour les données d'un exercice comptable
	 *
	 * @param exerciceId
	 * @param exerciceComptable
	 * @param currentEspaceOrganisationId
	 * @return un exercice comptable
	 */
	ExerciceComptableSimpleDto updateExerciceComptable(Long exerciceId, ExerciceComptableSimpleDto exerciceComptable, Long currentEspaceOrganisationId);

	/**
	 * recupère une liste des exercices comptables avec une liste simplifiée des activités liées pour le back office.
	 *
	 * @param currentEspaceOrganisationId
	 * @return une liste des exercices comptables avec une liste simplifiée des activités liées
	 */
	List<ExerciceComptableEtDeclarationDto> findAllWithDeclarationByEspaceOrganisationIdBack(Long currentEspaceOrganisationId);

	/**
	 * génére un pdf récapitulant l'ensemble des informations d'un exercice (fiches incluses)
	 *
	 * @param exerciceId
	 * @param currentEspaceOrganisationId
	 * @return
	 */
	Pair<byte[], String> buildPdf(Long exerciceId, Boolean withMoyen, List<StatutPublicationEnum> statut, Long currentEspaceOrganisationId);

	/**
	 * met à jour les données d'un exercice comptable
	 *
	 * @param exerciceId
	 * @param exerciceComptable
	 * @param currentEspaceOrganisationId
	 * @return un exercice comptable
	 */
	ExerciceComptableSimpleDto updateDatesExerciceComptable(Long exerciceId, ExerciceComptableDatesDto exerciceComptable, Long espaceOrganisationId);

	/**
	 * précise qu'aucune activité n'a été déclarée pour cette période
	 *
	 * @param exerciceId
	 * @param exerciceComptable
	 * @param currentEspaceOrganisationId
	 * @return un exercice comptable
	 */
	ExerciceComptableSimpleDto updateExerciceComptableNoActivite(Long exerciceId, ExerciceComptableSimpleDto exerciceComptable, Long currentEspaceOrganisationId);

	/**
	 * Récupérer la liste des statuts de publication.
	 *
	 * @return la liste des statuts de publication.
	 */
	ListStatutPubDto getListStatutPubDto();

	/**
	 * Récupérer la liste des exercices sans publication de moyens ou d'activité à partir de leur date de clôture.
	 *
	 * @param dateFin
	 * @return La liste des exercices sans publication de moyens ou d'activité.
	 */
	List<DeclarationReminderDto> getExerciceWithoutDeclarationByDate(LocalDate dateFin);

	/**
	 *
	 * @return
	 */
	Map<Long, DeclarationReminderDto> getFailedReminderMail();

	/**
	 * sauvegarde des relances
	 *
	 * @param relanceDtos
	 * @param today
	 */
	void savePublicationRelance(List<PublicationRelanceDto> relanceDtos, LocalDate today);

	/**
	 *
	 * @param
	 */
	void updatePublicationRelance(List<Long> relanceId, LocalDate today);

	/**
	 * créé des exercices comptables pour tous les espaces
	 *
	 */
	void createExerciceComptableForAllEspace();

	List<ExerciceComptableSimpleDto> initExerciceComptable(Long currentEspaceOrganisationId);

    Boolean hasPublishedAfterCessationDate(long espaceId, LocalDate cessationDate);

}
