/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import fr.hatvp.registre.commons.dto.DesinscriptionDto;
import fr.hatvp.registre.commons.dto.DesinscriptionPieceDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.exceptions.BusinessGlobalException;
import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;


/**
 * Proxy du service Inscription Espace.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface InscriptionEspaceService
    extends CommonService<InscriptionEspaceDto> {

    /**
     * Récupérer inscriptions d'un déclarant.
     *
     * @param declarantId identifiant du déclarant
     * @return liste des inscriptions du déclarant
     */
    List<InscriptionEspaceDto> getAllByDeclarantId(Long declarantId);

    /**
     * Récupérer les demandes en attente.
     *
     * @param espaceOrganisationId identifiant de l'espace organisation
     * @return liste des demandes d'inscription en attente pour l'espace organisation
     */
    List<InscriptionEspaceDto> getAllPendingRequestsByEspaceOrganisationId(
        Long espaceOrganisationId);

    /**
     * Récupérer les inscriptions valides et en cours pour l'espace organisation.
     *
     * @param espaceOrganisationId identifiant de l'espace organisation
     * @return liste des inscriptions validées pour l'espace organisation
     */
    List<InscriptionEspaceDto> getAllResgitrationsByEspaceOrganisationId(Long espaceOrganisationId);

    /**
     * Récupérer les inscriptions valides sans le rôle Opérationnel => soit Admin, pour l'espace organisation.
     *
     * @param espaceOrganisationId identifiant de l'espace organisation
     * @return liste des inscriptions validées pour l'espace organisation
     */
    List<InscriptionEspaceDto> getAllAdminRegistrationsByEspaceOrganisationId(Long espaceOrganisationId);

    /**
     * Récupérer les inscriptions valides avec un rôle Opérationnel => soit Admin, pour l'espace organisation.
     *
     * @param espaceOrganisationId identifiant de l'espace organisation
     * @return liste des inscriptions validées pour l'espace organisation
     */
    List<InscriptionEspaceDto> getAllNonAdminRegistrationsByEspaceOrganisationId(Long espaceOrganisationId);

    /**
     * Valider ou rejeter une demande d'inscription en attente.
     *
     * @param inscriptionId       identifiant inscription
     * @param currentEspaceOrganisationId l'identifiant de l'espace auquel doit appartenir l'inscription.
     * @param validate validée ou rejetée
     * @param envoiEmail indique s'il faut envoyer l'email de notification
     */
    void setPendingRequestValidity(Long inscriptionId, Long currentEspaceOrganisationId, boolean validate, boolean envoiEmail);

    /**
     * Définir espace organisation favoris pour le déclarant.
     *
     * @param id          identifiant inscription
     * @param declarantId identifiant déclarant
     */
    void setFavoriteByDeclarantId(Long id, Long declarantId);

    /**
     * Mettre à jour les droits et rôles d'un déclarant.
     *
     * @param inscriptionId     identifiant de l'inscription
     * @param currentEspaceOrganisationId l'identifiant de l'espace auquel doit appartenir l'inscription.
     * @param roles  liste des rôles du déclarant pour l'espace
     * @param locked verrou de l'espace pour le déclarant
     */
    void setDeclarantRoles(Long inscriptionId, Long currentEspaceOrganisationId, Set<RoleEnumFrontOffice> roles, boolean locked);

    /**
     * Permet l'inscription d'un déclarant à un espace collaboratif lorsque celui-ci n'existe pas.
     * Lance une {@link BusinessGlobalException} si l'espace existe déjà.
     *
     * @param declarantId    id du déclarant.
     * @param organizationId id de l'organisation.
     * @param pieces la liste des pièces à enregistrer à la création
     * @throws BusinessGlobalException si l'espace existe déjà
     */
    InscriptionEspaceDto inscriptionNouvelEspace(Long declarantId,
                                                InscriptionEspaceDto inscriptionEspaceDto, 
                                                List<EspaceOrganisationPieceDto> pieces);

    /**
     * Permet à un déclarant de s'inscrire à un espace collaboratif lorsque celui-ci existe déjà.
     *
     * @param declarantId    id du déclarant.
     * @param organizationId id de l'organisation.
     * @throws BusinessGlobalException si l'espace n'existe pas
     */
    void inscriptionEspaceExistant(Long declarantId, InscriptionEspaceDto inscriptionEspaceDto);

    void demanderDesinscription(Long idEspaceOrganisation, Long idDeclarant, DesinscriptionDto dto, ArrayList<DesinscriptionPieceDto> piece);

    /**
     * Récupérer l'inscription à un espace corporate.
     *
     * @param registrationId id de l'inscription.
     * @param declarantId          id du déclarant.
     * @return l'inscription à un espace organisation.
     */
    InscriptionEspaceDto getRegistrationByIdAndDeclarantId(Long registrationId,
                                                           Long declarantId);

    /**
     * Récupérer l'inscription à un espace corporate
     *
     * @param espaceOrganisationId id de l'espace organisation.
     * @param declarantId          id du déclarant.
     * @return l'inscription à un espace organisation.
     */
    InscriptionEspaceDto getRegistrationByEspaceOrganisationIdAndDeclarantId(Long espaceOrganisationId,
                                                           Long declarantId);

    /**
     * Si l'espace organisation sélectionné est verrouillé, on charge aucun role.
     * Cette manière de faire, permet de garantir ll'integrité des services en appel direct.
     * Etant donné que tous les services sont filtrés par rôle, il n'aura accès à aucun d'entre eux.
     *
     * @param roleEnums        les roles enregistrer dans le context.
     * @param isEspaceDisabled L'etat de l'espace organisation courrant.
     */
    void changeRolesInContext(Set<String> roleEnums, boolean isEspaceDisabled);

    void devenirContactOperationnel(final Long declarantId, final Long espaceOrganisationId,final List<EspaceOrganisationPieceDto> pieces);

}
