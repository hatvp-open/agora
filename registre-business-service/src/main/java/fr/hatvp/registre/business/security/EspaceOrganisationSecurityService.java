/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.security;

/**
 * Proxy du service sécurité de l'espace collaboratif.
 * @version $Revision$ $Date${0xD}
 */
public interface EspaceOrganisationSecurityService
{

    /**
     * Vérification des accès (validés, non vérouillés) utilisateurs pour un espace corporate.
     * Si l'utilisateur en session est verrouillé ou non validé une exception est lancée.
     *
     * @param currentEspaceOrganisationId identifiant de l'espace organisation
     * @param currentUserId identifiant de l'utilisateur
     */
    void checkUserAuthorization(Long currentEspaceOrganisationId, Long currentUserId);

}
