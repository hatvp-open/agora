/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.backoffice;

import java.text.ParseException;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.commons.dto.ChiffresSurUnePeriodeDto;

/**
 */
@Service
public interface ExtractionService {

	ChiffresSurUnePeriodeDto getChiffreSurPeriode(ChiffresSurUnePeriodeDto chiffresSurUnePeriodeDto) throws ParseException;

}
