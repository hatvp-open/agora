/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

public interface RapportActivitePdfService {

	byte[] buildPdf(Long exerciceId, Long currentEspaceOrganisationId);

}
