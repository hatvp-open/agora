/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.backoffice.comment;

import fr.hatvp.registre.commons.dto.backoffice.CommentBoDto;
import org.springframework.stereotype.Service;

/**
 * Interface de gestion des commentaires des déclarants.
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
public interface CommentDeclarantBoService extends CommonCommentService<CommentBoDto> {

}
