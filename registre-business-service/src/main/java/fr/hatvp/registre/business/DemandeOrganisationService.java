

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import java.util.List;


import fr.hatvp.registre.commons.dto.DemandeOrganisationDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.lists.backoffice.StatutDemandeOrganisationEnum;

/**
 * Interface de gestion des Demandes d'ajout d'Organisation.
 *

 * @version $Revision$ $Date${0xD}
 */
public interface DemandeOrganisationService extends CommonService<DemandeOrganisationDto> {

	/**
	 * Récupérer la liste de toutes les demandes.
	 *
	 * @return liste des demandes organisation
	 */
	List<DemandeOrganisationDto> getDemandesOrganisation();

	/**
	 * Récupérer une liste paginée de toutes les demandes.
	 *
	 * @param resultPerPage
	 * @param pageNumber
	 * @param all
	 * @return liste paginée des demandes organisation
	 */
	PaginatedDto<DemandeOrganisationDto> getPaginatedDemandesOrganisation(Integer pageNumber, Integer resultPerPage);

	/**
	 * Récupérer une liste paginée de toutes les demandes par status.
	 *
	 * @param resultPerPage
	 * @param pageNumber
	 * @param all
	 * @return liste paginée des demandes organisation
	 */
	PaginatedDto<DemandeOrganisationDto> getPaginatedDemandesOrganisationByStatus(StatutDemandeOrganisationEnum status, Integer pageNumber, Integer resultPerPage);

	/**
	 * Récupérer une demande d'organisation par id.
	 *
	 * @param id
	 *            identifiant demande
	 * @return la demande organisation
	 */
	DemandeOrganisationDto getDemandeOrganisation(Long id);

	/**
	 * Clôturer demande organisation avec MAJ du statut
	 *
	 * @param id
	 *            identifiant demande organisation
	 * @param statut
	 *            nouveau statut de clôture
	 */
	void cloturerDemande(Long id, StatutDemandeOrganisationEnum statut);

	/**
	 * Sauvegarde et envoi un mail concernant une Demande d'ajout d'une Organisation.
	 *
	 * @param demande
	 *            entité DemandeOrganisationDto du formulaire de contact remplis
	 */
	void saveDemandeOrganisation(final DemandeOrganisationDto demande);

	PaginatedDto<DemandeOrganisationDto> getDemandesOrganisationParRecherche(String type, String keywords,	Integer pageNumber, Integer resultPerPage);

	/**
	 * Change le statut de la demande. Le demandeur est notifié par email de la demande de complément.
	 *
	 * @param id
	 *            identifiant de l'espace corporate créé
	 * @param message
	 *            corps de l'email à envoyer.
	 */
	void demandeComplementInformation(Long id, String corpsEmail);

	Integer getNombreDemandesStatutNouvelle();

}
