/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.backoffice;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.business.CommonService;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoDto;
import fr.hatvp.registre.commons.dto.backoffice.UtilisateurBoGestionDto;

/**
 */
@Service
public interface UtilisateurBoService extends CommonService<UtilisateurBoDto> {

	/**
	 * Cette méthode recherche un utilisateur à partir de son nom d'utiisateur.
	 *
	 * @param username
	 *            username de l'utilisateur.
	 * @return l'objet utilisateur trouvé en bdd.
	 */
	UtilisateurBoDto findUtilisateurByEmail(String username);

	/**
	 * Ajouter un nouveau utilisateur bo.
	 *
	 * @param utilisateurBoDto
	 *            utilisateur à créer.
	 * @return utilisateur DTO créé.
	 */
	UtilisateurBoGestionDto ajouterUtilisateurBo(UtilisateurBoDto utilisateurBoDto);

	/**
	 * Supprimer un utilisatuer bo.
	 *
	 * @param id
	 *            id de l'utilisateur bo.
	 */
	void supprimerUtilisateur(Long id);

	/**
	 * Mise à jour d'un utilisateur bo.
	 *
	 * @param utilisateurBoDto
	 *            données de l'utilisateur.
	 * @return utilisateur DTO à jour.
	 */
	UtilisateurBoDto majUtilisateurBo(UtilisateurBoDto utilisateurBoDto);

	/**
	 * Mise à jour du mot de passe utilisateur.
	 *
	 * @param id
	 *            id de l'utilisateur.
	 * @return utilisateur mis à jour.
	 */
	UtilisateurBoGestionDto updatePassword(Long id);

	/**
	 * Changer le statut d'activation du compte.
	 *
	 * @param utilisateurBoDto
	 *            dto de l'utilisateur à Activer/désactiver.
	 * @return entité utilisateur.
	 */
	UtilisateurBoDto switchActivationCompte(UtilisateurBoDto utilisateurBoDto);

	/**
	 * Récupérer une page de la liste des utilisateurs Bo.
	 *
	 * @param pageNumber
	 * @param resultPerPage
	 * @return
	 */
	PaginatedDto<UtilisateurBoDto> getAllBoUsersPaginated(Integer pageNumber, Integer resultPerPage);

	/**
	 * Récupérer une page de la liste des utilisateurs Bo avec recherche.
	 *
	 * @param type
	 * @param keywords
	 * @param pageNumber
	 * @param resultPerPage
	 * @return
	 */
	PaginatedDto<UtilisateurBoDto> getAllBoUsersPaginatedParRecherche(String type, String keywords, Integer pageNumber, Integer resultPerPage);

	List<String> getMailOfUsersToNotify();

}
