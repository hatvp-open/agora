/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import fr.hatvp.registre.commons.dto.publication.activite.PublicationBucketActiviteDto;

public interface PublicationActiviteService {
	/**
	 * Construire le livrable de publication pour une activite. toutes les publications d'une activites
	 *
	 * @param exerciceId
	 *            id de l'exercice.
	 * @return le livrable de publication.
	 */
	PublicationBucketActiviteDto getPublicationLivrableActivite(final Long activiteId,boolean withHistorique);

	/**
	 * Enregistrer la publication de l'activite
	 *
	 * @param connectedUser
	 *            informations du publicateur.
	 * @param currentEspaceOrganisationId
	 *            id de l'espace organisation.
	 * @param activiteId
	 *            id de l'activite de l'espace organisation.
	 */
	void savePublicationActivite(final Long connectedUser, final Long currentEspaceOrganisationId, final Long activiteId);

	/**
	 * Dépublier une fiche d'activite.
	 *
	 * @param id
	 *            identifiant activite
	 * @param userId
	 *            identifiant utilisateur BO
	 */
	void depublierActivite(Long id, Long userId);

	/**
	 * Republier une fiche d'activite.
	 *
	 * @param id
	 *            identifiant activite
	 * @param userId
	 *            identifiant utilisateur BO
	 */
	void republierActivite(Long id, Long userId);
	
	void republierListActivite(Long[] activiteIdList, Long userId);

	void depublierListActivite(Long[] activiteIdList, Long userId);

}
