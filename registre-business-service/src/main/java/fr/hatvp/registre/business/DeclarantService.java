/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import fr.hatvp.registre.commons.dto.DeclarantContactDto;
import fr.hatvp.registre.commons.dto.DeclarantDto;

/**
 * Interface de gestion des déclarants.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface DeclarantService
    extends CommonService<DeclarantDto>
{

    /**
     * @param email email du déclarant.
     * @param needPassword permet de savoir si on a besoin du password en retour ou non.
     * @return le déclarant trouvé par son email.
     */
    DeclarantDto loadDeclarantByEmail(String email, boolean needPassword);

    /**
     * Enregistrer un déclarant.
     *
     * @param declarantDto déclarant à enregistrer.
     */
    DeclarantDto saveDeclarant(DeclarantDto declarantDto);

    /**
     * Trouver un déclarant à partir de son id.
     *
     * @param id id du déclarant à retrouver.
     * @return le déclarant trouvé par son id.
     */
    DeclarantDto loadDeclarantById(Long id);

    /**
     * Cette méthode permet de:
     * <p>
     * 1- Modifier l'email pricipale d'un declarant {@link DeclarantDto}.
     * 2- Modifier le mot de passe à partir du compte utilisateur.
     * 3- modifier les informations de contact {@link DeclarantContactDto} d'un déclarant
     * {@link DeclarantDto}.
     *
     * @param declarantDto les information à modifiées.
     * @return le déclarant avec les informlations modifiées.
     */
    DeclarantDto updateDeclarant(DeclarantDto declarantDto);

    /**
     * @param key token de validation.
     * @param password mot de passe du déclarant.
     * @return les données du déclarant.
     */
    DeclarantDto activationDeCompte(String key, String password);

    /**
     * @param key token de validation.
     * @param password mot de passe du déclarant.
     * @return les données du déclarant.
     */
    DeclarantDto activationDeLaNouvelleAdresseEmail(String key, String password);

    /**
     * Modification deu mot de passe du déclarant.
     *
     * @param declarant l'entité déclarant du demandeur de modification de mot de passe.
     */
    DeclarantDto updateMotDePasseRecuperationDeCompte(DeclarantDto declarant);

    /**
     * Envoyer un email de récupération de compte.
     *
     * @param email email du déclarant.
     */
    DeclarantDto recuperationDeCompte(String email);

    /**
     * Mise à jour de la dernière date de connexion.
     *
     * @param id declarant id
     */
    void updateLastConnexionDate(Long id);

    void updateFirstConn(Long id);
}
