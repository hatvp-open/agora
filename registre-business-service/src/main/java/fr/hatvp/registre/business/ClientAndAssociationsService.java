/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import java.util.List;

import fr.hatvp.registre.commons.dto.AssoClientBatchDto;
import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.client.ClientSimpleDto;

/**
 * Interface de gestion des clients.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface ClientAndAssociationsService {

	/**
	 * Récupérer une page des clients d'un espace organisation.
	 *
	 * @param pageNumber
	 *            numero de page
	 * @param resultPerPage
	 *            nombre de résultats par page
	 * @param currentEspaceOrganisationId
	 *            id de l'espace organisation
	 * @return une page des clients d'un espace organisation.
	 */
	PaginatedDto<ClientSimpleDto> getPaginatedClientsByEspaceOrganisation(String type, Integer pageNumber, Integer resultPerPage, Long currentEspaceOrganisationId);

	/**
	 * Récupéerer une page des organisations pro d'un espace organisation.
	 *
	 * @param pageNumber
	 *            numero de page
	 * @param resultPerPage
	 *            nombre de résultats par page
	 * @param currentEspaceOrganisationId
	 *            id de l'espace organisation
	 * @return une page des organisations pro de l'espace organisation.
	 */
	PaginatedDto<OrganisationDto> getPaginatedAssociations(Integer pageNumber, Integer resultPerPage, Long currentEspaceOrganisationId);

	/**
	 * Ajout d'un client
	 *
	 * @param espaceOrganisationId
	 * @param organisationDto
	 * @return
	 */
	AssoClientDto addClient(Long espaceOrganisationId, OrganisationDto organisationDto);

	/**
	 * Ajout d'un association
	 *
	 * @param espaceOrganisationId
	 * @param organisationDto
	 * @return
	 */
	AssoClientDto addAssociation(Long espaceOrganisationId, OrganisationDto organisationDto);

	/**
	 * Supprimer un client d'un espace organisation.
	 *
	 * @param clientId
	 *            id du client.
	 * @param espaceOrganisation
	 *            id de l'espace organisation.
	 */
	void deleteClient(Long clientId, Long espaceOrganisation);

	/**
	 * Supprimer une association d'un espace organisation.
	 *
	 * @param associationId
	 *            id du client.
	 * @param espaceOrganisation
	 *            id de l'espace organisation.
	 */
	void deleteAssociation(Long associationId, Long espaceOrganisation);

	/**
	 * Vérifie la possibilité d'ajouter un lot de clients ou associations à un espace organisation.
	 *
	 * @param batchReport
	 *            l'objet de report préparé
	 * @param espaceOrganisationId
	 *            association.
	 * @return Le rapport de ce qui peut être inséré, notamment sans créer de doublon.
	 */
	AssoClientBatchDto verifyAssociationBatch(AssoClientBatchDto batchReport, Long espaceOrganisationId);

	/**
	 * Ajouter un lot d'associations à un espace organisation.
	 *
	 * @param espaceOrganisationId
	 *            id de l'espace organisation.
	 * @param organisationDtos
	 *            id de l'organisation.
	 */
	void addAssociationBatch(Long espaceOrganisationId, List<OrganisationDto> organisationDtos);

	/**
	 * Modifie si un client est ancien ou non
	 *
	 * @param currentEspaceOrganisationId
	 *            id de l'espace organisation.
	 * @param clientId
	 *            id de l'organisation.
	 */
	void changeIsAncienClient(Long clientId, Long currentEspaceOrganisationId);

	/**
	 * Modifie tous les client à ancien ou non
	 *
	 * @param isAncienClient
	 *
	 * @param isAncienClient
	 *            client actif ou inactif
	 *
	 * @param currentEspaceOrganisationId
	 *            id de l'espace organisation.
	 *
	 */
	void changeIsAncienClientAllClients(Boolean isAncienClient, Long currentEspaceOrganisationId);
	
	/**
	 * maj de la période d'activite en cours d'un client lors de sa désactivation 
	 * @param clientSimpleDto
	 */
	void updatePeriodeActiviteClientRepository(ClientSimpleDto clientSimpleDto);
	
	/**
	 * ajout d'une nouvelle période d'activité lors la réactivation d'un client
	 * @param clientSimpleDto
	 */
	void addPeriodeActiviteClientRepository(ClientSimpleDto clientSimpleDto);

}
