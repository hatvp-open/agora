/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import java.util.List;

import fr.hatvp.registre.commons.dto.AssoClientBatchDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.TypeOrganisationDto;

/**
 * This class defines all services pertaining to organization lookup. It basically acts as a "middleman" between external sources of information (sirene repertory, RNA, DUNS, etc.)
 * and our application to provide a unified lookup service for disparate sources of information.
 *
 */
public interface OrganisationService extends CommonService<OrganisationDto> {

	/**
	 * Retourne la liste de toutes les organisations créées par la HATVP.
	 *
	 * @return liste des organisation d'identifiant de type HATVP
	 */
	List<OrganisationDto> getHatvpOrganisations();

	/**
	 * Crée une nouvelle organisation à partir du Back Office.
	 *
	 * @param organisation
	 *            objet du formulaire BO de création d'une organisation
	 * @return numéro HATVP unique généré
	 */
	String creerOrganisation(OrganisationDto organisation);

	/**
	 * Return an organization based on its French national identifier (siren or rna). Can return multiple results with partial siren number ("starts with" lookup).
	 *
	 * @param nationalId
	 *            identifiant national de l'organisation.
	 * @return any organisation starting with the nationalNumber
	 */
	List<OrganisationDto> lookUpByNationalNumber(String nationalId);

	/**
	 * Return an organization List based on their French national identifiers (siren or rna), into a prepared DTO. Cannot return multiple results with partial siren number (=> no more
	 * "starts with" lookup). FIXME: see lookUpByNationalNumber and explain why also multiple are pretended to be returned whereas not possible as repository use strict = in lookup
	 *
	 * @param nationalIdList
	 *            identifiant national de l'organisation.
	 * @return any organisation starting with the nationalNumber
	 */
	AssoClientBatchDto lookUpByNationalNumberBatch(List<String> nationalIdList);

	/**
	 * @param organisationId
	 *            id de l'organisation.
	 * @return les informations de l'organisation.
	 */
	OrganisationDto getOrganizationDataById(Long organisationId);

	/**
	 * Enregistrer le résultat de la rehcerche ds la table organisation;
	 *
	 * @param organisationDto
	 *            organisation à enregistrer.
	 */
	OrganisationDto saveSearchResult(OrganisationDto organisationDto);

	/**
	 * @return liste des types des organisations disponibles.
	 */
	List<TypeOrganisationDto> getListeTypesOrganisations();





}
