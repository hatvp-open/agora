/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import fr.hatvp.registre.commons.dto.CollaborateurDto;

import java.util.List;

/**
 * Interface de gestion des collaborateurs.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface CollaborateurService
    extends CommonService<CollaborateurDto> {

    /**
     * Récupérer liste des Collaborateurs pour un espace organisation.
     *
     * @param espaceOrganisationId identifiant de l'espace organisation
     * @return liste des collaborateurs
     */
    List<CollaborateurDto> getAllCollaborateurs(final Long espaceOrganisationId);

    /**
     * Ajouter un nouveau collaborateur additionnel.
     *
     * @param collaborateur informations du collaborateur additionnel
     * @return objet collaborateur créé
     */
    CollaborateurDto addCollaborateurAdditionel(final CollaborateurDto collaborateur, final Long espaceOrganisationId);

    /**
     * Modifier le flag 'actif' pour un collaborateur inscrit.
     *
     * @param id                   identifiant du collaborateur
     * @param espaceOrganisationId identifiant espace organisation actuel
     * @param actif                si actif ou pas
     */
    void setCollaborateurActif(final Long id, final Long espaceOrganisationId, final boolean actif);

    /**
     * Remonter le collaborateur en hierarchie.
     *
     * @param id identifiant du collaborateur
     */
    void remonterCollaborateur(final Long id, final Long espaceOrganisationId);

    /**
     * Faire descendre le collaborateur en hierarchie.
     *
     * @param id identifiant du collaborateur
     */
    void descendreCollaborateur(final Long id, final Long espaceOrganisationId);

    /**
     * Supprimer le collaborateur de la table.
     *
     * @param id                   identifiant du collaborateur à supprimer
     * @param espaceOrganisationId identifiant espace organisation actuel
     */
    void deleteCollaborateur(final Long id, final Long espaceOrganisationId);

    /**
     * Modifier un collaborateur.
     *
     * @param dto données du collaborateur à editer.
     * @param espaceOrganisationId identifiant de l'espace du collaborateur à modifier.
     * @return collaborateur mis à jour.
     */
    CollaborateurDto editFonctionCollaborateur(CollaborateurDto dto, Long espaceOrganisationId);
}
