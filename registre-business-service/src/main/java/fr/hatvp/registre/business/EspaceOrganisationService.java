/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.hatvp.registre.commons.dto.AssoClientDto;
import fr.hatvp.registre.commons.dto.CollaborateurDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationAffichageFODto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationBlacklistedDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.dto.ListActiviteDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.backoffice.ConsultationEspaceBoSimpleDto;
import fr.hatvp.registre.commons.dto.backoffice.EspaceCollaboratifDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceBoDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceBoSimpleDto;
import fr.hatvp.registre.commons.dto.backoffice.ValidationEspaceDetailBoDto;
import fr.hatvp.registre.commons.dto.batch.EspaceOrganisationPublicationBatchDto;
import fr.hatvp.registre.commons.dto.client.ClientSimpleDto;
import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.commons.lists.backoffice.MotifComplementEspaceEnum;
import fr.hatvp.registre.commons.lists.backoffice.MotifRefusEspaceEnum;

/**
 * Proxy du service Espace Corporate.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface EspaceOrganisationService extends CommonService<ValidationEspaceBoDto> {

	/**
	 * Chargement d'un fichier Logo sur l'espace organisation
	 *
	 * @param id
	 *            identifiant de l'espace organisation
	 * @param logo_file
	 *            données binaires du fichier logo
	 * @param logo_type
	 *            type MIME du fichier logo
	 */
	void uploadLogo(Long id, byte[] logo_file, String logo_type);

	/**
	 * Supprimer Logo de l'espace Organisation
	 *
	 * @param id
	 *            identifiant espace organisation
	 */
	void deleteLogo(Long id);

	/**
	 *
	 * @param id
	 *            id de 'espace
	 * @return l'espaces organisation dont le statut est "En Attente" ou bien "Validation Manager nécessaire".
	 */
	ValidationEspaceDetailBoDto getEspaceOrganisationEnCoursDeValidation(Long id);

	/**
	 *
	 * @param pageNumber
	 *            le numéro de page, en tenant compte ...
	 * @param resultPerPage
	 *            ... du nombre de résultat attendu par page
	 * @return Une page spécifique de la liste des demandes de création d'espaces organisation dont le statut est "En Attente" ou bien "Validation Manager nécessaire".
	 */
	PaginatedDto<ValidationEspaceBoSimpleDto> getEspacesOrganisationEnAttente(Integer pageNumber, Integer resultPerPage);

    /**
     *
     * @param pageNumber
     *            le numéro de page, en tenant compte ...
     * @param resultPerPage
     *            ... du nombre de résultat attendu par page
     * @return La liste des espaces blacklsité.
     */
    PaginatedDto<EspaceOrganisationBlacklistedDto> getEspacesOrganisationBlacklisted(Integer pageNumber, Integer resultPerPage);

	/**
	 *
	 * @param type
	 *            type de recherche effectuée (colonne du tableau)
	 *
	 * @param keywords
	 *            caractères à rechercher
	 *
	 * @param pageNumber
	 *            le numéro de page, en tenant compte ...
	 * @param resultPerPage
	 *            ... du nombre de résultat attendu par page
	 * @return Une page spécifique de la liste des demandes de création d'espaces organisation dont le statut est "En Attente" ou bien "Validation Manager nécessaire".
	 */
	PaginatedDto<ValidationEspaceBoSimpleDto> getEspacesOrganisationEnAttenteParRecherche(String type, String keywords, Integer pageNumber, Integer resultPerPage);

	/**
	 * Modifier flag de publications sur l'espace.
	 *
	 * @param id
	 *            identifiant espace
	 * @param active
	 *            activer ou désactiver publications
	 */
	void setPublicationState(final Long id, final boolean active);

	/**
	 * Valider la création d'un nouvel espace corporate
	 *
	 * @param id
	 *            identifiant de l'espace corporate créé.
	 * @param userId
	 */
	void validateCreationRequest(Long id, Long userId);

	/**
	 * Rejeter la demande de création d'un nouvel espace corporate.
	 *
	 * @param id
	 *            identifiant de l'espace corporate créé
	 * @param message
	 *            corps de l'email à envoyer.
	 */
	void rejectCreationRequest(Long id, Long userId, String message);

	/**
	 * Change le statut de l'espace pour envoi d'une demande de complément pour la création de l'espace. Le demandeur est notifié par email de la demande de complément.
	 *
	 * @param id
	 *            identifiant de l'espace corporate créé
	 * @param message
	 *            corps de l'email à envoyer.
	 */
	void demandeComplementInformation(Long id, String message);

	/**
	 * Générer un message à partir du template du motif donné
	 *
	 * @param id
	 *            identifiant espace organisation pour lequel le refus est envoyé
	 * @param idMotif
	 *            identifiant type de template de motif
	 * @return message généré
	 */
	String generateRejectMessage(Long id, MotifRefusEspaceEnum idMotif);

	/**
	 * Générer un message à partir du template du motif donné
	 *
	 * @param id
	 *            identifiant espace organisation pour lequel la demande est faite
	 * @param motif
	 *            le type de template de motif
	 * @return message le message généré
	 */
	String generateDemandeComplementMessage(Long id, MotifComplementEspaceEnum motif);

	/**
	 * Récupérer la liste des epsaces organisations valides.
	 *
	 * @return liste des espaces organisations validés.
	 */
	List<ValidationEspaceBoDto> getEspacesOrganisationsConfirmes();

	/**
	 *
	 * @param id
	 * @return
	 */
	ValidationEspaceBoDto getEspacesOrganisationConfirmeById(Long id);

	/**
	 * Récupérer une page de la liste des epsaces organisations valides.
	 *
	 * @param pageNumber
	 * @param resultPerPage
	 * @return
	 */
	PaginatedDto<ConsultationEspaceBoSimpleDto> getEspacesOrganisationsConfirmes(Integer pageNumber, Integer resultPerPage);

	/**
	 *
	 * @param type
	 *            type de recherche effectuée (colonne du tableau)
	 *
	 * @param keywords
	 *            caractères à rechercher
	 *
	 * @param pageNumber
	 *            le numéro de page, en tenant compte ...
	 * @param resultPerPage
	 *            ... du nombre de résultat attendu par page
	 * @return Une page spécifique de la liste des demandes de création d'espaces organisation dont le statut est "En Attente" ou bien "Validation Manager nécessaire".
	 */
	PaginatedDto<ConsultationEspaceBoSimpleDto> getEspacesOrganisationsConfirmesParRecherche(String type, String keywords, Integer pageNumber, Integer resultPerPage);

	/**
	 * Changer l'état d'activation de l'espace orgaisation.
	 *
	 * @param espaceId
	 *            id de l'espace organisation.
	 */
	ValidationEspaceBoDto switchActivationStatus(Long espaceId);

	/**
	 * @param espaceId
	 *            id de l'espace organisation.
	 * @return liste des déclarants de l'espace organisation.
	 */
	List<InscriptionEspaceDto> getEspaceOrganisationDeclarant(Long espaceId);

    /**
     * Récupère les informations des clients d'un espace organisation
     * @param espaceId
     * @return
     */
	List<AssoClientDto> getEspaceOrganisationClients(Long espaceId);

    /**
     * Récupère les informations des clients d'un espace organisation
     * @param espaceId
     * @return
     */
	List<CollaborateurDto> getEspaceOrganisationCollaborateurs(Long espaceId);

	/**
	 * Récupérer les informations d'un espace organisation par son id telles qu'elles seront présentées sur le front.
	 *
	 * Ne retourne pas les clients si on déclare ne pas avoir de clients. Ne retourne pas les associations si on déclare ne pas appartenir à des associations.
	 *
	 * @param espaceOrganisationId
	 *            id de l'espace organisation.
	 * @return les informations d'un espace organisation.
	 */
	EspaceOrganisationDto getAllEspaceOrganisationInformations(Long espaceOrganisationId);
	
	/**
	 * Récupérer les informations minimales d'un espace organisation par son id pour l'affichage coté front
	 *
	 * @param espaceOrganisationId
	 *            id de l'espace organisation.
	 * @return les informations d'un espace organisation.
	 */
	EspaceOrganisationAffichageFODto getEspaceOrganisationInformations(Long espaceOrganisationId);

	/**
	 * MAJ Données Espace Organisation de la carte Dirigeants
	 *
	 * @param espaceOrganisationDto
	 *            DTO des données
	 * @return DTO MAJ
	 */
	EspaceOrganisationDto updateDirigeants(final EspaceOrganisationDto espaceOrganisationDto);

	/**
	 * MAJ Données Espace Organisation de la carte Données
	 *
	 * @param espaceOrganisationDto
	 *            DTO des données
	 * @return DTO MAJ
	 */
	EspaceOrganisationDto updateDonnees(final EspaceOrganisationDto espaceOrganisationDto);

	/**
	 * MAJ Données Espace Organisation de la carte Infos_Contact
	 *
	 * @param espaceOrganisationDto
	 *            DTO des données
	 * @return DTO MAJ
	 */
	EspaceOrganisationDto updateInfosContact(final EspaceOrganisationDto espaceOrganisationDto);

	/**
	 * MAJ Données Espace Organisation de la carte Localisation
	 *
	 * @param espaceOrganisationDto
	 *            DTO des données
	 * @return DTO MAJ
	 */
	EspaceOrganisationDto updateLocalisation(final EspaceOrganisationDto espaceOrganisationDto);

	/**
	 * MAJ Données Espace Organisation de la carte Profile_Orga
	 *
	 * @param espaceOrganisationDto DTO des données
	 * @param fromBO si données viennent du BO
	 *            
	 * @return DTO MAJ
	 */
	EspaceOrganisationDto updateProfileOrga(final EspaceOrganisationDto espaceOrganisationDto, boolean fromBO);

	/**
	 * MAJ Données Espace Organisation de la carte Web
	 *
	 * @param espaceOrganisationDto
	 *            DTO des données
	 * @return DTO MAJ
	 */
	EspaceOrganisationDto updateWeb(final EspaceOrganisationDto espaceOrganisationDto);

	/**
	 * Mise à jour des données de l'espace organisation.
	 *
	 * @param dto
	 *            information à mettre à jour.
	 * @param connectedUserId
	 *            données à mettre à jour.
	 * @return l'objet espace organisation à jour.
	 */
	ValidationEspaceDetailBoDto updateEspaceOrganisationEnCoursDeValidation(ValidationEspaceDetailBoDto dto, Long connectedUserId);

	/**
	 * Mise à jour de l'attestation de déclaration des clients.
	 *
	 * @param espaceId
	 *            id de l'espace organisation.
	 * @param espaceOrganisationDto
	 *            données de l'espace organisation.
	 */
	EspaceOrganisationDto setAttestationClient(Long espaceId, EspaceOrganisationDto espaceOrganisationDto);

	/**
	 * Mise à jour de l'attestation de déclaration des associations.
	 *
	 * @param espaceId
	 *            id de l'espace organisation.
	 * @param espaceOrganisationDto
	 *            données de l'espace organisation.
	 */
	EspaceOrganisationDto setAttestationAssociation(Long espaceId, EspaceOrganisationDto espaceOrganisationDto);

	/**
	 * Mise à jour des représentations d'interets.
	 *
	 * @param espaceOrganisation
	 *            données à mettre à jour.
	 * @return données de l'espace organisation à jour.
	 */
	EspaceOrganisationDto updateActivites(EspaceOrganisationDto espaceOrganisation);

	/**
	 * MAJ Données Espace Organisation de la carte Exercice comptable
	 *
	 * @param espaceOrganisationDto
	 *            DTO des données
	 * @return DTO MAJ
	 */
	EspaceOrganisationDto updateExerciceComptable(final EspaceOrganisationDto espaceOrganisationDto);

	/**
	 * Récupérer les listes à afficher dans la rubrique, représentation d'intéret.
	 *
	 * @return les deux liste d'enum à afficher.
	 */
	ListActiviteDto getListeDesActiviteDeRepresentationDinteret();

	/**
	 * Cette méthode permet de déterminer la liste des identifiants et version des espaces qui doivent être publiés.
	 *
	 * @return liste des EspaceOrganisationPublicationBatchDto
	 */
	List<EspaceOrganisationPublicationBatchDto> listeEspacesAPublier();

	/**
	 * Cette méthode permet de déterminer la liste des identifiants et version des espaces qui doivent être dépubliés.
	 *
	 * @return liste des EspaceOrganisationPublicationBatchDto
	 */
	List<EspaceOrganisationPublicationBatchDto> listeEspacesADepublier();

	/**
	 * Cette méthode permet de déterminer la liste des identifiants des espaces dont la publication est active et qui ont au moins une publication active.
	 *
	 * @return liste des identifiants des espaces concernés
	 */
	List<Long> listeIdEspacesPublies();

	/**
	 * Récupérer une liste simplifié des client de l'espace organisation.
	 *
	 * @return liste des clients.
	 */
	List<ClientSimpleDto> getClientEspaceOrganisation(Long currentEspaceOrganisationId);

	void updateFlagElementsAPublier(Long currentEspaceOrganisationId);

	EspaceOrganisationDto updateLienMembres(EspaceOrganisationDto espaceOrganisation, Long currentEspaceOrganisationId);

	void bloquerRelances(Long espaceId, Long userId);

	void debloquerRelances(Long espaceId, Long userId);

	Integer getNombreDemandesParStatut(StatutEspaceEnum statut);

	PaginatedDto<EspaceCollaboratifDto> getNotValidatedEspacePaginated(Integer page, Integer occurrence);

	PaginatedDto<EspaceCollaboratifDto> getEspacesBySearch(String type, String keyword, Integer page, Integer occurrence);

	PaginatedDto<EspaceOrganisationBlacklistedDto> getEspacesBlacklistedParRecherche(String type, String keyword, Integer page, Integer occurence);

	long countNombreEspace();

	void ajouterContactOperationnel(MultipartFile file1, MultipartFile file2, MultipartFile file3,
			Long espaceOrganisationId, Long declarantId, String message);

	List<EspaceOrganisationBlacklistedDto> getAllSpaceNotPaginated();

	EspaceOrganisationDto getEspaceOrganisationDtoById (final Long espaceOrganisationId);

	/**
	 * @param espaceId
	 * @return
	 */
	List<AssoClientDto> getEspaceOrganisationAffiliations(Long espaceId);
	
	
	void forcePublicationEspace(final Long espaceOrganisationId);
}
