/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.dto.activite.QualificationObjetDto;

/**
 * Interface de gestion des qualifications d'objets.
 *
 */
public interface QualificationObjetService
{


    /**
     * Récupère les espaces blacklisté en fonction d'un mot clé
     * @param pageNumber
     * @param resultPerPage
     * @return paginatedDTO des espaces filtrés
     */
    PaginatedDto<QualificationObjetDto> getQualif(Integer pageNumber, Integer resultPerPage);

    /**
     * Récupère toutes les qualifs
     * @return paginatedDTO des espaces
     */
    PaginatedDto<QualificationObjetDto> getAllQualif();

    /**
     * Récupère les objets qualifiés par recherche
     * @param type
     * @param searchTerm
     * @param pageNumber
     * @param resultPerPage
     * @return paginatedDTO des objets qualifiés filtrés
     */
    PaginatedDto<QualificationObjetDto> getQualifParRecherche(String type, String searchTerm, Integer pageNumber, Integer resultPerPage);

    /**
     * Recupère la derniere qualif de l'objet pour une fiche
     *
     * @param idFiche
     * @return le dto de la derniere de qualif de l'objet
     */
    QualificationObjetDto getLastQualiObjetByFicheId(final String idFiche);

	QualificationObjetDto qualifObjetActivite( Long currentEspaceOrganisationId, QualificationObjetDto qualificationObjetDto);

    QualificationObjetDto createQualificationObjet(final QualificationObjetDto qualificationObjetDto, final Long currentEspaceOrganisationId);

    QualificationObjetDto updateQualificationObjet(final Long qualificationObjetId, final String idFiche, final Long currentEspaceOrganisationId);

    /**
     * Requalification des précédents objets en echec (pas de note ni de coeff)
     */
    void repriseQualif();

    }
