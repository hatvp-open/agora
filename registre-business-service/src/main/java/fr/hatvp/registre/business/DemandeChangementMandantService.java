

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import java.util.List;

import fr.hatvp.registre.commons.dto.DemandeChangementMandantDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationPieceDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;
import fr.hatvp.registre.commons.lists.StatutDemandeEnum;
import fr.hatvp.registre.commons.lists.backoffice.MotifComplementEspaceEnum;

/**
 * Interface de gestion des Demandes de changement de Mandant.
 *
 * @version $Revision$ $Date${0xD}
 */
public interface DemandeChangementMandantService extends CommonService<DemandeChangementMandantDto> {

	/**
	 * Récupérer la liste des demandes de changement de mandant.
	 *
	 * @param
	 * @return liste des demandes de changement de mandant
	 */
	PaginatedDto<DemandeChangementMandantDto> getPaginatedDemandesChangementMandant(Integer pageNumber, Integer resultPerPage);

	/**
	 * Récupérer la liste de toutes les demandes de changement de mandant.
	 *
	 * @param espaceOrganisationId
	 * @return liste des demandes de changement de mandant
	 */
	List<DemandeChangementMandantDto> getAllDemandesChangementByEspaceOrganisation(Long espaceOrganisationId);

	/**
	 * Récupérer la demande de changement de mandant.
	 *
	 * @param id
	 *            de la demande
	 * @return la demandes de changement de mandant
	 */
	DemandeChangementMandantDto getDemandeChangementMandant(Long id);

	/**
	 * Rejeter demande de changement de mandant
	 *
	 * @param id
	 *            identifiant demande
	 */
	void rejeterDemande(Long id);

	/**
	 * Valider demande de changement de mandant
	 *
	 * @param id
	 *            identifiant demande
	 */
	void validerDemande(Long id);

	/**
	 * Sauvegarde et envoi un mail concernant une Demande d'ajout d'une Organisation.
	 *
	 * @param demande
	 *            entité DemandeOrganisationDto du formulaire de contact remplit
	 * @param identite
	 * @param mandat
	 */
	void saveDemandeChangementMandant(final DemandeChangementMandantDto demande, EspaceOrganisationPieceDto identite, EspaceOrganisationPieceDto mandat);

	/**
	 * Récupérer la liste de toutes les demandes de changement de mandant par recherche.
	 *
	 * @param espaceOrganisationId
	 * @return liste des demandes de changement de mandant
	 */
	PaginatedDto<DemandeChangementMandantDto> getDemandesParRecherche(String type, String keywords, Integer pageNumber, Integer resultPerPage);

	/**
	 * Change le statut de la demande. Le demandeur est notifié par email de la demande de complément.
	 *
	 * @param id
	 *            identifiant de l'espace corporate créé
	 * @param message
	 *            corps de l'email à envoyer.
	 */
	void demandeComplementInformation(Long id, String corpsEmail);

	/**
	 * Sauvegarde jusqu'à 4 pièces complémentaires pour une demande de complément de changement de mandant.
	 *
	 * @param idEspaceOrganisation
	 *            espace pour lequel les pieces sont versées.
	 * @param idDeclarant
	 *            déclarant à l'origine du versement.
	 * @param pieces
	 *            la listes des pieces à enregistrer.
	 * @param demandeId
	 */
	void verserPiecesComplementaires(Long currentEspaceOrganisationId, Long currentUserId, List<EspaceOrganisationPieceDto> pieces, Long demandeId);

	/**
	 * Générer un message à partir du template du motif donné
	 *
	 * @param id
	 *            identifiant espace organisation pour lequel la demande est faite
	 * @param motif
	 *            le type de template de motif
	 * @return message le message généré
	 */
	String generateDemandeComplementMessage(Long id, MotifComplementEspaceEnum motif);

	Integer getNombreDemandesParStatut(StatutDemandeEnum statut);

}
