/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import fr.hatvp.registre.commons.dto.DesinscriptionPieceDto;

public interface GestionPieceDesinscriptionService extends CommonService<DesinscriptionPieceDto> {

}
