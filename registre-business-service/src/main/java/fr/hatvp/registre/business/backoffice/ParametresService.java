/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.backoffice;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.business.CommonService;
import fr.hatvp.registre.commons.dto.backoffice.ParametresDto;


@Service
public interface ParametresService
    extends CommonService<ParametresDto>
{

    /**
     * Cette méthode active le mode ou le desactive
     *
     * @return le statut du mode.
     */
    Boolean switchActivation();

    /**
     * Set le nombre limite de déclarants
     *
     * @return le nombre limite.
     */
    Integer setLimite(Integer number);

    /**
     * Recuperer les parametres actuels
     *
     * @return le dto.
     */
    ParametresDto getState();

}
