/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.backoffice;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.business.CommonService;
import fr.hatvp.registre.commons.dto.backoffice.DemandeOrganisationLockBoDto;

/**
 * Interface de gestion des locks sur les demandes organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
public interface DemandeOrganisationLockBoService
    extends CommonService<DemandeOrganisationLockBoDto>
{

    /**
     * Récupérer le lock d'une demande.
     *
     * @param demandeOrganisationId demande organisation id
     * @return les informations de la demande
     */
    DemandeOrganisationLockBoDto getLockOn(final Long demandeOrganisationId);

    /**
     * Mettre le lock en place.
     *
     * @param demandeOrganisationId id de la demande
     * @param connectedUserId id de l'utilisateur bo connecté
     */
    void setLockOn(final Long demandeOrganisationId, final Long connectedUserId);

    /**
     * Mise à jour du lock.
     *
     * @param dto DTO du lock de la demande
     * @param connectedUserId id de l'utilisateur bo connecté
     * @return dto
     */
    DemandeOrganisationLockBoDto updateLockOnDemande(DemandeOrganisationLockBoDto dto,
            Long connectedUserId);
}
