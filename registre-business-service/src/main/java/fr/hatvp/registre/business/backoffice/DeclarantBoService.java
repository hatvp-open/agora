/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business.backoffice;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import fr.hatvp.registre.business.CommonService;
import fr.hatvp.registre.commons.dto.DeclarantDto;
import fr.hatvp.registre.commons.dto.EspaceOrganisationDto;
import fr.hatvp.registre.commons.dto.InscriptionEspaceDto;
import fr.hatvp.registre.commons.dto.OrganisationDto;
import fr.hatvp.registre.commons.dto.PaginatedDto;

/**
 * Interface de gestion des déclarants coté backoffice.
 *
 * @version $Revision$ $Date${0xD}
 */
@Service
public interface DeclarantBoService extends CommonService<DeclarantDto> {

	/**
	 * Les noms des organisations du déclarants est la liste des organisations pour
	 * lesquelles le déclarant est rattaché à l’espace collaboratif
	 *
	 * @param id
	 *            id du déclarant.
	 * @return liste de {@link OrganisationDto}.
	 */
	Collection<String> getAllDeclarantOrganisations(Long id);

	/**
	 * Mise à jour de l'état d'activation du compte declarant.
	 *
	 * @param declarantDto
	 *            information du declarant à modifier.
	 * @return un {@link DeclarantDto}.
	 */
	DeclarantDto switchActivationCompte(DeclarantDto declarantDto);

	/**
	 * Méthode de mise à jour de l'mail principal du déclarant.
	 *
	 * @param declarantDto
	 *            données du déclarant à modifié.
	 * @return un {@link DeclarantDto}.
	 */
	DeclarantDto updateEmailDeclarant(DeclarantDto declarantDto);

	/**
	 * * Cette méthode permet bien de valider le changement de mail demandé par le
	 * déclarant au niveau front, dans le cas où il fait un recours "SAV", notamment
	 * si son délais d'expiration pour l'utilisation du lien d'activation est
	 * expiré.
	 *
	 * @param declarantDto
	 *            objet déclarant en entrée.
	 * @return les informations du déclarant mises à jour.
	 */
	DeclarantDto validateDeclarantEmailUpdate(DeclarantDto declarantDto);

	/**
	 * liste des espaces collaboratifs du déclarant
	 *
	 * @param declarantId
	 *            id du déclarant.
	 * @return liste de {@link EspaceOrganisationDto}
	 */
	List<InscriptionEspaceDto> getAllDeclarantEspaceOrganisation(Long declarantId);

	/**
	 * @param declarantDto
	 *            declrant dto à modifié.
	 * @return le {@link DeclarantDto} modifié.
	 */
	DeclarantDto updateDeclarant(DeclarantDto declarantDto);

	/**
	 * Récupérer la liste de tous les déclarants.
	 *
	 * @return liste de {@link DeclarantDto}
	 */
	List<DeclarantDto> findAllDeclarant();

	/**
	 * Récupérer une liste paginée de tous les déclarants.
	 *
	 * @return le {@link PaginatedDto}.
	 */
	PaginatedDto<DeclarantDto> findPaginatedDeclarant(Integer pageNumber, Integer resultPerPage);

	/**
	 * Récupérer une liste paginée de tous les déclarants par recherche
	 *
	 * @return le {@link PaginatedDto}.
	 */
	PaginatedDto<DeclarantDto> findPaginatedDeclarantParRecherche(String type, String keywords, Integer pageNumber, Integer resultPerPage);

	/**
	 * Récupérer les informations du déclarant et sa liste d'organisations
	 *
	 * @return le {@link DeclarantDto}
	 */
	@Override
	DeclarantDto findOne(Long declarantId);


	DeclarantDto loadDeclarantByEmail(String mail);

}
