/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.business;

import java.util.List;
import java.util.Map;

import fr.hatvp.registre.commons.dto.publication.PublicationBucketDto;
import fr.hatvp.registre.commons.dto.publication.PublicationDto;
import fr.hatvp.registre.commons.dto.publication.PublicationFrontDto;
import fr.hatvp.registre.commons.dto.publication.PublicationMetaDto;
import fr.hatvp.registre.commons.dto.publication.PublicationRechercheBlacklistDto;
import fr.hatvp.registre.commons.dto.publication.PublicationRechercheDto;
import fr.hatvp.registre.commons.dto.publication.activite.PublicationActiviteRechercheDto;

/**
 * Interface de service pour les publications
 *
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationService extends CommonService<PublicationFrontDto> {

	/**
	 * Enregistrer une publication.
	 *
	 * @param currentEspaceOrganisationId
	 *            identifiant de l'espace à publier.
	 * @param connectedUser
	 *            utilisateur connecté.
	 * @param httpSessionId
	 *            identifiant de la session utilisé pour stocker temporairement la publication.
	 * @return données de la publication.
	 */
	void savePublication(Long connectedUser, Long currentEspaceOrganisationId, String httpSessionId);

	/**
	 * Dépublier une publication.
	 *
	 * @param id
	 *            identifiant publication
	 * @param userId
	 *            identifiant utilisateur BO
	 */
	void depublier(Long id, Long userId);

	/**
	 * Republier une publication.
	 *
	 * @param id
	 *            identifiant publication
	 * @param userId
	 *            identifiant utilisateur BO
	 */
	void republier(Long id, Long userId);

	/**
	 * Récupérer la dernière publication d'un espace organisation.
	 *
	 * @param espaceId
	 *            id de l'espace organisation.
	 * @return données de la publication.
	 */
	PublicationFrontDto getDernierePublication(Long espaceId);

	/**
	 * Construit la prochaine publication d'un espace organisation.
	 *
	 * @param espaceId
	 *            id de l'espace organisation.
	 * @param httpSessionId
	 *            identifiant de la session où l'on fait la publication
	 * @return données de la publication à venir.
	 */
	PublicationFrontDto getNouvellePublication(Long espaceId, String httpSessionId);

	/**
	 * Récupérer la dernière publication d'un espace organisation dans le bon format pour la rendre public. (Utilisé pour la construction du fichier opendata).
	 *
	 * @param espaceId
	 *            id de l'espace organisation.
	 * @return données de la dernière publication.
	 */
	PublicationDto getDernierContenuPourPublication(Long espaceId);

	/**
	 * Récupérer la liste des publications d'un espace organisation
	 *
	 * @param espaceId
	 *            id de l'espace organisation
	 * @return liste des publications
	 */
	List<PublicationMetaDto> getPublicationsEspace(Long espaceId);

	/**
	 * Construire le livrable de publication pour un espace orgaisation.
	 *
	 * @param espaceId
	 *            id de l'espace organisation.
	 * @return le livrable de publication.
	 */
	PublicationBucketDto getPublicationLivrable(Long espaceId);

	/**
	 * Construit le livrable des identites pour le document de recherche des publications.
	 *
	 * @return représentation objet du livrable à transformer en JSON
	 */
	List<PublicationRechercheDto> getPublicationsRechercheLivrable();

	/**
	 * Construit le livrable des activites pour le document de recherche des publications.
	 *
	 * @return représentation objet du livrable à transformer en JSON
	 */
	List<PublicationActiviteRechercheDto> getPublicationsActivitesRechercheLivrable();

    /**
     * Calcule le nombre d'activités total publiées, pour affichage sur la page
     *  d'accueil du registre des RI.
     *
     * @return le nombre total d'activités publiées
     */
    Integer getNbPublicationsActivites();
	/**
	 * Permet de supprimer une publication stockée temporairement pour la session. Apelé au moment de la destruction de la session par SessionEndedListener#onApplicationEvent
	 *
	 * @param id
	 *            l'identifiant de la session
	 */
	void viderSessionPublication(String id);

	/**
	 * Construit le livrable global (identite et activites)pour le document de recherche des publications.
	 *
	 * @return représentation objet du livrable à transformer en JSON
	 */
	List<Object> getPublicationsGlobalesRechercheLivrable();

	List<PublicationRechercheDto> getEspacesAvecActivitesPubliees();

	List<PublicationRechercheBlacklistDto> getEspacesBlacklistes();

	Map<String, String> countEspaceInscritWithDifferenceThroughTime();

    /**
     * retourne true si l'espace à publier son identité
     *
     * @param inscriptionEspaceId
     * @return Boolean
     */
    Boolean hasPublishedIdentite(long inscriptionEspaceId);

    Integer hasPublished(long inscriptionEspaceId);

    /**
     *  appelle la méthode haspublished en récupérant l'inscription id avec l'espace id
     * @param espaceId
     * @return
     */
    Integer hasPublishedWithEspaceId(long espaceId);
    
    Map<String, String> getCountPublications(long espaceId);

    Long countByStatut();
}
