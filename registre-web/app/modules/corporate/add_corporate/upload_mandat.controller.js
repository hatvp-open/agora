/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.corporate')

/**
 * Controlleur pour l'étape de confirmation/upload de mandat pour l'espace organisation
 * JOIN ORGANIZATION - STEP(2-B)
 */
    .controller('UploadMandatCtrl', ['$scope', '$rootScope', 'blockUI', '$mdDialog', 'CorporateAddService', '$log',
        function ($scope, $rootScope, blockUI, $mdDialog, CorporateAddService, $log) {
            /***********************************************************
             * Initialisation des variables
             ***********************************************************/
            var vm = $scope;
            vm.organisation = $rootScope.organisation;
            vm.maxFileSizeInMB = $rootScope.getMaxUploadSizeInMB();
            vm.files01 = [];
            vm.files02 = [];
            vm.files03 = [];
            vm.files04 = [];

            /**
             * Méthode pour la soumission du formulaire de création d'un nouvel espace.
             * @param uploadForm
             * @param representantLegal
             * @returns {boolean}
             */
            vm.submit = function (valid, representantLegal) {
                if(valid) {
                    blockUI.start();
                    
                    // Récupération dans un object des fichiers chargés.
                    // On récupère le mandat uniquement si la case représentant légal est décochée.
                    var formData = new FormData();
                    vm.files01.length > 0 && formData.append('file1', vm.files01[0].lfFile);
                    vm.files02.length > 0 && !vm.representantLegal && formData.append('file2', vm.files02[0].lfFile);
                    vm.files03.length > 0 && formData.append('file3', vm.files03[0].lfFile);
                    vm.files04.length > 0 && vm.selectedOrganization.type === 'RNA' && formData.append('file4', vm.files04[0].lfFile);

                    delete vm.organisation["remoteAddress"];
                    delete vm.organisation["declareForTiers"];
                    vm.organisation.creationDate = null;
                    formData.append("organisation", JSON.stringify(vm.organisation));
                    formData.append("representantLegal", representantLegal);
                    formData.append("association", vm.selectedOrganization.type === 'RNA');

                    CorporateAddService.uploadMandat(formData).then(function (res) {
                        //upload success
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Votre espace collaboratif a bien été créé')
                                .textContent("Les rôles 'contact opérationnel', 'publicateur' et 'contributeur' vous seront attribués après validation de la Haute Autorité. En attendant cette validation, vous pouvez commencer à renseigner les éléments relatifs à votre organisation et les consulter dans votre espace publication. En revanche, vous ne pourrez pas publier ces informations pour le moment.")
                                .ok('OK')
                        );

                        $rootScope.organisation = null;
                        $rootScope.selectCorporate(res.data);
                    }).finally(function () {
                        blockUI.stop();
                    })
                }
            };

        }]);
