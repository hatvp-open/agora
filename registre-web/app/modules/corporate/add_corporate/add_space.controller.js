/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.corporate')

/**
 * Controlleur pour l'écran d'ajout d'un nouvel espace corporate
 * AUTO-COMPLETE ORGANIZATION - STEP(1)
 * STEP -2 COMMONS
 * CREATE ORGANIZATION - STEP(2-A)
 */
    .controller('CorporateAddCtrl', ['$log', 'TYPE_RECHERCHE', 'SearchFactory', '$scope', 'CorporateAddService', '$mdDialog', 'EnvConf', 'HTML', '$timeout', '$q', 'OrganisationService', '$state', '$rootScope', 'blockUI',
        function ($log, TYPE_RECHERCHE, SearchFactory, $scope, CorporateAddService, $mdDialog, EnvConf, HTML, $timeout, $q, OrganisationService, $state, $rootScope, blockUI) {

            var vmCorporate = $scope;

            /***********************************************************
             * Initialisation des variables
             ***********************************************************/
            vmCorporate.add_corporate = {
                SEARCH_TEMPLATE: EnvConf.views_dir + HTML.SEARCH,
                STEP1A: EnvConf.views_dir + HTML.corporate_template.add_corporate.STEP1,
                STEP1B: EnvConf.views_dir + HTML.corporate_template.add_corporate.STEP2,
                CREATE_SPACE: EnvConf.views_dir + HTML.corporate_template.add_corporate.STEP3,
                JOIN_SPACE: EnvConf.views_dir + HTML.corporate_template.add_corporate.STEP4
            };

            vmCorporate.current_step = vmCorporate.add_corporate.STEP1A;

            vmCorporate.espace_clients = [];

            vmCorporate.selectedOrganization = null;
            vmCorporate.validOrganization = false;
            vmCorporate.representantLegal = false;

            SearchFactory.set(vmCorporate, TYPE_RECHERCHE.AJOUTER_OU_REJOINDRE_ESPACE);

            /***********************************************************
             * Déclaration des fonctions dans le scope.
             ***********************************************************/

            vmCorporate.validateStep1 = function () {
                if (vmCorporate.selectedOrganization === null) return false;
                vmCorporate.showDetails = true;
                vmCorporate.selectedOrganization.data.declareForTiers = false;
            };

            //Retour arrière.
            vmCorporate.goBackToChangeOrganization = function (event) {
                vmCorporate.current_step = vmCorporate.add_corporate.STEP1A;
            };

            //Controle des étapes
            vmCorporate.createOrJoinOrganizationSpace = function (event) {
                if (!vmCorporate.showDetails) return false;
                $rootScope.organisation = vmCorporate.selectedOrganization.data;
                $rootScope.organisation.declareForTiers = vmCorporate.selectedOrganization.data.declareForTiers;
                vmCorporate.selectedOrganization.organizationSpaceExist ? vmCorporate.current_step = vmCorporate.add_corporate.JOIN_SPACE : vmCorporate.current_step = vmCorporate.add_corporate.CREATE_SPACE
            };

            //Rejoindre un espace organisation
            vmCorporate.confirmJoinOrganizationSpace = function (event) {
                blockUI.start();
                CorporateAddService.joinCreateSpace(vmCorporate.selectedOrganization.data).then(function (response) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.body))
                            .clickOutsideToClose(true)
                            .title('Inscription réussie')
                            .textContent("Votre demande d'inscription a bien été transmise au contact opérationnel.")
                            .ok('OK')
                            .targetEvent(event)
                    );

                    $state.go("home", {}, {reload: true});
                    $rootScope.organisation = null;
                }, function (res) {
                }).finally(function(){
                    blockUI.stop();
                });
            };

            /**
             * contact HATVP
             * @param ev
             */
            vmCorporate.contact = function (ev) {
                $mdDialog.show({
                    controller: 'ContactCtrl',
                    templateUrl: EnvConf.views_dir + HTML.CONTACT,
                    locals: {
                        context: 'Ajout_Espace'
                    },
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            };
        }
    ]);

