
/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.home')

    .controller('UnsubscribeCtrl', ['$scope', '$rootScope', '$state', '$mdDialog', '$log', 'EspaceOrganisationService', 'blockUI', '$q', 'EnvConf','API','HTML','PublicationService',
        function ($scope, $rootScope, $state, $mdDialog, $log, EspaceOrganisationService, blockUI, $q, EnvConf,API, HTML, PublicationService) {
            var vm = $scope;
            vm.maxFileSizeInMB = $rootScope.getMaxUploadSizeInMB();
            /***********************************************************
             * Initialisation des variables
             ***********************************************************/
            vm.espace = $rootScope.currentCorporate();
            vm.desinscription = {
                motif: "",
                observationDeclarant: "",
                cessationDate: null,
            };
            vm.file= [];
            vm.cessationDate = "";
            vm.pubCount = {};
            vm.currentExercice = {};

            /**
             * URL pour le service de téléchargement de l'attestation de désinscription.
             */
            vm.attestationDesinscriptionDownloadUrl = EnvConf.api + API.corporate.desinscription.downloadAttest;

            vm.init = function(){
                PublicationService.getCountPublications(vm.espace.id).then(function (res) {
                    vm.pubCount = res.data;
                });
            };
            vm.init();
            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            vm.unsubscribe = function (valid) {
                if (valid) {
                    blockUI.start();
                    var dateDuJour = new Date;
                    var formData = new FormData();
                    var dayCessation = vm.cessationDate.slice(0, 2);
                    var monthCessation = vm.cessationDate.slice(2, 4);
                    var yearCessation = vm.cessationDate.slice(4, 9);
                    var hasPublication = (vm.pubCount.nbrePubActivites != 0 || vm.pubCount.nbrePubMoyens != 0);

                    if((dateDuJour.getFullYear().toString().padStart(4,'0')> yearCessation && hasPublication)
                        || ((dateDuJour.getMonth()+1).toString().padStart(2, '0') > monthCessation && hasPublication)
                        || (dateDuJour.getDate().toString().padStart(2, '0') > dayCessation && hasPublication)){
                        // on refuse
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Date de cessation invalide")
                                .textContent("Vous ne pouvez pas renseigner de date de cessation d’activités puisque vous avez déclaré des activités pour une période postérieure à la demande, merci de contacter les services de la Haute Autorité.")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        );
                        blockUI.stop();
                        $state.reload();
                    }else{
                        // on accepte
                        vm.desinscription.cessationDate = dayCessation + "-" + monthCessation + "-" + yearCessation;
                        angular.forEach(vm.file, function (obj) {
                            formData.append("files", obj.lfFile);
                        });
                        formData.append("demandeDesinscription", JSON.stringify(vm.desinscription));
                        EspaceOrganisationService.unsubscribe(formData).then(function (res) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Votre demande a été prise en compte")
                                    .textContent("Une fois validée par les services de la Haute Autorité, vous serez notifié de la désinscription de votre organisation du répertoire des représentants d’intérêts.")
                                    .ariaLabel("Votre demande a été prise en compte")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            );
                        }).finally(function () {
                            blockUI.stop();
                            $state.reload();
                        })
                    }
                }
            };

            // Modale pour ajouter une nouvelle demande
            vm.formDesinscription = function () {
                $mdDialog.show({
                    controller: 'UnsubscribeCtrl',
                    locals: {
                        item: {},
                        edit: false
                    },
                    templateUrl:  EnvConf.views_dir + HTML.HOME.UNSUBSCRIBE,
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    fullscreen: false
                });
            };

            //téléchargement de l'attestation
            vm.downloadAttestPdf = function () {
                EspaceOrganisationService.downloadAttestPdf();
            };

            vm.desinscriptionNoActivite = function () {
                EspaceOrganisationService.desinscriptionNoActivite().then(function () {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Attention")
                            .textContent("Votre demande ne sera prise en compte qu'à réception de la demande formelle, datée et signée par le représentant légal de "+ vm.espace.organisationDenomination + " dont le modèle vous a été envoyé par mail.")
                            .ariaLabel("Attention")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    );
                })
            };
            /***********************************************************
             * Initialisation du controlleur.
             ***********************************************************/
            var initController = function () {
                /* Démarrage spinner */
                blockUI.start();


                /* Ici les promises : on attends les réponses des appels à l'API pour masquer le spinner */
                $q.all([
                    //* Placer les appels asynchrones ici. */
                ]).finally(function () {
                    /* Arrêt spinner */
                    blockUI.stop();
                });
            };
            initController();

        }]);