/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.home')

/**
 * Controlleur de la page d'accueil
 */
    .controller('HomeCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'InscriptionEspaceService', 'LimiteService', 'EnvConf', 'HTML', 'AppFactory', 'USER_ROLES',
        function ($scope, $rootScope, $state, $stateParams, InscriptionEspaceService, LimiteService, EnvConf, HTML, AppFactory, USER_ROLES ) {
            var vm = $scope;

            /**
             * Home view options
             */
            vm.loginView = true;
            vm.home_views = {
                login: EnvConf.views_dir + HTML.HOME.LOGIN,
                signup: EnvConf.views_dir + HTML.HOME.SIGNUP
            };

            vm.limitation = false;
            vm.limite = 0;

            // Recuperation des parametres
            LimiteService.getState().then(function (params) {
                vm.limitation = params.data.active;
                vm.limite = params.data.nombreLimite;
            });

            // Corporate home forward when user authenticated
            if ($rootScope.isConnected()) {
                InscriptionEspaceService.refresh().then(function (corporates) {
                    $rootScope.corporates = corporates;
                    var currentCorporate = $rootScope.currentCorporate();

                    // si un ou plusieurs inscriptions espace corporate
                    // redirection vers écran accueil corporate si existe
                    if (currentCorporate !== null) {
                        // si Inscription Espace courrant active (non verrouillé, validé et espace actif)
                        // aller sur la page de contribution de l'espace
                        if (AppFactory.isCorporateActive(currentCorporate)) {
                            // Si utilisateur a des droits pour accéder à la page
                            if (currentCorporate.roles.length > 0) {
                                $state.go('identite', {}, {location: 'replace'});
                            }
                            // Si pas de rôle Contributeur, afficher page erreur
                            else {
                                $state.go('error', {}, {location: 'replace'});
                            }
                        }
                        // sinon aller sur une page affichant le message selon le cas
                        else {
                            $state.go('error', {}, {location: 'replace'});
                        }
                    }
                    // si aucun espace corporate
                    // redirection vers écran ajout corporate sinon
                    else {
                        $state.go('corporateAdd', {}, {location: false});
                    }
                });
            }

        }]);