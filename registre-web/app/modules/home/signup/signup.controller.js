/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.home')
// .value('uiMaskConfig', {
//     maskDefinitions: {
//         '9': /\d/,
//         'A': /[a-zA-Z]/,
//         '*': /[a-zA-Z0-9]/
//     },
//     clearOnBlur: true
// })
    .controller('SignupCtrl', ['$scope', '$rootScope', '$state', '$mdDialog', '$log', 'ProfileService', 'AuthService', 'blockUI', '$q',
        function ($scope, $rootScope, $state, $mdDialog, $log, ProfileService, AuthService, blockUI, $q) {
            var vm = $scope;
            vm.maxFileSizeInMB = $rootScope.getMaxUploadSizeInMB();

            /***********************************************************
             * Initialisation des variables
             ***********************************************************/
            vm.user = {
                civilite: "M",
                nom: "",
                prenom: "",
                date_naissance: null,
                email: "",
                emails_complement: [],
                telephones: [],
                password: ""
            };

            vm.files01 = [];

            /***********************************************************
             * Déclaration des fonctions dans le scope.
             ***********************************************************/
            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            vm.addEmail = function () {
                vm.user.emails_complement.push({email: "", categorie: ""});
            };

            vm.addTel = function () {
                vm.user.telephones.push({telephone: "", categorie: ""});
            };

            vm.remEmail = function (i) {
                vm.user.emails_complement.splice(i, 1);
            };

            vm.remTel = function (i) {
                vm.user.telephones.splice(i, 1);
            };

            /**
             * Appel service d'inscription
             */
            vm.signup = function (ev, valid, form) {
                if (valid) {// Si formulaire valide
                    blockUI.start();

                    if(vm.user.date_naissance.length == 8){
                        var day = vm.user.date_naissance.slice(0, 2);
                        var month = vm.user.date_naissance.slice(2, 4);
                        var year = vm.user.date_naissance.slice(4, 9);
                        vm.user.date_naissance = day + "/" + month + "/" + year;
                    }
                    vm.user.emails_complement = vm.user.emails_complement.filter(filterEmails);
                    vm.user.telephones = vm.user.telephones.filter(filterPhones);

                    //var input = angular.element(element[0].querySelector('#fileInput'));

                    var formData = new FormData();
                    vm.files01.length > 0 && formData.append('file1', vm.files01[0].lfFile);
                    formData.append("declarant", JSON.stringify(vm.user));

                    AuthService.init().then(function (res) {
                        ProfileService.register(formData).then(function (response) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Votre demande a été prise en compte")
                                    .textContent("Pour terminer votre inscription, merci de cliquer sur le lien d'activation qui vous a été envoyé à l'adresse suivante : " + response.data.email + ".")
                                    .ariaLabel("Votre demande a été prise en compte")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            ).then(function () {
                                $state.go('home', {}, {reload: true});
                            });
                        }, function (err) {
                            if (vm.user.emails_complement.length === 0)
                                vm.addEmail();
                            if (vm.user.telephones.length === 0)
                                vm.addTel();
                        }).finally(function () {
                            blockUI.stop();
                        });
                    }, function (err) {
                        blockUI.stop();
                    });

                }
                else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Formulaire invalide")
                            .textContent("Au moins un champ de votre formulaire d'inscription est incorrect. Veuillez corriger votre formulaire pour compléter votre inscription.")
                            .ariaLabel("Formulaire invalide")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    );
                }
            };

            /***********************************************************
             * Déclaration des fonctions privées.
             ***********************************************************/
            function filterEmails(element) {
                return (element.categorie.length !== 0 && element.email.length !== 0);
            }

            function filterPhones(element) {
                return ((element.categorie !== undefined && element.categorie.length !== 0) && (element.telephone !== undefined && element.telephone.length != 0));
            }

            /***********************************************************
             * Initialisation du controlleur.
             ***********************************************************/
            var initController = function () {
                /* Démarrage spinner */
                blockUI.start();

                /* Ici les fonctions sans appel au serveur */
                vm.addEmail();
                vm.addTel();

                /* Ici les promises : on attends les réponses des appels à l'API pour masquer le spinner */
                $q.all([
                    /* Placer les appels asynchrones ici. */
                ]).finally(function () {
                    /* Arrêt spinner */
                    blockUI.stop();
                });
            };
            initController();

        }]);