/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.home')

    .controller('LoginCtrl', ['$scope', '$log', '$window', '$mdDialog', '$mdToast', '$location', 'AuthService', '$rootScope', '$state', 'ProfileService', 'blockUI',
        function ($scope, $log, $window, $mdDialog, $mdToast, $location, AuthService, $rootScope, $state, ProfileService, blockUI) {
            var vm = $scope;
            vm.sessionsCount = $rootScope.sessionCount;
            /***********************************************************
             * Initialisation des variables
             ***********************************************************/
            vm.authModel = {
                email: "",
                password: ""
            };

            /***********************************************************
             * Déclaration des fonctions dans le scope.
             ***********************************************************/
            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
                $state.go('home');
            };

            /**
             * Appel service d'authentification
             * @param ev
             * @param valid
             */
            vm.login = function (ev, valid) {
                if (valid) {// Si formulaire valide
                    $mdDialog.hide();
                    blockUI.start();
                    AuthService.init().then(function (res) {
                        AuthService.authenticate(vm.authModel).then(function (response) {
                            switch (response.status) {
                                case 200:// Authentification success
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .theme('info')
                                            .textContent('Vous êtes maintenant authentifié.')
                                            .action('Fermer')
                                            .position('top right')
                                            .hideDelay(8000)
                                    );

                                    if ($rootScope.targetState)
                                        $state.go($rootScope.targetState, {}, {reload: true});
                                    else
                                        $state.go('home', {}, {reload: true});
                                    break;
                            }
                        }).finally(function () {
                            blockUI.stop();
                        });
                    }, function (err) {
                        blockUI.stop();
                    });
                }
                else {
                    $log.error("Formulaire d'authentification non valide")
                }

            };

            /**
             * Appel service d'oubli de mot de passe
             * @param email
             * @param ev
             */
            vm.forgotPassword = function (email, ev) {
                $mdDialog.hide();
                var confirm = $mdDialog.prompt()
                    .title('Mot de passe oublié')
                    .textContent('Veuillez renseigner votre adresse courriel de connexion :')
                    .placeholder('Adresse courriel principale')
                    .ariaLabel('Adresse courriel principale')
                    .initialValue(email)
                    .targetEvent(ev)
                    .ok('Récupérer')
                    .cancel('Abandonner');

                $mdDialog.show(confirm).then(function (email_result) {
                    blockUI.start();
                    if (email_result.length > 0) {
                        AuthService.init().then(function (res) {
                            ProfileService.init_password_1(email_result).then(function (res) {
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .targetEvent(ev)
                                        .title("Mot de passe oublié")
                                        .textContent("Un courriel de réinitialisation de votre mot de passe vous a été envoyé sur votre adresse de connexion.")
                                        .ariaLabel("Mot de passe oublié")
                                        .ok('OK')
                                        .clickOutsideToClose(true)
                                );
                            }).finally(function () {
                                blockUI.stop();
                            });
                        }, function (err) {
                            blockUI.stop();
                        });
                    } else {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .targetEvent(ev)
                                .title("Adresse courriel principale requise")
                                .textContent("Merci de renseigner votre adresse courriel de connexion pour réinitialiser votre mot de passe.")
                                .ariaLabel("Adresse courriel principale requise")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        );
                        blockUI.stop();
                    }
                });
            };

        }]);