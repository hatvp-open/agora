/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.home')

    .controller('EmptyformCtrl', ['$scope', '$rootScope', '$state',
        function ($scope, $rootScope, $state) {
            var vm = $scope;
            vm.currentNavItem;

            if ($state.current.name == 'emptyforms.emptyMoyens') {
                vm.currentNavItem = 'moyens';
            } else {
                vm.currentNavItem = 'activites';
            }
        }])

    .controller('EmptyMoyensCtrl', ['$scope', 'EnvConf', 'HTML',
        function ($scope ) {

            var vm = $scope;


        }])

    .controller('EmptyActivitesCtrl', ['$scope', '$state', 'EnvConf', 'HTML',
        function ($scope ) {

            var vm = $scope;
            vm.getUnknownTier = function () {
                var alert = $mdDialog.alert()
                    .title("Contacter nos services")
                    .textContent("Afin d'ajouter un bénéficiaire n'apparaissant pas dans la liste, merci d'approcher nos services à l’adresse repertoire@hatvp.fr.")
                    .ariaLabel("Contacter nos services")
                    .ok("OK");

                $mdDialog.show(alert);
            };

        }]);
