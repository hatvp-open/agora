/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.home')

/**
 * Controlleur de la page d'accueil
 */
    .controller('ErrorCtrl', ['$scope', '$rootScope', 'AppFactory', '$state', 'USER_ROLES', 'EnvConf', 'HTML',
        function ($scope, $rootScope, AppFactory, $state, USER_ROLES, EnvConf, HTML) {
            var vm = $scope;

            // récupération de l'espace corporate actuel
            var currentCorporate = $rootScope.currentCorporate();

            // redirection vers la page contribution si l'espace est actif
            if (AppFactory.isCorporateActive(currentCorporate)
                && $rootScope.userHasAccess())
                $state.go('home');

            // récupération de la dénomination de l'organisation de cet espace
            vm.denomination = currentCorporate.organisationDenomination;

            // affectation des flags d'affichage du message d'erreur
            // par priorité, à commencer par l'espace actif, inscription valide puis inscription non verrouillée
            vm.espaceInactif = !currentCorporate.espaceOrganisationActif;
            vm.inscriptionInvalide = !vm.espaceInactif ? !currentCorporate.actif : false;
            vm.inscriptionVerrouille = !vm.espaceInactif ? currentCorporate.verrouille : false;
            vm.rolesInsuffisants = (!vm.espaceInactif && !vm.inscriptionVerrouille && !vm.inscriptionInvalide) ?
                !currentCorporate.roles.includes(USER_ROLES.contributor) : false;

        }]);