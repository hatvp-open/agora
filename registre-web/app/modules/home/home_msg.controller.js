/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.home')

/**
 * Controlleur de la page Home avec affichage des messages d'erreur
 */
    .controller('HomeMsgCtrl', ['$scope', '$rootScope', '$log', '$state', '$stateParams', '$mdDialog', '$mdToast',
        function ($scope, $rootScope, $log, $state, $stateParams, $mdDialog, $mdToast) {
            $mdDialog.hide();

            switch ($stateParams.entity) {
                case 'authError':
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Votre adresse courriel ou votre mot de passe est incorrect. Veuillez réessayer.")
                            .action('Fermer')
                            .position('top right')
                            .hideDelay(8000)
                    );
                    $state.go('home');
                    break;
                case 'sessionError':
                    $mdDialog.show(
                        $mdDialog.alert()
                            .theme('info')
                            .title("Session expirée")
                            .textContent("Votre session a expiré. Veuillez vous reconnecter.")
                            .ariaLabel("Session expirée")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    );
                    $rootScope.logout();
                    break;
                case 'technicalError':
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Téléservice indisponible")
                            .textContent("Si cette erreur persiste, merci de contacter les services de la Haute Autorité à l'adresse " + $rootScope.email_contact_repertoire + ".")
                            .ariaLabel("Téléservice indisponible")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    ).then(function () {
                        $state.go('home');
                    });
                    break;
                case 'appError':
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Erreur")
                            .textContent($stateParams.msg)
                            .ariaLabel("Erreur")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    ).then(function () {
                        $state.go('home');
                    });
                    break;
                default:
                    $state.go('home');
                    break;
            }
        }]);