/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function() {
    'use strict';

    angular.module('hatvpRegistre.blockUiConfig', [])

        .config(['blockUIConfig','EnvConf',
            function(blockUIConfig, EnvConf) {
                // Block UI configuration
                blockUIConfig.autoBlock = false;
                blockUIConfig.message = 'Chargement en cours ...';
                blockUIConfig.delay = 0;
                blockUIConfig.templateUrl = EnvConf.views_dir + '/blockui/blockui.tmpl.html';
                blockUIConfig.blockBrowserNavigation  = true;
            }
        ])


})();