/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre')

        .factory('AppFactory', function () {

                // Fonctions métier de l'application, utilisées en transversen sur le projet web
                return {
                    startsWithFilter: function (actual, expected) {
                        var lowerStr = (actual + "").toLowerCase();
                        return lowerStr.indexOf(expected.toLowerCase()) === 0;
                    },

                    isCorporateActive: function (corporate) {
                        return corporate && corporate.actif && !corporate.verrouille && corporate.espaceOrganisationActif;
                    },

                    hasRole: function (corporate, authenticated, role) {
                        if (!authenticated)
                            return false;
                        return corporate && corporate.roles.includes(role);
                    },
                }

            }
        );
})();