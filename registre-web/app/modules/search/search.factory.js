/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.search', [])

        .factory('SearchFactory', ['blockUI', 'TYPE_RECHERCHE', '$rootScope', '$q', '$log', '$state', '$mdDialog', '$injector', 'EnvConf', 'HTML', 'SearchService',
            function (blockUI, TYPE_RECHERCHE, $rootScope, $q, $log, $state, $mdDialog, $injector, EnvConf, HTML, SearchService) {

                // Controller's scope
                var vmCorporate;

                return {
                    /**
                     * Intégre les méthodes du scope
                     * @param scope du controlleur qui veut faire uen recherche.
                     * @param type type de la recherche,
                     *          1: Ajouter/Rejoindre EC,
                     *          2: Ajouter client,
                     *          3: Ajouter Organisation pr.
                     */
                    set: function (scope, type) {
                        vmCorporate = scope;

                        /***********************************************************
                         * Recherche d'organisation par ID.
                         ***********************************************************/

                        vmCorporate.querySearch = function (query) {
                            query = query.replace(/ /g, '');

                            var results = [], deferred = $q.defer();
                            vmCorporate.va1lidOrganization = false;
                            var queryNumber = parseInt(query, 10);

                            if (!isNaN(queryNumber) && query.length === 9 || query.length === 14 ||
                                ((query.startsWith("w") || query.startsWith("W")) && query.length === 10) ||
                                ((query.startsWith("h") || query.startsWith("H")) && query.length === 10)) {

                                if (query.length === 14) {
                                    query = query.slice(0, 9);
                                }

                                // blockUI.start();
                                SearchService.searchOrganisationByNationalId(query).then(function (res) {
                                    vmCorporate.listOrganisations = res.data.map(function (o) {
                                        return {
                                            value: query,
                                            display: o.denomination,
                                            organizationSpaceExist: o.organizationSpaceExist,
                                            code: o.nationalId,
                                            type: o.originNationalId,
                                            id: o.id,
                                            data: o
                                        };
                                    });

                                    // résultat filtré limité à 5
                                    // blockUI.stop();
                                    results = vmCorporate.listOrganisations.slice(0, 5);
                                    deferred.resolve(results);
                                }, function (err) {
                                    // blockUI.stop();
                                    deferred.reject(err);
                                })

                            }
                            return deferred.promise;
                        };

                        /********************************************************************************
                         * Si le text de recherche change avant de sélectionner un élément ds la liste.
                         *******************************************************************************/
                        vmCorporate.searchTextChange = function (searchText) {
                            vmCorporate.validOrganization = false;
                            vmCorporate.selectedOrganization = null;
                        };

                        /**************************************************
                         * Si l'élément sélectionner ds la liste change.
                         **************************************************/
                        vmCorporate.selectedItemChange = function (selectedOrganization) {
                            if (selectedOrganization !== undefined) {
                                vmCorporate.selectedOrganization = selectedOrganization;
                                vmCorporate.validOrganization = true;
                                if (angular.equals(type, TYPE_RECHERCHE.AJOUTER_OU_REJOINDRE_ESPACE)) {
                                    vmCorporate.validateStep1();
                                } else if (angular.equals(type, TYPE_RECHERCHE.AJOUTER_CLIENT)) {
                                    //TODO: Ajouetr un client
                                } else if (angular.equals(type, TYPE_RECHERCHE.AJOUTER_ORGANISATION_PRO)) {
                                    //TODO: Ajouetr une organisation pro
                                } else if (angular.equals(type, TYPE_RECHERCHE.AJOUTER_UN_TIER)) {
                                    //TODO: Ajouter un tier
                                }
                            } else {
                                vmCorporate.showDetails = false;
                            }

                            vmCorporate.organizationData = {};
                        };
                    }
                }

            }]);
})();