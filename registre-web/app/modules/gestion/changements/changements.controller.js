/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.gestion')

/**
 * Controlleur pour écran gestion
 */

    .controller('SaisieDemandeCtrl', ['$scope', '$state', '$rootScope', 'blockUI', '$q', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'InscriptionEspaceService', 'STATUS_DEMANDE_TEXT',

        function ($scope, $state, $rootScope, blockUI, $q, $log, $mdDialog, $mdToast, EnvConf, HTML, InscriptionEspaceService, STATUS_DEMANDE_TEXT) {

            var vm = $scope;
            vm.requests = [];
            vm.data = new Object();
            vm.item = new Object();
            vm.item.typeDemande = "AJOUT_OPERATIONNEL";
            vm.data.nonAdminContacts = new Object();
            vm.data.adminContacts = new Object();
            vm.maxFileSizeInMB = $rootScope.getMaxUploadSizeInMB();
            vm.files01 = [];
            vm.files02 = [];
            vm.demandesStatus = angular.copy(STATUS_DEMANDE_TEXT);

            // Modale pour ajouter une nouvelle demande
            vm.newDemande = function (ev) {
                $mdDialog.show({
                    controller: 'SaisieDemandeCtrl',
                    locals: {
                        item: {},
                        edit: false
                    },
                    templateUrl: EnvConf.views_dir + HTML.GESTION.CHANGEMENT_EDIT,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: false
                }).then(function () {
                    refresh();
                });
            };

            vm.complement = function (ev,id) {
                $mdDialog.show({
                    controller: 'MandatesComplementCtrl',
                    templateUrl: EnvConf.views_dir + HTML.GESTION.CHANGEMENT_COMPLEMENT,
                    locals: {
                        id: id
                    },
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: false
                });
            };

            /**
             * Raffraichi les données de la page
             * @returns promise
             */
            vm.refresh = function () {
                return InscriptionEspaceService.getAllDemandesChangement().then(function (res) {
                    vm.requests = res.data;
                });
            };

            vm.refresh();

            vm.loadNonAdminContacts = function () {
                blockUI.start();
                InscriptionEspaceService.getNonAdminActiveRegistrations().then(function (res) {
                    vm.data.nonAdminContacts = res.data;
                }).finally(function () {
                    blockUI.stop();
                });
            };

            vm.loadAdminContacts = function () {
                blockUI.start();
                InscriptionEspaceService.getAdminActiveRegistrations().then(function (res) {
                    vm.data.adminContacts = res.data;
                }).finally(function () {
                    blockUI.stop();
                });

            };

            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            /**
             * Méthode pour la soumission du formulaire d'opérations de changement de contact op de rep legal.
             * @param uploadForm
             * @param representantLegal
             * @returns {boolean}
             */
            vm.submit = function (valid) {
                if (valid) {
                    blockUI.start();

                    // Récupération dans un object des fichiers chargés.
                    // On récupère le mandat uniquement si la case représentant légal est décochée.
                    var formData = new FormData();
                    vm.files01.length > 0 && vm.item.typeDemande == 'CHANGEMENT_REPRESENTANT' && formData.append('identite', vm.files01[0].lfFile);
                    vm.files02.length > 0 && formData.append('mandat', vm.files02[0].lfFile);

                    formData.append("demandeChangementMandant", JSON.stringify(vm.item));

                    InscriptionEspaceService.addNewDemandeChangement(formData).then(function (res) {
                        //upload success
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Votre demande de changement de contact opérationnel ou représentant légal a bien été prise en compte')
                                .textContent("Cette demande ne sera effective qu'après validation de la Haute Autorité. En attendant cette validation, vous pouvez en suivre l'évolution dans votre espace de gestion.")
                                .ok('OK')
                        );
                    }).finally(function () {
                        blockUI.stop();
                        $state.reload();
                    })
                }
            };

            vm.loadNonAdminContacts();
            vm.loadAdminContacts();

        }]).controller('MandatesComplementCtrl', ['$scope', '$state', '$rootScope', '$mdDialog', '$mdToast', 'blockUI', 'InscriptionEspaceService', 'id',
    function ($scope, $state, $rootScope, $mdDialog, $mdToast, blockUI, InscriptionEspaceService, id) {
        var vmComplement = $scope;
        var root = $rootScope;

        vmComplement.id =id;

        vmComplement.maxFileSizeInMB = root.getMaxUploadSizeInMB();
        /**
         * Fermer modale.
         */
        vmComplement.cancel = function () {
            $mdDialog.cancel();
        };

        /**
         * Soumission du formulaire
         */
        vmComplement.submit = function (valid) {
            if (valid) {
                blockUI.start();

                // Récupération dans un object des fichiers chargés
                var formData = new FormData();
                vmComplement.files01.length > 0 && formData.append('file1', vmComplement.files01[0].lfFile);
                vmComplement.files02.length > 0 && formData.append('file2', vmComplement.files02[0].lfFile);
                vmComplement.files03.length > 0 && formData.append('file3', vmComplement.files03[0].lfFile);
                vmComplement.files04.length > 0 && formData.append('file4', vmComplement.files04[0].lfFile);
                formData.append('demandeId', vmComplement.id);

                InscriptionEspaceService.uploadPieces(formData).then(function (res) {
                    //upload success
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Pièces transmises")
                            .textContent("Vos pièces ont bien été transmises aux services de la Haute Autorité.")
                            .ariaLabel("Pièces transmises")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    );
                    $state.reload();
                }).finally(function () {
                    blockUI.stop();
                });
            }
        };
    }
]);
