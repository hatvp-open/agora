/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.gestion')

/**
 * Controlleur pour écran gestion
 */
    .controller('GestionCtrl', ['$scope', '$state', '$rootScope', 'blockUI', '$q', '$log', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'InscriptionEspaceService', 'AuthService', 'TABLE_OPTIONS', 'USER_ROLES', 'DemandesFactory', 'DeclarantsFactory',
        function ($scope, $state, $rootScope, blockUI, $q, $log, $mdDialog, $mdToast, EnvConf, HTML, InscriptionEspaceService, AuthService, TABLE_OPTIONS, USER_ROLES, DemandesFactory, DeclarantsFactory) {
            var vm = $scope;

            /**
             * Inititalisation des onglets de la page
             */
            vm.espaceName = $rootScope.currentCorporate().organisationDenomination;
            vm.tabs = {
                DECLARANTS: EnvConf.views_dir + HTML.GESTION.DECLARANTS,
                DEMANDES: EnvConf.views_dir + HTML.GESTION.DEMANDES,
                CHANGEMENTS: EnvConf.views_dir + HTML.GESTION.CHANGEMENTS
            };

            if (!$rootScope.currentTabG) {
                $rootScope.currentTabG = {
                    index: 0,
                    state: $state.current.name
                };
            }

            /**
             * Initialisation des variables et des fonctions du scope
             * L'initialisation est faite dans des factories (1 par onglet du scope)
             */
            DemandesFactory.set($scope);
            DeclarantsFactory.set($scope);

            /**
             * On initialise ici les données selon l'onglet sélectionné :
             * on évite ainsi de charger tous les onglets au premier chargement de la page.
             */
            vm.selectedIndex = $rootScope.currentTabG ? $rootScope.currentTabG.index : 0;
            $scope.$watch('selectedIndex', function (current, old) {
                $rootScope.currentTabG.index = vm.selectedIndex;

                if ($rootScope.isCorporateActive()) {
                    switch (current + 1) {
                        case 1: // Demandes en attente
                            blockUI.start();
                            DemandesFactory.refresh().finally(function () {
                                blockUI.stop();
                            });
                            break;
                        case 2: // Gestion des accès (déclarants)
                            blockUI.start();
                            DeclarantsFactory.initTable(TABLE_OPTIONS);
                            DeclarantsFactory.refresh().finally(function () {
                                blockUI.stop();
                            });
                            break;
                        case 3: // Gestion des accès (déclarants)

                            break;
                    }
                }
            });

        }]);