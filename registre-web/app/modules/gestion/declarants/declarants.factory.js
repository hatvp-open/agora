/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.gestion')

        .factory('DeclarantsFactory', ['$rootScope', '$log', '$state', '$q', '$mdDialog', '$mdToast',
            'EnvConf', 'HTML', 'InscriptionEspaceService', 'AuthService', 'USER_ROLES', 'blockUI',
            function ($rootScope, $log, $state, $q, $mdDialog, $mdToast,
                      EnvConf, HTML, InscriptionEspaceService, AuthService, USER_ROLES, blockUI) {

                // Controller's scope
                var vm;

                /**
                 * Raffraichi les données de la page
                 * @returns promise
                 */
                function refresh() {
                    vm.declarantsTableOptions.promise = InscriptionEspaceService.getActiveRegistrations();
                    return vm.declarantsTableOptions.promise.then(function (res) {
                        vm.declarants = res.data.map(function (e) {
                            return {
                                id: e.id,
                                name: e.declarantNomComplet,
                                email: e.declarantEmail,
                                administrator: e.roles.includes(USER_ROLES.administrator),
                                publisher: e.roles.includes(USER_ROLES.publisher),
                                contributor: e.roles.includes(USER_ROLES.contributor),
                                verrouille: e.verrouille
                            };
                        });
                    });
                }

                return {
                    /**
                     * Intégre les méthodes du scope
                     * @param scope
                     */
                    set: function (scope) {
                        vm = scope;

                        vm.declarants = [];

                        // Ouvrir fenêtre modale pour modification des rôles du déclarant
                        vm.openDeclarant = function (declarant, ev) {
                            $mdDialog.show({
                                controller: 'DeclarantEditCtrl',
                                locals: {
                                    item: declarant
                                },
                                templateUrl: EnvConf.views_dir + HTML.GESTION.DECLARANT_EDIT,
                                parent: angular.element(document.body),
                                targetEvent: ev,
                                clickOutsideToClose: false,
                                fullscreen: true
                            }).finally(function () {
                                refresh();
                            });
                        };
                    },

                    /**
                     * Paramétrage de la table
                     * @param TABLE_OPTIONS
                     */
                    initTable: function (TABLE_OPTIONS) {
                        vm.declarantsTableOptions = angular.copy(TABLE_OPTIONS);
                    },

                    /**
                     * Raffraichi les données de la page
                     * @returns promise
                     */
                    refresh: refresh

                };
            }]);
})();