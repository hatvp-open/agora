/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.gestion')

/**
 * Controlleur pour modale de modification d'un Déclarant
 */
    .controller('DeclarantEditCtrl', ['$scope', '$state', '$rootScope', 'item', '$mdDialog', '$mdToast', 'InscriptionEspaceService', 'AuthService', 'blockUI', 'DeclarantsFactory',
        function ($scope, $state, $rootScope, item, $mdDialog, $mdToast, InscriptionEspaceService, AuthService, blockUI, DeclarantsFactory) {
            var vm = $scope;

            /***********************************************************
             * Initialisation du contrôleur.
             ***********************************************************/

            vm.isAuthUser = false;
            if (item.email === AuthService.getUser().email) {
                $mdToast.show(
                    $mdToast.simple()
                        .theme('info')
                        .textContent("Vous êtes en train de modifier vos propres droits !")
                        .action('Fermer')
                        .position('top right')
                        .hideDelay(8000)
                );
                vm.isAuthUser = true;
            }

            /***********************************************************
             * Initialisation du scope.
             ***********************************************************/
            // déclarant info
            vm.item = angular.copy(item);
            // images path
            vm.images_dir = $rootScope.images_dir;
            // style des boutons à confirmer
            vm.style = {
                color: vm.isAuthUser ? "red" : "inherit"
            };

            /***********************************************************
             * Déclaration des fonctions dans le scope.
             ***********************************************************/
            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            /**
             * Appel service validation des données de modification
             */
            vm.submit = function (declarant, dirtyForm, ev) {
                if (dirtyForm) {
                    // fonction de mise à jour des droits
                    var update = function () {
                        blockUI.start();
                        InscriptionEspaceService.setAccessRoles(declarant.id,
                            declarant.administrator,
                            declarant.publisher,
                            declarant.contributor,
                            declarant.verrouille)
                            .then(function (res) {
                                $mdDialog.hide();
                                $mdToast.show(
                                    $mdToast.simple()
                                        .theme('info')
                                        .textContent("Les droits ont été mis à jour pour " + declarant.name + ".")
                                        .action('Fermer')
                                        .position('top right')
                                        .hideDelay(8000)
                                );
                            })
                            .finally(function () {
                                blockUI.stop();
                                //DeclarantsFactory.refresh();
                                $state.reload();
                            });
                    };

                    // Si modification des rôles contact opérationnel de l'utilisateur connecté
//                    if (vm.isAuthUser && (!declarant.administrator || declarant.verrouille)) {
//                        // afficher un avertissement
//                        var confirm = $mdDialog.confirm()
//                            .title("Avertissement")
//                            .textContent("Vous êtes sur le point de désactiver votre propre accès contact opérationnel. \n" +
//                                "En cas de confirmation vous n'aurez plus accès à cet écran et ne pourrez plus administrer votre espace collaboratif.")
//                            .ariaLabel("Avertissement")
//                            .targetEvent(ev)
//                            .ok('Confirmer')
//                            .cancel('Annuler');
//
//                        // mettre à jour les droits si confirmé
//                        $mdDialog.show(confirm).then(function () {
//                            update();
//                        });
//                    }
//                    else {
                        update();
  //                  }
                }
                else
                    $mdDialog.hide();
            };

        }]);