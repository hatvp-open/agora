/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.gestion')

        .factory('DemandesFactory', ['$rootScope', '$log', '$state', '$q', '$mdDialog', '$mdToast', 'EnvConf', 'HTML', 'InscriptionEspaceService', 'blockUI',
            function ($rootScope, $log, $state, $q, $mdDialog, $mdToast, EnvConf, HTML, InscriptionEspaceService, blockUI) {

                // Controller's scope
                var vm;

                return {
                    /**
                     * Intégre les méthodes du scope
                     * @param scope
                     */
                    set: function (scope) {
                        vm = scope;

                        vm.requests = [];

                        // Valider demande en attente
                        vm.validerDemande = function (request, i, ev) {
                            var confirm = $mdDialog.confirm()
                                .title("Valider la demande")
                                .textContent("Confirmez-vous la validation de la demande d’inscription de " + request.declarantNomComplet + " ? Si vous confirmez cette action, " + request.declarantNomComplet + " rejoindra votre espace collaboratif avec un accès à l'espace \"contribution\"." +
                                    " Vous pourrez ensuite modifier ses droits dans l'onglet de gestion des accès.")
                                .ariaLabel("Valider la demande")
                                .targetEvent(ev)
                                .ok('Confirmer')
                                .cancel('Annuler');

                            $mdDialog.show(confirm).then(function () {
                                blockUI.start();
                                InscriptionEspaceService.setValidRequest(request.id).then(function (res) {
                                    vm.requests.splice(i, 1);
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .theme('valid')
                                            .textContent("La demande d’inscription de " + request.declarantNomComplet + " a été acceptée. Ce dernier a été notifié par courriel de votre décision.")
                                            .action('Fermer')
                                            .position('top right')
                                            .hideDelay(8000)
                                    );
                                }).finally(function () {
                                    blockUI.stop();
                                });
                            });
                        };

                        // Rejeter demande en attente
                        vm.rejeterDemande = function (request, i, ev) {
                            var confirm = $mdDialog.confirm()
                                .title("Rejeter la demande")
                                .textContent("Êtes-vous sûr de vouloir rejeter la demande de " + request.declarantNomComplet + " ?")
                                .ariaLabel("Rejeter la demande")
                                .targetEvent(ev)
                                .ok('Confirmer')
                                .cancel('Annuler');

                            $mdDialog.show(confirm).then(function () {
                                blockUI.start();
                                InscriptionEspaceService.setRejectedRequest(request.id).then(function (res) {
                                    vm.requests.splice(i, 1);
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .textContent("La demande d’inscription de " + request.declarantNomComplet + " a été rejetée. Ce dernier a été notifié par courriel de votre décision.")
                                            .action('Fermer')
                                            .position('top right')
                                            .hideDelay(8000)
                                    );
                                }).finally(function () {
                                    blockUI.stop();
                                });
                            });
                        };
                    },

                    /**
                     * Raffraichi les données de la page
                     * @returns promise
                     */
                    refresh: function () {
                        return InscriptionEspaceService.getPendingRequests().then(function (res) {
                            vm.requests = res.data;
                        });
                    }

                };
            }]);
})();