/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.contact', [])

/**
 * Controlleur pour modale de contact HATVP
 */
    .controller('ContactCtrl', ['$scope', '$state', '$rootScope', 'context', '$mdDialog', '$mdToast', 'blockUI', 'ContactService', 'OrganisationService',
        function ($scope, $state, $rootScope, context, $mdDialog, $mdToast, blockUI, ContactService, OrganisationService) {
            var vm = $scope;

            /**
             * Types organisation
             * @type {*}
             */
            blockUI.start();
            OrganisationService.getAllTypeOrganisation().then(function (res) {
                vm.types = res.data;
            }).finally(function () {
                blockUI.stop();
            });

            /**
             * Binding Object
             * @type {{context: (*), declarantId: *, denomination: string, type: string,
             * identifiant: string, dirigeant: string, adresse: string, codePostal: string, ville: string,
             * pays: string, commentaire: string}}
             */
            vm.to = {
                context: context,
                denomination: "",
                type: "",
                identifiant: "",
                dirigeant: "",
                adresse: "",
                codePostal: "",
                ville: "",
                pays: "",

                commentaire: ""
            };

            /**
             * Fermer modale.
             */
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            /**
             * Envoyer la demande.
             * @param valid si formulaire valide
             * @param ev $event
             */
            vm.submit = function (valid, ev) {
                if (valid) {

                    var confirm = $mdDialog.confirm()
                        .title("Confirmation de création")
                        .textContent("Vous vous apprêtez à faire une demande de création d'un numéro " +
                            "d'identification pour une organisation. Merci de bien vérifier que cette organisation " +
                            "ne dispose ni d'un SIRET/SIREN, ni d'un numéro RNA. Pour cela, tapez le nom de cette organisation " +
                            "accompagné des mots clefs \"SIREN\" ou \"RNA\" dans votre moteur de recherche.")
                        .ariaLabel("Confirmation de création")
                        .ok("J'ai vérifié")
                        .cancel("Je n'ai pas encore vérifié");

                    $mdDialog.show(confirm).then(function () {
                            blockUI.start();
                            ContactService.send(vm.to).then(function (res) {
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .title("Demande transmise")
                                        .textContent("Votre demande a bien été prise en compte.")
                                        .ariaLabel("Demande transmise")
                                        .ok('OK')
                                        .clickOutsideToClose(true)
                                );
                            }).finally(function () {
                                blockUI.stop();
                            });
                    });
                }
            };

        }]);