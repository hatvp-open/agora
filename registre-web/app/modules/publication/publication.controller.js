/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.publication')

/**
 * Controlleur pour écran Publication
 */
    .controller('PublicationCtrl', ['HTML', '$state', '$mdDialog', 'PublicationService', '$rootScope', '$scope', 'EnvConf', '$timeout', 'blockUI', '$q', 'TABLE_OPTIONS', 'OrganisationService','EspaceOrganisationService',
        function (HTML, $state, $mdDialog, PublicationService, $rootScope, $scope, EnvConf, $timeout, blockUI, $q, TABLE_OPTIONS, OrganisationService, EspaceOrganisationService) {

            /**
             * Déclaration des var.
             */
            var vm = $scope;
            vm.espaceName = $rootScope.currentCorporate().organisationDenomination;

            vm.oldPublication = {};
            vm.newPublication = {};

            vm.newDirigeants = [];
            vm.newCollaborateurs = [];
            vm.newClients = [];
            vm.newAssociations = [];
            vm.newSecteurActivite = [];
            vm.newNiveauIntervention = [];

            vm.oldDirigeants = [];
            vm.oldCollaborateurs = [];
            vm.oldClients = [];
            vm.oldAssociations = [];
            vm.oldSecteurActivite = [];
            vm.oldNiveauIntervention = [];

            /**
             * Les block de la publication.
             */
            vm.block = {
                dirigeant: {
                    nouveau: EnvConf.views_dir + "/publication/dirigeant/nouveau.html",
                    ancien: EnvConf.views_dir + "/publication/dirigeant/ancien.html"
                },
                collaborateur: {
                    nouveau: EnvConf.views_dir + "/publication/collaborateur/nouveau.html",
                    ancien: EnvConf.views_dir + "/publication/collaborateur/ancien.html"
                },
                client: {
                    nouveau: EnvConf.views_dir + "/publication/client/nouveau.html",
                    ancien: EnvConf.views_dir + "/publication/client/ancien.html"
                },
                association: {
                    nouveau: EnvConf.views_dir + "/publication/association/nouveau.html",
                    ancien: EnvConf.views_dir + "/publication/association/ancien.html"
                },
                activite: {
                    nouveau: EnvConf.views_dir + "/publication/activite_interet/nouveau.html",
                    ancien: EnvConf.views_dir + "/publication/activite_interet/ancien.html"
                },
                internet: {
                    nouveau: EnvConf.views_dir + "/publication/internet/nouveau.html",
                    ancien: EnvConf.views_dir + "/publication/internet/ancien.html"
                },
                contact: {
                    nouveau: EnvConf.views_dir + "/publication/contact/nouveau.html",
                    ancien: EnvConf.views_dir + "/publication/contact/ancien.html"
                },
                localisation: {
                    nouveau: EnvConf.views_dir + "/publication/localisation/nouveau.html",
                    ancien: EnvConf.views_dir + "/publication/localisation/ancien.html"
                },
                profil: {
                    nouveau: EnvConf.views_dir + "/publication/profil/nouveau.html",
                    ancien: EnvConf.views_dir + "/publication/profil/ancien.html"
                },
                tab: {
                    identite: EnvConf.views_dir + HTML.PUBLICATION.TAB_PUB
                }
            };

            /**
             * Initialiser les données de la publication.
             */
            function initPublication() {

                // on verifie d'abord que l'espace a bien une date de cloture comptable renseignée
                // on vérifie les droits de publication
                EspaceOrganisationService
                .getCurrentEspaceOrganisation()
                .then(function (res) {
                    vm.corporate = res.data;
                    var confirm = $mdDialog.alert()
                    if (vm.corporate.finExerciceFiscal == null) {
                        confirm.title("Erreur de données")
                            .textContent("Vous devez renseigner la date de cloture d'exercice comptable de votre organisation, où le cas échéant une date de référence, afin d'accéder à cette page.")
                            .ariaLabel("Confirmation")
                            .ok("ok");

                        $mdDialog.show(confirm).then(function () {
                            blockUI.start();
                            $state.go('identite', {}, {location: 'replace'});
                            blockUI.stop();
                        });
                    }
                    else if(!$rootScope.isPublisher() && !$rootScope.isAdministrator()){
                        confirm.title("Attention")
                            .textContent("Vous ne disposez pas du rôle \"publicateur\". En conséquence, vous ne pouvez pas communiquer ces informations à la Haute Autorité."+
                            "<br/> Merci de vous adresser à votre contact opérationnel, seul habilité à vous attribuer ce rôle.")
                            .ariaLabel("Confirmation")
                            .ok("ok");

                            $mdDialog.show(confirm).then(function () {
                                blockUI.start();
                                $state.go('identite', {}, {location: 'replace'});
                                blockUI.stop();
                            });

                    }else if(!$rootScope.isPublisher() && $rootScope.isAdministrator()){
                        confirm.title("Attention")
                        .textContent("Vous ne disposez par du rôle de publicateur. Assurez-vous que votre espace ait été validé par les services de la Haute Autorité."+
                        " Le cas échéant, la gestion des rôles est attribuée au contact opérationnel depuis l'onglet \"gestion\" de l\'espace collaboratif.")
                        .ariaLabel("Confirmation")
                        .ok("ok");

                        $mdDialog.show(confirm).then(function () {
                            blockUI.start();
                            $state.go('identite', {}, {location: 'replace'});
                            blockUI.stop();
                        });
                    }
                    else{
                        blockUI.start();              
                
			                $q.all([
			                    PublicationService.getNewPublication().then(function (res) {
			                        vm.newPublication = res.data;
			                        vm.newDirigeants = vm.newPublication.dirigeants;
			                        vm.newCollaborateurs = vm.newPublication.collaborateurs;
			                        vm.newClients = vm.newPublication.clients;
			                        vm.newAssociations = vm.newPublication.affiliations;

			                        if (vm.newPublication.activites !== null) {
			                            vm.newSecteurActivite = vm.newPublication.activites.listSecteursActivites;
			                            vm.newNiveauIntervention = vm.newPublication.activites.listNiveauIntervention;
			                        }
			                    }),
			                    vm.progress = PublicationService.getLastPublication().then(function (res) {
			                        vm.oldPublication = res.data;
			                        vm.oldDirigeants = vm.oldPublication.dirigeants;
			                        vm.oldCollaborateurs = vm.oldPublication.collaborateurs;
			                        vm.oldClients = vm.oldPublication.clients;
			                        vm.oldAssociations = vm.oldPublication.affiliations;

			                        if (vm.oldPublication.activites !== null) {
			                            vm.oldSecteurActivite = vm.oldPublication.activites.listSecteursActivites;
			                            vm.oldNiveauIntervention = vm.oldPublication.activites.listNiveauIntervention;
			                        }
			                    })
			                ]).then(function () {
			                    formatDate(vm.oldPublication.dateCreation);
			                    traitementGrappePourCalculDeDifferences(vm.oldPublication);
			                    traitementGrappePourCalculDeDifferences(vm.newPublication);
			                    vm.newPublication.listOrganisation = vm.listOrganisation;
                                vm.oldPublication.listOrganisation = vm.listOrganisation;
                                vm.checkBasPage = false;
			                }).finally(function () {
			                    blockUI.stop();
			                });
                    }
                });
            }

            /**
             * Configuration du calcul des différences.
             */
            vm.options = {
                editCost: 4,
                interLineDiff: true,
                ignoreTrailingNewLines: true,
                attrs: {
                    insert: {
                        'data-attr': 'insert',
                        'class': 'insertion'
                    },
                    delete: {
                        'data-attr': 'delete'
                    },
                    equal: {
                        'data-attr': 'equal'
                    }
                }
            };
            /**
             * on vérifie si l'utilisateur a bien fait défilr la synthèse pour afficher le bouton de publication
             */
            vm.BasPage = false;
            vm.checkBasPage = function($event){
                return this.BasPage;
            }



            /**
             * Teste si les données à publier sont correctes.
             */
            var pubErrors = {};

            function isDataReadyToBePublished() {
                if (!vm.newPublication.categorieOrganisation || !vm.newPublication.categorieOrganisation.code ||
                    vm.newDirigeants.length <= 0 || vm.newCollaborateurs.length <= 0 ||
                    (vm.newPublication.declarationTiers === true && vm.newClients.length === 0) ||
                    ((vm.newPublication.declarationOrgaAppartenance === true && vm.newAssociations.length === 0)) ||
                    (vm.newPublication.activites.listNiveauIntervention.length <= 0 && vm.newPublication.activites.listSecteursActivites.length <= 0)
                ) {
                	// catégorie organisation
                	if(!vm.newPublication.categorieOrganisation || !vm.newPublication.categorieOrganisation.code)
                		pubErrors.categOrga = true;
                    // dirigeants
                    if (vm.newDirigeants.length === 0)
                        pubErrors.dirigeants = true;
                    // collaborateurs
                    if (vm.newCollaborateurs.length === 0)
                        pubErrors.collaborateurs = true;
                    // clients
                    if (vm.newPublication.declarationTiers === true && vm.newClients.length === 0)
                        pubErrors.clients = true;
                    // orga pro
                    if (vm.newPublication.declarationOrgaAppartenance === true && vm.newAssociations.length === 0)
                        pubErrors.associations = true;
                    //activites
                    if (vm.newPublication.activites.listNiveauIntervention.length <= 0 && vm.newPublication.activites.listSecteursActivites.length <= 0)
                        pubErrors.activites = true;
                    return false;
                }

                pubErrors.categOrga = false;
                pubErrors.dirigeants = false;
                pubErrors.collaborateurs = false;
                pubErrors.clients = false;
                pubErrors.associations = false;
                pubErrors.activites = false;

                return true;
            }

            /**
             * Créer un tableu comparatif contennt des objets de type {old: '', new:''}
             * Les tableaux comparés, contient des objets de type {code: '', label:''}
             * @param arrNew nv tableau
             * @param arrOld ancien tableau
             */
            function getComparativeActivityArray(arrOld, arrNew) {
                var size = arrNew.length;
                if (arrOld.length > arrNew.length) {
                    size = arrOld.length;
                }

                var compareTab = [];

                for (var i = 0; i < size; i++) {
                    if (angular.isDefined(arrOld[i]) && angular.isDefined(arrNew[i])) {
                        compareTab.push({old: arrOld[i].label, new: arrNew[0].label})
                    } else if (angular.isDefined(arrOld[i])) {
                        compareTab.push({old: arrOld[i].label, new: ""})
                    } else if (angular.isDefined(arrNew[i])) {
                        compareTab.push({old: "", new: arrNew[i].label})
                    }
                }

                return compareTab;
            }

            /**
             * Fonction de publication.
             */
            vm.publier = function (ev) {

                //si aucunes erreures -> OK
                if (isDataReadyToBePublished()) {
                    var title = "Attention",
                        message = "Vous vous apprêtez à communiquer à la Haute Autorité les éléments relatifs à votre organisation qui vous ont été présentés."
                                + " Sauf mention contraire, ces éléments seront rendus publics sur le site internet de la Haute Autorité. <br/><br/>Êtes&#8209;vous sûr de vouloir continuer ?";

                    var confirm = $mdDialog.confirm()
                        .title(title)
                        .htmlContent(message)
                        .ariaLabel(title)
                        .targetEvent(ev)
                        .ok('Confirmer')
                        .cancel('Annuler');

                    $mdDialog.show(confirm).then(function () {
                        blockUI.start();
                        PublicationService.publierLesInformations().then(function (res) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .targetEvent(ev)
                                    .title("Succès")
                                    .textContent("Votre demande a bien été enregistrée. La publication de ces éléments sur le site de la Haute Autorité sera effective dans les 60 minutes.")
                                    .ariaLabel("Succès")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            ).finally(function() {
                                $state.go('identite', {}, {reload: true});
                            });
                        }).finally(function () {
                            blockUI.stop();
                        });
                    });
                }
                // sinon -> KO
                else {
                    $mdDialog.show({
                        controller: 'ErrorDialogController',
                        locals: {
                            errors: pubErrors
                        },
                        templateUrl: EnvConf.views_dir + HTML.PUBLICATION.ERROR,
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: false,
                        fullscreen: true
                    }).then(function () {
                        //$state.reload();
                    });
                }
            };

            vm.compareActivityArrays = function () {
                vm.compareArrays(vm.oldSecteurActivite, vm.newSecteurActivite, 5);
                vm.compareArrays(vm.oldNiveauIntervention, vm.newNiveauIntervention, 6);
            };

            /**
             * Comparaison de deux tableaux
             * type 1: dirigeants
             * type 2: collaborateurs
             * type 3: client
             * type 4: association
             */
            vm.compareArrays = function (array1, array2, type) {
                arrayDiff(array1, array2, type, "green");
                arrayDiff(array2, array1, type, "red");
            };

            function arrayDiff(base_array, diff_array, type, color) {
                if (base_array.length === 0 && diff_array.length === 0) {
                    return false;
                }
                else if (base_array.length > 0 && diff_array.length === 0) {
                } else {
                    var temp = [];

                    var i = 0;
                    angular.forEach(diff_array, function (value1, key1) {

                        angular.forEach(base_array, function (value2, key2) {
                            if (angular.equals(type, 1) || angular.equals(type, 2)) {
                                temp[i] = value1.civilite === value2.civilite && value1.nom === value2.nom && value1.prenom === value2.prenom && value1.fonction === value2.fonction;
                                i++;
                            } else if (angular.equals(type, 3)) {
                                temp[i] = value1.denomination === value2.denomination && value1.identifiantNational === value2.identifiantNational &&  value1.ancienClient === value2.ancienClient;
                                i++;
                            } else if (angular.equals(type, 4)) {
                                temp[i] = value1.denomination === value2.denomination && value1.identifiantNational === value2.identifiantNational;
                                i++;
                            }else if (angular.equals(type, 5) || angular.equals(type, 6)) {
                                temp[i] = value1.code === value2.code && value1.label === value2.label;
                                i++;
                            }
                        })

                    });

                    //vérifier si la ligné a été trouvé
                    var oldTabSize = base_array.length;
                    var newTabSize = diff_array.length;


                    //diviser le tableau temporaires de calcul en +ieurs tableau,
                    // chaque tableau fait référence à une colonne
                    var tab_diff = new Array(newTabSize);
                    for (var j = 0; j < newTabSize; j++) {
                        tab_diff[j] = temp.splice(0, oldTabSize);
                    }

                    //Afficher les différences à l'écran
                    angular.forEach(tab_diff, function (val, key) {

                        if (tab_diff[key].indexOf(true) === -1) {
                            var element;
                            switch (type) {
                                case 1:
                                    if (color === "green") {
                                        element = document.getElementById("dirigeant_" + key);
                                        element.classList.toggle("show_diff_green");
                                    } else {
                                        element = document.getElementById("dirigeant_old_" + key);
                                        element.classList.toggle("show_diff_red");
                                    }
                                    break;
                                case 2:
                                    if (color === "green") {
                                        element = document.getElementById("collab_" + key);
                                        element.classList.toggle("show_diff_green");
                                    } else {
                                        element = document.getElementById("collab_old_" + key);
                                        element.classList.toggle("show_diff_red");
                                    }
                                    break;
                                case 3:
                                    if (color === "green") {
                                        element = document.getElementById("client_" + key);
                                        element.classList.toggle("show_diff_green");
                                    } else {
                                        element = document.getElementById("client_old_" + key);
                                        element.classList.toggle("show_diff_red");
                                    }
                                    break;
                                case 4:
                                    if (color === "green") {
                                        element = document.getElementById("association_" + key);
                                        element.classList.toggle("show_diff_green");
                                    } else {
                                        element = document.getElementById("association_old_" + key);
                                        element.classList.toggle("show_diff_red");
                                    }
                                    break;
                                case 5:
                                    if (color === "green") {
                                        element = document.getElementById("activity_new_sec_" + key);
                                        element.classList.toggle("show_diff_fast_green");
                                    } else {
                                        element = document.getElementById("activity_old_sec_" + key);
                                        element.classList.toggle("show_diff_red");
                                    }
                                    break;
                                case 6:
                                    if (color === "green") {
                                        element = document.getElementById("activity_new_niv_" + key);
                                        element.classList.toggle("show_diff_fast_green");
                                    } else {
                                        element = document.getElementById("activity_old_niv_" + key);
                                        element.classList.toggle("show_diff_red");
                                    }
                                    break;
                            }
                        }
                    });
                }
            }

            /**
             * Formatter la date au format date + à + heure.
             * @param date la date à formatter.
             */
            function formatDate(date) {
                if (angular.isDefined(date) && date !== null) {
                    vm.dateDeCreation = date.split(" ");
                    vm.dateDeCreation = vm.dateDeCreation[0] + " à " + vm.dateDeCreation[1];
                }
            }

            /**
             * Traitement des deux grappes d'objet pour faciliter la comparaison
             */
            function traitementGrappePourCalculDeDifferences(grappe) {

                angular.forEach(grappe, function (val, key) {
                    if (val === null) grappe[key] = "";
                });
            }
            /**
             * Active/désactive bouton de publication 
             * @param {*} ev 
             */
            vm.modificationEtatBoutonPublication = function (ev) {
                if(!vm.checkBasPage){
                    vm.checkBasPage = true;
                }else{
                    vm.checkBasPage = false;
                }
            }


            /**
             * Initialisation
             */
            vm.dirigeantsTableOptions = angular.copy(TABLE_OPTIONS);
            vm.collaborateursTableOptions = angular.copy(TABLE_OPTIONS);
            vm.clientsTableOptions = angular.copy(TABLE_OPTIONS);
            vm.assocTableOptions = angular.copy(TABLE_OPTIONS);
            vm.newDirigeantsTableOptions = angular.copy(TABLE_OPTIONS);
            vm.newCollaborateursTableOptions = angular.copy(TABLE_OPTIONS);
            vm.newClientsTableOptions = angular.copy(TABLE_OPTIONS);
            vm.newAssocTableOptions = angular.copy(TABLE_OPTIONS);
            initPublication();

        }])
    .controller('ErrorDialogController', ['HTML', '$state', '$mdDialog', '$rootScope', '$scope', 'errors',
        function (HTML, $state, $mdDialog, $rootScope, $scope, errors) {
            $scope.images_dir = $rootScope.images_dir;
            $scope.errors = errors;
            $scope.errorsIndex = Object.keys(errors);

            $scope.countErrors = Object.keys(errors).length;

            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };

            $scope.redirectToEdit = function (ev) {
                $scope.cancel();
                $state.go('identite', {}, {reload: true});
            }
        }]);
