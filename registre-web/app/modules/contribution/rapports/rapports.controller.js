/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.contribution')

/**
 * Controlleur parent pour écrans rapports
 */
    .controller('RapportsCtrl', ['$scope', '$state', '$rootScope', 'blockUI', 'NomenclaturesService', 'EspaceOrganisationService',
        function ($scope, $state, $rootScope, blockUI, NomenclaturesService, EspaceOrganisationService) {

            var vm = $scope;
            vm.espaceName = $rootScope.currentCorporate().organisationDenomination;
            vm.satutDesinscription = $rootScope.currentCorporate().satutDesinscription;
            vm.statutMap = new Map();
            vm.statutMap.set("NON_PUBLIEE", "Non publiée");
            vm.statutMap.set("PUBLIEE", "Publiée");
            vm.statutMap.set("DEPUBLIEE", "Dépubliée");
            vm.statutMap.set("REPUBLIEE", "Republiée");
            vm.statutMap.set("MAJ_NON_PUBLIEE", "Mise à jour non publiée");
            vm.statutMap.set("MAJ_PUBLIEE", "Mise à jour publiée");

            $scope.images_dir = $rootScope.images_dir;
            vm.currentNavItem;

            if ($state.current.name == 'rapports.moyens') {
                vm.currentNavItem = 'moyens';                
            } else {
                vm.currentNavItem = 'declaration';
            }


            /**
             * Controle d'existence de la date de cloture d'exercice comptable
             * récupérer informations générales espace organisation
             */
            EspaceOrganisationService
                .getCurrentEspaceOrganisation()
                .then(function (res) {
                    vm.corporate = res.data;

                    NomenclaturesService.getNomenclatures().then(function (res) {
                        vm.nomenclatures = res.data;
                    }).finally(function () {
                        blockUI.stop();
                    });
                });

        }])
    /**
     * Controlleur pour écran des moyens
     */
    .controller('RapportsMoyensCtrl', ['$scope', '$state','$stateParams', '$rootScope', 'blockUI', '$q', '$mdDialog', 'RapportService', 'PublicationService',
        function ($scope, $state, $stateParams, $rootScope, blockUI, $q, $mdDialog, RapportService, PublicationService) {

            var vm = $scope;
            vm.rapport = {};
            vm.exercices = [];
            vm.rapport.exercice = $stateParams.exercice;

            /**
             * initialisation des données
             */
            var initData = function () {
                blockUI.start();            
                $q.all([
                    vm.rapport.promise = RapportService.getExercicesSimple($rootScope.currentCorporate().id),
                    vm.rapport.promise.then(function (res) {

                        vm.exercices = res.data.map(function (e) {
                            // si on arrive sur l'onglet
                            if (vm.rapport.exercice === undefined) {
                                // on selectionne l'exercice courant si l'inscription s'est faite dans l'année
                                // sinon on affiche le dernier exercice cloturé
                                if ((toDate(e.dateDebut) > stringSlashToDate($rootScope.currentCorporate().dateValidation)) && (stringSlashToDate($rootScope.currentCorporate().dateValidation).getFullYear() == (new Date()).getFullYear())) {
                                    vm.rapport.exercice = e;
                                    vm.rapport.exercice.periode = e.dateDebut + " au " + e.dateFin;
                                }
                                if(toDate(e.dateFin).getFullYear() == ((new Date()).getFullYear()-1)){
                                    vm.rapport.exercice = e;
                                    vm.rapport.exercice.periode = e.dateDebut + " au " + e.dateFin;
                                }
                            }

                            return {
                                id: e.id,
                                dateDebut: e.dateDebut,
                                dateFin: e.dateFin,
                                chiffreAffaire: e.chiffreAffaire,
                                hasNotChiffreAffaire: e.hasNotChiffreAffaire,
                                montantDepense: e.montantDepense,
                                nombreSalaries: e.nombreSalaries,
                                periode: e.dateDebut + " au " + e.dateFin,
                                tempToSort: toDate(e.dateDebut),
                                isPublished: e.isPublished,
                                dataToPublish: e.dataToPublish,
                                commentaire: e.commentaire
                            };
                        });
                    })
                ]).catch(function (err) {
                    $state.go('identite', {}, {location: 'replace'});
                }).finally(function () {
                    blockUI.stop();
                });
            };

            /**
             * Sauvegarde des moyens de l'exercice
             * @param valid si formulaire valide
             */
            vm.submitMoyens = function () {

                if (vm.moyensForm.$valid) {

                    blockUI.start();

                    var doneSave = $mdDialog.alert()
                        .title("Données sauvegardées")
                        .textContent("Vos informations ont bien été sauvegardées.")
                        .ariaLabel("Données sauvegardées")
                        .ok('OK')
                        .clickOutsideToClose(true);

                    RapportService.saveMoyens(vm.rapport.exercice).then(function () {
                        $mdDialog.show(doneSave);
                        initData();
                    }).finally(function () {
                        blockUI.stop();
                    });
                }
            };

            vm.publishMoyens = function () {

                if (vm.moyensForm.$valid) {

                    var donePublier = $mdDialog.confirm()
                        .title("Attention")
                        .htmlContent("Vous vous apprêtez à communiquer à la Haute Autorité les moyens alloués à la période du " + vm.rapport.exercice.periode
                            + ". Ces éléments seront rendus publics sur le site internet de la Haute Autorité. <br/><br/>Êtes vous sûr de vouloir continuer ?")
                        .ariaLabel("Attention")
                        .ok('Confirmer')
                        .cancel('Annuler');

                    if (vm.moyensForm.$dirty) {
                        blockUI.start();
                        RapportService.saveMoyens(vm.rapport.exercice).then(function () {
                        }).finally(function () {
                            blockUI.stop();
                        });
                    }

                    $mdDialog.show(donePublier).then(function () {
                        blockUI.start();
                        PublicationService.publierMoyens(vm.rapport.exercice).then(function () {
                            initData();
                            blockUI.stop();
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Succès")
                                    .textContent("Votre demande a bien été enregistrée. La publication de ces éléments sur le site de la Haute Autorité sera effective dans les 60 minutes.")
                                    .ariaLabel("Succès")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            ).finally(function () {
                                blockUI.stop();
                                $state.go('contribution', {}, {reload: true});
                            });
                        }).finally(function () {
                            blockUI.stop();
                        });
                    });
                }
            };

            vm.alertChiffreAffaire = function(exercice) {
                if(exercice.dataToPublish === undefined && vm.satutDesinscription != 'DESINSCRIPTION_VALIDEE' && vm.satutDesinscription != 'DESINSCRIT'){
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Attention")
                            .textContent("vous ne devez renseigner que le chiffre d’affaires au sens comptable, si votre organisation n’a pas de chiffre d’affaires, cochez la case « organisation sans chiffre d’affaires »")
                            .ariaLabel("CA")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    );
                }                
            };

            /**
             * parsing d'un string format dd-MM-yyyy vers une date javascript 
             * @param dateStr
             * @returns {Date}
             */
            function toDate(dateStr) {
                var parts = dateStr.split("-");
                return new Date(parts[2], parts[1] - 1, parts[0]);
            }
            /**
             * parsing d'un string dd/MM/yyyy vers une date javascript
             * @param dateStr
             * @returns {Date}
             */
            function stringSlashToDate(dateStr) {
                var parts = dateStr.split("/");
                return new Date(parts[2], parts[1] - 1, parts[0]);
            }

            initData();

        }])
    /**
     * Controlleur pour écran des activités
     */
    .controller('RapportsActivitesCtrl', ['$scope', '$state', '$rootScope', 'blockUI', '$q', '$mdDialog', 'RapportService', 'TABLE_OPTIONS', 'EnvConf', 'HTML',
        function ($scope, $state, $rootScope, blockUI, $q, $mdDialog, RapportService, TABLE_OPTIONS, EnvConf, HTML) {

            var vm = $scope;
            $scope.images_dir = $rootScope.images_dir;
            vm.activitesTableOptions = angular.copy(TABLE_OPTIONS);
            vm.activitesTableOptions.order = 'ordre' ;

            // Paramétres de mapping et binding
            vm.rapport = {};
            vm.exercices = [];
            vm.activites = [];

            vm.declarant = $rootScope.currentUser();



            /**
             * initialisation des données
             */
            var initData = function () {
                blockUI.start();
                $q.all([

                    //vm.guidelines(),
                    vm.rapport.promise = RapportService.getExercices($rootScope.currentCorporate().id),
                    vm.rapport.promise.then(function (res) {

                        vm.exercices = res.data.map(function (e) {

                            if (toDate(e.dateFin) > new Date() && toDate(e.dateDebut) < new Date()) {
                                vm.rapport.exercice = e;
                                vm.rapport.exercice.periode = e.dateDebut + " au " + e.dateFin;
                                vm.activites = vm.rapport.exercice.activites;
                            }

                            return {
                                id: e.id,
                                dateDebut: e.dateDebut,
                                dateFin: e.dateFin,
                                chiffreAffaire: e.chiffreAffaire,
                                hasNotChiffreAffaire: e.hasNotChiffreAffaire,
                                montantDepense: e.montantDepense,
                                nombreSalaries: e.nombreSalaries,
                                tempToSort: toDate(e.dateDebut),
                                periode: e.dateDebut + " au " + e.dateFin,
                                activites: e.activiteSimpleDto.map(function (a) {
                                    return {
                                        id: a.id,
                                        idFiche: a.idFiche,
                                        objet: a.objet,
                                        statut: a.statut.label,
                                        ordre: a.statut.ordre,
                                        domainesIntervention: a.domainesIntervention.join(', '),
                                        nomCreateur: a.nomCreateur
                                    }
                                }),
                                statut: e.statut,
                                isPublication: e.isPublication,
                                noActivite: e.noActivite,
                            };
                        });
                    })
                ]).catch(function (err) {
                    $state.go('identite', {}, {location: 'replace'});
                }).finally(function () {
                    blockUI.stop();
                });
            };
            // Modale afficher les guidelines de la rubrique
            vm.showGuidelines = function (exercice) {
                $mdDialog.show({
                    controller: 'ModaleCtrl',
                    templateUrl: EnvConf.views_dir + HTML.CONTRIBUTION.RAPPORTS_GUIDELINES,
                    locals: {
                    	exercices: vm.exercices,
                        exercice: exercice
                    },
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    openFrom:{top:-50, width:30, height:80},
                    closeTo: {left:1500},
                    fullscreen: false
                }
            )
            };
            /**
             * création d'une nouvelle activité
             */
            vm.newActivite = function (exercice) {
                if(vm.declarant.firstConn){
                    vm.showGuidelines(exercice);
                }
                else {
                    $state.go('rapports.saisie', {
                        exercices: vm.exercices,
                        exercice: exercice
                    });
                }

            };

            /**
             * Réorientation vers la page moyen de la période concernée
             * @param {*} exercice 
             */
            vm.declarerMoyen = function (rapport) {
                $state.go('rapports.moyens', {
                    exercice: rapport.exercice
                }, {reload: true});

            };

            /**
             * ouverture d'une activité existente
             */
            vm.editActivite = function (id) {
                $state.go('rapports.saisie', {
                    id: id,
                    data: id,
                    exercices: vm.exercices
                });
            };

            /**
             * duplication d'une activité existente
             */
            vm.duplicateActivite = function (id) {
                $state.go('rapports.saisie', {
                    data: id,
                    exercices: vm.exercices,
                    duplicate: true
                });
            };

            /**
             * suppresion d'une activité
             */
            vm.deleteActivite = function (id) {

                var confirm = $mdDialog.confirm()
                    .title("Suppression d'une activité de représentation d'intêret")
                    .textContent("Etes-vous sûr(e) de vouloir supprimer cette activité de représentation d'intérêts pour l'exercice du " + vm.rapport.exercice.periode + " ?")
                    .ariaLabel("Confirmation de la suppression")
                    .ok("Supprimer")
                    .cancel("Annuler");

                $mdDialog.show(confirm).then(function () {
                    blockUI.start();
                    RapportService.deleteActivite(id).then(function () {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Activité de représentation d'intêret supprimée")
                                .textContent("L'activité de représentation d'intêret a bien été supprimée.")
                                .ariaLabel("Données supprimées")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        );
                    }).finally(function () {
                        initData();
                        blockUI.stop();
                    });
                });

            };

            vm.downloadRapportPdf = function (ev, id) {
                $mdDialog.show({
                    controller: 'PdfRapportCtrl',
                    templateUrl: EnvConf.views_dir + HTML.CONTRIBUTION.RAPPORTS_PDF,
                    locals: {
                        id: id
                    },
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true
                });
            };

            vm.declareNoActivite = function (evt, exercices) {
                $mdDialog.show({
                    controller: 'NoActiviteRapportCtrl',
                    templateUrl: EnvConf.views_dir + HTML.CONTRIBUTION.RAPPORTS_NO_ACTIVITE,
                    locals: {
                        exercices: exercices
                    },
                    parent: angular.element(document.body),
                    targetEvent: evt,
                    clickOutsideToClose: true,
                    escapeToClose: true
                }).then(function(){
                }).finally(function (){
                });
            };

            /**
             * parsing d'un string vers une date javascript
             * @param dateStr
             * @returns {Date}
             */
            function toDate(dateStr) {
                var parts = dateStr.split("-");
                return new Date(parts[2], parts[1] - 1, parts[0]);
            };

            initData();

        }]).controller('PdfRapportCtrl', ['$scope', '$state', '$rootScope', 'blockUI', '$q', '$mdDialog', 'RapportService', 'id',
    function ($scope, $state, $rootScope, blockUI, $q, $mdDialog, RapportService, id) {

        var vm = $scope;
        vm.statut = {};
        vm.id = id;


        /**
         * initialisation des données
         */
        var initData = function () {
            blockUI.start();
            $q.all([
                vm.statut.promise = RapportService.getStatutEnum(),
                vm.statut.promise.then(function (res) {
                    vm.statut = res.data.statutActiviteEnumList.map(function (s) {
                        return {
                            code: s.code,
                            label: s.label,
                            value: true
                        }
                    });
                    vm.statut.moyen = true;
                })
            ]).catch(function (err) {
            }).finally(function () {
                blockUI.stop();
            });
        };

        vm.downloadRapportPdf = function () {
            function checkTrue(stat) {
                return stat.value === true;
            }

            vm.checked = vm.statut.filter(checkTrue);

            RapportService.downloadRapportPdf(vm.id, vm.statut.moyen, vm.checked);

            $mdDialog.cancel();
        };

        vm.cancel = function () {
            $mdDialog.cancel();
        };

        initData();
    }]).controller('NoActiviteRapportCtrl', ['$scope', '$state', '$rootScope', 'blockUI', '$q', '$mdDialog', 'RapportService', 'exercices',
    function ($scope, $state, $rootScope, blockUI, $q, $mdDialog, RapportService, exercices) {

        var vm = $scope;

        vm.exerciceComptable = {};
        vm.exercices = exercices.filter(function (element) {
            return element.activites.length === 0 && toDate(element.dateFin) < new Date() && element.noActivite === false;
        });

        vm.selectExercice = function (evt) {

            if (vm.exerciceComptable.id === undefined) {
                return null;
            }

            var confirm = $mdDialog.confirm()
                .title("Confirmation")
                .textContent("Merci de confirmer que vous attestez n'avoir conduit aucune activité de représentation d'intérêts pour la période du " + vm.exerciceComptable.periode + ".")
                .ariaLabel("Confirmation de l'attestation")
                .ok("Je confirme mon attestation")
                .cancel("Annuler");

            $mdDialog.show(confirm).then(function () {
                blockUI.start();
                RapportService.noActivityToDeclare(vm.exerciceComptable).then(function () {
                    if (vm.exerciceComptable.statut.code === "NON_PUBLIEE" || vm.exerciceComptable.statut.code === "DEPUBLIEE") {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Déclaration nulle sauvegardée")
                                .textContent("Nous avons bien pris en compte votre déclaration nulle. Un affichage spécifique sera prochainement mis en ligne sur votre page du répertoire numérique des représentants d'intérêts sur www.hatvp.fr. N'oubliez pas de communiquer à la Haute Autorité les moyens alloués à la représentation d'intérêts, qui demeurent exigibles pour la période considérée.")
                                .ariaLabel("Déclaration nulle sauvegardée")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        ).finally(function () {
                            blockUI.stop();
                            $state.go('rapports.declaration', {}, {reload: true});
                        });
                    } else {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Déclaration nulle sauvegardée")
                                .textContent("Nous avons bien pris en compte votre déclaration nulle. Un affichage spécifique sera prochainement mis en ligne sur votre page du répertoire numérique des représentants d'intérêts sur www.hatvp.fr.")
                                .ariaLabel("Déclaration nulle sauvegardée")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        ).finally(function () {
                            blockUI.stop();
                            $state.go('rapports.declaration', {}, {reload: true});
                        });
                    }
                    blockUI.stop();
                }).finally(function () {
                    blockUI.stop();
                });
            });
        };

        vm.closeModal = function (evt) {
            $mdDialog.hide();
        };

        function toDate(dateStr) {
            var parts = dateStr.split("-");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        };

    }]).controller('ModaleCtrl', ['$scope', '$state', '$rootScope', 'blockUI', '$q', '$mdDialog', 'RapportService','exercices', 'exercice',
    function ($scope, $state, $rootScope, blockUI, $q, $mdDialog, RapportService, exercices,exercice) {

        var vm = $scope;
        vm.exercice = exercice;
        vm.exercices = exercices;
        $scope.images_dir = $rootScope.images_dir;
     
        vm.closeGuidelines = function () {
            $mdDialog.hide();
            RapportService.hasReadGuidelines();
            $state.go('rapports.saisie', {
                exercices: vm.exercices,
                exercice: vm.exercice
            });
        };



    }]);