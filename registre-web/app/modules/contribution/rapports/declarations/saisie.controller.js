/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.contribution')

/**
 * Controlleur pour écran rapports
 */
    .controller('RapportsNewDeclarationCtrl', ['$scope', '$rootScope', '$state', 'EnvConf', 'HTML', '$stateParams', 'blockUI', '$q', '$mdDialog', 'RapportService', 'PublicationService',
        function ($scope, $rootScope, $state, EnvConf, HTML, $stateParams, blockUI, $q, $mdDialog, RapportService, PublicationService) {

            var vm = $scope;
            vm.espace = $rootScope.currentCorporate();
            vm.searchTerm = '';



            //function de recherche pour les listes
            vm.clearSearchTerm = function () {
                this.searchTerm = '';
            };

            vm.stopEvent = function (event) {
                event.stopPropagation();
            };

            // Paramétres pour l'ajout, l'édition et la duplication d'une fiche
            vm.id = $stateParams.id;
            vm.data = $stateParams.data;

            if ($stateParams.exercices !== null) {
                vm.exercices = $stateParams.exercices;
            } else {
                $state.go('rapports.declaration');
            }
            if ($stateParams.exercice !== null) {
                vm.exerciceComptable = $stateParams.exercice.id;
            }

            // Paramétres de mapping et de binding
            vm.rapport = {};
            vm.clients = [];
            vm.activite = {};
            vm.espaceSimple = {
                organisationId: vm.espace.organizationId,
                denomination: vm.espace.organisationDenomination,
                nomUsage: vm.espace.nomUsage
            };

            //liste des bénéficiaires : ce sont les clients et l'organisation elle meme si le declarant a choisi "en propre".
            vm.beneficiaires = [];

            /**
             * initialisation des données
             */
            var initData = function () {
                blockUI.start();
                if (vm.data) {
                    $q.all([
                        RapportService.getClients().then(function (res) {
                            vm.clients = res.data;
                            for (var i in vm.clients) {
                                if (vm.clients[i].id === vm.espace.organizationId) {
                                    vm.clients.splice(i, 1);
                                }
                            }
                        }),

                        vm.rapport.promise = RapportService.getActivite(vm.data),
                        vm.rapport.promise.then(function (res) {
                            vm.activite = {
                                id: vm.id,
                                objet: res.data.objet,
                                isPourMoi: vm.checkIsPourMoi(res.data.actionsRepresentationInteret),
                                isPourTiers: vm.checkTiers(res.data.actionsRepresentationInteret),
                                domainesIntervention: res.data.domainesIntervention.map(function (di) {
                                    return {
                                        id: di
                                    }
                                }),
                                actionsRepresentationInteret: res.data.actionsRepresentationInteret.map(function (a) {
                                    return {
                                        id: a.id,
                                        reponsablesPublics: a.reponsablesPublics.map(function (rp) {
                                            return {
                                                id: rp
                                            }
                                        }),
                                        decisionsConcernees: a.decisionsConcernees.map(function (dc) {
                                            return {
                                                id: dc
                                            }
                                        }),
                                        actionsMenees: a.actionsMenees.map(function (am) {
                                            return {
                                                id: am
                                            }
                                        }),
                                        responsablePublicAutre: a.responsablePublicAutre,
                                        reponsablePublicCheck: false,
                                        actionMeneeAutre: a.actionMeneeAutre,
                                        actionMeneeCheck: false,
                                        tiers: a.tiers.map(function (t) {
                                            return {
                                                organisationId: t.organisationId,
                                                denomination: t.denomination,
                                                nomUsage: t.nomUsage
                                            }
                                        }),
                                        observation: a.observation
                                    }
                                }),
                                statut: res.data.statut.code,
                                exerciceComptable: res.data.exerciceComptable,
                                dataToPublish: res.data.dataToPublish
                            };
                        })
                    ]).then(function () {
                    }).finally(function () {
                        if (vm.activite.isPourMoi) {
                            vm.beneficiaires.unshift(vm.espaceSimple);
                        }
                        if (vm.activite.isPourTiers) {
                            vm.beneficiaires = vm.clients.map(function (c) {
                                return {
                                    organisationId: c.id,
                                    denomination: c.denomination
                                }
                            })
                        }
                        //permet de modifier la période d'exercice lors duplication activité
                        if($stateParams.duplicate && $stateParams.duplicate == true){
                            $scope.activite.statut = null;
                        }
                        blockUI.stop();
                        vm.remakeBeneficiaires();

                    });
                } else {
                    $q.all([
                        RapportService.getClients().then(function (res) {
                            vm.clients = res.data;
                            //on supprime l'organisation de ses client si jamais elle y est
                            for (var i in vm.clients) {
                                if (vm.clients[i].id === vm.espace.organizationId) {
                                    vm.clients.splice(i, 1);
                                }
                            }
                        })
                    ]).then(function () {
                    }).finally(function () {
                        blockUI.stop();
                    });
                    vm.activite.actionsRepresentationInteret = [];
                    vm.activite.exerciceComptable = vm.exerciceComptable;
                    var actionsRepresentationInteret = {};
                    vm.activite.actionsRepresentationInteret.push(actionsRepresentationInteret);
                }
            };

            function checkErrors(redirect) {
                var errors = {};
                if (vm.activiteForm.$error.length != 0) {
                    vm.hasErrors = true;
                    //message generique si plusieurs actions dans l'activite
                    if (vm.activite.actionsRepresentationInteret.length > 1) {
                        errors.msgPlusieursActions = true;
                    } else {
                        for (var key in vm.activiteForm.$error) {
                            for (var index = 0; index < vm.activiteForm.$error[key].length; index++) {

                                if (vm.activiteForm.$error[key][index].$name === 'objet') {
                                    errors.objet = true;
                                }
                                if (vm.activiteForm.$error[key][index].$name === 'beneficiaires') {
                                    errors.beneficiaires = true;
                                } else if (vm.activiteForm.$error[key][index].$name === 'domaine') {
                                    errors.domaine = true;
                                } else if (vm.activiteForm.$error[key][index].$name === 'responsables') {
                                    errors.responsables = true;
                                } else if (vm.activiteForm.$error[key][index].$name === 'responsablesAutres') {
                                    errors.responsablesAutres = true;
                                } else if (vm.activiteForm.$error[key][index].$name === 'actions') {
                                    errors.actions = true;
                                } else if (vm.activiteForm.$error[key][index].$name === 'actionsAutres') {
                                    errors.actionsAutres = true;
                                } else if (vm.activiteForm.$error[key][index].$name === 'decisions') {
                                    errors.decisions = true;
                                }
                            }
                        }
                    }
                }

                $mdDialog.show({
                    controller: 'ErrorSaisieDialogController',
                    locals: {
                        errors: errors,
                        redirect: redirect
                    },
                    templateUrl: EnvConf.views_dir + HTML.CONTRIBUTION.RAPPORTS_ERROR,
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    fullscreen: true
                });
            };

            function buildFiche() {
                var activite = {
                    id: vm.id,
                    objet: vm.activite.objet,
                    domainesIntervention: vm.activite.domainesIntervention.map(function (di) {
                        return di.id;
                    }),
                    actionsRepresentationInteret: vm.activite.actionsRepresentationInteret.map(function (a) {
                        return {
                            id: a.id,
                            reponsablesPublics: a.reponsablesPublics.map(function (rp) {
                                return rp.id;
                            }),
                            decisionsConcernees: a.decisionsConcernees.map(function (dc) {
                                return dc.id;
                            }),
                            actionsMenees: a.actionsMenees.map(function (am) {
                                return am.id;
                            }),
                            actionMeneeAutre: a.actionMeneeAutre,
                            responsablePublicAutre: a.responsablePublicAutre,
                            tiers: a.tiers.map(function (t) {
                                return {
                                    organisationId: t.organisationId
                                }
                            }),
                            observation: a.observation
                        }
                    }),
                    exerciceComptable: vm.activite.exerciceComptable
                };
                return activite;
            };

            function noTiersModal() {
                $mdDialog.show(
                    $mdDialog.alert()
                        .title("Sauvegarde impossible")
                        .textContent("Vous ne pouvez sauvegarder une fiche sans bénéficiaire.")
                        .ariaLabel("Ajout impossible")
                        .ok('OK')
                        .clickOutsideToClose(true));
            };

            vm.checkMinLength = function (field, curLength, maxLength) {
                if (curLength < maxLength) {
                    vm.activiteForm[field].$setValidity("minLength", false);
                } else {
                    vm.activiteForm[field].$setValidity("minLength", true);
                    vm.activiteForm.$dirty = true;
                }
            };

            vm.saveAndPublishActivite = function () {
                if (vm.activiteForm.$valid) {
                	//si aucune données à publier et aucune saisie dans le formulaire
                	if (! vm.activite.dataToPublish && !vm.activiteForm.$dirty) {
                		$mdDialog.show(
                        $mdDialog.alert()
                        .title("publication impossible")
                        .textContent("Il n'y a aucun nouvel élément à publier")
                        .ariaLabel("publication impossible")
                        .ok('OK')
                        .clickOutsideToClose(true));
                	}else{
                		// si pas d'actions saisies
	                    if (!vm.activite.actionsRepresentationInteret.length > 0) {
	                        noTiersModal();
	                    } else {
	                        var activite = buildFiche();
	                        vm.askIA(activite).then(function(res){
	                        if (vm.activiteForm.$dirty || vm.id === null) {
	                            //blockUI.start();           
		                            vm.saveOrUpdate(activite).then(function (res) {
                                        RapportService.updateQualification(res.data.idFiche, vm.idQualif);
                                        vm.activiteForm.$dirty = false;
		                                vm.publier(res.data.id);
	                                    blockUI.stop();
	                                });


	                        } else {
	                            vm.publier(vm.id);
	                        }
                            });
	                    }
                	}
                } else {
                    checkErrors(false);
                }
            };

            vm.saveActiviteAndRedirect = function () {
                if (vm.activiteForm.$valid) {
                    if (!vm.activite.actionsRepresentationInteret.length > 0) {
                        noTiersModal();
                    } else {
                        var activite = buildFiche();

                        if (vm.activiteForm.$dirty || $stateParams.duplicate) {
                            //blockUI.start();
                        	vm.askIA(activite).then( function(res){
                                vm.saveOrUpdate(activite).then(function (res) {
                                    vm.activiteForm.$dirty = false;
                                    blockUI.stop();

                                    var doneRedirect = $mdDialog.alert().title("Données sauvegardées").textContent("Votre fiche d'activité a bien été sauvegardée. Vous pouvez la modifier à tout moment en cliquant 'ÉDITER' sur la ligne correspondante dans le tableau récapitulatif.")
                                        .ariaLabel("Confirmation de sauvegarde").ok("OK").clickOutsideToClose(true);
                                    $mdDialog.show(doneRedirect).then(function () {

                                        activite.id = res.data.id;
                                        RapportService.updateQualification(res.data.idFiche, vm.idQualif);

                                        $state.go('rapports.declaration');
                                    });
                                });
                        	});

                        } else {
                            $state.go('rapports.declaration');
                        }
                    }
                } else {
                    checkErrors(true);
                }
            };

            vm.saveActiviteAndStay = function () {
                if (vm.activiteForm.$valid) {
                    if (!vm.activite.actionsRepresentationInteret.length > 0) {
                        noTiersModal();
                    } else {
                        var activite = buildFiche();

                        if (vm.activiteForm.$dirty || $stateParams.duplicate) {
                            //blockUI.start();
                            vm.askIA(activite).then( function(res){

                                vm.saveOrUpdate(activite).then(function (res) {
                                    vm.activiteForm.$dirty = false;
                                    blockUI.stop();

                                    var doneSave = $mdDialog.alert().title("Données sauvegardées").textContent("Vos informations ont bien été sauvegardées.").ariaLabel("Données sauvegardées").ok('OK').clickOutsideToClose(true);
                                    $mdDialog.show(doneSave).then(function () {

                                        activite.id = res.data.id;
                                        RapportService.updateQualification(res.data.idFiche, vm.idQualif);

                                        $state.reload();
                                    });
                                })
                            });
                        }
                    }
                } else {
                    checkErrors(false);
                }
            };


            vm.publier = function (id) {
                var donePublier = $mdDialog.confirm().title("Attention").htmlContent("Vous vous apprêtez à communiquer à la Haute Autorité cette activité de représentation d'interêts."
                    + " Ces éléments seront rendus publics sur le site internet de la Haute Autorité. <br/><br/>Êtes vous sûr de vouloir continuer ?")
                    .ariaLabel("Attention").ok('Confirmer').cancel('Annuler');
                $mdDialog.show(donePublier).then(function () {
                    blockUI.start();

                    	PublicationService.publierActivite(id).then(function () {
                            blockUI.stop();
                            initData();
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Succès")
                                    .textContent("Votre demande a bien été enregistrée. La publication de ces éléments sur le site de la Haute Autorité sera effective dans les 60 minutes.")
                                    .ariaLabel("Succès")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            ).finally(function () {
                                blockUI.stop();
                            });
                        }).finally(function () {
                        blockUI.stop();
                    });
                },function () {
                    $state.reload();
                });
            };

            vm.saveOrUpdate = function (activite) {
                if (vm.id) {
                    return RapportService.updateActivite(activite);
                } else {
                    return RapportService.saveActivite(activite);
                }
            };

            vm.askIA = function (activite) {
                return $q(function(resolve, reject) {
                    vm.objetQualif = {};
                    vm.objetQualif.objet = activite.objet;
                    vm.objetQualif.sentence = activite.objet;
                    RapportService.getQualification(vm.objetQualif).then(function (res) {
                        vm.idQualif = res.data.id;
                        if(res.data.valid === false && res.data.confidence > 0.8) {
                        	var message = "<p>Il semblerait que l\’objet saisi dans votre fiche d’activités ne présente pas les caractéristiques attendues.</p>"
                        					+"<p>Le voici pour rappel : \""+activite.objet+"\" </p>"
											+"<ul><p>Merci de bien vouloir vous assurer des éléments suivants :</p>"
											+"<li>Que l’objet retranscrit bien l’objectif que vous cherchiez à atteindre (évitez en particulier de rendre compte factuellement de rendez-vous ou d’évènements) ;</li>"
											+"<li>Qu’il indique également, dans la mesure du possible, la ou les décisions publiques visées ;</li>"
											+"<li>Enfin, que le sujet ou la thématique qu’il concerne peuvent être compris facilement.</li></ul>"
											+"<p>Nous vous invitons en complément à consulter notre <a target=\"blank\" href=\"https://www.hatvp.fr/wordpress/wp-content/uploads/2018/09/fiche-pratique-objet-sept-18.pdf\"> fiche pratique </a> pour plus d’informations.";
                            var confirm = $mdDialog.confirm()
                                .title("Objet de votre fiche d’activités")
                                .htmlContent(message)
                                .ariaLabel("Analyse de votre objet ")
                                .ok("modifier l’objet saisi")
                                .cancel("conserver l’objet saisi");
                                $mdDialog.show(confirm).then(function () {
                                        reject(true);
                                    },function(){
                                        resolve(false);
                                });
                        } else {
                            resolve(false);
                        }
                    });
                });
            };


            vm.addAction = function () {
                blockUI.start();
                var actionsRepresentationInteret = {};
                if (vm.activite.isPourTiers && vm.beneficiaires.length === 0) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Ajout impossible")
                            .htmlContent("Vous n'avez plus de bénéficiaires disponibles pour cette fiche d'activités. ")
                            .ariaLabel("Ajout impossible")
                            .ok('OK')
                            .clickOutsideToClose(true));
                } else if (vm.activite.isPourTiers || vm.beneficiaires.indexOf(vm.espaceSimple) > -1) {
                    vm.activite.actionsRepresentationInteret.push(actionsRepresentationInteret);
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Ajout impossible")
                            .htmlContent("Vous avez déclaré que votre organisation n’effectuait pas de représentation d’intérêts pour des tiers sur ce sujet. "
                                + "<br>Afin de pouvoir déclarer des activités pour des tiers, merci d’indiquer que votre organisation effectue des activités pour des tiers en haut de la fiche d’activités. ")
                            .ariaLabel("Ajout impossible")
                            .ok('OK')
                            .clickOutsideToClose(true));
                }
                blockUI.stop();
            };

            /**
             * suppression d'une action
             */
            vm.deleteAction = function (action) {

                var confirm = $mdDialog.confirm()
                    .title("Confirmation de suppression")
                    .textContent("Vous vous apprêtez à supprimer des bénéficiaires de votre fiche.")
                    .ariaLabel("Confirmation de suppression")
                    .ok("Confirmer")
                    .cancel("Annuler");

                $mdDialog.show(confirm).then(function () {
                    if (action.tiers !== undefined) {
                        action.tiers.forEach(function (tier) {
                            if (vm.activite.isPourTiers && tier.organisationId !== vm.espace.organizationId) {
                                vm.beneficiaires.push(tier);
                            } else if (vm.activite.isPourMoi && tier.organisationId === vm.espace.organizationId) {
                                vm.beneficiaires.push(tier);
                            }
                        });
                    }

                    const index = vm.activite.actionsRepresentationInteret.indexOf(action);
                    vm.activite.actionsRepresentationInteret.splice(index, 1);
                    vm.activiteForm.$dirty = true;
                    blockUI.stop();
                });
            };

            /**
             * Ajout d'un tier  : on le retire  de la liste des beneficiaires
             */
            vm.addTier = function (tier) {
                const index = vm.beneficiaires.indexOf(tier);
                vm.beneficiaires.splice(index, 1);
            };

            /**
             * Suppression d'un tier  : on le remet  de la liste des beneficiaires et on le retire des tiers de l'action courante
             */
            vm.removeTier = function (tier) {
                if (vm.activite.isPourTiers && tier.organisationId !== vm.espace.organizationId) {
                    vm.beneficiaires.push(tier);
                } else if (vm.activite.isPourMoi && tier.organisationId === vm.espace.organizationId) {
                    vm.beneficiaires.push(tier);
                }
                const index = this.action.tiers.indexOf(tier);
                this.action.tiers.splice(index, 1);
                //si tiers ne contient plus aucun beneficiaires il faut le supprimer pour rendre invalide le form
                if (this.action.tiers.length === 0) {
                    this.action.tiers = undefined;
                }
            };

            /**
             * Mise à jour des beneficiaires en fonction des cases cochées (pour moi et/ou pour les clients)
             */
            vm.updateListeBeneficiaires = function (choix) {

                const check = vm.checkIsPourMoi(vm.activite.actionsRepresentationInteret);
                if (choix === 'me') {
                    if (vm.activite.isPourMoi && !check) {
                        vm.beneficiaires.unshift(vm.espaceSimple);
                        // preselection de l'organisation en propre dans l'action de l'activite
                        vm.ajouterEnPropre(vm.activite.actionsRepresentationInteret[0]);
                    } else if (check) {
                        //do nothing
                    } else {
                        const index = vm.beneficiaires.indexOf(vm.espaceSimple);
                        vm.beneficiaires.splice(index, 1);
                    }
                } else {
                    if (vm.activite.isPourTiers) {
                        vm.beneficiaires = vm.clients.map(function (c) {
                            return {
                                organisationId: c.id,
                                denomination: c.denomination
                            }
                        });
                        vm.remakeBeneficiaires();
                        if (vm.activite.isPourMoi && !check) {
                            vm.beneficiaires.unshift(vm.espaceSimple);
                        }
                    } else {
                        vm.beneficiaires = [];
                        if (vm.activite.isPourMoi && !check) {
                            vm.beneficiaires.unshift(vm.espaceSimple);
                        }
                    }
                }
            };

            vm.ajouterEnPropre = function(actionsRepresentationInteret) {
                // Si la case 'en propre' est cochée
                if(vm.activite.isPourMoi) {
                    // on veut preselectionner uniquement si il n'y a qu'un seul bloc car
                    // pour laisser le choix au declarant de se placer
                    if(vm.activite.actionsRepresentationInteret.length > 1) { return false; }
                    // on  check si il y a deja une liste de tiers instancié sinon on la créer
                    if(actionsRepresentationInteret.tiers === undefined) {
                        actionsRepresentationInteret.tiers = [];
                        // on check si l'organisation n'est pas deja en tiers sinon on l'ajoute
                        if(actionsRepresentationInteret.tiers.indexOf(vm.espaceSimple) == -1) {
                            // on preselectionne l'organisation pour l'action
                            vm.addTier(vm.espaceSimple);
                            actionsRepresentationInteret.tiers.unshift(vm.espaceSimple);
                        }
                        // dans le cas ou il y a deja une liste de tiers et que l'orga n'est pas deja dedans, on l'ajoute
                    } else if(actionsRepresentationInteret.tiers.indexOf(vm.espaceSimple) == -1) {
                        // on preselectionne l'organisation pour l'action
                        vm.addTier(vm.espaceSimple);
                        actionsRepresentationInteret.tiers.unshift(vm.espaceSimple);
                    }
                }
            };

            /**
             * verifie la présence de tiers pour une action
             * @param array
             * @returns {boolean}
             */
            vm.checkTiers = function (listeActions) {
                var i, j;
                for (i in listeActions) {
                    for (j in listeActions[i].tiers) {
                        if (listeActions[i].tiers[j].organisationId !== vm.espace.organizationId) {
                            return true;
                        }
                    }
                }
                return false;
            };

            /**
             * verifie la présence de l'organisation dans les beneficiaires des activites de la fiche
             * @param array
             * @returns {boolean}
             */
            vm.checkIsPourMoi = function (listeActions) {
                var i, j;
                for (i in listeActions) {
                    for (j in listeActions[i].tiers) {
                        if (listeActions[i].tiers[j].organisationId === vm.espace.organizationId) {
                            return true;
                        }
                    }
                }
                return false;
            };

            /**
             * verifie la présence de tiers pour une action et l'enleve de la liste des bénéficiaires
             * @param array
             * @returns {boolean}
             */
            vm.remakeBeneficiaires = function () {
                var i, j;
                for (i in vm.activite.actionsRepresentationInteret) {
                    for (j in vm.activite.actionsRepresentationInteret[i].tiers) {
                        var tierToRemove = vm.beneficiaires.find(function (tier) {
                            if (tier.organisationId === vm.activite.actionsRepresentationInteret[i].tiers[j].organisationId) {
                                return tier;
                            } else {
                                return undefined;
                            }
                        });

                        if (tierToRemove !== undefined) {
                            const index = vm.beneficiaires.indexOf(tierToRemove);
                            vm.beneficiaires.splice(index, 1);
                        }
                    }
                }
            };

            /**
             * modale pour un tier non trouvé
             */
            vm.getUnknownTier = function () {
                var alert = $mdDialog.alert()
                    .title("Contacter nos services")
                    .textContent("Afin d'ajouter un bénéficiaire n'apparaissant pas dans la liste, merci d'approcher nos services à l’adresse repertoire@hatvp.fr.")
                    .ariaLabel("Contacter nos services")
                    .ok("OK");

                $mdDialog.show(alert);
            };

            /**
             * selection de tous les clients
             */
            vm.selectAll = function () {
                if (this.action.tiers === undefined) {
                    this.action.tiers = [];
                }
                this.action.tiers = this.action.tiers.concat(vm.beneficiaires.map(function (b) {
                    return {
                        organisationId: b.organisationId,
                        denomination: b.denomination,
                        nomUsage: b.nomUsage
                    }
                }));
                vm.beneficiaires.splice(0);
            };

            /**
             * check sur le responsable public 'Autre: à préciser'
             * @param actions
             * @returns {boolean}
             */
            vm.checkResponsablesPublics = function (reponsablesPublics) {
                var i;
                for (i in reponsablesPublics) {
                    if (reponsablesPublics[i].id === 19) {
                        reponsablesPublics.reponsablePublicCheck = true;
                        return true;
                    } else {
                        reponsablesPublics.reponsablePublicCheck = false;
                    }
                }
            };

            /**
             * check sur l'action menée 'Autre: à préciser'
             * @param actions
             * @returns {boolean}
             */
            vm.checkActionsMenees = function (actionsRepresentationInteret) {
                var i;
                for (i in actionsRepresentationInteret) {
                    if (actionsRepresentationInteret[i].id === 10) {
                        actionsRepresentationInteret.actionMeneeCheck = true;
                        return true;
                    } else {
                        actionsRepresentationInteret.actionMeneeCheck = false;
                    }
                }
            };

            /**
             * empeche de saisir plus de 5 domaines
             */
            vm.limiteNombreDomaines = function (domaines) {
                if (domaines && domaines.length > 5) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Ajout impossible")
                            .htmlContent("Vous ne pouvez pas choisir plus de 5 domaines d'intervention.")
                            .ariaLabel("Ajout impossible")
                            .ok('OK')
                            .clickOutsideToClose(true));
                    vm.activite.domainesIntervention = vm.prevModel;
                } else {
                    vm.prevModel = domaines;
                }
            };

            initData();

        }])
    .filter('accentFoldingFilter', [function () {
        /*
         *  ACCENT FOLDING POUR LES FILTRES DE RECHERCHE SUR LES NOMS e = éêë...
         */
        var defaultDiacriticsRemovalMap = [

            {
                'base': 'a',
                'letters': /[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g
            },

            {'base': 'b', 'letters': /[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
            {'base': 'c', 'letters': /[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
            {'base': 'd', 'letters': /[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
            {
                'base': 'e',
                'letters': /[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g
            },
            {'base': 'f', 'letters': /[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
            {'base': 'g', 'letters': /[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
            {'base': 'h', 'letters': /[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
            {'base': 'i', 'letters': /[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
            {'base': 'j', 'letters': /[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
            {'base': 'k', 'letters': /[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
            {'base': 'l', 'letters': /[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
            {'base': 'm', 'letters': /[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
            {'base': 'n', 'letters': /[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
            {
                'base': 'o',
                'letters': /[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g
            },
            {'base': 'p', 'letters': /[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
            {'base': 'q', 'letters': /[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
            {'base': 'r', 'letters': /[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
            {'base': 's', 'letters': /[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
            {'base': 't', 'letters': /[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
            {
                'base': 'u',
                'letters': /[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g
            },
            {'base': 'v', 'letters': /[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
            {'base': 'w', 'letters': /[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
            {'base': 'x', 'letters': /[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
            {'base': 'y', 'letters': /[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
            {'base': 'z', 'letters': /[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
        ];

        var changes;

        function removeDiacritics(str) {
            if (!changes) {
                changes = defaultDiacriticsRemovalMap;
            }
            for (var i = 0; i < changes.length; i++) {
                str = str.replace(changes[i].letters, changes[i].base);
            }
            return str;
        }

        var accentFolding = function (arr, searchText, nomenclature) {

            if (!searchText)
                return arr;
            if (typeof  nomenclature === undefined) {
                nomenclature = '';
            }
            return arr.filter(function (arrayItem) {
                var match = false;

                for (var key in arrayItem) {
                    if (!arrayItem.hasOwnProperty(key) || key === '$$hashKey' || key === 'id')
                        continue;
                    //recherche sur les categories parentes
                    if (key === 'nomenclature') {
                        for (var child in arrayItem[key]) {
                            var libelle = arrayItem[key][child].libelle;
                            if (removeDiacritics(libelle.toString().toLowerCase()).includes(removeDiacritics(searchText.toString().toLowerCase()))) {
                                match = true;
                                break;
                            }
                        }

                    }

                    var search = nomenclature + '' + arrayItem[key].toString();
                    if (removeDiacritics(search.toLowerCase()).includes(removeDiacritics(searchText.toString().toLowerCase()))) {
                        match = true;
                        break;
                    }
                }

                return match;

            });
        };

        return accentFolding;

    }]).controller('ErrorSaisieDialogController', ['HTML', '$state', '$mdDialog', '$rootScope', '$scope', 'errors', 'redirect',
    function (HTML, $state, $mdDialog, $rootScope, $scope, errors, redirect) {
        $scope.errors = errors;
        $scope.redirect = redirect;

        $scope.images_dir = $rootScope.images_dir;

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.redirectPrevious = function (ev) {
            $scope.cancel();
            $state.go('rapports.declaration', {}, {reload: true});
        }
    }]);
