/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.contribution')

        .factory('CollaborateursFactory', ['$rootScope', '$log', '$state', '$q', '$mdDialog', '$injector', 'EnvConf', 'HTML', 'CorporateDataService', 'EspaceOrganisationService', 'blockUI',
            function ($rootScope, $log, $state, $q, $mdDialog, $injector, EnvConf, HTML, CorporateDataService, EspaceOrganisationService, blockUI) {

                // Controller's scope
                var vmCollaborateurs;

                /**
                 * Raffraichi les données de la page
                 * @returns promise
                 */
                function refresh() {
                    vmCollaborateurs.collaborateur = {};
                    vmCollaborateurs.collaborateursTableOptions.promise = CorporateDataService.getAllCollaborators();

                    return $q.all([
                        vmCollaborateurs.collaborateursTableOptions.promise.then(function (res) {
                            vmCollaborateurs.collaborateurs = res.data;
                            vmCollaborateurs.listEnd = vmCollaborateurs.collaborateurs.filter(function (col) {
                                return col.actif === true;
                            }).length - 1;
                        }),
                        EspaceOrganisationService
                            .getCurrentEspaceOrganisation()
                            .then(function (res) {
                                vmCollaborateurs.$parent.corporate = res.data;
                            })
                    ]).then();
                };

                return {
                    /**
                     * Intégre les méthodes du scope
                     * @param scope
                     */
                    set: function (scope) {
                        vmCollaborateurs = scope;

                        vmCollaborateurs.collaborateur = {
                            civilite: "",
                            nom: "",
                            prenom: "",
                            email: "",
                            fonction: ""
                        };

                        vmCollaborateurs.collaborateurs = [];

                        /**
                         * Cancel modifications at the card
                         * @param card
                         */
                        vmCollaborateurs.cancelCard = function (card) {
                            card.active = false;
                            angular.extend(vmCollaborateurs.corporate, card.data_backup);
                        };

                        /**
                         * Remonter collaborateur dans l'ordre hierarchique
                         * @param collab
                         */
                        vmCollaborateurs.upCollab = function (collab) {
                            if (collab.sortOrder > 0) {
                                blockUI.start();
                                CorporateDataService.sortCollaboratorOrder(collab.id, true).then(function (res) {
                                    refresh();
                                }).finally(function () {
                                    blockUI.stop();
                                });
                            }
                        };

                        /**
                         * Faire descendre collaborateur dans l'ordre hierarchique
                         * @param collab
                         */
                        vmCollaborateurs.downCollab = function (collab) {
                            if (collab.sortOrder < vmCollaborateurs.collaborateurs.length - 1) {
                                blockUI.start();
                                CorporateDataService.sortCollaboratorOrder(collab.id, false).then(function (res) {
                                    refresh();
                                }).finally(function () {
                                    blockUI.stop();
                                });
                            }
                        };

                        /**
                         * Supprimer collaborateur de la liste des collaborateurs
                         * @param ev
                         * @param collab
                         */
                        vmCollaborateurs.deleteCollaborateur = function (ev, collab) {
                            var title = "Retirer un collaborateur",
                                message = "Vous êtes sur le point de retirer un collaborateur. " +
                                    "Si vous publiez les modifications apportées, cette action aura pour effet de le " +
                                    "retirer de la liste de vos collaborateurs telle qu'elle figure sur le site de la Haute Autorité.";

                            var confirm = $mdDialog.confirm()
                                .title(title)
                                .textContent(message)
                                .ariaLabel(title)
                                .targetEvent(ev)
                                .ok('Confirmer')
                                .cancel('Annuler');

                            $mdDialog.show(confirm).then(function () {
                                blockUI.start();
                                CorporateDataService.deleteCollaborator(collab.id).then(function (res) {
                                    if (collab.inscrit === true) {
                                        collab.actif = false;
                                    } else {
                                        var index = vmCollaborateurs.collaborateurs.indexOf(collab);
                                        vmCollaborateurs.collaborateurs.splice(index, 1);
                                    }
                                    // 1ère maj
                                    if(!$rootScope.currentPremiereModifValidee){
                                        $mdDialog.show(
                                            $mdDialog.alert()
                                                .title("Informations mises à jour à publier")
                                                .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                                .ariaLabel("Informations mises à jour")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        ).then(function (res) {
                                            $rootScope.currentPremiereModifValidee = true;                                                                              
                                        });
                                    }
                                }).finally(function () {
                                    blockUI.stop();
                                    $state.reload();
                                });
                            });
                        };

                        /**
                         * Activer/Désactiver collaborateur
                         * @param ev
                         * @param collab
                         * @param actif
                         */
                        vmCollaborateurs.setActifCollaborateur = function (ev, collab, actif) {
                            var title, message;

                            if (actif) {
                                title = "Activer un collaborateur";
                                message = "Vous êtes sur le point d'activer un collaborateur qui est inscrit à votre espace collaboratif. " +
                                    "Si vous publiez les modifications apportées, cette action aura pour effet de l'ajouter à la liste de vos collaborateurs telle qu'elle figure sur le site de la Haute Autorité.";
                            }
                            else {
                                title = "Désactiver un collaborateur";
                                message = "Vous êtes sur le point de désactiver un collaborateur qui est inscrit à votre espace collaboratif. " +
                                    "Si vous publiez les modifications apportées, cette action aura pour effet de le retirer de la liste de vos collaborateurs telle qu'elle figure sur le site de la Haute Autorité.";
                            }

                            var confirm = $mdDialog.confirm()
                                .title(title)
                                .textContent(message)
                                .ariaLabel(title)
                                .targetEvent(ev)
                                .ok('Confirmer')
                                .cancel('Annuler');

                            $mdDialog.show(confirm).then(function () {
                                blockUI.start();
                                CorporateDataService.activateCollaborator(collab.id, actif).then(function (res) {
                                    if (collab.inscrit === true) {
                                        collab.actif = actif;
                                    } else {
                                        var index = vmCollaborateurs.collaborateurs.indexOf(collab);
                                        vmCollaborateurs.collaborateurs.splice(index, 1);
                                    }
                                    refresh();
                                }).finally(function () {
                                    blockUI.stop();
                                });
                            });
                        };

                        /**
                         * Ajouter nouveau collaborateur
                         * @param ev
                         * @param form
                         */
                        vmCollaborateurs.showConfirmAddCollaborateur = function (ev, form) {
                            var confirmationMsgM = " est sur le point d'être ajouté à votre liste de collaborateurs. ",
                                confirmationMsgMME = " est sur le point d'être ajoutée à votre liste de collaborateurs. ";

                            var confirm = $mdDialog.confirm()
                                .title("Ajouter un collaborateur")
                                .textContent(vmCollaborateurs.collaborateur.civilite + " " + vmCollaborateurs.collaborateur.nom
                                    + ' ' + vmCollaborateurs.collaborateur.prenom + (vmCollaborateurs.collaborateur.civilite === 'MME' ? confirmationMsgMME : confirmationMsgM) +
                                    "Si vous confirmez cette action, un courriel de notification lui sera envoyé à l'adresse que vous avez renseigné.")
                                .ariaLabel("Ajouter un collaborateur")
                                .targetEvent(ev)
                                .ok('Confirmer')
                                .cancel('Annuler');

                            $mdDialog.show(confirm).then(function () {
                                blockUI.start();
                                CorporateDataService.addNewCollaborator(vmCollaborateurs.collaborateur).then(function (res) {
                                    // 1ère maj
                                    if(!$rootScope.currentPremiereModifValidee){
                                        $mdDialog.show(
                                            $mdDialog.alert()
                                                .title("Informations mises à jour à publier")
                                                .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                                .ariaLabel("Informations mises à jour")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        ).then(function (res) {
                                            $rootScope.currentPremiereModifValidee = true;                                                                              
                                        });
                                    }
                                    refresh().finally(function () {
                                        form.$setPristine();
                                    });

                                }).finally(function () {
                                    blockUI.stop();
                                    $state.reload();
                                });
                            });
                        };

                        vmCollaborateurs.editCollaborateurDialog = function (ev, collaborateur) {
                            $mdDialog.show({
                                controller: 'CollaboratorEditCtrl',
                                locals: {
                                    item: collaborateur,
                                    edit: true
                                },
                                templateUrl: EnvConf.views_dir + "/contribution/collaborateurs/edit_collaborateur.html",
                                parent: angular.element(document.body),
                                targetEvent: ev,
                                clickOutsideToClose: true,
                                fullscreen: true
                            }).then(function () {
                            });
                        };

                    },

                    /**
                     * Paramétrage de la table
                     * @param TABLE_OPTIONS
                     */
                    initTable: function (TABLE_OPTIONS) {
                        vmCollaborateurs.collaborateursTableOptions = angular.copy(TABLE_OPTIONS);
                        vmCollaborateurs.collaborateursTableOptions.order = "sortOrder";
                    },

                    /**
                     * Raffraichi les données de la page
                     * @returns promise
                     */
                    refresh: refresh
                };
            }])

        .controller('CollaboratorEditCtrl', ['$scope', '$state', 'blockUI', '$q', '$rootScope', 'EnvConf', 'HTML', '$mdDialog', 'item', 'CorporateDataService',
            function ($scope, $state, blockUI, $q, $rootScope, EnvConf, HTML, $mdDialog, item, CorporateDataService) {

                $scope.images_dir = $rootScope.images_dir;
                $scope.collaborateur = item;
                $scope.collaborateur.fonctionTemp = $scope.collaborateur.fonction;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $scope.collaborateur.fonction = $scope.collaborateur.fonctionTemp;
                    $mdDialog.cancel();
                };

                $scope.confirmerModificationCollaborateur = function (ev) {
                    blockUI.start();
                    CorporateDataService.editCollaborator($scope.collaborateur).then(function (res) {
                        // 1ère maj
                        if(!$rootScope.currentPremiereModifValidee){
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Informations mises à jour à publier")
                                    .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                    .ariaLabel("Informations mises à jour")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            ).then(function (res) {
                                $rootScope.currentPremiereModifValidee = true;                                                                              
                            });
                        }
                    }).finally(function () {
                        blockUI.stop();
                        $mdDialog.cancel();
                        $state.reload();
                    });
                }
            }])

})();