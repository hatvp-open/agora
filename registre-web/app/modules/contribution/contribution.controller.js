/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.contribution')

/**
 * Controlleur pour écran Contribution
 */
    .controller('ContributionCtrl', ['ActiviteFactory', 'AssociationsFactory', '$scope', '$state', '$mdDialog', 'blockUI', '$q', '$rootScope', '$timeout', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'USER_ROLES', 'InfosFactory', 'CollaborateursFactory', 'DirigeantsFactory',
        function (ActiviteFactory, AssociationsFactory, $scope, $state, $mdDialog, blockUI, $q, $rootScope, $timeout, EnvConf, HTML, TABLE_OPTIONS, USER_ROLES, InfosFactory, CollaborateursFactory, DirigeantsFactory) {
            var vm = $scope;

            /**
             * Inititalisation des onglets de la page
             */
            vm.tabs_content = {
                CORPORATE_CLIENTS: EnvConf.views_dir + HTML.CONTRIBUTION.CORPORATE_CLIENTS,
                CORPORATE_COLLABORATORS: EnvConf.views_dir + HTML.CONTRIBUTION.CORPORATE_COLLABORATORS,
                ASSO_APPARTENANCE: EnvConf.views_dir + HTML.CONTRIBUTION.ASSO_APPARTENANCE,
                DIRIGEANTS: EnvConf.views_dir + HTML.CONTRIBUTION.DIRIGEANTS,
                CORPORATE_INFORMATION: EnvConf.views_dir + HTML.CONTRIBUTION.CORPORATE_INFO,
                DONNEES: EnvConf.views_dir + HTML.CONTRIBUTION.DONNEES,
                REPRESENTATION_INTERET: EnvConf.views_dir + HTML.CONTRIBUTION.REPRESENTATION_INTERET
            };

            if (!$rootScope.currentTab) {
                $rootScope.currentTab = {
                    index: 0,
                    state: $state.current.name
                };
            } else if ($rootScope.currentTab.index === 3) {
                $state.go('identite.initClient');
            } else if ($rootScope.currentTab.index === 4) {
                $state.go('identite.initAssociation');
            }

            /**
             * Nom de l'espace organisation courrant
             */
            vm.espaceName = $rootScope.currentCorporate().organisationDenomination;
            vm.satutDesinscription = $rootScope.currentCorporate().satutDesinscription;
            /**
             * Variables pour la gestion de la date d'exercice comptable
             */
            vm.nombreJours = 31;
            vm.joursList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
            vm.moisList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
            vm.moisListLabel = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];

            /**
             * Ouverture de la page de versement de pièces complémentaires.
             * @param ev
             */
            vm.complement = function (ev) {
                $mdDialog.show({
                    controller: 'ComplementCtrl',
                    templateUrl: EnvConf.views_dir + HTML.COMPLEMENT,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: false
                });
            };
            /**
             * Ouverture de la page de versement de pièces complémentaires.
             * @param ev
             */
            vm.complementDesinscription = function (ev) {
                $mdDialog.show({
                    controller: 'ComplementCtrl',
                    templateUrl: EnvConf.views_dir + HTML.COMPLEMENT_DESINSCRIPTION,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: false
                });
            };

            
            /**
             * On initialise ici les données selon l'onglet sélectionné :
             * on évite ainsi de charger tous les onglets au premier chargement de la page.
             */
            vm.selectedIndex = $rootScope.currentTab ? $rootScope.currentTab.index : 0;
            $scope.$watch('selectedIndex', function (current, old) {
                $rootScope.currentTab.index = vm.selectedIndex;

                if ($rootScope.isCorporateActive()) {

                    switch (current + 1) {
                        case 1: // Informations générales
                            blockUI.start();
                            InfosFactory.set($scope);
                            InfosFactory.refresh().finally(function () {
                                InfosFactory.initCards();
                                blockUI.stop();
                                // update de la limite du nb de jours apres chargements des données.
                                vm.majNbJours();
                            });
                            break;
                        case 2: // Les dirigeants
                            blockUI.start();
                            DirigeantsFactory.set($scope);
                            DirigeantsFactory.initTable(TABLE_OPTIONS);
                            DirigeantsFactory.refresh().finally(function () {
                                blockUI.stop();
                            });
                            break;
                        case 3: // Mes collaborateurs
                            blockUI.start();
                            CollaborateursFactory.set($scope);
                            CollaborateursFactory.initTable(TABLE_OPTIONS);
                            CollaborateursFactory.refresh().finally(function () {
                                blockUI.stop();
                            });
                            break;
                        case 4: // Mes clients
                            // let the Controller do the job
                            break;
                        case 5: // Mes associations
                            // let the controller do the job
                            break;
                        case 6: //représentation d'intéret
                            blockUI.start();
                            ActiviteFactory.set($scope);
                            ActiviteFactory.refresh().finally(function () {
                                blockUI.stop();
                            });
                            break;
                    }
                }
            });

            function getNbJoursDuMois(mois) {
                var moisChoisis = mois || 0;
                return 31 - ((moisChoisis == 2) ? (3) : ((moisChoisis - 1) % 7 % 2));
            }

            vm.majNbJours = function () {
                if (this.cardExerciceComptable.data.moisFinExerciceFiscal !== undefined) {
                    this.nombreJours = getNbJoursDuMois(this.cardExerciceComptable.data.moisFinExerciceFiscal);
                    if(this.nombreJours < this.cardExerciceComptable.data.jourFinExerciceFiscal) {
                        this.cardExerciceComptable.data.jourFinExerciceFiscal = this.nombreJours;
                    }
                    return this.cardExerciceComptable.data.moisFinExerciceFiscal;
                } else {
                    return 0;
                }
            };

            vm.setDateIfChecked = function (ev) {
                vm.cardExerciceComptable.data.existeExerciceComptable = false;
            };

            vm.popUpInfoPublication = function (espaceName) {
                var confirm = $mdDialog.confirm()
                        .title("Attention, pensez à publier")
                        .textContent("Afin d’inscrire définitivement l’entité "+ espaceName + " sur le répertoire, vous devez publier les informations d’identité ")
                        .ariaLabel("Confirmation de création")
                        .ok("Publier maintenant")
                        .cancel("Publier ultérieurement");
                    $mdDialog.show(confirm).then(function () {
                        $state.go('publicationIdentite');                            
                    });                
            };
        }]);