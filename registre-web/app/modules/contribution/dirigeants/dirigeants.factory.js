/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.contribution')

        .factory('DirigeantsFactory', ['$rootScope', 'EspaceOrganisationService', 'CorporateDataService', '$log', '$state', '$mdDialog', '$injector', 'EnvConf', 'HTML', 'blockUI', '$q',
            function ($rootScope, EspaceOrganisationService, CorporateDataService, $log, $state, $mdDialog, $injector, EnvConf, HTML, blockUI, $q) {
                
                // Controller's scope
                var vmDirigeants;

                /**
                 * Raffraichi les données de la page
                 * @returns promise
                 */
                function refresh() {
                    // récupérer informations générales espace organisation
                    vmDirigeants.dirigeant = {};
                    vmDirigeants.dirigeantsTableOptions.promise = CorporateDataService.getAllDirigeants();
                    return vmDirigeants.dirigeantsTableOptions.promise.then(function (res) {
                        vmDirigeants.dirigeants = res.data;
                        EspaceOrganisationService.getCurrentEspaceOrganisation().then(function (res) {
                        	vmDirigeants.corporate = res.data;
                        });
                     });
                }

                return {
                    /**
                     * Intégre les méthodes du scope
                     * @param scope
                     */
                    set: function (scope) {
                        vmDirigeants = scope;

                        vmDirigeants.dirigeant = {
                            civilite: "",
                            nom: "",
                            prenom: "",
                            fonction: ""
                        };

                        vmDirigeants.dirigeants = [];

                        /**
                         * Ajouter nouveau dirigeant
                         * @param ev
                         * @param form
                         */
                        vmDirigeants.showConfirmAddDirigeant = function (ev, form, valid) {
                            if (valid) {
                                var confirmationMsgM = " est sur le point d'être ajouté à votre liste de dirigeants. ",
                                    confirmationMsgMME = " est sur le point d'être ajoutée à votre liste de dirigeants. ";

                                var confirm = $mdDialog.confirm()
                                    .title("Ajouter un dirigeant")
                                    .textContent(vmDirigeants.dirigeant.civilite + " " + vmDirigeants.dirigeant.prenom
                                        + ' ' + vmDirigeants.dirigeant.nom + (vmDirigeants.dirigeant.civilite === 'MME' ? confirmationMsgMME : confirmationMsgM))
                                    .ariaLabel("Ajouter un dirigeant")
                                    .targetEvent(ev)
                                    .ok('Confirmer')
                                    .cancel('Annuler');

                                $mdDialog.show(confirm).then(function () {
                                    blockUI.start();
                                    CorporateDataService.addNewDirigeant(vmDirigeants.dirigeant).then(function (res) {
                                        // 1ère maj
                                        if(!$rootScope.currentPremiereModifValidee){
                                            $mdDialog.show(
                                                $mdDialog.alert()
                                                    .title("Informations mises à jour à publier")
                                                    .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                                    .ariaLabel("Informations mises à jour")
                                                    .ok('OK')
                                                    .clickOutsideToClose(true)
                                            ).then(function (res) {
                                                $rootScope.currentPremiereModifValidee = true;                                                                              
                                            });

                                        }
                                        refresh().finally(function () {
                                            form.$setPristine();
                                        });

                                    }).finally(function () {
                                        blockUI.stop();
                                        $state.reload();
                                    });

                                });
                            }
                        };
                        /**
                         * Remonter dirideant dans l'ordre hierarchique
                         * @param dir
                         */
                        vmDirigeants.upDir = function (dir) {
                            if (dir.sortOrder > 0) {
                                blockUI.start();
                                CorporateDataService.sortDirigeantOrder(dir.id, true).then(function (res) {
                                    refresh();
                                }).finally(function () {
                                    blockUI.stop();
                                });
                            }
                        };

                        /**
                         * Faire descendre dirideant dans l'ordre hierarchique
                         * @param dir
                         */
                        vmDirigeants.downDir = function (dir) {
                            if (dir.sortOrder < vmDirigeants.dirigeants.length - 1) {
                                blockUI.start();
                                CorporateDataService.sortDirigeantOrder(dir.id, false).then(function (res) {
                                    refresh();
                                }).finally(function () {
                                    blockUI.stop();
                                });
                            }
                        };
                        /**
                         * Supprimer dirigeant de la liste des dirigeants
                         * @param ev
                         * @param dirigeant
                         */
                        vmDirigeants.deleteDirigeant = function (ev, dirigeant) {
                            var title = "Retirer un dirigeant",
                                message = "Vous êtes sur le point de retirer un dirigeant. " +
                                    "Si vous publiez les modifications apportées, cette action aura pour effet de le " +
                                    "retirer de la liste de vos dirigeants telle qu'elle figure sur le site de la Haute Autorité.";

                            var confirm = $mdDialog.confirm()
                                .title(title)
                                .textContent(message)
                                .ariaLabel(title)
                                .targetEvent(ev)
                                .clickOutsideToClose(true)
                                .ok('Confirmer')
                                .cancel('Annuler');

                            $mdDialog.show(confirm).then(function () {
                                blockUI.start();
                                CorporateDataService.deleteDirigeant(dirigeant.id).then(function (res) {
                                    var index = vmDirigeants.dirigeants.indexOf(dirigeant);
                                    vmDirigeants.dirigeants.splice(index, 1);
                                    // 1ère maj
                                    if(!$rootScope.currentPremiereModifValidee){
                                        $mdDialog.show(
                                            $mdDialog.alert()
                                                .title("Informations mises à jour à publier")
                                                .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                                .ariaLabel("Informations mises à jour")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        ).then(function (res) {
                                            $rootScope.currentPremiereModifValidee = true;                                                                              
                                        });

                                    }

                                    refresh();
                                }).finally(function () {
                                    blockUI.stop();
                                    $state.reload();
                                });
                            });
                        };

                        vmDirigeants.editDirigeantsDialog = function (ev, dirigeant) {
                            $mdDialog.show({
                                controller: 'DirigeantEditCtrl',
                                locals: {
                                    item: dirigeant,
                                    edit: true
                                },
                                templateUrl: EnvConf.views_dir + "/contribution/dirigeants/edit_dirigeants.html",
                                parent: angular.element(document.body),
                                targetEvent: ev,
                                clickOutsideToClose: true,
                                fullscreen: true
                            }).then(function () {
                            });
                        };

                    },

                    /**
                     * Paramétrage de la table
                     * @param TABLE_OPTIONS
                     */
                    initTable: function (TABLE_OPTIONS) {
                        vmDirigeants.dirigeantsTableOptions = angular.copy(TABLE_OPTIONS);
                        vmDirigeants.dirigeantsTableOptions.order = "sortOrder";
                    },

                    /**
                     * Raffraichi les données de la page
                     * @returns promise
                     */
                    refresh: refresh
                };
            }])

        .controller('DirigeantEditCtrl', ['$scope', '$state', 'blockUI', '$q', '$rootScope', 'EnvConf', 'HTML', '$mdDialog', 'item', 'CorporateDataService',
            function ($scope, $state, blockUI, $q, $rootScope, EnvConf, HTML, $mdDialog, item, CorporateDataService) {

                $scope.images_dir = $rootScope.images_dir;
                $scope.dirigeant = item;
                $scope.dirigeant.fonctionTemp = $scope.dirigeant.fonction;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $scope.dirigeant.fonction = $scope.dirigeant.fonctionTemp;
                    $mdDialog.cancel();
                };

                $scope.confirmerModificationDirigeant = function (ev) {
                    blockUI.start();
                    CorporateDataService.editDirigeant($scope.dirigeant).then(function (res) {
                        // 1ère maj
                        if(!$rootScope.currentPremiereModifValidee){
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Informations mises à jour à publier")
                                    .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                    .ariaLabel("Informations mises à jour")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            ).then(function (res) {
                                $rootScope.currentPremiereModifValidee = true;                                                                              
                            });
                        }
                    }).finally(function () {
                        blockUI.stop();
                        $mdDialog.cancel();
                        $state.reload();
                    });
                }
            }])

})();