/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.contribution')

        .factory('ActiviteFactory', ['$rootScope', 'blockUI', '$log', '$state', '$mdDialog', '$injector', 'EnvConf', 'HTML',
            'EspaceOrganisationService',
            function ($rootScope, blockUI, $log, $state, $mdDialog, $injector, EnvConf, HTML, EspaceOrganisationService) {

                var vmActivite;
                
                
                
                /**
                 * Raffraichi les données de la page
                 * @returns promise
                 */
                function refresh() {
                    return EspaceOrganisationService.getAllActivities().then(function (res) {
                        // Chargement de la liste complète des niveaux d'intervention.
                        vmActivite.niveauIntervention = res.data.niveauInterventionEnums;
                        // Chargement de la liste complète des secteurs d'activités.
                        vmActivite.secteurActivite = res.data.secteurActiviteEnumList;

                        EspaceOrganisationService.getCurrentEspaceOrganisation().then(function (res) {
                        	vmActivite.$parent.corporate = res.data;
                            /* Chargement des niveaux d'intervention */
                            // Chargement de la liste des niveaux d'intervention sélectionnés.
                            var listNiveauInterventionSelected = res.data.listNiveauIntervention;
                            // Enrichissement de la liste complète avec la valeur correspondante (value=true si sélectionné false sinon).
                            vmActivite.niveauIntervention = vmActivite.niveauIntervention.map(function (item) {
                                item.value = vmActivite.findCodeInList(item.code, listNiveauInterventionSelected);
                                return item;
                            });

                            /* Chargement des secteurs d'activités */
                            // Chargement de la liste des niveaux d'intervention sélectionnés.
                            var listSecteurActiviteSelected = res.data.listSecteursActivites;
                            // Enrichissement de la liste complète avec la valeur correspondante (true si sélectionné false sinon).
                            vmActivite.secteurActivite = vmActivite.secteurActivite.map(function (item) {
                                item.value = vmActivite.findCodeInList(item.code, listSecteurActiviteSelected);
                                return item;
                            });

                            /* Mise à jour des compteurs. */
                            vmActivite.updateCountNiveauIntervention();
                            vmActivite.updateCountSecteurActivite();
                        });
                    });
                }
                
                
                
                
                
                
                

                return {
                    /**
                     * Méthode de chargement et de refresh de la page.
                     */

                    set: function (scope) {
                        /** Scope de la page. */
                        vmActivite = scope;

                        /** La liste des niveaux d'intervention. */
                        vmActivite.niveauIntervention = [];
                        /** Compteur des niveaux sélectionnés. */
                        vmActivite.countSelectedNiveauIntervention = 0;

                        /** La liste des secteurs d'activités. */
                        vmActivite.secteurActivite = [];
                        /** Compteur des secteurs sélectionnés. */
                        vmActivite.countSelectedSecteurActivite = 0;

                        /** Incrémente de compteur des niveaux d'intervention sélectionnés (sur le ng-change). */
                        vmActivite.updateCountNiveauIntervention = function () {
                            vmActivite.countSelectedNiveauIntervention = vmActivite.countValueTrueInList(vmActivite.niveauIntervention);
                        };

                        /** Incrémente de compteur des secteurs d'activités sélectionnés. */
                        vmActivite.updateCountSecteurActivite = function () {
                            vmActivite.countSelectedSecteurActivite = vmActivite.countValueTrueInList(vmActivite.secteurActivite);
                        };

                        /** Vérifie qu'on excède pas 5 secteurs d'activités (sur le ng-change) et décoche la 6ème case cochée.*/
                        vmActivite.checkMaxCountSecteurActivite = function (item) {
                            vmActivite.updateCountSecteurActivite();
                            if (vmActivite.countSelectedSecteurActivite > 5) {
                                item.value = false;
                                vmActivite.countSelectedSecteurActivite--;
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .title('Attention')
                                        .textContent('Merci de limiter votre sélection à vos cinq principaux secteurs d’activités.')
                                        .ariaLabel('Attention')
                                        .ok('Oui, j\'ai compris')
                                );
                            }
                        };

                        /**
                         * Détermine si l'élément code d'une liste est égal au codeValue passé en paramètre.
                         * @param codeValue le code recherché.
                         * @param list la list à parcourir.
                         *
                         * @return true si codeValue trouvé parmis les code de la liste.
                         */
                        vmActivite.findCodeInList = function (codeValue, list) {
                            for (var i = 0; i < list.length; i++) {
                                if (list[i].code === codeValue) {
                                    return true;
                                }
                            }
                            return false;
                        };

                        /**
                         * Filtre les éléments de list pour retenir ceux dont le champ value est true.
                         * @param list la liste à filtrer.
                         * @return la liste filtrée.
                         */
                        vmActivite.filterValueTrueInList = function (list) {
                            var newList = [];
                            for (var i = 0; i < list.length; i++) {
                                if (list[i].value === true) {
                                    newList.push(list[i]);
                                }
                            }
                            return newList;
                        };

                        /**
                         * Filtre les éléments de list pour retenir ceux dont le champ value est true.
                         * @param list la liste à parcourir.
                         * @return le nombre d'éléments avec value=true.
                         */
                        vmActivite.countValueTrueInList = function (list) {
                            var count = 0;
                            for (var i = 0; i < list.length; i++) {
                                if (list[i].value === true) {
                                    count++;
                                }
                            }
                            return count;
                        };

                        /* Soumission des données saisies. */
                        vmActivite.submitCorporateData = function (ev, valid, card) {
                            var listNiveauInterventionSelected = vmActivite.filterValueTrueInList(vmActivite.niveauIntervention);
                            var listSecteurActiviteSelected = vmActivite.filterValueTrueInList(vmActivite.secteurActivite);
                            if (valid && !(listSecteurActiviteSelected.length === 0 ||
                                    listNiveauInterventionSelected.length === 0)) {

                                var corporate = {};
                                corporate.listNiveauIntervention = listNiveauInterventionSelected;
                                corporate.listSecteursActivites = listSecteurActiviteSelected;

                                blockUI.start();
                                EspaceOrganisationService.setOrganisation(card, corporate).then(function (res) {
                                    refresh();
                                    // 1ère maj
                                    if(!$rootScope.currentPremiereModifValidee){
                                        $mdDialog.show(
                                            $mdDialog.alert()
                                                .title("Informations mises à jour à publier")
                                                .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                                .ariaLabel("Informations mises à jour")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        ).then(function (res) {
                                            $rootScope.currentPremiereModifValidee = true;                                                                              
                                        });
                                    }else{
                                        $mdDialog.show(
                                            $mdDialog.alert()
                                                .title("Informations mises à jour")
                                                .textContent("Vos modifications ont été sauvegardées avec succès.")
                                                .ariaLabel("Informations mises à jour")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        ).then(function (res) {
    
                                            refresh();
                                        });
                                    }                                    
                                }, function (err) {
                                }).finally(function () {
                                	
                                    blockUI.stop();
                                    $state.reload();
                                    
                                });
                            } else {

                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .title("Attention")
                                        .textContent("Vous devez renseigner au moins un niveau d'intervention et au moins un secteur d'activités")
                                        .ariaLabel("Attention")
                                        .ok('OK')
                                        .clickOutsideToClose(true)
                                )
                            }
                        };
                    },
                    /**
                     * Raffraichi les données de la page
                     * @returns promise
                     */
                    refresh: refresh

                }



            }]);
})();


