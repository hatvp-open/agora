/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.contribution')
    .controller('ContributionClientsCtrl', ['TYPE_RECHERCHE', 'SearchFactory', '$scope', '$rootScope', '$state', '$q', '$location', '$mdDialog', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'OrganisationService', 'CorporateDataService', 'EspaceOrganisationService', 'InscriptionEspaceService', 'blockUI',
        function (TYPE_RECHERCHE, SearchFactory, $scope, $rootScope, $state, $q, $location, $mdDialog, EnvConf, HTML, TABLE_OPTIONS, OrganisationService, CorporateDataService, EspaceOrganisationService, InscriptionEspaceService, blockUI) {

            blockUI.start();
            set($scope);
            initTable(TABLE_OPTIONS);

            if( vmClients.$parent.client == undefined){
                refresh().finally(function () {              
                    blockUI.stop();
                });
            }else{
                blockUI.stop();
            }            

            // Controller's scope
            var vmClients;

            /**
             * Rafraichir les données de la page
             * @returns promise
             */
            function refresh() {
                vmClients.clientsTableOptions.promise = CorporateDataService.getPaginatedClients(vmClients.clientsTableOptions.page, vmClients.clientsTableOptions.limit);
                vmClients.clientsAncienTableOptions.promise = CorporateDataService.getPaginatedClientsAnciens(vmClients.clientsAncienTableOptions.page, vmClients.clientsAncienTableOptions.limit);
                return $q.all([
                    vmClients.clientsTableOptions.promise.then(function (res) {
                        vmClients.clients = res.data.dtos;
                        vmClients.clients.totalNumber = res.data.resultTotalNumber;
                    }),
                    vmClients.clientsAncienTableOptions.promise.then(function (res) {
                        vmClients.clientsAncien = res.data.dtos;
                        vmClients.clientsAncien.totalNumber = res.data.resultTotalNumber;
                    }),
                    EspaceOrganisationService
                        .getCurrentEspaceOrganisation()
                        .then(function (res) {
                            vmClients.$parent.corporate = res.data;
                        })
                ]).then();
            };

            /**
             * Intégre les méthodes du scope
             * @param scope
             */
            function set(scope) {
                vmClients = scope;
                vmClients.clients = [];
                vmClients.client = {};
                
                vmClients.searchTemplate = EnvConf.views_dir + HTML.SEARCH;
                /**
                 * Appel paginé pour la liste des mandants
                 * @param page
                 * @param limit
                 * @returns {*}
                 */
                vmClients.onPaginate = function (page, limit) {
                    vmClients.clientsTableOptions.promise = CorporateDataService.getPaginatedClients(page, limit);
                    return $q.all([
                        vmClients.clientsTableOptions.promise.then(function (res) {
                            vmClients.clients = res.data.dtos;
                            vmClients.clients.totalNumber = res.data.resultTotalNumber;
                        })
                    ]).then();
                };

                vmClients.onPaginateAncien = function (page, limit) {
                    vmClients.clientsAncienTableOptions.promise = CorporateDataService.getPaginatedClientsAnciens(page,limit);
                    return $q.all([
                        vmClients.clientsAncienTableOptions.promise.then(function (res) {
                            vmClients.clientsAncien = res.data.dtos;
                            vmClients.clientsAncien.totalNumber = res.data.resultTotalNumber;
                        }),
                    ]).then();
                };

                vmClients.changeAllClientCurrentness = function (evt) {

                    $mdDialog.show({
                        targetEvent: evt,
                        controller: function () {
                            this.parent = $scope;
                        },
                        controllerAs: 'ctrl',
                        scope: $scope,
                        preserveScope: true,
                        templateUrl: EnvConf.views_dir + HTML.CONTRIBUTION.CORPORATE_MODAL_CHANGE_ALL_CLIENTS_STATUS,
                        parent: angular.element(document.body),
                        clickOutsideToClose: false,
                        escapeToClose: false,
                        fullscreen: false,
                        disableParentScroll: false
                    });
                };

                vmClients.changeAllClientOldness = function (statut) {
                    CorporateDataService.changeAllClientOldness(statut).then(function (res) {
                        refresh();
                        if(!$rootScope.currentPremiereModifValidee){
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Informations mises à jour à publier")
                                    .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                    .ariaLabel("Informations mises à jour")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            ).then(function (res) {
                                $rootScope.currentPremiereModifValidee = true;                                                                              
                            });
                        }
                    }).finally(function () {
                        blockUI.stop();
                        $mdDialog.hide();
                        refresh();
                    });
                };

                vmClients.addClient = function () {
                    blockUI.start();
                    CorporateDataService.addClient(vmClients.selectedOrganization.data).then(function (res) {
                        refresh();
                    }).finally(function () {
                        blockUI.stop();
                    });
                };

                /**
                 * contact HATVP
                 * @param ev
                 */
                vmClients.contact = function (ev) {
                    $mdDialog.show({
                        controller: 'ContactCtrl',
                        templateUrl: EnvConf.views_dir + HTML.CONTACT,
                        locals: {
                            context: 'Ajout_Client'
                        },
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        fullscreen: true
                    });
                };

                /**
                 * ajout lien des membres
                 * @param ev
                 */
                vmClients.lienMembres = function (ev) {
                    $mdDialog.show({
                        controller: function () {
                            this.parent = $scope;
                        },
                        controllerAs: 'ctrl',
                        scope: $scope,
                        preserveScope: true,
                        templateUrl: EnvConf.views_dir + HTML.CONTRIBUTION.CORPORATE_CLIENTS_AJOUT_LIEN_MEMBRES,
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        fullscreen: true
                    });
                };                
                vmClients.saveLienMembres = function (ev,valid) {
                    if (valid) {
                        blockUI.start();
                        CorporateDataService.addLienMembres(vmClients.$parent.corporate).then(function (res) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Enregistrement effectué")
                                    .textContent("Le lien a été enregistré.")
                                    .ariaLabel("Enregistrement effectué")
                                    .ok('OK')
                                    .clickOutsideToClose(true));
                            refresh();
                        }).finally(function () {
                            blockUI.stop();
                        });
                    }
                };
                /**
                 * Autocomplete
                 */
                vmClients.espace_clients = [];

                vmClients.selectedOrganization = null;
                vmClients.validOrganization = false;

                //Initialiser le module de recherche.
                SearchFactory.set(vmClients, TYPE_RECHERCHE.AJOUTER_CLIENT);

                vmClients.showConfirmAddOrganization = function (ev) {
                    var confirm = $mdDialog.confirm()
                        .title("Ajouter un tiers")
                        .textContent('Êtes vous certain de vouloir ajouter cette organisation à la liste des organisations pour lesquelles vous effectuez des actions de représentation d\'intérêts ?')
                        .ariaLabel('Ajouter un tiers')
                        .targetEvent(ev)
                        .ok('Confirmer')
                        .cancel('Annuler');
                    $mdDialog.show(confirm).then(function (res) {
                        vmClients.addClient();
                        // 1ère maj
                        if(!$rootScope.currentPremiereModifValidee){
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Informations mises à jour à publier")
                                    .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                    .ariaLabel("Informations mises à jour")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            ).then(function (res) {
                                $rootScope.currentPremiereModifValidee = true;                                                                              
                            });
                        }
                    });
                };

                /**
                 * if true existing clients are all desactivated
                 * if false nothing is done to any desactivated (manual activation if needed)
                 * @param {*} ev 
                 * @param {*} attesteClient 
                 */
                vmClients.toggleAttestationClient = function (ev, attesteClient) {
                    blockUI.start();
                    EspaceOrganisationService.toggleAttestation(attesteClient, 1).then(function (res) {
                        // 1ère maj
                        if(!$rootScope.currentPremiereModifValidee){
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .title("Informations mises à jour à publier")
                                    .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                    .ariaLabel("Informations mises à jour")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            ).then(function (res) {
                                $rootScope.currentPremiereModifValidee = true;                                                                              
                            });
                        }                                              
                        
                    }).finally(function () {
                        refresh().finally(function () {              
                            blockUI.stop();
                        });
                    });
                };
                /**
                 * No client declaration
                 * if ckeck the box existing clients are all desactivated
                 * if unckeck the box nothing is done to any desactivated (manual activation if needed)
                 * @param {*} evt 
                 * @param {*} elementId 
                 */
                vmClients.check = function (evt, elementId) {
                    if (document.getElementById(elementId).checked === true) {
                        vmClients.corporate.nonDeclarationTiers = false;
                        var confirm = $mdDialog.confirm()
                            .title("Attention")
                            .textContent('Vous déclarez ne pas avoir d\'activités pour le compte de tiers. Cela va désactiver les tiers renseignés. Confirmez vous cette action?')
                            .ariaLabel('Attention')
                            .targetEvent(evt)
                            .ok('Confirmer')
                            .cancel('Annuler');
                        $mdDialog.show(confirm).then(function (res) {
                            // blockUI.start();
                            vmClients.toggleAttestationClient(evt, true);
                        }).then(function () {
                            // blockUI.stop();
                            // $state.reload();
                        }).finally(function () {
                            // blockUI.stop();
                            // $state.reload();
                            // refresh();
                        });
                    } else {
                        vmClients.toggleAttestationClient(evt, false);
                        $state.reload();
                        // refresh();
                    }
                };

                vmClients.toggleAttestationAndAddClient = function (evt) {
                    blockUI.start();
                    EspaceOrganisationService.toggleAttestation(false, 1).then(function (res) {
                        CorporateDataService.addClient(vmClients.selectedOrganization.data).then(function (res) {
                            refresh();
                        }).then(function (err) {
                            vmClients.corporate.nonDeclarationTiers = true;
                        });
                    }).then(function (err) {
                        vmClients.corporate.nonDeclarationTiers = true;
                    }).finally(function () {
                        blockUI.stop();
                        $mdDialog.hide();
                    });
                };

                vmClients.closeModal = function (evt) {
                    $mdDialog.hide();
                };

                vmClients.addClient = function () {
                    blockUI.start();
                    CorporateDataService.addClient(vmClients.selectedOrganization.data).then(function (res) {
                        // refresh();
                    }).finally(function () {
                        blockUI.stop();
                        $state.reload();
                    });
                };
                /**
                 * if client open the pop-up
                 * if old client update the data
                 * @param {*} event 
                 * @param {*} client 
                 */
                vmClients.modifierClient = function (event, client, typeModification) {
                    var title = "";
                    var message = "";
                    if(typeModification == 'desactivation'){
                        title = "Désactivation client";
                        message = "Vous vous apprêtez à désactiver un client parce que vous n’exercez plus d’action de représentation d’intérêt pour son compte.";
                    }else{
                        title = "Activation client";
                        message = "Vous réactivez ce client car vous exercez des actions de représentation d’intérêt pour lui.";
                    }
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title(title)
                            .textContent(message)
                            .ariaLabel(title)
                            .ok('Suivant')
                            .clickOutsideToClose(true)
                    ).then(function (res) {
                        if(typeModification == 'desactivation'){
                            $mdDialog.show({
                                controller: function () {
                                    this.parent = $scope;
                                    $scope.client = client;
                                },
                                controllerAs: 'ctrl',
                                scope: $scope,
                                preserveScope: true,
                                templateUrl: EnvConf.views_dir + HTML.CONTRIBUTION.CORPORATE_CLIENTS_DESACTIVATION,
                                parent: angular.element(document.body),
                                targetEvent: event,
                                clickOutsideToClose: true,
                                fullscreen: true
                            });
    
                        }else  if(typeModification == 'activation'){
                            vmClients.client = client;
                            vmClients.client.ancienClient = false;
    
                            blockUI.start();
                            CorporateDataService.updateClient(vmClients.client).then(function (res) {
                                
                            }).finally(function () {
                                blockUI.stop();
                                $state.reload();
                                // refresh();
                            });                      
    
                        }                                                                          
                    });

                    if(!$rootScope.currentPremiereModifValidee){
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Informations mises à jour à publier")
                                .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                .ariaLabel("Informations mises à jour")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        ).then(function (res) {
                            $rootScope.currentPremiereModifValidee = true;   
                            refresh();                                                                           
                        });
                    }                    
                };
                /**
                 * Send data from desactivation pop-up
                 * @param {*} event 
                 * @param {*} valide 
                 * @param {*} client 
                 */
                vmClients.desactivation = function (event, valide, client) {
                    vmClients.client = $scope.$parent.client;
                    vmClients.client.ancienClient = true;
                    vmClients.client.commentaire = client.commentaire;                    
                    
                    if(client.dateFinContrat != undefined){
                        var jourFinContrat = client.dateFinContrat.slice(0, 2);
                        var moisFinContrat = client.dateFinContrat.slice(2, 4);
                        var anneeFinContrat = client.dateFinContrat.slice(4, 9);
        
                        vmClients.client.dateFinContrat = jourFinContrat + "-" + moisFinContrat + "-" + anneeFinContrat;
                    }
                    blockUI.start();
                    CorporateDataService.updateClient(vmClients.client).then(function (res) {
                        // refresh();
                    }).finally(function () {
                        blockUI.stop();
                        $state.reload();
                    });
                };
            };            

            /**
             * Paramétrage de la table
             * @param TABLE_OPTIONS
             */
            function initTable(TABLE_OPTIONS) {
                vmClients.clientsTableOptions = angular.copy(TABLE_OPTIONS);
                vmClients.clientsAncienTableOptions = angular.copy(TABLE_OPTIONS);
            };
        }]);