/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.contribution')
    .controller('ContributionClientsBatchImportCtrl', ['TYPE_RECHERCHE', 'SearchFactory', '$scope', '$rootScope', '$state', '$q', '$mdDialog', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'OrganisationService', 'CorporateDataService', 'EspaceOrganisationService', 'InscriptionEspaceService', 'blockUI', 'SearchService',
        function (TYPE_RECHERCHE, SearchFactory, $scope, $rootScope, $state, $q, $mdDialog, EnvConf, HTML, TABLE_OPTIONS, OrganisationService, CorporateDataService, EspaceOrganisationService, InscriptionEspaceService, blockUI, SearchService) {

            blockUI.start();
            set($scope);
            blockUI.stop();


            // Controller's scope
            var vmClients;

            /**
             * Intégre les méthodes du scope
             * @param scope
             */
            function set(scope) {

                vmClients = scope;
                vmClients.data = new Object();
                vmClients.data.list = [];
                vmClients.data.list = vmClients.$parent.data? angular.copy(vmClients.$parent.data.list):(
                    !$state.params.clear && sessionStorage.clientBatchReportList ? JSON.parse(sessionStorage.clientBatchReportList ): [] );
                sessionStorage.clientBatchReportList = JSON.stringify(vmClients.data.list);

                vmClients.showConfirmAddOrganizations = function (ev, $scope) {

                    var confirm = $mdDialog.confirm()
                        .title("Ajouter des mandants et clients")
                        .textContent('Vous vous apprêtez à vérifier la liste des mandants et clients issue du traitement de vos données par AGORA.')
                        .ariaLabel('Ajouter des tiers à partir d\'une liste')
                        .targetEvent(ev)
                        .ok('Confirmer')
                        .cancel('Annuler');

                    vmClients.$parent.data = new Object();
                    vmClients.$parent.data.list = angular.copy (vmClients.data.list);

                    $mdDialog.show(confirm).then(function (res) {
                        console.log(vmClients.data.list);
                        $state.go('identite.clientBatchReport');
                    });

                };


            }

        }])
    .controller('ContributionClientsBatchReportCtrl', ['TYPE_RECHERCHE', 'SearchFactory', '$scope', '$rootScope', '$state', '$q', '$mdDialog', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'OrganisationService', 'CorporateDataService', 'EspaceOrganisationService', 'InscriptionEspaceService', 'blockUI', 'SearchService',
        function (TYPE_RECHERCHE, SearchFactory, $scope, $rootScope, $state, $q, $mdDialog, EnvConf, HTML, TABLE_OPTIONS, OrganisationService, CorporateDataService, EspaceOrganisationService, InscriptionEspaceService, blockUI, SearchService) {

            blockUI.start();
            set($scope);
            blockUI.stop();


            // Controller's scope
            var vmClients;


            /**
             * Intégre les méthodes du scope
             * @param scope
             */
            function set(scope) {

                vmClients = scope;
                vmClients.data = new Object();
                vmClients.data.validEntitiesLength = 0;
                vmClients.data.list = [];
                vmClients.data.validList = [];
                vmClients.data.list = vmClients.$parent.data? angular.copy(vmClients.$parent.data.list):(
                    !$state.params.clear && sessionStorage.clientBatchReportList ? JSON.parse(sessionStorage.clientBatchReportList ): [] );

                sessionStorage.clientBatchReportList = JSON.stringify(vmClients.data.list);

                vmClients.addClients = function (list) {
                    blockUI.start();
                    CorporateDataService.addClientBatch(list).then().finally(function () {
                        $state.go('identite.initClient');
                        blockUI.stop();
                    });

                };
                vmClients.verifyClients = function (list) {
                    blockUI.start();
                    CorporateDataService.verifyClientBatch(list).then(function (res) {
                        vmClients.data.clientBatchReport = res.data;
                        vmClients.data.validEntitiesLength = Object.keys(res.data.validEntities).length;
                        vmClients.data.validList = Object.keys(res.data.validEntities);
                        vmClients.data.invalidList = Object.keys(res.data.errorMessages).length !== 0;
                        console.log("ValidList to persist: " + vmClients.data.validList);
                    }).finally(function () {
                        blockUI.stop();
                    });

                };

                vmClients.verifyClients(vmClients.data.list);

                vmClients.showConfirmAddOrganizations = function (ev) {

                    var confirm = $mdDialog.confirm()
                        .title("Ajouter des tiers")
                        .textContent('Êtes vous certain de vouloir ajouter ces organisations à la liste des organisations pour lesquelles vous effectuez des actions de représentation d\'intérêts ?')
                        .ariaLabel('Validation')
                        .targetEvent(ev)
                        .ok('Confirmer')
                        .cancel('Annuler');
                    $mdDialog.show(confirm).then(function (res) {
                        console.log(vmClients.data.validList);
                        vmClients.addClients(vmClients.data.validList);
                    });

                };


            }

        }]);