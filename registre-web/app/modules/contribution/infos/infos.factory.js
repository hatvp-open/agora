/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.contribution')

        .factory('InfosFactory', ['$rootScope', 'blockUI', '$log', '$state', '$mdDialog', '$injector', 'EnvConf', 'HTML',
            'InscriptionEspaceService', 'EspaceOrganisationService', 'OrganisationService',
            function ($rootScope, blockUI, $log, $state, $mdDialog, $injector, EnvConf, HTML,
                      InscriptionEspaceService, EspaceOrganisationService, OrganisationService) {

                // Controller's scope
                var vmInfos;
                var root = $rootScope;
                root.email_contact_repertoire = EnvConf.email_contact_repertoire;
                return {
                    /**
                     * Intégre les méthodes du scope
                     * @param scope
                     */
                    set: function (scope) {
                        vmInfos = scope;

                        vmInfos.corporate = {};
                        vmInfos.cardProfil = {};
                        vmInfos.cardLocalisation = {};
                        vmInfos.cardInfosContact = {};
                        vmInfos.cardWeb = {};
                        vmInfos.cardExerciceComptable = {};

                        vmInfos.maskDefaultValue = function (attr) {
                            if (!vmInfos.corporate[attr] || vmInfos.corporate[attr] === '0')
                                vmInfos.corporate[attr] = '';
                        };

                        // Initialisation des ojets pour la sauvegardes des données des cards et annulation. //
                        /** Initialisation card profil. */
                        vmInfos.initCardProfil = function () {
                            vmInfos.cardProfil = {
                                active: false,
                                data: {
                                    categorieOrganisation: {
                                        code: "",
                                        label: "",
                                        categorie: "",
                                        notifSansChiffreAffaire: ""
                                    },
                                    nomUsage: ""
                                }
                            };
                            vmInfos.cardProfil.data.categorieOrganisation = angular.copy(vmInfos.corporate.categorieOrganisation);
                            vmInfos.cardProfil.data.nomUsage = vmInfos.corporate.nomUsage;
                        };

                        /** Initialisation card localisation. */
                        vmInfos.initCardLocalisation = function () {
                            vmInfos.cardLocalisation = {
                                active: false,
                                data: {
                                    adresse: "",
                                    codePostal: "",
                                    ville: "",
                                    pays: "",
                                    nonPublierMonAdressePhysique: ""
                                }
                            };
                            vmInfos.cardLocalisation.data.adresse = angular.copy(vmInfos.corporate.adresse);
                            vmInfos.cardLocalisation.data.codePostal = angular.copy(vmInfos.corporate.codePostal);
                            vmInfos.cardLocalisation.data.ville = angular.copy(vmInfos.corporate.ville);
                            vmInfos.cardLocalisation.data.pays = angular.copy(vmInfos.corporate.pays);
                            vmInfos.cardLocalisation.data.nonPublierMonAdressePhysique = angular.copy(vmInfos.corporate.nonPublierMonAdressePhysique);
                        };

                        /** Initialisation card infos de contact. */
                        vmInfos.initCardInfosContact = function () {
                            vmInfos.cardInfosContact = {
                                active: false,
                                data: {
                                    telephoneDeContact: "",
                                    nonPublierMonTelephoneDeContact: "",
                                    emailDeContact: "",
                                    nonPublierMonAdresseEmail: ""
                                }
                            };
                            vmInfos.cardInfosContact.data.telephoneDeContact = angular.copy(vmInfos.corporate.telephoneDeContact);
                            vmInfos.cardInfosContact.data.nonPublierMonTelephoneDeContact = angular.copy(vmInfos.corporate.nonPublierMonTelephoneDeContact);
                            vmInfos.cardInfosContact.data.emailDeContact = angular.copy(vmInfos.corporate.emailDeContact);
                            vmInfos.cardInfosContact.data.nonPublierMonAdresseEmail = angular.copy(vmInfos.corporate.nonPublierMonAdresseEmail);
                        };

                        /** Initialisation card web. */
                        vmInfos.initCardWeb = function () {
                            vmInfos.cardWeb = {
                                active: false,
                                data: {
                                    lienSiteWeb: "",
                                    lienListeTiers: "",
                                    lienPageTwitter: "",
                                    lienPageLinkedin: "",
                                    lienPageFacebook: ""
                                }
                            };
                            vmInfos.cardWeb.data.lienSiteWeb = angular.copy(vmInfos.corporate.lienSiteWeb);
                            vmInfos.cardWeb.data.lienListeTiers = angular.copy(vmInfos.corporate.lienListeTiers);
                            vmInfos.cardWeb.data.lienPageTwitter = angular.copy(vmInfos.corporate.lienPageTwitter);
                            vmInfos.cardWeb.data.lienPageLinkedin = angular.copy(vmInfos.corporate.lienPageLinkedin);
                            vmInfos.cardWeb.data.lienPageFacebook = angular.copy(vmInfos.corporate.lienPageFacebook);
                        };

                        /** Initialisation card Exercice comptable. */
                        vmInfos.initCardExerciceComptable = function () {
                            vmInfos.cardExerciceComptable = {
                                active: false,
                                confirm: false,
                                data: {
                                    finExerciceFiscal: "",
                                    moisFinExerciceFiscal: "",
                                    jourFinExerciceFiscal: "",
                                    nonExerciceComptable: ""

                                }

                            };
                            vmInfos.cardExerciceComptable.data.nonExerciceComptable = vmInfos.corporate.nonExerciceComptable;
                            if (vmInfos.corporate.finExerciceFiscal != "" && vmInfos.corporate.finExerciceFiscal != null) {
                                vmInfos.cardExerciceComptable.confirm = true;
                                vmInfos.cardExerciceComptable.data.jourFinExerciceFiscal = vmInfos.corporate.finExerciceFiscal.split('-')[0];
                                vmInfos.cardExerciceComptable.data.moisFinExerciceFiscal = vmInfos.corporate.finExerciceFiscal.split('-')[1];
                                //suppression des 0 devant les chiffres
                                vmInfos.cardExerciceComptable.data.jourFinExerciceFiscal < 10 ? vmInfos.cardExerciceComptable.data.jourFinExerciceFiscal = vmInfos.cardExerciceComptable.data.jourFinExerciceFiscal.substring(1) : vmInfos.cardExerciceComptable.data.jourFinExerciceFiscal = vmInfos.cardExerciceComptable.data.jourFinExerciceFiscal;
                                vmInfos.cardExerciceComptable.data.moisFinExerciceFiscal < 10 ? vmInfos.cardExerciceComptable.data.moisFinExerciceFiscal = vmInfos.cardExerciceComptable.data.moisFinExerciceFiscal.substring(1) : vmInfos.cardExerciceComptable.data.moisFinExerciceFiscal = vmInfos.cardExerciceComptable.data.moisFinExerciceFiscal;
                            }
                        };


                        /**
                         * Cartes graphiques inclues dans cette page
                         * @type {{CARDS: string, PROFILE_ORGA: string, LOCALISATION: string, INFOS_CONTACT: string, DONNEES: string, WEB: string, ASSO_APPARTENANCE: string, DIRIGEANTS: string}}
                         */
                        vmInfos.CARDS = {
                            LOGO: EnvConf.views_dir + HTML.CONTRIBUTION.INFORMATION.CARDS.LOGO,
                            PROFILE_ORGA: EnvConf.views_dir + HTML.CONTRIBUTION.INFORMATION.CARDS.PROFILE_ORGA,
                            LOCALISATION: EnvConf.views_dir + HTML.CONTRIBUTION.INFORMATION.CARDS.LOCALISATION,
                            INFOS_CONTACT: EnvConf.views_dir + HTML.CONTRIBUTION.INFORMATION.CARDS.INFOS_CONTACT,
                            DONNEES: EnvConf.views_dir + HTML.CONTRIBUTION.INFORMATION.CARDS.DONNEES,
                            WEB: EnvConf.views_dir + HTML.CONTRIBUTION.INFORMATION.CARDS.WEB,
                            EXERCICE_COMPTABLE: EnvConf.views_dir + HTML.CONTRIBUTION.INFORMATION.CARDS.EXERCICE_COMPTABLE
                            // à supprimer: modif preprod
                            // DATE_DU_JOUR: EnvConf.views_dir + "/contribution/infos/cards/date_du_jour.card.html"
                            // à supprimer: modif preprod
                        };

                        // Image de profile par défaut
                        vmInfos.imageSrc = $rootScope.images_dir + "/logo_HATVP1.png";

                        /**
                         * Upload Espace Organisation Logo file to server
                         * @param $file
                         * @param $event
                         * @param $flow
                         */
                        vmInfos.uploadLogo = function ($file, $event, $flow) {
                            blockUI.start();
                            var formData = new FormData();
                            formData.append('file', $file.file);
                            EspaceOrganisationService.uploadLogo(formData)
                                .finally(function () {
                                    blockUI.stop();
                                });
                        };

                        /**
                         * Delete Logo file from server
                         * @param $event
                         * @param $flow
                         */
                        vmInfos.deleteLogo = function ($event, $flow) {
                            var title = "Suppression du logo",
                                message = "Voulez-vous vraiment supprimer le logo ?";

                            var confirm = $mdDialog.confirm()
                                .title(title)
                                .textContent(message)
                                .ariaLabel(title)
                                .targetEvent($event)
                                .ok('Confirmer')
                                .cancel('Annuler');

                            $mdDialog.show(confirm).then(function () {
                                blockUI.start();

                                if ($flow.files.length > 0) {
                                    $flow.cancel();
                                }

                                EspaceOrganisationService.deleteLogo()
                                    .then(function () {
                                        $state.reload();
                                    })
                                    .finally(function () {
                                        blockUI.stop();
                                    });
                            });
                        };


                        /**
                         * Publication des informations générales de l'organisation
                         * @param ev event html du clic
                         * @param corporate l'objet organisation avec modifications à publier
                         */
                        vmInfos.submitCorporateData = function (ev, valid, card, data) {

                            if (valid) {
                                vmInfos.cardExerciceComptable.data.finExerciceFiscal = ((data.jourFinExerciceFiscal < 10) ? "0" + data.jourFinExerciceFiscal : data.jourFinExerciceFiscal) + "-" + ((data.moisFinExerciceFiscal < 10) ? "0" + data.moisFinExerciceFiscal : data.moisFinExerciceFiscal);
                                // Service modification
                                blockUI.start();
                                EspaceOrganisationService.setOrganisation(card, data).then(function (res) {
                                    // 1ère maj
                                    if(!$rootScope.currentPremiereModifValidee){
                                        $mdDialog.show(
                                            $mdDialog.alert()
                                                .title("Informations mises à jour à publier")
                                                .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                                .ariaLabel("Informations mises à jour")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        ).then(function (res) {
                                            $rootScope.currentPremiereModifValidee = true;
                                            $state.reload();                                                                               
                                        });

                                    }else{
                                        // 2ème maj et suivantes
                                        $mdDialog.show(
                                            $mdDialog.alert()
                                                .title("Informations mises à jour")
                                                .textContent("Vos modifications ont été enregistrées avec succès.")
                                                .ariaLabel("Informations mises à jour")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        ).then(function (res) {
                                            $state.reload();                                                                               
                                        });
                                    }                                    
                                    vmInfos.corporate = res.data;
                                }, function (err) {
                                    // annulation des modifications si erreur
                                    $state.reload();
                                }).finally(function () {
                                    blockUI.stop();
                                });

                            }
                        };

                        /**
                         * Validation de la date de cloture
                         * @param ev event html du clic
                         * @param corporate l'objet organisation avec modifications à publier
                         */
                        vmInfos.submitDateCloture = function (ev, valid, card, data) {
                            if (valid) {

                                var dialog = $mdDialog.confirm()
                                    .title("Enregistrement")
                                    .textContent("Attention, une fois enregistrées, vous ne pourrez plus modifier ces informations.")
                                    .ariaLabel("Enregistrement")
                                    .ok('Confirmer')
                                    .cancel('Annuler');

                                $mdDialog.show(dialog).then(function () {
                                    if (data.jourFinExerciceFiscal === "" || data.moisFinExerciceFiscal === "") {
                                        var error = $mdDialog.alert()
                                            .title("Informations incomplètes")
                                            .textContent("Veuillez renseigner une date de fin d'exercice ou une date de référence complète")
                                            .ariaLabel("Informations incomplètes")
                                            .ok('OK')
                                            .clickOutsideToClose(true);
                                        $mdDialog.show(error);
                                    } else {
                                            vmInfos.cardExerciceComptable.data.finExerciceFiscal = ((data.jourFinExerciceFiscal < 10) ? "0" + data.jourFinExerciceFiscal : data.jourFinExerciceFiscal) + "-" + ((data.moisFinExerciceFiscal < 10) ? "0" + data.moisFinExerciceFiscal : data.moisFinExerciceFiscal);
                                            vmInfos.saveDateCloture(card, data);
                                    }
                                });
                            }
                        };

                        vmInfos.saveDateCloture = function (card, data) {

                            var alert = $mdDialog.alert()
                                .title("Informations enregistrées")
                                .htmlContent("Vos modifications ont été enregistrées avec succès.<br> Pour toute mise à jour, merci de contacter nos services par courriel à <a  href='mailto:" + root.email_contact_repertoire + "'>" + root.email_contact_repertoire + "</a>")
                                .ariaLabel("Informations mises à jour")
                                .ok('OK')
                                .clickOutsideToClose(true);

                            blockUI.start();
                            EspaceOrganisationService.setOrganisation(card, data).then(function (res) {
                                $mdDialog.show(alert).then(function () {
                                    $state.reload();
                                });
                                vmInfos.corporate = res.data;
                            }, function () {
                                $state.reload();
                            }).finally(function () {
                                blockUI.stop();
                            });
                        };
                    },

                    /**
                     * Raffraichi les données de la page
                     * @returns promise
                     */
                    refresh: function () {
                        // récupérer informations générales espace organisation
                        return EspaceOrganisationService
                            .getCurrentEspaceOrganisation()
                            .then(function (res) {
                                vmInfos.corporate = res.data;
                                OrganisationService.getAllTypeOrganisation().then(function (res) {
                                    vmInfos.corporate.listeOrganisations = res.data;
                                });
                            })
                    },

                    initCards: function () {
                        vmInfos.initCardProfil();
                        vmInfos.initCardLocalisation();
                        vmInfos.initCardInfosContact();
                        vmInfos.initCardWeb();
                        vmInfos.initCardExerciceComptable();
                        // à supprimer: modif preprod
                        //vmInfos.initCardDateDuJour();
                        // à supprimer: modif preprod
                    }

                };
            }]);
})();