/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.contribution')

        .factory('AssociationsFactory', ['EspaceOrganisationService', 'TYPE_RECHERCHE', 'SearchFactory', '$rootScope', '$state', '$q', '$mdDialog', 'EnvConf', 'HTML', 'OrganisationService', 'CorporateDataService', 'blockUI',
            function (EspaceOrganisationService, TYPE_RECHERCHE, SearchFactory, $rootScope, $state, $q, $mdDialog, EnvConf, HTML, OrganisationService, CorporateDataService, blockUI) {

                // Controller's scope
                var vmAsso;

                /**
                 * Rafraichir les données de la page
                 * @returns promise
                 */
                function refresh() {
                    vmAsso.assoTableOptions.promise = CorporateDataService.getPaginatedAssociations(vmAsso.assoTableOptions.page, vmAsso.assoTableOptions.limit);
                    return $q.all([
                        // TODO utilité de cet appel?
                        EspaceOrganisationService
                            .getCurrentEspaceOrganisation()
                            .then(function (res) {
                                vmAsso.corporate = res.data;
                            }),

                        vmAsso.assoTableOptions.promise.then(function (res) {
                            vmAsso.asso = res.data.organisationDtos;
                            vmAsso.asso.totalNumber = res.data.resultTotalNumber;
                        })
                    ]).then();
                }

                return {
                    /**
                     * Intégre les méthodes du scope
                     * @param scope
                     */
                    set: function (scope) {
                        vmAsso = scope;
                        vmAsso.asso = [];

                        vmAsso.searchTemplate = EnvConf.views_dir + HTML.SEARCH;

                        /**
                         * Appel paginé pour la liste des associations
                         * @param page
                         * @param limit
                         * @returns {*}
                         */
                        vmAsso.onPaginate = function(page, limit) {
                            vmAsso.assoTableOptions.promise = CorporateDataService.getPaginatedAssociations(page, limit);
                            return $q.all([
                                // TODO utilité de cet appel?
                                EspaceOrganisationService
                                    .getCurrentEspaceOrganisation()
                                    .then(function (res) {
                                        vmAsso.corporate = res.data;
                                    }),

                                vmAsso.assoTableOptions.promise.then(function (res) {
                                    vmAsso.asso = res.data.organisationDtos;
                                    vmAsso.asso.totalNumber = res.data.resultTotalNumber;
                                })
                            ]).then();
                        };

                        vmAsso.addAssociation = function () {
                            blockUI.start();
                            CorporateDataService.addAssociation(vmAsso.selectedOrganization.data).then(function (res) {
                                refresh();
                            }).finally(function () {
                                blockUI.stop();
                                $state.reload();
                            });
                        };

                        vmAsso.deleteOrganizationConfirmation = function (ev, association) {
                            // Appending dialog to document.body to cover sidenav in docs app
                            var title = "Supprimer un client",
                                message = 'Voulez-vous vraiment supprimer cette organisation ?';

                            var confirm = $mdDialog.confirm()
                                .title(title)
                                .textContent(message)
                                .ariaLabel(title)
                                .targetEvent(ev)
                                .ok('Confirmer')
                                .cancel('Annuler');

                            $mdDialog.show(confirm).then(function () {
                                blockUI.start();
                                CorporateDataService.deleteAssociation(association.id).then(function (res) {
                                    var index = vmAsso.asso.indexOf(association);
                                    vmAsso.asso.splice(index, 1);
                                }).finally(function () {
                                    blockUI.stop();
                                    $state.reload();
                                });
                            });
                        };

                        /**
                         * contact HATVP
                         * @param ev
                         */
                        vmAsso.contact = function (ev) {
                            $mdDialog.show({
                                controller: 'ContactCtrl',
                                templateUrl: EnvConf.views_dir + HTML.CONTACT,
                                locals: {
                                    context: 'Ajout_Asso_Appartenance'
                                },
                                parent: angular.element(document.body),
                                targetEvent: ev,
                                clickOutsideToClose: true,
                                fullscreen: true
                            });
                        };

                        /**
                         * Autocomplete
                         */

                        vmAsso.espace_clients = [];

                        vmAsso.selectedOrganization = null;
                        vmAsso.validOrganization = false;

                        //Initialiser le module de recherche.
                        SearchFactory.set(vmAsso, TYPE_RECHERCHE.AJOUTER_ORGANISATION_PRO);

                        vmAsso.showConfirmAddOrganization = function (ev) {
                            // Appending dialog to document.body to cover sidenav in docs app
                            var confirm = $mdDialog.confirm()
                                .title("Ajouter une organisation ou association d'appartenance")
                                .textContent("Voulez vous vraiment ajouter cette organisation ou association d'appartenance ?")
                                .ariaLabel("Ajouter une organisation ou association d'appartenance")
                                .targetEvent(ev)
                                .ok('Confirmer')
                                .cancel('Annuler');
                            $mdDialog.show(confirm).then(function (res) {
                                vmAsso.addAssociation();
                            });
                        };

                        //Toggle attestation
                        vmAsso.toggleAttestationClient = function (ev, attesteAssociation) {
                            blockUI.start();
                            EspaceOrganisationService.toggleAttestation(attesteAssociation, 2).then(function (res) {
                            }).finally(function () {
                                blockUI.stop();
                                $state.reload();
                            })
                        }
                    },

                    /**
                     * Paramétrage de la table
                     * @param TABLE_OPTIONS
                     */
                    initTable: function (TABLE_OPTIONS) {
                        vmAsso.assoTableOptions = angular.copy(TABLE_OPTIONS);
                    },

                    /**
                     * Raffraichi les données de la page
                     * @returns promise
                     */
                    refresh: refresh

                };
            }]);
})();