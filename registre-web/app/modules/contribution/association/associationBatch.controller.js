/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.contribution')
    .controller('ContributionAssoBatchImportCtrl', ['TYPE_RECHERCHE', 'SearchFactory', '$scope', '$rootScope', '$state', '$q', '$mdDialog', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'OrganisationService', 'CorporateDataService', 'EspaceOrganisationService', 'InscriptionEspaceService', 'blockUI', 'SearchService',
        function (TYPE_RECHERCHE, SearchFactory, $scope, $rootScope, $state, $q, $mdDialog, EnvConf, HTML, TABLE_OPTIONS, OrganisationService, CorporateDataService, EspaceOrganisationService, InscriptionEspaceService, blockUI, SearchService) {

            blockUI.start();
            set($scope);
            blockUI.stop();

            // Controller's scope
            var vmAsso;

            /**
             * Intégre les méthodes du scope
             * @param scope
             */
            function set(scope) {

                vmAsso = scope;
                vmAsso.data = new Object();
                vmAsso.data.list = [];
                vmAsso.data.list = vmAsso.$parent.data? angular.copy(vmAsso.$parent.data.list):(
                    !$state.params.clear && sessionStorage.clientBatchReportList ? JSON.parse(sessionStorage.clientBatchReportList ): [] );
                sessionStorage.clientBatchReportList = JSON.stringify(vmAsso.data.list);

                vmAsso.showConfirmAddOrganizations = function (ev, $scope) {

                    var confirm = $mdDialog.confirm()
                        .title("Ajouter des organisations et associations d’appartenance ")
                        .textContent('Vous vous apprêter à vérifier la liste des organisations et associations d’appartenance issue du traitement de vos données par AGORA.')
                        .ariaLabel('Ajouter des tiers à partir d\'une liste')
                        .targetEvent(ev)
                        .ok('Confirmer')
                        .cancel('Annuler');

                    vmAsso.$parent.data = new Object();
                    vmAsso.$parent.data.list = angular.copy (vmAsso.data.list);

                    $mdDialog.show(confirm).then(function (res) {
                        $state.go('identite.associationBatchReport');
                    });

                };

            }

        }])
    .controller('ContributionAssoBatchReportCtrl', ['TYPE_RECHERCHE', 'SearchFactory', '$scope', '$rootScope', '$state', '$q', '$mdDialog', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'OrganisationService', 'CorporateDataService', 'EspaceOrganisationService', 'InscriptionEspaceService', 'blockUI', 'SearchService',
        function (TYPE_RECHERCHE, SearchFactory, $scope, $rootScope, $state, $q, $mdDialog, EnvConf, HTML, TABLE_OPTIONS, OrganisationService, CorporateDataService, EspaceOrganisationService, InscriptionEspaceService, blockUI, SearchService) {

            blockUI.start();
            set($scope);
            blockUI.stop();


            // Controller's scope
            var vmAsso;


            /**
             * Intégre les méthodes du scope
             * @param scope
             */
            function set(scope) {

                vmAsso = scope;
                vmAsso.data = new Object();
                vmAsso.data.validEntitiesLength = 0;
                vmAsso.data.list = [];
                vmAsso.data.validList = [];
                vmAsso.data.list = vmAsso.$parent.data? angular.copy(vmAsso.$parent.data.list):(
                    !$state.params.clear && sessionStorage.clientBatchReportList ? JSON.parse(sessionStorage.clientBatchReportList ): [] );

                sessionStorage.clientBatchReportList = JSON.stringify(vmAsso.data.list);

                vmAsso.addAssociations = function (list) {
                    blockUI.start();
                    CorporateDataService.addAssociationBatch(list).then().finally(function () {
                        $state.go('identite.initAssociation');
                        blockUI.stop();
                    });

                };
                vmAsso.verifyAssociations = function (list) {
                    blockUI.start();
                    CorporateDataService.verifyAssociationBatch(list).then(function (res) {
                        vmAsso.data.clientBatchReport = res.data;
                        vmAsso.data.validEntitiesLength = Object.keys(res.data.validEntities).length;
                        vmAsso.data.validList = Object.keys(res.data.validEntities);
                        vmAsso.data.invalidList = Object.keys(res.data.errorMessages).length !== 0;;
                    }).finally(function () {
                        blockUI.stop();
                    });

                };

                vmAsso.verifyAssociations(vmAsso.data.list);

                vmAsso.showConfirmAddOrganizations = function (ev) {

                    var confirm = $mdDialog.confirm()
                        .title("Ajouter des tiers")
                        .textContent('Êtes vous certain de vouloir ajouter ces organisations à la liste des organisations pour lesquelles vous effectuez des actions de représentation d\'intérêts ?')
                        .ariaLabel('Validation')
                        .targetEvent(ev)
                        .ok('Confirmer')
                        .cancel('Annuler');
                    $mdDialog.show(confirm).then(function (res) {
                        vmAsso.addAssociations(vmAsso.data.validList);
                    });

                };


            }

        }]);