/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use Strict';

    angular.module('hatvpRegistre.contribution')
        .controller('ContributionAssoCtrl', ['InfosFactory', 'EspaceOrganisationService', 'TYPE_RECHERCHE', 'SearchFactory', '$scope', '$rootScope', '$state', '$q', '$mdDialog', 'EnvConf', 'HTML', 'TABLE_OPTIONS', 'OrganisationService', 'CorporateDataService', 'blockUI',
            function (InfosFactory, EspaceOrganisationService, TYPE_RECHERCHE, SearchFactory, $scope, $rootScope, $state, $q, $mdDialog, EnvConf, HTML, TABLE_OPTIONS, OrganisationService, CorporateDataService, blockUI) {

                blockUI.start();
                //InfosFactory.set($scope);
                set($scope);
                initTable(TABLE_OPTIONS);
                refresh().finally(function () {
                 blockUI.stop();
                });

                // Controller's scope
                var vmAsso;

                /**
                 * Rafraichir les données de la page
                 * @returns promise
                 */
                function refresh() {
                    vmAsso.assoTableOptions.promise = CorporateDataService.getPaginatedAssociations(vmAsso.assoTableOptions.page, vmAsso.assoTableOptions.limit);
                    return $q.all([
                        vmAsso.assoTableOptions.promise.then(function (res) {
                            vmAsso.asso = res.data.dtos;
                            vmAsso.asso.totalNumber = res.data.resultTotalNumber;
                        }),
                        EspaceOrganisationService
                            .getCurrentEspaceOrganisation()
                            .then(function (res) {
                                vmAsso.$parent.corporate = res.data;
                            })
                    ]).then();
                }

                /**
                 * Intégre les méthodes du scope
                 * @param scope
                 */
                function set(scope) {
                    vmAsso = scope;
                    vmAsso.asso = [];

                    vmAsso.searchTemplate = EnvConf.views_dir + HTML.SEARCH;

                    /**
                     * Appel paginé pour la liste des associations
                     * @param page
                     * @param limit
                     * @returns {*}
                     */
                    vmAsso.onPaginate = function(page, limit) {
                        vmAsso.assoTableOptions.promise = CorporateDataService.getPaginatedAssociations(page, limit);
                        return $q.all([
                            vmAsso.assoTableOptions.promise.then(function (res) {
                                vmAsso.asso = res.data.dtos;
                                vmAsso.asso.totalNumber = res.data.resultTotalNumber;
                            })
                        ]).then();
                    };

                    vmAsso.addAssociation = function () {
                        blockUI.start();
                        CorporateDataService.addAssociation(vmAsso.selectedOrganization.data).then(function (res) {
                            refresh();
                        }).finally(function () {
                            blockUI.stop();
                        });
                    };

                    vmAsso.deleteOrganizationConfirmation = function (ev, association) {
                        // Appending dialog to document.body to cover sidenav in docs app
                        var title = "Supprimer un client",
                            message = 'Voulez-vous vraiment supprimer cette organisation ?';

                        var confirm = $mdDialog.confirm()
                            .title(title)
                            .textContent(message)
                            .ariaLabel(title)
                            .targetEvent(ev)
                            .ok('Confirmer')
                            .cancel('Annuler');

                        $mdDialog.show(confirm).then(function () {
                            blockUI.start();
                            CorporateDataService.deleteAssociation(association.id).then(function (res) {
                                var index = vmAsso.asso.indexOf(association);
                                vmAsso.asso.splice(index, 1);
                                // 1ère maj
                                if(!$rootScope.currentPremiereModifValidee){
                                    $mdDialog.show(
                                        $mdDialog.alert()
                                            .title("Informations mises à jour à publier")
                                            .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                            .ariaLabel("Informations mises à jour")
                                            .ok('OK')
                                            .clickOutsideToClose(true)
                                    ).then(function (res) {
                                        $rootScope.currentPremiereModifValidee = true;                                                                              
                                    });
                                }
                            }).finally(function () {
                                blockUI.stop();
                            });
                        });
                    };

                    /**
                     * contact HATVP
                     * @param ev
                     */
                    vmAsso.contact = function (ev) {
                        $mdDialog.show({
                            controller: 'ContactCtrl',
                            templateUrl: EnvConf.views_dir + HTML.CONTACT,
                            locals: {
                                context: 'Ajout_Asso_Appartenance'
                            },
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: true,
                            fullscreen: true
                        });
                    };

                    /**
                     * Autocomplete
                     */

                    vmAsso.espace_clients = [];

                    vmAsso.selectedOrganization = null;
                    vmAsso.validOrganization = false;

                    //Initialiser le module de recherche.
                    SearchFactory.set(vmAsso, TYPE_RECHERCHE.AJOUTER_ORGANISATION_PRO);

                    vmAsso.showConfirmAddOrganization = function (ev) {
                        // Appending dialog to document.body to cover sidenav in docs app
                        var confirm = $mdDialog.confirm()
                            .title("Ajouter une organisation ou association d'appartenance")
                            .textContent("Voulez vous vraiment ajouter cette organisation ou association d'appartenance ?")
                            .ariaLabel("Ajouter une organisation ou association d'appartenance")
                            .targetEvent(ev)
                            .ok('Confirmer')
                            .cancel('Annuler');
                        $mdDialog.show(confirm).then(function (res) {
                            vmAsso.addAssociation();
                        });
                    };

                    //Toggle attestation
                    vmAsso.toggleAttestationClient = function (ev, attesteAssociation) {
                        blockUI.start();
                        EspaceOrganisationService.toggleAttestation(attesteAssociation, 2).then(function (res) {
                            // 1ère maj
                            if(!$rootScope.currentPremiereModifValidee){
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .title("Informations mises à jour à publier")
                                        .textContent("Vos modifications ont été enregistrées avec succès. Toute modification des informations d’identité de votre structure doit être publiée. Si vous avez plusieurs modifications à effectuer, nous vous conseillons de les publier à la fin de toutes vos modifications.")
                                        .ariaLabel("Informations mises à jour")
                                        .ok('OK')
                                        .clickOutsideToClose(true)
                                ).then(function (res) {
                                    $rootScope.currentPremiereModifValidee = true;                                                                              
                                });
                            }
                            refresh();
                        }).finally(function () {
                            blockUI.stop();
                        })
                    }

                }

                /**
                 * Paramétrage de la table
                 * @param TABLE_OPTIONS
                 */
                function initTable (TABLE_OPTIONS) {
                    vmAsso.assoTableOptions = angular.copy(TABLE_OPTIONS);
                }

            }]);