/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.profile')

    .controller('ValidatePasswordCtrl', ['$scope', '$rootScope', '$log', '$mdDialog', '$state', '$stateParams', '$mdToast', 'ProfileService', 'AuthService', 'blockUI',
        function ($scope, $rootScope, $log, $mdDialog, $state, $stateParams, $mdToast, ProfileService, AuthService, blockUI) {
            var vm = $scope;

            /***********************************************************
             * Initialisation des variables
             ***********************************************************/
            vm.user = {
                password: ''
            };

            var token = $stateParams.key;
            var type = $stateParams.type;


            /***********************************************************
             * Déclaration des fonctions dans le scope.
             ***********************************************************/
            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            /**
             * Appel service pour validation du mot de passe suivi d'une authentification automatique
             * @param valid
             */
            // http://[SERVER]/app/#!/profile/validate?key=[TOKEN]
            vm.validate_password = function (valid) {
                if (valid) {
                    blockUI.start();
                    switch (type) {
                        case "1": //validation compte
                            ProfileService.validate_compte(token, vm.user.password).then(function (res) {
                                var authModel = {
                                    email: res.data.email,
                                    password: vm.user.password
                                };

                                // authentification
                                AuthService.init().then(function (res) {
                                    AuthService.authenticate(authModel).then(function (res) {
                                        $mdToast.show(
                                            $mdToast.simple()
                                                .theme('valid')
                                                .textContent("Votre compte associé à l'adresse courriel " + res.data.email + " est maintenant activé.")
                                                .action('Fermer')
                                                .position('top right')
                                                .hideDelay(8000)
                                            //.capsule(true)
                                        );

                                        $state.go('home', {}, {reload: true});
                                    }).finally(function () {
                                        blockUI.stop();
                                    });
                                }, function (err) {
                                    blockUI.stop();
                                });

                            }, function (err) {
                                blockUI.stop();
                            });
                            break;
                        case "2": //validation nouvelle adresse email
                            ProfileService.validate_email(token, vm.user.password).then(function (res) {
                                var authModel = {
                                    email: res.data.email,
                                    password: vm.user.password
                                };

                                // authentification
                                AuthService.init().then(function (res) {
                                    AuthService.authenticate(authModel).then(function (res) {
                                        $mdToast.show(
                                            $mdToast.simple()
                                                .theme('valid')
                                                .textContent("Votre compte associé à l'adresse courriel " + res.data.email + " est maintenant activé.")
                                                .action('Fermer')
                                                .position('top right')
                                                .hideDelay(8000)
                                            //.capsule(true)
                                        );

                                        $state.go('home', {}, {reload: true});
                                    }).finally(function () {
                                        blockUI.stop();
                                    });
                                }, function (err) {
                                    blockUI.stop();
                                });
                            }, function (err) {
                                blockUI.stop();
                            });
                            break;
                    }

                }
                else {
                    alert("Merci de compléter le formulaire");
                }
            }

        }]);