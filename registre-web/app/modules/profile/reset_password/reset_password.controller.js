/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.profile')

    .controller('ResetPasswordCtrl', ['$scope', '$log', '$mdDialog', '$state', '$stateParams', 'ProfileService', 'blockUI',
        function ($scope, $log, $mdDialog, $state, $stateParams, ProfileService, blockUI) {
            var vm = $scope;

            /***********************************************************
             * Initialisation des variables
             ***********************************************************/
            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            vm.user = {
                password: '',
                password2: ''
            };

            var token = $stateParams.key;

            /***********************************************************
             * Déclaration des fonctions dans le scope.
             ***********************************************************/
            // http://[SERVER]/app/#!/reset-password?key=[TOKEN]
            vm.reset_password = function (valid) {
                if (valid) {
                    blockUI.start();
                    ProfileService.init_password_2(token, vm.user.password).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title("Mot de passe modifié")
                                .textContent("Votre mot de passe est maintenant réinitialisé.")
                                .ariaLabel("Mot de passe modifié")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        ).then(function () {
                            $state.go('home');
                        });
                    }).finally(function () {
                        blockUI.stop();
                    });

                }
                else {
                    alert("Merci de valider le formulaire");
                }
            }

        }]);