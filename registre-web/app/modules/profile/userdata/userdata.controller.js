/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.profile')

    .controller('UserDataCtrl', ['$rootScope', '$log', '$scope', '$state', '$mdDialog', '$mdToast', 'AuthService', 'ProfileService', 'EnvConf', 'API', 'HTML', 'blockUI', '$q',
        function ($rootScope, $log, $scope, $state, $mdDialog, $mdToast, AuthService, ProfileService, EnvConf, API, HTML, blockUI, $q) {
            var vm = $scope;
            vm.maxFileSizeInMB = $rootScope.getMaxUploadSizeInMB();

            /***********************************************************
             * Initialisation des variables
             ***********************************************************/

            /* Récupération données du déclarant */
            vm.user = AuthService.getUser();
            vm.pieces = [];
            vm.declarant = [];

            /**
             * URL pour le service de téléchargement des pièces.
             */
            vm.pieceDownloadUrl = EnvConf.api + API.declarant.root + API.declarant.pieces.root + API.declarant.pieces.download;

            /* Conversion date de naissance string en objet date pour le datepicker */
            // vm.user.date_naissance = new Date(vm.user.date_naissance);

            vm.settings = {
                maxDate: new Date(),
                disableCaching: false
            };

            /***********************************************************
             * Déclaration des fonctions dans le scope.
             ***********************************************************/
            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            /**
             * Modifier Email principal.
             */
            vm.editEmail = function (ev) {
                var confirm = $mdDialog.prompt()
                    .title('Modification de l\'adresse courriel principale')
                    .textContent('Veuillez saisir votre nouvelle adresse :')
                    .placeholder('Adresse courriel principale')
                    .ariaLabel('Adresse courriel principale')
                    .initialValue(vm.user.email)
                    .targetEvent(ev)
                    .ok('Modifier')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function (result) {
                    blockUI.start();
                    ProfileService.update_email(result).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .targetEvent(ev)
                                .title("Confirmation de modification")
                                .textContent("Un courriel de validation vous a été envoyé à l'adresse " + result + ". Merci de cliquer sur le lien contenu dans ce courriel pour valider cette nouvelle adresse.")
                                .ariaLabel("Confirmation de modification")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        );
                    }).finally(function () {
                        blockUI.stop();
                    });
                }, function () {
                });
            };

            /**
             * Modifier Téléphone.
             */
            vm.editTelephone = function (ev) {
                var confirm = $mdDialog.prompt()
                    .title('Modification du numéro de téléphone')
                    .textContent('Veuillez saisir votre nouveau numéro :')
                    .placeholder('Numéro de téléphone')
                    .ariaLabel('Numéro de téléphone')
                    .initialValue(vm.user.telephone)
                    .targetEvent(ev)
                    .ok('Modifier')
                    .cancel('Annuler');

                $mdDialog.show(confirm).then(function (result) {
                    blockUI.start();
                    ProfileService.update_telephone(result).then(function (res) {
                        AuthService.refresh().then(function () {
                            $state.reload();
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .targetEvent(ev)
                                    .title("Confirmation de modification")
                                    .textContent("Votre numéro de téléphone a été modifié avec succès.")
                                    .ariaLabel("Confirmation de modification")
                                    .ok('OK')
                                    .clickOutsideToClose(true)
                            );
                        }).finally(function () {
                            blockUI.stop();
                        });
                    }, function (err) {
                        blockUI.stop();
                    });
                }, function () {
                });
            };

            /**
             * Mot de passe.
             */
            vm.editPassword = function (ev) {
                $mdDialog.show({
                    controller: 'PasswordCtrl',
                    templateUrl: EnvConf.views_dir + HTML.PROFIL.password,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: true
                });
            };

            /**
             * Ajouter une pièce jointe
             */
            vm.addPiece = function (ev) {
                var confirm = $mdDialog.confirm()
                    .title('Ajout d\'une pièce d\'identité')
                    .textContent('Ajouter cette pièce d\'identité ?')
                    .targetEvent(ev)
                    .ok('Ajouter')
                    .cancel('Annuler');

                var formData = new FormData();
                vm.files01.length > 0 && formData.append('file1', vm.files01[0].lfFile);

                $mdDialog.show(confirm).then(function (result) {
                    blockUI.start();
                    ProfileService.addPiece(formData).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .targetEvent(ev)
                                .title("Confirmation de l'ajout")
                                .textContent("La pièce d'identité a bien été ajoutée.")
                                .ariaLabel("Confirmation de l'ajout")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        ).then(function() {
                            $state.reload();
                        });
                    }).finally(function () {
                        blockUI.stop();
                    });
                }, function () {
                });
            };

            /**
             * Informations de contact.
             */

            // fonctions de modification Emails et Tels
            vm.addEmail = function () {
                vm.user.emails_complement.push({email: "", categorie: ""});
            };

            vm.remEmail = function (i) {
                vm.user.emails_complement.splice(i, 1);
            };

            vm.addTel = function () {
                vm.user.telephones.push({telephone: "", categorie: ""});
            };

            vm.remTel = function (i) {
                var tel = vm.user.telephones[i];
                vm.user.telephones.splice(i, 1);
            };

            /**
             * Supprimer les données de contact vide.
             */
            function filterEmails(element) {
                return ((element.categorie !== undefined && element.categorie.length !== 0) && (element.email !== undefined && element.email.length !== 0));
            }

            function filterPhones(element) {
                return ((element.categorie !== undefined && element.categorie.length !== 0) && (element.telephone !== undefined && element.telephone.length !== 0));
            }

            //  Modifier contacts emails et téléphones
            vm.editContacts = function (ev, valid) {
                if (valid) { // formulaire valide
                    blockUI.start();
                    ProfileService.update_contact({
                        emails_complement: vm.user.emails_complement.filter(filterEmails),
                        telephones: vm.user.telephones.filter(filterPhones)
                    }).then(function (res) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .targetEvent(ev)
                                .title("Moyens de contact mis à jour")
                                .textContent("Vos moyens de contact ont été mis à jour avec succès.")
                                .ariaLabel("Moyens de contact mis à jour")
                                .ok('OK')
                                .clickOutsideToClose(true)
                        ).then(function () {
                            AuthService.refresh().then(function (res) {
                                $state.reload();
                            });
                        });
                    }).finally(function () {
                        blockUI.stop();
                    });
                }
                else { // Formulaire non valide
                    $mdDialog.show(
                        $mdDialog.alert()
                            .targetEvent(ev)
                            .title("Formulaire invalide")
                            .textContent("Au moins un champ de votre formulaire de contacts est incorrect. Veuillez réessayer.")
                            .ariaLabel("Formulaire invalide")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    );
                }
            };

            /***********************************************************
             * Déclaration des fonctions privées.
             ***********************************************************/
            /* Placer les fonctions hors scope ici. */

            /***********************************************************
             * Initialisation du controlleur.
             ***********************************************************/
            var initController = function () {
                /* Démarrage spinner */
                blockUI.start();

                /* Ici les fonctions sans appel au serveur */
                vm.addEmail();
                vm.addTel();

                /* Ici les promises : on attends les réponses des appels à l'API pour masquer le spinner */
                $q.all([
                    /* Placer les appels asynchrones ici. */
                    vm.declarant.promise = ProfileService.getPieces(),
                    vm.declarant.promise.then(function (res) {
                        vm.pieces = res.data;
                    })
                ]).then(function () {
                }).finally(function () {
                    /* Arrêt spinner */
                    blockUI.stop();
                });
            };
            initController();

        }]);