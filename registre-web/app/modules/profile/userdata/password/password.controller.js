/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.profile')

    .controller('PasswordCtrl', ['$scope', '$rootScope', '$state', '$log', '$mdDialog', 'ProfileService', 'blockUI',
        function ($scope, $rootScope, $state, $log, $mdDialog, ProfileService, blockUI) {
            var vm = $scope;

            /***********************************************************
             * Initialisation des variables
             ***********************************************************/
            vm.images_dir = $rootScope.images_dir;

            vm.user = {
                prev_password: "",
                password: "",
                password2: ""
            };

            /***********************************************************
             * Déclaration des fonctions dans le scope.
             ***********************************************************/
            // fermer modale
            vm.cancel = function () {
                $mdDialog.cancel();
            };

            /**
             * Appel service de modification du mot de passe
             */
            vm.editPassword = function () {
                blockUI.start();
                ProfileService.update_password(vm.user.prev_password, vm.user.password).then(function (response) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title("Mot de passe modifié")
                            .textContent("Votre mot de passe a été modifié avec succès.")
                            .ariaLabel("Mot de passe modifié")
                            .ok('OK')
                            .clickOutsideToClose(true)
                    ).then(function () {
                        $state.go('home');
                    });
                }).finally(function () {
                    blockUI.stop();
                });
            };

        }]);