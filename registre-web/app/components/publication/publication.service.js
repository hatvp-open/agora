/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('PublicationService', ['$rootScope', '$http', '$q', '$log', '$cookies', 'EnvConf', 'API',
            function ($rootScope, $http, $q, $log, $cookies, EnvConf, API) {

                var PUBLICATION_URL = EnvConf.api + API.corporate.root + API.corporate.publication.root;
                var PUBLICATION_NEW_URL = EnvConf.api + API.corporate.root + API.corporate.publication.root + API.corporate.publication.new;
                var PUBLICATION_LAST_URL = EnvConf.api + API.corporate.root + API.corporate.publication.root + API.corporate.publication.last;

                var PUBLICATION_EXERCICES_URL = EnvConf.api + API.corporate.root + API.corporate.publication.root + API.corporate.publication.exercice;
                var PUBLICATION_ACTIVITE_URL = EnvConf.api + API.corporate.root + API.corporate.publication.root + API.corporate.publication.activite;
                var PUBLICATIONS_COUNT_URL = PUBLICATION_URL + API.corporate.publication.count;

                return {

                    /**
                     * Récupérer les données de la dernière publication de l'espace organisation.
                     */
                    getLastPublication: function () {
                        var deferred = $q.defer();
                        $http.get(PUBLICATION_LAST_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer les données de la nouvelle publication de l'espace organisation.
                     */
                    getNewPublication: function () {
                        var deferred = $q.defer();
                        $http.get(PUBLICATION_NEW_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Publier les données de l'espace corporate
                     */
                    publierLesInformations: function () {
                        var deferred = $q.defer();

                        $http.post(PUBLICATION_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Publier les moyens de l'espace
                     */
                    publierMoyens: function (exercice) {
                        var deferred = $q.defer();

                        $http.post(PUBLICATION_EXERCICES_URL + "/" + exercice.id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },


                    /**
                     * Publier les moyens de l'espace
                     */
                    publierActivite: function (activiteId) {
                        var deferred = $q.defer();

                        $http.post(PUBLICATION_ACTIVITE_URL + "/" + activiteId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getCountPublications: function (inscriptionEspaceId) {
                        var deferred = $q.defer();

                        $http.get(PUBLICATIONS_COUNT_URL + "/" + inscriptionEspaceId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }
                }

            }]);

})();