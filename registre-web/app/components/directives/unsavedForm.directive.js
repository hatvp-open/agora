/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre')

    .directive('unsavedForm', ['$mdDialog', '$state',
        function ($mdDialog, $state) {
            return {
                require: 'form',
                link: function ($scope, formElement, attrs, formCtrl) {

                    var navigateMessage = $mdDialog.confirm()
                        .title("Quitter la page ?")
                        .textContent("Etes-vous sûr(e) de vouloir quitter cette page ?")
                        .ariaLabel("Quitter la page ?")
                        .ok("Quitter")
                        .cancel("Annuler");

                    var form = formCtrl;

                    $scope.$on('$stateChangeStart', function (event, next, current) {

                        if (form.$dirty) {
                            event.preventDefault();

                            $mdDialog.show(navigateMessage).then(function () {

                                form.$dirty = false;
                                $state.go(next.name);

                            });
                        }
                    });
                }
            };
        }]);
