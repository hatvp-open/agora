/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre')

        .directive("scroll", ['$window', function ($window) {
            return function ($scope, element, attrs) {
                element.on("scroll", function () {
                    // detecte le scroll jusqu'en bas de page
                    if (element[0].scrollHeight - element[0].scrollTop === element[0].clientHeight) {
                        $scope.BasPage = true;
                        $scope.$apply();
                    }
                });
            };
        }]);
})();