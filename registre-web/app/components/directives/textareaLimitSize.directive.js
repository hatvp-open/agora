/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function() {
    'use strict';

    angular.module('hatvpRegistre')

    .value('version', '0.0.1')

    .directive('textareaLimitSize', function() {
    	  return {
    	    restrict: 'AE',
    	    link : function(scope, elem, attrs) {
                var limit = parseInt(attrs.textareaLimitSize);
                angular.element(elem).on("keypress", function(e) {
                    if (this.value.length == limit) e.preventDefault();
                });

    	    }
    	  }
    	});

})();