/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function() {
    'use strict';

    angular.module('hatvpRegistre')

    .value('version', '0.0.1')

    .directive('hatvpSelect', function() {
    	  return {
    	    restrict: 'AE',
    	    template: '<div layout="row"> <md-input-container> <label>Choisir</label> <md-select ng-model="selectedNomenclature" md-on-close="clearSearchTerm()" data-md-container-class="selectdemoSelectHeader" multiple="" ng-model-options="{trackBy: \'$value.id\'}"> <md-select-header class="demo-select-header"> <input ng-model="searchTerm" type="search" placeholder="Rechercher..." class="demo-header-searchbox md-text"> </md-select-header> <md-optgroup label="nomenclatures"> <md-option ng-value="nomenclature" ng-repeat="nomenclature in items | orderBy: property | filter:searchTerm">{{nomenclature[property]}}</md-option> </md-optgroup> </md-select> </md-input-container> <p ng-if="selectedNomenclature"> {{printSelected()}} </p> </div>',
    	    replace: true,
    	    scope: {
    	      items: '=',
    	      selectedNomenclature: '=',
    	      property: '='
    	    },
    	    controller: function($scope) {
    	      $scope.searchTerm;
    	      $scope.clearSearchTerm = function() {
    	        $scope.searchTerm = '';
    	      };
    	      
    	       $scope.printSelected = function printSelected() {
    	         
    	         // LA LISTE DE RECAP EST TRié PAR ORDRE ALPHABETIQUE
    	       var listeLibelle = this.selectedNomenclature.map(function(a) {return a[$scope.property];}).sort();
    	         
    	        var numberOfSelectedNomenclature = listeLibelle.length;
    	        if (numberOfSelectedNomenclature > 1) {
    	          var needsOxfordComma = numberOfSelectedNomenclature > 2;
    	          var lastSelectedConjunction = (needsOxfordComma ? ', ' : '') + ' et ';
    	          var lastNomenclature = lastSelectedConjunction +
    	          listeLibelle[listeLibelle.length - 1];
    	          return listeLibelle.slice(0, -1).join(', ') + lastNomenclature;
    	        }

    	        return listeLibelle.join('');
    	      };
    	    }
    	  };
    	});

})();