/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre')
        .directive('trimSpace', function () {
            return {
                restrict: 'AE',
                scope: {
                    trimSpace: '='
                },
                link: function (scope, ele, attrs, ctrl) {
                    scope.$watch('trimSpace', function (str) {
                        if (str) {
                            str = str.replace(/^ +/g, '');
                            scope.trimSpace = str.replace(/\s\s+/g, ' ');
                        }
                    });
                }
            };
        });
})();