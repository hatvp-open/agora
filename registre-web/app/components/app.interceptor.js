/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre')

        .factory('serverErrorInterceptor', ['$q', '$location', '$injector',
            function ($q, $location, $injector) {
                return {
                    'responseError': function (err) {
                        var state = $injector.get('$state');
                        var mdDialog = $injector.get('$mdDialog');

                        var status_type = String(err.status).charAt(0);

                        switch (status_type) {
                            case '4': // App Errors
                                switch (err.status) {
                                    case 401:// unauthorized
                                        state.current !== 'homeMessage' &&
                                        state.go('homeMessage', {entity: 'authError'}, {
                                            reload: false,
                                            location: false
                                        });
                                        break;

                                    case 403: // forbidden
                                        mdDialog.show(
                                            mdDialog.alert()
                                                .title("Droits insuffisants")
                                                .textContent("Vous n'avez pas les droits nécéssaires pour accéder à cette page.")
                                                .ariaLabel("Droits insuffisants")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        );
                                        break;

                                    case 400: // bad request
                                    case 404: // not found
                                        mdDialog.show(
                                            mdDialog.alert()
                                                .title("Erreur de données")
                                                .textContent(err.data.message)
                                                .ariaLabel("Erreur de données")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        );
                                        break;
                                    case 409: // conflict
                                        mdDialog.show(
                                            mdDialog.alert()
                                                .title("Conflit de données")
                                                .textContent(err.data.message)
                                                .ariaLabel("Conflit de données")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        );
                                        break;
                                    case 413: // Size overflow
                                    case 422: // entity error
                                        mdDialog.show(
                                            mdDialog.alert()
                                                .title("Données incorrectes")
                                                .textContent(err.data.message)
                                                .ariaLabel("Données incorrectes")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        );
                                        break;

                                    /* Erreur Optimistic Lock */
                                    case 412:
                                        mdDialog.show(
                                            mdDialog.alert()
                                                .title("Enregistrement impossible")
                                                .textContent("Les données actuelles ne sont plus à jour car elles ont été modifiées par un autre utilisateur." +
                                                    "Veuillez rafraîchir la page avant de resaisir vos modifications.")
                                                .ariaLabel("Enregistrement impossible")
                                                .ok('OK')
                                                .clickOutsideToClose(true)
                                        );
                                        break;

                                    /* Erreurs session */
                                    case 418:// csrf token error
                                    case 419:// session expired
                                        state.go('homeMessage', {
                                            entity: 'sessionError'
                                        }, {reload: true});
                                        break;

                                    // Autres erreurs non gérées
                                    default:
                                        state.go('homeMessage', {
                                            entity: 'appError',
                                            msg: err.status + ":" + err.statusText + " - " + err.data.message
                                        }, {reload: true, location: false});
                                        break;
                                }
                                break;
                            case '5': // Server Errors
                                state.go('homeMessage', {entity: 'technicalError'}, {
                                    reload: true,
                                    location: false
                                });
                                break;
                        }


                        return $q.reject(err);
                    }
                };
            }]);

})();