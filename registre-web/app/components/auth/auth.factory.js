/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function() {
    'use strict';

    angular.module('hatvpRegistre')

    .factory('csrfInterceptor', ['$log', '$cookies', '$injector',
        function ($log, $cookies, $injector) {

        return {
            'request': function (config) {
                config.headers['X-XSRF-TOKEN'] = $cookies.get('X-XSRF-TOKEN');
                return config;
            }
        };
    }]);

})();