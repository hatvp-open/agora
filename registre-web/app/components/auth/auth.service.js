/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function() {
    'use strict';

    angular.module('hatvpRegistre.service')

    .service('AuthService', ['$http', '$q', '$log', '$cookies', '$window','EnvConf', 'API',
        function ($http, $q, $log, $cookies, $window, EnvConf, API) {

            var LOGIN_URL = EnvConf.api + API.login;
            var LOGOUT_URL = EnvConf.api + API.logout;
            var INIT_URL = EnvConf.api + API.version;
            var INIT_SESSION_URL = EnvConf.api + API.checkSessions;
            var PROFILE_URL = EnvConf.api + API.declarant.root;



        var saveUser = function(user) {
            // Mapping date_naissance
            // var date_naissance = user.date_naissance.split('/');
            // user.date_naissance = new Date(date_naissance[2], date_naissance[1]-1, date_naissance[0]);

            //var date_naissance = user.date_naissance.split('/');
            //user.date_naissance = new Date(date_naissance[2], date_naissance[1] - 1, date_naissance[0]);

            // Session storage
            $window.sessionStorage.setItem('user', JSON.stringify(user));

            return user;
        };

        return {
            getUser: function() {
                return JSON.parse($window.sessionStorage.getItem('user'));
            },

            refresh: function() {
                var deferred = $q.defer();

                $http.get(PROFILE_URL).then(function(res) {
                    saveUser(res.data);
                    deferred.resolve(res);
                }, function(err) {
                    $window.sessionStorage.removeItem('user');
                    $window.sessionStorage.removeItem('corporate');
                    $window.sessionStorage.removeItem('tab');
                    deferred.reject(err);
                });

                return deferred.promise;
            },

            isConnected: function() {
                return !!$window.sessionStorage.getItem('user');
            },

            
            
            
            
            // Initialisation token CSRF
            init: function() {

                var deferred = $q.defer();

                $http.get(INIT_URL).then(function(res) {
                    deferred.resolve(res);
                }, function(err) {
                    deferred.reject(err);
                });

                return deferred.promise;

            },

            checkNumberSessions: function() {

                var deferred = $q.defer();

                $http.get(INIT_SESSION_URL).then(function(res) {
                    deferred.resolve(res);
                }, function(err) {
                    deferred.reject(err);
                });

                return deferred.promise;

            },

            authenticate: function(config) {
                var deferred = $q.defer();

                var contentTypeHeader = {'Content-Type': 'application/x-www-form-urlencoded'};
                var header = {headers: contentTypeHeader};

                $http.post(LOGIN_URL, "email=" + config.email + "&password=" + encodeURIComponent(config.password) +
                    "&_csrf=" + $cookies.get('X-XSRF-TOKEN'), header).then(
                    function(res) {
                    res.data = saveUser(res.data);

                    // récupération nouvelle valeur Token CSRF
                    $http.get(INIT_URL).then(function() {
                        deferred.resolve(res);
                    }, function(err) {
                        deferred.reject(err);
                    });
                }, function(response) {
                    deferred.reject(response);
                });

                return deferred.promise;
            },

            logout: function() {
                var deferred = $q.defer();

                $http.get(LOGOUT_URL).then(function(res) {
                    $log.info("Logout success");
                    deferred.resolve(res);
                }, function(err) {
                    $log.error("Logout error!");
                    deferred.reject(err);
                }).finally(function () {
                    $window.sessionStorage.removeItem('user');
                    $window.sessionStorage.removeItem('corporate');
                    $window.sessionStorage.removeItem('tab');
                    $window.sessionStorage.removeItem('premiereModifValidee');
                });

                return deferred.promise;
            },

        };

    }]);

})();