/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('SearchService', ['$http', '$q', 'EnvConf', 'API',
            function ($http, $q, EnvConf, API) {

                var CORPORATE_FIND_NID_URL = EnvConf.api + API.corporate.root + API.corporate.find_national_id;

                return {
                    /**
                     * Récupérer la liste des espaces organisations validés.
                     * @returns {Promise}
                     */
                    searchOrganisationByNationalId: function (organisationId) {
                        var deferred = $q.defer();

                        $http.get(CORPORATE_FIND_NID_URL + "/" + organisationId).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }

                }
            }]);

})();
