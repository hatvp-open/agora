/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
'use strict';

angular.module('hatvpRegistre.service')

    .constant('TABLE_OPTIONS', {
        filter: {},
        labels: {page: 'Page : ', rowsPerPage: 'Résultats par page : ', of: 'de'},
        order: 'id',
        limit: 10,
        limitOptions: [10, 20, 50, 100],
        page: 1
    })

    .constant('USER_ROLES', {
        administrator: 'ADMINISTRATEUR',
        publisher: 'PUBLICATEUR',
        contributor: 'CONTRIBUTEUR'
    })

    .constant('API', {
        version: "/version",
        checkSessions: "/sessions",
        contact: "/contact",
        declarant: {
            root: "/declarant",
            add_declarant: "/declarant/add",
            update_email: "/email",
            update_telephone: "/telephone",
            update_password: "/password",
            update_contact: "/contact",
            pieces: {
                root: "/pieces",
                download: "/download",
                add: "/add",
                all: "/all"
            }

        },
        account: {
            root: "/account",
            recover_email: "/account/recover/email",
            recover_password: "/account/recover/password/",
            validate_email: "/account/validate/email/",
            activate_compte: "/account/validate/"
        },
        corporate: {
            root: "/corporate",
            find_national_id: "/find/nid",
            find_espace_id: "/find/espace",
            update: "/update",
            unsubscribe: "/unsubscribe",
            upload: "/logo/upload",
            complement: "/complement",
            inscription_espace: {
                root: "/registration",
                favorite: "/favorite",
                pending: "/pending",
                inscriptions: "/inscriptions",
                admin: "/admin",
                nonadmin: "/nonadmin",
                validate: "/validate",
                reject: "/reject",
                roles: "/access",
                create: "/create",
                current: "/current",
                changements: {
                    root: "/mandant",
                    all: "/demandes",
                    demande: "/demande",
                    complement: "/complement"
                }
            },
            desinscription: {
                root:"/unsubscribe",
                downloadAttest: "/download_attest",
                uploadPiecesDesinscription: "/uploadPiecesDesinscription",
                desinscription_no_activite: "/desinscription_no_activite"
            },
            information_generale: {
                root: "/information",
                clients: "/client",
                membres: "/membres",
                association: "/association",
                verify: "/verify",
                batch: "/batch",
                collaborateurs: {
                    root: "/collaborateur",
                    activate: "/activate"
                },
                dirigeants: "/dirigeant"
            },
            type_orga: "/type/organisation",
            publication: {
                root: "/publication",
                last: "/last",
                new: "/new",
                exercice:"/exercice",
                activite:"/activite",
                hasPublished: "/hasPublished",
                count: "/count",
            },
            organisation: "/organisation",
            client_list_simple: "/client/list/simple"
        },
        declaration: {
            root: "/declaration",
            exercice: "/exercice",
            fiche: "/fiche",
            all: "/all",
            pdf:"/pdf",
            no_activite:"/nulle",
            statut_list: "/statut/list",
            qualif_objet: "/qualif/saveQualif",
            qualifier_objet: "/qualif/getQualif",
            update_qualif: "/qualif/updateQualif"

        },
        ia: {
            predict: "/predict"
        },
        login: "/login",
        logout: "/logout",
        limitation: {
            root: "/limitation",
            state: {
                get:"/activation/state",
                switch:"/activation/switch",
                limite:"/activation/limite"
            }
        }
    })

    .constant("TYPE_RECHERCHE", {
        AJOUTER_OU_REJOINDRE_ESPACE: 1,
        AJOUTER_CLIENT: 2,
        AJOUTER_ORGANISATION_PRO: 3,
        AJOUTER_UN_TIER: 4
    })

    .constant('HTML', {
        HOME: {
            HOME: "/home/home.html",
            LOGIN: "/home/login/login.html",
            SIGNUP: "/home/signup/signup.html",
            UNSUBSCRIBE: "/home/unsubscribe/unsubscribe.html",
            UNSUBSCRIBE_ATTEST: "/home/unsubscribe/unsubscribe_attest.html",
            UNSUBSCRIBE_NO_ACTIVITE: "/home/unsubscribe/unsubscribe_no_activite.html",
            ERROR: "/home/error/error.html",
            FORMEXEMPLE: "/home/emptyform/emptyform.html",
            MOYENEXEMPLE : "/home/emptyform/emptyMoyens/emptyMoyens.html",
            ACTIVITEMOYEN : "/home/emptyform/emptyActivites/emptyActivites.html"

        },
        COMPLEMENT: "/complement/complement.html",
        COMPLEMENT_DESINSCRIPTION: "/complement/complement_desinscription.html",
        QUESTIONNAIRE: "/contact/questionnaire.html",
        CONTACT: "/contact/contact.html",
        PROFIL: {
            root: "/profile/profile.html",
            password: "/profile/userdata/password/password.html",
            reset_password: "/profile/reset_password/reset_password.html",
            validate_password: "/profile/validate_password/validate_password.html",
            userdata: "/profile/userdata/userdata.html"
        },
        CONTRIBUTION: {
            HOME: "/contribution/contribution.html",
            HOME_RAPPORTS_RI: "/contribution/rapports/rapports.html",
            RAPPORTS_ACCUEIL: "/contribution/rapports/rapports_accueil.html",
            RAPPORTS_PDF: "/contribution/rapports/declarations/rapport_pdf.html",
            RAPPORTS_NO_ACTIVITE: "/contribution/rapports/declarations/rapport_no_activite.html",
            RAPPORTS_ERROR: "/contribution/rapports/declarations/error_saisie.html",
            RAPPORTS_MOYENS: "/contribution/rapports/moyens/moyens.html",
            RAPPORTS_DECLARATIONS: "/contribution/rapports/declarations/activites.html",
            RAPPORTS_DECLARATIONS_EDIT: "/contribution/rapports/declarations/saisie_activite.html",
            RAPPORTS_GUIDELINES: "/contribution/rapports/guidelines.html",
            CORPORATE_CLIENTS: "/contribution/clients/clients.html",
            CORPORATE_CLIENTS_AJOUT_LIEN_MEMBRES: "/contribution/clients/modal_ajout_lien_membres.html",
            CORPORATE_MODAL_CHANGE_ALL_CLIENTS_STATUS: "/contribution/clients/modal_change_all_clients_status.html",
            CORPORATE_CLIENTS_DESACTIVATION: "/contribution/clients/desactivation_client.html",
            CORPORATE_COLLABORATORS: "/contribution/collaborateurs/collaborateurs.html",
            CORPORATE_INFO: "/contribution/infos/infos.html",
            ASSO_APPARTENANCE: "/contribution/association/association.html",
            ASSO_APPARTENANCE_BATCH_IMPORT: "/contribution/association/associationBatchImport.html",
            ASSO_APPARTENANCE_BATCH_REPORT: "/contribution/association/associationBatchReport.html",
            DIRIGEANTS: "/contribution/dirigeants/dirigeants.html",
            DONNEES: "/contribution/donnees/donnees.html",
            REPRESENTATION_INTERET: "/contribution/activites/presentation_interet.html",
            INFORMATION: {
                CARDS: {
                    LOGO: "/contribution/infos/cards/logo.card.html",
                    PROFILE_ORGA: "/contribution/infos/cards/profile_orga.card.html",
                    LOCALISATION: "/contribution/infos/cards/localisation.card.html",
                    INFOS_CONTACT: "/contribution/infos/cards/infos_contact.card.html",
                    WEB: "/contribution/infos/cards/web.card.html",
                    EXERCICE_COMPTABLE: "/contribution/infos/cards/exercice_comptable.card.html"
                }
            }
        },
        PUBLICATION: {
            HOME: "/publication/publication.html",
            TAB_PUB: "/publication/profile_tab.html",
            ERROR: "/publication/error.html",
            HOME_RAPPORTS_RI: "/publication/rapports/rapports.html",
        },
        GESTION: {
            HOME: "/gestion/gestion.html",
            DECLARANTS: "/gestion/declarants/declarants.html",
            DEMANDES: "/gestion/demandes/demandes.html",
            DECLARANT_EDIT: "/gestion/declarants/declarant_edit.dialog.html",
            CHANGEMENTS: "/gestion/changements/changements.html",
            CHANGEMENT_EDIT: "/gestion/changements/saisie_demande.html",
            CHANGEMENT_COMPLEMENT: "/gestion/changements/complement_demande.html"
        },
        corporate_template: {
            menubar: "/corporate/menubar/menubar.html",
            home: "/corporate/home/home.html",
            add_corporate: {
                STEP0: "/corporate/add_corporate/add_corporate.html",
                STEP1: "/corporate/add_corporate/templates/add_corporate_step1.html",
                STEP2: "/corporate/add_corporate/templates/add_corporate_step2.html",
                STEP3: "/corporate/add_corporate/templates/add_corporate_step2-A.html",
                STEP4: "/corporate/add_corporate/templates/add_corporate_step2-B.html"
            }
        },
        SEARCH: "/search/search.tmpl.html"
    })
    .constant("STATUS_DEMANDE_TEXT", {
        AJOUT_OPERATIONNEL: "Ajout d'un nouveau contact opérationnel",
        REMPLACEMENT_OPERATIONNEL: "Remplacement d'un nouveau contact opérationnel",
        CHANGEMENT_REPRESENTANT: "Changement de représentant légal"
    })
;