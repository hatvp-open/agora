/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('LimiteService', ['$http', '$q', '$log', '$cookies', 'EnvConf', 'API',
            function ($http, $q, $log, $cookies, EnvConf, API) {

                var BO_STATE_URL = EnvConf.api + API.limitation.root + API.limitation.state.get;

                /**
                 * Récupérer le parametrage actuel
                 * @returns {Promise}
                 */
                return {
                    /**
                     * Get parametres.
                     *
                     * @returns {Promise}
                     */
                    getState: function () {

                        var deferred = $q.defer();

                        $http.get(BO_STATE_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }
                }
            }]);
})();