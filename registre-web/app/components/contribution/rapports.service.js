/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('RapportService', ['$http', '$q', '$log', '$cookies', '$state', 'EnvConf', 'API',
            function ($http, $q, $log, $cookies, $state, EnvConf, API) {

                var UPDATE_EXERCICE_URL = EnvConf.api + API.declaration.root + API.declaration.exercice;
                var EXERCICES_SIMPLE_URL = EnvConf.api + API.declaration.root + API.declaration.exercice + API.declaration.all;
                var EXERCICES_URL = EnvConf.api + API.declaration.root + API.declaration.exercice + API.declaration.fiche + API.declaration.all;
                var ACTIVITE = EnvConf.api + API.declaration.root + API.declaration.fiche;
                var ASK_IA = EnvConf.api + API.declaration.root + API.declaration.qualifier_objet;
                var UPDATE_QUALIF = EnvConf.api + API.declaration.root + API.declaration.update_qualif;
                var GET_CLIENTS = EnvConf.api + API.corporate.root + API.corporate.client_list_simple;
                var GET_RAPPORT_PDF = EnvConf.api + API.declaration.root + API.declaration.exercice + API.declaration.pdf;
                var GET_STATUT_ENUM = EnvConf.api + API.declaration.root + API.declaration.exercice + API.declaration.statut_list;
                var UPDATE_NO_ACTIVITE_EXERCICE_URL =  EnvConf.api + API.declaration.root + API.declaration.exercice + API.declaration.no_activite;
                var UPDATE_GUIDELINES = EnvConf.api + API.declarant.root;
                var EXERCICES_SIMPLE_FROM_ESPACE = EnvConf.api + API.declaration.root + API.declaration.exercice + "/espace";

                return {

                    getExercicesSimple: function () {
                        var deferred = $q.defer();

                        $http.get(EXERCICES_SIMPLE_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);


                            });

                        return deferred.promise;
                    },

                    saveMoyens: function (exercice) {
                        var deferred = $q.defer();

                        $http.put(UPDATE_EXERCICE_URL + "/" + exercice.id, exercice
                        ).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getExercices: function () {
                        var deferred = $q.defer();

                        $http.get(EXERCICES_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);

                            });

                        return deferred.promise;
                    },

                    deleteActivite: function (id) {
                        var deferred = $q.defer();

                        $http.delete(ACTIVITE + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getActivite: function (id) {
                        var deferred = $q.defer();

                        $http.get(ACTIVITE + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    saveActivite: function (activite) {
                        var deferred = $q.defer();

                        $http.post(ACTIVITE, activite
                        ).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    updateActivite: function (activite) {
                        var deferred = $q.defer();

                        $http.put(ACTIVITE + "/" + activite.id, activite
                        ).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getQualification: function (qualifObjet) {
                        var deferred = $q.defer();

                        $http.post(ASK_IA,qualifObjet
                        ).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    updateQualification: function (idFiche, qualifObjetId) {
                        var deferred = $q.defer();

                        $http.post(UPDATE_QUALIF + '/' + idFiche, qualifObjetId
                        ).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },



                    getClients: function () {
                        var deferred = $q.defer();

                        $http.get(GET_CLIENTS).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    downloadRapportPdf: function (id, withMoyen, checked) {
                        var deferred = $q.defer();

                        $http({
                            method: 'POST',
                            url: GET_RAPPORT_PDF + "/" + id + "/" + withMoyen,
                            data: checked,
                            responseType: 'arraybuffer',
                            transformResponse: pdfBufferToJson
                        }).success(function (data, status, headers) {
                            headers = headers();

                            var filename = headers['x-filename'];
                            var contentType = headers['content-type'];

                            var linkElement = document.createElement('a');
                            try {
                                var blob = new Blob([data], {type: contentType});
                                var url = window.URL.createObjectURL(blob);

                                linkElement.setAttribute('href', url);
                                linkElement.setAttribute("download", filename);

                                var clickEvent = new MouseEvent("click", {
                                    "view": window,
                                    "bubbles": true,
                                    "cancelable": false
                                });
                                linkElement.dispatchEvent(clickEvent);
                            } catch (ex) {
                                deferred.reject(ex);
                            }
                        }).error(function (err) {
                            deferred.reject(err);
                        });
                    },


                    getStatutEnum: function () {
                        var deferred = $q.defer();

                        $http.get(GET_STATUT_ENUM).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    noActivityToDeclare: function (exercice) {
                        var deferred = $q.defer();

                        $http.put(UPDATE_NO_ACTIVITE_EXERCICE_URL + "/" + exercice.id, exercice
                        ).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    hasReadGuidelines: function () {
                        var deferred = $q.defer();

                        $http.put(UPDATE_GUIDELINES+"/guidelines").then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    hasPublished: function (corporate) {
                        var deferred = $q.defer();

                        $http.get(EXERCICES_SIMPLE_FROM_ESPACE + "/" + corporate.id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }
                }
            }]);

    /**
     * conversion pour gestion des erreurs back-end
     * @param data
     * @param header
     * @param status
     * @returns {*}
     */
    var pdfBufferToJson = function byteArrayToJson(data, header, status) {
        var type = header("Content-Type");
        if (type.startsWith("application/pdf")) {
            return data;
        }
        var decoder = new TextDecoder("utf-8");
        var string = decoder.decode(data);
        var json = JSON.parse(string);
        return json;
    };

})();