/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('NomenclaturesService', ['$http', '$q', '$log', '$cookies', 'EnvConf', 'API',
            function ($http, $q, $log, $cookies, EnvConf, API) {

                var NOMENCLATURES_URL = EnvConf.api + "/nomenclature/declaration/all";

                return {

                    /** Retourne les nomenclatures utilisés par le service. */
                    getNomenclatures: function () {
                        var deferred = $q.defer();

                        $http.get(NOMENCLATURES_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;

                    },                  
                }
            }]);

})();