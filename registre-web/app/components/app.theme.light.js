/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 * 
 *  * Palette de couelurs ADEL suivant la charte graphique
 *
 */
'use strict';

angular.module('hatvpRegistre')

.config(['$mdThemingProvider', function ($mdThemingProvider) {

    var customPrimaryLight = {
        '50': '#288cfb',
        '100': '#0f7ffb',
        '200': '#0472ec',
        '300': '#0466d3',
        '400': '#035aba',
        '500': '#034ea1',
        '600': '#034288',
        '700': '#02366f',
        '800': '#022a56',
        '900': '#011d3d',
        'A100': '#419afb',
        'A200': '#5ba7fc',
        'A400': '#74b5fc',
        'A700': '#011124'
    };
    $mdThemingProvider.definePalette('customPrimaryLight', customPrimaryLight);

    var customAccentLight = {
        '50': '#66090c',
        '100': '#7e0b0f',
        '200': '#950d12',
        '300': '#ad0e15',
        '400': '#c51017',
        '500': '#dc121a',
        '600': '#ee343b',
        '700': '#f04b51',
        '800': '#f26368',
        '900': '#f47a7f',
        'A100': '#ee343b',
        'A200': '#ec1c24',
        'A400': '#dc121a',
        'A700': '#f69295'
    };
    $mdThemingProvider.definePalette('customAccentLight', customAccentLight);

    var customWarnLight = {
        '50': '#288cfb',
        '100': '#0f7ffb',
        '200': '#0472ec',
        '300': '#0466d3',
        '400': '#035aba',
        '500': '#034ea1',
        '600': '#034288',
        '700': '#02366f',
        '800': '#022a56',
        '900': '#011d3d',
        'A100': '#419afb',
        'A200': '#5ba7fc',
        'A400': '#74b5fc',
        'A700': '#011124'
    };
    $mdThemingProvider.definePalette('customWarnLight', customWarnLight);

    var customBackgroundLight = {
        '50': '#ffffff',
        '100': '#ffffff',
        '200': '#ffffff',
        '300': '#ffffff',
        '400': '#ffffff',
        '500': '#F2F2F2',
        '600': '#e5e5e5',
        '700': '#d8d8d8',
        '800': '#cccccc',
        '900': '#bfbfbf',
        'A100': '#ffffff',
        'A200': '#ffffff',
        'A400': '#ffffff',
        'A700': '#b2b2b2'
    };
    $mdThemingProvider.definePalette('customBackgroundLight', customBackgroundLight);


    $mdThemingProvider.theme('light')
        .primaryPalette('customPrimaryLight')
        .accentPalette('customAccentLight')
        .warnPalette('customWarnLight')
        .backgroundPalette('customBackgroundLight')

}]);