/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';

    angular.module('hatvpRegistre.service')

        .service('ContactService', ['$http', '$q', 'EnvConf', 'API', function ($http, $q, EnvConf, API) {

            var CONTACT_URL = EnvConf.api + API.contact;

            return {
                send: function (demande) {
                    var deferred = $q.defer();

                    $http.post(CONTACT_URL, demande).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                }
            }
        }]);
})();