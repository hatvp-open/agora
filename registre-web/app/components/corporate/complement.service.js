/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 * 
 * Service utilisé pour l'envoi de pièces complémentaires.
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('ComplementService', ['$http', '$q', 'EnvConf', 'API', function ($http, $q, EnvConf, API) {

            var self = this;

            var CORPORATE_URL = EnvConf.api + API.corporate.root;
            var COMPLEMENT_URL = CORPORATE_URL + API.corporate.complement;
            var COMPLEMENT_DESINSCRIPTION_URL = CORPORATE_URL + API.corporate.desinscription.root + API.corporate.desinscription.uploadPiecesDesinscription;

            self.uploadPieces = function (formData) {
                var deferred = $q.defer();
                $http.post(COMPLEMENT_URL, formData, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).then(function (response) {
                    deferred.resolve(response);
                }, function (response) {
                    deferred.reject(response);
                });
                return deferred.promise;
            };
            self.uploadPiecesDesinscription = function (formData) {
                var deferred = $q.defer();
                $http.post(COMPLEMENT_DESINSCRIPTION_URL, formData, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).then(function (response) {
                    deferred.resolve(response);
                }, function (response) {
                    deferred.reject(response);
                });
                return deferred.promise;
            }
        }]);
})();