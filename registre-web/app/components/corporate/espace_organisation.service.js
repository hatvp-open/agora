/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('EspaceOrganisationService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf, API) {

                var CORPORATE_URL = EnvConf.api + API.corporate.root;
                var CORPORATE_ATTESTE_CLIENT_URL = EnvConf.api + API.corporate.root + "/atteste/client/";
                var CORPORATE_ATTESTE_ASSOCIATION_URL = EnvConf.api + API.corporate.root + "/atteste/association/";
                var UNSUBSCRIBE_URL = EnvConf.api  + API.corporate.root+ API.corporate.unsubscribe;
                var CORPORATE_FIND_ESPACE_URL = CORPORATE_URL + API.corporate.find_espace_id;
                var CORPORATE_UPDATE_URL = CORPORATE_URL + API.corporate.update;
                var LOGO_UPLOAD_URL = CORPORATE_URL + API.corporate.upload;
                var LIST_ACTIVITE_URL = CORPORATE_URL + "/activite/list";
                var GET_ATTEST_PDF_URL = CORPORATE_URL + "/download/attest";
                var DESINSCRIPTION_NO_ACTIVITE = CORPORATE_URL + API.corporate.desinscription.root + API.corporate.desinscription.desinscription_no_activite;

                return {

                    /**
                     * Supprimer fichier logo pour espace organisation
                     * @param formData
                     * @returns {Promise}
                     */
                    deleteLogo: function () {
                        var deferred = $q.defer();

                        $http.delete(LOGO_UPLOAD_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise
                    },

                    /**
                     * Charger fichier logo pour espace organisation
                     * @param formData
                     * @returns {Promise}
                     */
                    uploadLogo: function (formData) {
                        var deferred = $q.defer();

                        $http.post(LOGO_UPLOAD_URL, formData, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        }).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise
                    },

                    /**
                     * Récupérer objet Espace Organisation courant
                     * @returns {Promise}
                     */
                    getCurrentEspaceOrganisation: function () {
                        var deferred = $q.defer();

                        $http.get(CORPORATE_FIND_ESPACE_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * MAJ Espace Organisation
                     * @param organisation
                     * @param card
                     * @returns {Promise}
                     */
                    setOrganisation: function (card, data) {
                        var deferred = $q.defer();

                        $http.put(CORPORATE_UPDATE_URL + "/" + card, data).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * MAJ de l'attestation sur l'honneur.
                     * if type === 1 & true existing clients are all desactivated
                     * if type === 1 & false nothing is done to any desactivated (manual activation if needed)
                     * @param state etat de l'attestation: TRUE / FALSE
                     * @param espaceId id de l'espace organisation.
                     * @param type association ou client.
                     */
                    toggleAttestation: function (state, type) {
                        var deferred = $q.defer();

                        var link = null, data = {};
                        if (type === 1) {
                            link = CORPORATE_ATTESTE_CLIENT_URL;
                            data = {nonDeclarationTiers: state};
                        } else if (type === 2) {
                            link = CORPORATE_ATTESTE_ASSOCIATION_URL;
                            data = {nonDeclarationOrgaAppartenance: state};
                        }

                        $http.put(link, data).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    getAllActivities: function () {
                        var deferred = $q.defer();

                        $http.get(LIST_ACTIVITE_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    unsubscribe: function (formData) {
                        var deferred = $q.defer();

                        $http.post(UNSUBSCRIBE_URL,formData,{
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        }).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    downloadAttestPdf: function () {
                        var deferred = $q.defer();

                        $http({
                            method:'POST',
                            url:GET_ATTEST_PDF_URL,
                            responseType: 'arraybuffer',
                        }).success(function (data, status,headers) {
                            headers = headers();

                            var filename = headers['x-filename'];
                            var contentType = headers['content-type'];

                            var linkElement = document.createElement('a');
                            try {
                                var blob = new Blob([data], {type: contentType});
                                var url = window.URL.createObjectURL(blob);

                                linkElement.setAttribute('href', url);
                                linkElement.setAttribute("download", filename);

                                var clickEvent = new MouseEvent("click", {
                                    "view": window,
                                    "bubbles": true,
                                    "cancelable": false
                                });
                                linkElement.dispatchEvent(clickEvent);
                            } catch (ex) {
                                deferred.reject(ex);
                            }
                        }).error(function (err) {
                            deferred.reject(err);
                        });
                    },
                    desinscriptionNoActivite: function () {
                        var deferred = $q.defer();

                        $http.get(DESINSCRIPTION_NO_ACTIVITE).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                }
            }]);


})();