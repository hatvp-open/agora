/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('InscriptionEspaceService', ['$http', '$q', '$log', '$window', 'EnvConf', 'API',
            function ($http, $q, $log, $window, EnvConf, API) {

                var CORPORATE_URL = EnvConf.api + API.corporate.root;
                var REGISTRATION_URL = CORPORATE_URL + API.corporate.inscription_espace.root;

                var CURRENT_REGISTRATION_URL = REGISTRATION_URL + API.corporate.inscription_espace.current;

                var FAVORITE_URL = REGISTRATION_URL + API.corporate.inscription_espace.favorite;
                var PENDING_REQUESTS_URL = REGISTRATION_URL + API.corporate.inscription_espace.pending;
                var INSCRIPTIONS_REGISTRATION_URL = REGISTRATION_URL + API.corporate.inscription_espace.inscriptions;
                var INSCRIPTIONS_REGISTRATION_ADMIN_URL = REGISTRATION_URL + API.corporate.inscription_espace.admin;
                var INSCRIPTIONS_REGISTRATION_NONADMIN_URL = REGISTRATION_URL + API.corporate.inscription_espace.nonadmin;

                var VALIDATE_URL = REGISTRATION_URL + API.corporate.inscription_espace.validate;
                var REJECT_URL = REGISTRATION_URL + API.corporate.inscription_espace.reject;
                var ROLES_URL = REGISTRATION_URL + API.corporate.inscription_espace.roles;

                var ALL_DEMANDES_CHANGEMENT_URL = CORPORATE_URL + API.corporate.inscription_espace.changements.root + API.corporate.inscription_espace.changements.all;
                var ADD_DEMANDE_CHANGEMENT_URL = CORPORATE_URL + API.corporate.inscription_espace.changements.root + API.corporate.inscription_espace.changements.demande;
                var COMPLEMENT_URL = CORPORATE_URL + API.corporate.inscription_espace.changements.root + API.corporate.inscription_espace.changements.complement;
                var HAS_PUBLISHED = EnvConf.api + API.corporate.root + API.corporate.publication.root + API.corporate.publication.hasPublished;


                return {
                    /**
                     * Raffraichir les données des corporates en cache navigateur
                     * @returns {Promise}
                     */
                    refresh: function () {

                        var deferred = $q.defer();
                        var self = this;

                        self.getAll().then(function (res) {
                            var corporates = res.data;

                            // redirection vers écran accueil corporate si existe
                            if (corporates && corporates.length > 0) {
                                // Associer l'espace corporate courrant à l'espace favori si pas encore défini
                                if (typeof self.getCurrentCorporate() === 'undefined' || self.getCurrentCorporate() === null) {
                                    var corporate = corporates.filter(function (e) {
                                        return e.favori;
                                    })[0];
                                    self.setCurrentCorporate(corporate).then(function () {
                                        self.hasPublished(corporate).then(function () {
                                            deferred.resolve(corporates);
                                        });
                                    });
                                }
                                else {
                                    self.setCurrentCorporate(corporates.filter(function (e) {
                                        return e.id === self.getCurrentCorporate().id;
                                    })[0]).then(function () {
                                        self.hasPublished(corporates.filter(function (e) {
                                            return e.id === self.getCurrentCorporate().id;
                                        })[0]).then(function () {
                                            deferred.resolve(corporates);
                                        });
                                    });
                                }
                            }
                            else
                                deferred.resolve(corporates);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    /**
                     * Récupérer la liste des espaces corporate non supprimés du déclarant
                     * @returns {Promise}
                     */
                    getAll: function () {
                        var deferred = $q.defer();

                        $http.get(REGISTRATION_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Définir l'espace corporate favori du déclarant
                     * @param id
                     * @returns {Promise}
                     */
                    setFavorite: function (id) {
                        var deferred = $q.defer();

                        $http.put(FAVORITE_URL + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * R&cupérer l'espace corporate de la session
                     */
                    getCurrentCorporate: function () {
                        return JSON.parse($window.sessionStorage.getItem('corporate'));
                    },
                    isCurrentCorporateHasPublished: function () {
                        return JSON.parse($window.sessionStorage.getItem('hasPublished'));
                    },
                    /**
                     * Définir l'espace corporate de la session
                     * @param corporate
                     */
                    setCurrentCorporate: function (corporate) {
                        var deferred = $q.defer();

                        $http.put(CURRENT_REGISTRATION_URL + "/" + corporate.id).then(
                            function (res) {
                                $window.sessionStorage.setItem('corporate', JSON.stringify(corporate));
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    hasPublished: function (corporate) {
                        var deferred = $q.defer();

                        $http.get(HAS_PUBLISHED + "/" + corporate.id).then(
                            function (res) {
                                $window.sessionStorage.setItem('hasPublished', JSON.stringify(res.data));
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * liste des demandes d'inscriptions en attente
                     * @returns {Promise}
                     */
                    getPendingRequests: function () {
                        var deferred = $q.defer();

                        $http.get(PENDING_REQUESTS_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * confirmer une demande d'inscription
                     * @param id
                     * @returns {Promise}
                     */
                    setValidRequest: function (id) {
                        var deferred = $q.defer();

                        $http.put(VALIDATE_URL + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * rejeter une demande d'inscription
                     * @param id
                     * @returns {Promise}
                     */
                    setRejectedRequest: function (id) {
                        var deferred = $q.defer();

                        $http.put(REJECT_URL + "/" + id).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Liste des inscriptions actives sur l'espace corporate actuel
                     * @returns {Promise}
                     */
                    getActiveRegistrations: function () {
                        var deferred = $q.defer();

                        $http.get(INSCRIPTIONS_REGISTRATION_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Liste des inscriptions actives n'ayant pas le rôle Admin (contact opérationnel) sur l'espace corporate actuel
                     * @returns {Promise}
                     */
                    getNonAdminActiveRegistrations: function () {
                        var deferred = $q.defer();

                        $http.get(INSCRIPTIONS_REGISTRATION_NONADMIN_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Liste des inscriptions actives ayant le rôle Admin (contact opérationnel) sur l'espace corporate actuel
                     * @returns {Promise}
                     */
                    getAdminActiveRegistrations: function () {
                        var deferred = $q.defer();

                        $http.get(INSCRIPTIONS_REGISTRATION_ADMIN_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Modifications des rôles du déclarant par rapport à l'espace corporate actuel
                     * @param id
                     * @param administrator
                     * @param publisher
                     * @param contributor
                     * @param locked
                     * @returns {Promise}
                     */
                    setAccessRoles: function (id, administrator, publisher, contributor, locked) {
                        var deferred = $q.defer();

                        $http.put(ROLES_URL + "/" + id, {
                            administrator: administrator,
                            publisher: publisher,
                            contributor: contributor,
                            locked: locked
                        }).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * liste des demandes de changement de mandant
                     * @returns {Promise}
                     */
                    getAllDemandesChangement: function () {
                        var deferred = $q.defer();

                        $http.get(ALL_DEMANDES_CHANGEMENT_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    /**
                     * Ajout d'une demande de changement de mandant
                     * @returns {Promise}
                     */
                    addNewDemandeChangement: function (formData) {
                        var deferred = $q.defer();
                        $http.post(ADD_DEMANDE_CHANGEMENT_URL, formData, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        }).then(function (response) {
                            deferred.resolve(response);
                        }, function (response) {
                            deferred.reject(response);
                        });
                        return deferred.promise;
                    },

                    uploadPieces: function (formData) {
                        var deferred = $q.defer();
                        $http.post(COMPLEMENT_URL, formData, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        }).then(function (response) {
                            deferred.resolve(response);
                        }, function (response) {
                            deferred.reject(response);
                        });
                        return deferred.promise;
                    },
                }

            }]);

})();