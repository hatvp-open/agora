/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
	'use strict';
	
	angular.module('hatvpRegistre.service')

    .service('CorporateAddService', ['$http', 'EnvConf', 'API', '$q', function ($http, EnvConf, API, $q) {

        var self = this;

        var CORPORATE_URL = EnvConf.api + API.corporate.root;
        var REGISTRATION_URL = EnvConf.api + API.corporate.root + API.corporate.inscription_espace.root;
        var ESPACE_CREATE_URL = EnvConf.api + API.corporate.root + API.corporate.inscription_espace.root + API.corporate.inscription_espace.create;

        self.getSelectedOrganizationData = function (organisation, success, failure) {
            $http.get(CORPORATE_URL + "/" + organisation.organizationId).then(function (response) {
                success(response)
            }, function (response) {
                failure(response)
            })
        };

        self.joinCreateSpace = function (organization) {
            var deferred = $q.defer();

            $http.post(REGISTRATION_URL, organization).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });

            return deferred.promise;
        };

        self.uploadMandat = function (formData) {
            var deferred = $q.defer();
            $http.post(ESPACE_CREATE_URL, formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });
            return deferred.promise;
        }

    }]);
})();