/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';

    angular.module('hatvpRegistre.service')

        .service('CorporateDataService', ['$http', '$q', 'EnvConf', 'API', function ($http, $q, EnvConf, API) {

            var CORPORATE_ADMIN_CLIENTS_URL = EnvConf.api + API.corporate.root
                + API.corporate.information_generale.root + API.corporate.information_generale.clients;

            var CORPORATE_INFO_COLLABORATEURS_URL = EnvConf.api + API.corporate.root
                + API.corporate.information_generale.root + API.corporate.information_generale.collaborateurs.root;
            var CORPORATE_INFO_COLLABORATEURS_ACTIVATE_URL = CORPORATE_INFO_COLLABORATEURS_URL
                + API.corporate.information_generale.collaborateurs.activate;

            var CORPORATE_INFO_DIRIGEANTS_URL = EnvConf.api + API.corporate.root
                + API.corporate.information_generale.root + API.corporate.information_generale.dirigeants;

            var CORPORATE_ASSOCIATION_URL = EnvConf.api + API.corporate.root
                + API.corporate.information_generale.root + API.corporate.information_generale.association;

            var ADD_CLIENT_URL = EnvConf.api + API.corporate.root + API.corporate.information_generale.root + API.corporate.information_generale.clients;
            var UPDATE_CLIENT_URL = EnvConf.api + API.corporate.root + API.corporate.information_generale.root + API.corporate.information_generale.clients + "/update";

            var ADD_ASSO_URL = EnvConf.api + API.corporate.root + API.corporate.information_generale.root + API.corporate.information_generale.association;
            var ADD_LIEN_MEMBRES = EnvConf.api + API.corporate.root + "/update/lienMembres/";
            var ADD_CLIENT_BATCH_URL = EnvConf.api + API.corporate.root + API.corporate.information_generale.root + API.corporate.information_generale.clients  + API.corporate.information_generale.membres ;
            var ADD_ASSO_BATCH_URL = EnvConf.api + API.corporate.root + API.corporate.information_generale.root + API.corporate.information_generale.association + API.corporate.information_generale.batch;

            var VERIFY_CLIENT_BATCH_URL = EnvConf.api + API.corporate.root + API.corporate.information_generale.root + API.corporate.information_generale.clients + API.corporate.information_generale.verify;
            var VERIFY_ASSO_BATCH_URL = EnvConf.api + API.corporate.root + API.corporate.information_generale.root + API.corporate.information_generale.association + API.corporate.information_generale.verify;

            return {

                /**
                 * Get all collaborators.
                 * @returns {Promise}
                 */
                getAllCollaborators: function () {
                    var deferred = $q.defer();

                    $http.get(CORPORATE_INFO_COLLABORATEURS_URL).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Add new collaborator.
                 * @param collaborator
                 * @returns {Promise}
                 */
                addNewCollaborator: function (collaborator) {
                    var deferred = $q.defer();

                    $http.post(CORPORATE_INFO_COLLABORATEURS_URL, collaborator).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                editCollaborator: function (collaborator) {
                    var deferred = $q.defer();

                    $http.put(CORPORATE_INFO_COLLABORATEURS_URL, collaborator).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Delete collaborator.
                 * @param collaborateurId
                 * @returns {Promise}
                 */
                deleteCollaborator: function (collaborateurId) {
                    var deferred = $q.defer();

                    $http.delete(CORPORATE_INFO_COLLABORATEURS_URL + "/" + collaborateurId).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Get all dirigeants.
                 * @returns {Promise}
                 */
                getAllDirigeants: function () {
                    var deferred = $q.defer();
                    $http.get(CORPORATE_INFO_DIRIGEANTS_URL).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Add new dirigeant.
                 * @param dirigeant
                 * @returns {Promise}
                 */
                addNewDirigeant: function (dirigeant) {
                    var deferred = $q.defer();

                    $http.post(CORPORATE_INFO_DIRIGEANTS_URL, dirigeant).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Remonter/descendre un dirigeant.
                 * @param dirigeantId
                 * @returns {Promise}
                 */
                sortDirigeantOrder: function (dirigeantId, up) {
                    var deferred = $q.defer();

                    $http.put(CORPORATE_INFO_DIRIGEANTS_URL + "/" + dirigeantId + (up === true ? '/up' : '/down')).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                editDirigeant: function (dirigeant) {
                    var deferred = $q.defer();

                    $http.put(CORPORATE_INFO_DIRIGEANTS_URL, dirigeant).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Delete dirigeant.
                 * @param dirigeantId
                 * @returns {Promise}
                 */
                deleteDirigeant: function (dirigeantId) {
                    var deferred = $q.defer();

                    $http.delete(CORPORATE_INFO_DIRIGEANTS_URL + "/" + dirigeantId).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Activer un collaborateur.
                 * @param collaborateurId
                 * @param actif
                 * @returns {Promise}
                 */
                activateCollaborator: function (collaborateurId, actif) {
                    var deferred = $q.defer();

                    $http.put(CORPORATE_INFO_COLLABORATEURS_ACTIVATE_URL + "/" + collaborateurId, actif).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Remonter/descendre un collaborateur.
                 * @param collaborateurId
                 * @param actif
                 * @returns {Promise}
                 */
                sortCollaboratorOrder: function (collaborateurId, up) {
                    var deferred = $q.defer();

                    $http.put(CORPORATE_INFO_COLLABORATEURS_URL + "/" + collaborateurId + (up === true ? '/up' : '/down')).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Get paginated clients
                 * @param pageNumber
                 * @param resultPerPage
                 * @returns {Promise}
                 */
                getPaginatedClients: function (pageNumber, resultPerPage) {
                    var deferred = $q.defer();

                    $http.get(CORPORATE_ADMIN_CLIENTS_URL + "/type/actif/page/" + pageNumber + "/" + resultPerPage).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });
                    return deferred.promise;
                },

                getPaginatedClientsAnciens: function (pageNumber, resultPerPage) {
                    var deferred = $q.defer();

                    $http.get(CORPORATE_ADMIN_CLIENTS_URL + "/type/ancien/page/" + pageNumber + "/" + resultPerPage).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });
                    return deferred.promise;
                },

                changeAllClientOldness: function (statut) {
                    var deferred = $q.defer();

                    $http.get(CORPORATE_ADMIN_CLIENTS_URL + "/ancien/all/" + statut).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });
                    return deferred.promise;
                },

                /**
                 * Add client.
                 * @param client
                 * @returns {Promise}
                 */
                addClient: function (client) {
                    var deferred = $q.defer();

                    $http.post(ADD_CLIENT_URL, client).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Delete client.
                 * @param clientId
                 * @returns {Promise}
                 */
                deleteClient: function (clientId) {
                    var deferred = $q.defer();

                    $http.delete(CORPORATE_ADMIN_CLIENTS_URL + "/" + clientId).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },
                addLienMembres: function (lien) {
                    var deferred = $q.defer();

                    $http.put(ADD_LIEN_MEMBRES, lien).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Update statut client 
                 * @param {*} client 
                 */
                updateClient: function (client) {
                    var deferred = $q.defer();

                    $http.post(UPDATE_CLIENT_URL, client).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Verify clients to add in batch mode
                 * @param clients, a list of clients
                 * @returns  report {Promise}
                 */
                verifyClientBatch: function (clients) {
                    var deferred = $q.defer();

                    $http.post(VERIFY_CLIENT_BATCH_URL, clients).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            if (err.statusText === "Found") {
                                deferred.resolve(err);
                            }
                            else {
                                console.error(err);
                                deferred.reject(err);
                            }
                        });

                    return deferred.promise;
                },

                /**
                 * Add client par lot.
                 * @param clients, a list of clients
                 * @returns {Promise}
                 */
                addClientBatch: function (clients) {
                    var deferred = $q.defer();

                    $http.post(ADD_CLIENT_BATCH_URL, clients).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });
                    return deferred.promise;
                },

                /**
                 * All associations with pagination feature: Get one single page of associations.
                 * @param pageNumber le numéro de page à afficher
                 * @param resultPerPage le nombre de résultats par page
                 */
                getPaginatedAssociations: function (pageNumber, resultPerPage) {
                    var deferred = $q.defer();

                    $http.get(CORPORATE_ASSOCIATION_URL + "/page/" + pageNumber + "/" + resultPerPage).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Add association.
                 * @param data
                 * @returns {Promise}
                 */
                addAssociation: function (asso) {
                    var deferred = $q.defer();

                    $http.post(ADD_ASSO_URL, asso).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Delete association.
                 * @param clientId
                 * @returns {Promise}
                 */
                deleteAssociation: function (clientId) {
                    var deferred = $q.defer();

                    $http.delete(CORPORATE_ASSOCIATION_URL + "/" + clientId).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                /**
                 * Verify association to add in batch mode
                 * @param clients, a list of clients
                 * @returns  report {Promise}
                 */
                verifyAssociationBatch: function (associations) {
                    var deferred = $q.defer();

                    $http.post(VERIFY_ASSO_BATCH_URL, associations).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            if (err.statusText === "Found") {
                                deferred.resolve(err);
                            }
                            else {
                                console.error(err);
                                deferred.reject(err);
                            }
                        });

                    return deferred.promise;
                },

                /**
                 * Add client par lot.
                 * @param clients, a list of clients
                 * @returns {Promise}
                 */
                addAssociationBatch: function (associations) {
                    var deferred = $q.defer();

                    $http.post(ADD_ASSO_BATCH_URL, associations).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });
                    return deferred.promise;
                }
            }
        }]);
})();