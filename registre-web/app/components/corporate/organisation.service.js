/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('OrganisationService', ['$http', '$q', 'EnvConf', 'API', function ($http, $q, EnvConf, API) {

            var CORPORATE_FIND_NID_URL = EnvConf.api + API.corporate.root + API.corporate.find_national_id;
            var TYPE_ORGA_URL = EnvConf.api + API.corporate.root + API.corporate.type_orga;

            return {
                loadAllOrganizationsByNationalId: function (organisationId) {
                    var deferred = $q.defer();

                    $http.get(CORPORATE_FIND_NID_URL + "/" + organisationId).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                getAllTypeOrganisation: function () {
                    var deferred = $q.defer();

                    $http.get(TYPE_ORGA_URL).then(
                        function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                }
            };


        }]);
})();