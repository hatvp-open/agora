/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */
(function () {
    'use strict';
    
    angular.module('hatvpRegistre.service')

        .service('ProfileService', ['$http', '$q', '$log', '$cookies', 'EnvConf', 'API',
            function ($http, $q, $log, $cookies, EnvConf, API) {

                var ADD_DECLARANT = EnvConf.api + API.declarant.add_declarant;
                var UPDATE_EMAIL = EnvConf.api + API.declarant.root + API.declarant.update_email;
                var UPDATE_TELEPHONE = EnvConf.api + API.declarant.root + API.declarant.update_telephone;
                var UPDATE_PASSWORD = EnvConf.api + API.declarant.root + API.declarant.update_password;
                var UPDATE_CONTACT = EnvConf.api + API.declarant.root + API.declarant.update_contact;
                var ACCOUNT_RECOVER_EMAIL = EnvConf.api + API.account.recover_email;
                var ACCOUNT_RECOVER_PASSWORD = EnvConf.api + API.account.recover_password;
                var ACCOUNT_VALIDATE_EMAIL = EnvConf.api + API.account.validate_email;
                var ACCOUNT_ACTIVATION = EnvConf.api + API.account.activate_compte;
                var DECLARANT_PIECES_URL = EnvConf.api + API.declarant.root + API.declarant.pieces.root + API.declarant.pieces.all;
                var DECLARANT_AJOUT_PIECE_URL = EnvConf.api + API.declarant.root + API.declarant.pieces.root + API.declarant.pieces.add;


                return {
                    register: function (formData) {
                        var deferred = $q.defer();

                        // Date mapping to server format (dd/mm/yyyy)
                        // var dateObj = profile.date_naissance;
                        // var month = ('0' + (dateObj.getMonth() + 1)).slice(-2);
                        // var date = ('0' + dateObj.getDate()).slice(-2);
                        // var year = dateObj.getFullYear();

                        // profile.date_naissance = date + "/" + month + "/" + year;

                        $http.post(ADD_DECLARANT, formData, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        }).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    update_email: function (email) {
                        var deferred = $q.defer();

                        $http.put(UPDATE_EMAIL, {
                            email: email
                        }).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    update_telephone: function (telephone) {
                        var deferred = $q.defer();

                        $http.put(UPDATE_TELEPHONE, {
                            telephone: telephone
                        }).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    update_password: function (oldPassword, password) {
                        var deferred = $q.defer();

                        $http.put(UPDATE_PASSWORD, {
                            prev_password: oldPassword,
                            password: password
                        }).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    update_contact: function (contacts) {
                        var deferred = $q.defer();

                        $http.put(UPDATE_CONTACT, contacts)
                            .then(function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },

                    init_password_1: function (email) {
                        var deferred = $q.defer();

                        $http.put(ACCOUNT_RECOVER_EMAIL, {
                            email: email
                        }).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    init_password_2: function (token, password) {
                        var deferred = $q.defer();

                        $http.put(ACCOUNT_RECOVER_PASSWORD + token, {
                            password: password
                        }).then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    validate_compte: function (token, password) {
                        var deferred = $q.defer();

                        $http.put(ACCOUNT_ACTIVATION + token, password)
                        .then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    validate_email: function (token, password) {
                        var deferred = $q.defer();

                        $http.put(ACCOUNT_VALIDATE_EMAIL + token, password)
                        .then(function (res) {
                            deferred.resolve(res);
                        }, function (err) {
                            deferred.reject(err);
                        });

                        return deferred.promise;
                    },

                    /** Retourne la liste des pièces versées lors de la demande de création de l'espace. */
                    getPieces: function () {
                        var deferred = $q.defer();

                        $http.get(DECLARANT_PIECES_URL).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    },
                    /** Ajout d'une pièce d'identité. */
                    addPiece: function (formData) {
                        var deferred = $q.defer();

                        $http.post(DECLARANT_AJOUT_PIECE_URL, formData, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        }).then(
                            function (res) {
                                deferred.resolve(res);
                            }, function (err) {
                                deferred.reject(err);
                            });

                        return deferred.promise;
                    }
                }
            }]);

})();