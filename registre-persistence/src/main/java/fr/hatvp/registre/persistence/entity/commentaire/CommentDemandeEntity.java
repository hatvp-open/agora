/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.commentaire;

import javax.persistence.*;

import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;

/**
 * Entité des commentaires des demande organisation.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "commentaire_demande")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "commentaire_demande_seq",
        allocationSize = 1)
public class CommentDemandeEntity
    extends AbstractCommentEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = 4072125690376730599L;

    /** Declarant commenté. */
    @ManyToOne
    @JoinColumn(nullable = false, name = "demande_organisation_id")
    private DemandeOrganisationEntity demandeOrganisationEntity;

    /**
     * Gets demande organisation entity.
     *
     * @return the demande organisation entity
     */
    public DemandeOrganisationEntity getDemandeOrganisationEntity()
    {
        return this.demandeOrganisationEntity;
    }

    /**
     * Sets demande organisation entity.
     *
     * @param demandeOrganisationEntity the demande organisation entity
     */
    public void setDemandeOrganisationEntity(
            final DemandeOrganisationEntity demandeOrganisationEntity)
    {
        this.demandeOrganisationEntity = demandeOrganisationEntity;
    }
}
