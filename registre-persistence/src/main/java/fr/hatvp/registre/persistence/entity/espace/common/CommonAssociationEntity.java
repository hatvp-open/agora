/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace.common;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

/**
 * Classe commune des associations.
 *
 * @version $Revision$ $Date$.
 */
@MappedSuperclass
public class CommonAssociationEntity
		extends AbstractCommonEntity {
	
	/** Clé de sérialisation. */
	private static final long serialVersionUID = -3681969910458643347L;
	
	/** Référence à l'Organisation. */
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organisation_id", nullable = false)
	private OrganisationEntity organisation;
	
	/** Référence à l'espace organisation. */
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "espace_organisation_id", nullable = false)
	private EspaceOrganisationEntity espaceOrganisation;
	
	/**
	 * Accesseur en lecture du champ <code>organisation</code>.
	 *
	 * @return le champ <code>organisation</code>.
	 */
	public OrganisationEntity getOrganisation() {
		return this.organisation;
	}
	
	/**
	 * Accesseur en écriture du champ <code>organisation</code>.
	 *
	 * @param organisation
	 * 		la valeur à écrire dans <code>organisation</code>.
	 */
	public void setOrganisation(final OrganisationEntity organisation) {
		this.organisation = organisation;
	}
	
	/**
	 * Accesseur en lecture du champ <code>espaceOrganisation</code>.
	 *
	 * @return le champ <code>espaceOrganisation</code>.
	 */
	public EspaceOrganisationEntity getEspaceOrganisation() {
		return this.espaceOrganisation;
	}
	
	/**
	 * Accesseur en écriture du champ <code>espaceOrganisation</code>.
	 *
	 * @param espaceOrganisation
	 * 		la valeur à écrire dans <code>espaceOrganisation</code>.
	 */
	public void setEspaceOrganisation(final EspaceOrganisationEntity espaceOrganisation) {
		this.espaceOrganisation = espaceOrganisation;
	}
}
