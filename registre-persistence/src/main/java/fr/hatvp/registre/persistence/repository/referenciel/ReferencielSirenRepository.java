/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.referenciel;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.persistence.entity.referentiel.ReferentielSireneEntity;

/**
 *
 * Repository JPA pour l'entité {@link ReferentielSireneEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface ReferencielSirenRepository
    extends JpaRepository<ReferentielSireneEntity, Long>
{
    /**
     * Récupérer entités par SIREN ou RNA.
     *
     * @param siren identifiant siren de l'organisation.
     * @param rna identifiant rna de l'organisation.
     * @return liste des organisations.
     */
    @Query("SELECT s FROM ReferentielSireneEntity s WHERE s.siren = ?1 OR s.rna = UPPER(?2)")
    List<ReferentielSireneEntity> findBySirenOrRna(String siren, String rna);

    /**
     * Récupérer entités par lot de SIREN ou RNA.
     *
     * @param sirenList identifiant siren de l'organisation.
     * @param rnaList identifiant rna de l'organisation.
     * @return liste des organisations.
     */
    @Query("SELECT s FROM ReferentielSireneEntity s WHERE s.siren in (?1) OR s.rna in (?2)")
    List<ReferentielSireneEntity> findBySirenOrRnaBatch(List<String> sirenList, List<String> rnaList);

    /**
     * Retourne le premier résultat correspondant au siren fourni.
     * A priori il n'existe qu'une ligne par SIREN car contrainte d'unicité sur la colonne siren.
     * @param siren
     * @return
     */
    ReferentielSireneEntity findFirstBySiren(String siren);

    /**
     * Supprime un établissement correspondant au SIREN et au NIC fournis.
     * @param siren
     * @param nic
     * @return
     */
    Long deleteBySirenAndNic(String siren, String nic);
}
