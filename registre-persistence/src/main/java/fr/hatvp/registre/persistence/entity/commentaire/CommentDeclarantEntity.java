/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.commentaire;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;

/**
 * 
 * Entité des commentaires des déclarants.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "commentaire_declarant")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "commentaire_declarant_seq",
        allocationSize = 1)
public class CommentDeclarantEntity
    extends AbstractCommentEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = 4072125690376730599L;

    /** Declarant commenté. */
    @ManyToOne
    @JoinColumn(nullable = false, name = "declarant_id")
    private DeclarantEntity declarantEntity;

    /**
     * Accesseur en lecture du champ <code>declarantEntity</code>.
     * @return le champ <code>declarantEntity</code>.
     */
    public DeclarantEntity getDeclarantEntity()
    {
        return this.declarantEntity;
    }

    /**
     * Accesseur en écriture du champ <code>declarantEntity</code>.
     * @param declarantEntity la valeur à écrire dans <code>declarantEntity</code>.
     */
    public void setDeclarantEntity(final DeclarantEntity declarantEntity)
    {
        this.declarantEntity = declarantEntity;
    }

}
