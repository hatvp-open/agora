/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.declarant;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;

/**
 * Table qui enregistre les déclarants (utilisateurs).
 * @version $Revision$ $Date${0xD}.
 */
@Entity
@Audited
@Table(name = "declarant")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "declarant_seq", allocationSize = 1)
public class DeclarantEntity extends AbstractCommonEntity {

	/**
	 * Clé de sérialisation.
	 */
	private static final long serialVersionUID = -5526781406532586434L;

	/**
	 * Civilité du déclarant.
	 */
	@Column(name = "civilite", nullable = false, columnDefinition = "TEXT")
	@Enumerated(EnumType.STRING)
	private CiviliteEnum civility;

	/**
	 * Nom du déclarant.
	 */
	@Column(name = "nom", nullable = false, columnDefinition = "TEXT")
	private String nom;

	/**
	 * Prenom du déclarant.
	 */
	@Column(name = "prenom", nullable = false, columnDefinition = "TEXT")
	private String prenom;

	/**
	 * Date de naissance.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_naissance", nullable = false)
	private Date birthDate;

	/**
	 * Email.
	 */
	@Column(name = "email", nullable = false, unique = true, columnDefinition = "TEXT")
	private String email;

	/**
	 * Telephone.
	 */
	@Column(name = "telephone", nullable = false, columnDefinition = "TEXT")
	private String telephone;

	/**
	 * Email temporaire, avant la validation d'une nouvelle adresse email principale.
	 */
	@Column(name = "email_temp", columnDefinition = "TEXT")
	private String emailTemp;

	/**
	 * Mot de passe.
	 */
	@Column(name = "password", nullable = false, columnDefinition = "TEXT")
	private String password;

	/**
	 * Clé de confirmation du compte.
	 */
	@Column(name = "key_email_confirmation", unique = true, nullable = false, columnDefinition = "TEXT")
	private String emailConfirmationCode;

	/**
	 * Date d'expiration de la clé d'activation.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_expiration_key_email", nullable = false)
	private Date emailCodeExpiryDate;

	/**
	 * Clef de génération.
	 */
	@Column(name = "key_recuperation_compte", columnDefinition = "TEXT")
	private String recoverConfirmationCode;

	/**
	 * Date d'expiration de la clé de récupération.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_expiration_key_recuperation")
	private Date recoverCodeExpiryDate;

	/**
	 * Date de dernière connexion.
	 */
	@NotAudited
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_derniere_connexion")
	private Date lastConnexionDate;

	/**
	 * État du compte.
	 */
	@Column(name = "compte_actif", nullable = false)
	private boolean activated = false;

    /**
     * Première connexion oui/non
     */
    @Column(name = "premiere_connexion", nullable = false)
    private boolean firstConn = true;

	/**
	 * Liste des contacts.
	 */
	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "declarant", fetch = FetchType.LAZY, orphanRemoval = true)
	private final Set<DeclarantContactEntity> contacts = new HashSet<>();

	/**
	 * Liste des inscription espace organisations.
	 */
	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "declarant", fetch = FetchType.LAZY)
	private Set<InscriptionEspaceEntity> inscriptionEspaces;

	/**
	 * Default Constructor
	 */
	public DeclarantEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public DeclarantEntity(Long id) {
		super();
		this.setId(id);
	}

	/**
	 * Accesseur en lecture du champ <code>civility</code>.
	 *
	 * @return le champ <code>civility</code>.
	 */
	public CiviliteEnum getCivility() {
		return this.civility;
	}

	/**
	 * Accesseur en écriture du champ <code>civility</code>.
	 *
	 * @param civility
	 *            la valeur à écrire dans <code>civility</code>.
	 */
	public void setCivility(final CiviliteEnum civility) {
		this.civility = civility;
	}

	/**
	 * Accesseur en lecture du champ <code>nom</code>.
	 *
	 * @return le champ <code>nom</code>.
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Accesseur en écriture du champ <code>nom</code>.
	 *
	 * @param nom
	 *            la valeur à écrire dans <code>nom</code>.
	 */
	public void setNom(final String nom) {
		this.nom = nom;
	}

	/**
	 * Accesseur en lecture du champ <code>prenom</code>.
	 *
	 * @return le champ <code>prenom</code>.
	 */
	public String getPrenom() {
		return this.prenom;
	}

	/**
	 * Accesseur en écriture du champ <code>prenom</code>.
	 *
	 * @param prenom
	 *            la valeur à écrire dans <code>prenom</code>.
	 */
	public void setPrenom(final String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Accesseur en lecture du champ <code>birthDate</code>.
	 *
	 * @return le champ <code>birthDate</code>.
	 */
	public Date getBirthDate() {
		return this.birthDate;
	}

	/**
	 * Accesseur en écriture du champ <code>birthDate</code>.
	 *
	 * @param birthDate
	 *            la valeur à écrire dans <code>birthDate</code>.
	 */
	public void setBirthDate(final Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * Accesseur en lecture du champ <code>email</code>.
	 *
	 * @return le champ <code>email</code>.
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Accesseur en écriture du champ <code>email</code>.
	 *
	 * @param email
	 *            la valeur à écrire dans <code>email</code>.
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * Accesseur en lecture du champ <code>emailTemp</code>.
	 *
	 * @return le champ <code>emailTemp</code>.
	 */
	public String getEmailTemp() {
		return this.emailTemp;
	}

	/**
	 * Accesseur en écriture du champ <code>emailTemp</code>.
	 *
	 * @param emailTemp
	 *            la valeur à écrire dans <code>emailTemp</code>.
	 */
	public void setEmailTemp(final String emailTemp) {
		this.emailTemp = emailTemp;
	}

	/**
	 * Accesseur en lecture du champ <code>password</code>.
	 *
	 * @return le champ <code>password</code>.
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Accesseur en écriture du champ <code>password</code>.
	 *
	 * @param password
	 *            la valeur à écrire dans <code>password</code>.
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Accesseur en lecture du champ <code>emailConfirmationCode</code>.
	 *
	 * @return le champ <code>emailConfirmationCode</code>.
	 */
	public String getEmailConfirmationCode() {
		return this.emailConfirmationCode;
	}

	/**
	 * Accesseur en écriture du champ <code>emailConfirmationCode</code>.
	 *
	 * @param emailConfirmationCode
	 *            la valeur à écrire dans <code>emailConfirmationCode</code>.
	 */
	public void setEmailConfirmationCode(final String emailConfirmationCode) {
		this.emailConfirmationCode = emailConfirmationCode;
	}

	/**
	 * Accesseur en lecture du champ <code>emailCodeExpiryDate</code>.
	 *
	 * @return le champ <code>emailCodeExpiryDate</code>.
	 */
	public Date getEmailCodeExpiryDate() {
		return this.emailCodeExpiryDate;
	}

	/**
	 * Accesseur en écriture du champ <code>emailCodeExpiryDate</code>.
	 *
	 * @param emailCodeExpiryDate
	 *            la valeur à écrire dans <code>emailCodeExpiryDate</code>.
	 */
	public void setEmailCodeExpiryDate(final Date emailCodeExpiryDate) {
		this.emailCodeExpiryDate = emailCodeExpiryDate;
	}

	/**
	 * Accesseur en lecture du champ <code>recoverConfirmationCode</code>.
	 *
	 * @return le champ <code>recoverConfirmationCode</code>.
	 */
	public String getRecoverConfirmationCode() {
		return this.recoverConfirmationCode;
	}

	/**
	 * Accesseur en écriture du champ <code>recoverConfirmationCode</code>.
	 *
	 * @param recoverConfirmationCode
	 *            la valeur à écrire dans <code>recoverConfirmationCode</code>.
	 */
	public void setRecoverConfirmationCode(final String recoverConfirmationCode) {
		this.recoverConfirmationCode = recoverConfirmationCode;
	}

	/**
	 * Accesseur en lecture du champ <code>recoverCodeExpiryDate</code>.
	 *
	 * @return le champ <code>recoverCodeExpiryDate</code>.
	 */
	public Date getRecoverCodeExpiryDate() {
		return this.recoverCodeExpiryDate;
	}

	/**
	 * Accesseur en écriture du champ <code>recoverCodeExpiryDate</code>.
	 *
	 * @param recoverCodeExpiryDate
	 *            la valeur à écrire dans <code>recoverCodeExpiryDate</code>.
	 */
	public void setRecoverCodeExpiryDate(final Date recoverCodeExpiryDate) {
		this.recoverCodeExpiryDate = recoverCodeExpiryDate;
	}

	/**
	 * Accesseur en lecture du champ <code>activated</code>.
	 *
	 * @return le champ <code>activated</code>.
	 */
	public boolean isActivated() {
		return this.activated;
	}

	/**
	 * Accesseur en écriture du champ <code>activated</code>.
	 *
	 * @param activated
	 *            la valeur à écrire dans <code>activated</code>.
	 */
	public void setActivated(final boolean activated) {
		this.activated = activated;
	}

	/**
	 * Accesseur en lecture du champ <code>contacts</code>.
	 *
	 * @return le champ <code>contacts</code>.
	 */
	public Set<DeclarantContactEntity> getContacts() {
		return this.contacts;
	}

	/**
	 * Accesseur en écriture du champ <code>contacts</code>.
	 *
	 * @param contacts
	 *            la valeur à écrire dans <code>contacts</code>.
	 */
	public void setContacts(final Set<DeclarantContactEntity> contacts) {
		this.contacts.clear();
		this.contacts.addAll(contacts);
	}

	/**
	 * Accesseur en lecture du champ <code>inscriptionEspaces</code>.
	 *
	 * @return le champ <code>inscriptionEspaces</code>.
	 */
	public Set<InscriptionEspaceEntity> getInscriptionEspaces() {
		return this.inscriptionEspaces;
	}

	/**
	 * Accesseur en écriture du champ <code>inscriptionEspaces</code>.
	 *
	 * @param inscriptionEspaces
	 *            la valeur à écrire dans <code>inscriptionEspaces</code>.
	 */
	public void setInscriptionEspaces(final Set<InscriptionEspaceEntity> inscriptionEspaces) {
		this.inscriptionEspaces = inscriptionEspaces;
	}

	/**
	 * Accesseur en lecture du champ <code>lastConnexionDate</code>.
	 *
	 * @return le champ <code>lastConnexionDate</code>.
	 */
	public Date getLastConnexionDate() {
		return this.lastConnexionDate;
	}

	/**
	 * Accesseur en écriture du champ <code>lastConnexionDate</code>.
	 *
	 * @param lastConnexionDate
	 *            la valeur à écrire dans <code>lastConnexionDate</code>.
	 */
	public void setLastConnexionDate(final Date lastConnexionDate) {
		this.lastConnexionDate = lastConnexionDate;
	}

	/**
	 * Gets telephone.
	 *
	 * @return the telephone
	 */
	public String getTelephone() {
		return this.telephone;
	}

	/**
	 * Sets telephone.
	 *
	 * @param telephone
	 *            the telephone
	 */
	public void setTelephone(final String telephone) {
		this.telephone = telephone;
	}

    public boolean getFirstConn() {
        return firstConn;
    }

    public void setFirstConn(boolean firstConn) {
        this.firstConn = firstConn;
    }
}
