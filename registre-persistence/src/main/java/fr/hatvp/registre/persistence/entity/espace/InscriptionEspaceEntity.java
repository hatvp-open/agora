

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace;

import java.util.Date;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;

/**
 *
 * Table qui enregistre les inscriptions à un espace organisation.
 * @version $Revision$ $Date${0xD}
 */
@Audited
@Entity
@Table(name = "inscription_espace")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "inscription_espace_seq", allocationSize = 1)
public class InscriptionEspaceEntity extends AbstractCommonEntity {

	/** Clé de sérialisation. */
	private static final long serialVersionUID = 1667821413951932101L;

	/**
	 * Default Constructor
	 */
	public InscriptionEspaceEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public InscriptionEspaceEntity(Long id) {
		super();
		this.setId(id);
	}

	/** Espace organisation favori. */
	@Column(name = "favori", nullable = false)
	private boolean favori = false;

	/** Espace organisation actif. */
	@Column(name = "actif", nullable = false)
	private boolean actif = false;

	/** Espace organisation vérouillé. */
	@Column(name = "verrouille", nullable = false)
	private boolean verrouille = false;

	/** Date de la demande. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_demande", nullable = false, updatable = false)
	private Date dateDemande = new Date();

	/** Date de validation de la demande. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_validation")
	private Date dateValidation;

	/** Date de suppression. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_suppression")
	private Date dateSuppression;

	/** Référence vers le déclarant. */
	@ManyToOne
	@JoinColumn(name = "declarant_id")
	private DeclarantEntity declarant;

	/** Référence vers l'espace organisation. */
	@ManyToOne
	@JoinColumn(name = "espace_organisation_id")
	private EspaceOrganisationEntity espaceOrganisation;

	/** Table roles. */
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "roles", joinColumns = @JoinColumn(name = "inscription_espace_id"))
	@Column(name = "role", columnDefinition = "TEXT")
	@Enumerated(EnumType.STRING)
	private Set<RoleEnumFrontOffice> rolesFront;

	/**
	 * Accesseur en lecture du champ <code>favori</code>.
	 *
	 * @return le champ <code>favori</code>.
	 */
	public boolean isFavori() {
		return this.favori;
	}

	/**
	 * Accesseur en écriture du champ <code>favori</code>.
	 *
	 * @param favori
	 *            la valeur à écrire dans <code>favori</code>.
	 */
	public void setFavori(final boolean favori) {
		this.favori = favori;
	}

	/**
	 * Accesseur en lecture du champ <code>actif</code>.
	 *
	 * @return le champ <code>actif</code>.
	 */
	public boolean isActif() {
		return this.actif;
	}

	/**
	 * Accesseur en écriture du champ <code>actif</code>.
	 *
	 * @param actif
	 *            la valeur à écrire dans <code>actif</code>.
	 */
	public void setActif(final boolean actif) {
		this.actif = actif;
	}

	/**
	 * Accesseur en lecture du champ <code>verrouille</code>.
	 *
	 * @return le champ <code>verrouille</code>.
	 */
	public boolean isVerrouille() {
		return this.verrouille;
	}

	/**
	 * Accesseur en écriture du champ <code>verrouille</code>.
	 *
	 * @param verrouille
	 *            la valeur à écrire dans <code>verrouille</code>.
	 */
	public void setVerrouille(final boolean verrouille) {
		this.verrouille = verrouille;
	}

	/**
	 * Accesseur en lecture du champ <code>dateDemande</code>.
	 *
	 * @return le champ <code>dateDemande</code>.
	 */
	public Date getDateDemande() {
		return this.dateDemande;
	}

	/**
	 * Accesseur en écriture du champ <code>dateDemande</code>.
	 *
	 * @param dateDemande
	 *            la valeur à écrire dans <code>dateDemande</code>.
	 */
	public void setDateDemande(final Date dateDemande) {
		this.dateDemande = dateDemande;
	}

	/**
	 * Accesseur en lecture du champ <code>dateValidation</code>.
	 *
	 * @return le champ <code>dateValidation</code>.
	 */
	public Date getDateValidation() {
		return this.dateValidation;
	}

	/**
	 * Accesseur en écriture du champ <code>dateValidation</code>.
	 *
	 * @param dateValidation
	 *            la valeur à écrire dans <code>dateValidation</code>.
	 */
	public void setDateValidation(final Date dateValidation) {
		this.dateValidation = dateValidation;
	}

	/**
	 * Accesseur en lecture du champ <code>dateSuppression</code>.
	 *
	 * @return le champ <code>dateSuppression</code>.
	 */
	public Date getDateSuppression() {
		return this.dateSuppression;
	}

	/**
	 * Accesseur en écriture du champ <code>dateSuppression</code>.
	 *
	 * @param dateSuppression
	 *            la valeur à écrire dans <code>dateSuppression</code>.
	 */
	public void setDateSuppression(final Date dateSuppression) {
		this.dateSuppression = dateSuppression;
	}

	/**
	 * Accesseur en lecture du champ <code>declarant</code>.
	 *
	 * @return le champ <code>declarant</code>.
	 */
	public DeclarantEntity getDeclarant() {
		return this.declarant;
	}

	/**
	 * Accesseur en écriture du champ <code>declarant</code>.
	 *
	 * @param declarant
	 *            la valeur à écrire dans <code>declarant</code>.
	 */
	public void setDeclarant(final DeclarantEntity declarant) {
		this.declarant = declarant;
	}

	/**
	 * Accesseur en lecture du champ <code>espaceOrganisation</code>.
	 *
	 * @return le champ <code>espaceOrganisation</code>.
	 */
	public EspaceOrganisationEntity getEspaceOrganisation() {
		return this.espaceOrganisation;
	}

	/**
	 * Accesseur en écriture du champ <code>espaceOrganisation</code>.
	 *
	 * @param espaceOrganisation
	 *            la valeur à écrire dans <code>espaceOrganisation</code>.
	 */
	public void setEspaceOrganisation(final EspaceOrganisationEntity espaceOrganisation) {
		this.espaceOrganisation = espaceOrganisation;
	}

	/**
	 * Accesseur en lecture du champ <code>roleEnumFrontOfficeSet</code>.
	 *
	 * @return le champ <code>roleEnumFrontOfficeSet</code>.
	 */
	public Set<RoleEnumFrontOffice> getRolesFront() {
		return this.rolesFront;
	}

	/**
	 * Accesseur en écriture du champ <code>roleEnumFrontOfficeSet</code>.
	 *
	 * @param roleEnumFrontOfficeSet
	 *            la valeur à écrire dans <code>roleEnumFrontOfficeSet</code>.
	 */
	public void setRolesFront(final Set<RoleEnumFrontOffice> roleEnumFrontOfficeSet) {
		this.rolesFront = roleEnumFrontOfficeSet;
	}
}
