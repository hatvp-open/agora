/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice.commentaire;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.commentaire.CommentDemandeEntity;

/**
 * 
 * repository des commentaires des demandes.
 * @version $Revision$ $Date${0xD}
 */
public interface CommentDemandeRepository
    extends JpaRepository<CommentDemandeEntity, Long>
{
    /**
     *
     * @param demandeOrganisationId id de la demande organisation;
     * @return liste de commentaire de la demande.
     */
    List<CommentDemandeEntity> findByDemandeOrganisationEntityId(Long demandeOrganisationId);
}
