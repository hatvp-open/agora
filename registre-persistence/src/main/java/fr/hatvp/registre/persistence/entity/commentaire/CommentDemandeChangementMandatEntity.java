/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.commentaire;

import javax.persistence.*;

import fr.hatvp.registre.persistence.entity.espace.DemandeChangementMandantEntity;

/**
 * Entité des commentaires des demande organisation.
 *
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "commentaire_demande_changement_mandat")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "commentaire_demande_changement_mandat_seq",
        allocationSize = 1)
public class CommentDemandeChangementMandatEntity
    extends AbstractCommentEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = 4072125690376730599L;

    /** Declarant commenté. */
    @ManyToOne
    @JoinColumn(nullable = false, name = "demande_id")
    private DemandeChangementMandantEntity demandeChangementMandantEntity;

    /**
     * Gets demande organisation entity.
     *
     * @return the demande organisation entity
     */
    public DemandeChangementMandantEntity getDemandeChangementMandantEntity()
    {
        return this.demandeChangementMandantEntity;
    }

    /**
     * Sets demande organisation entity.
     *
     * @param demandeOrganisationEntity the demande organisation entity
     */
    public void setDemandeChangementMandantEntity(
            final DemandeChangementMandantEntity demandeChangementMandantEntity)
    {
        this.demandeChangementMandantEntity = demandeChangementMandantEntity;
    }
}
