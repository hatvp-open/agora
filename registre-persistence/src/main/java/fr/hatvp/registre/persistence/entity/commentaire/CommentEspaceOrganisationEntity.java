/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.commentaire;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * 
 * Classe des commentaires des organisations.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "commentaire_espace_organisation")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "commentaire_espace_organisation_seq",
        allocationSize = 1)
public class CommentEspaceOrganisationEntity
    extends AbstractCommentEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = 8008486214636409008L;

    @ManyToOne
    @JoinColumn(name = "espace_organisation_id", nullable = false)
    private EspaceOrganisationEntity espaceOrganisationEntity;

    /**
     * Accesseur en lecture du champ <code>espaceOrganisationEntity</code>.
     * @return le champ <code>espaceOrganisationEntity</code>.
     */
    public EspaceOrganisationEntity getEspaceOrganisationEntity()
    {
        return this.espaceOrganisationEntity;
    }

    /**
     * Accesseur en écriture du champ <code>espaceOrganisationEntity</code>.
     * @param espaceOrganisationEntity la valeur à écrire dans
     * <code>espaceOrganisationEntity</code>.
     */
    public void setEspaceOrganisationEntity(final EspaceOrganisationEntity espaceOrganisationEntity)
    {
        this.espaceOrganisationEntity = espaceOrganisationEntity;
    }

}
