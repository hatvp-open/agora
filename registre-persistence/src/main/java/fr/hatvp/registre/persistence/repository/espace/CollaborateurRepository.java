/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import fr.hatvp.registre.persistence.entity.espace.CollaborateurEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 *
 * Repository JPA pour l'entité {@link CollaborateurEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface CollaborateurRepository
    extends JpaRepository<CollaborateurEntity, Long>
{
    /**
     * Retourne les données d'uun collaborateur selon son id s'il appartient à 
     * l'espace organisation dont l'identifiant est passé en paramètre.
     *  
     * @param id du collaborateur
     * @param espaceOrganisationId id de l'espace organisation où doit se trouver le collaborateur
     * @return
     */
    CollaborateurEntity findByIdAndEspaceOrganisationId(Long id, Long espaceOrganisationId);
    
    /**
     * Retourne l'ensemble des collaborateurs d'un espace organisation.
     * @param espaceOrganisationId id de l'espace organisation consulté.
     * @return
     */
    List<CollaborateurEntity> findByEspaceOrganisationId(Long espaceOrganisationId);

}
