/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.apiSirene;

import java.util.ArrayList;
import java.util.List;

public class ListeEtablissements {

    private List<Etablissement> etablissements;

    public ListeEtablissements() {
    	etablissements = new ArrayList<>();
    }

    public List<Etablissement> getEtablissements() {
        return etablissements;
    }

    public void setEtablissements(List<Etablissement> listDesEtablissements) {
        this.etablissements = listDesEtablissements;
    }
}
