/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.activite;

import fr.hatvp.registre.persistence.entity.activite.QualificationObjetEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QualificationObjetRepository
    extends JpaRepository<QualificationObjetEntity, Long> {

    /**
     * Récupérer la liste des objets qualifié en filtrant sur l'identifiant national
     *
     * @param keyword terme a filtrer
     * @param pageable
     * @return une page des objet qualifiés filtrées par statut
     */
    Page<QualificationObjetEntity> findByIdentifiantNationalIgnoreCaseContaining(String keyword, Pageable pageable);

    /**
     * Récupérer la liste des objets qualifié en filtrant sur l'id de la fiche
     *
     * @param keyword terme a filtrer
     * @param pageable
     * @return une page des objet qualifiés filtrées par statut
     */
    Page<QualificationObjetEntity> findByIdFicheIgnoreCaseContaining(String keyword, Pageable pageable);

    /**
     * Récupérer la liste des objets qualifié en filtrant sur l'objet
     *
     * @param keyword terme a filtrer
     * @param pageable
     * @return une page des objet qualifiés filtrées par statut
     */
    Page<QualificationObjetEntity> findByObjetIgnoreCaseContaining(String keyword, Pageable pageable);

    /**
     * Récupérer la liste des objets qualifié en filtrant sur l'objet
     *
     * @param idFiche id de la fiche
     * @return l'entité de la dernière qualif pour l'objet
     */
    QualificationObjetEntity findTopByIdFiche(String idFiche);

    /**
     * Récupérer la liste des objets qualifié en filtrant sur la dénomination ou le nom d'usage
     *
     * @param keyword terme a filtrer
     * @param pageable
     * @return une page des objet qualifiés filtrées par denomination ou nom d'usage
     */
    Page<QualificationObjetEntity> findByDenominationIgnoreCaseContainingOrNomUsageIgnoreCaseContaining(String denomination, String nomUsage, Pageable pageable);

    /**
     * Récupérer la liste des objets dont la qualification a échoué
     *
     * @return une page des objet dont la qualif a echoué
     */
    List<QualificationObjetEntity> findByCoefficiantConfianceIsNull();

}
