/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.apiSirene;

public class UniteLegale {
	
    private String denominationUniteLegale;
    private String nomUniteLegale;
    private String prenom1UniteLegale;
    private String nomUsageUniteLegale;
    private String categorieJuridiqueUniteLegale;
    private String nicSiegeUniteLegale;

    public String getNicSiegeUniteLegale() {
        return nicSiegeUniteLegale;
    }

    public void setNicSiegeUniteLegale(String nicSiegeUniteLegale) {
        this.nicSiegeUniteLegale = nicSiegeUniteLegale;
    }

    public String getDenominationUniteLegale() {
		return denominationUniteLegale;
	}
	public void setDenominationUniteLegale(String denominationUniteLegale) {
		this.denominationUniteLegale = denominationUniteLegale;
	}
	public String getNomUsageUniteLegale() {
		return nomUsageUniteLegale;
	}
	public void setNomUsageUniteLegale(String nomUsageUniteLegale) {
		this.nomUsageUniteLegale = nomUsageUniteLegale;
	}
	public String getNomUniteLegale() {
		return nomUniteLegale;
	}
	public void setNomUniteLegale(String nomUniteLegale) {
		this.nomUniteLegale = nomUniteLegale;
	}
	public String getCategorieJuridiqueUniteLegale() {
		return categorieJuridiqueUniteLegale;
	}
	public void setCategorieJuridiqueUniteLegale(String categorieJuridiqueUniteLegale) {
		this.categorieJuridiqueUniteLegale = categorieJuridiqueUniteLegale;
	}
	public String getPrenom1UniteLegale() {
		return prenom1UniteLegale;
	}
	public void setPrenom1UniteLegale(String prenom1UniteLegale) {
		this.prenom1UniteLegale = prenom1UniteLegale;
	}
	
    
    

}
