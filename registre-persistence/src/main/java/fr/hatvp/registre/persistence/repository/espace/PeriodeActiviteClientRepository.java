/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.espace.PeriodeActiviteClientEntity;

/**
 *
 * Repository JPA pour l'entité {@link PeriodeActiviteClientEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface PeriodeActiviteClientRepository extends JpaRepository<PeriodeActiviteClientEntity, Long> {

	/**
	 * Récupère la période d'activité en cours pour connaitre le statut du client
	 *
	 * @param clientId
	 * @return PeriodeActiviteClientEntity
	 */
	PeriodeActiviteClientEntity findFirstByClientIdOrderByDateAjoutDesc(Long clientId);
	
	PeriodeActiviteClientEntity findByClientIdAndStatutOrStatut(Long clientId, StatutPublicationEnum statut, StatutPublicationEnum statut2);
	
	List<PeriodeActiviteClientEntity> findByClientIdAndStatut(Long clientId, StatutPublicationEnum statut);

	@Query(value = "select per.* from periode_activite_client per " + 
			"inner join clients c ON c.id = per.client_id " + 
			"inner join espace_organisation eo ON eo.id = c.espace_organisation_id " + 
			"where eo.id = ?1 and per.date_desactivation is null", nativeQuery = true)
	List<PeriodeActiviteClientEntity> findPeriodeActiveByEspaceId(Long espaceOrganisationId);

}
