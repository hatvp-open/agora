/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.backoffice;

import java.util.Date;

import javax.persistence.*;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;

/**
 * Table qui enregistre les verrous de modification sur les demandes organisations.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "demande_organisation_lock_bo")
@SequenceGenerator(name = "id_sequence_generator",
        sequenceName = "demande_organisation_lock_bo_seq", allocationSize = 1)
public class DemandeOrganisationLockBoEntity
    extends AbstractCommonEntity
{

    /**
     * Clé de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Demande Organisation.
     */
    @ManyToOne(targetEntity = DemandeOrganisationEntity.class)
    @JoinColumn(name = "demande_organisation_id")
    private DemandeOrganisationEntity demandeOrganisation;

    /**
     * Utilisateur BO.
     */
    @ManyToOne(targetEntity = UtilisateurBoEntity.class)
    @JoinColumn(name = "utilisateur_bo_id")
    private UtilisateurBoEntity utilisateurBo;

    /**
     * Date lock.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "locked_time")
    private Date lockedTime = new Date();

    /**
     * Gets utilisateur bo.
     *
     * @return the utilisateur bo
     */
    public UtilisateurBoEntity getUtilisateurBo()
    {
        return this.utilisateurBo;
    }

    /**
     * Sets utilisateur bo.
     *
     * @param utilisateurBo the utilisateur bo
     */
    public void setUtilisateurBo(final UtilisateurBoEntity utilisateurBo)
    {
        this.utilisateurBo = utilisateurBo;
    }

    /**
     * Gets locked time.
     *
     * @return the locked time
     */
    public Date getLockedTime()
    {
        return this.lockedTime;
    }

    /**
     * Sets locked time.
     *
     * @param lockedTime the locked time
     */
    public void setLockedTime(final Date lockedTime)
    {
        this.lockedTime = lockedTime;
    }

    /**
     * Gets demande organisation.
     *
     * @return the demande organisation
     */
    public DemandeOrganisationEntity getDemandeOrganisation()
    {
        return this.demandeOrganisation;
    }

    /**
     * Sets demande organisation.
     *
     * @param demandeOrganisation the demande organisation
     */
    public void setDemandeOrganisation(final DemandeOrganisationEntity demandeOrganisation)
    {
        this.demandeOrganisation = demandeOrganisation;
    }
}
