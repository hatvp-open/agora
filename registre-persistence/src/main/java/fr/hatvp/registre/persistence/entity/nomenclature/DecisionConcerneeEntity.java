/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.nomenclature;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "nomenclature_decision_concernee")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "nomenclature_decision_concernee_seq", allocationSize = 1)
public class DecisionConcerneeEntity extends CommonNomenclatureEntity {

	private static final long serialVersionUID = 7453985889001262663L;

	/**
	 * Default Constructor
	 */
	public DecisionConcerneeEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public DecisionConcerneeEntity(Long id) {
		super();
		this.setId(id);
	}

}
