/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.commons.lists.RoleEnumFrontOffice;
import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;

/**
 *
 * Repository JPA pour l'entité {@link InscriptionEspaceEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface InscriptionEspaceRepository extends JpaRepository<InscriptionEspaceEntity, Long> {

	/**
	 * Renvoie l'inscription valide selon son identifiant si elle appartient à l'espace passé en paramètre.
	 *
	 * @param id
	 *            inscription sespace id.
	 * @param espaceOrganisationId
	 *            id de l'espace auquel doit appartenir l'inscription.
	 * @return l'inscription trouvée.
	 */
	InscriptionEspaceEntity findByDateSuppressionIsNullAndIdAndEspaceOrganisationId(Long id, Long espaceOrganisationId);

	/**
	 * Récupérer entités par Id déclarant.
	 * 
	 * @param declarantId
	 *            id du déclarant.
	 * @return liste des inscriptions.
	 */
	List<InscriptionEspaceEntity> findAllByDeclarantId(Long declarantId);

	/**
	 * Récupérer entités par Id Espace Organisation.
	 *
	 * @param espaceOrganisationId
	 *            id de l'espace organisation
	 * @return liste des inscriptions
	 */
	List<InscriptionEspaceEntity> findAllByEspaceOrganisationId(Long espaceOrganisationId);

	/**
	 * Récupérer entités par Id déclarant et Date suppression nulle.
	 * 
	 * @param declarantId
	 *            id du déclarant.
	 * @return liste des inscriptions courrantes du déclarant.
	 */
	List<InscriptionEspaceEntity> findAllByDateSuppressionIsNullAndDeclarantId(Long declarantId);

	/**
	 * Récupérer entités par Id organisation et date suppression nulle et date validation nulle.
	 * 
	 * @param espaceOrganisationId
	 *            id de l'espace organisation.
	 * @return liste des inscriptions en attente pour l'organisation.
	 */
	List<InscriptionEspaceEntity> findAllByDateSuppressionIsNullAndDateValidationIsNullAndEspaceOrganisationId(Long espaceOrganisationId);

	/**
	 * Récupérer entités par Id organisation et date suppression nulle et date validation non nulle.
	 *
	 * @param espaceOrganisationId
	 *            id de l'espace organisation.
	 * @return liste des inscriptions validées pour l'organisation.
	 */
	List<InscriptionEspaceEntity> findAllByActifTrueAndDateSuppressionIsNullAndEspaceOrganisationId(Long espaceOrganisationId);

	/**
	 * Récupérer entités par Id organisation et date suppression nulle et date validation non nulle. et possédant le rôle donné
	 *
	 * @param roleFront
	 * @param espaceOrganisationId
	 *            id de l'espace organisation.
	 * @param admins
	 *            Liste d'ids des déclarants administrateurs de l'espace
	 * @return liste des inscriptions validées, SANS le rôle donné, pour l'organisation.
	 */
	List<InscriptionEspaceEntity> findDistinctDeclarantIdByActifTrueAndDateSuppressionIsNullAndRolesFrontNotAndEspaceOrganisationIdAndDeclarantIdNotIn(
			RoleEnumFrontOffice roleFront, Long espaceOrganisationId, Collection<Long> admins);

	/**
	 * Récupérer entités par Id organisation et date suppression nulle et date validation non nulle. et possédant le rôle donné
	 *
	 * @param roleFront
	 * @param espaceOrganisationId
	 *            id de l'espace organisation.
	 * @return liste des inscriptions validées, avec le rôle donné, pour l'organisation.
	 */
	List<InscriptionEspaceEntity> findAllByActifTrueAndDateSuppressionIsNullAndRolesFrontAndEspaceOrganisationId(RoleEnumFrontOffice roleFront, Long espaceOrganisationId);

	/**
	 * Récupérer entité non supprimée logiquement par Id declarant et espace favori à true.
	 * 
	 * @param declarantId
	 *            id du déclarant.
	 * @return objet inscription favori du déclarant.
	 */
	Optional<InscriptionEspaceEntity> findByDateSuppressionIsNullAndFavoriTrueAndDeclarantId(Long declarantId);

	/**
	 * Récupérer entité par Id déclarant et Id organisation.
	 * 
	 * @param declarantId
	 *            id du déclarant.
	 * @param organizationId
	 *            id de l'organisation.
	 * @return objet inscription pour le déclarant sur l'organisation.
	 */
	InscriptionEspaceEntity findByDateSuppressionIsNullAndDeclarantIdAndEspaceOrganisationOrganisationId(Long declarantId, Long organizationId);

	/**
	 * Récupérer entité par Id déclarant et Id espace organisation.
	 *
	 * @param declarantId
	 *            id du déclarant.
	 * @param espaceOrganizationId
	 *            id de l'espace organisation.
	 * @return objet inscription pour le déclarant sur l'espace organisation.
	 */
	Optional<InscriptionEspaceEntity> findByDateSuppressionIsNullAndDeclarantIdAndEspaceOrganisationId(Long declarantId, Long espaceOrganizationId);

	/**
	 *
	 * @param id
	 *            inscription sespace id.
	 * @param declarantId
	 *            declarant id.
	 * @return l'inscription trouvé.
	 */
	InscriptionEspaceEntity findByDateSuppressionIsNullAndIdAndDeclarantId(Long id, Long declarantId);

	InscriptionEspaceEntity findFirstByIdOrderByIdDesc(Long inscriptionEspaceId);

	InscriptionEspaceEntity findFirstByEspaceOrganisationIdOrderByIdDesc(Long espaceId);
}
