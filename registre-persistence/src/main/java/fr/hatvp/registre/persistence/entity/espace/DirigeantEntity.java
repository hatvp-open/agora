

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.persistence.entity.espace.common.CommonDirigeantEntity;

/**
 * Entité des dirigeants d'une Organisation
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Audited
@Table(name = "dirigeant")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "dirigeant_seq",
        allocationSize = 1)
public class DirigeantEntity
    extends CommonDirigeantEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -2227996052230212180L;
}
