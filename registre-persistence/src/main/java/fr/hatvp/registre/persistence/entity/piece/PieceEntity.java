/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.piece;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;

/**
 * Super Entité d'enregistrement des fichiers.
 * @version $Revision$ $Date${0xD}
 */

@MappedSuperclass
public abstract class PieceEntity extends AbstractCommonEntity {
	/** Clé de sérialisation. */
	private static final long serialVersionUID = 5734556203196008801L;

	/** Date de versement du fichier. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_versement_piece", nullable = false, updatable = false)
	private Date dateVersementPiece = new Date();

	/** Ce champ permet de déterminer l'origine de la pièce versée. */
	@Column(name = "origine_versement", nullable = false, columnDefinition = "TEXT", updatable = false)
	@Enumerated(EnumType.STRING)
	private OrigineVersementEnum origineVersement = null;

	/** Nom du fichier versé. */
	@Column(name = "nom_fichier", columnDefinition = "TEXT", nullable = false, updatable = false)
	private String nomFichier;

	/** Le contenu du fichier versé. */
	@Column(name = "contenu_fichier", nullable = false, updatable = false)
	private byte[] contenuFichier;

	/** Type mime du fichier versé. */
	@Column(name = "media_type", columnDefinition = "TEXT", updatable = false)
	private String mediaType;

	/**
	 * Déclarant ayant fait le versement de la pièce.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "declarant_id", nullable = false, updatable = false)
	private DeclarantEntity declarant;

	/**
	 * Gets declarant.
	 *
	 * @return the declarant
	 */
	public DeclarantEntity getDeclarant() {
		return this.declarant;
	}

	/**
	 * Sets declarant.
	 *
	 * @param declarant
	 *            the declarant
	 */
	public void setDeclarant(final DeclarantEntity declarant) {
		this.declarant = declarant;
	}

	/**
	 * Gets dateVersementPiece
	 * 
	 * @return the dateVersementPiece
	 */
	public Date getDateVersementPiece() {
		return dateVersementPiece;
	}

	/**
	 * Sets dateVersementPiece
	 * 
	 * @param dateVersementPiece
	 *            the dateVersementPiece to set
	 */
	public void setDateVersementPiece(Date dateVersementPiece) {
		this.dateVersementPiece = dateVersementPiece;
	}

	/**
	 * Gets origineVersement
	 * 
	 * @return the origineVersement
	 */
	public OrigineVersementEnum getOrigineVersement() {
		return origineVersement;
	}

	/**
	 * Sets origineVersement
	 * 
	 * @param origineVersement
	 *            the origineVersement to set
	 */
	public void setOrigineVersement(OrigineVersementEnum origineVersement) {
		this.origineVersement = origineVersement;
	}

	/**
	 * Gets nomFichier
	 * 
	 * @return the nomFichier
	 */
	public String getNomFichier() {
		return nomFichier;
	}

	/**
	 * Sets nomFichier
	 * 
	 * @param nomFichier
	 *            the nomFichier to set
	 */
	public void setNomFichier(String nomFichier) {
		this.nomFichier = nomFichier;
	}

	/**
	 * Gets contenuFichier
	 * 
	 * @return the contenuFichier
	 */
	public byte[] getContenuFichier() {
		return contenuFichier;
	}

	/**
	 * Sets contenuFichier
	 * 
	 * @param contenuFichier
	 *            the contenuFichier to set
	 */
	public void setContenuFichier(byte[] contenuFichier) {
		this.contenuFichier = contenuFichier;
	}

	/**
	 * Gets mediaType
	 * 
	 * @return the mediaType
	 */
	public String getMediaType() {
		return mediaType;
	}

	/**
	 * Sets mediaType
	 * 
	 * @param mediaType
	 *            the mediaType to set
	 */
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
}
