/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.publication;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.persistence.entity.publication.PubClientEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

/**
 * Repository JPA pour l'entité {@link PublicationEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface PubClientRepository extends JpaRepository<PubClientEntity, Long> {
	
	List<PubClientEntity> findByEspaceOrganisationIdAndOrganisationIdAndDateAjoutOrderByIdAsc(Long espaceOrganisationId,Long organisationId, LocalDateTime dateAjout);
	
	/**
	 * Retourne la dernière ligne de publication d'un client
	 * @param espaceOrganisationId
	 * @param organisationId
	 * @return
	 */
	@Query(value = "select pub_client.* from pub_client " 
			+ " inner join publication on publication.id=pub_client.publication_id " 
			+  " where pub_client.espace_organisation_id= ?1 and pub_client.organisation_id= ?2 "
			+ " order by publication.date_creation desc LIMIT 1", nativeQuery = true)
	PubClientEntity getLastPubClientByEspaceOrganisationAndorganisation(Long espaceOrganisationId,Long organisationId);
	
	

}
