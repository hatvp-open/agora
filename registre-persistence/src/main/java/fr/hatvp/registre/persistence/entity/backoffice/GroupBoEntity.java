/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.backoffice;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

@Audited
@Entity
@Table(name = "group_bo")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "group_bo_seq", allocationSize = 1)
public class GroupBoEntity extends AbstractCommonEntity {

    private static final long serialVersionUID = 4300511457587509428L;

    @Column(name = "libelle",nullable = false,columnDefinition = "TEXT")
    private String libelle;

    @NotAudited
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "group_utilisateur_bo")
    private List<UtilisateurBoEntity> utilisateurBoEntity;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public List<UtilisateurBoEntity> getUtilisateurBoEntity() {
        return utilisateurBoEntity;
    }

    public void setUtilisateurBoEntity(List<UtilisateurBoEntity> utilisateurBoEntity) {
        this.utilisateurBoEntity = utilisateurBoEntity;
    }
}
