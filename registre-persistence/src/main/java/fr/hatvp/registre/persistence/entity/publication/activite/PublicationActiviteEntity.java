/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.publication.activite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.common.PublicationEntity;

/**
 * Entité des publications pour les activités.
 * @version $Revision$ $Date${0xD}
 */
@Audited
@Entity
@Table(name = "publication_activite")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "publication_activite_seq", allocationSize = 1)
public class PublicationActiviteEntity extends PublicationEntity {

	private static final long serialVersionUID = -6831013966853871371L;

	/** Référence à l'activite. */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "activite_id", nullable = false)
	private ActiviteRepresentationInteretEntity activite;

	/** Version de l'activite à la publication */
	@Column(name = "version_activite")
	private Integer versionActivite;

	public ActiviteRepresentationInteretEntity getActivite() {
		return activite;
	}

	public void setActivite(ActiviteRepresentationInteretEntity activite) {
		this.activite = activite;
	}

	public Integer getVersionActivite() {
		return versionActivite;
	}

	public void setVersionActivite(Integer versionActivite) {
		this.versionActivite = versionActivite;
	}
}