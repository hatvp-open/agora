/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import fr.hatvp.registre.commons.lists.ContextDemandeOrganisationEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;
import fr.hatvp.registre.commons.lists.backoffice.StatutDemandeOrganisationEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.backoffice.DemandeOrganisationLockBoEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;

/**
 * Entité des demandes d'ajout d'une nouvelle Organisation
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "demande_organisation")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "demande_organisation_seq",
        allocationSize = 1)
public class DemandeOrganisationEntity
    extends AbstractCommonEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -4121898513933036714L;

    /**
     * Context d'envoi du formulaire de contact.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "context", columnDefinition = "TEXT")
    private ContextDemandeOrganisationEnum context;

    /** Statut de traitement de la demande. */
    @Enumerated(EnumType.STRING)
    @Column(name = "statut", columnDefinition = "TEXT")
    private StatutDemandeOrganisationEnum statut = StatutDemandeOrganisationEnum.A_TRAITER;

    /** Date de création de la demande. */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_creation", nullable = false, updatable = false)
    private Date dateCreation = new Date();

    /**
     * Espace Organisation.
     */
    @ManyToOne
    @JoinColumn(name = "espace_organisation_id", updatable = false)
    private EspaceOrganisationEntity espaceOrganisation;

    /**
     * Déclarant envoyant la demande.
     */
    @ManyToOne
    @JoinColumn(name = "declarant_id", updatable = false, nullable = false)
    private DeclarantEntity declarant;

    /**
     * Objet Lock BO Sur la demande.
     */
    @OneToMany(mappedBy = "demandeOrganisation", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<DemandeOrganisationLockBoEntity> demandeOrganisationLockBoEntities;

    /**
     * Nom organisation.
     */
    @Column(name = "denomination", columnDefinition = "TEXT")
    private String denomination;

    /**
     * Type organisation.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "type", columnDefinition = "TEXT")
    private TypeOrganisationEnum type;

    /**
     * Identifiant organisation.
     */
    @Column(name = "identifiant", columnDefinition = "TEXT")
    private String identifiant;

    /**
     * Nom dirigeant de l'organisation.
     */
    @Column(name = "dirigeant", columnDefinition = "TEXT")
    private String dirigeant;

    /**
     * Adresse organisation.
     */
    @Column(name = "adresse", columnDefinition = "TEXT")
    private String adresse;

    /**
     * Code postal organisation.
     */
    @Column(name = "codePostal", columnDefinition = "TEXT")
    private String codePostal;

    /**
     * Ville organisation.
     */
    @Column(name = "ville", columnDefinition = "TEXT")
    private String ville;

    /**
     * Pays organisation.
     */
    @Column(name = "pays", columnDefinition = "TEXT")
    private String pays;

    /**
     * Commentaire additionnel demande.
     */
    @Column(name = "commentaire", length = 500, columnDefinition = "TEXT")
    private String commentaire;

    /**
     * Gets context.
     *
     * @return the context
     */
    public ContextDemandeOrganisationEnum getContext()
    {
        return this.context;
    }

    /**
     * Sets context.
     *
     * @param context the context
     */
    public void setContext(final ContextDemandeOrganisationEnum context)
    {
        this.context = context;
    }

    /**
     * Gets espace organisation.
     *
     * @return the espace organisation
     */
    public EspaceOrganisationEntity getEspaceOrganisation()
    {
        return this.espaceOrganisation;
    }

    /**
     * Sets espace organisation.
     *
     * @param espaceOrganisation the espace organisation
     */
    public void setEspaceOrganisation(final EspaceOrganisationEntity espaceOrganisation)
    {
        this.espaceOrganisation = espaceOrganisation;
    }

    /**
     * Gets declarant.
     *
     * @return the declarant
     */
    public DeclarantEntity getDeclarant()
    {
        return this.declarant;
    }

    /**
     * Sets declarant.
     *
     * @param declarant the declarant
     */
    public void setDeclarant(final DeclarantEntity declarant)
    {
        this.declarant = declarant;
    }

    /**
     * Gets denomination.
     *
     * @return the denomination
     */
    public String getDenomination()
    {
        return this.denomination;
    }

    /**
     * Sets denomination.
     *
     * @param denomination the denomination
     */
    public void setDenomination(final String denomination)
    {
        this.denomination = denomination;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public TypeOrganisationEnum getType()
    {
        return this.type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(final TypeOrganisationEnum type)
    {
        this.type = type;
    }

    /**
     * Gets identifiant.
     *
     * @return the identifiant
     */
    public String getIdentifiant()
    {
        return this.identifiant;
    }

    /**
     * Sets identifiant.
     *
     * @param identifiant the identifiant
     */
    public void setIdentifiant(final String identifiant)
    {
        this.identifiant = identifiant;
    }

    /**
     * Gets dirigeant.
     *
     * @return the dirigeant
     */
    public String getDirigeant()
    {
        return this.dirigeant;
    }

    /**
     * Sets dirigeant.
     *
     * @param dirigeant the dirigeant
     */
    public void setDirigeant(final String dirigeant)
    {
        this.dirigeant = dirigeant;
    }

    /**
     * Gets adresse.
     *
     * @return the adresse
     */
    public String getAdresse()
    {
        return this.adresse;
    }

    /**
     * Sets adresse.
     *
     * @param adresse the adresse
     */
    public void setAdresse(final String adresse)
    {
        this.adresse = adresse;
    }

    /**
     * Gets code postal.
     *
     * @return the code postal
     */
    public String getCodePostal()
    {
        return this.codePostal;
    }

    /**
     * Sets code postal.
     *
     * @param codePostal the code postal
     */
    public void setCodePostal(final String codePostal)
    {
        this.codePostal = codePostal;
    }

    /**
     * Gets ville.
     *
     * @return the ville
     */
    public String getVille()
    {
        return this.ville;
    }

    /**
     * Sets ville.
     *
     * @param ville the ville
     */
    public void setVille(final String ville)
    {
        this.ville = ville;
    }

    /**
     * Gets pays.
     *
     * @return the pays
     */
    public String getPays()
    {
        return this.pays;
    }

    /**
     * Sets pays.
     *
     * @param pays the pays
     */
    public void setPays(final String pays)
    {
        this.pays = pays;
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire()
    {
        return this.commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param commentaire the commentaire
     */
    public void setCommentaire(final String commentaire)
    {
        this.commentaire = commentaire;
    }

    /**
     * Gets statut.
     *
     * @return the statut
     */
    public StatutDemandeOrganisationEnum getStatut()
    {
        return this.statut;
    }

    /**
     * Sets statut.
     *
     * @param statut the statut
     */
    public void setStatut(final StatutDemandeOrganisationEnum statut)
    {
        this.statut = statut;
    }

    /**
     * Gets date creation.
     *
     * @return the date creation
     */
    public Date getDateCreation()
    {
        return this.dateCreation;
    }

    /**
     * Sets date creation.
     *
     * @param dateCreation the date creation
     */
    public void setDateCreation(final Date dateCreation)
    {
        this.dateCreation = dateCreation;
    }

    /**
     * Gets demande organisation lock bo entities.
     *
     * @return the demande organisation lock bo entities
     */
    public List<DemandeOrganisationLockBoEntity> getDemandeOrganisationLockBoEntities()
    {
        return this.demandeOrganisationLockBoEntities;
    }

    /**
     * Sets demande organisation lock bo entities.
     *
     * @param demandeOrganisationLockBoEntities the demande organisation lock bo entities
     */
    public void setDemandeOrganisationLockBoEntities(
            final List<DemandeOrganisationLockBoEntity> demandeOrganisationLockBoEntities)
    {
        this.demandeOrganisationLockBoEntities = demandeOrganisationLockBoEntities;
    }
}
