/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.declarant;

import javax.persistence.*;

import fr.hatvp.registre.commons.lists.CategoryTelephoneMail;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

/**
 * Table qui enregistre les infiormations de contact des déclarnats.
 * @version $Revision$ $Date${0xD}.
 */
@Entity
@Table(name = "declarant_contact")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "declarant_contact_seq",
        allocationSize = 1)
public class DeclarantContactEntity
    extends AbstractCommonEntity
{

    /**
     * Clé de sérialisation.
     */
    private static final long serialVersionUID = 840753242094381383L;

    /**
     * Adresse email de contact.
     */
    @Column(name = "email_contact")
    private String email;

    /**
     * Téléphone de contact.
     */
    @Column(name = "telephone_contact")
    private String telephone;

    /**
     * Catégorie du (téléphone & email) contact (PRO, PERSO).
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "categorie_contact")
    private CategoryTelephoneMail category;

    /**
     * Declarant.
     */
    @ManyToOne
    @JoinColumn(name = "declarant_id", nullable = false)
    private DeclarantEntity declarant;

    /**
     * Accesseur en lecture du champ <code>email</code>.
     *
     * @return le champ <code>email</code>.
     */
    public String getEmail()
    {
        return this.email;
    }

    /**
     * Accesseur en écriture du champ <code>email</code>.
     *
     * @param email la valeur à écrire dans <code>email</code>.
     */
    public void setEmail(final String email)
    {
        this.email = email;
    }

    /**
     * Accesseur en lecture du champ <code>telephone</code>.
     *
     * @return le champ <code>telephone</code>.
     */
    public String getTelephone()
    {
        return this.telephone;
    }

    /**
     * Accesseur en écriture du champ <code>telephone</code>.
     *
     * @param telephone la valeur à écrire dans <code>telephone</code>.
     */
    public void setTelephone(final String telephone)
    {
        this.telephone = telephone;
    }

    /**
     * Accesseur en lecture du champ <code>category</code>.
     *
     * @return le champ <code>category</code>.
     */
    public CategoryTelephoneMail getCategory()
    {
        return this.category;
    }

    /**
     * Accesseur en écriture du champ <code>category</code>.
     *
     * @param category la valeur à écrire dans <code>category</code>.
     */
    public void setCategory(final CategoryTelephoneMail category)
    {
        this.category = category;
    }

    /**
     * Accesseur en lecture du champ <code>declarant</code>.
     *
     * @return le champ <code>declarant</code>.
     */
    public DeclarantEntity getDeclarant()
    {
        return this.declarant;
    }

    /**
     * Accesseur en écriture du champ <code>declarant</code>.
     *
     * @param declarant la valeur à écrire dans <code>declarant</code>.
     */
    public void setDeclarant(final DeclarantEntity declarant)
    {
        this.declarant = declarant;
    }

    /**
     * Comparaison de deux objets.
     *
     * @param o objet à comparer.
     * @return état de la comparaison.
      */
    @Override
    public boolean equals(final Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        final DeclarantContactEntity that = (DeclarantContactEntity) o;

        if (!this.email.equals(that.email))
            return false;
        if (!this.telephone.equals(that.telephone))
            return false;
        return this.category == that.category;
    }

    /**
     *
     * @return un code d'identification unique de l'entitée.
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * (this.email != null ? result + this.email.hashCode() : 0);
        result = 31 * (this.telephone != null ? result + this.telephone.hashCode() : 0);
        result = 31 * result + this.category.hashCode();
        return result;
    }
}
