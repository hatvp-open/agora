/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.apiSirene;

public class AdresseEtablissement {
	


	private String codePostalEtablissement;
	private String libelleCommuneEtablissement;
	private String libelleCommuneEtrangerEtablissement;
    private String typeVoieEtablissement;
    private String numeroVoieEtablissement;
    private String libelleVoieEtablissement;
    private String indiceRepetitionEtablissement;
    private String libellePaysEtrangerEtablissement;

    public String getCodePostalEtablissement() {
		return codePostalEtablissement;
	}
	public void setCodePostalEtablissement(String codePostalEtablissement) {
		this.codePostalEtablissement = codePostalEtablissement;
	}
	public String getLibelleCommuneEtablissement() {
		return libelleCommuneEtablissement;
	}
	public void setLibelleCommuneEtablissement(String libelleCommuneEtablissement) {
		this.libelleCommuneEtablissement = libelleCommuneEtablissement;
	}
	public String getTypeVoieEtablissement() {
		return typeVoieEtablissement;
	}
	public void setTypeVoieEtablissement(String typeVoieEtablissement) {
		this.typeVoieEtablissement = typeVoieEtablissement;
	}
	public String getNumeroVoieEtablissement() {
		return numeroVoieEtablissement;
	}
	public void setNumeroVoieEtablissement(String numeroVoieEtablissement) {
		this.numeroVoieEtablissement = numeroVoieEtablissement;
	}
	public String getLibelleVoieEtablissement() {
		return libelleVoieEtablissement;
	}
	public void setLibelleVoieEtablissement(String libelleVoieEtablissement) {
		this.libelleVoieEtablissement = libelleVoieEtablissement;
	}
	public String getIndiceRepetitionEtablissement() {
		return indiceRepetitionEtablissement;
	}
	public void setIndiceRepetitionEtablissement(String indiceRepetitionEtablissement) {
		this.indiceRepetitionEtablissement = indiceRepetitionEtablissement;
	}
	public String toString(){
        return numeroVoieEtablissement+" "+(indiceRepetitionEtablissement != null ? indiceRepetitionEtablissement+" " : "")+typeVoieEtablissement+libelleVoieEtablissement;
    }
    public String getLibellePaysEtrangerEtablissement() {
        return libellePaysEtrangerEtablissement;
    }

    public void setLibellePaysEtrangerEtablissement(String libellePaysEtrangerEtablissement) {
        this.libellePaysEtrangerEtablissement = libellePaysEtrangerEtablissement;
    }
    public String getLibelleCommuneEtrangerEtablissement() {
        return libelleCommuneEtrangerEtablissement;
    }

    public void setLibelleCommuneEtrangerEtablissement(String libelleCommuneEtrangerEtablissement) {
        this.libelleCommuneEtrangerEtablissement = libelleCommuneEtrangerEtablissement;
    }
}
