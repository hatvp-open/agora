
/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.persistence.entity.espace.common.CommonAssociationEntity;

/**
 * Table qui enregistre les Organisations professionnelles
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Audited
@Table(name = "association_pro")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "association_pro_seq",
        allocationSize = 1)
public class AssociationEntity
    extends CommonAssociationEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -2025573844887206172L;
}
