/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.publication;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

/**
 * Entité utilisée pour enregistrer les données de la publication hors de la session
 * pour éviter d'utiliser trop de mémoire sur le serveur.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "publication_temp")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "publication_temp_seq",
    allocationSize = 1)
public class PublicationTempEntity
    extends AbstractCommonEntity {

    /** Clé de sérialisation. */
    private static final long serialVersionUID = 6154363703211655404L;
    
    /** L'identifiant de la session HTTP de l'utilisateur que va faire la publication. */
    @Column(name = "session_id", columnDefinition = "TEXT", nullable = false, unique = true)
    private String httpSessionId;

    /** Contenu des données à publier. */
    @Column(name = "json_content", columnDefinition = "TEXT", nullable = false)
    private String jsonContent;

    /**
     * Gets sessionId.
     *
     * @return the sessionId
     */
    public String getHttpSessionId()
    {
        return httpSessionId;
    }

    /**
     * Sets sessionId.
     *
     * @param sessionId the sessionId
     */
    public void setHttpSessionId(String httpSessionId)
    {
        this.httpSessionId = httpSessionId;
    }
    
    /**
     * Gets jsonContent.
     *
     * @return the jsonContent
     */
    public String getJsonContent()
    {
        return jsonContent;
    }

    /**
     * Sets jsonContent.
     *
     * @param jsonContent the jsonContent
     */
    public void setJsonContent(String jsonContent)
    {
        this.jsonContent = jsonContent;
    }

}
