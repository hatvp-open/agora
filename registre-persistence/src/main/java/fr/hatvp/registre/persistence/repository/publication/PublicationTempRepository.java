/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.publication;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.publication.PublicationTempEntity;

/**
 * Repository pour {@link PublicationTempEntity}
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationTempRepository
    extends JpaRepository<PublicationTempEntity, Long> {

    /**
     * Récupérer le contenu a publier lié à la session d'un utilisateur.
     *
     * @param espaceId id de l'espace organisation.
     * @return la liste de publications.
     */
    PublicationTempEntity findByHttpSessionId(String httpSessionId);
    
    /**
     * Supprime un contenu à publier enregistré pour la session d'un utilisateur.
     *  
     * @param httpSessionId
     * @return le nombre d'éléments supprimés (normalement 0 ou 1).
     */
    Long deleteByHttpSessionId(String httpSessionId);
    
}
