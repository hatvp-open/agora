/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

/**
 * Table qui enregistre les données d'une organisation.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Audited
@Table(name = "organisation")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "organisation_seq", allocationSize = 1)
public class OrganisationEntity extends AbstractCommonEntity {

	/** Clé de sérialisation. */
	private static final long serialVersionUID = -4993601607708005404L;

	/** Date de création. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_creation", nullable = false, updatable = false)
	private Date creationDate = new Date();

	/** Dénomination. */
	@Column(name = "denomination", nullable = false, columnDefinition = "TEXT")
	private String denomination;

	/** Nom d'usage. */
	@Column(name = "nom_usage_siren", columnDefinition = "TEXT")
	private String nomUsageSiren;

	/** Origine de l'identifiant national. */
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, name = "type_identifiant_national", columnDefinition = "TEXT")
	private TypeIdentifiantNationalEnum originNationalId;

	/**
	 * Identifiant national. Contrainte d'unicité dans column definition pour permettre plusieurs NULL.
	 */
	@Column(name = "siren", columnDefinition = "TEXT")
	private String siren;

	/**
	 * Duns identifiant. Contrainte d'unicité dans column definition pour permettre plusieurs NULL.
	 */
	@Column(name = "duns_number", columnDefinition = "TEXT")
	private String dunsNumber;

	/**
	 * Rna identifiant. Contrainte d'unicité dans column definition pour permettre plusieurs NULL.
	 */
	@Column(name = "rna", columnDefinition = "TEXT")
	private String rna;

	/**
	 * Identifiant HATVP. Contrainte d'unicité dans column definition pour permettre plusieurs NULL.
	 */
	@Column(name = "hatvp_number", columnDefinition = "TEXT")
	private String hatvpNumber;

	/** Adresse. */
	@Column(name = "adresse", columnDefinition = "TEXT")
	private String adresse;

	/** Code Postal. */
	@Column(name = "code_postal", columnDefinition = "TEXT")
	private String codePostal;

	/** Ville. */
	@Column(name = "ville", columnDefinition = "TEXT")
	private String ville;

	/** Pays. */
	@Column(name = "pays", columnDefinition = "TEXT")
	private String pays;

	/** Adresse du site Internet déclarée par l'association. Extrait de siteweb. */
	@Column(name = "siteweb", columnDefinition = "TEXT")
	private String siteWeb;

    @OneToOne(mappedBy = "organisationEntity")
    private SurveillanceEntity surveillanceEntity;
	/**
	 * Default Constructor
	 */
	public OrganisationEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public OrganisationEntity(Long id) {
		super();
		this.setId(id);
	}

	/**
	 * Accesseur en lecture du champ <code>creationDate</code>.
	 *
	 * @return le champ <code>creationDate</code>.
	 */
	public Date getCreationDate() {
		return this.creationDate;
	}

	/**
	 * Accesseur en écriture du champ <code>creationDate</code>.
	 *
	 * @param creationDate
	 *            la valeur à écrire dans <code>creationDate</code>.
	 */
	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Accesseur en lecture du champ <code>denomination</code>.
	 *
	 * @return le champ <code>denomination</code>.
	 */
	public String getDenomination() {
		return this.denomination;
	}

	/**
	 * Accesseur en écriture du champ <code>denomination</code>.
	 *
	 * @param denomination
	 *            la valeur à écrire dans <code>denomination</code>.
	 */
	public void setDenomination(final String denomination) {
		this.denomination = denomination;
	}

	/**
	 * Accesseur en lecture du champ <code>nomUsageSiren</code>.
	 *
	 * @return le champ <code>nomUsageSiren</code>.
	 */
	public String getNomUsageSiren() {
		return this.nomUsageSiren;
	}

	/**
	 * Accesseur en écriture du champ <code>nomUsageSiren</code>.
	 *
	 * @param nomUsage
	 *            la valeur à écrire dans <code>nomUsageSiren</code>.
	 */
	public void setNomUsageSiren(final String nomUsageSiren) {
		this.nomUsageSiren = nomUsageSiren;
	}

	/**
	 * Accesseur en lecture du champ <code>siren</code>.
	 *
	 * @return le champ <code>siren</code>.
	 */
	public String getSiren() {
		return this.siren;
	}

	/**
	 * Accesseur en écriture du champ <code>siren</code>.
	 *
	 * @param siren
	 *            la valeur à écrire dans <code>siren</code>.
	 */
	public void setSiren(final String siren) {
		this.siren = siren;
	}

	/**
	 * Accesseur en lecture du champ <code>dunsNumber</code>.
	 *
	 * @return le champ <code>dunsNumber</code>.
	 */
	public String getDunsNumber() {
		return this.dunsNumber;
	}

	/**
	 * Accesseur en écriture du champ <code>dunsNumber</code>.
	 *
	 * @param dunsNumber
	 *            la valeur à écrire dans <code>dunsNumber</code>.
	 */
	public void setDunsNumber(final String dunsNumber) {
		this.dunsNumber = dunsNumber;
	}

	/**
	 * Accesseur en lecture du champ <code>rna</code>.
	 *
	 * @return le champ <code>rna</code>.
	 */
	public String getRna() {
		return this.rna;
	}

	/**
	 * Accesseur en écriture du champ <code>rna</code>.
	 *
	 * @param rna
	 *            la valeur à écrire dans <code>rna</code>.
	 */
	public void setRna(final String rna) {
		this.rna = rna;
	}

	/**
	 * Accesseur en lecture du champ <code>hatvpNumber</code>.
	 *
	 * @return le champ <code>hatvpNumber</code>.
	 */
	public String getHatvpNumber() {
		return this.hatvpNumber;
	}

	/**
	 * Accesseur en écriture du champ <code>hatvpNumber</code>.
	 *
	 * @param hatvpNumber
	 *            la valeur à écrire dans <code>hatvpNumber</code>.
	 */
	public void setHatvpNumber(final String hatvpNumber) {
		this.hatvpNumber = hatvpNumber;
	}

	/**
	 * Accesseur en lecture du champ <code>adresse</code>.
	 *
	 * @return le champ <code>adresse</code>.
	 */
	public String getAdresse() {
		return this.adresse;
	}

	/**
	 * Accesseur en écriture du champ <code>adresse</code>.
	 *
	 * @param adresse
	 *            la valeur à écrire dans <code>adresse</code>.
	 */
	public void setAdresse(final String adresse) {
		this.adresse = adresse;
	}

	/**
	 * Accesseur en lecture du champ <code>codePostal</code>.
	 *
	 * @return le champ <code>codePostal</code>.
	 */
	public String getCodePostal() {
		return this.codePostal;
	}

	/**
	 * Accesseur en écriture du champ <code>codePostal</code>.
	 *
	 * @param codePostal
	 *            la valeur à écrire dans <code>codePostal</code>.
	 */
	public void setCodePostal(final String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * Accesseur en lecture du champ <code>ville</code>.
	 *
	 * @return le champ <code>ville</code>.
	 */
	public String getVille() {
		return this.ville;
	}

	/**
	 * Accesseur en écriture du champ <code>ville</code>.
	 *
	 * @param ville
	 *            la valeur à écrire dans <code>ville</code>.
	 */
	public void setVille(final String ville) {
		this.ville = ville;
	}

	/**
	 * Accesseur en lecture du champ <code>pays</code>.
	 *
	 * @return le champ <code>pays</code>.
	 */
	public String getPays() {
		return this.pays;
	}

	/**
	 * Accesseur en écriture du champ <code>pays</code>.
	 *
	 * @param pays
	 *            la valeur à écrire dans <code>pays</code>.
	 */
	public void setPays(final String pays) {
		this.pays = pays;
	}

	/**
	 * Accesseur en lecture du champ <code>originNationalId</code>.
	 *
	 * @return le champ <code>originNationalId</code>.
	 */
	public TypeIdentifiantNationalEnum getOriginNationalId() {
		return this.originNationalId;
	}

	/**
	 * Accesseur en écriture du champ <code>originNationalId</code>.
	 *
	 * @param originNationalId
	 *            la valeur à écrire dans <code>originNationalId</code>.
	 */
	public void setOriginNationalId(final TypeIdentifiantNationalEnum originNationalId) {
		this.originNationalId = originNationalId;
	}

	/**
	 * Accesseur en lecture du champ <code>siteweb</code>.
	 *
	 * @return le champ <code>siteweb</code>.
	 */
	public String getSiteWeb() {
		return this.siteWeb;
	}

	/**
	 * Accesseur en écriture du champ <code>siteweb</code>.
	 *
	 * @param siteweb
	 *            la valeur à écrire dans <code>siteweb</code>.
	 */
	public void setSiteWeb(final String siteweb) {
		this.siteWeb = siteweb;
	}

    public SurveillanceEntity getSurveillanceEntity() {
        return surveillanceEntity;
    }

    public void setSurveillanceEntity(SurveillanceEntity surveillanceEntity) {
        this.surveillanceEntity = surveillanceEntity;
    }

    public String computeIdNational() {
		switch (this.originNationalId) {
		case SIREN:
			return this.siren;
		case RNA:
			return this.rna;
		case HATVP:
			return this.hatvpNumber;
		default:
			return null;
		}
	}


}
