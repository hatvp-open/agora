/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.backoffice;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.commons.lists.backoffice.RoleEnumBackOffice;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;


@Audited
@Entity
@Table(name = "utilisateur_bo")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "utilisateur_bo_seq", allocationSize = 1)
public class UtilisateurBoEntity extends AbstractCommonEntity {

	/** Clé de sérialisation. */
	private static final long serialVersionUID = -6037615634070197109L;

	/** Nom de l'utilisateur. */
	@Column(name = "nom", nullable = false, columnDefinition = "TEXT")
	private String nom;

	/** prénom de l'utilisateur. */
	@Column(name = "prenom", nullable = false, columnDefinition = "TEXT")
	private String prenom;

	/** mot de passe de l'utilisateur. */
	@Column(name = "password", nullable = false, columnDefinition = "TEXT")
	private String password;

	/** email de l'utilisateur. */
	@Column(name = "email", nullable = false, unique = true, columnDefinition = "TEXT")
	private String email;

	/** etat du compte de l'utilisateur. */
	@Column(name = "compte_actif", nullable = false)
	private Boolean compteActive = false;

	/** Table roles. */
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "roles_bo", joinColumns = @JoinColumn(name = "utilisateur_bo_id"))
	@Column(name = "nom", columnDefinition = "TEXT")
	@Enumerated(EnumType.STRING)
	private Set<RoleEnumBackOffice> roleEnumBackOffices = new HashSet<>();

	/** Etat du compte */
	@Column(name = "compte_supprime", nullable = false)
	private Boolean compteSupprime = false;

	/**
	 * Accesseur en lecture du champ <code>nom</code>.
	 *
	 * @return le champ <code>nom</code>.
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Accesseur en écriture du champ <code>nom</code>.
	 *
	 * @param nom
	 *            la valeur à écrire dans <code>nom</code>.
	 */
	public void setNom(final String nom) {
		this.nom = nom;
	}

	/**
	 * Accesseur en lecture du champ <code>prenom</code>.
	 *
	 * @return le champ <code>prenom</code>.
	 */
	public String getPrenom() {
		return this.prenom;
	}

	/**
	 * Accesseur en écriture du champ <code>prenom</code>.
	 *
	 * @param prenom
	 *            la valeur à écrire dans <code>prenom</code>.
	 */
	public void setPrenom(final String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Accesseur en lecture du champ <code>password</code>.
	 *
	 * @return le champ <code>password</code>.
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Accesseur en écriture du champ <code>password</code>.
	 *
	 * @param password
	 *            la valeur à écrire dans <code>password</code>.
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Accesseur en lecture du champ <code>email</code>.
	 *
	 * @return le champ <code>email</code>.
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Accesseur en écriture du champ <code>email</code>.
	 *
	 * @param email
	 *            la valeur à écrire dans <code>email</code>.
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * Accesseur en lecture du champ <code>roleEnumBackOffices</code>.
	 *
	 * @return le champ <code>roleEnumBackOffices</code>.
	 */
	public Set<RoleEnumBackOffice> getRoleEnumBackOffices() {
		return this.roleEnumBackOffices;
	}

	/**
	 * Accesseur en écriture du champ <code>roleEnumBackOffices</code>.
	 *
	 * @param roleEnumBackOffices
	 *            la valeur à écrire dans <code>roleEnumBackOffices</code>.
	 */
	public void setRoleEnumBackOffices(final Set<RoleEnumBackOffice> roleEnumBackOffices) {
		this.roleEnumBackOffices = roleEnumBackOffices;
	}

	/**
	 * Accesseur en lecture du champ <code>compteSupprime</code>.
	 *
	 * @return le champ <code>compteSupprime</code>.
	 */
	public Boolean getCompteSupprime() {
		return this.compteSupprime;
	}

	/**
	 * Accesseur en écriture du champ <code>compteSupprime</code>.
	 *
	 * @param compteSupprime
	 *            la valeur à écrire dans <code>compteSupprime</code>.
	 */
	public void setCompteSupprime(final Boolean compteSupprime) {
		this.compteSupprime = compteSupprime;
	}

	/**
	 * Accesseur en lecture du champ <code>compteActive</code>.
	 *
	 * @return le champ <code>compteActive</code>.
	 */
	public Boolean getCompteActive() {
		return this.compteActive;
	}

	/**
	 * Accesseur en écriture du champ <code>compteActive</code>.
	 *
	 * @param compteActive
	 *            la valeur à écrire dans <code>compteActive</code>.
	 */
	public void setCompteActive(final Boolean compteActive) {
		this.compteActive = compteActive;
	}

}
