/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.referentiel;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * Référentiel RNA (Registre National des Associations).
 * Les associations sont identifiées en France selon un numéro RNA repris de l'ancien numéro Waldec.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "referentiel_rna")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "rna_seq", allocationSize = 1)
public class ReferentielRnaEntity
    extends AbstractReferentielEntity
{

    /** Identifiant de sérialisation. */
    private static final long serialVersionUID = 5886341229976995655L;

    /**
     * Identifiant de l'association dans le Registre National des Associations.
     * Format W + 9 chiffres.
     * Extrait de id.
     */
    @Column(name = "rna", nullable = false, unique = true, columnDefinition = "TEXT")
    private String rna;

    /**
     * SIREN de l'association si présente au répertoire SIRENE.
     * Extrait de siret.
     */
    @Column(name = "siren", columnDefinition = "TEXT")
    private String siren;

    /**
     * NIC de l'association si présente au répertoire SIRENE.
     * Extrait de siret.
     */
    @Column(name = "nic", columnDefinition = "TEXT")
    private String nic;

    /**
     * Date de création de l'association.
     * Extrait de date_creat.
     */
    @Column(name = "date_creat")
    private Date dateCreation;

    /**
     * Date de déclaration de l'association.
     * Extrait de date_decla.
     */
    @Column(name = "date_decla")
    private Date dateDeclaration;

    /**
     * Date de PublicationEntity au RNA.
     * Extrait de date_publi.
     */
    @Column(name = "date_publi")
    private Date datePublication;

    /**
     * Description de l'objet de l'association.
     * Extrait de objet.
     */
    @Column(name = "objet", columnDefinition = "TEXT")
    private String objet;

    /**
     * Adresse du site Internet déclarée par l'association.
     * Extrait de siteweb.
     */
    @Column(name = "siteweb", columnDefinition = "TEXT")
    private String siteWeb;

    /**
     * Date de dernière mise à jour issue du fichier RNA (utile pour le batch pour déterminer les
     * lignes à effectivement mettre à jour).
     * Extrait de maj_time.
     */
    @Column(name = "maj_time", columnDefinition = "TEXT")
    private String majTime;

    /**
     * Accesseur en lecture du champ <code>rna</code>.
     * @return le champ <code>rna</code>.
     */
    public String getRna()
    {
        return this.rna;
    }

    /**
     * Accesseur en écriture du champ <code>rna</code>.
     * @param rna la valeur à écrire dans <code>rna</code>.
     */
    public void setRna(final String rna)
    {
        this.rna = rna;
    }

    /**
     * Accesseur en lecture du champ <code>siren</code>.
     * @return le champ <code>siren</code>.
     */
    public String getSiren()
    {
        return this.siren;
    }

    /**
     * Accesseur en écriture du champ <code>siren</code>.
     * @param siren la valeur à écrire dans <code>siren</code>.
     */
    public void setSiren(final String siren)
    {
        this.siren = siren;
    }

    /**
     * Accesseur en lecture du champ <code>nic</code>.
     * @return le champ <code>nic</code>.
     */
    public String getNic()
    {
        return this.nic;
    }

    /**
     * Accesseur en écriture du champ <code>nic</code>.
     * @param nic la valeur à écrire dans <code>nic</code>.
     */
    public void setNic(final String nic)
    {
        this.nic = nic;
    }

    /**
     * Accesseur en lecture du champ <code>dateCreation</code>.
     * @return le champ <code>dateCreation</code>.
     */
    public Date getDateCreation()
    {
        return this.dateCreation;
    }

    /**
     * Accesseur en écriture du champ <code>dateCreation</code>.
     * @param dateCreation la valeur à écrire dans <code>dateCreation</code>.
     */
    public void setDateCreation(final Date dateCreation)
    {
        this.dateCreation = dateCreation;
    }

    /**
     * Accesseur en lecture du champ <code>dateDeclaration</code>.
     * @return le champ <code>dateDeclaration</code>.
     */
    public Date getDateDeclaration()
    {
        return this.dateDeclaration;
    }

    /**
     * Accesseur en écriture du champ <code>dateDeclaration</code>.
     * @param dateDeclaration la valeur à écrire dans <code>dateDeclaration</code>.
     */
    public void setDateDeclaration(final Date dateDeclaration)
    {
        this.dateDeclaration = dateDeclaration;
    }

    /**
     * Accesseur en lecture du champ <code>datePublication</code>.
     * @return le champ <code>datePublication</code>.
     */
    public Date getDatePublication()
    {
        return this.datePublication;
    }

    /**
     * Accesseur en écriture du champ <code>datePublication</code>.
     * @param datePublication la valeur à écrire dans <code>datePublication</code>.
     */
    public void setDatePublication(final Date datePublication)
    {
        this.datePublication = datePublication;
    }

    /**
     * Accesseur en lecture du champ <code>objet</code>.
     * @return le champ <code>objet</code>.
     */
    public String getObjet()
    {
        return this.objet;
    }

    /**
     * Accesseur en écriture du champ <code>objet</code>.
     * @param objet la valeur à écrire dans <code>objet</code>.
     */
    public void setObjet(final String objet)
    {
        this.objet = objet;
    }

    /**
     * Accesseur en lecture du champ <code>siteweb</code>.
     * @return le champ <code>siteweb</code>.
     */
    public String getSiteWeb()
    {
        return this.siteWeb;
    }

    /**
     * Accesseur en écriture du champ <code>siteweb</code>.
     * @param siteweb la valeur à écrire dans <code>siteweb</code>.
     */
    public void setSiteWeb(final String siteweb)
    {
        this.siteWeb = siteweb;
    }

    /**
     * Accesseur en lecture du champ <code>majTime</code>.
     * @return le champ <code>majTime</code>.
     */
    public String getMajTime()
    {
        return this.majTime;
    }

    /**
     * Accesseur en écriture du champ <code>majTime</code>.
     * @param majTime la valeur à écrire dans <code>majTime</code>.
     */
    public void setMajTime(final String majTime)
    {
        this.majTime = majTime;
    }

}
