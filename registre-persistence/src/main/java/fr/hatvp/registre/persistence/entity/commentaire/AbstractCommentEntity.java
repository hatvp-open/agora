/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.commentaire;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;

/**
 *
 * Classe commune de gestion des commentaires.
 * @version $Revision$ $Date${0xD}
 */
@MappedSuperclass
public abstract class AbstractCommentEntity extends AbstractCommonEntity {

	/** Clé de sérialisation. */
	private static final long serialVersionUID = 8117974280107917263L;

	/** Editeur du commentaire. */
	@ManyToOne
	@JoinColumn(nullable = false, name = "editeur_id")
	private UtilisateurBoEntity editeur;

	/** Modificateur du commentaire. */
	@ManyToOne
	@JoinColumn(nullable = true, name = "rectificateur_id")
	private UtilisateurBoEntity rectificateur;

	/** Message du commentaire. */
	@Column(name = "message", nullable = false, columnDefinition = "TEXT")
	private String message;

	/** Date de création du comentaire; */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_creation", nullable = false)
	private Date dateCreation = new Date();

	/** Date de création du comentaire; */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modification", nullable = true)
	private Date dateModification = new Date();

	/**
	 * @return the editeur
	 */
	public UtilisateurBoEntity getEditeur() {
		return editeur;
	}

	/**
	 * @param editeur
	 *            the editeur to set
	 */
	public void setEditeur(UtilisateurBoEntity editeur) {
		this.editeur = editeur;
	}

	/**
	 * @return the rectificateur
	 */
	public UtilisateurBoEntity getRectificateur() {
		return rectificateur;
	}

	/**
	 * @param rectificateur
	 *            the rectificateur to set
	 */
	public void setRectificateur(UtilisateurBoEntity rectificateur) {
		this.rectificateur = rectificateur;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the dateCreation
	 */
	public Date getDateCreation() {
		return dateCreation;
	}

	/**
	 * @param dateCreation
	 *            the dateCreation to set
	 */
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	/**
	 * @return the dateModification
	 */
	public Date getDateModification() {
		return dateModification;
	}

	/**
	 * @param dateModification
	 *            the dateModification to set
	 */
	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

}
