/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.publication;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.persistence.entity.espace.common.CommonDirigeantEntity;

/**
 * Entité des dirigants publiés.
 * @version $Revision$ $Date${0xD}
 */
@Audited
@Entity
@Table(name = "pub_dirigeant")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "pub_dirigeant_seq",
        allocationSize = 1)
public class PubDirigeantEntity
    extends CommonDirigeantEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = 2791697251666191405L;

    /** Référence vers la publicationEntity. */
    @ManyToOne
    private PublicationEntity publication;

    /**
     * Accesseur en lecture du champ <code>publicationEntity</code>.
     * @return le champ <code>publicationEntity</code>.
     */
    public PublicationEntity getPublication()
    {
        return this.publication;
    }

    /**
     * Accesseur en écriture du champ <code>publicationEntity</code>.
     * @param publicationEntity la valeur à écrire dans <code>publicationEntity</code>.
     */
    public void setPublication(final PublicationEntity publicationEntity)
    {
        this.publication = publicationEntity;
    }

}
