/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace.common;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * Entité commune des collaborateurs.
 *
 * @version $Revision$ $Date$.
 */
@MappedSuperclass
public class CommonCollaborateurEntity
    extends AbstractCommonEntity {
    /** Clé de sérialisation. */
    private static final long serialVersionUID = 601955312457444489L;

    /** Ordre hierarchique. */
    @Column(name = "sort_order")
    private Integer sortOrder = 0;

    /** Civilité. */
    @Audited
    @Column(name = "civilite", columnDefinition = "TEXT")
    @Enumerated(EnumType.STRING)
    private CiviliteEnum civility;

    /** Nom. */
    @Audited
    @Column(name = "nom", columnDefinition = "TEXT")
    private String nom;

    /** Prénom. */
    @Audited
    @Column(name = "prenom", columnDefinition = "TEXT")
    private String prenom;

    /** Email. */
    @Audited
    @Column(name = "email", columnDefinition = "TEXT")
    private String email;

    /** Fonctions exercées. */
    @Audited
    @Column(name = "fonction", columnDefinition = "TEXT")
    private String fonction;

    /** Date d'enregistrement. */
    @Column(name = "date_enregistrement", nullable = false, updatable = false)
    private LocalDateTime enregistrementDate = LocalDateTime.now();

    /** Référence vers l'espace organisation. */
    @ManyToOne(targetEntity = EspaceOrganisationEntity.class)
    @JoinColumn(name = "espace_organisation_id", updatable = false)
    private EspaceOrganisationEntity espaceOrganisation;

    /** Référence vers le dirigeant */
    @OneToOne(targetEntity = DeclarantEntity.class)
    @JoinColumn(name = "declarant_id")
    private DeclarantEntity declarant;

    /** État d'un collaborateur inscrit. */
    @Audited
    @Column(name = "actif", nullable = false)
    private Boolean actif = Boolean.TRUE;

    /** Date d'enregistrement. */
    @Column(name = "date_desactivation")
    private LocalDateTime dateDesactivation;

    /**
     * Gets civilite.
     *
     * @return the civilite
     */
    public CiviliteEnum getCivility() {
        return this.civility;
    }

    /**
     * Sets civilite.
     *
     * @param civilite the civilite
     */
    public void setCivility(final CiviliteEnum civilite) {
        this.civility = civilite;
    }

    /**
     * Gets nom.
     *
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Sets nom.
     *
     * @param nom the nom
     */
    public void setNom(final String nom) {
        this.nom = nom;
    }

    /**
     * Gets prenom.
     *
     * @return the prenom
     */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * Sets prenom.
     *
     * @param prenom the prenom
     */
    public void setPrenom(final String prenom) {
        this.prenom = prenom;
    }

    /**
     * Gets enregistrement date.
     *
     * @return the enregistrement date
     */
    public LocalDateTime getEnregistrementDate() {
        return this.enregistrementDate;
    }

    /**
     * Sets enregistrement date.
     *
     * @param enregistrementDate the enregistrement date
     */
    public void setEnregistrementDate(final LocalDateTime enregistrementDate) {
        this.enregistrementDate = enregistrementDate;
    }

    /**
     * Gets espace organisation entity.
     *
     * @return the espace organisation entity
     */
    public EspaceOrganisationEntity getEspaceOrganisation() {
        return this.espaceOrganisation;
    }

    /**
     * Sets espace organisation entity.
     *
     * @param espaceOrganisationEntity the espace organisation entity
     */
    public void setEspaceOrganisation(final EspaceOrganisationEntity espaceOrganisationEntity) {
        this.espaceOrganisation = espaceOrganisationEntity;
    }

    /**
     * Gets declarant entity.
     *
     * @return the declarant entity
     */
    public DeclarantEntity getDeclarant() {
        return this.declarant;
    }

    /**
     * Sets declarant entity.
     *
     * @param declarantEntity the declarant entity
     */
    public void setDeclarant(final DeclarantEntity declarantEntity) {
        this.declarant = declarantEntity;
    }

    /**
     * Gets actif.
     *
     * @return the actif
     */
    public Boolean getActif() {
        return this.actif;
    }

    /**
     * Sets actif.
     *
     * @param actif the actif
     */
    public void setActif(final Boolean actif) {
        this.actif = actif;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Gets fonction.
     *
     * @return the fonction
     */
    public String getFonction() {
        return this.fonction;
    }

    /**
     * Sets fonction.
     *
     * @param fonction the fonction
     */
    public void setFonction(final String fonction) {
        this.fonction = fonction;
    }

    /**
     * Gets order.
     *
     * @return the order
     */
    public Integer getSortOrder() {
        return this.sortOrder;
    }

    /**
     * Sets order.
     *
     * @param sortOrder the order
     */
    public void setSortOrder(final Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public LocalDateTime getDateDesactivation() {
        return dateDesactivation;
    }

    public void setDateDesactivation(LocalDateTime dateDesactivation) {
        this.dateDesactivation = dateDesactivation;
    }


}
