/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.persistence.entity.piece.EspaceOrganisationPieceEntity;

/**
 * Repository JPA pour l'entité {@link EspaceOrganisationPieceEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface EspaceOrganisationPiecesRepository
    extends JpaRepository<EspaceOrganisationPieceEntity, Long>
{
    /**
     * Renvoie la liste des pièces versées pour une origine donnée et un espace donné.
     * @return liste des pièces sélectionnées
     */
    Stream<EspaceOrganisationPieceEntity> findByEspaceOrganisationIdAndOrigineVersementOrderByDateVersementPieceDesc(Long idEspaceOrganisation, OrigineVersementEnum origineVersement);
}
