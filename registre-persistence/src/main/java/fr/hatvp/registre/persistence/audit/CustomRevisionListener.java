/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.audit;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


public class CustomRevisionListener
    implements RevisionListener
{

    /**
     * {@inheritDoc}
     */
    @Override
    public void newRevision(final Object revisionEntity)
    {
        final CustomRevisionEntity revision = (CustomRevisionEntity) revisionEntity;

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if( auth != null ) {
            revision.setUserEmail(auth.getName());
            revision.setUserRoles(auth.getAuthorities().toString());
        } else {
            revision.setUserEmail("BATCH");
            revision.setUserRoles("BATCH");
        }
    }
}
