/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.apiSirene;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

@JsonIgnoreProperties
public class Etablissement extends AbstractCommonEntity{
    /**
     * Numéro Siren
     */
    private String siren;

    private String etablissementSiege;
    
    private UniteLegale uniteLegale;
    
    private AdresseEtablissement adresseEtablissement;




    public String getSiren() {
        return siren;
    }

    public void setSiren(String siren) {
        this.siren = siren;
    }

	public String getEtablissementSiege() {
		return etablissementSiege;
	}

	public void setEtablissementSiege(String etablissementSiege) {
		this.etablissementSiege = etablissementSiege;
	}

	public UniteLegale getUniteLegale() {
		return uniteLegale;
	}

	public void setUniteLegale(UniteLegale uniteLegale) {
		this.uniteLegale = uniteLegale;
	}

	public AdresseEtablissement getAdresseEtablissement() {
		return adresseEtablissement;
	}

	public void setAdresseEtablissement(AdresseEtablissement adresseEtablissement) {
		this.adresseEtablissement = adresseEtablissement;
	}
    
    

}
