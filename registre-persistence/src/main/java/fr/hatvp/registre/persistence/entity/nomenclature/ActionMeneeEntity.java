/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.nomenclature;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "nomenclature_action_menee")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "nomenclature_action_menee_seq", allocationSize = 1)
public class ActionMeneeEntity extends CommonNomenclatureEntity {

	private static final long serialVersionUID = 8878508362138541942L;

	/**
	 * Default Constructor
	 */
	public ActionMeneeEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public ActionMeneeEntity(Long id) {
		super();
		this.setId(id);
	}

}
