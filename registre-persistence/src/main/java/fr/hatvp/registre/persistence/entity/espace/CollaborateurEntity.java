/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.persistence.entity.espace.common.CommonCollaborateurEntity;

/**
 * Entité des collaborateurs d'un espace organisation
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Audited
@Table(name = "collaborateur")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "collaborateur_seq",
        allocationSize = 1)
public class CollaborateurEntity
		extends CommonCollaborateurEntity {

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -4131999427243380839L;
}
