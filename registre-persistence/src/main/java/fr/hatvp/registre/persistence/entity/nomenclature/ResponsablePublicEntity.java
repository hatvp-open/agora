/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.nomenclature;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "nomenclature_responsable_public")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "nomenclature_responsable_public_seq", allocationSize = 1)
public class ResponsablePublicEntity extends CommonNomenclatureParentEntity {

	private static final long serialVersionUID = -2905513725766933086L;

	/**
	 * Default Constructor
	 */
	public ResponsablePublicEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public ResponsablePublicEntity(Long id) {
		super();
		this.setId(id);
	}

}
