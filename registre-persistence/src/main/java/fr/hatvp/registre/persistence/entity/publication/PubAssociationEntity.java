/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.publication;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.persistence.entity.espace.common.CommonAssociationEntity;

/**
 * Entité des associations publiés..
 * @version $Revision$ $Date${0xD}
 */
@Audited
@Entity
@Table(name = "pub_association")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "pub_association_seq",
        allocationSize = 1)
public class PubAssociationEntity
    extends CommonAssociationEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -9217926759634669557L;

    /** Référence vers la publicationEntity. */
    @ManyToOne
    private PublicationEntity publication;

    /**
     * Accesseur en lecture du champ <code>publicationEntity</code>.
     * @return le champ <code>publicationEntity</code>.
     */
    public PublicationEntity getPublication()
    {
        return this.publication;
    }

    /**
     * Accesseur en écriture du champ <code>publicationEntity</code>.
     * @param publicationEntity la valeur à écrire dans <code>publicationEntity</code>.
     */
    public void setPublication(final PublicationEntity publicationEntity)
    {
        this.publication = publicationEntity;
    }
}
