/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.nomenclature;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "nomenclature_montant_depense")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "nomenclature_montant_depense_seq", allocationSize = 1)
public class MontantDepenseEntity extends CommonNomenclatureEntity {

	private static final long serialVersionUID = 7554295241445064976L;

	/**
	 * Default Constructor
	 */
	public MontantDepenseEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public MontantDepenseEntity(Long id) {
		super();
		this.setId(id);
	}

}
