/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.activite;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DomaineInterventionEntity;

@Entity
@Audited
@Table(name = "activite_representation_interet")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "activite_representation_interet_seq", allocationSize = 1)
public class ActiviteRepresentationInteretEntity extends AbstractCommonEntity {

	private static final long serialVersionUID = 5053376163091896286L;

	/**
	 * Default Constructor
	 */
	public ActiviteRepresentationInteretEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public ActiviteRepresentationInteretEntity(Long id) {
		super();
		this.setId(id);
	}

	@Column(name = "objet", columnDefinition = "TEXT")
	private String objet;

	@ManyToMany
	@JoinTable(name = "activite_representation_domaine_intervention")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private Set<DomaineInterventionEntity> domainesIntervention = new HashSet<>();

	@NotAudited
	@ManyToOne
	@JoinColumn(name = "createur_declaration", nullable = false)
	private DeclarantEntity createurDeclaration;

	/** Date de création. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_creation", nullable = false, updatable = false)
	private Date creationDate = new Date();

	@OneToMany(mappedBy = "activiteRepresentationInteret", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<ActionRepresentationInteretEntity> actionsRepresentationInteret = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "exercice_comptable_id", nullable = false)
	private ExerciceComptableEntity exerciceComptable;

	@Column(name = "statut", nullable = false, columnDefinition = "TEXT")
	@Enumerated(EnumType.STRING)
	private StatutPublicationEnum statut;

	@Column(name = "id_fiche", columnDefinition = "TEXT")
	private String idFiche;

	/** Ce flag indique si l'exercice doit être publié sur le site institutionnel. */
	@Column(name = "publication_enabled")
	private Boolean publicationEnabled = Boolean.TRUE;

	@Column(name = "data_to_publish")
	private Boolean dataToPublish;

	/**
	 * @return the objet
	 */
	public String getObjet() {
		return objet;
	}

	/**
	 * @param objet
	 *            the objet to set
	 */
	public void setObjet(String objet) {
		this.objet = objet;
	}

	/**
	 * @return the domainesIntervention
	 */
	public Set<DomaineInterventionEntity> getDomainesIntervention() {
		return domainesIntervention;
	}

	/**
	 * @param domainesIntervention
	 *            the domainesIntervention to set
	 */
	public void setDomainesIntervention(Set<DomaineInterventionEntity> domainesIntervention) {
		this.domainesIntervention = domainesIntervention;
	}

	/**
	 * @return the createurDeclaration
	 */
	public DeclarantEntity getCreateurDeclaration() {
		return createurDeclaration;
	}

	/**
	 * @param createurDeclaration
	 *            the createurDeclaration to set
	 */
	public void setCreateurDeclaration(DeclarantEntity createurDeclaration) {
		this.createurDeclaration = createurDeclaration;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the actionsRepresentationInteret
	 */
	public List<ActionRepresentationInteretEntity> getActionsRepresentationInteret() {
		return actionsRepresentationInteret;
	}

	/**
	 * @param actionsRepresentationInteret
	 *            the actionsRepresentationInteret to set
	 */
	public void setActionsRepresentationInteret(List<ActionRepresentationInteretEntity> actionsRepresentationInteret) {
		this.actionsRepresentationInteret = actionsRepresentationInteret;
	}

	/**
	 * @return the exerciceComptable
	 */
	public ExerciceComptableEntity getExerciceComptable() {
		return exerciceComptable;
	}

	/**
	 * @param exerciceComptable
	 *            the exerciceComptable to set
	 */
	public void setExerciceComptable(ExerciceComptableEntity exerciceComptable) {
		this.exerciceComptable = exerciceComptable;
	}

	/**
	 *
	 * @return statut
	 */
	public StatutPublicationEnum getStatut() {
		return statut;
	}

	/**
	 *
	 * @param statut
	 */
	public void setStatut(StatutPublicationEnum statut) {
		this.statut = statut;
	}

	/**
	 * @return the idFiche
	 */
	public String getIdFiche() {
		return idFiche;
	}

	/**
	 * @param idFiche
	 *            the idFiche to set
	 */
	public void setIdFiche(String idFiche) {
		this.idFiche = idFiche;
	}

	public Boolean getPublicationEnabled() {
		return publicationEnabled;
	}

	public void setPublicationEnabled(Boolean publicationEnabled) {
		this.publicationEnabled = publicationEnabled;
	}

	public Boolean getDataToPublish() {
		return dataToPublish;
	}

	public void setDataToPublish(Boolean dataToPublish) {
		this.dataToPublish = dataToPublish;
	}

}