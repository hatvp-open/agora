/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.audit;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.ModifiedEntityNames;
import org.hibernate.envers.RevisionEntity;


@Entity
@Table(name = "audit_revision")
@RevisionEntity(CustomRevisionListener.class)
public class CustomRevisionEntity
    extends DefaultRevisionEntity
{
    private static final long serialVersionUID = 3775550420286576001L;

    /** Email de l'utilisateur responsable de la modification. */
    @Column(name = "user_email", columnDefinition = "TEXT")
    private String userEmail;

    /** Liste des rôles de l'utilisateur. */
    @Column(name = "user_roles", length = 1000, columnDefinition = "TEXT")
    private String userRoles;

    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "audit_revchanges", joinColumns = @JoinColumn(name = "rev"))
    @Column(name = "entity")
    @ModifiedEntityNames
    private Set<String> modifiedEntityNames;

    /**
     * Gets user email.
     *
     * @return the user email
     */
    public String getUserEmail()
    {
        return this.userEmail;
    }

    /**
     * Sets user email.
     *
     * @param username the username
     */
    public void setUserEmail(final String username)
    {
        this.userEmail = username;
    }

    /**
     * Gets user roles.
     *
     * @return the user roles
     */
    public String getUserRoles()
    {
        return this.userRoles;
    }

    /**
     * Sets user roles.
     *
     * @param userRoles the user roles
     */
    public void setUserRoles(final String userRoles)
    {
        this.userRoles = userRoles;
    }

    public Set<String> getModifiedEntityNames()
    {
        return this.modifiedEntityNames;
    }

    public void setModifiedEntityNames(final Set<String> modifiedEntityNames)
    {
        this.modifiedEntityNames = modifiedEntityNames;
    }
}