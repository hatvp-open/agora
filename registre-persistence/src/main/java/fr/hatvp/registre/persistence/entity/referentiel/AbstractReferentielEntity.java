/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.referentiel;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

/**
 * Classe commune de gestion des entités liées au référentiel pour la recherche des organisations.
 * @version $Revision$ $Date$.
 */
@MappedSuperclass
public abstract class AbstractReferentielEntity
    extends AbstractCommonEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -9084624112543435756L;

    /**
     * Date de dernière mise à jour des informations du référentiel par un batch HATVP.
     * Calculé par nos soins à chaque mise à jour des données.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_dernier_traitement", nullable = false)
    private Date dateDernierTraitement = new Date();

    /**
     * Nom complet ou raison sociale de l'association ou de l'entreprise.
     * Pour RNA : extrait de titre.
     * Pour SIRENE : extrait de nomen_long.
     */
    @Column(name = "denomination", columnDefinition = "TEXT")
    private String denomination;

    /**
     * Adresse du siège de l'association ou de l'entreprise.
     * Pour RNA : extrait de adrs_complement, adrs_numvoie, adrs_repetition, adrs_typevoie,
     * adrs_libvoie,
     * adrs_distrib.
     * Pour SIRENE : extrait de L1_NORMALISEE, L2_NORMALISEE, L3_NORMALISEE, L4_NORMALISEE,
     * L5_NORMALISEE
     */
    @Column(name = "adresse", columnDefinition = "TEXT")
    private String adresse;

    /**
     * Code postal du siège de l'association ou de l'entreprise.
     * Pour RNA : extrait de adrs_codepostal ou, si absent, du premier code postal correspondant au
     * code insee
     * adrs_codeinsee du
     * siège de l'association.
     * Pour SIRENE : extrait de L6_NORMALISEE si présent sinon de COSPOS.
     */
    @Column(name = "codepostal", columnDefinition = "TEXT")
    private String codePostal;

    /**
     * Ville du siège de l'association ou de l'entreprise.
     * Pour RNA : Extrait de adrs_libcommune ou, si absent, de la commune correspondant au code
     * insee
     * adrs_codeinsee du siège de l'association.
     * 
     * Pour SIRENE : extrait de L6_NORMALISEE.
     */
    @Column(name = "ville", columnDefinition = "TEXT")
    private String ville;

    /**
     * Champ pays de l'adresse de l'association ou de l'entreprise.
     * Pour RNA : extrait de adrg_pays.
     * Pour SIRENE : extrait de L7_NORMALISEE.
     */
    @Column(name = "pays", columnDefinition = "TEXT")
    private String pays;

    /**
     * Accesseur en lecture du champ <code>dateDernierTraitement</code>.
     * @return le champ <code>dateDernierTraitement</code>.
     */
    public Date getDateDernierTraitement()
    {
        return this.dateDernierTraitement;
    }

    /**
     * Accesseur en écriture du champ <code>dateDernierTraitement</code>.
     * @param dateDernierTraitement la valeur à écrire dans <code>dateDernierTraitement</code>.
     */
    public void setDateDernierTraitement(final Date dateDernierTraitement)
    {
        this.dateDernierTraitement = dateDernierTraitement;
    }

    /**
     * Accesseur en lecture du champ <code>denomination</code>.
     * @return le champ <code>denomination</code>.
     */
    public String getDenomination()
    {
        return this.denomination;
    }

    /**
     * Accesseur en écriture du champ <code>denomination</code>.
     * @param denomination la valeur à écrire dans <code>denomination</code>.
     */
    public void setDenomination(final String denomination)
    {
        this.denomination = denomination;
    }

    /**
     * Accesseur en lecture du champ <code>adresse</code>.
     * @return le champ <code>adresse</code>.
     */
    public String getAdresse()
    {
        return this.adresse;
    }

    /**
     * Accesseur en écriture du champ <code>adresse</code>.
     * @param adresse la valeur à écrire dans <code>adresse</code>.
     */
    public void setAdresse(final String adresse)
    {
        this.adresse = adresse;
    }

    /**
     * Accesseur en lecture du champ <code>codePostal</code>.
     * @return le champ <code>codePostal</code>.
     */
    public String getCodePostal()
    {
        return this.codePostal;
    }

    /**
     * Accesseur en écriture du champ <code>codePostal</code>.
     * @param codePostal la valeur à écrire dans <code>codePostal</code>.
     */
    public void setCodePostal(final String codePostal)
    {
        this.codePostal = codePostal;
    }

    /**
     * Accesseur en lecture du champ <code>ville</code>.
     * @return le champ <code>ville</code>.
     */
    public String getVille()
    {
        return this.ville;
    }

    /**
     * Accesseur en écriture du champ <code>ville</code>.
     * @param ville la valeur à écrire dans <code>ville</code>.
     */
    public void setVille(final String ville)
    {
        this.ville = ville;
    }

    /**
     * Accesseur en lecture du champ <code>pays</code>.
     * @return le champ <code>pays</code>.
     */
    public String getPays()
    {
        return this.pays;
    }

    /**
     * Accesseur en écriture du champ <code>pays</code>.
     * @param pays la valeur à écrire dans <code>pays</code>.
     */
    public void setPays(final String pays)
    {
        this.pays = pays;
    }

}
