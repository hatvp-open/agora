/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.referentiel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * Référentiel SIRENE des entreprises françaises.
 * Les entreprises sont identifiées de manière unique par un numéro SIREN à 9 chiffres.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "referentiel_sirene")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "sirene_seq", allocationSize = 1)
public class ReferentielSireneEntity
    extends AbstractReferentielEntity
{

    /** Identifiant de sérialisation. */
    private static final long serialVersionUID = -6183221277475693023L;

    /**
     * Code SIREN de l'entreprise sur 9 chiffres.
     * Respecte la formule de Luhn.
     * Extrait de siren.
     */
    @Column(name = "siren", nullable = false, unique = true, columnDefinition = "TEXT")
    private String siren;

    /**
     * NIC de l'entreprise.
     * Extrait de nic.
     */
    @Column(name = "nic", columnDefinition = "TEXT")
    private String nic;

    /**
     * Le cas échéant : identifiant dans le Registre National des Associations.
     * Format W + 9 chiffres.
     * Extrait de rna.
     */
    @Column(name = "rna", columnDefinition = "TEXT")
    private String rna;

    /**
     * Date de création de l'entreprise : année et mois
     * Extrait de DCREN.
     */
    @Column(name = "date_creation_entreprise", columnDefinition = "TEXT")
    private String dateCreationEntreprise;

    /**
     * Date d'entrée de l'entreprise dans le référentiel : année et mois
     * Extrait de AMINTREN.
     */
    @Column(name = "date_entree_entreprise", columnDefinition = "TEXT")
    private String dateEntreeEntreprise;

    /**
     * Date de dernière mise à jour des données de l'entreprise dans le référentiel SIRENE.
     * Extrait de DATEMAJ.
     */
    @Column(name = "date_maj_entreprise", columnDefinition = "TEXT")
    private String dateMajEntreprise;

    /**
     * Accesseur en lecture du champ <code>siren</code>.
     * @return le champ <code>siren</code>.
     */
    public String getSiren()
    {
        return this.siren;
    }

    /**
     * Accesseur en écriture du champ <code>siren</code>.
     * @param siren la valeur à écrire dans <code>siren</code>.
     */
    public void setSiren(final String siren)
    {
        this.siren = siren;
    }

    /**
     * Accesseur en lecture du champ <code>nic</code>.
     * @return le champ <code>nic</code>.
     */
    public String getNic()
    {
        return this.nic;
    }

    /**
     * Accesseur en écriture du champ <code>nic</code>.
     * @param nic la valeur à écrire dans <code>nic</code>.
     */
    public void setNic(final String nic)
    {
        this.nic = nic;
    }

    /**
     * Accesseur en lecture du champ <code>rna</code>.
     * @return le champ <code>rna</code>.
     */
    public String getRna()
    {
        return this.rna;
    }

    /**
     * Accesseur en écriture du champ <code>rna</code>.
     * @param rna la valeur à écrire dans <code>rna</code>.
     */
    public void setRna(final String rna)
    {
        this.rna = rna;
    }

    /**
     * Accesseur en lecture du champ <code>dateCreationEntreprise</code>.
     * @return le champ <code>dateCreationEntreprise</code>.
     */
    public String getDateCreationEntreprise()
    {
        return this.dateCreationEntreprise;
    }

    /**
     * Accesseur en écriture du champ <code>dateCreationEntreprise</code>.
     * @param dateCreationEntreprise la valeur à écrire dans <code>dateCreationEntreprise</code>.
     */
    public void setDateCreationEntreprise(final String dateCreationEntreprise)
    {
        this.dateCreationEntreprise = dateCreationEntreprise;
    }

    /**
     * Accesseur en lecture du champ <code>dateEntreeEntreprise</code>.
     * @return le champ <code>dateEntreeEntreprise</code>.
     */
    public String getDateEntreeEntreprise()
    {
        return this.dateEntreeEntreprise;
    }

    /**
     * Accesseur en écriture du champ <code>dateEntreeEntreprise</code>.
     * @param dateEntreeEntreprise la valeur à écrire dans <code>dateEntreeEntreprise</code>.
     */
    public void setDateEntreeEntreprise(final String dateEntreeEntreprise)
    {
        this.dateEntreeEntreprise = dateEntreeEntreprise;
    }

    /**
     * Accesseur en lecture du champ <code>dateMajEntreprise</code>.
     * @return le champ <code>dateMajEntreprise</code>.
     */
    public String getDateMajEntreprise()
    {
        return this.dateMajEntreprise;
    }

    /**
     * Accesseur en écriture du champ <code>dateMajEntreprise</code>.
     * @param dateMajEntreprise la valeur à écrire dans <code>dateMajEntreprise</code>.
     */
    public void setDateMajEntreprise(final String dateMajEntreprise)
    {
        this.dateMajEntreprise = dateMajEntreprise;
    }

}
