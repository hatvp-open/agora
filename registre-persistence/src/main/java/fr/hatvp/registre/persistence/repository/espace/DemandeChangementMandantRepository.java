/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.commons.lists.ChangementContactEnum;
import fr.hatvp.registre.commons.lists.StatutDemandeEnum;
import fr.hatvp.registre.persistence.entity.espace.DemandeChangementMandantEntity;

/**
 *
 * Repository JPA pour l'entité {@link DemandeChangementMandantEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface DemandeChangementMandantRepository extends JpaRepository<DemandeChangementMandantEntity, Long> {

	/**
	 * Récupérer demandes par Id Espace Organisation.
	 *
	 * @param espaceOrganisationId
	 *            id de l'espace organisation
	 * @return liste des inscriptions
	 */
	List<DemandeChangementMandantEntity> findAllByEspaceOrganisationId(Long espaceOrganisationId);

	/**
	 * Récupérer demandes en cours.
	 *
	 * @return liste des inscriptions
	 */
	Page<DemandeChangementMandantEntity> findAllByDateValidationIsNullAndDateRejetIsNull(Pageable page);

	/**
	 * Récupérer demandes validées par.
	 *
	 * @return liste des inscriptions
	 */
	Page<DemandeChangementMandantEntity> findAllByDateValidationIsNotNull(Pageable page);

	/**
	 * Récupérer demandes rejetées.
	 *
	 * @return liste des inscriptions
	 */
	Page<DemandeChangementMandantEntity> findAllByDateRejetIsNotNull(Pageable page);

	/**
	 * Récupérer demandes recherche par denomination
	 *
	 * @return liste des inscriptions
	 */
	Page<DemandeChangementMandantEntity> findByEspaceOrganisationOrganisationDenominationIgnoreCaseContainingOrderByIdAsc(String keywords, Pageable pageRequest);

	@Query(value = "SELECT * " + "FROM demande_changement_mandant AS dem " + "LEFT JOIN espace_organisation AS esp ON dem.espace_organisation_id = esp.id "
			+ "LEFT JOIN organisation AS org ON esp.organisation_id = org.id " + "WHERE unaccent(org.denomination) ILIKE unaccent(?1) " + "ORDER BY dem.id DESC "
			+ "LIMIT ?2 OFFSET ?3 ", nativeQuery = true)
	List<DemandeChangementMandantEntity> findByDenomination(String keyword, Integer limit, Integer offset);

	@Query(value = "SELECT COUNT(*) " + "FROM demande_changement_mandant AS dem " + "LEFT JOIN espace_organisation AS esp ON dem.espace_organisation_id = esp.id "
			+ "LEFT JOIN organisation AS org ON esp.organisation_id = org.id " + "WHERE unaccent(org.denomination) ILIKE unaccent(?1) ", nativeQuery = true)
	Integer countDemandeFilteredByDenomination(String keyword);

	/**
	 * Récupérer demandes recherche par nom et prenom
	 *
	 * @return liste des inscriptions
	 */
	Page<DemandeChangementMandantEntity> findByDeclarantNomIgnoreCaseContainingOrDeclarantPrenomIgnoreCaseContainingOrderByIdAsc(String nom, String prenom, Pageable pageRequest);

	@Query(value = "SELECT * " + "FROM demande_changement_mandant AS dem " + "LEFT JOIN declarant AS dec ON dem.declarant_id = dec.id " + "WHERE unaccent(dec.nom) ILIKE unaccent(?1) "
			+ "OR unaccent(dec.prenom) ILIKE unaccent(?1) " + "ORDER BY dem.id DESC " + "LIMIT ?2 OFFSET ?3 ", nativeQuery = true)
	List<DemandeChangementMandantEntity> findByNomOrPrenom(String keyword, Integer limit, Integer offset);

	@Query(value = "SELECT COUNT(*) " + "FROM demande_changement_mandant AS dem " + "LEFT JOIN declarant AS dec ON dem.declarant_id = dec.id " + "WHERE unaccent(dec.nom) ILIKE unaccent(?1) "
			+ "OR unaccent(dec.prenom) ILIKE unaccent(?1) ", nativeQuery = true)
	Integer countDemandeFilteredByNomOrPrenom(String keyword);

	/**
	 * Récupérer demandes recherche par type de la demande
	 *
	 * @return liste des inscriptions
	 */
	Page<DemandeChangementMandantEntity> findByTypeDemande(ChangementContactEnum type, Pageable pageRequest);

	/**
	 * Récupérer demandes recherche par date de la demande
	 *
	 * @return liste des inscriptions
	 */
	Page<DemandeChangementMandantEntity> findByDateDemandeBetweenOrderByDateDemandeAsc(Date date, Date date2, Pageable pageRequest);

	Page<DemandeChangementMandantEntity> findByStatutOrderByIdAsc(StatutDemandeEnum statut, Pageable pageable);

	/**
	 * Récupérer demandes par Id Espace Organisation triées par date.
	 *
	 * @param espaceOrganisationId
	 *            id de l'espace organisation
	 * @return liste des inscriptions
	 */
	List<DemandeChangementMandantEntity> findAllByEspaceOrganisationIdOrderByDateDemandeDesc(Long espaceOrganisationId);

	List<DemandeChangementMandantEntity> findByStatut(StatutDemandeEnum statut);

}
