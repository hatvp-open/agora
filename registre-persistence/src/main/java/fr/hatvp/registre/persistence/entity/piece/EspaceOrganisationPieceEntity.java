/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.piece;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * Entité d'enregistrement des fichiers fournis pour la validation de la création de l'espace collaboratif.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "espace_organisation_piece")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "espace_organisation_piece_seq", allocationSize = 1)
public class EspaceOrganisationPieceEntity extends PieceEntity {
	/** Clé de sérialisation. */
	private static final long serialVersionUID = -8530127281847343989L;

	/**
	 * Espace organisation pour lequel la pièce a été versée.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "espace_organisation_id", nullable = false, updatable = false)
	private EspaceOrganisationEntity espaceOrganisation;

	/**
	 * Gets espace organisation.
	 *
	 * @return the espace organisation
	 */
	public EspaceOrganisationEntity getEspaceOrganisation() {
		return this.espaceOrganisation;
	}

	/**
	 * Sets espace organisation.
	 *
	 * @param espaceOrganisation
	 *            the espace organisation
	 */
	public void setEspaceOrganisation(final EspaceOrganisationEntity espaceOrganisation) {
		this.espaceOrganisation = espaceOrganisation;
	}
}
