/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.referentiel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

/**
 * 
 * Référentiel INSEE des communes françaises.
 * Utile pour la mise à jour du référentiel RNA. Permet de déterminer la commune ou le code postal
 * du siège lorsqu'il n'est pas précisé.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "referentiel_insee_communes")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "insee_communes_seq",
        allocationSize = 1)
public class ReferentielInseeCommunesEntity
    extends AbstractCommonEntity
{

    /** Identifiant de sérialisation. */
    private static final long serialVersionUID = -1391332473530616461L;

    /**
     * Code INSEE de la commune.
     */
    @Column(name = "codeinsee", nullable = false, unique = true, columnDefinition = "TEXT")
    private String codeinsee;

    /**
     * Codes postaux de la commune. Séparés par des /. Généralement un seul code postal.
     */
    @Column(name = "codepostal", nullable = false, columnDefinition = "TEXT")
    private String codepostal;

    /**
     * Libellé de la commune associée au code INSEE.
     */
    @Column(name = "commune", nullable = false, columnDefinition = "TEXT")
    private String commune;

    /**
     * Accesseur en lecture du champ <code>codeinsee</code>.
     * @return le champ <code>codeinsee</code>.
     */
    public String getCodeinsee()
    {
        return this.codeinsee;
    }

    /**
     * Accesseur en écriture du champ <code>codeinsee</code>.
     * @param codeinsee la valeur à écrire dans <code>codeinsee</code>.
     */
    public void setCodeinsee(final String codeinsee)
    {
        this.codeinsee = codeinsee;
    }

    /**
     * Accesseur en lecture du champ <code>codepostal</code>.
     * @return le champ <code>codepostal</code>.
     */
    public String getCodepostal()
    {
        return this.codepostal;
    }

    /**
     * Accesseur en écriture du champ <code>codepostal</code>.
     * @param codepostal la valeur à écrire dans <code>codepostal</code>.
     */
    public void setCodepostal(final String codepostal)
    {
        this.codepostal = codepostal;
    }

    /**
     * Accesseur en lecture du champ <code>commune</code>.
     * @return le champ <code>commune</code>.
     */
    public String getCommune()
    {
        return this.commune;
    }

    /**
     * Accesseur en écriture du champ <code>commune</code>.
     * @param commune la valeur à écrire dans <code>commune</code>.
     */
    public void setCommune(final String commune)
    {
        this.commune = commune;
    }

}
