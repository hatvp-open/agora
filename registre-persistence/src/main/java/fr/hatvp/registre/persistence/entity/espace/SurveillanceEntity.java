/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.hatvp.registre.commons.lists.SurveillanceOrganisationEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.backoffice.GroupBoEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;

@Entity
@Table(name = "surveillance")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "surveillance_seq", allocationSize = 1)
public class SurveillanceEntity extends AbstractCommonEntity  {

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -6037615634070497109L;

    @OneToOne(fetch = FetchType.LAZY)
    private OrganisationEntity organisationEntity;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "surveillance_type", joinColumns = @JoinColumn(name = "surveillance_id"))
    @Column(name = "type_surveillance", columnDefinition = "TEXT")
    @Enumerated(EnumType.STRING)
    private Set<SurveillanceOrganisationEnum> typeSurveillance = new HashSet<>();

    @ManyToMany()
    @JoinTable(name = "surveillance_utilisateur_bo",joinColumns = {
        @JoinColumn(name = "surveillance_id", nullable = false, updatable = false) },
        inverseJoinColumns = { @JoinColumn(name = "surveillant_id",
            nullable = false, updatable = false) })

    private Set<UtilisateurBoEntity> surveillants = new HashSet<>();


    @ManyToMany()
    @JoinTable(name = "surveillance_groupe_bo",joinColumns = {
        @JoinColumn(name = "surveillance_id", nullable = false, updatable = false) },
        inverseJoinColumns = { @JoinColumn(name = "group_id",
            nullable = false, updatable = false) })
    private Set<GroupBoEntity> surveillantsGroupes = new HashSet<>();


    @Column(name = "date_surveillance")
    private LocalDateTime dateSurveillance;

    /** Table surveillance. */
    @Column(name = "motif_surveillance", columnDefinition = "TEXT")
    private String motif;



    public Set<SurveillanceOrganisationEnum> getTypeSurveillance() {
        return typeSurveillance;
    }

    public void setTypeSurveillance(Set<SurveillanceOrganisationEnum> typeSurveillance) {
        this.typeSurveillance = typeSurveillance;
    }

    public OrganisationEntity getOrganisationEntity() {
        return organisationEntity;
    }

    public void setOrganisationEntity(OrganisationEntity organisationEntity) {
        this.organisationEntity = organisationEntity;
    }

    public Set<GroupBoEntity> getSurveillantsGroupes() {
        return surveillantsGroupes;
    }

    public void setSurveillantsGroupes(Set<GroupBoEntity> surveillantsGroupes) {
        this.surveillantsGroupes = surveillantsGroupes;
    }

    public Set<UtilisateurBoEntity> getSurveillants() {
        return surveillants;
    }

    public void setSurveillants(Set<UtilisateurBoEntity> surveillants) {
        this.surveillants = surveillants;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public LocalDateTime getDateSurveillance() {
        return dateSurveillance;
    }

    public void setDateSurveillance(LocalDateTime dateSurveillance) {
        this.dateSurveillance = dateSurveillance;
    }

}

