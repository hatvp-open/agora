/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.activite;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationExerciceEntity;
import org.springframework.data.jpa.repository.Query;

public interface PublicationExerciceRepository extends JpaRepository<PublicationExerciceEntity, Long> {

	List<PublicationExerciceEntity> findAllByExerciceComptableIdAndStatutNotOrderByIdDesc(Long exerciceId, StatutPublicationEnum statut);

	PublicationExerciceEntity findFirstByExerciceComptableIdOrderByCreationDateDesc(Long exerciceId);

	/**
	 * Récupérer la dernière publication d'un espace organisation.
	 *
	 * @param espaceId
	 *            id de l'espace organisation.
	 * @return la publication si elle existe.
	 */
	PublicationExerciceEntity findFirstByExerciceComptableIdAndStatutNotOrderByIdDesc(Long exerciceId, StatutPublicationEnum statut);

	@Query("select min(p.creationDate) from PublicationExerciceEntity AS p where p.exerciceComptable.id = ?1")
	String findDateFirstCommMoyens(Long exerciceId);

    @Query("select max(p.creationDate) from PublicationExerciceEntity AS p where p.exerciceComptable.id = ?1")
    String findDateLastChangeMoyens(Long exerciceId);

    Integer countByExerciceComptableIdAndStatutNotOrderByIdDesc(long exerciceId, StatutPublicationEnum statut);
    
    List<PublicationExerciceEntity> findAllByExerciceComptableIdOrderByIdAsc(Long exerciceId);

}
