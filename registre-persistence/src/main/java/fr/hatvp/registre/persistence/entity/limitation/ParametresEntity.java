/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.limitation;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.envers.Audited;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

/**
 * Table qui enregistre les paramètres du mode haute activité.
 * @version $Revision$ $Date${0xD}.
 */
@Entity
@Audited
@Table(name = "limitation")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "parametres_seq", allocationSize = 1)
public class ParametresEntity extends AbstractCommonEntity
{

    private static final long serialVersionUID = -4889232242048494066L;

    /**
     * Mode actif ou non
     */
    @Column(name = "active", nullable = false)
    private Boolean active = Boolean.FALSE;

    /**
     * Nombre limite de déclarants simultanés sur l'application.
     */
    @Column(name = "nombre_limite", nullable = false)
    private Integer nombreLimite = 50;

    public Boolean getActive()
    {
        return active;
    }
    public void setActive(Boolean active)
    {
        this.active = active;
    }
    public Integer getNombreLimite()
    {
        return nombreLimite;
    }
    public void setNombreLimite(Integer nombreLimite)
    {
        this.nombreLimite = nombreLimite;
    }
}
