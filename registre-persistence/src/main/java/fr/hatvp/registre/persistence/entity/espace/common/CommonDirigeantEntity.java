/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace.common;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.commons.lists.CiviliteEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * Entité commune des dirigeants.
 *
 * @version $Revision$ $Date$.
 */
@MappedSuperclass
public class CommonDirigeantEntity
    extends AbstractCommonEntity
{
    /**
     * Clé de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /** Civilité. */
    @Audited
    @Column(name = "civilite", columnDefinition = "TEXT")
    @Enumerated(EnumType.STRING)
    private CiviliteEnum civilite;

    /**
     * Nom
     */
    @Audited
    @Column(name = "nom", columnDefinition = "TEXT")
    private String nom;

    /**
     * Prénom
     */
    @Audited
    @Column(name = "prenom", columnDefinition = "TEXT")
    private String prenom;

    /** Fonctions exercées. */
    @Audited
    @Column(name = "fonction", columnDefinition = "TEXT")
    private String fonction;

    /**
     * Organisation
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "espace_organisation_id", nullable = false)
    private EspaceOrganisationEntity espaceOrganisation;
    
    /** Ordre hierarchique. */
    @Column(name = "sort_order")
    private Integer sortOrder = 0;

    /**
     * Accesseur en lecture du champ <code>nom</code>.
     *
     * @return le champ <code>nom</code>.
     */
    public String getNom()
    {
        return this.nom;
    }

    /**
     * Accesseur en écriture du champ <code>nom</code>.
     *
     * @param nom la valeur à écrire dans <code>nom</code>.
     */
    public void setNom(final String nom)
    {
        this.nom = nom;
    }

    /**
     * Accesseur en lecture du champ <code>prenom</code>.
     *
     * @return le champ <code>prenom</code>.
     */
    public String getPrenom()
    {
        return this.prenom;
    }

    /**
     * Accesseur en écriture du champ <code>prenom</code>.
     *
     * @param prenom la valeur à écrire dans <code>prenom</code>.
     */
    public void setPrenom(final String prenom)
    {
        this.prenom = prenom;
    }

    /**
     * Accesseur en lecture du champ <code>espaceOrganisationEntity</code>.
     *
     * @return le champ <code>espaceOrganisationEntity</code>.
     */
    public EspaceOrganisationEntity getEspaceOrganisation()
    {
        return this.espaceOrganisation;
    }

    /**
     * Accesseur en écriture du champ <code>espaceOrganisationEntity</code>.
     *
     * @param espaceOrganisation la valeur à écrire dans
     * <code>espaceOrganisationEntity</code>.
     */
    public void setEspaceOrganisation(final EspaceOrganisationEntity espaceOrganisation)
    {
        this.espaceOrganisation = espaceOrganisation;
    }

    /**
     * Gets civilite.
     *
     * @return the civilite
     */
    public CiviliteEnum getCivilite()
    {
        return this.civilite;
    }

    /**
     * Sets civilite.
     *
     * @param civilite the civilite
     */
    public void setCivilite(final CiviliteEnum civilite)
    {
        this.civilite = civilite;
    }

    /**
     * Gets fonction.
     *
     * @return the fonction
     */
    public String getFonction()
    {
        return this.fonction;
    }

    /**
     * Sets fonction.
     *
     * @param fonction the fonction
     */
    public void setFonction(final String fonction)
    {
        this.fonction = fonction;
    }

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}    
    
}