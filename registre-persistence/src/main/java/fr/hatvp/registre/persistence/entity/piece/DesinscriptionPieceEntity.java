/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.piece;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;

@Entity
@Table(name = "desinscription_piece")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "desinscription_piece_seq", allocationSize = 1)
public class DesinscriptionPieceEntity extends AbstractCommonEntity{

    private static final long serialVersionUID = 8202215888648391464L;
    
    /** Date de versement du fichier. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_versement_piece", nullable = false, updatable = false)
	private Date dateVersementPiece = new Date();

	/** Ce champ permet de déterminer l'origine de la pièce versée.
	 * DEMANDE_DESINSCRIPTION_ESPACE ou DESINSCRIPTION_ESPACE
	 *  */
	@Column(name = "origine_versement", nullable = false, columnDefinition = "TEXT", updatable = false)
	@Enumerated(EnumType.STRING)
	private OrigineVersementEnum origineVersement = null;

	/** Nom du fichier versé. */
	@Column(name = "nom_fichier", columnDefinition = "TEXT", nullable = false, updatable = false)
	private String nomFichier;

	/** Le contenu du fichier versé. */
	@Column(name = "contenu_fichier", nullable = false, updatable = false)
	private byte[] contenuFichier;

	/** Type mime du fichier versé. */
	@Column(name = "media_type", columnDefinition = "TEXT", updatable = false)
	private String mediaType;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "desinscription_id", nullable = false)
    private DesinscriptionEntity desinscription;
    
    public DesinscriptionEntity getDesinscription() {
        return desinscription;
    }

    public void setDesinscription(DesinscriptionEntity desinscription) {
        this.desinscription = desinscription;
    }

	public Date getDateVersementPiece() {
		return dateVersementPiece;
	}

	public void setDateVersementPiece(Date dateVersementPiece) {
		this.dateVersementPiece = dateVersementPiece;
	}

	public OrigineVersementEnum getOrigineVersement() {
		return origineVersement;
	}

	public void setOrigineVersement(OrigineVersementEnum origineVersement) {
		this.origineVersement = origineVersement;
	}

	public String getNomFichier() {
		return nomFichier;
	}

	public void setNomFichier(String nomFichier) {
		this.nomFichier = nomFichier;
	}

	public byte[] getContenuFichier() {
		return contenuFichier;
	}

	public void setContenuFichier(byte[] contenuFichier) {
		this.contenuFichier = contenuFichier;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
    
    
}
