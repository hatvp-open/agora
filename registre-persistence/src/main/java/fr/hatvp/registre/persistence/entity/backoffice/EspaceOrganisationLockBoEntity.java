/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.backoffice;

import java.util.Date;

import javax.persistence.*;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * Table qui enregistre les verrous de modification sur les espaces organisations.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "espace_organisation_lock_bo")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "espace_organisation_lock_bo_seq",
        allocationSize = 1)
public class EspaceOrganisationLockBoEntity
    extends AbstractCommonEntity
{

    /**
     * Clé de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Espace Organisation.
     */
    @ManyToOne(targetEntity = EspaceOrganisationEntity.class)
    @JoinColumn(name = "espace_organisation_id")
    private EspaceOrganisationEntity espaceOrganisation;

    /**
     * Utilisateur BO.
     */
    @ManyToOne(targetEntity = UtilisateurBoEntity.class)
    @JoinColumn(name = "utilisateur_bo_id")
    private UtilisateurBoEntity utilisateurBo;

    /**
     * Date lock.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "locked_time")
    private Date lockedTime = new Date();

    /**
     * Gets espace organisation.
     *
     * @return the espace organisation
     */
    public EspaceOrganisationEntity getEspaceOrganisation()
    {
        return this.espaceOrganisation;
    }

    /**
     * Sets espace organisation.
     *
     * @param espaceOrganisation the espace organisation
     */
    public void setEspaceOrganisation(final EspaceOrganisationEntity espaceOrganisation)
    {
        this.espaceOrganisation = espaceOrganisation;
    }

    /**
     * Gets utilisateur bo.
     *
     * @return the utilisateur bo
     */
    public UtilisateurBoEntity getUtilisateurBo()
    {
        return this.utilisateurBo;
    }

    /**
     * Sets utilisateur bo.
     *
     * @param utilisateurBo the utilisateur bo
     */
    public void setUtilisateurBo(final UtilisateurBoEntity utilisateurBo)
    {
        this.utilisateurBo = utilisateurBo;
    }

    /**
     * Gets locked time.
     *
     * @return the locked time
     */
    public Date getLockedTime()
    {
        return this.lockedTime;
    }

    /**
     * Sets locked time.
     *
     * @param lockedTime the locked time
     */
    public void setLockedTime(final Date lockedTime)
    {
        this.lockedTime = lockedTime;
    }
}
