/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice.commentaire;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.commentaire.CommentEspaceOrganisationEntity;

/**
 * 
 * Repository des commentaires des espaces organisations.
 * @version $Revision$ $Date${0xD}
 */
public interface CommentEspaceOrganisationRepository
    extends JpaRepository<CommentEspaceOrganisationEntity, Long>
{
    /**
     *
     * @param espaceOrganisationId id de l'espacfe organisation.
     * @return liste des commentaire de l'espace organisation
     */
    List<CommentEspaceOrganisationEntity> findByEspaceOrganisationEntityId(
            Long espaceOrganisationId);
}
