/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice;

import fr.hatvp.registre.persistence.entity.backoffice.GroupBoEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface GroupBoRepository extends JpaRepository<GroupBoEntity, Long> {

@Query("select g from GroupBoEntity as g where g.libelle LIKE :keyword")
    Page<GroupBoEntity> findByLibelle(@Param("keyword") String keyword, Pageable pageable);

@Query("select g from GroupBoEntity as g, UtilisateurBoEntity u where u.nom LIKE :keyword and u MEMBER OF g.utilisateurBoEntity ")
    Page<GroupBoEntity> findByUserBo(@Param("keyword") String keyword, Pageable pageable);

    GroupBoEntity findById(Long id);

    }
