/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.commons.lists.backoffice.StatutDemandeOrganisationEnum;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;

/**
 *
 * Repository JPA pour l'entité {@link fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface DemandeOrganisationRepository extends JpaRepository<DemandeOrganisationEntity, Long> {

	/**
	 * Retourne une liste paginée des demandes d'ajout d'organisations non traitées
	 *
	 * @param statut
	 * @param page
	 * @return liste paginée
	 */
	Page<DemandeOrganisationEntity> findByStatut(StatutDemandeOrganisationEnum statut, Pageable page);

	/**
	 * Retourne une liste paginée des demandes d'ajout d'organisations par demandeur
	 *
	 * @param nom
	 * @param page
	 * @return liste paginée
	 */
	Page<DemandeOrganisationEntity> findByDeclarantNomIgnoreCaseContainingOrderByIdAsc(String nom, Pageable page);

	@Query(value = "SELECT * " + "FROM demande_organisation AS dem " + "LEFT JOIN declarant AS dec ON dem.declarant_id = dec.id " + "WHERE unaccent(dec.nom) ILIKE unaccent(?1) "
			+ "OR unaccent(dec.prenom) ILIKE unaccent(?1) " + "ORDER BY dem.id DESC " + "LIMIT ?2 OFFSET ?3 ", nativeQuery = true)
	List<DemandeOrganisationEntity> findByDemandeur(String keyword, Integer limit, Integer offset);

	@Query(value = "SELECT COUNT(*) " + "FROM demande_organisation AS dem " + "LEFT JOIN declarant AS dec ON dem.declarant_id = dec.id " + "WHERE unaccent(dec.nom) ILIKE unaccent(?1) "
			+ "OR unaccent(dec.prenom) ILIKE unaccent(?1) ", nativeQuery = true)
	Integer countDemandeFilteredByDemandeur(String keyword);

	/**
	 * Retourne une liste paginée des demandes d'ajout d'organisations par date
	 *
	 * @param nom
	 * @param page
	 * @return liste paginée
	 */
	Page<DemandeOrganisationEntity> findByDateCreationBetweenOrderByDateCreationAsc(Date date, Date date2, Pageable pageRequest);

	/**
	 * Retourne une liste paginée des demandes d'ajout d'organisations recherche par denomination
	 *
	 * @param denomination
	 * @param page
	 * @return liste paginée
	 */
	Page<DemandeOrganisationEntity> findByDenominationIgnoreCaseContainingOrderByIdAsc(String denomination, Pageable page);

	@Query(value = "SELECT * " + "FROM demande_organisation " + "WHERE unaccent(denomination) ILIKE unaccent(?1) " + "ORDER BY id DESC " + "LIMIT ?2 OFFSET ?3 ", nativeQuery = true)
	List<DemandeOrganisationEntity> findByDenomination(String keyword, Integer limit, Integer offset);

	@Query(value = "SELECT COUNT(*) " + "FROM demande_organisation " + "WHERE unaccent(denomination) ILIKE unaccent(?1) ", nativeQuery = true)
	Integer countDemandeFilteredByDenomination(String keyword);

	List<DemandeOrganisationEntity> findByStatut(StatutDemandeOrganisationEnum aTraiter);

	@Query(value = "SELECT * " + "FROM demande_organisation " + "WHERE id IN(SELECT inn.id\r\n" + "FROM(SELECT DISTINCT demande_organisation_id AS id , max(locked_time) "
			+ "FROM demande_organisation_lock_bo AS lock " + "LEFT JOIN utilisateur_bo AS us ON lock.utilisateur_bo_id = us.id " + "WHERE unaccent(us.nom) ILIKE unaccent(?1) "
			+ "OR unaccent(us.prenom) ILIKE unaccent(?1) " + "GROUP BY demande_organisation_id " + ") AS inn " + ") " + "ORDER BY id DESC " + "LIMIT ?2 OFFSET ?3 ", nativeQuery = true)
	List<DemandeOrganisationEntity> findByDernierAcces(String keyword, Integer limit, Integer offset);

	@Query(value = "SELECT COUNT(*) " + "FROM (SELECT inn.id " + "FROM(SELECT DISTINCT demande_organisation_id AS id , max(locked_time) " + "FROM demande_organisation_lock_bo AS lock "
			+ "LEFT JOIN utilisateur_bo AS us ON lock.utilisateur_bo_id = us.id " + "WHERE unaccent(us.nom) ILIKE unaccent(?1) " + "OR unaccent(us.prenom) ILIKE unaccent(?1) "
			+ "GROUP BY demande_organisation_id\r\n" + ") AS inn " + ") AS out ", nativeQuery = true)
	Integer countDemandeFilteredByDernierAcces(String keyword);
}
