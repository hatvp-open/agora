/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.activite;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.persistence.entity.activite.ActiviteRepresentationInteretEntity;

/**
 * Repository JPA pour l'entité {@link ActiviteRepresentationInteretEntity}.
 */
public interface ActiviteRepresentationInteretRepository extends JpaRepository<ActiviteRepresentationInteretEntity, Long> {
	/**
	 * 
	 * @param cessationDate
	 * @param espaceId
	 * @return une activité publiée après la date de cessation demandée si elle existe
	 */
	@Query(value = "select  a.* from activite_representation_interet a " + 
			"inner join exercice_comptable ec ON ec.id = a.exercice_comptable_id " + 
			"where ec.statut='NON_PUBLIEE' and a.statut in ('PUBLIEE','MAJ_PUBLIEE','REPUBLIEE') and ec.date_debut> ?1 and ec.espace_organisation_id= ?2 " +
			"order by a.id desc limit 1", nativeQuery = true)
	ActiviteRepresentationInteretEntity findActivitePublieeApresDateCessation(Date cessationDate, Long espaceId);
}
