/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.nomenclature;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Classe qui représente un domaine concerné par une action de lobbying
 *
 */
@Entity
@Table(name = "nomenclature_domaines_intervention")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "nomenclature_domaines_intervention_seq", allocationSize = 1)
public class DomaineInterventionEntity extends CommonNomenclatureParentEntity {

	private static final long serialVersionUID = 1542129862424452L;

	/**
	 * Default Constructor
	 */
	public DomaineInterventionEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public DomaineInterventionEntity(Long id) {
		super();
		this.setId(id);
	}

}
