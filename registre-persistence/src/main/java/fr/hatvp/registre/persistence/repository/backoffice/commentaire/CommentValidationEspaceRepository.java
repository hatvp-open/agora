/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice.commentaire;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.commentaire.CommentValidationEspaceEntity;

/**
 * 
 * Repository des commentaires de validation d'espace organisations.
 * @version $Revision$ $Date${0xD}
 */
public interface CommentValidationEspaceRepository
    extends JpaRepository<CommentValidationEspaceEntity, Long>
{
    /**
     *
     * @param inscriptionEspaceId id de l'inscription.
     * @return liste des commentaires de l'inscription à valider.
     */
    List<CommentValidationEspaceEntity> findByInscriptionEspaceEntityId(Long inscriptionEspaceId);
}
