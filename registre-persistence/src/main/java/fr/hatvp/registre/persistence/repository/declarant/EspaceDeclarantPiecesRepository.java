/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.declarant;

import fr.hatvp.registre.commons.lists.OrigineVersementEnum;
import fr.hatvp.registre.persistence.entity.piece.EspaceDeclarantPieceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.stream.Stream;

/**
 * Repository JPA pour l'entité {@link EspaceDeclarantPieceEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface EspaceDeclarantPiecesRepository
    extends JpaRepository<EspaceDeclarantPieceEntity, Long>
{
    /**
     * Renvoie la liste des pièces versées pour une origine donnée et un declarant donné.
     * @return liste des pièces sélectionnées
     */
    Stream<EspaceDeclarantPieceEntity> findByDeclarantIdAndOrigineVersementOrderByDateVersementPieceDesc(
        Long idDeclarant, OrigineVersementEnum origineVersement);

    /**
     * Renvoie la liste de toute les pièces pour un declarant donné.
     * @return liste des pièces sélectionnées
     */
    Stream<EspaceDeclarantPieceEntity> findByDeclarantIdOrderByDateVersementPieceDesc(
        Long idDeclarant);
}
