/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.persistence.entity.backoffice.GroupBoEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.SurveillanceEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.ArrayList;

public interface SurveillanceRepository extends JpaRepository<SurveillanceEntity, Long> {

    SurveillanceEntity findByOrganisationEntityId(long organisationId);

    ArrayList<SurveillanceEntity> findBysurveillantsGroupes(GroupBoEntity findById);

    SurveillanceEntity findByOrganisationEntity(OrganisationEntity organisation);

    Page<SurveillanceEntity> findByOrganisationEntityDenominationIgnoreCaseContaining(String denomination, Pageable pageable);
    @Query("SELECT s FROM SurveillanceEntity s WHERE s.organisationEntity.id in (select e.organisation.id from EspaceOrganisationEntity e where upper(e.nomUsage) like upper(?1))")
    Page<SurveillanceEntity> findByOrganisationEntityNomUsageIgnoreCaseContaining(String nomUsage, Pageable pageable);

    @Query(value = "SELECT * from surveillance as s where s.date_surveillance between ?1 and ?2 ORDER BY ?#{#pageable}",
        countQuery = "SELECT count(*) from surveillance as s where s.date_surveillance between ?1 and ?2 ORDER BY ?#{#pageable}",
        nativeQuery = true)
    Page<SurveillanceEntity> findByDateSurveillance(LocalDateTime of, LocalDateTime of1, Pageable pageable);

    @Query("SELECT s FROM SurveillanceEntity s WHERE s.organisationEntity.id in (select e.organisation.id from EspaceOrganisationEntity e where e.statut = ?1)")
    Page<SurveillanceEntity> findByOrganisationEntityStatut(StatutEspaceEnum statutEspaceEnum, Pageable pageable);

    Page<SurveillanceEntity> findByMotifIgnoreCaseContaining(String s, Pageable pageable);

    @Query("SELECT s FROM SurveillanceEntity s WHERE s.organisationEntity.id NOT in (select e.organisation.id from EspaceOrganisationEntity e)")
    Page <SurveillanceEntity> findByOrganisationEntityAndEspaceIsNull(Pageable pageable);


}
