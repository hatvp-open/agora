/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.nomenclature;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

@MappedSuperclass
public class CommonNomenclatureEntity extends AbstractCommonEntity {

	private static final long serialVersionUID = 568720075611418863L;

	@Column(name = "libelle", columnDefinition = "TEXT", updatable = false)
	private String libelle;

	/**
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * @param libelle
	 *            the libelle to set
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

}
