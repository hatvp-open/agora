/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.publication.batch.relance;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.hatvp.registre.commons.lists.PublicationRelanceNiveauEnum;
import fr.hatvp.registre.commons.lists.PublicationRelanceTypeEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;

@Entity
@Table(name = "publication_relance")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "publication_relance_seq", allocationSize = 1)
public class PublicationRelanceEntity extends AbstractCommonEntity {

	private static final long serialVersionUID = 7683083630158466578L;

	public PublicationRelanceEntity() {
	}

	public PublicationRelanceEntity(ExerciceComptableEntity exerciceComptable, DeclarantEntity contactOp, PublicationRelanceNiveauEnum niveau, PublicationRelanceTypeEnum type,
			boolean envoye, LocalDateTime creationDate) {
		this.exerciceComptable = exerciceComptable;
		this.contactOp = contactOp;
		this.niveau = niveau;
		this.type = type;
		this.envoye = envoye;
		this.creationDate = creationDate;
	}

	@Column(name = "date_creation", nullable = false, updatable = false)
	private LocalDateTime creationDate;

	@ManyToOne
	@JoinColumn(name = "exercice_comptable_id", nullable = false, updatable = false)
	private ExerciceComptableEntity exerciceComptable;

	@ManyToOne
	@JoinColumn(name = "declarant_id", nullable = false, updatable = false)
	private DeclarantEntity contactOp;

	/** Etat publication. */
	@Enumerated(EnumType.STRING)
	@Column(name = "niveau", nullable = false, updatable = false, columnDefinition = "TEXT")
	private PublicationRelanceNiveauEnum niveau;

	/** Etat publication. */
	@Enumerated(EnumType.STRING)
	@Column(name = "type", nullable = false, updatable = false, columnDefinition = "TEXT")
	private PublicationRelanceTypeEnum type;

	/** Status de la publication. */
	@Column(name = "envoye", nullable = false)
	private boolean envoye;

	@Column(name = "rejeu_date")
	private LocalDateTime rejeuDate;

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public ExerciceComptableEntity getExerciceComptable() {
		return exerciceComptable;
	}

	public void setExerciceComptable(ExerciceComptableEntity exerciceComptable) {
		this.exerciceComptable = exerciceComptable;
	}

	public DeclarantEntity getContactOp() {
		return contactOp;
	}

	public void setContactOp(DeclarantEntity contactOp) {
		this.contactOp = contactOp;
	}

	public PublicationRelanceNiveauEnum getNiveau() {
		return niveau;
	}

	public void setNiveau(PublicationRelanceNiveauEnum niveau) {
		this.niveau = niveau;
	}

	public PublicationRelanceTypeEnum getType() {
		return type;
	}

	public void setType(PublicationRelanceTypeEnum type) {
		this.type = type;
	}

	public boolean isEnvoye() {
		return envoye;
	}

	public void setEnvoye(boolean envoye) {
		this.envoye = envoye;
	}

	public LocalDateTime getRejeuDate() {
		return rejeuDate;
	}

	public void setRejeuDate(LocalDateTime rejeuDate) {
		this.rejeuDate = rejeuDate;
	}

}
