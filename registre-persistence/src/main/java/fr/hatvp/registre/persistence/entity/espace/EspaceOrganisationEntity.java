/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.backoffice.EspaceOrganisationLockBoEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

/**
 * Table qui enregistre l'espace organisation.
 * @version $Revision$ $Date${0xD}
 */
@Audited
@Entity
@Table(name = "espace_organisation")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "espace_organisation_seq", allocationSize = 1)
public class EspaceOrganisationEntity extends AbstractCommonEntity {

	/** Clé de sérialisation. */
	private static final long serialVersionUID = -908597443410461686L;

	/**
	 * Default Constructor
	 */
	public EspaceOrganisationEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public EspaceOrganisationEntity(Long id) {
		super();
		this.setId(id);
	}

	/** Date de création. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_creation", nullable = false, updatable = false)
	private Date creationDate = new Date();

	/** Statut création Espace. */
	@Column(name = "statut_creation_bo", nullable = false, columnDefinition = "TEXT")
	@Enumerated(EnumType.STRING)
	private StatutEspaceEnum statut = StatutEspaceEnum.NOUVEAU;

	/** Déclaration pour un tiers. */
	@Column(name = "non_declaration_tiers")
	private boolean nonDeclarationTiers = false;

	/** Déclaration pour un tiers. */
	@Column(name = "non_declaration_association")
	private boolean nonDeclarationOrgaAppartenance = false;

	/** Représentant légal. */
	@Column(name = "representant_legal", updatable = false)
	private boolean representantLegal = false;

	/** État d'activation de l'espace collaboratif. */
	@Column(name = "espace_actif", nullable = false)
	private boolean espaceActif = true;

	/** Référence vers l'organisation. */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organisation_id", nullable = false, updatable = false)
	private OrganisationEntity organisation;

	/** Nom d'usage de l'organisation associé à l'espace */
	@Column(name = "nom_usage", columnDefinition = "TEXT")
	private String nomUsage;

	/** Objet Lock BO Sur l'espace Organisation. */
	@NotAudited
	@OneToMany(mappedBy = "espaceOrganisation", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EspaceOrganisationLockBoEntity> espaceOrganisationLockBoEntityList;

	/** Créateur de l'espace organisation. */
	@NotAudited
	@ManyToOne
	@JoinColumn(name = "createur_espace_id", nullable = false)
	private DeclarantEntity createurEspaceOrganisation;

	/** Utilisateur BO ayant validé ou refusé la création de l'espace. */
	@NotAudited
	@ManyToOne
	@JoinColumn(name = "utilisateur_bo_action_creation_id")
	private UtilisateurBoEntity utilisateurActionCreation;

	/** Date action effectuée pour Validation/Rejet. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_action_creation")
	private Date dateActionCreation;

	/** Le logo de l'espace organisation. */
	@Column(name = "logo")
	@NotAudited
	private byte[] logo;

	/** Le type MIME du logo de l'espace organisation. */
	@Column(name = "logo_type", columnDefinition = "TEXT")
	private String logoType;

	/** Dirigeants. */
	@OneToMany(mappedBy = "espaceOrganisation", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<DirigeantEntity> dirigeants = new ArrayList<>();

	/**
	 * Organisations professionnelles ou syndicales et associations d'appartenance en lien avec la représentation d'intérêts.
	 */
	@OneToMany(mappedBy = "espaceOrganisation", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<AssociationEntity> organisationProfessionnelles = new ArrayList<>();

	/** Liste des collaborateurs de l'espace organisation. */
	@OneToMany(mappedBy = "espaceOrganisation", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<CollaborateurEntity> listeDesCollaborateurs = new ArrayList<>();

	/** Liste des clients de l'espace organisation. */
	@OneToMany(mappedBy = "espaceOrganisation", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ClientEntity> clients = new ArrayList<>();

	/** Liste des publications. */
	@NotAudited
	@OneToMany(mappedBy = "espaceOrganisation", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<PublicationEntity> publications = new ArrayList<>();

	/** Liste des exercices comptable */
	@OneToMany(mappedBy = "espaceOrganisation", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ExerciceComptableEntity> exercicesComptable = new ArrayList<>();

	/** Adresse. */
	@Column(name = "adresse", length = 500, columnDefinition = "TEXT")
	private String adresse;

	/** Code Postal. */
	@Column(name = "code_postal", columnDefinition = "TEXT")
	private String codePostal;

	/** Ville. */
	@Column(name = "ville", columnDefinition = "TEXT")
	private String ville;

	/** Pays. */
	@Column(name = "pays", columnDefinition = "TEXT")
	private String pays;

	
	/** blocage des relances de publications*/
	@Column(name = "relances_bloquees")
	private boolean relancesBloquees = false;
	
		/** publier ou pas mon adresse physique. */
	@Column(name = "non_publier_mon_adresse")
	private Boolean nonPublierMonAdressePhysique = Boolean.FALSE;

	/** Champ des activités. */
	@Column(name = "secteur_activite", length = 500, columnDefinition = "TEXT")
	private String secteursActivites;

	@Column(name = "niveau_intervention", length = 500, columnDefinition = "TEXT")
	private String niveauIntervention;

	/** Email de contact. */
	@Column(name = "email_de_contact", columnDefinition = "TEXT")
	private String emailDeContact;

	/** publier mon adresse email. */
	@Column(name = "non_publier_mon_email")
	private Boolean nonPublierMonAdresseEmail = Boolean.FALSE;

	/** Téléphone de contact. */
	@Column(name = "telephone_de_contact", columnDefinition = "TEXT")
	private String telephoneDeContact;

	/** publier ou pas le téléphone de contact. */
	@Column(name = "non_publier_mon_telephone")
	private Boolean nonPublierMonTelephoneDeContact = Boolean.FALSE;

	/** Téléphone de contact. */
	@Column(name = "website", columnDefinition = "TEXT")
	private String lienSiteWeb;

	/** lien page des tiers */
	@Column(name = "page_liste_tiers", columnDefinition = "TEXT")
	private String lienListeTiers;

	/** lien page tweeter. */
	@Column(name = "page_twitter", columnDefinition = "TEXT")
	private String lienPageTwitter;

	/** lien page facebook. */
	@Column(name = "page_facebook", columnDefinition = "TEXT")
	private String lienPageFacebook;

	/** lien page linkedin. */
	@Column(name = "page_linkedin", columnDefinition = "TEXT")
	private String lienPageLinkedin;

	/**
	 * Données financières:
	 */
	/** Catégorie de l'espace organisation */
	@Enumerated(EnumType.STRING)
	@Column(name = "categorie_organisation", columnDefinition = "TEXT")
	private TypeOrganisationEnum categorieOrganisation;

	@Column(name = "fin_exercice_fiscal")
	private String finExerciceFiscal;

	/** Chiffre d'Affaire. */
	@Column(name = "ca")
	private Long chiffreAffaire;

	/** Année Chiffre d'Affaire. */
	@Column(name = "annee_ca")
	private Integer anneeChiffreAffaire;

	/** Chiffre d'Affaire. */
	@Column(name = "ca_interet")
	private Long chiffreAffaireRepresentationInteret;

	/** Année Chiffre d'Affaire. */
	@Column(name = "annee_ca_intert")
	private Integer anneeChiffreAffaireRepresentationInteret;

	/** Budget total. */
	@Column(name = "budget")
	private Long budgetTotal;

	/** Année du budget. */
	@Column(name = "anneeBudget")
	private Integer anneeBudget;

	/** Montant des dépenses liées aux actions de lobbying pour l'année précédente. */
	@Column(name = "depenses")
	private Long depenses;

	/** Années des dépenses. */
	@Column(name = "anneeDepenses")
	private Integer anneeDepenses;

	/** Nombre de personnes employées dans l'activité de représentation d'intérêts. */
	@Column(name = "n_personnes_employees")
	private Integer nPersonnesEmployees;

	/** Ce flag indique si l'espace organisation a fait une nouvelle publication ou non. */
	@Column(name = "new_publication", nullable = false)
	private Boolean newPublication = Boolean.FALSE;

	/** Ce flag indique si l'espace doit être publié sur le site institutionnel. */
	@Column(name = "publication_enabled"/* , nullable = false */)
	private Boolean publicationEnabled = Boolean.TRUE;

	/** Ce flag indique si l'espace doit publier des nouveaux éléments (true des que des modifications ont lieu sans être publiées). */
	@Column(name = "elements_a_publier"/* , nullable = false */)
	private Boolean elementsAPublier = Boolean.FALSE;

	/** Existence d'une date de cloture d'exercice comptable */
	@Column(name = "non_exercice_comptable", nullable = true)
	private Boolean nonExerciceComptable = Boolean.FALSE;

	@Column(name = "sigle_hatvp", columnDefinition = "TEXT")
	private String sigleHatvp;
	
	@Column(name = "nom_usage_hatvp", columnDefinition = "TEXT")
	private String nomUsageHatvp;
	
	@Column(name = "ancien_nom_hatvp", columnDefinition = "TEXT")
	private String ancienNomHatvp;
	
	/** Date de création. */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_premiere_publication", nullable = true)
    private Date datePremierePublication;

	/**
	 * Accesseur en lecture du champ <code>creationDate</code>.
	 *
	 * @return le champ <code>creationDate</code>.
	 */
	public Date getCreationDate() {
		return this.creationDate;
	}

	/**
	 * Accesseur en écriture du champ <code>creationDate</code>.
	 *
	 * @param creationDate
	 *            la valeur à écrire dans <code>creationDate</code>.
	 */
	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Accesseur en lecture du champ <code>statut</code>.
	 *
	 * @return le champ <code>statut</code>.
	 */
	public StatutEspaceEnum getStatut() {
		return this.statut;
	}

	/**
	 * Accesseur en écriture du champ <code>statut</code>.
	 *
	 * @param statut
	 *            la valeur à écrire dans <code>statut</code>.
	 */
	public void setStatut(final StatutEspaceEnum statut) {
		this.statut = statut;
	}

	/**
	 * Accesseur en lecture du champ <code>nonDeclarationTiers</code>.
	 *
	 * @return le champ <code>nonDeclarationTiers</code>.
	 */
	public boolean isNonDeclarationTiers() {
		return this.nonDeclarationTiers;
	}

	/**
	 * Accesseur en écriture du champ <code>nonDeclarationTiers</code>.
	 *
	 * @param nonDeclarationTiers
	 *            la valeur à écrire dans <code>nonDeclarationTiers</code>.
	 */
	public void setNonDeclarationTiers(final boolean nonDeclarationTiers) {
		this.nonDeclarationTiers = nonDeclarationTiers;
	}

	/**
	 * Accesseur en lecture du champ <code>espaceActif</code>.
	 *
	 * @return le champ <code>espaceActif</code>.
	 */
	public boolean isEspaceActif() {
		return this.espaceActif;
	}

	/**
	 * Accesseur en écriture du champ <code>espaceActif</code>.
	 *
	 * @param espaceActif
	 *            la valeur à écrire dans <code>espaceActif</code>.
	 */
	public void setEspaceActif(final boolean espaceActif) {
		this.espaceActif = espaceActif;
	}

	/**
	 * Accesseur en lecture du champ <code>organisation</code>.
	 *
	 * @return le champ <code>organisation</code>.
	 */
	public OrganisationEntity getOrganisation() {
		return this.organisation;
	}

	/**
	 * Accesseur en écriture du champ <code>organisation</code>.
	 *
	 * @param organisation
	 *            la valeur à écrire dans <code>organisation</code>.
	 */
	public void setOrganisation(final OrganisationEntity organisation) {
		this.organisation = organisation;
	}

	/**
	 * Accesseur en lecture du champ <code>espaceOrganisationLockBoEntityList</code>.
	 *
	 * @return le champ <code>espaceOrganisationLockBoEntityList</code>.
	 */
	public List<EspaceOrganisationLockBoEntity> getEspaceOrganisationLockBoEntityList() {
		return this.espaceOrganisationLockBoEntityList;
	}

	/**
	 * Accesseur en écriture du champ <code>espaceOrganisationLockBoEntityList</code>.
	 *
	 * @param espaceOrganisationLockBoEntityList
	 *            la valeur à écrire dans <code>espaceOrganisationLockBoEntityList</code>.
	 */
	public void setEspaceOrganisationLockBoEntityList(final List<EspaceOrganisationLockBoEntity> espaceOrganisationLockBoEntityList) {
		this.espaceOrganisationLockBoEntityList = espaceOrganisationLockBoEntityList;
	}

	/**
	 * Accesseur en lecture du champ <code>createurEspaceOrganisation</code>.
	 *
	 * @return le champ <code>createurEspaceOrganisation</code>.
	 */
	public DeclarantEntity getCreateurEspaceOrganisation() {
		return this.createurEspaceOrganisation;
	}

	/**
	 * Accesseur en écriture du champ <code>createurEspaceOrganisation</code>.
	 *
	 * @param createurEspaceOrganisation
	 *            la valeur à écrire dans <code>createurEspaceOrganisation</code>.
	 */
	public void setCreateurEspaceOrganisation(final DeclarantEntity createurEspaceOrganisation) {
		this.createurEspaceOrganisation = createurEspaceOrganisation;
	}

	/**
	 * Accesseur en lecture du champ <code>utilisateurActionCreation</code>.
	 *
	 * @return le champ <code>utilisateurActionCreation</code>.
	 */
	public UtilisateurBoEntity getUtilisateurActionCreation() {
		return this.utilisateurActionCreation;
	}

	/**
	 * Accesseur en écriture du champ <code>utilisateurActionCreation</code>.
	 *
	 * @param utilisateurActionCreation
	 *            la valeur à écrire dans <code>utilisateurActionCreation</code>.
	 */
	public void setUtilisateurActionCreation(final UtilisateurBoEntity utilisateurActionCreation) {
		this.utilisateurActionCreation = utilisateurActionCreation;
	}

	/**
	 * Accesseur en lecture du champ <code>dateActionCreation</code>.
	 *
	 * @return le champ <code>dateActionCreation</code>.
	 */
	public Date getDateActionCreation() {
		return this.dateActionCreation;
	}

	/**
	 * Accesseur en écriture du champ <code>dateActionCreation</code>.
	 *
	 * @param dateActionCreation
	 *            la valeur à écrire dans <code>dateActionCreation</code>.
	 */
	public void setDateActionCreation(final Date dateActionCreation) {
		this.dateActionCreation = dateActionCreation;
	}

	/**
	 * la valeur à écrire dans <code>utilisateurModification</code>. Accesseur en lecture du champ <code>logo</code>.
	 *
	 * @return le champ <code>logo</code>.
	 */
	public byte[] getLogo() {
		return this.logo;
	}

	/**
	 * Accesseur en écriture du champ <code>logo</code>.
	 *
	 * @param logo
	 *            la valeur à écrire dans <code>logo</code>.
	 */
	public void setLogo(final byte[] logo) {
		this.logo = logo;
	}

	/**
	 * Accesseur en lecture du champ <code>logoType</code>.
	 *
	 * @return le champ <code>logoType</code>.
	 */
	public String getLogoType() {
		return this.logoType;
	}

	/**
	 * Accesseur en écriture du champ <code>logoType</code>.
	 *
	 * @param logoType
	 *            la valeur à écrire dans <code>logoType</code>.
	 */
	public void setLogoType(final String logoType) {
		this.logoType = logoType;
	}

	/**
	 * Accesseur en lecture du champ <code>dirigeants</code>.
	 *
	 * @return le champ <code>dirigeants</code>.
	 */
	public List<DirigeantEntity> getDirigeants() {
		return this.dirigeants;
	}

	/**
	 * Accesseur en écriture du champ <code>dirigeants</code>.
	 *
	 * @param dirigeants
	 *            la valeur à écrire dans <code>dirigeants</code>.
	 */
	public void setDirigeants(final List<DirigeantEntity> dirigeants) {
		this.dirigeants.clear();
		this.dirigeants.addAll(dirigeants);
	}

	/**
	 * Accesseur en lecture du champ <code>organisationProfessionnelles</code>.
	 *
	 * @return le champ <code>organisationProfessionnelles</code>.
	 */
	public List<AssociationEntity> getOrganisationProfessionnelles() {
		return this.organisationProfessionnelles;
	}

	/**
	 * Accesseur en écriture du champ <code>organisationProfessionnelles</code>.
	 *
	 * @param organisationProfessionnelles
	 *            la valeur à écrire dans <code>organisationProfessionnelles</code>.
	 */
	public void setOrganisationProfessionnelles(final List<AssociationEntity> organisationProfessionnelles) {
		this.organisationProfessionnelles = organisationProfessionnelles;
	}

	/**
	 * Accesseur en lecture du champ <code>adresse</code>.
	 *
	 * @return le champ <code>adresse</code>.
	 */
	public String getAdresse() {
		return this.adresse;
	}

	/**
	 * Accesseur en écriture du champ <code>adresse</code>.
	 *
	 * @param adresse
	 *            la valeur à écrire dans <code>adresse</code>.
	 */
	public void setAdresse(final String adresse) {
		this.adresse = adresse;
	}

	/**
	 * Accesseur en lecture du champ <code>codePostal</code>.
	 *
	 * @return le champ <code>codePostal</code>.
	 */
	public String getCodePostal() {
		return this.codePostal;
	}

	/**
	 * Accesseur en écriture du champ <code>codePostal</code>.
	 *
	 * @param codePostal
	 *            la valeur à écrire dans <code>codePostal</code>.
	 */
	public void setCodePostal(final String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * Accesseur en lecture du champ <code>ville</code>.
	 *
	 * @return le champ <code>ville</code>.
	 */
	public String getVille() {
		return this.ville;
	}

	/**
	 * Accesseur en écriture du champ <code>ville</code>.
	 *
	 * @param ville
	 *            la valeur à écrire dans <code>ville</code>.
	 */
	public void setVille(final String ville) {
		this.ville = ville;
	}

	/**
	 * Accesseur en lecture du champ <code>pays</code>.
	 *
	 * @return le champ <code>pays</code>.
	 */
	public String getPays() {
		return this.pays;
	}

	/**
	 * Accesseur en écriture du champ <code>pays</code>.
	 *
	 * @param pays
	 *            la valeur à écrire dans <code>pays</code>.
	 */
	public void setPays(final String pays) {
		this.pays = pays;
	}

	/**
	 * Accesseur en lecture du champ <code>nonPublierMonAdressePhysique</code>.
	 *
	 * @return le champ <code>nonPublierMonAdressePhysique</code>.
	 */
	public Boolean getNonPublierMonAdressePhysique() {
		return this.nonPublierMonAdressePhysique;
	}

	/**
	 * Accesseur en écriture du champ <code>nonPublierMonAdressePhysique</code>.
	 *
	 * @param nonPublierMonAdressePhysique
	 *            la valeur à écrire dans <code>nonPublierMonAdressePhysique</code>.
	 */
	public void setNonPublierMonAdressePhysique(final Boolean nonPublierMonAdressePhysique) {
		this.nonPublierMonAdressePhysique = nonPublierMonAdressePhysique;
	}

	/**
	 * Accesseur en lecture du champ <code>secteursActivites</code>.
	 *
	 * @return le champ <code>secteursActivites</code>.
	 */
	public String getSecteursActivites() {
		return this.secteursActivites;
	}

	/**
	 * Accesseur en écriture du champ <code>secteursActivites</code>.
	 *
	 * @param secteursActivites
	 *            la valeur à écrire dans <code>secteursActivites</code>.
	 */
	public void setSecteursActivites(final String secteursActivites) {
		this.secteursActivites = secteursActivites;
	}

	/**
	 * Accesseur en lecture du champ <code>niveauIntervention</code>.
	 *
	 * @return le champ <code>niveauIntervention</code>.
	 */
	public String getNiveauIntervention() {
		return this.niveauIntervention;
	}

	/**
	 * Accesseur en écriture du champ <code>niveauIntervention</code>.
	 *
	 * @param niveauIntervention
	 *            la valeur à écrire dans <code>niveauIntervention</code>.
	 */
	public void setNiveauIntervention(final String niveauIntervention) {
		this.niveauIntervention = niveauIntervention;
	}

	/**
	 * Accesseur en lecture du champ <code>emailDeContact</code>.
	 *
	 * @return le champ <code>emailDeContact</code>.
	 */
	public String getEmailDeContact() {
		return this.emailDeContact;
	}

	/**
	 * Accesseur en écriture du champ <code>emailDeContact</code>.
	 *
	 * @param emailDeContact
	 *            la valeur à écrire dans <code>emailDeContact</code>.
	 */
	public void setEmailDeContact(final String emailDeContact) {
		this.emailDeContact = emailDeContact;
	}

	/**
	 * Accesseur en lecture du champ <code>nonPublierMonAdresseEmail</code>.
	 *
	 * @return le champ <code>nonPublierMonAdresseEmail</code>.
	 */
	public Boolean getNonPublierMonAdresseEmail() {
		return this.nonPublierMonAdresseEmail;
	}

	/**
	 * Accesseur en écriture du champ <code>nonPublierMonAdresseEmail</code>.
	 *
	 * @param nonPublierMonAdresseEmail
	 *            la valeur à écrire dans <code>nonPublierMonAdresseEmail</code>.
	 */
	public void setNonPublierMonAdresseEmail(final Boolean nonPublierMonAdresseEmail) {
		this.nonPublierMonAdresseEmail = nonPublierMonAdresseEmail;
	}

	/**
	 * Accesseur en lecture du champ <code>telephoneDeContact</code>.
	 *
	 * @return le champ <code>telephoneDeContact</code>.
	 */
	public String getTelephoneDeContact() {
		return this.telephoneDeContact;
	}

	/**
	 * Accesseur en écriture du champ <code>telephoneDeContact</code>.
	 *
	 * @param telephoneDeContact
	 *            la valeur à écrire dans <code>telephoneDeContact</code>.
	 */
	public void setTelephoneDeContact(final String telephoneDeContact) {
		this.telephoneDeContact = telephoneDeContact;
	}

	/**
	 * Accesseur en lecture du champ <code>nonPublierMonTelephoneDeContact</code>.
	 *
	 * @return le champ <code>nonPublierMonTelephoneDeContact</code>.
	 */
	public Boolean getNonPublierMonTelephoneDeContact() {
		return this.nonPublierMonTelephoneDeContact;
	}

	/**
	 * Accesseur en écriture du champ <code>nonPublierMonTelephoneDeContact</code>.
	 *
	 * @param publierMonTelephoneDeContact
	 *            la valeur à écrire dans <code>nonPublierMonTelephoneDeContact</code>.
	 */
	public void setNonPublierMonTelephoneDeContact(final Boolean publierMonTelephoneDeContact) {
		this.nonPublierMonTelephoneDeContact = publierMonTelephoneDeContact;
	}

	/**
	 * Accesseur en lecture du champ <code>lienSiteWeb</code>.
	 *
	 * @return le champ <code>lienSiteWeb</code>.
	 */
	public String getLienSiteWeb() {
		return this.lienSiteWeb;
	}

	/**
	 * Accesseur en écriture du champ <code>lienSiteWeb</code>.
	 *
	 * @param lienSiteWeb
	 *            la valeur à écrire dans <code>lienSiteWeb</code>.
	 */
	public void setLienSiteWeb(final String lienSiteWeb) {
		this.lienSiteWeb = lienSiteWeb;
	}

	/**
	 * @return the lienListeTiers
	 */
	public String getLienListeTiers() {
		return lienListeTiers;
	}

	/**
	 * @param lienListeTiers
	 *            the lienListeTiers to set
	 */
	public void setLienListeTiers(String lienListeTiers) {
		this.lienListeTiers = lienListeTiers;
	}

	/**
	 * Accesseur en lecture du champ <code>lienPageTwitter</code>.
	 *
	 * @return le champ <code>lienPageTwitter</code>.
	 */
	public String getLienPageTwitter() {
		return this.lienPageTwitter;
	}

	/**
	 * Accesseur en écriture du champ <code>lienPageTwitter</code>.
	 *
	 * @param lienPageTwitter
	 *            la valeur à écrire dans <code>lienPageTwitter</code>.
	 */
	public void setLienPageTwitter(final String lienPageTwitter) {
		this.lienPageTwitter = lienPageTwitter;
	}

	/**
	 * Accesseur en lecture du champ <code>lienPageFacebook</code>.
	 *
	 * @return le champ <code>lienPageFacebook</code>.
	 */
	public String getLienPageFacebook() {
		return this.lienPageFacebook;
	}

	/**
	 * Accesseur en écriture du champ <code>lienPageFacebook</code>.
	 *
	 * @param lienPageFacebook
	 *            la valeur à écrire dans <code>lienPageFacebook</code>.
	 */
	public void setLienPageFacebook(final String lienPageFacebook) {
		this.lienPageFacebook = lienPageFacebook;
	}

	/**
	 * Accesseur en lecture du champ <code>lienPageLinkedin</code>.
	 *
	 * @return le champ <code>lienPageLinkedin</code>.
	 */
	public String getLienPageLinkedin() {
		return this.lienPageLinkedin;
	}

	/**
	 * Accesseur en écriture du champ <code>lienPageLinkedin</code>.
	 *
	 * @param lienPageLinkedin
	 *            la valeur à écrire dans <code>lienPageLinkedin</code>.
	 */
	public void setLienPageLinkedin(final String lienPageLinkedin) {
		this.lienPageLinkedin = lienPageLinkedin;
	}

	/**
	 * Accesseur en lecture du champ <code>categorieOrganisation</code>.
	 *
	 * @return le champ <code>categorieOrganisation</code>.
	 */
	public TypeOrganisationEnum getCategorieOrganisation() {
		return this.categorieOrganisation;
	}

	/**
	 * Accesseur en écriture du champ <code>categorieOrganisation</code>.
	 *
	 * @param categorieOrganisation
	 *            la valeur à écrire dans <code>categorieOrganisation</code>.
	 */
	public void setCategorieOrganisation(final TypeOrganisationEnum categorieOrganisation) {
		this.categorieOrganisation = categorieOrganisation;
	}

	/**
	 * @return the finExerciceFiscal
	 */
	public String getFinExerciceFiscal() {
		return finExerciceFiscal;
	}

	/**
	 * @param finExerciceFiscal
	 *            the finExerciceFiscal to set
	 */
	public void setFinExerciceFiscal(String finExerciceFiscal) {
		this.finExerciceFiscal = finExerciceFiscal;
	}

	/**
	 * Accesseur en lecture du champ <code>chiffreAffaire</code>.
	 *
	 * @return le champ <code>chiffreAffaire</code>.
	 */
	public Long getChiffreAffaire() {
		return this.chiffreAffaire;
	}

	/**
	 * Accesseur en écriture du champ <code>chiffreAffaire</code>.
	 *
	 * @param chiffreAffaire
	 *            la valeur à écrire dans <code>chiffreAffaire</code>.
	 */
	public void setChiffreAffaire(final Long chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	/**
	 * Accesseur en lecture du champ <code>anneeChiffreAffaire</code>.
	 *
	 * @return le champ <code>anneeChiffreAffaire</code>.
	 */
	public Integer getAnneeChiffreAffaire() {
		return this.anneeChiffreAffaire;
	}

	/**
	 * Accesseur en écriture du champ <code>anneeChiffreAffaire</code>.
	 *
	 * @param anneeChiffreAffaire
	 *            la valeur à écrire dans <code>anneeChiffreAffaire</code>.
	 */
	public void setAnneeChiffreAffaire(final Integer anneeChiffreAffaire) {
		this.anneeChiffreAffaire = anneeChiffreAffaire;
	}

	/**
	 * Accesseur en lecture du champ <code>chiffreAffaireRepresentationInteret</code>.
	 *
	 * @return le champ <code>chiffreAffaireRepresentationInteret</code>.
	 */
	public Long getChiffreAffaireRepresentationInteret() {
		return this.chiffreAffaireRepresentationInteret;
	}

	/**
	 * Accesseur en écriture du champ <code>chiffreAffaireRepresentationInteret</code>.
	 *
	 * @param chiffreAffaireRepresentationInteret
	 *            la valeur à écrire dans <code>chiffreAffaireRepresentationInteret</code>.
	 */
	public void setChiffreAffaireRepresentationInteret(final Long chiffreAffaireRepresentationInteret) {
		this.chiffreAffaireRepresentationInteret = chiffreAffaireRepresentationInteret;
	}

	/**
	 * Accesseur en lecture du champ <code>anneeChiffreAffaireInteret</code>.
	 *
	 * @return le champ <code>anneeChiffreAffaireInteret</code>.
	 */
	public Integer getAnneeChiffreAffaireRepresentationInteret() {
		return this.anneeChiffreAffaireRepresentationInteret;
	}

	/**
	 * Accesseur en écriture du champ <code>anneeChiffreAffaireInteret</code>.
	 *
	 * @param anneeChiffreAffaireInteret
	 *            la valeur à écrire dans <code>anneeChiffreAffaireInteret</code>.
	 */
	public void setAnneeChiffreAffaireRepresentationInteret(final Integer anneeChiffreAffaireInteret) {
		this.anneeChiffreAffaireRepresentationInteret = anneeChiffreAffaireInteret;
	}

	/**
	 * Accesseur en lecture du champ <code>budgetTotal</code>.
	 *
	 * @return le champ <code>budgetTotal</code>.
	 */
	public Long getBudgetTotal() {
		return this.budgetTotal;
	}

	/**
	 * Accesseur en écriture du champ <code>budgetTotal</code>.
	 *
	 * @param budgetTotal
	 *            la valeur à écrire dans <code>budgetTotal</code>.
	 */
	public void setBudgetTotal(final Long budgetTotal) {
		this.budgetTotal = budgetTotal;
	}

	/**
	 * Accesseur en lecture du champ <code>anneeBudget</code>.
	 *
	 * @return le champ <code>anneeBudget</code>.
	 */
	public Integer getAnneeBudget() {
		return this.anneeBudget;
	}

	/**
	 * Accesseur en écriture du champ <code>anneeBudget</code>.
	 *
	 * @param anneeBudget
	 *            la valeur à écrire dans <code>anneeBudget</code>.
	 */
	public void setAnneeBudget(final Integer anneeBudget) {
		this.anneeBudget = anneeBudget;
	}

	/**
	 * Accesseur en lecture du champ <code>depenses</code>.
	 *
	 * @return le champ <code>depenses</code>.
	 */
	public Long getDepenses() {
		return this.depenses;
	}

	/**
	 * Accesseur en écriture du champ <code>depenses</code>.
	 *
	 * @param depenses
	 *            la valeur à écrire dans <code>depenses</code>.
	 */
	public void setDepenses(final Long depenses) {
		this.depenses = depenses;
	}

	/**
	 * Accesseur en lecture du champ <code>anneeDepenses</code>.
	 *
	 * @return le champ <code>anneeDepenses</code>.
	 */
	public Integer getAnneeDepenses() {
		return this.anneeDepenses;
	}

	/**
	 * Accesseur en écriture du champ <code>anneeDepenses</code>.
	 *
	 * @param anneeDepenses
	 *            la valeur à écrire dans <code>anneeDepenses</code>.
	 */
	public void setAnneeDepenses(final Integer anneeDepenses) {
		this.anneeDepenses = anneeDepenses;
	}

	/**
	 * Accesseur en lecture du champ <code>nPersonnesEmployees</code>.
	 *
	 * @return le champ <code>nPersonnesEmployees</code>.
	 */
	public Integer getnPersonnesEmployees() {
		return this.nPersonnesEmployees;
	}

	/**
	 * Accesseur en écriture du champ <code>nPersonnesEmployees</code>.
	 *
	 * @param nPersonnesEmployees
	 *            la valeur à écrire dans <code>nPersonnesEmployees</code>.
	 */
	public void setnPersonnesEmployees(final Integer nPersonnesEmployees) {
		this.nPersonnesEmployees = nPersonnesEmployees;
	}

	/**
	 * Accesseur en lecture du champ <code>listeDesCollaborateurs</code>.
	 *
	 * @return le champ <code>listeDesCollaborateurs</code>.
	 */
	public List<CollaborateurEntity> getListeDesCollaborateurs() {
		return this.listeDesCollaborateurs;
	}

	/**
	 * Accesseur en écriture du champ <code>listeDesCollaborateurs</code>.
	 *
	 * @param listeDesCollaborateurs
	 *            la valeur à écrire dans <code>listeDesCollaborateurs</code>.
	 */
	public void setListeDesCollaborateurs(final List<CollaborateurEntity> listeDesCollaborateurs) {
		this.listeDesCollaborateurs = listeDesCollaborateurs;
	}

	/**
	 * Accesseur en lecture du champ <code>clients</code>.
	 *
	 * @return le champ <code>clients</code>.
	 */
	public List<ClientEntity> getClients() {
		return this.clients;
	}

	/**
	 * Accesseur en écriture du champ <code>clients</code>.
	 *
	 * @param clients
	 *            la valeur à écrire dans <code>clients</code>.
	 */
	public void setClients(final List<ClientEntity> clients) {
		this.clients = clients;
	}

	/**
	 * Accesseur en lecture du champ <code>publications</code>.
	 *
	 * @return le champ <code>publications</code>.
	 */
	public List<PublicationEntity> getPublications() {
		return this.publications;
	}

	/**
	 * Accesseur en écriture du champ <code>publications</code>.
	 *
	 * @param publications
	 *            la valeur à écrire dans <code>publications</code>.
	 */
	public void setPublications(final List<PublicationEntity> publications) {
		this.publications = publications;
	}

	/**
	 * @return the exercicesComptable
	 */
	public List<ExerciceComptableEntity> getExercicesComptable() {
		return exercicesComptable;
	}

	/**
	 * @param exercicesComptable
	 *            the exercicesComptable to set
	 */
	public void setExercicesComptable(final List<ExerciceComptableEntity> exercicesComptable) {
		this.exercicesComptable = exercicesComptable;
	}

	/**
	 * Accesseur en lecture du champ <code>hasNewPublication</code>.
	 *
	 * @return le champ <code>hasNewPublication</code>.
	 */
	public Boolean isNewPublication() {
		return this.newPublication;
	}

	/**
	 * Accesseur en lecture du champ <code>hasNewPublication</code>.
	 *
	 * @return le champ <code>hasNewPublication</code>.
	 */
	public Boolean getNewPublication() {
		return this.newPublication;
	}

	/**
	 * Accesseur en écriture du champ <code>hasNewPublication</code>.
	 *
	 * @param newPublication
	 *            la valeur à écrire dans <code>hasNewPublication</code>.
	 */
	public void setNewPublication(final Boolean newPublication) {
		this.newPublication = newPublication;
	}

	/**
	 * Accesseur en lecture du champ <code>nonDeclarationOrgaAppartenance</code>.
	 *
	 * @return le champ <code>nonDeclarationOrgaAppartenance</code>.
	 */
	public boolean isNonDeclarationOrgaAppartenance() {
		return this.nonDeclarationOrgaAppartenance;
	}

	/**
	 * Accesseur en écriture du champ <code>nonDeclarationOrgaAppartenance</code>.
	 *
	 * @param nonDeclarationOrgaAppartenance
	 *            la valeur à écrire dans <code>nonDeclarationOrgaAppartenance</code>.
	 */
	public void setNonDeclarationOrgaAppartenance(final boolean nonDeclarationOrgaAppartenance) {
		this.nonDeclarationOrgaAppartenance = nonDeclarationOrgaAppartenance;
	}

	/**
	 * Is publication boolean.
	 *
	 * @return the boolean
	 */
	public Boolean getPublicationEnabled() {
		return this.publicationEnabled;
	}

	/**
	 * Is publication boolean.
	 *
	 * @return the boolean
	 */
	public Boolean isPublicationEnabled() {
		return this.publicationEnabled;
	}

	/**
	 * Sets publication.
	 *
	 * @param publicationEnabled
	 *            the publication
	 */
	public void setPublicationEnabled(final Boolean publicationEnabled) {
		this.publicationEnabled = publicationEnabled;
	}

	/**
	 * Is representant legal boolean.
	 *
	 * @return the boolean
	 */
	public boolean isRepresentantLegal() {
		return this.representantLegal;
	}

	/**
	 * Sets representant legal.
	 *
	 * @param representantLegal
	 *            the representant legal
	 */
	public void setRepresentantLegal(final boolean representantLegal) {
		this.representantLegal = representantLegal;
	}

	/**
	 * Accesseur en lecture du champ <code>nomUsageHatvp</code>.
	 *
	 * @return le champ <code>nomUsage</code>.
	 */
	public String getNomUsageHatvp() {
		return this.nomUsageHatvp;
	}
	
	public String getAncienNomHatvp() {
		return this.ancienNomHatvp;
	}
	/**
	 * Accesseur en lecture du champ <code>sigleHatvp</code>.
	 *
	 * @return le champ <code>nomUsage</code>.
	 */
	public String getSigleHatvp() {
		return this.sigleHatvp;
	}
	/**
	 * Accesseur en lecture du champ <code>nomUsage</code>.
	 *
	 * @return le champ <code>nomUsage</code>.
	 */
	public String getNomUsage() {
		return this.nomUsage;
	}
	/**
	 * Accesseur en écriture du champ <code>nomUsage</code>.
	 *
	 * @param nomUsage
	 *            la valeur à écrire dans <code>nomUsage</code>.
	 */
	public void setNomUsage(final String nomUsage) {
		this.nomUsage = nomUsage;
	}

    /**
     * Accesseur en écriture du champ <code>nomUsageHatvp</code>.
     *
     * @param nomUsageHatvp
     *            la valeur à écrire dans <code>nomUsageHatvp</code>.
     */
    public void setNomUsageHatvp(final String nomUsageHatvp) {
        this.nomUsageHatvp = nomUsageHatvp;
    }


    public void setAncienNomHatvp(final String ancienNomHatvp) {
        this.ancienNomHatvp = ancienNomHatvp;
    }

    /**
     * Accesseur en écriture du champ <code>sigleHatvp</code>.
     *
     * @param sigleHatvp
     *            la valeur à écrire dans <code>sigleHatvp</code>.
     */
    public void setSigleHatvp(final String sigleHatvp) {
        this.sigleHatvp = sigleHatvp;
    }

	/**
	 * Accesseur en lecture du champ <code>nonExerciceComptable</code>.
	 *
	 * @return le champ <code>nonExerciceComptable</code>.
	 */
	public Boolean getNonExerciceComptable() {
		return this.nonExerciceComptable;
	}

	/**
	 * Accesseur en écriture du champ <code>nonExerciceComptable</code>.
	 *
	 *            la valeur à écrire dans <code>nonExerciceComptable</code>.
	 */
	public void setNonExerciceComptable(Boolean nonExerciceComptable) {
		this.nonExerciceComptable = nonExerciceComptable;
	}
	
	public Boolean getElementsAPublier() {
		return elementsAPublier;
	}

	public void setElementsAPublier(Boolean elementsAPublier) {
		this.elementsAPublier = elementsAPublier;
	}

	public boolean isRelancesBloquees() {
		return relancesBloquees;
	}

	public void setRelancesBloquees(boolean relancesBloquees) {
		this.relancesBloquees = relancesBloquees;
	}

	public Date getDatePremierePublication() {
		return datePremierePublication;
	}

	public void setDatePremierePublication(Date datePremierePublication) {
		this.datePremierePublication = datePremierePublication;
	}

}
