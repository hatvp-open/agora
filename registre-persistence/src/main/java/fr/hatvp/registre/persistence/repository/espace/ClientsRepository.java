/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.persistence.entity.espace.ClientEntity;

/**
 *
 * Repository JPA pour l'entité {@link ClientEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface ClientsRepository extends JpaRepository<ClientEntity, Long> {

	/**
	 * Récupérer clients par Id espace organisation
	 *
	 * @param espaceOrganisationId
	 *            id espace organisation
	 * @return liste des clients d'un espace organisation
	 */
	List<ClientEntity> findByEspaceOrganisationId(Long espaceOrganisationId);
	
	/**
	 * Récupère liste des clients d'un espace classé par ordre alphabétique de leur dénomination
	 * @param espaceOrganisationId
	 * @return
	 */
	List<ClientEntity> findByEspaceOrganisationIdOrderByOrganisationDenominationAsc(Long espaceOrganisationId);

	/**
	 * Récupérer une page des clients par Id espace organisation
	 *
	 * @param espaceOrganisationId
	 *            id de l'espace organisation
	 * @param pageable
	 *            paramètres de pagination
	 * @return une page des clients d'un espace organisation
	 */

	@Query("SELECT c FROM ClientEntity c WHERE espace_organisation_id = ?1 AND (ancien_client IS NULL OR ancien_client = false)")
	Page<ClientEntity> findByEspaceOrganisationIdAndIsAncienClientNotNullOrFalse(Long espaceOrganisationId, Pageable pageable);

	/**
	 * Récupérer clients par Id espace organisation et id organisation
	 *
	 * @param espaceOrganisationId
	 *            id de l'espace organisation
	 * @param organisationId
	 *            id de l'organisation
	 * @return clients pour un espace organisation et une organisation
	 */
	ClientEntity findByEspaceOrganisationIdAndOrganisationId(Long espaceOrganisationId, Long organisationId);

	/**
	 * Récupère un client s'il appartient à l'espace organisation passé en paramètre.
	 *
	 * @param clientId
	 * @param espaceOrganisationId
	 * @return le client de l'espace.
	 */
	ClientEntity findByIdAndEspaceOrganisationId(Long clientId, Long espaceOrganisationId);

	/**
	 * Récupérer une liste paginée des clients par Id espace organisation
	 *
	 * @param espaceOrganisationId
	 *            id espace organisation
	 * @return liste paginée des clients d'un espace organisation
	 */
	Page<ClientEntity> findByEspaceOrganisationId(Long currentEspaceOrganisationId, Pageable pageable);
	
	/**
	 * clients actif = client ayant une ligne periode_activite_client.date_desactivation = null
	 * @param espaceOrganisationId
	 * @param limit
	 * @param offset
	 * @return
	 */
	@Query(value="SELECT c.* FROM clients c "
			+ "inner join periode_activite_client p1 on p1.client_id=c.id "
			+ "inner join organisation o ON o.id = c.organisation_id "
			+ "WHERE c.espace_organisation_id = ?1 and p1.date_desactivation is null "
			+ "ORDER BY o.denomination ASC "
			+ " LIMIT ?2 OFFSET ?3", nativeQuery = true)
	List<ClientEntity> findClientsActifsByEspaceId(Long espaceOrganisationId, Integer limit, Integer offset);
	
	@Query(value="SELECT count(c.*) FROM clients c inner join periode_activite_client p1 on p1.client_id=c.id WHERE espace_organisation_id = ?1 and p1.date_desactivation is null ", nativeQuery = true)
	Long countClientsActifsByEspaceId(Long espaceOrganisationId);
	
	@Query(value="SELECT c.* FROM clients c "
			+ "inner join organisation o ON o.id = c.organisation_id "
			+ "left outer join (select * from periode_activite_client p1 where date_ajout=(select max(p2.date_ajout) from periode_activite_client p2 where p2.client_id=p1.client_id)) sel on sel.client_id=c.id "
			+ "WHERE c.espace_organisation_id = ?1 and sel.date_desactivation is not null "
			+ "ORDER BY o.denomination ASC "
			+ "LIMIT ?2 OFFSET ?3", nativeQuery = true)
	List<ClientEntity> findAnciensClientsByEspaceId(Long espaceOrganisationId, Integer limit, Integer offset);
	
	@Query(value="SELECT count(c.*) FROM clients c left outer join (select * from periode_activite_client p1 where date_ajout=(select max(p2.date_ajout) from periode_activite_client p2 where p2.client_id=p1.client_id)) sel on sel.client_id=c.id WHERE espace_organisation_id = ?1 and sel.date_desactivation is not null ", nativeQuery = true)
	Long countAnciensClientsByEspaceId(Long espaceOrganisationId);
	
	ClientEntity findById(Long clientId);
	

}
