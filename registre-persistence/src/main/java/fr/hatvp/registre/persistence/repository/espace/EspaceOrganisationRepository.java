/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.hatvp.registre.commons.lists.StatutEspaceEnum;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 *
 * Repository JPA pour l'entité {@link EspaceOrganisationEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface EspaceOrganisationRepository extends JpaRepository<EspaceOrganisationEntity, Long> {

	/**
	 * Récupérer la liste des entités selon le statut (BackOffice)
	 *
	 * @param statut
	 *            statut de l'espace organisation.
	 * @return liste des entités filtrées par statut
	 */
	Stream<EspaceOrganisationEntity> findByStatutOrderByIdAsc(StatutEspaceEnum statut);

	/**
	 * Récupérer une page de la liste des entités selon le statut (BackOffice)
	 *
	 * @param statut
	 *            statut de l'espace organisation.
	 * @param pageable
	 * @return liste des entités filtrées par statut
	 */
	Page<EspaceOrganisationEntity> findByStatutOrderByIdAsc(StatutEspaceEnum statut, Pageable pageable);

	/**
	 * Récupérer une liste paginée des espaces dont l'un des contact operationnel correspond a la recherche
	 *
	 * @param keyword
	 * @param limit
	 * @param offset
	 * @return liste paginée des espaces dont l'un des contact operationnel correspond a la recherche
	 */
	@Query(value = "SELECT * FROM espace_organisation AS esp " + "WHERE esp.id IN (" + "SELECT DISTINCT ins.espace_organisation_id " + "FROM inscription_espace AS ins "
			+ "LEFT JOIN roles AS rol ON rol.inscription_espace_id = ins.id " + "LEFT JOIN declarant AS dec ON dec.id = ins.declarant_id " + "WHERE rol.role = 'ADMINISTRATEUR' "
			+ "AND (dec.nom ILIKE ?1 OR dec.prenom ILIKE ?1)) " + "ORDER BY esp.id " + "LIMIT ?2 OFFSET ?3", nativeQuery = true)
	List<EspaceOrganisationEntity> findByContactOperationnel(String keyword, Integer limit, Integer offset);

	/**
	 * Récupérer une liste paginée des espaces dont l'un des contact operationnel ou bien le créateur de l'espace correspond a la recherche
	 *
	 * @param keyword
	 * @param limit
	 * @param offset
	 * @return liste paginée des espaces dont l'un des contact operationnel correspond a la recherche
	 */
	@Query(value = "SELECT * FROM espace_organisation AS esp " + "WHERE esp.id IN (" + "SELECT DISTINCT ins.espace_organisation_id FROM inscription_espace AS ins "
			+ "LEFT JOIN roles AS rol ON rol.inscription_espace_id = ins.id " + "LEFT JOIN declarant AS dec ON dec.id = ins.declarant_id " + "WHERE rol.role = 'ADMINISTRATEUR' "
			+ "AND (unaccent(dec.nom) ILIKE unaccent(?1) OR unaccent(dec.prenom) ILIKE unaccent(?1)) " + ") " + "OR esp.id IN ( " + "SELECT DISTINCT esp.id FROM espace_organisation AS e "
			+ "LEFT JOIN declarant AS dec ON dec.id = esp.createur_espace_id " + "WHERE (unaccent(dec.nom) ILIKE unaccent(?1) OR unaccent(dec.prenom) ILIKE unaccent(?1)) " + ") "
			+ "ORDER BY esp.id DESC " + "LIMIT ?2 OFFSET ?3 ", nativeQuery = true)
	List<EspaceOrganisationEntity> findByContactOperationnelOrCreateur(String keyword, Integer limit, Integer offset);

	/**
	 * Récupérer le nombre d'élément correspondant à la recherche
	 *
	 * @param keyword
	 * @return nombre d'élément correspondant à la recherche
	 */
	@Query(value = "SELECT COUNT(DISTINCT ins.espace_organisation_id) " + "FROM inscription_espace AS ins " + "LEFT JOIN roles AS rol ON rol.inscription_espace_id = ins.id "
			+ "LEFT JOIN declarant AS dec ON dec.id = ins.declarant_id " + "WHERE rol.role = 'ADMINISTRATEUR' " + "AND (dec.nom ILIKE ?1 OR dec.prenom ILIKE ?1)", nativeQuery = true)
	Integer countEspaceFilteredByContactOperationnel(String keyword);

	/**
	 * Récupérer le nombre d'élément correspondant à la recherche
	 *
	 * @param keyword
	 * @return nombre d'élément correspondant à la recherche
	 */
	@Query(value = "SELECT COUNT(DISTINCT esp.id) FROM espace_organisation AS esp " + "WHERE esp.id IN ( " + "SELECT DISTINCT ins.espace_organisation_id " + "FROM inscription_espace AS ins "
			+ "LEFT JOIN roles AS rol ON rol.inscription_espace_id = ins.id " + "LEFT JOIN declarant AS dec ON dec.id = ins.declarant_id " + "WHERE rol.role = 'ADMINISTRATEUR' "
			+ "AND (dec.nom ILIKE ?1 OR dec.prenom ILIKE ?1) " + ") " + "OR esp.id IN ( " + "SELECT DISTINCT esp.id " + "FROM espace_organisation AS e "
			+ "LEFT JOIN declarant AS dec ON dec.id = esp.createur_espace_id " + "WHERE (dec.nom ILIKE ?1 OR dec.prenom ILIKE ?1) " + ") ", nativeQuery = true)
	Integer countEspaceFilteredByContactOperationnelAndCreateur(String keyword);

	/**
	 * Récupérer la liste des entités en excluant 2 statuts (BackOffice)
	 *
	 * @param statut
	 *            statut de l'espace organisation.
	 * @param statut2
	 *            second statut à filtrer.
	 * @return liste des entités filtrées par statut
	 */
	Stream<EspaceOrganisationEntity> findByStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum statut, StatutEspaceEnum statut2);

	/**
	 * Récupérer la liste des entités en excluant 2 statuts et contenant le parametre dans sa denomination (BackOffice)
	 *
	 * @param denomination
	 *            denomination de l'espace
	 * @param statut
	 *            statut de l'espace organisation.
	 * @param statut2
	 *            second statut à filtrer.
	 * @param pageable
	 * @return une page des entités filtrées par statut
	 */
	Page<EspaceOrganisationEntity> findByOrganisationDenominationIgnoreCaseContainingAndStatutNotAndStatutNotOrderByIdAsc(String denomination, StatutEspaceEnum statut,
			StatutEspaceEnum statut2, Pageable pageable);

    /**
     * Recherche des exercices blacklisté par dénomination
     * @param keyword
     * @param pageable
     * @return
     */
	@Query(value = "select e from EspaceOrganisationEntity AS e, OrganisationEntity AS o where o.id = e.organisation.id and (upper(o.denomination) LIKE :keyword or upper(o.nomUsageSiren) LIKE :keyword) and e.id in (select ex.espaceOrganisation.id from ExerciceComptableEntity AS ex where ex.blacklisted = true and ex.dateFin in (select MAX(exe.dateFin) from ExerciceComptableEntity AS exe where exe.dateFin < current_date and exe.espaceOrganisation.id = ex.espaceOrganisation.id group by exe.espaceOrganisation.id))")
	Page<EspaceOrganisationEntity> findByBlacklistRechercheDenomination(@Param("keyword") String keyword, Pageable pageable);

    /**
     * Recherche des exercices blacklisté par identifiants
     * @param keyword
     * @param pageable
     * @return
     */
    @Query(value = "select e from EspaceOrganisationEntity AS e, OrganisationEntity AS o where o.id = e.organisation.id and o.siren LIKE :keyword and e.id in (select ex.espaceOrganisation.id from ExerciceComptableEntity AS ex where ex.blacklisted = true and ex.dateFin in (select MAX(exe.dateFin) from ExerciceComptableEntity AS exe where exe.dateFin < current_date and exe.espaceOrganisation.id = ex.espaceOrganisation.id group by exe.espaceOrganisation.id))")
    Page<EspaceOrganisationEntity> findByBlacklistRechercheIdentifiant(@Param("keyword") String keyword, Pageable pageable);


	/**
	 * Récupérer la liste des entités en excluant 2 statuts (BackOffice)
	 *
	 * @param statut
	 *            statut de l'espace organisation.
	 * @param statut2
	 *            second statut à filtrer.
	 * @param pageable
	 * @return une page des entités filtrées par statut
	 */
	Page<EspaceOrganisationEntity> findByStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum statut, StatutEspaceEnum statut2, Pageable pageable);

	/**
	 * Récupérer la liste des entités en excluant 2 statuts (BackOffice)
	 *
	 * @param statut
	 *            statut de l'espace organisation.
	 * @param statut2
	 *            second statut à filtrer.
	 * @param pageable
	 * @return une page des entités filtrées par statut
	 */
	Page<EspaceOrganisationEntity> findByStatutNotAndStatutNotOrderByIdDesc(StatutEspaceEnum statut, StatutEspaceEnum statut2, Pageable pageable);

    /**
     * Récupère la liste des espaces organisations blacklisté depuis la table publication relance
     *
     * @param pageable
     * @return une page des entité blacklisté
     */
    @Query(value="SELECT e FROM EspaceOrganisationEntity AS e WHERE e.id in (SELECT ex.espaceOrganisation.id from ExerciceComptableEntity ex where ex.blacklisted is true   and ex.dateFin in (select MAX(dateFin) from ExerciceComptableEntity exe where dateFin< current_date and exe.espaceOrganisation.id = ex.espaceOrganisation.id group by exe.espaceOrganisation.id))")
    Page<EspaceOrganisationEntity> findByExerciceBlacklistes(Pageable pageable);
    
    /**
     * Récupère la liste des espaces organisations blacklisté depuis la table publication relance (on ne recupere pas les espaces blacklistes que pour 2017)
     *
     * @param pageable
     * @return une page des entité blacklisté
     */
    @Query(value="SELECT e FROM EspaceOrganisationEntity AS e WHERE e.id in (SELECT ex.espaceOrganisation.id from ExerciceComptableEntity ex where ex.blacklisted is true   and ex.dateFin != :dateFin2017)")
    List<EspaceOrganisationEntity> findByExerciceBlacklistes(@Param("dateFin2017") LocalDate dateFin2017);

    /**
     * Récupère la liste des espaces organisations blacklisté depuis la table publication relance non paginé
     * @return une list des entitées blacklisté
     */
    @Query(value="SELECT e FROM EspaceOrganisationEntity AS e WHERE e.id in (SELECT ex.espaceOrganisation.id from ExerciceComptableEntity ex where ex.blacklisted is true   and ex.dateFin in (select MAX(dateFin) from ExerciceComptableEntity exe where dateFin< current_date and exe.espaceOrganisation.id = ex.espaceOrganisation.id group by exe.espaceOrganisation.id))")
    List<EspaceOrganisationEntity> findByExerciceBlacklistedNotPaginated();


	/**
	 * Récupérer la liste des entités en excluant 2 statuts (BackOffice)
	 *
	 * @param statut
	 *            statut de l'espace organisation.
	 * @param statut2
	 *            second statut à filtrer.
	 * @param pageable
	 * @return une page des entités filtrées par statut
	 */
	Page<EspaceOrganisationEntity> findByStatutNotInOrderByIdDesc(StatutEspaceEnum[] statuts, Pageable pageable);

	/**
	 * Détermine l'espace correspondant à l'organisation dont l'identifiant est passé en paramètre. La méthode exclut les espaces refusés. Il ne devrait jamais exister plus d'un espace
	 * non refusé pour une organisation donnée.
	 *
	 * @param organisationId
	 *            l'identifiant de l'organisation dont on recherche l'espace collaboratif.
	 * @return l'espace non refusé correspondant à l'organisation passée en paramètre (null si aucun trouvé).
	 */
	@Query("SELECT e FROM EspaceOrganisationEntity AS e " + "WHERE e.organisation.id = ?1 " + "AND e.statut != fr.hatvp.registre.commons.lists.StatutEspaceEnum.REFUSEE ORDER BY e.id ")
	EspaceOrganisationEntity findEspaceOrganisationByOrganisationIdAndStatutNotRefusee(Long organisationId);

    /**
     * Détermine l'espace oorrespondant à l'organisation dont le sirene est passé en parametre. La méthode exclu les espaces refusées
     * @param sirene
     * @return
     */
    @Query("SELECT e FROM EspaceOrganisationEntity AS e WHERE e.organisation.id = (SELECT o.id FROM OrganisationEntity AS o WHERE o.siren = ?1)")
    EspaceOrganisationEntity findEspaceOrganisationBySireneAndStatutNotRefusee(String sirene);

    /**
	 * Détermine la liste des espaces qui ont au moins une publication non dépubliée (statut != DEPUBLIEE) et dont la publication est active (publicationEnabled).
	 *
	 * @return la liste des identifiants des espaces concernés.
	 */
	@Query("SELECT e.id FROM EspaceOrganisationEntity AS e " + "WHERE e.publicationEnabled = true " + "AND EXISTS(" + "     SELECT p.id FROM PublicationEntity AS p "
			+ "     WHERE p.espaceOrganisation.id = e.id " + "     AND p.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE " + ")")
	List<Long> findIdEspacesPublies();

	/**
	 * Renvoie la liste des ids des espaces qui ont été validés et qui ne sont pas désinscrit
	 *
	 * @return la liste des identifiants des espaces concernés.
	 */
	@Query("SELECT distinct p.espaceOrganisation.id FROM PublicationEntity AS p "
			+ "WHERE p.espaceOrganisation.publicationEnabled = true "
			+ "and p.espaceOrganisation.statut != fr.hatvp.registre.commons.lists.StatutEspaceEnum.DESINSCRIT "
			+ "and p.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE ")
	List<Long> findIdEspacesValides();

	/**
	 * Récupérer la nombre des espaces non validés selon l'id du declarant créateur.
	 *
	 * @param id
	 *            id du déclarant créateur
	 * @return le nombre de demandes non validées pour le déclarant
	 */
	Long countByCreateurEspaceOrganisationIdAndDateActionCreationIsNull(Long declarantId);

	/**
	 * Détermine la liste des espaces marqués pour publication (newPublication ET publicationEnabled).
	 *
	 * @return la liste des espaces concernés.
	 */
	Stream<EspaceOrganisationEntity> findByNewPublicationTrueAndPublicationEnabledTrue();

	/**
	 * Détermine la liste des espaces marqués pour dépublication (newPublication ET NON publicationEnabled).
	 *
	 * @return la liste des espaces concernés.
	 */
	Stream<EspaceOrganisationEntity> findByNewPublicationTrueAndPublicationEnabledFalse();

	/**
	 * Récupérer la liste des entités en excluant 2 statuts et contenant le parametre dans sa denomination (BackOffice)
	 *
	 * @param denomination
	 *            denomination de l'espace
	 * @param statut
	 *            statut de l'espace organisation.
	 * @param statut2
	 *            second statut à filtrer.
	 * @param pageable
	 * @return une page des entités filtrées par statut
	 */
	Page<EspaceOrganisationEntity> findByCreateurEspaceOrganisationNomIgnoreCaseContainingAndStatutNotAndStatutNotOrderByIdAsc(String nom, StatutEspaceEnum statut, StatutEspaceEnum statut2,
			Pageable pageable);

	/**
	 * Récupérer la liste des entités en excluant 2 statuts et contenant le parametre de date de creation (BackOffice)
	 *
	 * @param dateBegin
	 *            date avec temps au début du jour 00:00:00
	 * @param dateEnd
	 *            date avec temps à la fin du jour 23:59:59
	 * @param statut
	 *            statut de l'espace organisation.
	 * @param statut2
	 *            second statut à filtrer.
	 * @param pageable
	 * @return une page des entités filtrées par statut
	 */
	Page<EspaceOrganisationEntity> findByCreationDateBetweenAndStatutNotAndStatutNotOrderByCreationDateAsc(Date dateBegin, Date dateEnd, StatutEspaceEnum statut, StatutEspaceEnum statut2,
			Pageable pageable);

	/**
	 * Récupérer la liste des entités en excluant 2 statuts et contenant le statut en parametre (BackOffice)
	 *
	 * @param denomination
	 *            denomination de l'espace
	 * @param statut
	 *            statut de l'espace organisation.
	 * @param statut2
	 *            second statut à filtrer.
	 * @param pageable
	 * @return une page des entités filtrées par statut
	 */
	Page<EspaceOrganisationEntity> findByStatutAndStatutNotAndStatutNotOrderByIdAsc(StatutEspaceEnum statutEspaceEnum, StatutEspaceEnum statut, StatutEspaceEnum statut2, Pageable pageable);

	/**
	 * Récupérer une page de la liste des entités selon le statut (BackOffice) et recherche par denomination
	 *
	 * @param denomination
	 *            denomination de l'espace
	 * @param statut
	 *            statut de l'espace organisation.
	 * @param pageable
	 * @return une page des entités filtrées par statut
	 */
	Page<EspaceOrganisationEntity> findByOrganisationDenominationIgnoreCaseContainingAndStatutOrderByIdAsc(String denomination, StatutEspaceEnum statut, Pageable pageable);

	/**
	 * Récupérer une page de la liste des entités selon le statut (BackOffice) et recherche par denomination
	 *
	 * @param siren
	 *            siren de l'espace
	 * @param rna
	 *            rna de l'espace
	 * @param dunsNumber
	 *            rna de l'espace
	 * @param hatvpNumber
	 *            rna de l'espace
	 * @param statut
	 *            statut de l'espace organisation.
	 * @param pageable
	 * @return une page des entités filtrées par statut
	 */
	Page<EspaceOrganisationEntity> findByStatutAndOrganisationSirenIgnoreCaseContainingOrStatutAndOrganisationRnaIgnoreCaseContainingOrStatutAndOrganisationDunsNumberIgnoreCaseContainingOrStatutAndOrganisationHatvpNumberIgnoreCaseContainingOrderByIdAsc(
			StatutEspaceEnum statut1, String siren, StatutEspaceEnum statut2, String rna, StatutEspaceEnum statut3, String dunsNumber, StatutEspaceEnum statut4, String hatvpNumber,
			Pageable pageable);

	/**
	 * Retourne une liste paginée des demandes d'ajout d'organisations par date
	 *
	 * @param date
	 * @param date2
	 * @param page
	 * @return liste paginée
	 */
	Page<EspaceOrganisationEntity> findByStatutAndCreationDateBetweenOrderByDateActionCreationAsc(StatutEspaceEnum statut, Date date, Date date2, Pageable pageable);

	List<EspaceOrganisationEntity> findByStatut(StatutEspaceEnum nouvelle);

	EspaceOrganisationEntity findByOrganisationId(Long id);

	Page<EspaceOrganisationEntity> findByStatutOrderByIdDesc(StatutEspaceEnum statutEspaceEnum, Pageable pageable);

	Page<EspaceOrganisationEntity> findByCreationDateBetweenOrderByCreationDateDesc(Date date, Date date2, Pageable pageable);

	Page<EspaceOrganisationEntity> findByDateActionCreationBetweenOrderByDateActionCreationDesc(Date date, Date date2, Pageable pageable);

	Page<EspaceOrganisationEntity> findByOrganisationSirenIgnoreCaseContainingOrOrganisationRnaIgnoreCaseContainingOrOrganisationHatvpNumberIgnoreCaseContainingOrderByIdDesc(String siren,
			String rna, String hatvp, Pageable pageable);

	Page<EspaceOrganisationEntity> findByOrganisationDenominationIgnoreCaseContainingOrderByIdDesc(String denomination, Pageable pageable);

	Page<EspaceOrganisationEntity> findByOrganisationDenominationIgnoreCaseContainingOrOrganisationNomUsageSirenIgnoreCaseContainingOrNomUsageIgnoreCaseContainingOrNomUsageHatvpIgnoreCaseContainingOrderByIdDesc(
			String denomination, String nomUsageOrg, String nomUsageEsp, String nomUsageHatvp, Pageable pageable);

	/**
	 * Récupérer une liste paginée des espaces dont la denomination ou l'un des noms d'usage correspond a la recherche
	 *
	 * @param keyword
	 * @param limit
	 * @param offset
	 * @return liste paginée des espaces dont l'un des contact operationnel correspond a la recherche
	 */
	@Query(value = "SELECT * " + "FROM espace_organisation " + "WHERE id IN (SELECT DISTINCT esp.id " + "FROM espace_organisation AS esp "
			+ "LEFT JOIN organisation AS org ON org.id = esp.organisation_id " + "WHERE unaccent(org.denomination) ILIKE unaccent(?1) " + "OR unaccent(org.nom_usage_siren) ILIKE unaccent(?1) "
			+ "OR unaccent(esp.nom_usage) ILIKE unaccent(?1) " + "OR unaccent(esp.nom_usage_hatvp) ILIKE unaccent(?1)) " + "ORDER BY id DESC LIMIT ?2 OFFSET ?3 ", nativeQuery = true)
	List<EspaceOrganisationEntity> findByDenominationOrNomUsage(String keyword, Integer limit, Integer offset);

	/**
	 * Récupérer le nombre d'élément correspondant à la recherche
	 *
	 * @param keyword
	 * @return nombre d'élément correspondant à la recherche
	 */
	@Query(value = "SELECT COUNT(DISTINCT esp.id) " + "FROM espace_organisation AS esp " + "LEFT JOIN organisation AS org ON org.id = esp.organisation_id "
			+ "WHERE unaccent(org.denomination) ILIKE unaccent(?1) " + "OR unaccent(org.nom_usage_siren) ILIKE unaccent(?1) " + "OR unaccent(esp.nom_usage) ILIKE unaccent(?1) "
			+ "OR unaccent(esp.nom_usage_hatvp) ILIKE unaccent(?1) ", nativeQuery = true)
	Integer countEspaceFilteredByDenominationOrNomUsage(String keyword);

	@Query("select e.statut from EspaceOrganisationEntity e where e.organisation.id = ?1")
	StatutEspaceEnum findStatutByOrganisationId(Long id);

	EspaceOrganisationEntity findById(Long espaceId);
	
	/**,; 
	 * Retourne la liste des espace orgnaisation sont le statut = DESINSCRIPTION_VALIDEE_HATVP
	 * et dont la date de cessation est inférieure ou égale à la date du jour
	 * @return
	 */
	@Query(value = "select e.* from espace_organisation e " + 
			"inner join desinscription d on d.espace_id=e.id " + 
			"where e.statut_creation_bo='DESINSCRIPTION_VALIDEE_HATVP' and d.date_cessation<=current_date", nativeQuery = true)
	List<EspaceOrganisationEntity> findEspaceOrganisationADesinscrire();
	
	/**
	 * retourne l'unique espace d'un organisation ou l'espace non refusé quand il y en a plusieurs
	 * @param organisationId
	 * @return
	 */
	@Query(value = "select * from espace_organisation "
			+ "inner join (select organisation_id, count(*) as nb from espace_organisation "
			+ "group by organisation_id) sel on sel.organisation_id=espace_organisation.organisation_id "
			+ "where (nb=1 or (nb>1 and espace_organisation.statut_creation_bo<>'REFUSEE')) and espace_organisation.organisation_id= ?1", nativeQuery = true)
	EspaceOrganisationEntity findByOrganisationIdUnSeulEspace(Long organisationId);

	Long countByStatut(StatutEspaceEnum statutEspace);
}
