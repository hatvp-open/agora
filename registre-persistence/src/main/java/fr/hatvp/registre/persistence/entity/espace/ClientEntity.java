/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.persistence.entity.espace.common.CommonClientEntity;

/**
 * 
 * Table qui enregistre les clients d'un espace organisation.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Audited
@Table(name = "clients")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "client_seq", allocationSize = 1)
public class ClientEntity extends CommonClientEntity{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -2013343332043719743L;    



}
