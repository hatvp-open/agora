/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.publication.activite.common;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.NotAudited;

import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;

/**
 * Entité des publications pour les activités.
 * @version $Revision$ $Date${0xD}
 */
@MappedSuperclass
public class PublicationEntity
    extends AbstractCommonEntity {

    /** Clé de sérialisation. */
    private static final long serialVersionUID = 628595254733158201L;

    /** Date de création. */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_creation", nullable = false, updatable = false)
    private Date creationDate = new Date();

    /** référence au publicateur. */
    @ManyToOne
    @JoinColumn(name = "publicateur_id", nullable = false)
    private DeclarantEntity publicateur;

    /** Date de dépublication. */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_depublication")
    private Date depublicationDate;

    /** référence au dépublicateur. */
    @ManyToOne
    @JoinColumn(name = "depublicateur_id")
    private UtilisateurBoEntity depublicateur;

    /** Date de republication. */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_republication")
    private Date republicationDate;

    /** référence au republicateur. */
    @ManyToOne
    @JoinColumn(name = "republicateur_id")
    private UtilisateurBoEntity republicateur;

    /** Etat publication. */
    @Enumerated(EnumType.STRING)
    @Column(name = "statut", nullable = false, columnDefinition = "TEXT")
    private StatutPublicationEnum statut = StatutPublicationEnum.PUBLIEE;

    /** Status de la publication. */
    @Column(name = "supprime")
    private Boolean deleted;

    /** La grappe de publication en format json */
    @NotAudited
    @Column(name = "pub_json", columnDefinition = "TEXT", nullable = false, updatable = false)
    private String publicationJson;

    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public DeclarantEntity getPublicateur()
    {
        return publicateur;
    }

    public void setPublicateur(DeclarantEntity publicateur)
    {
        this.publicateur = publicateur;
    }

    public Date getDepublicationDate()
    {
        return depublicationDate;
    }

    public void setDepublicationDate(Date depublicationDate)
    {
        this.depublicationDate = depublicationDate;
    }

    public UtilisateurBoEntity getDepublicateur()
    {
        return depublicateur;
    }

    public void setDepublicateur(UtilisateurBoEntity depublicateur)
    {
        this.depublicateur = depublicateur;
    }

    public Date getRepublicationDate()
    {
        return republicationDate;
    }

    public void setRepublicationDate(Date republicationDate)
    {
        this.republicationDate = republicationDate;
    }

    public UtilisateurBoEntity getRepublicateur()
    {
        return republicateur;
    }

    public void setRepublicateur(UtilisateurBoEntity republicateur)
    {
        this.republicateur = republicateur;
    }

    public StatutPublicationEnum getStatut()
    {
        return statut;
    }

    public void setStatut(StatutPublicationEnum statut)
    {
        this.statut = statut;
    }

    public Boolean getDeleted()
    {
        return deleted;
    }

    public void setDeleted(Boolean deleted)
    {
        this.deleted = deleted;
    }

    public String getPublicationJson()
    {
        return publicationJson;
    }

    public void setPublicationJson(String publicationJson)
    {
        this.publicationJson = publicationJson;
    }
}