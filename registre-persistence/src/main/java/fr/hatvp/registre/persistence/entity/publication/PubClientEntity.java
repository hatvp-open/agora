/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.publication;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import fr.hatvp.registre.persistence.entity.espace.common.CommonClientEntity;

/**
 * Entité des clients publiés.
 * @version $Revision$ $Date${0xD}
 */
@Audited
@Entity
@Table(name = "pub_client")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "pub_client_seq",
        allocationSize = 1)
public class PubClientEntity
    extends CommonClientEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -9170418015717774045L;

    /** Référence vers la publicationEntity. */
    @ManyToOne
    private PublicationEntity publication;
    
	@Column(name = "ancien_client")
	private Boolean isAncienClient = Boolean.FALSE;


    @Column(name="date_ajout")
    private LocalDateTime dateAjout;


    @Column(name = "date_desactivation")
    private LocalDateTime dateDesactivation;
    
    @Column(name = "date_fin_contrat")
	private LocalDate dateFinContrat;

    @Column(name = "commentaire")
    private String commentaire;

    /**
     * Accesseur en lecture du champ <code>publicationEntity</code>.
     * @return le champ <code>publicationEntity</code>.
     */
    public PublicationEntity getPublication()
    {
        return this.publication;
    }

    /**
     * Accesseur en écriture du champ <code>publicationEntity</code>.
     * @param publicationEntity la valeur à écrire dans <code>publicationEntity</code>.
     */
    public void setPublication(final PublicationEntity publicationEntity)
    {
        this.publication = publicationEntity;
    }

	public Boolean getIsAncienClient() {
		return isAncienClient;
	}

	public void setIsAncienClient(Boolean isAncienClient) {
		this.isAncienClient = isAncienClient;
	}

	public LocalDateTime getDateAjout() {
		return dateAjout;
	}

	public void setDateAjout(LocalDateTime dateAjout) {
		this.dateAjout = dateAjout;
	}

	public LocalDateTime getDateDesactivation() {
		return dateDesactivation;
	}

	public void setDateDesactivation(LocalDateTime dateDesactivation) {
		this.dateDesactivation = dateDesactivation;
	}

	public LocalDate getDateFinContrat() {
		return dateFinContrat;
	}

	public void setDateFinContrat(LocalDate dateFinContrat) {
		this.dateFinContrat = dateFinContrat;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
    
    

}
