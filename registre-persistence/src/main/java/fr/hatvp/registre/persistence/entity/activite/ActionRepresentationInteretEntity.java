/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.activite;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ActionMeneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.DecisionConcerneeEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ResponsablePublicEntity;

@Entity
@Audited
@Table(name = "action_representation_interet")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "action_representation_interet_seq", allocationSize = 1)
public class ActionRepresentationInteretEntity extends AbstractCommonEntity {

	private static final long serialVersionUID = -6320748543346576687L;



	/**
	 * Default Constructor
	 */
	public ActionRepresentationInteretEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public ActionRepresentationInteretEntity(Long id) {
		super();
		this.setId(id);
	}

	@ManyToMany
	@JoinTable(name = "action_representation_responsable_public")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private Set<ResponsablePublicEntity> reponsablesPublics = new HashSet<>();

	@ManyToMany
	@JoinTable(name = "action_representation_decision_concernee")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private Set<DecisionConcerneeEntity> decisionsConcernees = new HashSet<>();

	@ManyToMany
	@JoinTable(name = "action_representation_action_menee")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private Set<ActionMeneeEntity> actionsMenees = new HashSet<>();

	@Column(name = "action_menee_autre", columnDefinition = "TEXT")
	private String actionMeneeAutre;

	@Column(name = "responsable_public_autre", columnDefinition = "TEXT")
	private String responsablePublicAutre;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "action_representation_organisation")
	@JoinColumn(name = "organisation_id")
	private List<OrganisationEntity> tiers = new ArrayList<>();

	@Column(name = "observation", columnDefinition = "TEXT")
	private String observation;

	@ManyToOne
	@JoinColumn(name = "activite_representation_interet_id", nullable = false)
	private ActiviteRepresentationInteretEntity activiteRepresentationInteret;

	/**
	 * @return the reponsablesPublics
	 */
	public Set<ResponsablePublicEntity> getReponsablesPublics() {
		return reponsablesPublics;
	}

	/**
	 * @param reponsablesPublics
	 *            the reponsablesPublics to set
	 */
	public void setReponsablesPublics(Set<ResponsablePublicEntity> reponsablesPublics) {
		this.reponsablesPublics = reponsablesPublics;
	}

	/**
	 * @return the decisionsConcernees
	 */
	public Set<DecisionConcerneeEntity> getDecisionsConcernees() {
		return decisionsConcernees;
	}

	/**
	 * @param decisionsConcernees
	 *            the decisionsConcernees to set
	 */
	public void setDecisionsConcernees(Set<DecisionConcerneeEntity> decisionsConcernees) {
		this.decisionsConcernees = decisionsConcernees;
	}

	/**
	 * @return the actionsMenees
	 */
	public Set<ActionMeneeEntity> getActionsMenees() {
		return actionsMenees;
	}

	/**
	 * @param actionsMenees
	 *            the actionsMenees to set
	 */
	public void setActionsMenees(Set<ActionMeneeEntity> actionsMenees) {
		this.actionsMenees = actionsMenees;
	}

	/**
	 * @return the actionMeneeAutre
	 */
	public String getActionMeneeAutre() {
		return actionMeneeAutre;
	}

	/**
	 * @param actionMeneeAutre
	 *            the actionMeneeAutre to set
	 */
	public void setActionMeneeAutre(String actionMeneeAutre) {
		this.actionMeneeAutre = actionMeneeAutre;
	}

	/**
	 * @return the tiers
	 */
	public List<OrganisationEntity> getTiers() {
		return tiers;
	}

	/**
	 * @param tiers
	 *            the tiers to set
	 */
	public void setTiers(List<OrganisationEntity> tiers) {
		this.tiers = tiers;
	}

	/**
	 * @return the observation
	 */
	public String getObservation() {
		return observation;
	}

	/**
	 * @param observation
	 *            the observation to set
	 */
	public void setObservation(String observation) {
		this.observation = observation;
	}

	/**
	 * @return the activiteRepresentationInteret
	 */
	public ActiviteRepresentationInteretEntity getActiviteRepresentationInteret() {
		return activiteRepresentationInteret;
	}

	/**
	 * @param activiteRepresentationInteret
	 *            the activiteRepresentationInteret to set
	 */
	public void setActiviteRepresentationInteret(ActiviteRepresentationInteretEntity activiteRepresentationInteret) {
		this.activiteRepresentationInteret = activiteRepresentationInteret;
	}

	/**
	 * 
	 * @return responsablePublicAutre
	 */
	public String getResponsablePublicAutre() {
		return responsablePublicAutre;
	}

	/**
	 * @param responsablePublicAutre
	 *            the responsablePublicAutre to set
	 */
	public void setResponsablePublicAutre(String responsablePublicAutre) {
		this.responsablePublicAutre = responsablePublicAutre;
	}

}