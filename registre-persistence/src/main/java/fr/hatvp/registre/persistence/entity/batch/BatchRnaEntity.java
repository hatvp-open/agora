/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.batch;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * Classe de gestion de l'historique des mises à jour du référentiel Sirene.
 * @version $Revision$ $Date$.
 */
@Entity
@Table(name = "batch_rna_history")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "batch_rna_history_seq",
    allocationSize = 1)
public class BatchRnaEntity extends AbstractBatchEntity {

    /**
     * Identifiant de sérialization
     */
    private static final long serialVersionUID = -3284248411006577716L;

    /**
     * Numéro de séquence du fichier téléchargé au format YYYYDDD.
     */
    @Column(name = "sequence_fichier", nullable = false, columnDefinition = "TEXT")
    private String sequenceFichier;

    public String getSequenceFichier() {
        return this.sequenceFichier;
    }

    public void setSequenceFichier(final String sequenceFichier) {
        this.sequenceFichier = sequenceFichier;
    }

}

