/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.backoffice.DemandeOrganisationLockBoEntity;

/**
 *
 * Repository de l'entite
 * {@link fr.hatvp.registre.persistence.entity.backoffice.DemandeOrganisationLockBoEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface DemandeOrganisationLockBoRepository
    extends JpaRepository<DemandeOrganisationLockBoEntity, Long>
{
    /**
     * Trouver un Lock par Demande Organisation
     * @param id identifiant demande
     * @return les locks sur la demande
     */
    List<DemandeOrganisationLockBoEntity> findByDemandeOrganisationId(Long id);

}
