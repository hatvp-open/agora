/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.activite;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.activite.ActionRepresentationInteretEntity;

public interface ActionRepresentationInteretRepository extends JpaRepository<ActionRepresentationInteretEntity, Long> {

}
