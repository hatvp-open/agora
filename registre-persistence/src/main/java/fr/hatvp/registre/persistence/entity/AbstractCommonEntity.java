/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * Classe commune de gestion des entités.
 * @version $Revision$ $Date$.
 */
@MappedSuperclass
public abstract class AbstractCommonEntity
    implements Serializable
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -6893399491928930624L;

    /** Identifiant technique de l'objet. */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_sequence_generator")
    @Column(name = "id")
    private Long id;

    /** Champ version. */
    @Version
    @Column(name = "version")
    private Integer version = -1;

    /**
     * Constante pour la taille 512 des champs string.
     */
    public static final int SIZE_512 = 512;

    /**
     * Constante pour la taille max des champs string.
     */
    public static final int SIZE_STR_MAX = 255;

    /**
     * Constante pour la taille 128 des champs string.
     */
    public static final int SIZE_128 = 128;

    /**
     * Constante pour la taille 32 des champs string.
     */
    public static final int SIZE_32 = 32;

    /**
     * Constante pour la taille 32 des champs string.
     */
    public static final int SIZE_64 = 64;

    /**
     * Constante pour la taille 8096 des champs string.
     */
    public static final int SIZE_MEMO = 8096;

    /**
     * Constante représentant le pattern de date.
     */
    public static final String DATE_PATTERN = "dd/MM/yyyy";

    /**
     * {@inheritDoc}.
     */
    @Override
    public String toString()
    {
        return this.getClass().getName() + ": id: " + this.id + " version: " + this.version;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.id == null) ? 0 : this.id.hashCode());
        result = (prime * result) + ((this.version == null) ? 0 : this.version.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof AbstractCommonEntity)) {
            return false;
        }
        final AbstractCommonEntity other = (AbstractCommonEntity) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        }
        else if (!this.id.equals(other.id)) {
            return false;
        }
        if (this.version == null) {
            if (other.version != null) {
                return false;
            }
        }
        else if (!this.version.equals(other.version)) {
            return false;
        }
        return true;
    }

    /**
     * Getter pour id.
     * @return Le id
     */
    public Long getId()
    {
        return this.id;
    }

    /**
     * Setter pour id.
     * @param id Le id à écrire.
     */
    public void setId(final Long id)
    {
        this.id = id;
    }

    /**
     * Getter pour version.
     * @return Le version
     */
    public Integer getVersion()
    {
        return this.version;
    }

    /**
     * Setter pour version.
     * @param version Le version à écrire.
     */
    public void setVersion(final Integer version)
    {
        this.version = version;
    }
}
