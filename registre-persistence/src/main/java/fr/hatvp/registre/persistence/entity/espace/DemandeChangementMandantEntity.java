

/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import fr.hatvp.registre.commons.lists.ChangementContactEnum;
import fr.hatvp.registre.commons.lists.StatutDemandeEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.piece.EspaceOrganisationPieceEntity;

/**
 *
 * Table qui enregistre les inscriptions à un espace organisation.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "demande_changement_mandant")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "inscription_espace_seq", allocationSize = 1)
public class DemandeChangementMandantEntity extends AbstractCommonEntity {

	/** Clé de sérialisation. */
	private static final long serialVersionUID = 1667821158463232101L;

	/** Statut création Espace. */
	@Column(name = "statut", nullable = false, columnDefinition = "TEXT")
	@Enumerated(EnumType.STRING)
	private StatutDemandeEnum statut = StatutDemandeEnum.NOUVELLE;

	/** Date de la demande. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_demande", nullable = false, updatable = false)
	private Date dateDemande = new Date();

	/** Date de validation de la demande. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_validation")
	private Date dateValidation;

	/** Date de rejet. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_rejet", updatable = true)
	private Date dateRejet;

	/** Si representant Legal. */
	@Column(name = "is_representant_legal", nullable = false)
	private Boolean representantLegal = false;

	/**
	 * Catégorie de demande
	 *
	 * @see ChangementContactEnum AJOUT_OPERATIONNEL("Ajout d'un nouveau contact opérationnel"), REMPLACEMENT_OPERATIONNEL("Remplacement d'un nouveau contact opérationnel"),
	 *      CHANGEMENT_REPRESENTANT
	 */
	@Enumerated(EnumType.STRING)
	@Column(name = "categorie_demande", nullable = false)
	private ChangementContactEnum typeDemande;

	/** motif de la demande */
	@Column(name = "motif_demande")
	private String motifDemande;

	/** Référence vers la piece d'identite du representant legal */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "identite_e_o_piece_id", updatable = false)
	private EspaceOrganisationPieceEntity pieceIdentiteRepresentantLegal;

	/** Référence vers le déclarant auteur de la demande */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "declarant_id", nullable = false, updatable = false)
	private DeclarantEntity declarant;

	/** Référence vers le mandat du representant legal */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mandat_e_o_piece_id", nullable = false, updatable = false)
	private EspaceOrganisationPieceEntity mandatRepresentantLegal;

	/** Référence vers les pieces complémentaires */
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
	@JoinColumn(name = "demande_changement_mandant_id")
	private List<EspaceOrganisationPieceEntity> complementPieceRepresentantLegal = new ArrayList<>();

	/** Espace Organisation. */
	@ManyToOne
	@JoinColumn(name = "espace_organisation_id", updatable = false)
	private EspaceOrganisationEntity espaceOrganisation;

	/** Référence vers l'inscription du déclarant à promouvoir. */
	@ManyToOne
	@JoinColumn(name = "declarant_dechu_id", nullable = true)
	private InscriptionEspaceEntity declarantDechu;

	/** Référence vers l'inscription du déclarant à promouvoir. */
	@ManyToOne
	@JoinColumn(name = "declarant_promu_id", nullable = true)
	private InscriptionEspaceEntity declarantPromu;

	/**
	 * Default Constructor
	 */
	public DemandeChangementMandantEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public DemandeChangementMandantEntity(Long id) {

		super();
		this.setId(id);
	}

	/**
	 * @return the statut
	 */
	public StatutDemandeEnum getStatut() {
		return statut;
	}

	/**
	 * @param statut
	 *            the statut to set
	 */
	public void setStatut(StatutDemandeEnum statut) {
		this.statut = statut;
	}

	/**
	 * @return the dateDemande
	 */
	public Date getDateDemande() {
		return dateDemande;
	}

	/**
	 * @param dateDemande
	 *            the dateDemande to set
	 */
	public void setDateDemande(Date dateDemande) {
		this.dateDemande = dateDemande;
	}

	/**
	 * @return the dateValidation
	 */
	public Date getDateValidation() {
		return dateValidation;
	}

	/**
	 * @param dateValidation
	 *            the dateValidation to set
	 */
	public void setDateValidation(Date dateValidation) {
		this.dateValidation = dateValidation;
	}

	/**
	 * @return the dateRejet
	 */
	public Date getDateRejet() {
		return dateRejet;
	}

	/**
	 * @param dateRejet
	 *            the dateRejet to set
	 */
	public void setDateRejet(Date dateRejet) {
		this.dateRejet = dateRejet;
	}

	/**
	 * @return the representantLegal
	 */
	public Boolean getRepresentantLegal() {
		return representantLegal;
	}

	/**
	 * @param representantLegal
	 *            the representantLegal to set
	 */
	public void setRepresentantLegal(Boolean representantLegal) {
		this.representantLegal = representantLegal;
	}

	/**
	 * @return the typeDemande
	 */
	public ChangementContactEnum getTypeDemande() {
		return typeDemande;
	}

	/**
	 * @param typeDemande
	 *            the typeDemande to set
	 */
	public void setTypeDemande(ChangementContactEnum typeDemande) {
		this.typeDemande = typeDemande;
	}

	/**
	 * @return the motifDemande
	 */
	public String getMotifDemande() {
		return motifDemande;
	}

	/**
	 * @param motifDemande
	 *            the motifDemande to set
	 */
	public void setMotifDemande(String motifDemande) {
		this.motifDemande = motifDemande;
	}

	/**
	 * @return the pieceIdentiteRepresentantLegal
	 */
	public EspaceOrganisationPieceEntity getPieceIdentiteRepresentantLegal() {
		return pieceIdentiteRepresentantLegal;
	}

	/**
	 * @param pieceIdentiteRepresentantLegal
	 *            the pieceIdentiteRepresentantLegal to set
	 */
	public void setPieceIdentiteRepresentantLegal(EspaceOrganisationPieceEntity pieceIdentiteRepresentantLegal) {
		this.pieceIdentiteRepresentantLegal = pieceIdentiteRepresentantLegal;
	}

	/**
	 * @return the declarant
	 */
	public DeclarantEntity getDeclarant() {
		return declarant;
	}

	/**
	 * @param declarant
	 *            the declarant to set
	 */
	public void setDeclarant(DeclarantEntity declarant) {
		this.declarant = declarant;
	}

	/**
	 * @return the mandatRepresentantLegal
	 */
	public EspaceOrganisationPieceEntity getMandatRepresentantLegal() {
		return mandatRepresentantLegal;
	}

	/**
	 * @param mandatRepresentantLegal
	 *            the mandatRepresentantLegal to set
	 */
	public void setMandatRepresentantLegal(EspaceOrganisationPieceEntity mandatRepresentantLegal) {
		this.mandatRepresentantLegal = mandatRepresentantLegal;
	}

	/**
	 * @return the complementPieceRepresentantLegal
	 */
	public List<EspaceOrganisationPieceEntity> getComplementPieceRepresentantLegal() {
		return complementPieceRepresentantLegal;
	}

	/**
	 * @param complementPieceRepresentantLegal
	 *            the complementPieceRepresentantLegal to set
	 */
	public void setComplementPieceRepresentantLegal(List<EspaceOrganisationPieceEntity> complementPieceRepresentantLegal) {
		this.complementPieceRepresentantLegal = complementPieceRepresentantLegal;
	}

	/**
	 * @return the espaceOrganisation
	 */
	public EspaceOrganisationEntity getEspaceOrganisation() {
		return espaceOrganisation;
	}

	/**
	 * @param espaceOrganisation
	 *            the espaceOrganisation to set
	 */
	public void setEspaceOrganisation(EspaceOrganisationEntity espaceOrganisation) {
		this.espaceOrganisation = espaceOrganisation;
	}

	/**
	 * @return the declarantDechu
	 */
	public InscriptionEspaceEntity getDeclarantDechu() {
		return declarantDechu;
	}

	/**
	 * @param declarantDechu
	 *            the declarantDechu to set
	 */
	public void setDeclarantDechu(InscriptionEspaceEntity declarantDechu) {
		this.declarantDechu = declarantDechu;
	}

	/**
	 * @return the declarantPromu
	 */
	public InscriptionEspaceEntity getDeclarantPromu() {
		return declarantPromu;
	}

	/**
	 * @param declarantPromu
	 *            the declarantPromu to set
	 */
	public void setDeclarantPromu(InscriptionEspaceEntity declarantPromu) {
		this.declarantPromu = declarantPromu;
	}

}
