/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.nomenclature;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.nomenclature.DecisionConcerneeEntity;

public interface DecisionConcerneeRepository extends JpaRepository<DecisionConcerneeEntity, Long> {

}
