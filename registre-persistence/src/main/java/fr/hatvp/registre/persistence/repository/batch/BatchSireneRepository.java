/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.batch;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.batch.BatchSireneEntity;

/**
 * 
 * Repository de l'entite {@link BatchSireneEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface BatchSireneRepository
    extends JpaRepository<BatchSireneEntity, Long>
{

    /**
     * Recherche du dernier traitement de mise à jour de la base sirene.
     * 
     * @return la ligne d'historique la plus récente.
     */
    BatchSireneEntity findTopByOrderByDateTraitementDesc();
}
