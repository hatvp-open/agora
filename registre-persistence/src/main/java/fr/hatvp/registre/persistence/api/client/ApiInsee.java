/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.api.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.hatvp.registre.persistence.apiSirene.ListeEtablissements;
@Service
public class ApiInsee {
	private static final Logger LOGGER = LoggerFactory.getLogger(ApiInsee.class);
	
	/**
	 * Récupère la liste des établissements ayant comme identifiant le nationalId
	 * @param searchType
	 * @param nationalId
	 * @return
	 */
	public ListeEtablissements consumeSireneApi(String searchType, String nationalId){
	        String url = "https://api.insee.fr/entreprises/sirene/V3/siret";
	        String authToken = "c508e0b4-67b1-3bf6-bb7a-aa3894452c6b";

	        // On créer la template rest ainsi que les headers avec le token d'authentification
	        RestTemplate restTemplate = new RestTemplate();
	        HttpHeaders headers = new HttpHeaders();
	        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
	        headers.set("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
	        headers.set("Authorization", "Bearer "+authToken);

	        UriComponentsBuilder builder;
	        if(searchType.equals("siret")){
	            builder = UriComponentsBuilder.fromHttpUrl(url).query("q=siret:"+nationalId);
	        }else{
	            builder = UriComponentsBuilder.fromHttpUrl(url).query("q=siren:"+nationalId);
	        }

	        HttpEntity<String> entity = new HttpEntity<String>(headers);
	        ListeEtablissements listeEtablissements  = new ListeEtablissements();
	        try {
	        	ResponseEntity<ListeEtablissements> result = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, ListeEtablissements.class);	
	        	listeEtablissements = result.getBody();
	        }
	        catch(HttpClientErrorException e) {
	        	LOGGER.error("Pas d'établissement correspondant à l'identifiant"+nationalId, e);
	        }	        
	        return listeEtablissements;
	    }

}
