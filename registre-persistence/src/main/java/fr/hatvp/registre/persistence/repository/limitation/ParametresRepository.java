/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.limitation;

import fr.hatvp.registre.persistence.entity.limitation.ParametresEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * Repository de l'entite {@link ParametresEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface ParametresRepository extends JpaRepository<ParametresEntity, Long> {


}
