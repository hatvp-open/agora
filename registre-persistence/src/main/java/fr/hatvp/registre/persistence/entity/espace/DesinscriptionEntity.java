/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.hatvp.registre.commons.lists.OrigineSaisieEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.piece.DesinscriptionPieceEntity;

@Entity
@Table(name = "desinscription")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "desinscription_seq", allocationSize = 1)
public class DesinscriptionEntity extends AbstractCommonEntity {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "espace_id", nullable = false)
    private EspaceOrganisationEntity espaceOrganisation;

    /** Date de cessation. */
    @Column(name = "date_cessation", nullable = false)
    private LocalDate cessationDate;

    /**  motif de la désinscription. */
    @Column(name = "motif", nullable = false)
    private String motif;

    /**  observation déclarant de la désinscription. */
    @Column(name = "observation_declarant", nullable = false)
    private String observationDeclarant;

    /**  observation de la désinscription. */
    @Column(name = "observation")
    private String observationAgent;

    /** Référence vers la piece d'identite du representant legal */
    @OneToMany(mappedBy = "desinscription", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<DesinscriptionPieceEntity> pieces;
    
    @Column(name = "origine_saisie", nullable = false, columnDefinition = "TEXT", updatable = false)
	@Enumerated(EnumType.STRING)
	private OrigineSaisieEnum origineSaisie = null;


    public EspaceOrganisationEntity getEspaceOrganisation() {
        return espaceOrganisation;
    }

    public void setEspaceOrganisation(EspaceOrganisationEntity espaceOrganisation) {
        this.espaceOrganisation = espaceOrganisation;
    }

    public LocalDate getCessationDate() {
        return cessationDate;
    }

    public void setCessationDate(LocalDate cessationDate) {
        this.cessationDate = cessationDate;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getObservationDeclarant() {
        return observationDeclarant;
    }

    public void setObservationDeclarant(String observationDeclarant) {
        this.observationDeclarant = observationDeclarant;
    }

    public String getObservationAgent() {
        return observationAgent;
    }

    public void setObservationAgent(String observation) {
        this.observationAgent = observation;
    }

    public List<DesinscriptionPieceEntity> getPieces() {
        return pieces;
    }

    public void setPieces(List<DesinscriptionPieceEntity> pieces) {
        this.pieces = pieces;
    }

	public OrigineSaisieEnum getOrigineSaisie() {
		return origineSaisie;
	}

	public void setOrigineSaisie(OrigineSaisieEnum origineSaisie) {
		this.origineSaisie = origineSaisie;
	}   
    
}
