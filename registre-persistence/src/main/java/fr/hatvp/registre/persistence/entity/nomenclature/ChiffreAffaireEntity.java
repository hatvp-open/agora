/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.nomenclature;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "nomenclature_chiffre_affaire")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "nomenclature_chiffre_affaire_seq", allocationSize = 1)
public class ChiffreAffaireEntity extends CommonNomenclatureEntity {

	private static final long serialVersionUID = -31055617899013637L;

	/**
	 * Default Constructor
	 */
	public ChiffreAffaireEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public ChiffreAffaireEntity(Long id) {
		super();
		this.setId(id);
	}

}
