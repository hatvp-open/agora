/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;

/**
 *
 * Repository JPA pour l'entité {@link OrganisationEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface OrganisationRepository extends JpaRepository<OrganisationEntity, Long> {
	/**
	 * Récupérer entité par Id national
	 *
	 * @param hatvp
	 *            identifiant hatvp de l'organisation
	 * @param siren
	 *            identifiant siren de l'organisation
	 * @param rna
	 *            identifiant rna de l'organisation
	 * @return liste des organisations
	 */
	@Query("SELECT o FROM OrganisationEntity o WHERE o.siren = ?1 OR o.rna = UPPER(?2) OR o.hatvpNumber = UPPER(?3)")
	List<OrganisationEntity> findBySirenOrRnaOrHatvpNumber(String siren, String rna, String hatvp);

	/**
	 * Récupérer entité par lot d'Ids nationaux
	 *
	 * @param hatvpList
	 *            identifiant hatvp de l'organisation
	 * @param sirenList
	 *            identifiant siren de l'organisation
	 * @param rnaList
	 *            identifiant rna de l'organisation
	 * @return liste des organisations
	 */
	@Query("SELECT o FROM OrganisationEntity o WHERE UPPER(o.siren) in (?1) OR UPPER(o.rna) in (?2) OR UPPER(o.hatvpNumber) in (?3)")
	List<OrganisationEntity> findBySirenOrRnaOrHatvpNumberBatch(List<String> sirenList, List<String> rnaList, List<String> hatvpList);

	/**
	 * Récupérer entité par Id national
	 *
	 * @param duns
	 *            identifiant duns de l'organisation
	 * @param siren
	 *            identifiant siren de l'organisation
	 * @param rna
	 *            identifiant rna de l'organisation
	 * @return liste des organisations
	 */
	@Query("SELECT o FROM OrganisationEntity o WHERE o.siren = ?1 OR o.rna = UPPER(?2) OR o.dunsNumber = UPPER(?3)")
	List<OrganisationEntity> findBySirenOrRnaOrDunsNumber(String siren, String rna, String duns);

	/**
	 * Retourne la liste des organisations avec un numéro HATVP affecté.
	 *
	 * @return liste des entités organisation HATVP.
	 */
	List<OrganisationEntity> findByHatvpNumberIsNotNull();

	/**
	 * Cette méthode permet d'optimiser le code en utilisant java8.
	 *
	 * @param id
	 *            organisation id.
	 * @return les informations de l'organisation recherchée.
	 */
	Optional<OrganisationEntity> findById(Long id);

	/**
	 * Cette méthode permet d'optimiser le code en utilisant java8.
	 *
	 * @param idList
	 *            organisation id list.
	 * @return les informations de l'organisation recherchée.
	 */
	List<OrganisationEntity> findByIdIn(List<Long> idList);

	/**
	 * Cette méthode recherche une organisation par son rna.
	 *
	 * @param rna
	 *            organisation rna.
	 * @return les informations de l'organisation recherchée.
	 */
	@Query("SELECT o FROM OrganisationEntity o WHERE o.rna = UPPER(?1)")
	Optional<OrganisationEntity> findByRna(String rna);

	/**
	 * Cette méthode recherche une organisation par son rna.
	 *
	 * @param hatvpNumber
	 *            organisation hatvpNumber.
	 * @return les informations de l'organisation recherchée.
	 */
	@Query("SELECT o FROM OrganisationEntity o WHERE o.hatvpNumber = UPPER(?1)")
	OrganisationEntity findByHatvpNumber(String hatvpNumber);

	/**
	 * Cette méthode recherche une organisation par son rna.
	 *
	 * @param siren
	 *            organisation siren.
	 * @return les informations de l'organisation recherchée.
	 */
	Optional<OrganisationEntity> findBySiren(String siren);

	/**
	 * Retourne une organisation selon son SIREN si elle n'a pas d'espace collaboratif non refusé.
	 *
	 * @param siren
	 *            le SIREN de l'organisation recherchée
	 * @return l'organisation si elle existe
	 */
	@Query("SELECT o FROM OrganisationEntity o " + "WHERE o.siren = ?1 " + "AND o.originNationalId = fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum.SIREN " + "AND NOT EXISTS("
			+ "     SELECT e.id FROM EspaceOrganisationEntity e " + "     WHERE e.organisation.id = o.id " + "     AND e.statut != fr.hatvp.registre.commons.lists.StatutEspaceEnum.REFUSEE "
			+ ")")
	Optional<OrganisationEntity> findByEspaceOrganisationIsNullAndSirenAndOriginNationalIdSiren(String siren);

    /**
     * Retourne une organisation selon son RNA si elle n'a pas d'espace collaboratif non refusé.
     *
     * @param rna
     *            le RNA de l'organisation recherchée
     * @return l'organisation si elle existe
     */
    @Query("SELECT o FROM OrganisationEntity o " + "WHERE o.rna = ?1 " + "AND o.originNationalId = fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum.RNA " + "AND NOT EXISTS("
        + "     SELECT e.id FROM EspaceOrganisationEntity e " + "     WHERE e.organisation.id = o.id " + "     AND e.statut != fr.hatvp.registre.commons.lists.StatutEspaceEnum.REFUSEE "
        + ")")
    Optional<OrganisationEntity> findByEspaceOrganisationIsNullAndRnaAndOriginNationalIdRna(String rna);


	@Query(value = "SELECT COUNT(DISTINCT org.id) " + "FROM organisation AS org " + "WHERE id IN (SELECT DISTINCT organisation_id " + "FROM surveillance_organisation " + ") "
			+ "AND unaccent(org.denomination) ILIKE unaccent(?1) ", nativeQuery = true)
	Integer countOrganisationFilteredByDenomination(String keyword);


	@Query(value = "SELECT COUNT(DISTINCT org.id) " + "FROM espace_organisation AS esp, organisation AS org " + "WHERE esp.organisation_id = org.id "
			+ "AND org.id IN (SELECT DISTINCT organisation_id " + "FROM surveillance_organisation " + ") " + "AND (unaccent(org.nom_usage_siren) ILIKE unaccent(?1) "
			+ "OR unaccent(esp.nom_usage) ILIKE unaccent(?1) " + "OR unaccent(esp.nom_usage_hatvp) ILIKE unaccent(?1)) ", nativeQuery = true)
	Integer countOrganisationFilteredByNomUsage(String keyword);




}
