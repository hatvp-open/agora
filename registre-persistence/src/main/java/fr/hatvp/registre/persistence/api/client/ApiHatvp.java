/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.api.client;

import java.nio.charset.Charset;
import java.util.Collections;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import fr.hatvp.registre.commons.dto.activite.QualificationObjetDto;
@Service
public class ApiHatvp {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ApiHatvp.class);
	
	@Value("${antoinette.url}")
	private String urlAntoinette;
	
	@Value("${antoinette.origin}")
	private String originAntoinette;	
	

    public QualificationObjetDto consumeAntoinetteApi(String qualificationObjet) throws RestClientException {

        // On créer la template rest ainsi que les headers avec le token d'authentification
        RestTemplate restTemplate = new RestTemplate();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
        restTemplate.getMessageConverters().add(converter);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Origin", originAntoinette);
        restTemplate.getMessageConverters()
            .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlAntoinette);

        String json = "{\"sentence\":\""+ StringEscapeUtils.escapeJson(qualificationObjet) +"\"}";
        HttpEntity<String> entity = new HttpEntity<>(json, headers);

       ResponseEntity<QualificationObjetDto> result = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity, QualificationObjetDto.class);
       return result.getBody();

    }

}
