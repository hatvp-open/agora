/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.publication;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.publication.PubCollaborateurEntity;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

/**
 * Repository JPA pour l'entité {@link PublicationEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface PubCollaborateurRepository extends JpaRepository<PubCollaborateurEntity, Long> {

	/**
	 * Récupère la 1ère publication d'un collaborateur inscrit
	 * @param declarantId
	 * @return
	 */
	PubCollaborateurEntity findFirstByDeclarantAndEspaceOrganisationOrderByPublicationIdAsc(DeclarantEntity declarant, EspaceOrganisationEntity espaceOrganisation);
	
	PubCollaborateurEntity findFirstByEspaceOrganisationAndNomAndPrenomOrderByPublicationIdAsc(EspaceOrganisationEntity espaceOrganisation, String nom, String prenom);

	

}
