/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.batch;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.batch.BatchRnaEntity;

/**
 * Repository de l'entite {@link BatchRnaEntity}.
 */

public interface BatchRnaRepository extends JpaRepository<BatchRnaEntity, Long>{
    /**
     * Recherche du dernier traitement de mise à jour de la base sirene.
     *
     * @return la ligne d'historique la plus récente.
     */
    BatchRnaEntity findTopByOrderByDateTraitementDesc();
}
