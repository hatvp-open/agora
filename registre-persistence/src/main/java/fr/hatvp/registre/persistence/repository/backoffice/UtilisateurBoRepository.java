/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.commons.lists.backoffice.RoleEnumBackOffice;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;


public interface UtilisateurBoRepository extends JpaRepository<UtilisateurBoEntity, Long> {
	Optional<UtilisateurBoEntity> findByEmail(String username);

	/**
	 * Recherche d'un utilisateur par son email.
	 *
	 * @param email
	 *            email recherché.
	 * @return l'utilisateur recherché.
	 */
	UtilisateurBoEntity findByEmailIgnoreCase(String email);

	/**
	 * Récupérer la liste des entités paginés en recherchant par email (BackOffice)
	 *
	 * @param email
	 *            email à rechercher
	 * @param pageable
	 * @return une page des entités filtrées par email
	 */
	Page<UtilisateurBoEntity> findByEmailIgnoreCaseContaining(String email, Pageable pageable);

	@Query(value = "SELECT * " + "FROM utilisateur_bo " + "WHERE unaccent(email) ILIKE unaccent(?1) " + "ORDER BY id " + "LIMIT ?2 OFFSET ?3 ", nativeQuery = true)
	List<UtilisateurBoEntity> findByEmail(String keyword, Integer limit, Integer offset);

	@Query(value = "SELECT COUNT(*) " + "FROM utilisateur_bo " + "WHERE unaccent(email) ILIKE unaccent(?1) ", nativeQuery = true)
	Integer countUserBoFilteredByEmail(String keyword);

	/**
	 * Récupérer la liste des entités paginés en recherchant par nom et prenom (BackOffice)
	 *
	 * @param nom
	 *            String à rechercher
	 * @param prenom
	 *            String à rechercher
	 * @param pageable
	 * @return une page des entités filtrées par identifiant nom + prenom
	 */
	Page<UtilisateurBoEntity> findByNomIgnoreCaseContainingOrPrenomIgnoreCaseContaining(String nom, String prenom, Pageable pageable);

	@Query(value = "SELECT * " + "FROM utilisateur_bo " + "WHERE unaccent(nom) ILIKE unaccent(?1) " + "OR unaccent(prenom) ILIKE unaccent(?1) " + "ORDER BY id "
			+ "LIMIT ?2 OFFSET ?3 ", nativeQuery = true)
	List<UtilisateurBoEntity> findByNomOrPrenom(String keyword, Integer limit, Integer offset);

	@Query(value = "SELECT COUNT(*) " + "FROM utilisateur_bo " + "WHERE unaccent(nom) ILIKE unaccent(?1) " + "OR unaccent(prenom) ILIKE unaccent(?1) ", nativeQuery = true)
	Integer countUserBoFilteredByNomOrPrenom(String keyword);

	/**
	 * Récupérer toute la liste des entités paginés (BackOffice)
	 *
	 * @param pageable
	 * @return une page des entités
	 */
	@Override
	Page<UtilisateurBoEntity> findAll(Pageable pageable);

	List<UtilisateurBoEntity> findByRoleEnumBackOfficesContaining(RoleEnumBackOffice etatActivites);

	UtilisateurBoEntity findById(Long id);

}
