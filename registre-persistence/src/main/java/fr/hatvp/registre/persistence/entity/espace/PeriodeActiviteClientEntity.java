/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.espace;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

@Entity
@Table(name = "periode_activite_client")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "periode_activite_client_seq", allocationSize = 1)
public class PeriodeActiviteClientEntity extends AbstractCommonEntity {

    @ManyToOne
    @JoinColumn(name = "client_id", nullable = false)
    private ClientEntity client;
    /**
     * Date du jour de la saisie du nouveau client ou de la réactivation
     */
	@Column(name="date_ajout", nullable = false)
	private LocalDateTime dateAjout;	
	/**
	 * Date du jour de la saisie de la désactivation
	 */
	@Column(name = "date_desactivation")
	private LocalDateTime dateDesactivation;
	
	@Column(name = "date_fin_contrat")
	private LocalDate dateFinContrat;

    @Column(name = "commentaire")
    private String commentaire;
    
    @Column(name = "statut", nullable = false, columnDefinition = "TEXT")
	@Enumerated(EnumType.STRING)
	private StatutPublicationEnum statut;


	public ClientEntity getClient() {
		return client;
	}

	public void setClient(ClientEntity client) {
		this.client = client;
	}

	public LocalDateTime getDateAjout() {
		return dateAjout;
	}

	public void setDateAjout(LocalDateTime dateAjout) {
		this.dateAjout = dateAjout;
	}

	public LocalDateTime getDateDesactivation() {
		return dateDesactivation;
	}

	public void setDateDesactivation(LocalDateTime dateDesactivation) {
		this.dateDesactivation = dateDesactivation;
	}

	public LocalDate getDateFinContrat() {
		return dateFinContrat;
	}

	public void setDateFinContrat(LocalDate dateFinContrat) {
		this.dateFinContrat = dateFinContrat;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public StatutPublicationEnum getStatut() {
		return statut;
	}

	public void setStatut(StatutPublicationEnum statut) {
		this.statut = statut;
	}
	
}
