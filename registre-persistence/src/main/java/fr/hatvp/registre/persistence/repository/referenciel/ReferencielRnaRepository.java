/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.referenciel;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.persistence.entity.referentiel.ReferentielRnaEntity;

/**
 *
 * Repository JPA pour l'entité {@link ReferentielRnaEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface ReferencielRnaRepository
    extends JpaRepository<ReferentielRnaEntity, Long>
{

    /**
     * Rechercher les organisations par numéro rna.
     * @param rna numéro rna.
     * @return l'organisation trouvée.
     */
    @Query("SELECT r FROM ReferentielRnaEntity r WHERE r.rna = UPPER(?1)")
    List<ReferentielRnaEntity> findByRna(String rna);

    /**
     * Rechercher les organisations par lot numéros rna.
     * @param rnaList numéro rna.
     * @return l'organisation trouvée.
     */
    @Query("SELECT r FROM ReferentielRnaEntity r WHERE r.rna in (?1)")
    List<ReferentielRnaEntity> findByRnaBatch(List<String> rnaList);

    /**
     * Retourne le premier résultat correspondant au rna fourni.
     * On part du principe qu'il n'y a qu'un seul rna par ligne
     * @param rna
     * @return
     */
    ReferentielRnaEntity findFirstByRna(String rna);

    /**
     * Supprime un établissement correspondant au SIREN et au NIC fournis.
     * @param siren
     * @param nic
     * @return
     */
    Long deleteByRna(String rna);
}
