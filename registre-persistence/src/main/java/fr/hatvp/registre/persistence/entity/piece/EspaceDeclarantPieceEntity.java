/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.piece;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * Entité d'enregistrement des fichiers fournis pour les pieces d'identités dans le cadre de la validation de la création d'un compte déclarant.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "espace_declarant_piece")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "espace_declarant_piece_seq", allocationSize = 1)
public class EspaceDeclarantPieceEntity extends PieceEntity
{
    /** Clé de sérialisation. */
    private static final long serialVersionUID = -3745785207549090907L;
}

