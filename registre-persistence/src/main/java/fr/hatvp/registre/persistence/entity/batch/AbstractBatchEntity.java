/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.batch;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;

/**
 * Classe commune de gestion de l'historique des mises à jour des référentiels.
 * @version $Revision$ $Date$.
 */
@MappedSuperclass
public abstract class AbstractBatchEntity
    extends AbstractCommonEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -2550977679651866741L;

    /**
     * Date de fin de traitement du fichier.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_traitement", nullable = false)
    private Date dateTraitement = new Date();

    /**
     * URL complete du fichier téléchargé.
     */
    @Column(name = "url_fichier", nullable = false, columnDefinition = "TEXT")
    private String urlFichier;

    /**
     * Nombre de lignes mises à jour par le traitement.
     */
    @Column(name = "updated_rows_count", nullable = false)
    private Integer updatedRowsCount;

    public String getUrlFichier()
    {
        return this.urlFichier;
    }

    public void setUrlFichier(final String urlFichier)
    {
        this.urlFichier = urlFichier;
    }

    public Integer getUpdatedRowsCount()
    {
        return this.updatedRowsCount;
    }

    public void setUpdatedRowsCount(final Integer updatedRowsCount)
    {
        this.updatedRowsCount = updatedRowsCount;
    }

    public Date getDateTraitement()
    {
        return this.dateTraitement;
    }

    public void setDateTraitement(final Date dateTraitement)
    {
        this.dateTraitement = dateTraitement;
    }

}
