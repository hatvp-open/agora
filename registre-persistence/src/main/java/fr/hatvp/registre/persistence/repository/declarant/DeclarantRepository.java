/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.declarant;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;

/**
 *
 * Repository de l'entite {@link DeclarantEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface DeclarantRepository extends JpaRepository<DeclarantEntity, Long> {

	/**
	 * Recherche d'un utilisateur par son email.
	 *
	 * @param email
	 *            email recherché.
	 * @return le déclarant recherché.
	 */
	DeclarantEntity findByEmailIgnoreCase(String email);

	/**
	 * Recherche un utilisateur en fonction de son Token confirmation email.
	 *
	 * @param key
	 *            token de confirmation.
	 * @return le déclarant correspondant.
	 */
	DeclarantEntity findByEmailConfirmationCode(String key);

	/**
	 * Recherche un utilisateur en fonction de son Token de réinitialisation de mdp.
	 *
	 * @param key
	 *            token de de réinitialisation de mdp.
	 * @return le déclarant correspondant à l'adresse email.
	 */
	DeclarantEntity findByRecoverConfirmationCode(String key);

	/**
	 * Rechercher unutilisateur par son adresse email temporaire.
	 *
	 * @param email
	 *            nouvelle adresse email.
	 * @return l'utilisateur correspondant à l'adresse email.
	 */
	DeclarantEntity findByEmailTemp(String email);

	/**
	 * Chercher un déclarant par l'adresse email principale ou temporaire.
	 *
	 * @param email
	 *            email principal.
	 * @param email1
	 *            email temporaire.
	 * @return le déclarant trouvé.
	 */
	DeclarantEntity findByEmailIgnoreCaseOrEmailTempIgnoreCase(String email, String email1);

	/**
	 * Récupérer une page de la liste des entités selon le nom
	 *
	 * @param nom
	 *            nom déclarant
	 * @param pageable
	 * @return une page des entités filtrées par statut
	 */
	Page<DeclarantEntity> findByNomIgnoreCaseContaining(String nom, Pageable pageable);

    /**
     * Récupère un déclarant par id
     * @param id
     * @return un déclarant
     */
	DeclarantEntity findById(Long id);

	@Query(value = "SELECT * " + "FROM declarant " + "WHERE unaccent(nom) ILIKE unaccent(?1) " + "LIMIT ?2 OFFSET ?3", nativeQuery = true)
	List<DeclarantEntity> findByNom(String keyword, Integer limit, Integer offset);

	@Query(value = "SELECT COUNT (*) " + "FROM declarant " + "WHERE unaccent(nom) ILIKE unaccent(?1) ", nativeQuery = true)
	Integer countDeclarantFilteredByNom(String keyword);

	/**
	 * Récupérer une page de la liste des entités selon le nom
	 *
	 * @param nom
	 *            nom déclarant
	 * @param pageable
	 * @return une page des entités filtrées par statut
	 */
	Page<DeclarantEntity> findByEmailIgnoreCaseContaining(String email, Pageable pageable);

	@Query(value = "SELECT * " + "FROM declarant " + "WHERE unaccent(email) ILIKE unaccent(?1) " + "LIMIT ?2 OFFSET ?3", nativeQuery = true)
	List<DeclarantEntity> findByEmail(String keyword, Integer limit, Integer offset);

	@Query(value = "SELECT COUNT (*) " + "FROM declarant " + "WHERE unaccent(email) ILIKE unaccent(?1) ", nativeQuery = true)
	Integer countDeclarantFilteredByEmail(String keyword);

	/**
	 * Récupérer une page de la liste des entités selon le prenom
	 *
	 * @param prenom
	 *            prenom déclarant
	 * @param pageable
	 * @return une page des entités filtrées par statut
	 */
	Page<DeclarantEntity> findByPrenomIgnoreCaseContaining(String prenom, Pageable pageable);

	@Query(value = "SELECT * " + "FROM declarant " + "WHERE unaccent(prenom) ILIKE unaccent(?1) " + "LIMIT ?2 OFFSET ?3", nativeQuery = true)
	List<DeclarantEntity> findByPrenom(String keyword, Integer limit, Integer offset);

	@Query(value = "SELECT COUNT (*) " + "FROM declarant " + "WHERE unaccent(prenom) ILIKE unaccent(?1) ", nativeQuery = true)
	Integer countDeclarantFilteredByPrenom(String keyword);

	/**
	 * Récupérer une liste paginée des déclarants dont l'un des espaces correspond a la recherche
	 *
	 * @param keyword
	 * @param limit
	 * @param offset
	 * @return une liste paginée des déclarants dont l'un des espaces correspond a la recherche
	 */
	@Query(value = "SELECT * " + "FROM declarant AS dec " + "WHERE dec.id IN (" + "SELECT DISTINCT(ins.declarant_id) " + "FROM inscription_espace AS ins "
			+ "LEFT JOIN espace_organisation AS esp ON esp.id = ins.espace_organisation_id " + "LEFT JOIN organisation AS org ON org.id = esp.organisation_id "
			+ "WHERE unaccent(org.denomination) ILIKE unaccent(?1) " + ") " + "ORDER BY dec.id LIMIT ?2 OFFSET ?3 ", nativeQuery = true)
	List<DeclarantEntity> findByOrganisation(String keyword, Integer limit, Integer offset);

	/**
	 * Récupérer le nombre d'élément correspondant à la recherche
	 *
	 * @param keyword
	 * @return le nombre d'élément correspondant à la recherche
	 */
	@Query(value = "SELECT COUNT (DISTINCT ins.declarant_id) " + "FROM inscription_espace AS ins " + "LEFT JOIN espace_organisation AS esp ON esp.id = ins.espace_organisation_id "
			+ "LEFT JOIN organisation AS org ON org.id = esp.organisation_id " + "WHERE unaccent(org.denomination) ILIKE unaccent(?1) ", nativeQuery = true)
	Integer countDeclarantFilteredByEspace(String keyword);
}
