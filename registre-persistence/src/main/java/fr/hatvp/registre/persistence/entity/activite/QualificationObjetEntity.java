/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.activite;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.NotAudited;

import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;


@Entity
@Table(name = "qualification_objet")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "qualification_objet_seq", allocationSize = 1)
public class QualificationObjetEntity
    extends AbstractCommonEntity
{

    private static final long serialVersionUID = -1256538668259006222L;

    /**
     * Default Constructor
     */
    public QualificationObjetEntity()
    {
        super();
    }

    /**
     * Constructor with id
     *
     * @param id
     */
    public QualificationObjetEntity(Long id)
    {
        super();
        this.setId(id);
    }

    /**
     * denomination de la fiche d'activité
     */
    @NotAudited
    @Column(name = "denomination", columnDefinition = "TEXT")
    private String denomination;

    /**
     * nomUsage de la fiche d'activité
     */
    @NotAudited
    @Column(name = "nomUsage", columnDefinition = "TEXT")
    private String nomUsage;

    /**
     * Objet de la fiche d'activité
     */
    @NotAudited
    @Column(name = "objet", columnDefinition = "TEXT")
    private String objet;

    /**
     *
     *
     * Date où la qualification a été réalisée
     */
    @NotAudited
    @Column(name = "date_enregistrement")
    private LocalDate dateEnregistrement;

    /**
     * Identifiant de la fiche d'activité
     */
    @NotAudited
    @Column(name = "id_fiche", columnDefinition = "TEXT")
    private String idFiche;

    /**
     * Identifiant national de l'organisation
     */
    @NotAudited
    @Column(name = "identifiant_national", columnDefinition = "TEXT")
    private String identifiantNational;

    /**
     * Note attribué à l'objet 0 = Mauvais ; 1 = Bon
     */
    @NotAudited
    @Column(name = "note")
    private boolean note;

    /**
     * Coefficient de confiance lié à la note
     */
    @NotAudited
    @Column(name = "coefficient_confiance")
    private Float coefficiantConfiance;

    /**
     * Boolean de reprise par batch
     */
    @NotAudited
    @Column(name = "reprise")
    private boolean reprise = false;

    public LocalDate getDateEnregistrement()
    {
        return dateEnregistrement;
    }
    public void setDateEnregistrement(LocalDate dateEnregistrement)
    {
        this.dateEnregistrement = dateEnregistrement;
    }
    public String getIdFiche()
    {
        return idFiche;
    }
    public void setIdFiche(String idFiche)
    {
        this.idFiche = idFiche;
    }
    public String getIdentifiantNational()
    {
        return identifiantNational;
    }
    public void setIdentifiantNational(String identifiantNational)
    {
        this.identifiantNational = identifiantNational;
    }
    public boolean isNote()
    {
        return note;
    }
    public void setNote(boolean note)
    {
        this.note = note;
    }
    public Float getCoefficiantConfiance()
    {
        return coefficiantConfiance;
    }
    public void setCoefficiantConfiance(Float coefficiantConfiance)
    {
        this.coefficiantConfiance = coefficiantConfiance;
    }

    public String getObjet()
    {
        return objet;
    }
    public void setObjet(String objet)
    {
        this.objet = objet;
    }

    public String getDenomination()
    {
        return denomination;
    }
    public void setDenomination(String denomination)
    {
        this.denomination = denomination;
    }
    public String getNomUsage()
    {
        return nomUsage;
    }
    public void setNomUsage(String nomUsage)
    {
        this.nomUsage = nomUsage;
    }
    public boolean isReprise()
    {
        return reprise;
    }
    public void setReprise(boolean reprise)
    {
        this.reprise = reprise;
    }
}
