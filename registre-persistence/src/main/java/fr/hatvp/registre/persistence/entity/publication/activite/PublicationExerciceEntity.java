/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.publication.activite;

import javax.persistence.*;

import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.publication.activite.common.PublicationEntity;
import org.hibernate.envers.Audited;

/**
 * Entité des publications pour les activités.
 * @version $Revision$ $Date${0xD}
 */
@Audited
@Entity
@Table(name = "publication_exercice")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "publication_exercice_seq",
    allocationSize = 1)
public class PublicationExerciceEntity
    extends PublicationEntity
{

    private static final long serialVersionUID = -6831013966853871371L;

    /** Référence à l'exercice. */
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "exercice_comptable_id", nullable = false)
    private ExerciceComptableEntity exerciceComptable;

    /** Version de l'exercice comptable à la publication */
    @Column(name = "version_exercice_comptable")
    private Integer versionExercice;

    /** Publier les moyens alloués */
    @Column(name = "publierMoyens")
    private boolean nonDeclarationMoyens;

    public ExerciceComptableEntity getExerciceComptable()
    {
        return exerciceComptable;
    }
    public void setExerciceComptable(ExerciceComptableEntity exerciceComptable)
    {
        this.exerciceComptable = exerciceComptable;
    }

    public Integer getVersionExercice()
    {
        return versionExercice;
    }
    public void setVersionExercice(Integer versionExercice)
    {
        this.versionExercice = versionExercice;
    }

    public boolean isNonDeclarationMoyens()
    {
        return nonDeclarationMoyens;
    }
    public void setNonDeclarationMoyens(boolean nonDeclarationMoyens)
    {
        this.nonDeclarationMoyens = nonDeclarationMoyens;
    }
}