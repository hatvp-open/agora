/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.espace.AssociationEntity;

/**
 * Repository JPA pour l'entité {@link AssociationEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface AssoAppartenanceRepository extends JpaRepository<AssociationEntity, Long> {

	/**
	 * Récupérer une association par l'id de l'espace organisation et l'id de l'organisation.
	 *
	 * @param espaceOrganisationId
	 *            id de l'espace organisation.
	 * @param organisationId
	 *            id de l'organisation.
	 * @return l'association correspondante.
	 */
	AssociationEntity findByEspaceOrganisationIdAndOrganisationId(Long espaceOrganisationId, Long organisationId);

	/**
	 * Récupérer les organisations pro d'un espace organisation.
	 *
	 * @param currentEspaceOrganisationId
	 *            id de l'espace organisation.
	 * @return toutes les organisations pro de l'espace organisation.
	 */
	List<AssociationEntity> findByEspaceOrganisationId(Long currentEspaceOrganisationId);

	/**
	 * Récupérer une page des organisations pro d'un espace organisation.
	 *
	 * @param currentEspaceOrganisationId
	 *            id de l'espace organisation
	 * @param pageable
	 *            paramètres de pagination
	 * @return une page des organisations pro de l'espace organisation.
	 */
	Page<AssociationEntity> findByEspaceOrganisationId(Long currentEspaceOrganisationId, Pageable pageable);

	/**
	 * Récupère une association si elle appartient à l'espace organisation passé en paramètre.
	 *
	 * @param associationId
	 * @param espaceOrganisationId
	 * @return l'association de l'espace.
	 */
	AssociationEntity findByIdAndEspaceOrganisationId(Long associationId, Long espaceOrganisationId);
}
