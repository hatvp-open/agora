/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice.commentaire;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.commentaire.CommentDeclarantEntity;

import java.util.List;

/**
 * 
 * repository des commentaires des déclarants.
 * @version $Revision$ $Date${0xD}
 */
public interface CommentDeclarantRepository
    extends JpaRepository<CommentDeclarantEntity, Long>
{
    /**
     *
     * @param declarantId id du déclarant;
     * @return liste de commentaire du declarant.
     */
    List<CommentDeclarantEntity> findByDeclarantEntityId(Long declarantId);
}
