/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.publication;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.commons.lists.TypeOrganisationEnum;
import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;

/**
 * Entité des publications.
 * @version $Revision$ $Date${0xD}
 */
@Audited
@Entity
@Table(name = "publication")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "publication_seq",
    allocationSize = 1)
public class PublicationEntity
    extends AbstractCommonEntity {

    /** Clé de sérialisation. */
    private static final long serialVersionUID = -1615988636761681345L;

    /** Date de création. */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_creation", nullable = false, updatable = false)
    private Date creationDate = new Date();

    /** référence au publicateur. */
    @ManyToOne
    @JoinColumn(name = "publicateur_id", nullable = false)
    private DeclarantEntity publicateur;

    /** Date de dépublication. */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_depublication")
    private Date depublicationDate;

    /** référence au dépublicateur. */
    @ManyToOne
    @JoinColumn(name = "depublicateur_id")
    private UtilisateurBoEntity depublicateur;

    /** Date de republication. */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_republication")
    private Date republicationDate;

    /** référence au republicateur. */
    @ManyToOne
    @JoinColumn(name = "republicateur_id")
    private UtilisateurBoEntity republicateur;

    /** Etat publication. */
    @Enumerated(EnumType.STRING)
    @Column(name = "statut", nullable = false, columnDefinition = "TEXT")
    private StatutPublicationEnum statut = StatutPublicationEnum.PUBLIEE;

    /** Status de la publication. */
    @Column(name = "supprime")
    private Boolean deleted;

    /** Le logo de l'espace organisation. */
    @NotAudited
    @Column(name = "logo")
    private byte[] logo;

    /** Le type MIME du logo de l'espace organisation. */
    @Column(name = "logo_type", columnDefinition = "TEXT")
    private String logoType;

    /** Déclaration pour un tiers. */
    @Column(name = "non_declaration_tiers")
    private Boolean nonDeclarationTiers;

    /** Déclaration pour un tiers. */
    @Column(name = "non_declaration_association")
    private Boolean nonDeclarationOrgaAppartenance;

    /** Identifiant national de l'organisation. */
    @Column(name = "id_national", columnDefinition = "TEXT")
    private String nationalId;

    /** Type identifiant national (SIREN/RNA). */
    @Enumerated(EnumType.STRING)
    @Column(name = "type_id_national", columnDefinition = "TEXT")
    private TypeIdentifiantNationalEnum typeIdentifiantNational;

    /** Denomination de l'organisation. */
    @Column(name = "denomination", columnDefinition = "TEXT")
    private String denomination;

    /** Nom d'usage de l'organisation. */
    @Column(name = "nom_usage", columnDefinition = "TEXT")
    private String nomUsage;

    /** Champ des activités. */
    /** Champ des activités. */
    @Column(name = "secteur_activite", length = 500, columnDefinition = "TEXT")
    private String secteursActivites;

    @Column(name = "niveau_intervention", length = 500, columnDefinition = "TEXT")
    private String niveauIntervention;

    /**
     * Moyens de contact de l'organisation.
     */
    /** Email de contact. */
    @Column(name = "email_de_contact", columnDefinition = "TEXT")
    private String emailDeContact;

    /** publier mon adresse email. */
    @Column(name = "non_publier_mon_email")
    private Boolean nonPublierMonAdresseEmail;

    /** téléphone de contact. */
    @Column(name = "telephone_de_contact", columnDefinition = "TEXT")
    private String telephoneDeContact;

    /** publier ou pas le téléphone de contact. */
    @Column(name = "non_publier_mon_telephone")
    private Boolean nonPublierMonTelephoneDeContact;

    /** Site internet. */
    @Column(name = "website", columnDefinition = "TEXT")
    private String lienSiteWeb;

    /** page twiter. */
    @Column(name = "page_twitter", columnDefinition = "TEXT")
    private String lienPageTwitter;

    /** page facebook. */
    @Column(name = "page_facebook", columnDefinition = "TEXT")
    private String lienPageFacebook;

    /** page linkedin. */
    @Column(name = "page_linkedin", columnDefinition = "TEXT")
    private String lienPageLinkedin;
    
    /** page linkedin. */
    @Column(name = "page_liste_tiers", columnDefinition = "TEXT")
    private String lienListeTiers;
    /**
     * Adresse de l'organisation
     */
    /** Adresse. */
    @Column(name = "adresse", length = 500, columnDefinition = "TEXT")
    private String adresse;

    /** publier ou pas mon adresse physique. */
    @Column(name = "non_publier_mon_adresse")
    private Boolean nonPublierMonAdressePhysique;

    /** Code Postal. */
    @Column(name = "code_postal", columnDefinition = "TEXT")
    private String codePostal;

    /** Ville. */
    @Column(name = "ville", columnDefinition = "TEXT")
    private String ville;

    /** Pays. */
    @Column(name = "pays", columnDefinition = "TEXT")
    private String pays;

    /**
     * Données financières:
     */
    /** Catégorie de l'espace organisation */
    @Enumerated(EnumType.STRING)
    @Column(name = "categorie_organisation", columnDefinition = "TEXT")
    private TypeOrganisationEnum categorieOrganisation;

    /** Dirigeants. */
    @OneToMany(mappedBy = "publication", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<PubDirigeantEntity> dirigeants;

    /** azsociations. */
    @OneToMany(mappedBy = "publication", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<PubAssociationEntity> associations;

    /** azsociations. */
    @OneToMany(mappedBy = "publication", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<PubCollaborateurEntity> collaborateurs;

    /** azsociations. */
    @OneToMany(mappedBy = "publication", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<PubClientEntity> clients;

    /** Référence à l'espace organisation. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "espace_organisation_id", nullable = false)
    private EspaceOrganisationEntity espaceOrganisation;

    /** La grappe de publication en format json */
    @NotAudited
    @Column(name = "pub_json", columnDefinition = "TEXT", nullable = false, updatable = false)
    private String publicationJson;
    

    /**
     * Accesseur en lecture du champ <code>publicateur</code>.
     *
     * @return le champ <code>publicateur</code>.
     */
    public DeclarantEntity getPublicateur() {
        return this.publicateur;
    }

    /**
     * Accesseur en écriture du champ <code>publicateur</code>.
     *
     * @param publicateur la valeur à écrire dans <code>publicateur</code>.
     */
    public void setPublicateur(final DeclarantEntity publicateur) {
        this.publicateur = publicateur;
    }

    /**
     * Gets depublication date.
     *
     * @return the depublication date
     */
    public Date getDepublicationDate() {
        return this.depublicationDate;
    }

    /**
     * Sets depublication date.
     *
     * @param depublicationDate the depublication date
     */
    public void setDepublicationDate(final Date depublicationDate) {
        this.depublicationDate = depublicationDate;
    }

    /**
     * Gets depublicateur.
     *
     * @return the depublicateur
     */
    public UtilisateurBoEntity getDepublicateur() {
        return this.depublicateur;
    }

    /**
     * Sets depublicateur.
     *
     * @param depublicateur the depublicateur
     */
    public void setDepublicateur(final UtilisateurBoEntity depublicateur) {
        this.depublicateur = depublicateur;
    }

    /**
     * Gets republication date.
     *
     * @return the republication date
     */
    public Date getRepublicationDate() {
        return this.republicationDate;
    }

    /**
     * Sets republication date.
     *
     * @param republicationDate the republication date
     */
    public void setRepublicationDate(final Date republicationDate) {
        this.republicationDate = republicationDate;
    }

    /**
     * Gets republicateur.
     *
     * @return the republicateur
     */
    public UtilisateurBoEntity getRepublicateur() {
        return this.republicateur;
    }

    /**
     * Sets republicateur.
     *
     * @param republicateur the republicateur
     */
    public void setRepublicateur(final UtilisateurBoEntity republicateur) {
        this.republicateur = republicateur;
    }


    /**
     * Accesseur en lecture du champ <code>secteursActivites</code>.
     *
     * @return le champ <code>secteursActivites</code>.
     */
    public String getSecteursActivites() {
        return this.secteursActivites;
    }

    /**
     * Accesseur en écriture du champ <code>secteursActivites</code>.
     *
     * @param secteursActivites la valeur à écrire dans <code>secteursActivites</code>.
     */
    public void setSecteursActivites(final String secteursActivites) {
        this.secteursActivites = secteursActivites;
    }

    /**
     * Accesseur en lecture du champ <code>niveauIntervention</code>.
     *
     * @return le champ <code>niveauIntervention</code>.
     */
    public String getNiveauIntervention() {
        return this.niveauIntervention;
    }

    /**
     * Accesseur en écriture du champ <code>niveauIntervention</code>.
     *
     * @param niveauIntervention la valeur à écrire dans <code>niveauIntervention</code>.
     */
    public void setNiveauIntervention(final String niveauIntervention) {
        this.niveauIntervention = niveauIntervention;
    }

    /**
     * Accesseur en lecture du champ <code>emailDeContact</code>.
     *
     * @return le champ <code>emailDeContact</code>.
     */
    public String getEmailDeContact() {
        return this.emailDeContact;
    }

    /**
     * Accesseur en écriture du champ <code>emailDeContact</code>.
     *
     * @param emailDeContact la valeur à écrire dans <code>emailDeContact</code>.
     */
    public void setEmailDeContact(final String emailDeContact) {
        this.emailDeContact = emailDeContact;
    }

    /**
     * Accesseur en lecture du champ <code>telephoneDeContact</code>.
     *
     * @return le champ <code>telephoneDeContact</code>.
     */
    public String getTelephoneDeContact() {
        return this.telephoneDeContact;
    }

    /**
     * Accesseur en écriture du champ <code>telephoneDeContact</code>.
     *
     * @param telephoneDeContact la valeur à écrire dans <code>telephoneDeContact</code>.
     */
    public void setTelephoneDeContact(final String telephoneDeContact) {
        this.telephoneDeContact = telephoneDeContact;
    }

    /**
     * Accesseur en lecture du champ <code>lienSiteWeb</code>.
     *
     * @return le champ <code>lienSiteWeb</code>.
     */
    public String getLienSiteWeb() {
        return this.lienSiteWeb;
    }

    /**
     * Accesseur en écriture du champ <code>lienSiteWeb</code>.
     *
     * @param lienSiteWeb la valeur à écrire dans <code>lienSiteWeb</code>.
     */
    public void setLienSiteWeb(final String lienSiteWeb) {
        this.lienSiteWeb = lienSiteWeb;
    }

    /**
     * Accesseur en lecture du champ <code>lienPageTwitter</code>.
     *
     * @return le champ <code>lienPageTwitter</code>.
     */
    public String getLienPageTwitter() {
        return this.lienPageTwitter;
    }

    /**
     * Accesseur en écriture du champ <code>lienPageTwitter</code>.
     *
     * @param lienPageTwitter la valeur à écrire dans <code>lienPageTwitter</code>.
     */
    public void setLienPageTwitter(final String lienPageTwitter) {
        this.lienPageTwitter = lienPageTwitter;
    }

    /**
     * Accesseur en lecture du champ <code>lienPageFacebook</code>.
     *
     * @return le champ <code>lienPageFacebook</code>.
     */
    public String getLienPageFacebook() {
        return this.lienPageFacebook;
    }

    /**
     * Accesseur en écriture du champ <code>lienPageFacebook</code>.
     *
     * @param lienPageFacebook la valeur à écrire dans <code>lienPageFacebook</code>.
     */
    public void setLienPageFacebook(final String lienPageFacebook) {
        this.lienPageFacebook = lienPageFacebook;
    }

    /**
     * Accesseur en lecture du champ <code>lienPageLinkedin</code>.
     *
     * @return le champ <code>lienPageLinkedin</code>.
     */
    public String getLienPageLinkedin() {
        return this.lienPageLinkedin;
    }

    /**
     * Accesseur en écriture du champ <code>lienPageLinkedin</code>.
     *
     * @param lienPageLinkedin la valeur à écrire dans <code>lienPageLinkedin</code>.
     */
    public void setLienPageLinkedin(final String lienPageLinkedin) {
        this.lienPageLinkedin = lienPageLinkedin;
    }

	/**
	 * @return the lienListeTiers
	 */
	public String getLienListeTiers() {
		return lienListeTiers;
	}

	/**
	 * @param lienListeTiers
	 *            the lienListeTiers to set
	 */
	public void setLienListeTiers(String lienListeTiers) {
		this.lienListeTiers = lienListeTiers;
	}
    /**
     * Accesseur en lecture du champ <code>adresse</code>.
     *
     * @return le champ <code>adresse</code>.
     */
    public String getAdresse() {
        return this.adresse;
    }

    /**
     * Accesseur en écriture du champ <code>adresse</code>.
     *
     * @param adresse la valeur à écrire dans <code>adresse</code>.
     */
    public void setAdresse(final String adresse) {
        this.adresse = adresse;
    }

    /**
     * Accesseur en lecture du champ <code>codePostal</code>.
     *
     * @return le champ <code>codePostal</code>.
     */
    public String getCodePostal() {
        return this.codePostal;
    }

    /**
     * Accesseur en écriture du champ <code>codePostal</code>.
     *
     * @param codePostal la valeur à écrire dans <code>codePostal</code>.
     */
    public void setCodePostal(final String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * Accesseur en lecture du champ <code>ville</code>.
     *
     * @return le champ <code>ville</code>.
     */
    public String getVille() {
        return this.ville;
    }

    /**
     * Accesseur en écriture du champ <code>ville</code>.
     *
     * @param ville la valeur à écrire dans <code>ville</code>.
     */
    public void setVille(final String ville) {
        this.ville = ville;
    }

    /**
     * Accesseur en lecture du champ <code>pays</code>.
     *
     * @return le champ <code>pays</code>.
     */
    public String getPays() {
        return this.pays;
    }

    /**
     * Accesseur en écriture du champ <code>pays</code>.
     *
     * @param pays la valeur à écrire dans <code>pays</code>.
     */
    public void setPays(final String pays) {
        this.pays = pays;
    }

    /**
     * Accesseur en lecture du champ <code>categorieOrganisation</code>.
     *
     * @return le champ <code>categorieOrganisation</code>.
     */
    public TypeOrganisationEnum getCategorieOrganisation() {
        return this.categorieOrganisation;
    }

    /**
     * Accesseur en écriture du champ <code>categorieOrganisation</code>.
     *
     * @param categorieOrganisation la valeur à écrire dans <code>categorieOrganisation</code>.
     */
    public void setCategorieOrganisation(final TypeOrganisationEnum categorieOrganisation) {
        this.categorieOrganisation = categorieOrganisation;
    }

    /**
     * Accesseur en lecture du champ <code>creationDate</code>.
     *
     * @return le champ <code>creationDate</code>.
     */
    public Date getCreationDate() {
        return this.creationDate;
    }

    /**
     * Accesseur en lecture du champ <code>deleted</code>.
     *
     * @return le champ <code>deleted</code>.
     */
    public Boolean isDeleted() {
        return this.deleted;
    }

    /**
     * Accesseur en écriture du champ <code>deleted</code>.
     *
     * @param deleted la valeur à écrire dans <code>deleted</code>.
     */
    public void setDeleted(final Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * Accesseur en lecture du champ <code>dirigeants</code>.
     *
     * @return le champ <code>dirigeants</code>.
     */
    public List<PubDirigeantEntity> getDirigeants() {
        return this.dirigeants;
    }

    /**
     * Accesseur en écriture du champ <code>dirigeants</code>.
     *
     * @param dirigeants la valeur à écrire dans <code>dirigeants</code>.
     */
    public void setDirigeants(final List<PubDirigeantEntity> dirigeants) {
        this.dirigeants = dirigeants;
    }

    /**
     * Accesseur en lecture du champ <code>associations</code>.
     *
     * @return le champ <code>associations</code>.
     */
    public List<PubAssociationEntity> getAssociations() {
        return this.associations;
    }

    /**
     * Accesseur en écriture du champ <code>associations</code>.
     *
     * @param associations la valeur à écrire dans <code>associations</code>.
     */
    public void setAssociations(final List<PubAssociationEntity> associations) {
        this.associations = associations;
    }

    /**
     * Accesseur en lecture du champ <code>collaborateurs</code>.
     *
     * @return le champ <code>collaborateurs</code>.
     */
    public List<PubCollaborateurEntity> getCollaborateurs() {
        return this.collaborateurs;
    }

    /**
     * Accesseur en écriture du champ <code>collaborateurs</code>.
     *
     * @param collaborateurs la valeur à écrire dans <code>collaborateurs</code>.
     */
    public void setCollaborateurs(final List<PubCollaborateurEntity> collaborateurs) {
        this.collaborateurs = collaborateurs;
    }

    /**
     * Accesseur en lecture du champ <code>clients</code>.
     *
     * @return le champ <code>clients</code>.
     */
    public List<PubClientEntity> getClients() {
        return this.clients;
    }

    /**
     * Accesseur en écriture du champ <code>clients</code>.
     *
     * @param clients la valeur à écrire dans <code>clients</code>.
     */
    public void setClients(final List<PubClientEntity> clients) {
        this.clients = clients;
    }

    /**
     * Accesseur en lecture du champ <code>espaceOrganisation</code>.
     *
     * @return le champ <code>espaceOrganisation</code>.
     */
    public EspaceOrganisationEntity getEspaceOrganisation() {
        return this.espaceOrganisation;
    }

    /**
     * Accesseur en écriture du champ <code>espaceOrganisation</code>.
     *
     * @param espaceOrganisation la valeur à écrire dans <code>espaceOrganisation</code>.
     */
    public void setEspaceOrganisation(final EspaceOrganisationEntity espaceOrganisation) {
        this.espaceOrganisation = espaceOrganisation;
    }

    /**
     * Accesseur en lecture du champ <code>nationalId</code>.
     *
     * @return le champ <code>nationalId</code>.
     */
    public String getNationalId() {
        return this.nationalId;
    }

    /**
     * Accesseur en écriture du champ <code>nationalId</code>.
     *
     * @param nationalId la valeur à écrire dans <code>nationalId</code>.
     */
    public void setNationalId(final String nationalId) {
        this.nationalId = nationalId;
    }

    /**
     * Accesseur en lecture du champ <code>typeIdentifiantNational</code>.
     *
     * @return le champ <code>typeIdentifiantNational</code>.
     */
    public TypeIdentifiantNationalEnum getTypeIdentifiantNational() {
        return this.typeIdentifiantNational;
    }

    /**
     * Accesseur en écriture du champ <code>typeIdentifiantNational</code>.
     *
     * @param typeIdentifiantNational la valeur à écrire dans <code>typeIdentifiantNational</code>.
     */
    public void setTypeIdentifiantNational(
        final TypeIdentifiantNationalEnum typeIdentifiantNational) {
        this.typeIdentifiantNational = typeIdentifiantNational;
    }

    /**
     * Accesseur en lecture du champ <code>denomination</code>.
     *
     * @return le champ <code>denomination</code>.
     */
    public String getDenomination() {
        return this.denomination;
    }

    /**
     * Accesseur en écriture du champ <code>denomination</code>.
     *
     * @param denomination la valeur à écrire dans <code>denomination</code>.
     */
    public void setDenomination(final String denomination) {
        this.denomination = denomination;
    }

    /**
     * Accesseur en lecture du champ <code>nomUsage</code>.
     *
     * @return le champ <code>nomUsage</code>.
     */
    public String getNomUsage() {
        return this.nomUsage;
    }

    /**
     * Accesseur en écriture du champ <code>nomUsage</code>.
     *
     * @param nomUsage la valeur à écrire dans <code>nomUsage</code>.
     */
    public void setNomUsage(final String nomUsage) {
        this.nomUsage = nomUsage;
    }

    /**
     * la valeur à écrire dans <code>utilisateurModification</code>.
     * Accesseur en lecture du champ <code>logo</code>.
     *
     * @return le champ <code>logo</code>.
     */
    public byte[] getLogo() {
        return this.logo;
    }

    /**
     * Accesseur en écriture du champ <code>logo</code>.
     *
     * @param logo la valeur à écrire dans <code>logo</code>.
     */
    public void setLogo(final byte[] logo) {
        this.logo = logo;
    }

    /**
     * Accesseur en lecture du champ <code>logoType</code>.
     *
     * @return le champ <code>logoType</code>.
     */
    public String getLogoType() {
        return this.logoType;
    }

    /**
     * Accesseur en écriture du champ <code>logoType</code>.
     *
     * @param logoType la valeur à écrire dans <code>logoType</code>.
     */
    public void setLogoType(final String logoType) {
        this.logoType = logoType;
    }

    /**
     * Gets statut publication enum.
     *
     * @return the statut publication enum
     */
    public StatutPublicationEnum getStatut() {
        return this.statut;
    }

    /**
     * Sets statut publication enum.
     *
     * @param statutPublicationEnum the statut publication enum
     */
    public void setStatut(final StatutPublicationEnum statutPublicationEnum) {
        this.statut = statutPublicationEnum;
    }

    /**
     * Accesseur en lecture du champ <code>publicationJson</code>.
     *
     * @return le champ <code>publicationJson</code>.
     */
    public String getPublicationJson() {
        return this.publicationJson;
    }

    /**
     * Accesseur en écriture du champ <code>publicationJson</code>.
     *
     * @param publicationJson la valeur à écrire dans <code>publicationJson</code>.
     */
    public void setPublicationJson(final String publicationJson) {
        this.publicationJson = publicationJson;
    }

    /**
     * Accesseur en écriture du champ <code>creationDate</code>.
     *
     * @param creationDate la valeur à écrire dans <code>creationDate</code>.
     */
    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }
    
    /**
     * Accesseur en lecture du champ <code>nonDeclarationTiers</code>.
     *
     * @return le champ <code>nonDeclarationTiers</code>.
     */
    public Boolean isNonDeclarationTiers() {
        return this.nonDeclarationTiers;
    }

    /**
     * Accesseur en écriture du champ <code>nonDeclarationTiers</code>.
     *
     * @param declarationTiers
     * la valeur à écrire dans <code>nonDeclarationTiers</code>.
     */
    public void setNonDeclarationTiers(final Boolean nonDeclarationTiers) {
        this.nonDeclarationTiers = nonDeclarationTiers;
    }
    
    /**
     * Accesseur en lecture du champ <code>nonDeclarationOrgaAppartenance</code>.
     *
     * @return le champ <code>nonDeclarationOrgaAppartenance</code>.
     */
    public Boolean isNonDeclarationOrgaAppartenance() {
        return this.nonDeclarationOrgaAppartenance;
    }

    /**
     * Accesseur en écriture du champ <code>nonDeclarationOrgaAppartenance</code>.
     *
     * @param appartenanceTiers
     * la valeur à écrire dans <code>nonDeclarationOrgaAppartenance</code>.
     */
    public void setNonDeclarationOrgaAppartenance(final Boolean nonDeclarationOrgaAppartenance) {
        this.nonDeclarationOrgaAppartenance = nonDeclarationOrgaAppartenance;
    }
    
    /**
     * Accesseur en lecture du champ <code>nonPublierMonAdressePhysique</code>.
     *
     * @return le champ <code>nonPublierMonAdressePhysique</code>.
     */
    public Boolean getNonPublierMonAdressePhysique() {
        return this.nonPublierMonAdressePhysique;
    }

    /**
     * Accesseur en écriture du champ <code>nonPublierMonAdressePhysique</code>.
     *
     * @param publierMonAdressePhysique
     * la valeur à écrire dans <code>nonPublierMonAdressePhysique</code>.
     */
    public void setNonPublierMonAdressePhysique(final Boolean nonPublierMonAdressePhysique) {
        this.nonPublierMonAdressePhysique = nonPublierMonAdressePhysique;
    }

    /**
     * Accesseur en lecture du champ <code>nonPublierMonTelephoneDeContact</code>.
     *
     * @return le champ <code>nonPublierMonTelephoneDeContact</code>.
     */
    public Boolean getNonPublierMonTelephoneDeContact() {
        return this.nonPublierMonTelephoneDeContact;
    }

    /**
     * Accesseur en écriture du champ <code>nonPublierMonTelephoneDeContact</code>.
     *
     * @param publierMonTelephoneDeContact
     * la valeur à écrire dans <code>nonPublierMonTelephoneDeContact</code>.
     */
    public void setNonPublierMonTelephoneDeContact(final Boolean nonPublierMonTelephoneDeContact) {
        this.nonPublierMonTelephoneDeContact = nonPublierMonTelephoneDeContact;
    }
    
    /**
     * Accesseur en lecture du champ <code>nonPublierMonAdresseEmail</code>.
     *
     * @return le champ <code>nonPublierMonAdresseEmail</code>.
     */
    public Boolean getNonPublierMonAdresseEmail() {
        return this.nonPublierMonAdresseEmail;
    }

    /**
     * Accesseur en écriture du champ <code>nonPublierMonAdresseEmail</code>.
     *
     * @param publierMonAdresseEmail
     * la valeur à écrire dans <code>nonPublierMonAdresseEmail</code>.
     */
    public void setNonPublierMonAdresseEmail(final Boolean nonPublierMonAdresseEmail) {
        this.nonPublierMonAdresseEmail = nonPublierMonAdresseEmail;
    }

}
