/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DesinscriptionRepository extends JpaRepository<DesinscriptionEntity, Long> {

    DesinscriptionEntity findByEspaceOrganisation(EspaceOrganisationEntity espace);

    DesinscriptionEntity findById(long desinscriptionId);

}
