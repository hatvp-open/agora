/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.backoffice.EspaceOrganisationLockBoEntity;

/**
 *
 * Repository de l'entite {@link EspaceOrganisationLockBoEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface EspaceOrganisationLockBoRepository
    extends JpaRepository<EspaceOrganisationLockBoEntity, Long>
{
    /**
     * Trouver un Lock par Espace Organisation et sur un interval
     * @param id id {@link fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity}
     * @return les locks sur l'espace
     */
    List<EspaceOrganisationLockBoEntity> findByEspaceOrganisationId(Long id);

}
