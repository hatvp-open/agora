/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.publication;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.publication.PublicationEntity;

/**
 * Repository JPA pour l'entité {@link PublicationEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface PublicationRepository extends JpaRepository<PublicationEntity, Long> {

	/**
	 * Récupérer la dernière publication d'un espace organisation.
	 *
	 * @param espaceId
	 *            id de l'espace organisation.
	 * @return la publication si elle existe.
	 */
	Optional<PublicationEntity> findFirstByEspaceOrganisationIdAndStatutNotOrderByIdDesc(Long espaceId, StatutPublicationEnum statut);

	/**
	 * Récupérer les dernières publications pour les espaces dont la publication est active avec la date de première publication pour l'espace.
	 *
	 * @return stream des tableaux d'objects : - Object[0] la dernière publication pour chaque espace - Object[1] la date de première publication calculée pour l'espace.
	 */
	@Query("SELECT p, MIN(p2.creationDate) " + "FROM PublicationEntity AS p, PublicationEntity AS p2 " + "WHERE p.espaceOrganisation.id = p2.espaceOrganisation.id "
			+ "AND p.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE " + "AND p2.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE "
			+ "AND p.espaceOrganisation.publicationEnabled = true " + "GROUP BY p HAVING p.id = MAX(p2.id) ")
	Stream<Object[]> getAllLatestPublications();

	/**
	 * Récupérer les dernières publications pour les espaces dont la publication est active avec la date de première publication pour l'espace.
	 *
	 * @return stream des tableaux d'objects : - Object[0] la dernière publication pour chaque espace - Object[1] la date de première publication calculée pour l'espace.
	 */
	@Query("SELECT p, MIN(p2.creationDate) " + "FROM PublicationEntity AS p, PublicationEntity AS p2 " + "WHERE p.espaceOrganisation.id = p2.espaceOrganisation.id "
			+ "AND p.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE " + "AND p2.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE "
			+ "AND p.espaceOrganisation.publicationEnabled = true " + "AND p2.creationDate < ?1 " + "GROUP BY p HAVING p.id = MAX(p2.id) ")
	Stream<Object[]> getAllLatestPublicationsBefore(Date date);

	/**
	 * Récupérer la liste des publications d'un espace organisation.
	 *
	 * @param espaceId
	 *            id de l'espace organisation.
	 * @return la liste de publications.
	 */
	List<PublicationEntity> findByEspaceOrganisationId(Long espaceId);

	/**
	 * Récupérer l'historique des publications d'un espace organisation.
	 *
	 * @param espaceId
	 *            id de l'espace organisation.
	 * @return historique des publication.
	 */
	List<PublicationEntity> findAllByEspaceOrganisationIdAndStatutNotOrderByIdDesc(Long espaceId, StatutPublicationEnum statut);

	PublicationEntity findFirstByEspaceOrganisationIdOrderByIdDesc(Long currentEspaceOrganisationId);

	PublicationEntity findFirstByEspaceOrganisationIdAndStatutNotOrderByIdAsc(Long espaceId, StatutPublicationEnum depubliee);
	
	/**
	 * Récupère la 1ère publication d'un client
	 */
	@Query(value = "select  p.* from pub_client pc " + 
			"inner join publication p ON p.id=pc.publication_id " + 
			"where pc.espace_organisation_id = ?1 and pc.organisation_id = ?2 " +
			"order by p.date_creation ASC limit 1", nativeQuery = true)
	PublicationEntity findPremierePublicationClient(Long espaceOrganisationId,Long organisationId);
	
	/**
	 * Récupère la 1ère publication d'une affiliation
	 * @param espaceOrganisationId
	 * @param organisationId
	 * @return
	 */
	@Query(value = "select  p.* from pub_association pc " + 
			"inner join publication p ON p.id=pc.publication_id " + 
			"where pc.espace_organisation_id = ?1 and pc.organisation_id = ?2 " +
			"order by p.date_creation ASC limit 1", nativeQuery = true)
	PublicationEntity findPremierePublicationAssoPro(Long espaceOrganisationId,Long organisationId);
	
	List<PublicationEntity> findAllByEspaceOrganisationIdOrderByIdDesc(Long espaceId);
	
	
	/**
	 * Récupère la dernière publication d'un client
	 */
	@Query(value = "select  p.* from pub_client pc " + 
			"inner join publication p ON p.id=pc.publication_id " + 
			"where pc.espace_organisation_id = ?1 and pc.organisation_id = ?2 " +
			"order by p.date_creation DESC limit 1", nativeQuery = true)
	PublicationEntity findLastPublicationClient(Long espaceOrganisationId,Long organisationId);
}
