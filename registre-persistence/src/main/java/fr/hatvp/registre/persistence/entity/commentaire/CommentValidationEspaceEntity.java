/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.commentaire;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;

/**
 * 
 * Classes des commentaires des inscriptions espaces organisations.
 * @version $Revision$ $Date${0xD}
 */
@Entity
@Table(name = "commentaire_validation_espace")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "commentaire_validation_espace_seq",
        allocationSize = 1)
public class CommentValidationEspaceEntity
    extends AbstractCommentEntity
{

    /** Clé de sérialisation. */
    private static final long serialVersionUID = 7295570714791117737L;

    @ManyToOne
    @JoinColumn(nullable = false, name = "inscription_espace_id")
    private InscriptionEspaceEntity inscriptionEspaceEntity;

    /**
     * Accesseur en lecture du champ <code>inscriptionEspaceEntity</code>.
     * @return le champ <code>inscriptionEspaceEntity</code>.
     */
    public InscriptionEspaceEntity getInscriptionEspaceEntity()
    {
        return this.inscriptionEspaceEntity;
    }

    /**
     * Accesseur en écriture du champ <code>inscriptionEspaceEntity</code>.
     * @param inscriptionEspaceEntity la valeur à écrire dans <code>inscriptionEspaceEntity</code>.
     */
    public void setInscriptionEspaceEntity(final InscriptionEspaceEntity inscriptionEspaceEntity)
    {
        this.inscriptionEspaceEntity = inscriptionEspaceEntity;
    }

}
