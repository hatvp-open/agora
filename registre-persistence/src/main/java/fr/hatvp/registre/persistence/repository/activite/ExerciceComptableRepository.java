/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.activite;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;

public interface ExerciceComptableRepository extends JpaRepository<ExerciceComptableEntity, Long> {

	List<ExerciceComptableEntity> findAllByEspaceOrganisationId(Long id);

	ExerciceComptableEntity findOneBydateDebutAndEspaceOrganisationId(LocalDate dateDebut, Long id);
	ExerciceComptableEntity findOneBydateDebutAndDateFinAndEspaceOrganisationId(LocalDate dateDebut, LocalDate dateFin, Long id);

	List<ExerciceComptableEntity> findAllByDateFin(LocalDate dateFin);

	List<ExerciceComptableEntity> findByDateFinBefore(LocalDate dateFin);

	//ON FILTRE SUR LES ESPACES QUI SONT VALIDES SUR LE BO ET QUI ONT UNE FICHE IDENTITE PUBLIEE ET POUR QUI LES RELANCES NE SONT PAS BLOQUEES
	@Query(value = "SELECT * " + "FROM exercice_comptable AS ex " + "WHERE ex.id IN (SELECT ex.ex_id " + "FROM (SELECT MAX(ex.date_fin), MAX(ex.id) AS ex_id, esp.id "
			+ "FROM exercice_comptable AS ex " + "LEFT JOIN espace_organisation AS esp ON esp.id = ex.espace_organisation_id " 
			+ "LEFT JOIN publication AS pub ON pub.espace_organisation_id = esp.id WHERE ex.date_fin <= ?1 AND ex.date_fin > (select MIN(date_creation) from publication pub2 where pub.id = pub2.id) "
			+ "AND esp.statut_creation_bo='ACCEPTEE' AND esp.espace_actif = true AND esp.relances_bloquees= false AND pub.statut in ('REPUBLIEE','PUBLIEE') AND esp.publication_enabled = true " 
			+ "GROUP BY esp.id " + "ORDER BY esp.id) AS ex) " + "AND ex.blacklisted = false "
			+ "AND (ex.declaration_done IS NULL OR ex.declaration_done = false)", nativeQuery = true)
	List<ExerciceComptableEntity> findLastExerciceForEachEspaceByDateFinBefore(LocalDate dateFin);

	List<ExerciceComptableEntity> findAllByBlacklisted(boolean b);
    List<ExerciceComptableEntity> findAllByBlacklistedAndAfficheBlacklist(boolean b, boolean bb);

    /**
     * Récupère les dates de début et de fin d'un exercice comptable selon l'id organisation
     * @param espaceOrganisationId
     * @return une date de début d'exercice comptable
     */
    @Query(value = "SELECT * from exercice_comptable e WHERE e.espace_organisation_id = ?1  AND current_date between e.date_debut and e.date_fin",nativeQuery = true)
	ExerciceComptableEntity findDateExerciceCourant(Long espaceOrganisationId);

    /**
     * Récupère les dates de début et fin d'un exercice selon un id et une date
     * @param espaceOrganisationId
     * @param dateDebut
     * @return les dates de début et fin d'un exercice
     */
    @Query(value="SELECT * from exercice_comptable e WHERE e.espace_organisation_id = ?1 and date_fin = ?2",nativeQuery = true)
    ExerciceComptableEntity findDateExercicePrecedent (Long espaceOrganisationId, LocalDate dateFin);

    /**
     * Récupère la date de début la plus ancienne des exercice comptable blacklisté d'une organisation
     * @param espaceOrganisationId
     * @param typeBlacklistage1
     * @param typeBlacklistage2
     * @return
     */
    @Query(value="SELECT MIN(e.date_debut) from exercice_comptable e WHERE e.espace_organisation_id= ?1 and e.id IN (SELECT p.exercice_comptable_id from publication_relance p WHERE niveau = 'NBL' and type=?2 OR type = ?3)",nativeQuery = true)
    LocalDate findMaxdateDebutExercicesBlacklisted(Long espaceOrganisationId, String typeBlacklistage1, String typeBlacklistage2);

    /**
     * Récupère la date de fin la plus récente des exercice comptable blacklisté d'une organisation
     * @param espaceOrganisationId
     * @param typeBlacklistage1
     * @param typeBlacklistage1
     * @return
     */
    @Query(value="SELECT MAX(e.date_fin) from exercice_comptable e WHERE e.espace_organisation_id= ?1 and e.id IN (SELECT p.exercice_comptable_id from publication_relance p WHERE niveau = 'NBL' and type=?2 OR type = ?3)",nativeQuery = true)
    LocalDate findMaxdateFinExercicesBlacklisted(Long espaceOrganisationId, String typeBlacklistage1, String typeBlacklistage2);

    /**
     * 
     * @param espaceId
     * @param dateCessation
     * @param listStatutPublication
     * @return un des exercices publié après la date de demande de cessation s'il existe
     */
    ExerciceComptableEntity findFirstByEspaceOrganisationIdAndDateDebutAfterAndStatutIn(Long espaceId, LocalDate dateCessation, List<StatutPublicationEnum> listStatutPublication);

    ExerciceComptableEntity findFirstByEspaceOrganisationIdOrderByDateDebutDesc(Long espaceId);
    
    
    /**
     * 
     * @param dateDebut
     * @param dateFin
     * @return nombre de représentants ayant déclaré des activités sur la période
     */
    @Query(value="select count (distinct espace_organisation_id) from exercice_comptable "
    		+ "where id in (select distinct exercice_comptable_id from activite_representation_interet "
    						+ "where id in (select distinct activite_id from publication_activite "
    										+ "where date_creation >= ?1 and date_creation < ?2))",nativeQuery = true)
    Integer getNbRIAyantDeclareActiviteSurPeriode(Date dateDebut, Date dateFin);
}
