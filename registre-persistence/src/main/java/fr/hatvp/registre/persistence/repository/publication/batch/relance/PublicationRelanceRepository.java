/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.publication.batch.relance;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.hatvp.registre.commons.lists.PublicationRelanceNiveauEnum;
import fr.hatvp.registre.commons.lists.PublicationRelanceTypeEnum;
import fr.hatvp.registre.persistence.entity.activite.ExerciceComptableEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.publication.batch.relance.PublicationRelanceEntity;

public interface PublicationRelanceRepository extends JpaRepository<PublicationRelanceEntity, Long> {

	List<PublicationRelanceEntity> findAllByEnvoyeFalse();

	@Query(value = "select p.type from publication_relance p\n" +
        "where exercice_comptable_id in \n" +
        "(select ex.id from exercice_comptable ex where ex.espace_organisation_id= ?1"+
        " AND ?2 between ex.date_debut and ex.date_fin) and p.niveau = 'NBL'",nativeQuery = true)
	String findTypeByIdAndDate(Long Id, LocalDate dateExerciceComptable);

	List<PublicationRelanceEntity> findByExerciceComptable(ExerciceComptableEntity exerciceComptableEntity);

	PublicationRelanceEntity findOneByExerciceComptableAndNiveauAndEnvoye(ExerciceComptableEntity exerciceComptableEntity,
			PublicationRelanceNiveauEnum nbl,boolean envoye);

    @Query(value = "select distinct e from EspaceOrganisationEntity as e, PublicationRelanceEntity as p, ExerciceComptableEntity ec where p.niveau = 'NBL' and p.type = :keyword and e.id = ec.espaceOrganisation.id and ec.id = p.exerciceComptable.id and e.id in (select ex.espaceOrganisation.id from ExerciceComptableEntity AS ex where ex.blacklisted = true and ex.dateFin in (select MAX(exe.dateFin) from ExerciceComptableEntity AS exe where exe.dateFin < current_date and exe.espaceOrganisation.id = ex.espaceOrganisation.id group by exe.espaceOrganisation.id))")
    Page<EspaceOrganisationEntity> findByBlacklistRechercheRaisonBlacklist(@Param("keyword") PublicationRelanceTypeEnum keyword, Pageable pageable);

}
