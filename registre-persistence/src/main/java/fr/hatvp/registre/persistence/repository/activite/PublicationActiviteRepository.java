/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.activite;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.publication.activite.PublicationActiviteEntity;

public interface PublicationActiviteRepository extends JpaRepository<PublicationActiviteEntity, Long> {

	List<PublicationActiviteEntity> findAllByActiviteIdAndStatutNotOrderByIdDesc(Long activiteId, StatutPublicationEnum statut);

	PublicationActiviteEntity findFirstByActiviteIdOrderByCreationDateDesc(Long activiteId);
	
	Optional<PublicationActiviteEntity> findFirstByActivite_ExerciceComptable_EspaceOrganisationIdAndStatutNotOrderByIdDesc(Long espaceOrganisationId, StatutPublicationEnum statut);

	/**
	 * Récupérer les dernières publications pour les espaces qui ont publié des activites avec la date de dernière publication des activités.
	 * 
	 * @return stream des tableaux d'objects : - Object[0] la dernière publication pour chaque activite - Object[1] la date de derniere publication calculée pour l'activite.
	 */
	@Query("SELECT p, MAX(p2.creationDate) " + "FROM PublicationActiviteEntity AS p, PublicationActiviteEntity AS p2 " + "WHERE p.activite.id = p2.activite.id "
			+ "AND p.activite.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE "
			+ "AND p.activite.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.SUPPRIMEE "
			+ "AND p2.activite.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE " + "AND p.activite.exerciceComptable.espaceOrganisation.publicationEnabled = true "
			+ "GROUP BY p HAVING p.id = MAX(p2.id) ORDER BY p.creationDate DESC")
	Stream<Object[]> getAllLatestPublicationsActivites();


    /**
     * Récupérer les dernières publications pour les espaces qui ont publié des activites avec la date de dernière publication des activités.
     *
     * @return stream des tableaux d'objects : - Object[0] la dernière publication pour chaque activite - Object[1] la date de derniere publication calculée pour l'activite.
     */
    @Query("SELECT count(distinct p.activite.id) FROM PublicationActiviteEntity AS p WHERE "
        + "p.activite.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE " 
        + "AND p.activite.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.SUPPRIMEE "
    		+ "AND p.activite.exerciceComptable.espaceOrganisation.publicationEnabled = true ")
    Integer getCountPublicationsActivites();
	/**
	 * Récupérer les dernières publications pour les espaces qui ont publié des activites avec la date de dernière publication des activités.
	 * 
	 * @return stream des tableaux d'objects : - Object[0] la dernière publication pour chaque espace - Object[1] la date de première publication calculée pour l'espace.
	 */
	@Query("SELECT p, MAX(p2.creationDate) " + "FROM PublicationActiviteEntity AS p, PublicationActiviteEntity AS p2 " + "WHERE p.activite.exerciceComptable.espaceOrganisation.id = p2.activite.exerciceComptable.espaceOrganisation.id "
			+ "AND p.activite.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE "
			+ "AND p.activite.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.SUPPRIMEE "
			+ "AND p2.activite.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE " + "AND p.activite.exerciceComptable.espaceOrganisation.publicationEnabled = true "
			+ "GROUP BY p HAVING p.id = MAX(p2.id) ")
	Stream<Object[]> getAllLatestPublicationsActivitesByEspace();

	
	/**
	 * Récupérer la dernière publication pour  l'espace passé en parametre.
	 * 
	 * @return PublicationActiviteEntity
	 */
	@Query("SELECT p FROM PublicationActiviteEntity AS p WHERE p.activite.exerciceComptable.espaceOrganisation.id = ?1 "
			+" AND p.id = (select MAX(p2.id) from  PublicationActiviteEntity p2 where p2.activite.exerciceComptable.espaceOrganisation.id = ?1 )"
			+ "AND p.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE "
			+ "AND p.activite.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.SUPPRIMEE ")
	PublicationActiviteEntity getLatestPublicationActivite(Long id);
	
	/**
	 * Récupérer la dernière publication pour  l'exercice en parametre.
	 * 
	 * @return PublicationActiviteEntity
	 */
	@Query("SELECT p FROM PublicationActiviteEntity AS p WHERE p.activite.exerciceComptable.id = ?1 "
			+" AND p.id = (select MIN(p2.id) from  PublicationActiviteEntity p2 where p2.activite.exerciceComptable.id = ?1 )"
			+ "AND p.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.DEPUBLIEE "
			+ "AND p.activite.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.SUPPRIMEE ")
	PublicationActiviteEntity getFirstPublicationActivite(Long id);

	@Query("SELECT MAX(p.creationDate) FROM PublicationActiviteEntity AS p WHERE p.activite.id = ?1 " +
        "AND p.statut = fr.hatvp.registre.commons.lists.StatutPublicationEnum.MAJ_PUBLIEE")
	String getLatestPublicationDate(Long activiteId);

    @Query("SELECT MIN(p.creationDate) FROM PublicationActiviteEntity AS p WHERE p.activite.id = ?1")
    String getFirstPublicationDate(Long activiteId);

    Integer countByActivite_ExerciceComptableIdAndStatutNotOrderByIdDesc(long exerciceId, StatutPublicationEnum statut);

    /**
     * 
     * @param dateDebut
     * @param dateFin
     * @return nombre de fiches d’activités déposées par les représentants d’intérêts sur la période
     */
	@Query(value="SELECT COUNT(distinct p.activite.id) FROM PublicationActiviteEntity AS p WHERE p.creationDate >= ?1 AND p.creationDate < ?2 AND p.activite.statut != fr.hatvp.registre.commons.lists.StatutPublicationEnum.SUPPRIMEE ")
	Integer getNbFicheActiviteDeposeesSurPeriode(Date dateDebut, Date dateFin);
	
	
	List<PublicationActiviteEntity> findByActiviteId(Long activiteId);
}
