/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.espace.DesinscriptionEntity;
import fr.hatvp.registre.persistence.entity.piece.DesinscriptionPieceEntity;

public interface DesinscriptionPieceRepository extends JpaRepository<DesinscriptionPieceEntity, Long> {

    /**
     * Renvoie la liste des pièces versées pour une origine donnée et un espace donné.
     * @return liste des pièces sélectionnées
     */
    Stream<DesinscriptionPieceEntity> findByDesinscription_EspaceOrganisation_IdOrderByDateVersementPieceDesc(Long idEspaceOrganisation);

    List<DesinscriptionPieceEntity> findByDesinscription(DesinscriptionEntity desinscriptionEntity);
}
