/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.entity.activite;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import fr.hatvp.registre.commons.lists.StatutPublicationEnum;
import fr.hatvp.registre.persistence.entity.AbstractCommonEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.ChiffreAffaireEntity;
import fr.hatvp.registre.persistence.entity.nomenclature.MontantDepenseEntity;
import fr.hatvp.registre.persistence.entity.publication.batch.relance.PublicationRelanceEntity;

@Entity
@Audited
@Table(name = "exercice_comptable")
@SequenceGenerator(name = "id_sequence_generator", sequenceName = "exercice_comptable_seq", allocationSize = 1)
public class ExerciceComptableEntity extends AbstractCommonEntity {

	private static final long serialVersionUID = 95182397976403436L;

	/**
	 * Default Constructor
	 */
	public ExerciceComptableEntity() {
		super();
	}

	/**
	 * Constructor with id
	 *
	 * @param id
	 */
	public ExerciceComptableEntity(Long id) {
		super();
		this.setId(id);
	}

	/**
	 * Constructeur de création d'exercice comptable
	 *
	 * @param dateDebut
	 * @param dateFin
	 * @param espaceOrganisation
	 */
	public ExerciceComptableEntity(LocalDate dateDebut, LocalDate dateFin, EspaceOrganisationEntity espaceOrganisation) {
		super();
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.espaceOrganisation = espaceOrganisation;
		this.statut = StatutPublicationEnum.NON_PUBLIEE;
	}

	@Column(name = "date_debut")
	private LocalDate dateDebut;

	@Column(name = "date_fin")
	private LocalDate dateFin;

	@NotAudited
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "nomenclature_chiffre_affaire_id")
	private ChiffreAffaireEntity chiffreAffaire;

	@Column(name = "non_chiffre_affaire")
	private Boolean hasNotChiffreAffaire;

	@NotAudited
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "nomenclature_montant_depense_id")
	private MontantDepenseEntity montantDepense;

	@Column(name = "nombre_salaries")
	private Integer nombreSalaries;

	@OneToMany(mappedBy = "exerciceComptable", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ActiviteRepresentationInteretEntity> activitesRepresentationInteret = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "espace_organisation_id", nullable = false)
	private EspaceOrganisationEntity espaceOrganisation;

	/** Déclaration pour un tiers. */
	@Column(name = "non_declaration_moyens")
	private boolean nonDeclarationMoyens = false;

	/** Ce flag indique si l'exercice doit être publié sur le site institutionnel. */
	@Column(name = "publication_enabled")
	private Boolean publicationEnabled = Boolean.TRUE;

	@Column(name = "statut", nullable = false, columnDefinition = "TEXT")
	@Enumerated(EnumType.STRING)
	private StatutPublicationEnum statut;

	@Column(name = "data_to_publish")
	private Boolean dataToPublish;

	@Column(name = "no_activite")
	private boolean noActivite = false;

	@NotAudited
	@OneToMany(mappedBy = "exerciceComptable", fetch = FetchType.LAZY)
	private List<PublicationRelanceEntity> publicationRelance;

	@Column(name = "blacklisted")
	private boolean blacklisted = false;

	@Column(name = "declaration_done")
	private Boolean declarationDone;

    
    @Column(name = "commentaire", columnDefinition = "TEXT")
	private String commentaire;

    /**
     * permet de ne pas faire descendre sur la blacklist du site un RI blackliste
     */
    @Column(name = "affiche_blacklist")
    private boolean afficheBlacklist;

	/**
	 * @return the dateDebut
	 */
	public LocalDate getDateDebut() {
		return dateDebut;
	}

	/**
	 * @param dateDebut
	 *            the dateDebut to set
	 */
	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	/**
	 * @return the dateFin
	 */
	public LocalDate getDateFin() {
		return dateFin;
	}

	/**
	 * @param dateFin
	 *            the dateFin to set
	 */
	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	/**
	 * @return the chiffreAffaire
	 */
	public ChiffreAffaireEntity getChiffreAffaire() {
		return chiffreAffaire;
	}

	/**
	 * @param chiffreAffaire
	 *            the chiffreAffaire to set
	 */
	public void setChiffreAffaire(ChiffreAffaireEntity chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	/**
	 * @return the hasNotChiffreAffaire
	 */
	public Boolean getHasNotChiffreAffaire() {
		return hasNotChiffreAffaire;
	}

	/**
	 * @param hasNotChiffreAffaire
	 *            the hasNotChiffreAffaire to set
	 */
	public void setHasNotChiffreAffaire(Boolean hasNotChiffreAffaire) {
		this.hasNotChiffreAffaire = hasNotChiffreAffaire;
	}

	/**
	 * @return the montantDepense
	 */
	public MontantDepenseEntity getMontantDepense() {
		return montantDepense;
	}

	/**
	 * @param montantDepense
	 *            the montantDepense to set
	 */
	public void setMontantDepense(MontantDepenseEntity montantDepense) {
		this.montantDepense = montantDepense;
	}

	/**
	 * @return the nombreSalaries
	 */
	public Integer getNombreSalaries() {
		return nombreSalaries;
	}

	/**
	 * @param nombreSalaries
	 *            the nombreSalaries to set
	 */
	public void setNombreSalaries(Integer nombreSalaries) {
		this.nombreSalaries = nombreSalaries;
	}

	/**
	 * @return the activitesRepresentationInteret
	 */
	public List<ActiviteRepresentationInteretEntity> getActivitesRepresentationInteret() {
		return activitesRepresentationInteret;
	}

	/**
	 * @param activitesRepresentationInteret
	 *            the activitesRepresentationInteret to set
	 */
	public void setActivitesRepresentationInteret(List<ActiviteRepresentationInteretEntity> activitesRepresentationInteret) {
		this.activitesRepresentationInteret = activitesRepresentationInteret;
	}

	/**
	 * @return the espaceOrganisation
	 */
	public EspaceOrganisationEntity getEspaceOrganisation() {
		return espaceOrganisation;
	}

	/**
	 * @param espaceOrganisation
	 *            the espaceOrganisation to set
	 */
	public void setEspaceOrganisation(EspaceOrganisationEntity espaceOrganisation) {
		this.espaceOrganisation = espaceOrganisation;
	}

	/**
	 * @return the nonDeclarationMoyens
	 */
	public boolean isNonDeclarationMoyens() {
		return nonDeclarationMoyens;
	}

	/**
	 * @param nonDeclarationMoyens
	 *            the nonDeclarationMoyens to set
	 */
	public void setNonDeclarationMoyens(boolean nonDeclarationMoyens) {
		this.nonDeclarationMoyens = nonDeclarationMoyens;
	}

	/**
	 * @return the publicationEnabled
	 */
	public Boolean getPublicationEnabled() {
		return publicationEnabled;
	}

	/**
	 * @param publicationEnabled
	 *            the publicationEnabled to set
	 */
	public void setPublicationEnabled(Boolean publicationEnabled) {
		this.publicationEnabled = publicationEnabled;
	}

	/**
	 * @return the statut
	 */
	public StatutPublicationEnum getStatut() {
		return statut;
	}

	/**
	 * @param statut
	 *            the statut to set
	 */
	public void setStatut(StatutPublicationEnum statut) {
		this.statut = statut;
	}

	/**
	 * @return the dataToPublish
	 */
	public Boolean getDataToPublish() {
		return dataToPublish;
	}

	/**
	 * @param dataToPublish
	 *            the dataToPublish to set
	 */
	public void setDataToPublish(Boolean dataToPublish) {
		this.dataToPublish = dataToPublish;
	}

	/**
	 * @return the noActivite
	 */
	public boolean isNoActivite() {
		return noActivite;
	}

	/**
	 * @param noActivite
	 *            the noActivite to set
	 */
	public void setNoActivite(boolean noActivite) {
		this.noActivite = noActivite;
	}

	/**
	 * @return the publicationRelance
	 */
	public List<PublicationRelanceEntity> getPublicationRelance() {
		return publicationRelance;
	}

	/**
	 * @param publicationRelance
	 *            the publicationRelance to set
	 */
	public void setPublicationRelance(List<PublicationRelanceEntity> publicationRelance) {
		this.publicationRelance = publicationRelance;
	}

	/**
	 * @return the blacklisted
	 */
	public boolean isBlacklisted() {
		return blacklisted;
	}

	/**
	 * @param blacklisted
	 *            the blacklisted to set
	 */
	public void setBlacklisted(boolean blacklisted) {
		this.blacklisted = blacklisted;
	}

	/**
	 * @return the declarationDone
	 */
	public Boolean getDeclarationDone() {
		return declarationDone;
	}

	/**
	 * @param declarationDone
	 *            the declarationDone to set
	 */
	public void setDeclarationDone(Boolean declarationDone) {
		this.declarationDone = declarationDone;
	}


	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

    public boolean isAfficheBlacklist() { return afficheBlacklist;  }

    public void setAfficheBlacklist(boolean afficheBlacklist) { this.afficheBlacklist = afficheBlacklist;  }
}
