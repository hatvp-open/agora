/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.declarant;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.declarant.DeclarantContactEntity;

/**
 * Repository JPA pour l'entité {@link DeclarantContactEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface DeclarantContactRepository
    extends JpaRepository<DeclarantContactEntity, Long>
{
}
