/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hatvp.registre.persistence.entity.espace.DirigeantEntity;

/**
 * Repository JPA pour l'entité {@link DirigeantEntity}.
 * @version $Revision$ $Date${0xD}
 */
public interface DirigeantRepository extends JpaRepository<DirigeantEntity, Long> {

    /**
     * Renvoie la liste des dirigeants d'un espace organisation donné.
     * @param espaceOrganisationId
     * @return la liste des dirigeants de l'espace.
     */
    List<DirigeantEntity> findByEspaceOrganisationId(Long espaceOrganisationId);
    
    /**
     * Retrouve un dirigeant selon sont identifiant et l'espace auquel il appartient.
     * @param dirigeantId
     * @param espaceOrganisationId
     * @return
     */
    DirigeantEntity findByIdAndEspaceOrganisationId(Long dirigeantId, Long espaceOrganisationId);

}
