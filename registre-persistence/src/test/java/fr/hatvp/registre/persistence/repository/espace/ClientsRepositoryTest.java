/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.persistence.entity.espace.ClientEntity;
import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;
import fr.hatvp.registre.persistence.repository.espace.ClientsRepository;
import fr.hatvp.registre.persistence.repository.espace.EspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;

/**
 * Test de l'interface {@link ClientsRepository}
 * @version $Revision$ $Date${0xD}
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Sql({"classpath:/sql/clientRepositoryTest.sql"})
public class ClientsRepositoryTest
{

    /** Repository à tester. */
    @Autowired private ClientsRepository clientsRepository;
    /** Repository des organisations. */
    @Autowired private OrganisationRepository organisationRepository;
    /** Repository des organisations. */
    @Autowired private EspaceOrganisationRepository espaceOrganisationRepository;

    /** Identifiant d'espace organisation utilisé pour les tests. */
    private final Long ESPACE_ORGANISATION_ID_TEST = 1L;
    /** Identifiant d'espace organisation utilisé pour les tests. */
    private final Long ESPACE_ORGANISATION_ID_TEST2 = 2L;
    /** Identifiant d'organisation utilisé pour les tests. */
    private final Long ORGANISATION_ID_TEST = 1L;
    /** Identifiant du client utilisé pour les tests. */
    private final Long CLIENT_ID_TEST = 1L;

    /**
     * Finalisation des données de tests.
     */
    @After
    public void endTest()
    {
        this.espaceOrganisationRepository.deleteAll();
        this.organisationRepository.deleteAll();
    }

    /**
     * Test de la méthode findByEspaceOrganisationIdAndOrganisationId.
     */
    @Test
    public void testFindByEspaceOrganisationIdAndOrganisationId()
    {
        final ClientEntity clientEntity = this.clientsRepository
                .findByEspaceOrganisationIdAndOrganisationId(this.ESPACE_ORGANISATION_ID_TEST2,
                        this.ORGANISATION_ID_TEST);

        Assert.assertNotNull(clientEntity);
    }

    /**
     * Test de la méthode findFirstByEspaceOrganisationIdOrderByIdDesc.
     */
    @Test
    public void testFindByEspaceOrganisationId()
    {
        final List<ClientEntity> clientEntity = this.clientsRepository
                .findByEspaceOrganisationId(this.ESPACE_ORGANISATION_ID_TEST);

        Assert.assertNotNull(clientEntity);
        Assert.assertEquals(2, clientEntity.size());
    }

    /**
     * Test de la méthode delete
     */
    @Test
    public void testDeleteClient()
    {
        final ClientEntity clientEntity = this.clientsRepository.findOne(this.CLIENT_ID_TEST);

        Assert.assertNotNull(clientEntity);

        this.clientsRepository.delete(clientEntity);

        final ClientEntity clientEntity1 = this.clientsRepository.findOne(this.CLIENT_ID_TEST);

        Assert.assertNull(clientEntity1);

        /* Vérifier que l'orgaisation n'a pas été supprimer en supprimant le client */
        final OrganisationEntity organisationEntity = this.organisationRepository
                .findOne(this.ORGANISATION_ID_TEST);
        Assert.assertNotNull(organisationEntity);

        /* Vérifier que l'espace orgaisation n'a pas été supprimer en supprimant le client */
        final EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository
                .findOne(this.ESPACE_ORGANISATION_ID_TEST);
        Assert.assertNotNull(espaceOrganisationEntity);
    }
    
    /**
     * Test de la méthode findByIdAndEspaceOrganisationId.
     */
    @Test
    public void testFindByIdAndEspaceOrganisationIdNonExistant()    {
    	ClientEntity clientEntity = this.clientsRepository.findByIdAndEspaceOrganisationId(this.CLIENT_ID_TEST, this.ESPACE_ORGANISATION_ID_TEST);

        Assert.assertNull(clientEntity);
    }
    /**
     * Test de la méthode findByIdAndEspaceOrganisationId.
     */
    @Test
    public void testFindByIdAndEspaceOrganisationIdExistant()    {
    	ClientEntity clientEntity = this.clientsRepository.findByIdAndEspaceOrganisationId(this.CLIENT_ID_TEST, this.ESPACE_ORGANISATION_ID_TEST2);

        Assert.assertNotNull(clientEntity);
    }    
   
    /**
     * Test de la méthode countClientsActifsByEspaceId.
     * client actif existant
     */
    @Test
    public void countClientsActifsByEspaceIdExistant()    {
    	Long count = this.clientsRepository.countClientsActifsByEspaceId(this.ESPACE_ORGANISATION_ID_TEST2);

        Assert.assertEquals(1L, count.longValue());
    }
    
    /**
     * Test de la méthode findByIdAndEspaceOrganisationId.
     */
    @Test
    public void countAnciensClientsByEspaceIdExistant()    {
    	Long count = this.clientsRepository.countAnciensClientsByEspaceId(this.ESPACE_ORGANISATION_ID_TEST);

    	Assert.assertEquals(1L, count.longValue());
    }

}
