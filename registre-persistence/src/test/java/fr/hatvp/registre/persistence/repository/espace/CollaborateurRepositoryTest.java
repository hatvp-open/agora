/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.persistence.entity.espace.CollaborateurEntity;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

/**
 * Test de l'interface {@link ClientsRepository}
 *
 * @version $Revision$ $Date${0xD}
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Sql({"classpath:/sql/collaborateurRepositoryTest.sql"})
public class CollaborateurRepositoryTest{
	
    private final Long COLLABORATEUR_ID_TEST = 1L;
    private final Long DECLARANT_ID_TEST = 2L;
    private final Long ESPACE_ORGANISATION_ID_TEST = 1L;    

    /** Repository à tester. */
    @Autowired private CollaborateurRepository collaborateurRepository;
    
    




    /**
     * Finalisation des données de tests.
     */
    @After
    public void endTest()    {
        this.collaborateurRepository.deleteAll();
    }

    
    /**
     * Test de la méthode findByIdAndEspaceOrganisationId.
     */
    @Test
    public void testFindByIdAndEspaceOrganisationIdExistant()    {
    	CollaborateurEntity entity = this.collaborateurRepository.findByIdAndEspaceOrganisationId(this.COLLABORATEUR_ID_TEST, this.ESPACE_ORGANISATION_ID_TEST);

        Assert.assertNotNull(entity);
    }
    
    
    /**
     * Test de la méthode findByEspaceOrganisationId.
     */
    @Test
    public void tesFindByEspaceOrganisationIdExistant()    {
    	List<CollaborateurEntity> listEntity = this.collaborateurRepository.findByEspaceOrganisationId(this.ESPACE_ORGANISATION_ID_TEST);

        Assert.assertEquals(1, listEntity.size());
    }

}
