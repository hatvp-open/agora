/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.commons.lists.backoffice.StatutDemandeOrganisationEnum;
import fr.hatvp.registre.persistence.entity.espace.DemandeOrganisationEntity;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestRepositoryConfig.class }, loader = AnnotationConfigContextLoader.class)
@Sql({ "classpath:/sql/backoffice/demandeOrganisationRepositoryTest.sql" })
public class DemandeOrganisationRepositoryTest {

	@Autowired
	private DemandeOrganisationRepository demandeOrganisationRepository;

	@After
	public void endTest() {
		demandeOrganisationRepository.deleteAll();
	}

	@Test
	public void testFindByStatus() {

		final int demandeOrganisationRepositoryTestSqlTotalPage = 3;
		final int demandeOrganisationRepositoryTestSqlTotalElements = 28;
		final int pageNumber = 1;

		final Pageable page = new PageRequest(pageNumber, 10, Sort.Direction.ASC, "id");

		final Page<DemandeOrganisationEntity> pagedDemandeOrganisationEntity = demandeOrganisationRepository
				.findByStatut(StatutDemandeOrganisationEnum.A_TRAITER, page);

		assertEquals(pagedDemandeOrganisationEntity.getNumber(), pageNumber);
		assertEquals(pagedDemandeOrganisationEntity.getNumberOfElements(), page.getPageSize());
		assertEquals(pagedDemandeOrganisationEntity.getTotalPages(), demandeOrganisationRepositoryTestSqlTotalPage);
		assertEquals(pagedDemandeOrganisationEntity.getTotalElements(),
				demandeOrganisationRepositoryTestSqlTotalElements);
	}

}
