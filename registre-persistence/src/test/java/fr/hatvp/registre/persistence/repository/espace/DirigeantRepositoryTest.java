/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.persistence.entity.espace.DirigeantEntity;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

/**
 * Test de l'interface {@link ClientsRepository}
 *
 * @version $Revision$ $Date${0xD}
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Sql({"classpath:/sql/dirigeantRepositoryTest.sql"})
public class DirigeantRepositoryTest{
	
    private final Long DECLARANT_ID_TEST = 2L;
    private final Long ESPACE_ORGANISATION_ID_TEST = 1L; 
    private final Long DIRIGEANT_ID_TEST = 1L;

    /** Repository à tester. */
    @Autowired private DirigeantRepository dirigeantRepository;    
    
    /**
     * Finalisation des données de tests.
     */
    @After
    public void endTest()    {
        this.dirigeantRepository.deleteAll();    }

    
    /**
     * Test de la méthode findByIdAndEspaceOrganisationId.
     */
    @Test
    public void testFindByIdAndEspaceOrganisationIdExistant()    {
    	List<DirigeantEntity> lisEntity =  dirigeantRepository.findByEspaceOrganisationId(this.ESPACE_ORGANISATION_ID_TEST);

        Assert.assertEquals(1, lisEntity.size());
    }
    
    
    /**
     * Test de la méthode findByEspaceOrganisationId.
     */
    @Test
    public void tesFindByEspaceOrganisationIdExistant()    {
    	DirigeantEntity entity = this.dirigeantRepository.findByIdAndEspaceOrganisationId(this.DIRIGEANT_ID_TEST, this.ESPACE_ORGANISATION_ID_TEST);

        Assert.assertNotNull(entity);
    }

}
