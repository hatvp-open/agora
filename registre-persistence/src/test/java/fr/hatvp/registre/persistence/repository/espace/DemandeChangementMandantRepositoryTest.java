/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.Date;
import java.util.Optional;

import org.junit.After;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.commons.lists.ChangementContactEnum;
import fr.hatvp.registre.persistence.entity.espace.DemandeChangementMandantEntity;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

/***
 * Test de l'interface{
 *
 * @version $Revision$ $Date${0xD}
 */
@Ignore
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestRepositoryConfig.class }, loader = AnnotationConfigContextLoader.class)
@Sql({ "classpath:/sql/demandeChangementMandantRepositoryTest.sql" })
public class DemandeChangementMandantRepositoryTest {

	/**
	 * Repository à tester.
	 */
	@Autowired
	private DemandeChangementMandantRepository demandeChangementMandantRepository;

	/**
	 * National Id pour les tests.
	 */
	private final Long ID_TEST_1 = 1L;
	private final Long ID_TEST_2 = 2L;
	private final Long ID_TEST_3 = 3L;
	private final String SIREN_TEST = "123456789";
	private final String SIREN_TEST_2 = "987654321";
	private final String RNA_UCASE_TEST = "W123456789";
	private final String RNA_LCASE_TEST = "w123456789";
	private final String HATVP_NUMBER_UCASE_TEST = "H123456789";
	private final String HATVP_NUMBER_LCASE_TEST = "h123456789";

	/**
	 * Finalisation des données de tests.
	 */
	@After
	public void endTest() {
		this.demandeChangementMandantRepository.deleteAll();
	}

	/**
	 * Test de la méthode findFirstBySiren.
	 */
	@Test
	public void testFindByIdExists() {
		final DemandeChangementMandantEntity result1 = this.demandeChangementMandantRepository.findOne(this.ID_TEST_1);
		Assert.assertNotNull(result1);
		final DemandeChangementMandantEntity result2 = this.demandeChangementMandantRepository.findOne(this.ID_TEST_2);
		Assert.assertNotNull(result2);
		final DemandeChangementMandantEntity result3 = this.demandeChangementMandantRepository.findOne(this.ID_TEST_3);
		Assert.assertNotNull(result3);

		Assert.assertEquals(ChangementContactEnum.REMPLACEMENT_OPERATIONNEL, result1.getTypeDemande());
		Assert.assertEquals(ChangementContactEnum.AJOUT_OPERATIONNEL, result2.getTypeDemande());
		Assert.assertEquals(ChangementContactEnum.CHANGEMENT_REPRESENTANT, result3.getTypeDemande());
	}

	@Test
	public void testFindByDateRejetIsNotNull() {
		final Page<DemandeChangementMandantEntity> result1 = this.demandeChangementMandantRepository.findAllByDateRejetIsNotNull(buildPageRequest());
		Assert.assertTrue(Optional.of(result1).isPresent());

		Assert.assertEquals(ChangementContactEnum.AJOUT_OPERATIONNEL, result1.getContent().get(0).getTypeDemande());
	}

	@Test
	public void testFindByDateValidationIsNotNull() {
		final Page<DemandeChangementMandantEntity> result1 = this.demandeChangementMandantRepository.findAllByDateValidationIsNotNull(buildPageRequest());
		Assert.assertFalse(result1.getContent().isEmpty());

		Assert.assertEquals(ChangementContactEnum.CHANGEMENT_REPRESENTANT, result1.getContent().get(0).getTypeDemande());
	}

	@Test
	public void testFindByDateValidationIsNullAndDateRejetIsNull() {
		final Page<DemandeChangementMandantEntity> result1 = this.demandeChangementMandantRepository.findAllByDateValidationIsNullAndDateRejetIsNull(buildPageRequest());
		Assert.assertFalse(result1.getContent().isEmpty());

		Assert.assertEquals(ChangementContactEnum.REMPLACEMENT_OPERATIONNEL, result1.getContent().get(0).getTypeDemande());

	}

	@Test
	public void testSave() {
		DemandeChangementMandantEntity result1 = demandeChangementMandantRepository.findOne(ID_TEST_1);
		Assert.assertTrue(result1.getDateRejet() == null);
		result1.setDateRejet(new Date());
		DemandeChangementMandantEntity result2 = demandeChangementMandantRepository.save(result1);
		Assert.assertFalse(result1.getDateRejet() == null);
		Assert.assertFalse(result2.getDateRejet() == null);
		DemandeChangementMandantEntity result3 = demandeChangementMandantRepository.findOne(ID_TEST_1);
		Assert.assertFalse(result3.getDateRejet() == null);
	}

	private PageRequest buildPageRequest() {
		return new PageRequest(2, 10, Sort.Direction.ASC, "dateCreation");

	}

}