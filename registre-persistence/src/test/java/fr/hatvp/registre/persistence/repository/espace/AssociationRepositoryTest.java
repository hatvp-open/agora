/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.persistence.entity.espace.AssociationEntity;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

/**
 * Test de l'interface {@link fr.hatvp.registre.persistence.repository.espace.AssoAppartenanceRepository}
 * @version $Revision$ $Date${0xD}
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestRepositoryConfig.class }, loader = AnnotationConfigContextLoader.class)
@Sql({ "classpath:/sql/associationRepositoryTest.sql" })
public class AssociationRepositoryTest {

	/** Repository des associations. */
	@Autowired
	private AssoAppartenanceRepository assoAppartenanceRepository;

	/** Identifiant du client utilisé pour les tests. */
	private final Long ASSO_ID_TEST = 1L;

	/** Identifiant du client utilisé pour les tests. */
	private final Long ESP_ID_TEST = 1L;

	/** Test de la méthode {@link AssoAppartenanceRepository#findByEspaceOrganisationId(Long)}. */
	@Test
	public void testFindByEspaceOrganisationId() {
		final List<AssociationEntity> associationEntities = this.assoAppartenanceRepository.findByEspaceOrganisationId(this.ASSO_ID_TEST);

		Assert.assertNotNull(associationEntities);
		Assert.assertEquals(1, associationEntities.size());

		final List<AssociationEntity> entities = this.assoAppartenanceRepository.findAll();

		Assert.assertNotNull(entities);
		Assert.assertEquals(2, entities.size());

	}

	/** Test de la méthode {@link AssoAppartenanceRepository#findByIdAndEspaceOrganisationId(Long, Long)}. */
	@Test
	public void testFindByIdAndEspaceOrganisationId() {
		AssociationEntity associationEntities = this.assoAppartenanceRepository.findByIdAndEspaceOrganisationId(ASSO_ID_TEST, ESP_ID_TEST);

		assertThat(associationEntities.getId()).isEqualTo(ASSO_ID_TEST);
		assertThat(associationEntities.getEspaceOrganisation().getId()).isEqualTo(ESP_ID_TEST);
	}
}
