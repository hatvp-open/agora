/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.persistence.entity.commentaire.CommentDemandeEntity;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentDemandeRepository;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

/**
 * Repository de l'entite {@link CommentDemandeRepository}.
 * @version $Revision$ $Date${0xD}
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Sql({"classpath:/sql/backoffice/commentDemandeRepositoryTest.sql"})
public class CommentDemandeRepositoryTest
{

    /**
     * Constante de TEST.
     */
    private static final Long DEMANDE_ID = 1L;

    /**
     * Repository de {@link CommentDemandeEntity}
     */
    @Autowired
    private CommentDemandeRepository commentDemandeRepository;

    /**
     * Finalisation des données de tests.
     */
    @After
    public void endTest()
    {
        this.commentDemandeRepository.deleteAll();
    }

    /**
     * Test de la méthode {@link CommentDemandeRepository#findByDemandeOrganisationEntityId(Long)}.
     */
    @Test
    public void testFinById()
    {
        final List<CommentDemandeEntity> entity = this.commentDemandeRepository
                .findByDemandeOrganisationEntityId(DEMANDE_ID);
        Assert.assertNotNull(entity);
        Assert.assertEquals(DEMANDE_ID, entity.get(0).getId());
    }

}
