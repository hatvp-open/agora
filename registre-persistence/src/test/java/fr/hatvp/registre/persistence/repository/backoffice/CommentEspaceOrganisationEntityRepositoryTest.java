/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice;

import fr.hatvp.registre.persistence.entity.commentaire.CommentEspaceOrganisationEntity;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentEspaceOrganisationRepository;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Repository de l'entite {@link CommentEspaceOrganisationRepository}.
 * @version $Revision$ $Date${0xD}
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Sql({"classpath:/sql/backoffice/commentEspaceOrgaRepositoryTest.sql"})
public class CommentEspaceOrganisationEntityRepositoryTest
{

    /**
     * Constante de TEST.
     */
    private static final Long ESPACE_ORGANISATION_ID = 1L;

    /**
     * Repository de {@link CommentEspaceOrganisationEntity}
     */
    @Autowired
    private CommentEspaceOrganisationRepository commentEspaceOrganisationRepository;

    /**
     * Finalisation des données de tests.
     */
    @After
    public void endTest()
    {
        this.commentEspaceOrganisationRepository.deleteAll();
    }

    /**
     * Test de la méthode
     * {@link CommentEspaceOrganisationRepository#findByEspaceOrganisationEntityId(Long)} (Long)}.
     */
    @Test
    public void testFinById()
    {
        final List<CommentEspaceOrganisationEntity> entity = this.commentEspaceOrganisationRepository
                .findByEspaceOrganisationEntityId(ESPACE_ORGANISATION_ID);
        Assert.assertNotNull(entity);
        Assert.assertEquals(ESPACE_ORGANISATION_ID, entity.get(0).getId());
    }

}
