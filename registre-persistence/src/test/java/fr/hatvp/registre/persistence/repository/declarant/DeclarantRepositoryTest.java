/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.declarant;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.persistence.entity.declarant.DeclarantEntity;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

/**
 * Test de l'interface {@link fr.hatvp.registre.persistence.repository.declarant.DeclarantRepository}
 * @version $Revision$ $Date${0xD}
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestRepositoryConfig.class }, loader = AnnotationConfigContextLoader.class)
@Sql({ "classpath:/sql/declarantRepositoryTest.sql" })
public class DeclarantRepositoryTest {

	/** Repository à tester. */
	@Autowired
	private DeclarantRepository declarantRepository;

	/** Mail utilisé pour les tests. */
	private final String EMAIL_TEST = "test@hatvp.fr";

	/** Mail temporaire utilisé pour les tests. */
	private final String EMAIL_TEMP_TEST = "test_temp@hatvp.fr";

	/** Code confirmation utilisé pour les tests. */
	private final String EMAIL_CODE_CONFIRMATION_TEST = "ffzfzekinfzenf";

	/** Code récupération de compte utilisé pour les tests. */
	private final String RECOVER_CONFIRMATION_CODE_TEST = "ffzfzekinfzenf";

	/**
	 * Finalisation des données de tests.
	 */
	@After
	public void endTest() {
		this.declarantRepository.deleteAll();
	}

	/**
	 * Test de la méthode {@link DeclarantRepository#findByEmailIgnoreCase(String)}.
	 */
	@Test
	public void testFinByEmail() {
		final DeclarantEntity entity = this.declarantRepository.findByEmailIgnoreCase(this.EMAIL_TEST);
		Assert.assertNotNull(entity);
		Assert.assertEquals(this.EMAIL_TEST, entity.getEmail());
	}

	/**
	 * Test de la méthode {@link DeclarantRepository#findByEmailConfirmationCode(String)}.
	 */
	@Test
	public void testFindByEmailConfirmationCode() {
		final DeclarantEntity entity = this.declarantRepository.findByEmailConfirmationCode(this.EMAIL_CODE_CONFIRMATION_TEST);
		Assert.assertNotNull(entity);
		Assert.assertEquals(this.EMAIL_CODE_CONFIRMATION_TEST, entity.getEmailConfirmationCode());
	}

	/**
	 * Test de la méthode {@link DeclarantRepository#findByRecoverConfirmationCode(String)}.
	 */
	@Test
	public void testFindByRecoverConfirmationCode() {
		final DeclarantEntity entity = this.declarantRepository.findByRecoverConfirmationCode(this.RECOVER_CONFIRMATION_CODE_TEST);
		Assert.assertNotNull(entity);
		Assert.assertEquals(this.RECOVER_CONFIRMATION_CODE_TEST, entity.getRecoverConfirmationCode());
	}

	/**
	 * Test de la méthode {@link DeclarantRepository#findByEmailTemp(String)}.
	 */
	@Test
	public void testFindByEmailTemp() {
		final DeclarantEntity entity = this.declarantRepository.findByEmailTemp(this.EMAIL_TEMP_TEST);
		Assert.assertNotNull(entity);
		Assert.assertEquals(this.EMAIL_TEMP_TEST, entity.getEmailTemp());
	}

	/**
	 * Test de la méthode {@link DeclarantRepository#findByEmailOrEmailTemp(String, String)}.
	 */
	@Test
	public void testFindByEmailOrEmailTemp() {
		final DeclarantEntity entityEmail = this.declarantRepository.findByEmailIgnoreCaseOrEmailTempIgnoreCase(this.EMAIL_TEMP_TEST, this.EMAIL_TEMP_TEST);

		final DeclarantEntity entityEmailTemp = this.declarantRepository.findByEmailIgnoreCaseOrEmailTempIgnoreCase(this.EMAIL_TEST, this.EMAIL_TEST);

		Assert.assertNotNull(entityEmail);
		Assert.assertEquals(this.EMAIL_TEMP_TEST, entityEmail.getEmailTemp());
		Assert.assertEquals(this.EMAIL_TEST, entityEmail.getEmail());

		Assert.assertNotNull(entityEmailTemp);
		Assert.assertEquals(this.EMAIL_TEMP_TEST, entityEmailTemp.getEmailTemp());
		Assert.assertEquals(this.EMAIL_TEST, entityEmailTemp.getEmail());
	}
}
