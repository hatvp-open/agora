/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.persistence.entity.espace.InscriptionEspaceEntity;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

/**
 * Classe de test pour le repository {@link InscriptionEspaceRepository}
 * @version $Revision$ $Date${0xD}
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestRepositoryConfig.class }, loader = AnnotationConfigContextLoader.class)
@Sql({ "classpath:sql/inscriptionEspaceRepositoryTest.sql" })
public class InscriptionEspaceRepositoryTest {

	/** Test repository */
	@Autowired
	private InscriptionEspaceRepository inscriptionEspaceRepository;

	/** Identifiant du l'inscription. */
	private final static Long inscriptionId = 1L;
	/** Identifiant du déclarant utilisé pour le test. */
	private final static Long declarantId = 1L;
	/** Identifiant d'espace organisation utilisé pour le test. */
	private final static Long espaceOrganisationId = 1L;

	/**
	 * Test method for {@link fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository#findAllByDeclarantId(java.lang.Long)}.
	 */
	@Test
	public final void testFindAllByDeclarantId() {
		List<InscriptionEspaceEntity> list = this.inscriptionEspaceRepository.findAllByDeclarantId(declarantId);
		Assert.assertEquals(1, list.size());
	}

	/**
	 * Test method for {@link fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository#findAllByDateSuppressionIsNullAndDeclarantId(java.lang.Long)}.
	 */
	@Test
	public final void testFindAllByDateSuppressionIsNullAndDeclarantId() {
		List<InscriptionEspaceEntity> list = this.inscriptionEspaceRepository.findAllByDateSuppressionIsNullAndDeclarantId(declarantId);
		Assert.assertEquals(1, list.size());
	}

	/**
	 * Test method for
	 * {@link fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository#findAllByDateSuppressionIsNullAndDateValidationIsNullAndEspaceOrganisationId(java.lang.Long)}.
	 */
	@Test
	public final void testFindAllByDateSuppressionIsNullAndDateValidationIsNullAndEspaceOrganisationEspaceOrganisationId() {
		List<InscriptionEspaceEntity> list = this.inscriptionEspaceRepository.findAllByDateSuppressionIsNullAndDateValidationIsNullAndEspaceOrganisationId(espaceOrganisationId);
		Assert.assertEquals(1, list.size());
	}

	/**
	 * Test method for {@link fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository#findByDateSuppressionIsNullAndFavoriTrueAndDeclarantId(java.lang.Long)}.
	 */
	@Test
	public final void testFindByEspaceFavoriTrueAndDeclarantId() {
		Optional<InscriptionEspaceEntity> list = this.inscriptionEspaceRepository.findByDateSuppressionIsNullAndFavoriTrueAndDeclarantId(declarantId);
		Assert.assertEquals(true, list.isPresent());
	}

	/**
	 * Test method for
	 * {@link fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository#findByDateSuppressionIsNullAndDeclarantIdAndEspaceOrganisationOrganisationId(java.lang.Long, java.lang.Long)}.
	 */
	@Test
	public final void testFindByDeclarantIdAndEspaceOrganisationOrganisationOrganisationId() {
		InscriptionEspaceEntity entity = this.inscriptionEspaceRepository.findByDateSuppressionIsNullAndDeclarantIdAndEspaceOrganisationOrganisationId(declarantId, espaceOrganisationId);
		Assert.assertNotNull(entity);
	}

	/**
	 * Test method for
	 * {@link fr.hatvp.registre.persistence.repository.espace.InscriptionEspaceRepository#findByDateSuppressionIsNullAndIdAndEspaceOrganisationId(java.lang.Long, java.lang.Long)}.
	 */
	@Test
	public final void testFindByDateSuppressionIsNullAndIdAndEspaceOrganisationId() {
		InscriptionEspaceEntity ins = this.inscriptionEspaceRepository.findByDateSuppressionIsNullAndIdAndEspaceOrganisationId(inscriptionId, espaceOrganisationId);
		assertThat(ins).isNotNull();
		assertThat(ins.getId()).isEqualTo(inscriptionId);
		assertThat(ins.getEspaceOrganisation().getId()).isEqualTo(espaceOrganisationId);
	}

}
