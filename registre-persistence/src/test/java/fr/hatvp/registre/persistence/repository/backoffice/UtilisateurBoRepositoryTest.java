/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice;

import java.util.Optional;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.commons.utils.RegistreBeanUtils;
import fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentDeclarantRepository;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

/**
 * Repository de l'entite {@link UtilisateurBoRepository}.
 * @version $Revision$ $Date${0xD}
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Sql({"classpath:/sql/backoffice/utilisateurBoRepositoryTest.sql"})
public class UtilisateurBoRepositoryTest
{

    /** Id de test. */
    private static final Long USER_ID_TEST = 1L;

    /** Email de test. */
    private static final String EMAIL_TEST = "test@test.fr";

    /** Repository de {@link fr.hatvp.registre.persistence.entity.backoffice.UtilisateurBoEntity} */
    @Autowired
    private UtilisateurBoRepository utilisateurBoRepository;

    /** Finalisation des données de tests. */
    @After
    public void endTest() throws InterruptedException
    {
        this.utilisateurBoRepository.deleteAll();
    }

    /** Test de la méthode {@link CommentDeclarantRepository#findByDeclarantEntityId(Long)}. */
    @Test(expected = ObjectOptimisticLockingFailureException.class)
    public void testOptimisticLocking()
    {
        final UtilisateurBoEntity user1 = this.utilisateurBoRepository.getOne(USER_ID_TEST);
        final UtilisateurBoEntity user2 = new UtilisateurBoEntity();
        RegistreBeanUtils.copyProperties(user2, user1);
        user2.setNom("Toto");

        user1.setCompteActive(false);
        this.utilisateurBoRepository.saveAndFlush(user1);

        this.utilisateurBoRepository.saveAndFlush(user2);
    }

    /**
     * Test de la méthode {@link UtilisateurBoRepository#findByEmail(String)}
     */
    @Test
    public void testFindByEmail()
    {
        final Optional<UtilisateurBoEntity> optional = this.utilisateurBoRepository.findByEmail(EMAIL_TEST);

        Assert.assertNotNull(optional);
        Assert.assertEquals(true, optional.isPresent());
        Assert.assertEquals(EMAIL_TEST, optional.get().getEmail());
    }
}
