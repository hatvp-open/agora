/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice;

import fr.hatvp.registre.persistence.entity.backoffice.EspaceOrganisationLockBoEntity;
import fr.hatvp.registre.persistence.repository.backoffice.EspaceOrganisationLockBoRepository;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Repository de l'entite
 * {@link fr.hatvp.registre.persistence.repository.backoffice.EspaceOrganisationLockBoRepository}.
 * @version $Revision$ $Date${0xD}
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Sql({"classpath:/sql/backoffice/EspaceOrganisationLockBoRepository.sql"})
public class EspaceOrganisationLockBoRepositoryTest
{

    /** Id de test. */
    private static final Long USER_ID_TEST = 1L;

    /** Repository des espace organisations. */
    @Autowired
    private EspaceOrganisationLockBoRepository espaceOrganisationLockBoRepository;

    /** Finalisation des données de tests. */
    @After
    public void endTest() throws InterruptedException
    {
        this.espaceOrganisationLockBoRepository.deleteAll();
    }

    /**
     * Test de la méthode {@link EspaceOrganisationLockBoRepository#findByEspaceOrganisationId(Long)}
     */
    @Test
    public void testFindByEspaceOrganisationId()
    {
        final List<EspaceOrganisationLockBoEntity> entities =
                this.espaceOrganisationLockBoRepository.findByEspaceOrganisationId(USER_ID_TEST);

        Assert.assertNotNull(entities);
        Assert.assertEquals(false, entities.isEmpty());
        Assert.assertEquals(1, entities.size());
    }
}
