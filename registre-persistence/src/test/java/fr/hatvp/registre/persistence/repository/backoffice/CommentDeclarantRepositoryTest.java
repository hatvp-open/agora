/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice;

import fr.hatvp.registre.persistence.entity.commentaire.CommentDeclarantEntity;
import fr.hatvp.registre.persistence.repository.backoffice.commentaire.CommentDeclarantRepository;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Repository de l'entite {@link CommentDeclarantRepository}.
 * @version $Revision$ $Date${0xD}
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Sql({"classpath:/sql/backoffice/commentDeclarantRepositoryTest.sql"})
public class CommentDeclarantRepositoryTest
{

    /**
     * Constante de TEST.
     */
    private static final Long DECLARANT_ID = 1L;

    /**
     * Repository de {@link fr.hatvp.registre.persistence.entity.commentaire.CommentDeclarantEntity}
     */
    @Autowired
    private CommentDeclarantRepository commentDeclarantRepository;

    /**
     * Finalisation des données de tests.
     */
    @After
    public void endTest()
    {
        this.commentDeclarantRepository.deleteAll();
    }

    /**
     * Test de la méthode {@link CommentDeclarantRepository#findByDeclarantEntityId(Long)}.
     */
    @Test
    public void testFinById()
    {
        final List<CommentDeclarantEntity> entity =
                this.commentDeclarantRepository.findByDeclarantEntityId(DECLARANT_ID);
        Assert.assertNotNull(entity);
        Assert.assertEquals(DECLARANT_ID, entity.get(0).getId());
    }

}
