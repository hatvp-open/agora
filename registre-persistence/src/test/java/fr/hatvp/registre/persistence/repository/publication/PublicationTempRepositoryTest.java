/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.publication;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.persistence.entity.publication.PublicationTempEntity;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

/**
 * Classe de test du repository {@link PublicationTempRepository}
 * @version $Revision$ $Date${0xD}
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Sql({"classpath:/sql/publicationTempRepositoryTest.sql"})
public class PublicationTempRepositoryTest
{

    /** Repository à tester. */
    @Autowired
    private PublicationTempRepository publicationTempRepository;

    private String httpSessionId = "123456789ABCDEF";
    
    private String jsonContent = "{\"stringTest\":\"Welcome \",\"intTest\":0,\"basicDto2\":{\"stringTest2\":\"Hello \",\"intTest2\":1},\"basicDto2List\":[{\"stringTest2\":\"Hello \",\"intTest2\":1},{\"stringTest2\":\"World!!! \",\"intTest2\":2}]}";
    
    /**
     * Finalisation des données de tests.
     */
    @After
    public void endTest()
    {
        this.publicationTempRepository.deleteAll();
    }

    /**
     * Test de la méthode {@link PublicationTempRepository#findByHttpSessionId(String)}.
     * Cas publication existante.
     */
    @Test
    public void testFindByHttpSessionId()
    {
        final PublicationTempEntity entity = this.publicationTempRepository.findByHttpSessionId(this.httpSessionId);
        Assert.assertNotNull(entity);
        Assert.assertEquals(this.jsonContent, entity.getJsonContent());
    }
    
    /**
     * Test de la méthode {@link PublicationTempRepository#findByHttpSessionId(String)}.
     * Cas publication non trouvée.
     */
    @Test
    public void testFindByHttpSessionIdNotFound()
    {
        final PublicationTempEntity entity = this.publicationTempRepository.findByHttpSessionId("");
        Assert.assertNull(entity);
    }

    /**
     * Test de la méthode {@link PublicationTempRepository#deleteByHttpSessionId(String)}.
     * 1 ligne supprimée.
     */
    @Test
    public void testDeleteByHttpSessionIdOneResult()
    {
        Long result = this.publicationTempRepository.deleteByHttpSessionId(this.httpSessionId);
        Assert.assertEquals(1L, result.longValue());
    }

    /**
     * Test de la méthode {@link PublicationTempRepository#deleteByHttpSessionId(String)}.
     * 0 ligne supprimée.
     */
    @Test
    public void testDeleteByHttpSessionIdNoResult()
    {
        Long result = this.publicationTempRepository.deleteByHttpSessionId("");
        Assert.assertEquals(0L, result.longValue());
    }

}
