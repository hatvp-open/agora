/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.backoffice;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.persistence.entity.backoffice.DemandeOrganisationLockBoEntity;
import fr.hatvp.registre.persistence.repository.backoffice.DemandeOrganisationLockBoRepository;
import fr.hatvp.registre.persistence.repository.backoffice.EspaceOrganisationLockBoRepository;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

/**
 * Repository de l'entite
 * {@link DemandeOrganisationLockBoRepository}.
 * @version $Revision$ $Date${0xD}
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Sql({"classpath:/sql/backoffice/DemandeOrganisationLockBoRepository.sql"})
public class DemandeOrganisationLockBoRepositoryTest
{

    /** Id de test. */
    private static final Long ID_TEST = 1L;

    /** Repository des espace organisations. */
    @Autowired
    private DemandeOrganisationLockBoRepository demandeOrganisationLockBoRepository;

    /** Finalisation des données de tests. */
    @After
    public void endTest() throws InterruptedException
    {
        this.demandeOrganisationLockBoRepository.deleteAll();
    }

    /**
     * Test de la méthode
     * {@link EspaceOrganisationLockBoRepository#findByEspaceOrganisationId(Long)}
     */
    @Test
    public void testFindByEspaceOrganisationId()
    {
        final List<DemandeOrganisationLockBoEntity> entities = this.demandeOrganisationLockBoRepository
                .findByDemandeOrganisationId(ID_TEST);

        Assert.assertNotNull(entities);
        Assert.assertEquals(false, entities.isEmpty());
        Assert.assertEquals(1, entities.size());
    }
}
