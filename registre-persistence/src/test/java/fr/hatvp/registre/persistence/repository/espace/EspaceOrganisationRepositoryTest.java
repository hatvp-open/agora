/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.persistence.entity.espace.EspaceOrganisationEntity;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;

/**
 * Test de l'interface {@link EspaceOrganisationRepository}
 * @version $Revision$ $Date${0xD}
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestRepositoryConfig.class }, loader = AnnotationConfigContextLoader.class)
@Sql({ "classpath:/sql/espaceOrganisationRepositoryTest.sql" })
public class EspaceOrganisationRepositoryTest {
	/**
	 * Repository à tester.
	 */
	@Autowired
	private EspaceOrganisationRepository espaceOrganisationRepository;

	/**
	 * Repository annexe
	 */
	@Autowired
	private OrganisationRepository organisationRepository;

	/**
	 * Mail utilisé pour les tests.
	 */
	private final Long ORGANISATION_ID = 1L;

	/**
	 * Finalisation des données de tests.
	 */
	@After
	public void endTest() {
		this.organisationRepository.deleteAll();
		this.espaceOrganisationRepository.deleteAll();
	}

	/**
	 * Test de la méthode findByEspaceOrganisationIdAndOrganisationId.
	 */
	@Test
	public void testFindEspaceOrganisationByOrganisationIdAndStatutNotRefusee() {
		final EspaceOrganisationEntity espaceOrganisationEntity = this.espaceOrganisationRepository.findEspaceOrganisationByOrganisationIdAndStatutNotRefusee(this.ORGANISATION_ID);

		Assert.assertNotNull(espaceOrganisationEntity);
		assertThat(espaceOrganisationEntity.getId()).isEqualTo(this.ORGANISATION_ID);
	}

	@Test
	public void testFindByNewPublicationTrueAndPublicationEnabledFalse() {
		final List<EspaceOrganisationEntity> esp = this.espaceOrganisationRepository.findByNewPublicationTrueAndPublicationEnabledFalse().collect(Collectors.toList());

		assertThat(esp.size()).isEqualTo(1);
		assertThat(esp.get(0).getNewPublication()).isTrue();
		assertThat(esp.get(0).getPublicationEnabled()).isFalse();
	}

	@Test
	public void testFindByNewPublicationTrueAndPublicationEnabledTrue() {
		final List<EspaceOrganisationEntity> esp = this.espaceOrganisationRepository.findByNewPublicationTrueAndPublicationEnabledTrue().collect(Collectors.toList());

		assertThat(esp.size()).isEqualTo(1);
		assertThat(esp.get(0).getNewPublication()).isTrue();
		assertThat(esp.get(0).getPublicationEnabled()).isTrue();
	}

	@Test
	public void testFindIdEspacesPublies() {
		final List<Long> esp = this.espaceOrganisationRepository.findIdEspacesPublies();
		assertThat(esp.size()).isEqualTo(1);
		assertThat(esp.get(0)).isEqualTo(2);
	}

}
