/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.declarant;

import fr.hatvp.registre.persistence.entity.declarant.DeclarantContactEntity;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;
import fr.hatvp.registre.persistence.repository.declarant.DeclarantContactRepository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test de l'interface {@link fr.hatvp.registre.persistence.repository.declarant.DeclarantContactRepository}
 * @version $Revision$ $Date${0xD}
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Sql({"classpath:/sql/declarantContactRepositoryTest.sql"})
public class DeclarantContactRepositoryTest
{

    /** Id de test */
    private static final Long ID_TEST = 1L;

    /** Repository des contact. */
    @Autowired
    private DeclarantContactRepository declarantContactRepository;

    /**
     * test de la méthode {@link DeclarantContactRepository#delete(Object)}
     */
    @Test
    public void testDeleteContact()
    {
        final DeclarantContactEntity declarantContactEntity = this.declarantContactRepository.findOne(ID_TEST);

        Assert.assertNotNull(declarantContactEntity);
        Assert.assertEquals(ID_TEST, declarantContactEntity.getId());

        this.declarantContactRepository.delete(declarantContactEntity);

        final DeclarantContactEntity deleted = this.declarantContactRepository.findOne(ID_TEST);
        
        Assert.assertNull(deleted);

    }
}
