/**
 *
 * Copyright (c) 2019-2020 HATVP <https://hatvp.fr>
 * SPDX-License-Identifier: MIT
 *
 */

package fr.hatvp.registre.persistence.repository.espace;

import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import fr.hatvp.registre.commons.lists.TypeIdentifiantNationalEnum;
import fr.hatvp.registre.persistence.entity.espace.OrganisationEntity;
import fr.hatvp.registre.persistence.repository.configuration.TestRepositoryConfig;
import fr.hatvp.registre.persistence.repository.espace.OrganisationRepository;

/**
 * Test de l'interface {@link OrganisationRepository}
 * @version $Revision$ $Date${0xD}
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestRepositoryConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Sql({"classpath:/sql/organisationRepositoryTest.sql"})
public class OrganisationRepositoryTest
{

    /**
     * Repository à tester.
     */
    @Autowired
    private OrganisationRepository organisationRepository;

    /**
     * National Id pour les tests.
     */
    private final Long ID_TEST = 1L;
    private final String SIREN_TEST = "123456789";
    private final String SIREN_TEST_2 = "987654321";
    private final String RNA_UCASE_TEST = "W123456789";
    private final String RNA_LCASE_TEST = "w123456789";
    private final String HATVP_NUMBER_UCASE_TEST = "H123456789";
    private final String HATVP_NUMBER_LCASE_TEST = "h123456789";

    /**
     * Finalisation des données de tests.
     */
    @After
    public void endTest()
    {
        this.organisationRepository.deleteAll();
    }

    /**
     * Test de la méthode findFirstBySiren.
     */
    @Test
    public void testFindByIdExists()
    {
        final Optional<OrganisationEntity> result = this.organisationRepository
                .findById(this.ID_TEST);
        Assert.assertNotNull(result);
    }

    @Test
    public void testFindBySirenExists()
    {
        final Optional<OrganisationEntity> result1 = this.organisationRepository
                .findBySiren(this.SIREN_TEST);
        Assert.assertTrue(result1.isPresent());

        Assert.assertEquals(TypeIdentifiantNationalEnum.SIREN, result1.get().getOriginNationalId());
    }

    @Test
    public void testFindByRnaExists()
    {
        final Optional<OrganisationEntity> result1 = this.organisationRepository
                .findByRna(this.RNA_LCASE_TEST);
        Assert.assertTrue(result1.isPresent());

        final Optional<OrganisationEntity> result2 = this.organisationRepository
                .findByRna(this.RNA_UCASE_TEST);
        Assert.assertTrue(result2.isPresent());

        Assert.assertEquals(result1.get(), result2.get());

        Assert.assertEquals(TypeIdentifiantNationalEnum.RNA, result1.get().getOriginNationalId());
    }

    @Test
    public void testFindByHatvpNumberExists()
    {
        final OrganisationEntity result1 = this.organisationRepository
                .findByHatvpNumber(this.HATVP_NUMBER_LCASE_TEST);
        Assert.assertTrue(Optional.of(result1).isPresent());

        final OrganisationEntity result2 = this.organisationRepository
                .findByHatvpNumber(this.HATVP_NUMBER_UCASE_TEST);
        Assert.assertTrue(Optional.of(result2).isPresent());

        Assert.assertEquals(result1, result2);

        Assert.assertEquals(TypeIdentifiantNationalEnum.HATVP, result1.getOriginNationalId());
    }

    @Test
    public void testFindBySirenOrRnaOrHatvpNumberExistsSiren()
    {
        final List<OrganisationEntity> result1 = this.organisationRepository
                .findBySirenOrRnaOrHatvpNumber(this.SIREN_TEST, this.SIREN_TEST, this.SIREN_TEST);
        Assert.assertFalse(result1.isEmpty());

        Assert.assertEquals(TypeIdentifiantNationalEnum.SIREN, result1.get(0).getOriginNationalId());
    }

    @Test
    public void testFindBySirenOrRnaOrHatvpNumberExistsRna()
    {
        final List<OrganisationEntity> result1 = this.organisationRepository
                .findBySirenOrRnaOrHatvpNumber(this.RNA_LCASE_TEST, this.RNA_LCASE_TEST,
                        this.RNA_LCASE_TEST);
        Assert.assertFalse(result1.isEmpty());

        final List<OrganisationEntity> result2 = this.organisationRepository
                .findBySirenOrRnaOrHatvpNumber(this.RNA_UCASE_TEST, this.RNA_UCASE_TEST,
                        this.RNA_UCASE_TEST);
        Assert.assertFalse(result2.isEmpty());

        Assert.assertEquals(result1.size(), result2.size());
        Assert.assertEquals(result1.get(0), result2.get(0));

        Assert.assertEquals(TypeIdentifiantNationalEnum.RNA, result1.get(0).getOriginNationalId());
    }

    @Test
    public void testFindBySirenOrRnaOrHatvpNumberExistsHatvpNumber()
    {
        final List<OrganisationEntity> result1 = this.organisationRepository
                .findBySirenOrRnaOrHatvpNumber(this.HATVP_NUMBER_LCASE_TEST,
                        this.HATVP_NUMBER_LCASE_TEST, this.HATVP_NUMBER_LCASE_TEST);
        Assert.assertFalse(result1.isEmpty());

        final List<OrganisationEntity> result2 = this.organisationRepository
                .findBySirenOrRnaOrHatvpNumber(this.HATVP_NUMBER_UCASE_TEST,
                        this.HATVP_NUMBER_UCASE_TEST, this.HATVP_NUMBER_UCASE_TEST);
        Assert.assertFalse(result2.isEmpty());

        Assert.assertEquals(result1.size(), result2.size());
        Assert.assertEquals(result1.get(0), result2.get(0));

        Assert.assertEquals(TypeIdentifiantNationalEnum.HATVP, result1.get(0).getOriginNationalId());
    }

    /**
     * Pas de résultat si espace d'organisation créé.
     */
    @Test
    public void testFindByEspaceOrganisationIsNullAndSirenAndOriginNationalIdNotNull()
    {
        final Optional<OrganisationEntity> result1 = this.organisationRepository
                .findByEspaceOrganisationIsNullAndSirenAndOriginNationalIdSiren(this.SIREN_TEST);

        Assert.assertFalse(result1.isPresent());
    }

    /**
     * Résultat si pas d'espace organisation créé.
     */
    @Test
    public void testFindByEspaceOrganisationIsNullAndSirenAndOriginNationalIdNull()
    {
        final Optional<OrganisationEntity> result1 = this.organisationRepository
                .findByEspaceOrganisationIsNullAndSirenAndOriginNationalIdSiren(this.SIREN_TEST_2);

        Assert.assertTrue(result1.isPresent());
    }
}