-- Insert back-office user
INSERT INTO public.utilisateur_bo(id, version, compte_actif, compte_supprime, email, nom, password, prenom)
VALUES (1,0, TRUE, FALSE, 'test@test.fr', 'test', 'test','test');

-- Insert front-office user
INSERT INTO public.declarant(id, civilite, nom, prenom, date_naissance, email, telephone, compte_actif, password, key_email_confirmation, date_expiration_key_email, key_recuperation_compte,premiere_connexion)
	VALUES (1, 'M', 'SAID EL IMAM', 'SAID', '1990-08-20', 'test@hatvp.fr', '0613311331', true, 'password', 'ffzfzekinfzenf', '2990-01-01', 'ffzfzekinfzenf',false);

-- insert declarant comment
INSERT INTO public.commentaire_declarant(id, version, date_creation, message, editeur_id, declarant_id)
VALUES (1, 0, current_date, 'my first comment', 1, 1);