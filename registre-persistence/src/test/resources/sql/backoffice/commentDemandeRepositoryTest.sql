INSERT INTO public.declarant(id, civilite, nom, prenom, date_naissance, email, telephone, compte_actif, password, key_email_confirmation, date_expiration_key_email, key_recuperation_compte,premiere_connexion)
	VALUES (1, 'M', 'SAID EL IMAM', 'SAID', '1990-08-20', 'test@hatvp.fr', '0613311331', true, 'password', 'ffzfzekinfzenf', '2990-01-01', 'ffzfzekinfzenf',false);

INSERT INTO public.demande_organisation (id, version, adresse, codepostal, commentaire, context, denomination, dirigeant, identifiant, pays, type, ville, declarant_id, espace_organisation_id, date_creation, statut)
 VALUES (1, 0, '49 Rue du Pré Saint-Gervais', '93500', 'ckjgdehlwu hflwsr hflsweuhf luswdfg klwsgd fk', 'Ajout_Espace', 'SNCF Fret', 'Said', '1638435435', 'France', 'SOCOM', 'Paris', 1, null, '2017-05-30 16:15:05', 'A_TRAITER');

INSERT INTO public.utilisateur_bo(id, version, compte_actif, compte_supprime, email, nom, password, prenom)
VALUES (1,1, TRUE, FALSE, 'test@test.fr', 'test', 'test', 'test');

INSERT INTO public.demande_organisation_lock_bo(id, version, locked_time, demande_organisation_id, utilisateur_bo_id)
VALUES (1, 1, '2990-01-01' , 1 , 1);

-- insert declarant comment
INSERT INTO public.commentaire_demande(id, version, date_creation, message, editeur_id, demande_organisation_id)
VALUES (1, 0, current_date, 'my first comment', 1, 1);