INSERT INTO public.declarant(id, civilite, nom, prenom, date_naissance, email, telephone, compte_actif, password, key_email_confirmation, date_expiration_key_email, key_recuperation_compte,premiere_connexion)
	VALUES (1, 'M', 'TEST', 'TEST', '1990-08-20', 'test@hatvp.fr', '0613311331', true, 'password', 'ffzfzekinfzenf', '2990-01-01', 'ffzfzekinfzenf',false);

INSERT INTO public.organisation(id, siren, type_identifiant_national, denomination, adresse, date_creation)
	VALUES (1,'123456752', 'SIREN', 'TOTO', 'paris', '2990-01-01');

INSERT INTO public.espace_organisation(id, date_creation, statut_creation_bo, non_declaration_tiers, non_declaration_association, new_publication, publication_enabled, organisation_id,espace_actif, createur_espace_id, representant_legal,elements_a_publier,relances_bloquees)
	VALUES (1,'2990-01-01', 'EN_TRAITEMENT', FALSE , FALSE , FALSE, FALSE, 1, true, 1, FALSE,FALSE,FALSE );

INSERT INTO public.utilisateur_bo(id, version, compte_actif, compte_supprime, email, nom, password, prenom)
VALUES (1,1, TRUE, FALSE, 'test@test.fr', 'test', 'test', 'test');

INSERT INTO public.espace_organisation_lock_bo(id, version, locked_time, espace_organisation_id, utilisateur_bo_id)
VALUES (1, 1, '2990-01-01' , 1 , 1);
