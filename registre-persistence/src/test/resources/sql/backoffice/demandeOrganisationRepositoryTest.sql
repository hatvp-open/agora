INSERT INTO public.declarant(
	id, version, compte_actif, date_naissance, civilite, email, date_expiration_key_email, key_email_confirmation, email_temp, date_derniere_connexion, nom, password, prenom, date_expiration_key_recuperation, key_recuperation_compte, telephone,premiere_connexion)
	VALUES (1, 1, true, '1982-03-16 01:00:00', 'M', 'bla', '2017-12-02 11:17:53.973', 'kk5RbNHq8qvCacq9yM7CIHGwjICsvR', 'c.f@mail.com', '2017-12-01 17:19:39.954', 'bla', '$2a$10$Qzd3LVofYNNC8hYvMGROr.Ll1kxu1bT1Lf.dG7BljjrQZgA391wgC', 'prénom', null, null, '0000000000',false);

INSERT INTO public.organisation(id, adresse, date_creation, denomination, siren, type_identifiant_national)
	VALUES (1, '6 Avenue de l''arche', '2017-01-01', 'SNCF FRET', 'f54s5f4sd53f8', 'SIREN');
	
INSERT INTO public.espace_organisation(id, date_creation, statut_creation_bo, non_declaration_tiers, non_declaration_association, new_publication, publication_enabled, organisation_id, espace_actif, createur_espace_id, representant_legal,elements_a_publier,relances_bloquees)
	VALUES (1, '2017-01-01', 'EN_TRAITEMENT', TRUE, TRUE , TRUE, TRUE, 1, TRUE, 1, FALSE ,FALSE,FALSE);

INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (1, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (2, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (3, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (4, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (5, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (6, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (7, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (8, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (9, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (10, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (11, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (12, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (13, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (14, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (15, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (16, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (17, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (18, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (19, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (20, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (21, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (22, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (23, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (24, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (25, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (26, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (27, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (28, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'A_TRAITER', 'SOCIV', 'Paris', 1, 1);
	
INSERT INTO public.demande_organisation(
			id, version, adresse, 			codepostal, commentaire, context, 		 date_creation, 			denomination, 	dirigeant, 	 identifiant, pays, 	statut, 	 type,    ville,   declarant_id, espace_organisation_id)
	VALUES (29, 1, 'rue de Richelieu', '75015', 'cmt', 'Ajout_Espace', '2017-11-23 11:08:29.634', 'denomination', 'dirigeant', 'id_text', 'France', 'TRAITEE_ORGANISATION_CREEE', 'SOCIV', 'Paris', 1, 1);
	
	