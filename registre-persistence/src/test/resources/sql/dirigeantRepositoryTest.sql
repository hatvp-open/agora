INSERT INTO public.declarant(id, civilite, nom, prenom, date_naissance, email, telephone, compte_actif, password, key_email_confirmation, date_expiration_key_email, key_recuperation_compte, email_temp,premiere_connexion)
	VALUES (2, 'M', 'SAID EL IMAM', 'SAID', '1990-08-20', 'test@hatvp.fr', '0613311331', true, 'password', 'ffzfzekinfzenf', '2990-01-01', 'ffzfzekinfzenf', 'test_temp@hatvp.fr',false);

INSERT INTO public.organisation(id, siren, type_identifiant_national, denomination, adresse, date_creation)
	VALUES (1,'123456752', 'SIREN', 'TOTO', 'paris', '2990-01-01');
	
INSERT INTO public.espace_organisation(id, date_creation, statut_creation_bo, non_declaration_tiers, non_declaration_association, new_publication, publication_enabled, organisation_id, espace_actif, createur_espace_id, representant_legal,elements_a_publier,relances_bloquees)
	VALUES (1,'2990-01-01', 'EN_TRAITEMENT', FALSE, FALSE, TRUE, FALSE, 1, TRUE, 2, FALSE, FALSE,FALSE);

INSERT INTO public.dirigeant(
	id, version, civilite, fonction, nom, prenom, espace_organisation_id)
	VALUES (1, 1, 'M', 'Président', 'Muche', 'Truc', 1);