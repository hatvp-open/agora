INSERT INTO public.declarant(id, civilite, nom, prenom, date_naissance, email, telephone, compte_actif, password, key_email_confirmation, date_expiration_key_email, key_recuperation_compte,premiere_connexion)
	VALUES (1, 'M', 'SAID EL IMAM', 'SAID', '1990-08-20', 'test@hatvp.fr', '0613311331', true, 'password', 'ffzfzekinfzenf', '2990-01-01', 'ffzfzekinfzenf',false);

INSERT INTO public.organisation(id, adresse, date_creation, denomination, siren, type_identifiant_national)
	VALUES (1, '6 Avenue de l''arche', '2017-01-01', 'SNCF FRET', 'f54s5f4sd53f8', 'SIREN');
	
INSERT INTO public.espace_organisation(id, date_creation, statut_creation_bo, non_declaration_tiers, non_declaration_association, new_publication, publication_enabled, organisation_id, espace_actif, createur_espace_id, representant_legal,elements_a_publier,relances_bloquees)
	VALUES (1, '2017-01-01', 'EN_TRAITEMENT', TRUE, TRUE , TRUE, TRUE, 1, TRUE, 1, FALSE,FALSE,FALSE );
	
INSERT INTO public.inscription_espace(id, declarant_id, espace_organisation_id, favori, actif, verrouille, date_demande, date_validation, date_suppression)
	VALUES (1, 1, 1, TRUE, TRUE, FALSE , '2017-01-02', null, null);
	
