-- Insérer un déclarant.
INSERT INTO public.declarant(id, civilite, nom, prenom, date_naissance, email, telephone, compte_actif, password, key_email_confirmation, date_expiration_key_email, key_recuperation_compte, email_temp,premiere_connexion)
	VALUES (1, 'M', 'SAID EL IMAM', 'SAID', '1990-08-20', 'test@hatvp.fr', '0613311331', true, 'password', 'ffzfzekinfzenf', '2990-01-01', 'ffzfzekinfzenf', 'test_temp@hatvp.fr',false);

-- Insérer un contact.
INSERT INTO public.declarant_contact(id, version, categorie_contact, email_contact, telephone_contact, declarant_id)
VALUES (1, 1, 'PERSO', 'test@test.fr', '075xxxxxxx', 1);